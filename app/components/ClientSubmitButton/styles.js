

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    submitBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        marginRight: '15px'
    }
}));


export default useStyles;
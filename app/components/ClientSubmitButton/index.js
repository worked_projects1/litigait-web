/**
 *
 * ClientSubmitButton
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import ModalDialog from 'components/ModalDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import Styles from './styles';

function ClientSubmitButton(props) {

  const classes = Styles();

  const { handleSubmit, pristine, submitting, invalid, change } = props;

  const [loading, setLoading] = useState(false);

  const handleForm = (model, type) => {
    setLoading(true);
    if (type === 'Yes') {
      change('isRedirectURL', '/casesCategories/cases/create');
    }

    setTimeout(() => {
      handleSubmit();
      setLoading(false);
    }, 2000);
  }

  const formOptions = [
    {
      action: handleForm,
      value: 'Yes',
      label: 'Yes'
    },
    {
      action: handleForm,
      value: 'Later',
      label: 'Later'
    }
  ];


  return <ModalDialog
    title={"Do you want to add a new case for this client now?"}
    disableContainer
    onConfirmPopUpClose={true}
    options={formOptions}>
    {(open) => <Grid>
      <Button
        type="button"
        disabled={pristine || submitting || (!pristine && invalid)}
        variant="contained"
        onClick={!invalid ? open : null}
        color="primary"
        className={classes.submitBtn}>
        {(submitting || loading) && <ButtonSpinner /> || 'Create'}
      </Button>
    </Grid>}
  </ModalDialog>;
}

ClientSubmitButton.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  invalid: PropTypes.bool,
  submitting: PropTypes.bool,
  change: PropTypes.func
};

export default ClientSubmitButton;

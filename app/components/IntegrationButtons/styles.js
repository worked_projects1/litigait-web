

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  integration: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '24px'
  },
  integrationOption: {
    display: 'flex',
    flexWrap: 'wrap'
  }, 
  integrationBtn: {
    display: 'flex',
    marginTop: '18px',
    border: '1px solid #2ca01c',
    color: '#000000',
    textTransform: 'none',
    fontWeight: 'bold',
    "&:hover": {
      backgroundColor: '#2ca01c',
      color: '#ffff',
      border: '1px solid #2ca01c'
    }
  },
  googleBtn: {
    backgroundImage: `url(${require('images/integrations/google.png')})`,
    backgroundRepeat: 'no-repeat',
    backgroundOrigin: 'content-box',
    backgroundSize: '25px'
  },
  microsoftBtn: {
    backgroundImage: `url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMyAyMyI+PHBhdGggZmlsbD0iI2YzZjNmMyIgZD0iTTAgMGgyM3YyM0gweiIvPjxwYXRoIGZpbGw9IiNmMzUzMjUiIGQ9Ik0xIDFoMTB2MTBIMXoiLz48cGF0aCBmaWxsPSIjODFiYzA2IiBkPSJNMTIgMWgxMHYxMEgxMnoiLz48cGF0aCBmaWxsPSIjMDVhNmYwIiBkPSJNMSAxMmgxMHYxMEgxeiIvPjxwYXRoIGZpbGw9IiNmZmJhMDgiIGQ9Ik0xMiAxMmgxMHYxMEgxMnoiLz48L3N2Zz4=)`,
    backgroundRepeat: 'no-repeat',
    backgroundOrigin: 'content-box',
  },
  integrationImg: {
    display: 'flex',
    justifyContent: 'center',
    alignItem: 'center',
    height: '1rem',
    width: '1rem',
    flexShrink: '0',
    marginRight: '0.25rem',
    top: 'auto'
  }
}));


export default useStyles;
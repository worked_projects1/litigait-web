import React from 'react';
import cx from 'clsx';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';

function IntegrationButtons({ googleBtnLabel, microsoftBtnLabel, googleAuth, microsoftAuth }) {
    const classes = Styles();

    return (
        <>
            <Grid container style={{ marginTop: '10px'}} direction='row' justify='center' alignItems='center'>
                <Button fullWidth color='primary' onClick={microsoftAuth} variant="outlined" className={cx(classes.integrationBtn, classes.microsoftBtn)}>
                    <span className='btn_text'>{microsoftBtnLabel}</span>
                </Button>
                <Button id="google-signin-button" fullWidth color='primary' onClick={googleAuth} variant="outlined" className={cx(classes.integrationBtn, classes.googleBtn)}>
                    <span className='btn_text'>{googleBtnLabel}</span>
                </Button>
            </Grid>
        </>
    )
}

export default IntegrationButtons;
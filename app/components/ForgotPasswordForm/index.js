/**
 * 
 * Forgot Password Form
 * 
 */


import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Grid, Button } from '@material-ui/core';
import InputField from 'components/InputField';
import ButtonSpinner from 'components/ButtonSpinner';
import Styles from './styles';
import Success from 'components/Success';
import Error from 'components/Error';

function ForgotPasswordForm({ handleSubmit, submitting, success, error, errorMessage, clearCache, destroy, locationState, change, iframe }) {

    const classes = Styles();

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Grid container direction="row" justify="center" alignItems="center" style={{ marginBottom: '20px' }}>
                <a href={process.env.ENV === 'production' ? `https://www.esquiretek.com/` : `https://staging.esquiretek.com/`} >
                    <img src={require('images/icons/new_logo.jpg')} style={{ width: '80%' }} />
                </a>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Field name="email" label="Email" component={InputField} type="text" required autoFocus />
                </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit} >
                {submitting && <ButtonSpinner /> || 'Request Reset'}
            </Button>
            <Grid item xs={12}>
                {error ? <Error errorMessage={error} onClose={() => change('_error', null)} /> : null}
                {success && success.forgot ? <Success successMessage={success.forgot} onClick={clearCache} /> : null}
            </Grid>
            <Grid className={classes.div}>
                <Grid item xs>
                    <Link to={{
                        pathname: iframe ? '/signin' : '/',
                        state: Object.assign({}, { ...locationState }, { form: 'login' })
                    }} className={classes.linkColor}>
                        Back to Sign In
                    </Link>
                </Grid>
            </Grid>
        </form>
    )
}

const validate = (values) => {

    const errors = {}

    const requiredFields = ['email'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
    ) {
        errors.email = 'Invalid Email'
    }

    return errors
}


export default reduxForm({
    form: 'forgot',
    validate,
})(ForgotPasswordForm);


import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#2ca01c',
    textTransform: 'none',
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      backgroundColor: '#2ca01c',
    }
  },
  div: {
    textAlign: 'center',
    marginTop: theme.spacing(1.3),
  },
  linkColor: {
    color: '#0077c5',
    textDecoration: 'none'
  }
}));


export default useStyles;
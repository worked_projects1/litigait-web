
/**
 * 
 * Custom File Upload
 * 
 * 
 */



import React, { useCallback, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import PublishIcon from '@material-ui/icons/Publish';
import styles from './styles';
import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
import ProgressProvider from 'components/ProgressProvider';
import 'react-circular-progressbar/dist/styles.css';
import { Grid, Button, Paper, Typography } from '@material-ui/core';
import remotes from 'blocks/upload/remotes.js';
import { getContentType } from '../FileUpload/utils';
import Snackbar from 'components/Snackbar';
import axios from 'axios';

function CustomFileUpload({ input, label, multiple, max, contentType, labelValue, meta: { touched, error, warning } }) {
    const inputValue = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];
    const [uploadError, setuploadError] = useState(false);
    const [uploadPercentage, setUploadPercentage] = useState(false);
    const classes = styles();


    const onUpload = async (files) => {
        let uploadLimit = files && files.length + inputValue.length <= max;
        if (uploadLimit) {
            setUploadPercentage(0);
            const promises = await files.map(async (file, i) => {
                const Upload = remotes.getAttachDocuments;
                const fileName = file.name;
                const fileContentType = getContentType(fileName);
                return await Promise.resolve(Upload(fileName, fileContentType)
                .then(async (result) => {
                    if (result) {
                        return await axios.put(result.uploadURL, file, {
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': fileContentType,
                            },
                            onUploadProgress: (e) => setUploadPercentage(Math.round((e.loaded / e.total) * 100))
                        }).then(async () => {
                            return await Object.assign({}, { ...result }, { filename: fileName, contentType: file.type });
                        }).catch(err => setuploadError('Failed to upload files'))
                    }
                }))
            });

            Promise.all(promises).then((data) => {
                setUploadPercentage(0);
                input.onChange(inputValue.concat(data));
                
            });
        } else if (files) {
            setuploadError(`You cannot upload more than ${max} files`);
        }
    }

    const onDrop = useCallback(async (uploadedFiles) => {

        const acceptedFiles = uploadedFiles.map(file => new File([file], file.name.replace(/[^a-zA-Z.0-9]/g, ""), { type: file.type, lastModified: file.lastModified}));

        if(acceptedFiles && acceptedFiles.length) {
            if(acceptedFiles.every(file => file && file.size === 0)) {
                setuploadError(`You cannot upload the 0 kb files.`);
                return;
            } 
        }
        
        await onUpload(acceptedFiles)
    }, [onUpload]);

    const onDropRejected = useCallback((error) => {
        setuploadError(error[0].errors[0].message);
    }, []);

    const { getRootProps, open, getInputProps } = useDropzone({ accept: contentType, multiple, onDrop, onDropRejected });

    return (
        <Grid container className={classes.root}>
            {label && <Grid>
                <Typography variant="subtitle1" gutterBottom component="span">
                    {label || ''}
                </Typography>
            </Grid>}
            <Grid className={classes.paper}>
                {inputValue && inputValue.length > 0 && <Grid className={classes.gridList} style={{ margin: '15px' }}>
                    {inputValue && inputValue.map((file, i) => {
                        return (
                            <Grid container className={classes.gridList} key={file}>
                                <Grid item xs={10} style={{ paddingRight: '4px', margin: '10px 0' }}>
                                    <Typography variant="subtitle2" gutterBottom component="span">{file.filename}</Typography>
                                </Grid>
                                <Grid item xs={2} className={classes.gridDelete}>
                                    <img src={require('images/icons/trash.svg')} className={classes.attachPdf} onClick={() => input.onChange(inputValue.filter((f, key) => key !== i))} />
                                </Grid>
                            </Grid>
                        );
                    })}
                </Grid>}
                {inputValue && inputValue.length > 0 && <Grid item xs={12} ><hr className={classes.hr} /></Grid>}
                <Grid container>
                    <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container} style={{ marginTop: '0' }}>
                        <input {...getInputProps()} />
                        <Paper className={classes.upload} >
                            <ProgressProvider values={input.value ? [100] : inputValue && !uploadError ? [0, 50, 100] : [0]}>
                                {percentage => (<CircularProgressbarWithChildren
                                    value={uploadPercentage}
                                    className={classes.progress}
                                    styles={buildStyles({
                                        pathColor: '#2ca01c',
                                        pathTransition:
                                            percentage === 0
                                                ? "none"
                                                : "stroke-dashoffset 0.5s ease 0s",
                                            width: 80, 
                                            height: 80
                                    })}>
                                    {(uploadPercentage) ? <div className={classes.percentage}>{uploadPercentage}%</div> : <PublishIcon className={classes.icon} />}
                                </CircularProgressbarWithChildren>)}
                            </ProgressProvider>
                        </Paper>
                        <Grid className={classes.animate}>
                            <div className={classes.circle} style={{ animationDelay: '0s' }} />
                            <div className={classes.circle} style={{ animationDelay: '1s' }} />
                            <div className={classes.circle} style={{ animationDelay: '2s' }} />
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container className={classes.container} justify="center" direction="column" style={{ marginTop: '0' }}>
                            <Typography variant="subtitle2" gutterBottom component="span">{`Drag file to upload, or`}</Typography>
                            <Typography variant="subtitle3" gutterBottom component="span">
                                <Button type="button" variant="contained" color="primary" onClick={open} className={classes.button}>
                                    {`Choose ${labelValue}`}
                                </Button>
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Snackbar show={uploadError ? true : false} text={uploadError} severity={'error'} handleClose={() => setuploadError(false)} />
        </Grid>
    );
}

export default CustomFileUpload;



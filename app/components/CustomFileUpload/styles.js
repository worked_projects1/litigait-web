import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    height: 'auto',
    marginBottom: '10px',
    padding: '10px 8px',
    border: '1px solid #ddd',
    boxSizing: 'border-box',
    marginTop: '20px',
    flexBasis: '100%',
    marginRight: '10px',
    borderRadius: '15px'
  },
  progress: {
    height: '80px',
    width: '80px'
  },
  circle: {
    borderRadius: '50%',
    border: '1px solid #2ca01c',
    width: '89px',
    height: '89px',
    boxShadow: theme.shadows[2],
    position: 'absolute',
    opacity: '0',
    top: '0',
    animation: `$scaleIn 4s infinite cubic-bezier(.36, .11, .89, .32)`,
  },
  '@keyframes scaleIn': {
    "from": {
      transform: 'scale(.3, .3)',
      opacity: '.5'
    },
    "to": {
      transform: 'scale(1.5, 1.5)',
      opacity: '0'
    }

  },
  container: {
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: '25px',
    outline: 'none',
    position: 'relative'
  },
  upload: {
    boxShadow: 'none',
    width: '89px',
    height: '89px',
    zIndex: '100',
    padding: '5px',
    outline: 'none',
    display: 'inline-block',
    borderRadius: '100%',
    position: 'relative'
  },
  icon: {
    color: '#2ca01c',
    position: 'relative',
    display: 'flex',
    width: '40%',
    fontSize: '50px'
  },
  button: {
    fontWeight: 'bold',
    borderRadius: '28px',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular',
    fontSize: '11px',
    marginTop: '10px',
    outline: 'none'
  },
  animate: {
    display: 'flex',
    width: '100%',
    justifyContent: 'center'
  },
  percentage: {
    color: '#2ca01c',
    fontWeight: 'bold',
    fontSize: '13px'
  },
  gridContainer: {
    width: 'auto',
    alignItems: 'center',
  },
  gridDelete: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  gridList: {
    overflow: 'auto',
    height: 'auto',
    maxHeight: '450px'
  },
  attachPdf: {
    width: '18px',
    height: 'auto',
    cursor: 'pointer',
    marginLeft: '25px'
  },
  hr: {
    borderTop: '1px solid lightgray',
    borderBottom: 'none'
  }
}));

export default useStyles;

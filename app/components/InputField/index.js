
/***
 *
 * Input Field
 *
 */


import React from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import AddIcon from '@material-ui/icons/Add';
import SubIcon from '@material-ui/icons/Remove';
import clsx from 'clsx';


export default function ({ input, label, placeholder, autoFocus, type, disabled, prefix, style, variant, multiline, rows, rowMax, className, errorStyle, defaultBlur, enablePlusMinusIcon, disabledArrow, inputProps, asterisks, meta: { touched, error, warning } }) {

  const classes = Styles();
  const { name, value, onChange } = input;

  const InputChange = defaultBlur ? Object.assign({}, {
    onBlur: (e) => onChange(e.target.value),
    defaultValue: value || ''
  }) : Object.assign({}, {
    onChange: (e) => onChange(e.target.value),
    value: value || ''
  });

  const handleChangePlus = () => {
    if (value === "")
      input.onChange(1);
    else
      input.onChange(parseInt(value) + 1);
  }

  const handleChangeMinus = () => {
    if (parseInt(value) > inputProps?.min) {
      input.onChange(parseInt(value) - 1);
    }
  }

  return (
    <div style={style || {}}>
      <TextField
        name={name}
        type={type}
        label={asterisks ? <span className={classes.textSize}>{label} <span className={classes.mandatory}>*</span></span> : <span className={classes.textSize} >{label}</span>}
        placeholder={placeholder}
        disabled={disabled}
        className={className || (disabledArrow ? clsx(classes.disableArrow, classes.fieldColor) : classes.fieldColor)}
        fullWidth
        variant={variant || "standard"}
        {...InputChange}
        InputProps={{
          startAdornment: prefix ? <InputAdornment position="start">{prefix}</InputAdornment> : null,
          endAdornment: enablePlusMinusIcon ? (
            <InputAdornment position="end">
              <SubIcon className={classes.iconMinus} onClick={handleChangeMinus} /> {' '} 
              <AddIcon className={classes.iconPlus} onClick={handleChangePlus} />
            </InputAdornment>) : null
        }}
        autoFocus={autoFocus}
        multiline={multiline || false}
        rows={rows || 1} />
      <div style={errorStyle || {}} className={classes.error}>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>

  )
}

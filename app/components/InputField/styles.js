

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  fieldColor: {
    '& :after': {
      borderBottomColor: '#2ca01c',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }
  },
  textSize: {
    fontSize: '14px',
  },
  error: {
    fontSize: '14px',
    color: 'red',
    marginTop: '5px'
  },
  iconMinus: {
    borderRadius: '8%',
    backgroundColor: '#ccc',
    width: '20px',
    height: '20px',
    border: '1px solid #ccc',
    cursor: 'pointer',
    color: '#fff',
    "&:hover": {
      transform: 'scale(1.2)'
    },
  },
  iconPlus: {
    borderRadius: '8%',
    backgroundColor: '#2ca01c',
    width: '20px',
    height: '20px',
    marginLeft: '5px',
    color: '#fff',
    border: '1px solid #2ca01c',
    cursor: 'pointer',
    "&:hover": {
      transform: 'scale(1.2)'
    },
  },
  disableArrow: {
    '& input[type=number]': {
      '-moz-appearance': 'textfield'
    },
    '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0
    }
  },
  mandatory: { 
    color: 'red', 
    fontSize: '16px' 
  }
}));


export default useStyles;
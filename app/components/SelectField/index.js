/**
 * 
 * Select Field
 * 
 */

import React from 'react';
import Styles from './styles';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

export default function ({ input, label, required, metaData, options, variant, disabled, children, style, asterisks, meta: { touched, error, warning } }) {
    const classes = Styles();

    const { name, value } = input;
    const isPreDefinedSet = Array.isArray(options);

    const itemHeight = 48;
    const itemPaddingTop = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: itemHeight * 4.5 + itemPaddingTop,
            },
        },
    };

    return (
        <div className={classes.selectField} style={style || {}}>
            {children || ''}
            <FormControl variant={variant || null} className={classes.formControl}>
                <InputLabel className={classes.fieldColor} id={`${name}-id`}>
                    {asterisks ? <span>
                        {label} <span className={classes.mandatory}>*</span>
                    </span> :  <span>{label}</span>}
                </InputLabel>
                <Select
                    name={name}
                    fullWidth
                    disabled={disabled}
                    required={required}
                    labelId={`${name}-id`}
                    value={value}
                    MenuProps={MenuProps}
                    onChange={(e) => input.onChange(e.target.value)}>
                    {(typeof options === 'string' && metaData[options] === undefined ) ? <MenuItem value="">No options</MenuItem> : isPreDefinedSet ? (options || []).map((opt, index) => <MenuItem
                        key={index} disabled={opt.disabled || false} value={opt && opt.value || opt}>{opt && opt.label || opt}</MenuItem>) : (typeof options === 'string' && Object.keys(metaData[options]).length === 0) ? <MenuItem value="">No options</MenuItem> : (metaData[options] || []).map((opt, index) => <MenuItem key={index} disabled={opt.disabled || false} value={opt && opt.value != null
                            && opt.value || opt}>{opt && opt.label != null && opt.label || opt}</MenuItem>)
                    }
                </Select>
            </FormControl>
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>
    )
}


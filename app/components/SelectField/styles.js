

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%'
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
  fieldColor: {
    fontSize: '14px',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.MuiFormLabel-root.Mui-focused': {
      borderColor: '#2ca01c',
      color: '#2ca01c'
    }
  },
  selectField: {
    '& .MuiInput-underline:after': {
      borderBottomColor: '#2ca01c',
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: '#2ca01c',
    },
    '@global': {
      '.MuiOutlinedInput-notchedOutline': {
        border: 'none'
      }
    }
  },
  mandatory: { 
    color: 'red', 
    fontSize: '16px' 
  }
}));


export default useStyles;
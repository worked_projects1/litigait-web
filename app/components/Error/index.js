/**
 * 
 * Error
 * 
 */

import React, { useEffect } from 'react';
import Alert from '@material-ui/lab/Alert';

const errorMessage = ({ style, errorMessage, onClose }) => {

    useEffect(() => {
        if (onClose) {
            setTimeout(onClose, 4000);
        }
    }, []);

    return (<div style={style || {}}>
            {errorMessage != null ? <Alert severity="error" variant="filled">{errorMessage}</Alert> : null}
        </div>);
}

export default errorMessage;
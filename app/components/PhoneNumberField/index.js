
/***
 *
 * Phone Number Field
 *
 */


import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import { AsYouType } from 'libphonenumber-js';

export default function ({ input, label, autoFocus, type, disabled, prefix, variant, className, errorStyle, asterisks, meta: { touched, error, warning } }) {

    const classes = Styles();
    const { id, name, value, onChange } = input;

    const phoneType = new AsYouType('US');

    return (
        <div>
            <TextField
                name={name}
                type={type}
                label={asterisks ? <span className={classes.textSize}>{label} <span className={classes.mandatory}>*</span></span> : <span className={classes.textSize} >{label}</span>}
                className={className || classes.fieldColor}
                disabled={disabled}
                fullWidth
                variant={variant || "standard"}
                onChange={(e) => onChange(e.target.value)}
                value={value && phoneType.input(value) || ''}
                InputProps={{
                    startAdornment: prefix ? <InputAdornment position="start">{prefix}</InputAdornment> : null,
                }}
                autoFocus={autoFocus} />
            <div style={errorStyle || {}} className={classes.error}>
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>

    )
}

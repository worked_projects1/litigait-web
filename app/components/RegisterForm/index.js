
/**
 * 
 * Register Form
 * 
 */


import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';

import { Grid, Button, Typography } from '@material-ui/core';

import InputField from 'components/InputField';
import PasswordField from 'components/PasswordField';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from '../Error';

import Styles from './styles';


function RegisterForm({ handleSubmit, submitting, errorMessage, clearCache, destroy, locationState = {}, iframe, actions, dispatch, oauthIntegration }) {

    const classes = Styles();
    const { google, microsoft } = oauthIntegration;

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <>
            <form onSubmit={handleSubmit} className={classes.form} noValidate >
                <Grid container direction="row" justify="center" alignItems="center" style={{ marginBottom: '20px' }}>
                    <a href={process.env.ENV === 'production' ? `https://www.esquiretek.com/` : `https://staging.esquiretek.com/`} >
                        <img src={require('images/icons/new_logo.jpg')} style={{ width: '80%' }} />
                    </a>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Field name="email" label="Email" component={InputField} type="text" required disabled/>
                    </Grid>
                    <Grid item xs={12}>
                        <Field name="practice_name" label="Practice Name" component={InputField} type="text" required autoFocus />
                    </Grid>
                    <Grid item xs={12}>
                        <Field name="password" label="Password" component={PasswordField} type="text" required autoCompleteOff />
                    </Grid>
                    <Grid item xs={12}>
                        <Field name="confirm_password" label="Confirm Password" component={PasswordField} type="text" required />
                    </Grid>
                    <Grid item xs={12} className={classes.div}>                    
                    <Typography variant="body2" className={classes.marginTopMedium}>
                        By registering, you agree to accept our
                    </Typography>
                    <Grid>
                        <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/privacy.html' style={{ color: '#0077c5' }} target='_blank'>Privacy Policy</a> & <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/terms.html' style={{ color: '#0077c5' }} target='_blank'>Terms of Use</a>
                    </Grid>
                </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit} >
                    {submitting && <ButtonSpinner /> || 'Register'}
                </Button>
                {errorMessage && <Error errorMessage={errorMessage} onClose={clearCache} /> || null}
                {/* <Grid className={classes.div}>
                    <Typography variant="body2" className={classes.marginTopMedium}>
                        Already have an account?
                    </Typography>
                    <Grid item xs className={classes.signIn}>
                        <Link
                            to={{
                                pathname: iframe ? 'signin' : '/',
                                state: Object.assign({}, { ...locationState }, { form: 'login' })
                            }}
                            className={classes.linkColor}
                            onClick={clearCache}>
                            Sign In
                        </Link>
                    </Grid>
                </Grid> */}
            </form>
        </>

    )
}

const validate = (values) => {

    const errors = {}

    const sequence = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678)+/ig

    const identical = /^(?!.*(.)\1\1.*).*$/igm

    const requiredFields = ['practice_name', 'password', 'confirm_password'];
    const commonNames = ["123456", "password", "123456789", "12345678", "12345",
        "111111", "1234567", "sunshine", "qwerty", "iloveyou", "princess", "admin", "welcome",
        "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&amp;*", "charlie",
        "aa123456", "donald", "password1", "qwerty123"
    ]

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (values.password && values.password.length < 8) {
        errors.password = 'Password must be at least 8 characters'
    }

    if (values.password && values.password.length >= 8 && !/^(?=.*[\d#?!@$%^&*-]).{8,}$/i.test(values.password)) {
        errors.password = 'Must contain at least one numeric or special character '
    }

    if (values.password && sequence.test(values.password) || !identical.test(values.password)) {
        errors.password = 'Avoid consecutive sequential and identical characters'
    }

    commonNames.forEach(field => {
        if (values.password == field) {
            errors.password = "Password is easily guessable"
        }
    })

    if (values.password && values.confirm_password && values.confirm_password != values.password) {
        errors.confirm_password = 'Password Mismatch'
    }

    return errors
}

export default reduxForm({
    form: 'register',
    validate,
    touchOnChange: true,
})(RegisterForm);
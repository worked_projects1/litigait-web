/**
 *
 * CustomAlert
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ButtonSpinner from 'components/ButtonSpinner';
import AlertDialog from 'components/AlertDialog';
import Button from '@material-ui/core/Button';

function CustomCreateButton(props) {
  const { name, pristine, submitting, classes, confirmMessage } = props;
  const [showAlert, setShowAlert] = useState(false);

  return (
    <>
      <AlertDialog
        openDialog={showAlert}
        description={confirmMessage}
        onConfirmPopUpClose={true}
        btnLabel1="Settings"
        btnLabel2="Cancel"
        page={name} />
        <Button
          type="button"
          disabled={pristine || submitting}
          variant="contained"
          color="primary"
          onClick={() => setShowAlert(true)}
          className={classes.submitBtn}
        >
          {(submitting && <ButtonSpinner />) || 'Create'}
        </Button>
    </>
  );
}

CustomCreateButton.propTypes = {
  name: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  classes: PropTypes.object,
  confirmMessage: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default CustomCreateButton;

/**
 *
 * ManageCases
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import AlertDialog from 'components/AlertDialog';
import Icons from 'components/Icons';
import Styles from './styles';

function ManageCases(props) {

    const classes = Styles();
    const { handleSubmit, data, type, message, actions, dispatch } = props;
    const [openAlert, setOpenAlert] = React.useState(false);

    const handleManageCaseFunc = (evt) => {
        dispatch(actions.manageArchiveCase(true));
        evt.stopPropagation();
        setTimeout(() => {
            handleSubmit(data);
        }, 1000);
    }

    const handleOpenDialog = (evt) => {
        dispatch(actions.manageArchiveCase(true));
        evt.stopPropagation();
        setOpenAlert(true);
    }

    const handleCloseDialog = (evt) => {
        evt.stopPropagation();
        setOpenAlert(false);
        setTimeout(() => {
            dispatch(actions.manageArchiveCase(false));
        }, 1000);
    }

    return <>
        <Icons type={type} style={{ fill: "#2ca01c", cursor: 'pointer' }} className={classes.unArchive} onClick={handleOpenDialog} />
        <AlertDialog
            description={message}
            openDialog={openAlert}
            closeDialog={(evt) => handleCloseDialog(evt)}
            onConfirm={(evt) => handleManageCaseFunc(evt)}
            onConfirmPopUpClose
            btnLabel1='Yes'
            btnLabel2='No' />
    </>
}

ManageCases.propTypes = {
    handleSubmit: PropTypes.func,
    pristine: PropTypes.bool,
    invalid: PropTypes.bool,
    submitting: PropTypes.bool,
    change: PropTypes.func
};

export default ManageCases;

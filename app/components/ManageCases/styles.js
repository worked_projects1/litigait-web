

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    unArchive: {
        width: '30px',
        height: '30px',
    }
}));


export default useStyles;
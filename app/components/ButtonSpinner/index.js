/**
 * 
 * 
 * Button Spinner
 * 
 */

import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';
import Grid from '@material-ui/core/Grid';
import Styles from './styles';

const ButtonSpinner = ({ color, style, className, size }) => {

    const classes = Styles();
    return (<Grid className={`${classes.spinner} ${className}`} style={style}>
        <ClipLoader color={color || '#fff'} width="30" height="30" size={size}/>
    </Grid>)
}


export default ButtonSpinner;

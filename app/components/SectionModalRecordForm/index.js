/**
 * 
 *  Section Modal Record Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { normalize } from 'utils/tools';
import Styles from './styles';
import history from 'utils/history';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function SectionModalRecordForm(props) {
    const { title, message, messageRegular, handleSubmit, submitting, children, fields, metaData, className, style, btnLabel, onOpen, onClose, error, show, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, disableContainer, enableSubmitBtn, disableCancelBtn, progress, footerBtn, confirmPopUpClose, paperClassName, enableScroll, page, details, customNote, section, change } = props;
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                    <form onSubmit={handleSubmit.bind(this)} className={classes.form} noValidate>
                        <Grid container className={classes.header} justify="space-between">
                            <Grid item xs={10}>
                                <Typography component="span" className={classes.title}>{title || ''}</Typography>
                            </Grid>
                            <Grid item xs={12} style={{ textAlign: 'end' }}>
                                {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                            </Grid>
                        </Grid>
                        {customNote ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{customNote || ''}</Typography>
                            </Grid> : null}
                        {message ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{message || ''}</Typography>
                            </Grid> : null}
                        {messageRegular ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.messageRegular}>{messageRegular || ''}</Typography>
                            </Grid> : null}
                        <Grid container className={customNote ? classes.body1 : classes.body}>
                            {details ? 
                                <Grid item xs={4} className={classes.details}>
                                    {details}
                                </Grid> : null}
                            <Grid item xs={details ? 8 : 12} className={enableScroll ? enableScroll : null}>
                                <Grid container>
                                    {(section || []).map((col, index) => {
                                        const column = col && col.columns && col.columns?.filter(_ => _.editRecord);

                                        return <Grid xs={12} key={index}>
                                            <Grid item xs={12} style={col.style || {}}>
                                                <Grid className={classes.sectionTitle}>
                                                    <Typography variant="subtitle1">
                                                        <span className={col?.customTitleStyle ? classes.subTitle : classes.title}>{col.title || ''}</span>
                                                    </Typography>
                                                    {col.optionalLabel ? <Typography variant="subtitle1">
                                                        <span className={classes.optionalLabel}>{col.optionalLabel}</span>
                                                    </Typography>: null}
                                                </Grid>
                                            </Grid>
                                            <Grid container>
                                            {(column || []).map((field, i) => {
                                                const InputComponent = ImplementationFor[field.type];
                                                return <Grid key={i} item xs={12}>
                                                    {field.type !== 'fieldArray' ?
                                                        <Field
                                                            name={field.value}
                                                            label={field.label}
                                                            type="text"
                                                            metaData={metaData}
                                                            component={InputComponent}
                                                            required={field.required}
                                                            normalize={normalize(field)}
                                                            disabled={field.disableOptons && field.disableOptons.edit}
                                                            change={change}
                                                            {...field} /> :
                                                        <FieldArray
                                                            name={field.value}
                                                            label={field.label}
                                                            type="text"
                                                            fieldArray={field.fieldArray}
                                                            metaData={metaData}
                                                            component={InputComponent}
                                                            required={field.required}
                                                            normalize={normalize(field)}
                                                            ImplementationFor={ImplementationFor}
                                                            disabled={field.disableOptons && field.disableOptons.edit}
                                                            change={change}
                                                            {...field} />}
                                                </Grid>
                                            })}
                                            </Grid>
                                        </Grid>
                                    })}
                                </Grid>
                            </Grid>
                            {error ? <Grid item xs={12} className={classes.error}> <Error errorMessage={error} /></Grid> : null}
                        </Grid>
                        {notes ? <Grid >
                            <Typography component="span" className={classes.note}>{notes || ''}</Typography>
                        </Grid> : null}
                        <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                            {footerBtn && React.createElement(footerBtn) || null}
                            {confirmButton ?
                                <AlertDialog
                                    description={confirmMessage}
                                    onConfirm={() => handleSubmit()}
                                    onConfirmPopUpClose={confirmPopUpClose}
                                    btnLabel1='Yes'
                                    btnLabel2='No' >
                                    {(open) => <Button
                                        type="button"
                                        variant="contained"
                                        onClick={open}
                                        disabled={!enableSubmitBtn && (pristine || invalid)}
                                        color="primary"
                                        className={classes.button}>
                                        {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                    </Button>}
                                </AlertDialog> :
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={!enableSubmitBtn && (pristine || invalid)}
                                    className={classes.button}
                                    onClick={!invalid && onSubmitClose ? closeModal : null}>
                                    {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                </Button>}
                            {!disableCancelBtn ? <Button
                                type="button"
                                variant="contained"
                                onClick={closeModal}
                                className={classes.button}>
                                Cancel
                            </Button> : true}
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

SectionModalRecordForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    section: PropTypes.array,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'modalRecord',
    validate,
    enableReinitialize: true,
    touchOnChange: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(SectionModalRecordForm);
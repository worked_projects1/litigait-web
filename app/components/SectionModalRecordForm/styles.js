

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '25%'
    },
    body: {
        marginTop: '10px',
        marginBottom: '15px'
    },
    body1: {
        marginTop: '15px',
        marginBottom: '25px'
    },
    footer: {
        borderTop: '1px solid lightgray'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    title: {
        fontFamily: 'Avenir-Bold',
        fontSize: '19px'
    }, 
    subTitle: {
        fontFamily: 'Avenir-Bold',
        fontSize: '13px'
    }, 
    optionalLabel: {
        fontSize: '13px',
        marginLeft: '2px'
    },
    message: {
        fontFamily: 'Avenir-Bold',
        fontSize: '16px',
        paddingTop: '18px',
    },
    messageRegular: {
        fontSize: '16px',
        paddingTop: '18px',
    },
    closeIcon: {
        cursor: 'pointer'
    },
    messageGrid: {
        paddingTop: '10px'
    },
    error: {
        marginTop: '10px'
    },
    note: {
        fontSize: '16px',
    },
    details: {
        padding: '10px',
        whiteSpace: 'pre-wrap'
    },
    sectionTitle: {
        display: 'flex',
        alignItems: 'center',
    }
}));


export default useStyles;
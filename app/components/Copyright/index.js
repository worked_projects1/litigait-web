

/**
 * 
 * Copyright
 * 
 */

import React from 'react';

import Typography from '@material-ui/core/Typography';

class Copyright extends React.Component {
  render() {
    const { textColor } = this.props;
    
    return (<Typography variant="body2" align="center" style={{ color: textColor || '#000' }}>
        {'© '}  {new Date().getFullYear()}
        {' EsquireTek. All Rights Reserved'}

        {'.'}
      </Typography>);
  }
}


export default Copyright;
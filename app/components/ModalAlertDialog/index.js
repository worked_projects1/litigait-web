/**
 * 
 *  Modal Alert Dialog
 * 
 */

import React, { useState } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ModalAlertDialog(props) {
    const { title, submitting, children, className, style, btnLabel, onOpen, onClose, show, onSubmitClose, footerStyle, disableContainer, disableCancelBtn, progress, paperClassName, disableCancelButton, disableSubmitBtn, footerBtnFunc1, footerBtnFunc, description, enableScroll } = props;
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                    <Grid container className={classes.header} justify="space-between">
                        <Grid item xs={10}>
                            <Typography component="span" className={classes.title}>{title || ''}</Typography>
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                            {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                        </Grid>
                    </Grid>
                    <Grid container className={enableScroll ? enableScroll : null} >
                        {typeof (description) === 'function' ? React.createElement(description, props) : description}
                    </Grid>
                    {(!disableSubmitBtn) ? <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                        {footerBtnFunc && typeof footerBtnFunc === 'function' && footerBtnFunc(setModalOpen) || null}
                        {footerBtnFunc1 && typeof footerBtnFunc1 === 'function' && footerBtnFunc1(setModalOpen) || null}
                        {!disableSubmitBtn ? <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            onClick={onSubmitClose ? closeModal : null}>
                            {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                        </Button> : true}
                        {(!disableCancelBtn && !disableCancelButton) ? <Button
                            type="button"
                            variant="contained"
                            onClick={closeModal}
                            className={classes.button}>
                            Cancel
                        </Button> : true}
                    </Grid> : null}
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

export default ModalAlertDialog;
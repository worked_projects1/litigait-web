import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        paddingLeft: '10px'
    },
    signupBtn: {
        backgroundColor: '#2ca01c',
        marginTop: '15px',
        boxShadow: 'none',
        borderRadius: 'initial',
        textTransform: 'none',
        color: '#ffff',
        minWidth: '100px',
        fontFamily: 'ClearSans-Regular',
        "&:hover": {
            backgroundColor: '#2ca01c',
        }
    },
    linkColor: {
        color: '#0077c5',
        textDecoration: 'none',
        paddingLeft: '4px'
    },
}));


export default useStyles;

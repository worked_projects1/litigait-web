import React, { useEffect } from 'react';
import { reduxForm } from 'redux-form';
import { Grid, Button, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from './styles';
import ButtonSpinner from 'components/ButtonSpinner';
import Success from 'components/Success';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function LoginFailureForm(props) {

    const { handleSubmit, errorMessage, submitting, destroy, clearCache, locationState } = props;
    const classes = useStyles();
    const { identifier } = locationState || {};

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >

            <Grid container spacing={2} className={classes.container} >
                <Grid item xs={12} >
                    <img src={require(`images/home/logo1.png`)} style={{ width: "273px" }} />

                    <Grid container style={{ marginTop: "50px" }}>

                        <Grid item xs={12} >
                            <Grid style={{ marginTop: "50px" }}>
                                <Typography variant="h4" >
                                    Too Many Attempts
                                </Typography>
                                <Typography variant='body2'><br />
                                Your account is locked due to too many attempts. We sent you a reactivation link to your email address {identifier}
                                </Typography>
                                <Button type="submit" className={classes.signupBtn}>
                                    {submitting ? <ButtonSpinner /> : 'Resend'}
                                </Button>
                            </Grid>
                            <Grid item xs style={{ marginTop: "15px" }}>
                                <Typography variant='body2'>
                                    Not you?
                                    <Link
                                        to={{
                                            pathname: '/',
                                            state: Object.assign({}, { ...locationState }, { form: 'login' })
                                        }}
                                        className={classes.linkColor}  >
                                        Click here
                                    </Link>
                                </Typography>
                                {errorMessage && <Success successMessage={"Email Sent."} style={{ marginTop: '35px' }} onClose={clearCache} /> || null}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

        </form>
    )
}

export default reduxForm({
    form: 'loginFailure',
    touchOnChange: true,
})(LoginFailureForm);

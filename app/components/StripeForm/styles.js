

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    marginTop: '40px',
    backgroundColor: '#2ca01c',
    textTransform: 'none',
    "&:hover": {
      backgroundColor: '#2ca01c',
    }
  },
  viewPlanDetails: {
    display: 'flex',
    justifyContent: 'flex-end',
    position: 'relative',
    top: '15px',
    zIndex: '999',
    '@global': {
      'span': {
        cursor: 'pointer',
        fontSize: '10px',
        color: '#2ca01c'
      }
    }
  }
}));


export default useStyles;
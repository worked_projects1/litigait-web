
/**
 * 
 * Stripe Form
 * 
 */


import React, { useEffect, useState } from 'react';
import { Field, reduxForm } from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';

import { Grid, Button } from '@material-ui/core';

import InputField from 'components/InputField';
import SelectField from 'components/SelectField';
import ButtonSpinner from 'components/ButtonSpinner';
import StripeField from 'components/StripeField';
import CheckboxField from 'components/CheckboxField';
import AlertDialog from 'components/AlertDialog';
import Error from '../Error';
import Styles from './styles';
import { planConfirmationMessage } from 'utils/tools'


const SubmitButton = ({ classes, loading, openAlertDialog, submitting, handleSubmitForm, cardDetails, oneTimeActivationFee, invalid, formValues, form, confirmationMessage }) => {

    return <AlertDialog
        description={confirmationMessage}
        onConfirm={handleSubmitForm}
        btnLabel1='Yes'
        btnLabel2='No'
        onConfirmPopUpClose >
        {(openDialog) => {
            return <Button type="button" fullWidth variant="contained" color="primary" className={classes.submit} onClick={() => openAlertDialog(Object.assign({}, { dialog: openDialog, submitRecord: formValues, form, cardDetails, oneTimeActivationFee, handleSubmitForm }))} disabled={invalid}>
                {(submitting || loading) && <ButtonSpinner /> || 'Submit'}
            </Button>
        }}
    </AlertDialog>
};


function StripeForm({ handleSubmit, submitting, errorMessage, clearCache, destroy, invalid, loading, metaData, locationState, formValues, discountDetail, openAlertDialog, form }) {

    const classes = Styles();
    const [planId, setPlans] = useState(false);
    const oneTimeActivationFee = locationState && locationState.user && locationState.user.one_time_activation_fee || false;
    const cardDetails = formValues && formValues.name && formValues.plan_id && formValues.cardDetails && formValues.cardDetails['complete'] && formValues.terms || false;
    const cardDetailsAvailable = formValues && formValues.cardDetails && formValues.cardDetails['complete'] || false;

    const currentPlan = metaData && metaData['plans'] && metaData['plans'].length > 0 && planId && metaData['plans'].find(_ => _.plan_id === planId) || false;

    useEffect(() => {
        return () => destroy();
    }, []);

    const handleSubmitForm = () => {
        handleSubmit();
    }

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Grid container direction="row" justify="center" alignItems="center" style={{ marginBottom: '20px' }}>
                <img src={require('images/icons/new_logo.jpg')} style={{ width: '80%' }} />
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Field name="plan_id" label="Choose Plan" component={SelectField} metaData={metaData} autoFocus options="plans" onChange={(val) => setPlans(val)} required >
                        <Grid className={classes.viewPlanDetails}>
                            <span onClick={() => { window.open('https://www.esquiretek.com/pricing/', '_blank') }}>View Plan Details</span>
                        </Grid>
                    </Field>
                </Grid>
                {planId && planId !== 'free_trial' ? <React.Fragment>
                    <Grid item xs={12}>
                        <Field name="name" label="Card Holder's Name" component={InputField} required />
                    </Grid>
                    <Grid item xs={12}>
                        <Field name="cardDetails" label="Card Details" component={StripeField} required showRequired />
                    </Grid>
                    {cardDetailsAvailable ? <Grid item xs={12}>
                        <Field name="discount_code" label="Promo Code (If any)" component={InputField} />
                    </Grid> : null}
                    {cardDetailsAvailable ? <Grid item xs={12}>
                        <Field name="terms" label={"I have read and accepted <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'>EsquireTek Terms of Use for Subscription Plans</a>"} styles={{ color: "#2ca01c" }} component={CheckboxField} type="checkbox" required />
                    </Grid> : null}
                </React.Fragment> : null}
            </Grid>
            <SubmitButton
                classes={classes}
                handleSubmitForm={handleSubmitForm}
                invalid={invalid}
                submitting={submitting}
                loading={loading}
                metaData={metaData}
                locationState={locationState}
                formValues={formValues}
                cardDetails={cardDetails}
                currentPlan={currentPlan}
                oneTimeActivationFee={oneTimeActivationFee}
                discountDetail={discountDetail}
                openAlertDialog={openAlertDialog}
                form={form}
                handleSubmit={handleSubmit}
                confirmationMessage={planConfirmationMessage(currentPlan, discountDetail, oneTimeActivationFee, formValues, 'responding', 'homePage')}
            />
            {errorMessage && <Error errorMessage={errorMessage} onClose={clearCache} /> || null}
        </form>
    )
}

const validate = (values) => {

    let errors = {}

    const requiredFields = ['name', 'cardDetails', 'plan_id', 'terms'];

    const cardDetails = values && values['cardDetails'] && values['cardDetails']['complete'] || false;

    if (values && values['name'] || (cardDetails) || values['terms']) {
        requiredFields.forEach(field => {
            if (!values[field]) {
                errors[field] = 'Required';
            } else if (field === 'cardDetails' && values[field] && !values[field]['complete']) {
                errors[field] = 'Required';
            } else if (field === 'terms' && !values['terms']) {
                errors[field] = 'Required';
            }
        })
    }

    if (values && !values['name'] && !cardDetails) {
        errors = {};
    }

    return errors
}

function mapStateToProps({ form }, props) {
    const formValues = form[props.form] && form[props.form].values || false;

    return {
        formValues
    }
}


export default compose(
    connect(mapStateToProps),
    reduxForm({
        form: 'stripe',
        validate,
        touchOnChange: true,
    })
)(StripeForm);

/**
 * 
 * Header Links
 * 
 */


/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { List, ListItem, ListItemText } from "@material-ui/core";

// @material-ui/icons
import { StyledListItem } from './styles';
import styles from "./styles";

const useStyles = makeStyles(styles);

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function HeaderLinks(props) {
  
  const { locationState = {} } = props;
  const { page } = locationState;
  const classes = useStyles();

  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Link
          to={'/'}
          className={classes.link}>
          <StyledListItem
            button
            key={'Overview'}>
            <ListItemText className={classes.listItemText} primary={'Overview'} />
          </StyledListItem>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link
          to={{ 
            pathname: '/', 
            state: Object.assign({}, {...locationState}, { page: 'contact' }) }}
          className={classes.link}>
          <StyledListItem
            button
            key={'Contact'}>
            <ListItemText className={page === 'contact' ? classes.selectedListItem : classes.listItemText} primary={'Contact'} />
          </StyledListItem>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link
          to={{
            pathname: '/',
            state: Object.assign({}, {...locationState}, { form: 'login' })
          }}
          className={classes.link}>
          <StyledListItem
            button
            key={'Login'}>
            <ListItemText className={classes.listItemText} primary={'Login'} />
          </StyledListItem>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link
          to={{
            pathname: '/',
            state: Object.assign({}, {...locationState}, { form: 'register' })
          }}
          className={classes.link}>
          <StyledListItem
            button
            key={'Sign Up'}
            selected={true}>
            <ListItemText className={classes.listItemText} primary={'Sign Up'} />
          </StyledListItem>
        </Link>
      </ListItem>
    </List>
  );
}

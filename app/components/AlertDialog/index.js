/**
 * 
 * Alert Dialog
 * 
 */

import React, { useState } from 'react';
import { Grid, Typography, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import Styles from './styles';
import history from 'utils/history';
import CloseIcon from '@material-ui/icons/Close';

export default function AlertDialog(props) {

  const { title, children, btnLabel1, btnLabel2, description, onConfirm, onConfirmPopUpClose, openDialog, closeDialog, heading, footerBtn, page, alertOnClose, disableCloseModal, closeBtn, customDialog } = props;

  const classes = Styles();
  const [open, setOpen] = useState(false);

  const close = (e) => {
    setOpen(false);
    if (closeDialog) {
      closeDialog(e);
    }

    if (page === 'clients') {
      history.push({ pathname: '/clientCategories/clients', state: Object.assign({}, { ...history.location.state }, { respondCase: false }) })
    }
  }

  const confirm = (e) => {
    if (onConfirm) {
      onConfirm(e);
    }

    if (onConfirmPopUpClose) {
      close(e);
    }

    if (['users.create', 'users'].includes(page)) {
      history.push({ pathname: '/settings', state: Object.assign({}, { ...history.location.state }) })
    }
  }


  return (<div>
    {children && children(() => setOpen(!open))}
    <Dialog
      open={open || openDialog || false}
      onClose={!disableCloseModal ? close : null}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description">
      {
        customDialog ? <>
          {closeBtn ? <Grid item xs={12} style={{ textAlign: 'end', padding: '5px' }}>
            <CloseIcon onClick={close} className={classes.closeIcon} />
          </Grid> : null}
          <DialogContent style={{ minWidth: '420px'}}>
            <DialogContentText id="alert-dialog-description" className={classes.description}>
              {description && typeof description === 'function' ? React.createElement(description, Object.assign({}, { ...props, classes })) : <span dangerouslySetInnerHTML={{ __html: description }} />}
            </DialogContentText>
          </DialogContent>
        </> : <>
          <DialogTitle id="alert-dialog-title" >{title}</DialogTitle>
          {typeof heading === 'function' && heading() || heading && <Grid className={classes.gridHeader}>
            <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
          </Grid> || null}
          <DialogContent>
            <DialogContentText id="alert-dialog-description" className={classes.description}>
              {description && typeof description === 'function' ? React.createElement(description, Object.assign({}, { ...props, classes })) : <span dangerouslySetInnerHTML={{ __html: description }} />}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {btnLabel2 ? <Button onClick={alertOnClose && typeof alertOnClose === 'function' ? alertOnClose : close} color="primary" className={classes.button}>
              {btnLabel2}
            </Button> : null}
            {footerBtn && typeof footerBtn === 'function' ? React.createElement(footerBtn) : btnLabel1 ? <Button onClick={confirm} color="primary" autoFocus className={classes.button}>
              {btnLabel1}
            </Button> : null}
          </DialogActions>
        </>
      }
    </Dialog>
  </div>);
}
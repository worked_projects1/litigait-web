import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    button: {
        color: '#2DA01D',
        fontWeight: 'bold',
        fontFamily: 'Avenir-Bold',
    },
    description: {
        color: '#000000',
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    checkBoxLable: {
        '@global': {
            '.MuiFormControlLabel-label': {
                marginTop: '4px'
            }
        }
    },
    closeIcon: {
        cursor: 'pointer'
    },
}));


export default useStyles;
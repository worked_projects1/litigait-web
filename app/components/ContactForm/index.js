
/**
 * 
 * Contact Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';

import { Grid, Button } from '@material-ui/core';
import Error from '../Error';
import Success from '../Success';

import InputField from 'components/InputField';
import ButtonSpinner from 'components/ButtonSpinner';

import Styles from './styles';


/**
 * 
 * @param {object} props 
 * @returns 
 */
function ContactForm(props) {
    const { handleSubmit, submitting, success, error, destroy, clearCache } = props;
    const classes = Styles();

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Grid container justify="center">
                <Grid item xs={12} className={classes.questions}>QUESTIONS?</Grid>
                <Grid item xs={12} className={classes.contact}><h2>CONTACT US</h2></Grid>
            </Grid>
            <Grid container spacing={2} alignItems="center" direction="column">
                <Grid item xs={12}>
                    <Field name="first_name" label="First Name*" component={InputField} type="text" variant="outlined" className={classes.inputField} />
                </Grid>
                <Grid item xs={12}>
                    <Field name="last_name" label="Last Name*" component={InputField} type="text" variant="outlined" className={classes.inputField} />
                </Grid>
                <Grid item xs={12}>
                    <Field name="email" label="Email Address*" component={InputField} type="text" variant="outlined" className={classes.inputField} />
                </Grid>
                <Grid item xs={12}>
                    <Field name="message" label="Message" component={InputField} type="text" variant="outlined" className={classes.textareaField} multiline={true} rows={6} rowsMax={18} />
                </Grid>
                <Grid item xs={12}>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit} >
                        {submitting ?
                            <ButtonSpinner color="#e95d0c" /> :
                            'Send Message'}
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    {error && <Error errorMessage={error} style={{ width: '350px', marginBottom: '15px' }} onClose={() => props.change('_error', null)} /> || success && success.contactForm && <Success successMessage={success.contactForm} style={{ width: '350px', marginBottom: '15px' }} onClose={clearCache} /> || null}
                </Grid>
            </Grid>
            <Grid container justify="center">
                <Grid item xs={12} className={classes.info}>1-844-321-ETEK</Grid>
                <Grid item xs={12} className={classes.info}>support@esquiretek.com</Grid>
            </Grid>
        </form>
    )
}


const validate = (values) => {

    const errors = {}

    const requiredFields = ['first_name', 'last_name', 'email'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)) {
        errors.email = 'Invalid Email'
    }

    return errors
}


export default reduxForm({
    form: 'contact',
    validate,
    touchOnChange: true,
})(ContactForm);
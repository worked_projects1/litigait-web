

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(4),
    },
    submit: {
        width: '350px',
        backgroundColor: 'transparent !important',
        color: '#e95d0c !important',
        boxShadow: 'none',
        border: '2px solid #e95d0c',
        fontFamily: 'clearSans-Regular',
        letterSpacing: '2px',
        fontSize: '16px',
        textTransform: 'none',
        marginBottom: '15px',
        "&:hover": {
            //you want this to be the same as the backgroundColor above
            backgroundColor: '#2ca01c',
            boxShadow: 'none',
        }
    },
    questions: {
        color: '#e95d0c',
        fontFamily: 'ClearSans-Regular',
        letterSpacing: '1px',
        textAlign: 'center',
        fontSize: '18px'
    },
    contact: {
        color: '#848485',
        letterSpacing: '5px',
        textAlign: 'center',
        fontFamily: 'ClearSans-Regular',
        marginBottom: '10px',
        fontSize: '17px',
        "& > h2": {
            marginTop: '5px',
            fontWeight: '500'
        }
    },
    inputField: {
        background: 'transparent',
        width: '350px',
        fontFamily: 'ClearSans-Light !important',
        '& .MuiInputLabel-outlined > span': {
            fontFamily: 'ClearSans-Light',
            color: '#0a0a0a'
        },
        '& .MuiOutlinedInput-root': {
            height: '45px'
        },
        '& .MuiInputLabel-outlined': {
            marginTop: '-4px'
        },
        '& .Mui-focused > span': {
            marginLeft: '14px',
            fontSize: '16px'
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: '#848485'
        },
        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: '#848485'
        }
    },
    textareaField: {
        background: 'transparent',
        width: '350px',
        fontFamily: 'ClearSans-Light !important',
        '& .MuiInputLabel-outlined > span': {
            fontFamily: 'ClearSans-Light',
            color: '#0a0a0a'
        },
        '& .Mui-focused > span': {
            marginLeft: '6px',
            fontSize: '16px'
        },
        '& .MuiInputLabel-outlined': {
            marginTop: '-4px'
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: '#848485'
        },
        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: '#848485'
        }
    },
    info: {
        color: '#e95d0c',
        textAlign: 'center',
        fontFamily: 'clearSans-Light',
        paddingBottom: '15px'
    }
}));


export default useStyles;
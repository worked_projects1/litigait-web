
/***
 *
 * Component Field
 *
 */


import React from 'react';
import Styles from './styles';


export default function ({ input, ...props }) {
    const classes = Styles();
    const Component = props.render;
    return Component && <Component input={input} {...props} classes={classes} /> || null
}



import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  fieldColor: {
    maxWidth: '60px',
    '& :after': {
      borderBottomColor: '#2ca01c',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
    doubleArrow: {
    color: '#2ca01c',
    cursor: 'pointer',
    width: "30px",
    height: "30px",
    '&:hover': {
      backgroundColor: '#e8e8e8',
    }
  }
}));


export default useStyles;
/**
 * 
 *  Modal Record Form
 * 
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Paper, Grid, Typography, Button, Popover } from '@material-ui/core';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function PopOverForm(props) {
    const { title, message, messageRegular, handleSubmit, submitting, children, fields, metaData, className, style, btnLabel, onClose, error, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, disableContainer, enableSubmitBtn, disableCancelBtn, progress, disableHeader, disableFooter, paperStyle } = props;
    const classes = Styles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClose = () => {
        setAnchorEl(false);
        if (onClose)
            onClose();
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children((event) => setAnchorEl(event.currentTarget))}
        <Popover
            id={anchorEl ? 'simple-popover' : undefined}
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}>
            <Paper className={classes.paper} style={paperStyle || null}>
                <form onSubmit={handleSubmit.bind(this)} className={classes.form} noValidate>
                    {!disableHeader ? <Grid container className={classes.header} justify="space-between">
                        <Typography component="span" className={classes.title}>{title || ''}</Typography>
                        {!disableCancelBtn ? <CloseIcon onClick={handleClose} className={classes.closeIcon} /> : null}
                    </Grid> : null}
                    {message ?
                        <Grid className={classes.messageGrid}>
                            <Typography component="span" className={classes.message}>{message || ''}</Typography>
                        </Grid> : null}
                    {messageRegular ?
                        <Grid className={classes.messageGrid}>
                            <Typography component="span" className={classes.messageRegular}>{messageRegular || ''}</Typography>
                        </Grid> : null}
                    <Grid container className={classes.body}>
                        <Grid item xs={12}>
                            <Grid container spacing={3}>
                                {(fields || []).map((field, index) => {
                                    const InputComponent = ImplementationFor[field.type];
                                    return <Grid key={index} item xs={12}>
                                        <Field
                                            name={field.value}
                                            label={field.label}
                                            type="text"
                                            metaData={metaData}
                                            component={InputComponent}
                                            required={field.required}
                                            disabled={field.disableOptons && field.disableOptons.edit}
                                            {...field} />
                                    </Grid>
                                })}
                            </Grid>
                        </Grid>
                        {error ? <Grid item xs={12} className={classes.error}> <Error errorMessage={error} /></Grid> : null}
                    </Grid>
                    {notes ? <Grid >
                        <Typography component="span" className={classes.note}>{notes || ''}</Typography>
                    </Grid> : null}
                    {!disableFooter ? <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                        {confirmButton ?
                            <AlertDialog
                                description={confirmMessage}
                                onConfirm={() => handleSubmit()}
                                btnLabel1='Yes'
                                btnLabel2='No' >
                                {(open) => <Button
                                    type="button"
                                    variant="contained"
                                    onClick={open}
                                    disabled={!enableSubmitBtn && (pristine || invalid)}
                                    color="primary"
                                    className={classes.button}>
                                    {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                </Button>}
                            </AlertDialog> :
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                disabled={!enableSubmitBtn && (pristine || invalid)}
                                className={classes.button}
                                onClick={!invalid && onSubmitClose ? handleClose : null}>
                                {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                            </Button>}
                        {!disableCancelBtn ? <Button
                            type="button"
                            variant="contained"
                            onClick={handleClose}
                            className={classes.button}>
                            Cancel
                             </Button> : true}
                    </Grid> : null}
                </form>
            </Paper>
        </Popover>

    </Grid>
}

PopOverForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'popOverRecordForm',
    validate,
    enableReinitialize: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(PopOverForm);
/**
 *
 * FilevineSessionForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import schema from 'routes/schema';
import ModalForm from 'components/ModalRecordForm';
import moment from 'moment';
import { AsYouType } from 'libphonenumber-js';

function FilevineSessionForm(props) {
    const { record, actions, dispatch, formRecord = {}, filevineSessionForm, createClientCase } = props;
    const filevineForm = schema && schema().filevineSecret().columns;

    const handleFilevineSession = (data, dispatch, { form }) => {
        let formSubmitRecord = Object.assign({}, { ...formRecord });
        if (formSubmitRecord.phone) {
            const phoneType = new AsYouType('US');
            phoneType.input(formSubmitRecord.phone);
            const phoneVal = phoneType.getNumber().nationalNumber;
            formSubmitRecord.phone = phoneVal || formSubmitRecord.phone;
        }
        formSubmitRecord.createdAt = new Date(moment(formSubmitRecord.createdAt).format('MM/DD/YYYY'));
        formSubmitRecord.dob = formSubmitRecord.dob && new Date(moment(formSubmitRecord.dob).format('MM/DD/YYYY'));
        formSubmitRecord.dob = formSubmitRecord.dob == '' ? null : formSubmitRecord.dob;

        createClientCase ? dispatch(actions.filevineSessionCommonUpdate(data, `client_case_${record.id}`, formSubmitRecord)) : dispatch(actions.createNewFilevineSession(data, form, formSubmitRecord, `editRecord_${record.id}`));
    }

    return <Grid item>
        <ModalForm
            initialValues={Object.assign({}, { is_store_filevine_secret: true }) || {}}
            show={filevineSessionForm}
            title={'Filevine Integration'}
            fields={filevineForm && filevineForm.filter(_ => _.editRecord && !_.disableRecord)}
            form={`filevineForm`}
            onSubmit={handleFilevineSession.bind(this)}
            onClose={() => dispatch(actions.setFilevineSessionForm(false))}
            btnLabel="Submit"
            instructionNote={<>To get API Key and Secret, you need to follow the steps using this <a href="https://support.filevine.com/hc/en-us/articles/360032298611-Set-Up-API-v2" target="_blank" style={{ textDecoration: 'underline' }}>link</a>. Once you generate the keys, you can find the API Base Url from Orgs menu in the filevine portal.</>}
            onSubmitClose={true}
            style={{ paddingRight: '0px' }} />
    </Grid>;
}

FilevineSessionForm.propTypes = {
    pristine: PropTypes.bool,
    invalid: PropTypes.bool,
    submitting: PropTypes.bool,
    change: PropTypes.func
};

export default FilevineSessionForm;

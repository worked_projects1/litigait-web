/**
 * 
 * Success 
 * 
 */

import React, { useEffect } from 'react';
import Alert from '@material-ui/lab/Alert';

const errorMessage = ({ style, successMessage, onClose }) => {

    useEffect(() => {
        if (onClose) {
            setTimeout(onClose, 4000);
        }
    }, []);

    return (
        <div style={style || {}}>
            {successMessage != null ? <Alert severity="success" variant="filled">{successMessage}</Alert> : null}
        </div>

    );
}

export default errorMessage;
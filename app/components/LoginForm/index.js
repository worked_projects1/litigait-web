/**
 * 
 * Login Form
 * 
 */


import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import SVG from 'react-inlinesvg';
import { Grid, Button, Typography } from '@material-ui/core';
import Error from '../Error';

import InputField from 'components/InputField';
import PasswordField from 'components/PasswordField';
import ButtonSpinner from 'components/ButtonSpinner';

import Styles from './styles';
import IntegrationButtons from 'components/IntegrationButtons';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function LoginForm(props) {
    const { handleSubmit, pristine, submitting, errorMessage, clearCache, destroy, locationState = {}, iframe, actions, dispatch, oauthIntegration, location } = props;
    const classes = Styles();
    const { google, microsoft } = oauthIntegration;
    const pathname = location && location.pathname || '/';
    const integrationParams = pathname && pathname.includes('litify') ? pathname : false;
    const signupUrl = iframe ? (integrationParams && `/signup${integrationParams}` || `/signup`) : (integrationParams && `${integrationParams}` || '/');

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <Grid>
            <form onSubmit={handleSubmit} className={classes.form} noValidate >
                <Grid container direction='row' justify='center' alignItems='center' style={{ marginBottom: '20px' }}>
                    <a href={process.env.ENV === 'production' ? `https://www.esquiretek.com/` : `https://staging.esquiretek.com/`} >
                        <img src={require('images/icons/new_logo.jpg')} style={{ width: '80%' }} />
                    </a>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Field name='email' label='Email' component={InputField} type='text' required autoFocus />
                    </Grid>
                    <Grid item xs={12}>
                        <Field name='password' label='Password' component={PasswordField} type='text' />
                    </Grid>
                </Grid>
                <Button
                    type='submit'
                    fullWidth
                    variant='contained'
                    color='primary'
                    className={classes.submit} >
                    {submitting ?
                        <ButtonSpinner /> :
                        <Grid>
                            <SVG src={require('images/login/lock.svg')} className={classes.lockIcon} />
                            Sign In
                        </Grid>}
                </Button>
                {errorMessage && <Error errorMessage={errorMessage} onClose={clearCache} /> || null}
                <Grid className={classes.div}>
                    <Grid item xs>
                        <Link
                            to={{
                                pathname: iframe ? '/forgot' : '/',
                                state: Object.assign({}, { ...locationState }, { form: 'forgot' })
                            }}
                            className={classes.linkColor}>
                            Forgot your Password?
                        </Link>
                    </Grid>
                    <Typography variant='body2' className={classes.marginTopMedium}>
                        New to EsquireTek
                    </Typography>
                    <Grid item xs>
                        <Link
                            to={{
                                pathname: signupUrl,
                                state: Object.assign({}, { ...locationState }, { form: 'signup' })
                            }}
                            className={classes.linkColor}
                            onClick={clearCache}>
                            Create account
                        </Link>
                    </Grid>
                </Grid>
            </form>
            <Grid className='divider'>
                <span>OR</span>
            </Grid>
            <IntegrationButtons 
                googleBtnLabel={'Sign-in with Google'}
                microsoftBtnLabel={'Sign-in with Microsoft'}
                googleAuth={google}
                microsoftAuth={microsoft} />
        </Grid>
        
    )
}


const validate = (values) => {

    const errors = {}

    const requiredFields = ['email', 'password'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
    ) {
        errors.email = 'Invalid Email'
    }

    if (values.password && values.password.length < 4) {
        errors.password = 'Password must be at least 4 characters'
    }

    return errors
}


export default reduxForm({
    form: 'login',
    validate,
    touchOnChange: true,
})(LoginForm);
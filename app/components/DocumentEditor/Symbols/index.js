/**
 * 
 * Symbol
 * 
 */

import React, { useState } from "react";
import Styles from "./styles";
import { Paper, Grid, Popover, Table, TableBody, TableRow, TableCell } from '@material-ui/core';

const InsertSymbol = (props) => {
    const { open, children, paperStyle, disableContainer, style, destroy, onSubmitClose, handleSymbol } = props;
    const classes = Styles();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClose = () => {
        setAnchorEl(false);
        if (onSubmitClose)
            onSubmitClose();
    }

    const data = ["¶", "§", "©", "TM", "®", "*", "‡", "†", "SM", "№", "℗", "^", "@", "&", "⚖", "🄯", "ᵩ", "ₚ", "🜃"];
    const numCellsPerRow = 5;
    const numRows = Math.ceil(data.length / numCellsPerRow);

    const tableRows = [];
    for (let i = 0; i < numRows; i++) {
        const cells = [];
        for (let j = 0; j < numCellsPerRow; j++) {
            const dataIndex = i * numCellsPerRow + j;
            if (dataIndex < data.length) {
                cells.push(
                    <TableCell className={classes.cell} key={`cell-${dataIndex}`} onClick={() => handleSymbol(data[dataIndex])}>
                        {data[dataIndex]}
                    </TableCell>
                );
            }
        }
        tableRows.push(
            <TableRow key={`row-${i}`}>
                {cells}
            </TableRow>
        );
    }

    return (
        <Grid container={disableContainer ? false : true} className={classes.form}>
            {children && children((event) => setAnchorEl(event.currentTarget))}
            <Popover
                id={anchorEl ? 'simple-popover' : undefined}
                open={open || anchorEl}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Paper className={classes.paper} style={paperStyle || null}>
                    <Table>
                        <TableBody className={classes.table}>
                            {tableRows}
                        </TableBody>
                    </Table>
                </Paper>
            </Popover>

        </Grid>
    )
}

export default React.memo(InsertSymbol);

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    form: {
        display: 'inline',
        textAlign: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '10px',
        minWidth: '40%',
        outline: 'none',
        width: '100%',
        minWidth: '230px'
    },
    table: {
        border: "1px solid #ccc",
        overflowY: "scroll"
    },
    cell: {
        borderBottom: "1px solid #ccc",
        borderRight: "1px solid #ccc",
        cursor: "pointer",
        padding: "5px",
        textAlign: "center"
    }
}))

export default useStyles;
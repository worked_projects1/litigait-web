

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'auto'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '15px 20px',
        outline: 'none',
        width: '85%',
        [theme.breakpoints.down('sm')]: {
            width: '90%'
        }
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    footer: {
        paddingTop: '5px',
        borderTop: '1px solid lightgray'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize'
    },
    buttonCancel: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    title: {
        fontFamily: 'Avenir-Bold',
        fontSize: '22px',
    },
    messageGrid: {
        paddingTop: '5px',
        paddingBottom: '10px'
    },
    messageNote: {
        fontSize: '14px'
    },
    mailServeBtn: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icons: {
        marginTop: '10px',
        height: '35px',
        width: '35px',
        cursor: 'pointer',
        color: '#2ca01c'
    },
    symbol: {
        position: 'relative',
        float: "right",
        right: "60px",
        zIndex: "100",
        height: 0,
        [theme.breakpoints.down("sm")]: {
            top: "20px",
            right: "60px"
        },
        [theme.breakpoints.down("md")]: {
            top: "22px",
            right: "60px"
        },
    },
    symbolBtn: {
        textTransform: "none",
        color: "#000000DE",
        fontSize: "11px",
        padding: "12px",
        borderRight: "1px solid #ccc",
        borderLeft: "1px solid #ccc",
        cursor: "pointer",
        [theme.breakpoints.down("sm")]: {
            padding: 0,
            border: 'none'
        },
        [theme.breakpoints.down("md")]: {
            padding: 0,
            border: 'none'
        }
    },
    omegaIcon: {
        color: "rgba(0, 0, 0, 0.54)",
        fontSize: "14px"
    },
    prefixIcon: {
        backgroundImage: "𝝮",
    },
    list: {
        display: 'flex',
        justifyContent: 'flex-start',
        paddingBottom: '8px',
        borderBottom: '1px solid #eee',
    },
    date: {
        fontFamily: 'Roboto',
        fontSize: '14px',
        fontWeight: 400,
        color: '#000'
    },
    currentVersion: {
        display: 'flex',
        fontSize: '12px',
        fontStyle: 'italic'
    },
    userDetails: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    groupedUserDetails: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    userName: {
        fontFamily: 'Roboto,RobotoDraft,Helvetica,Arial,sans-serif',
        fontSize: '13px',
        color: '#3c4043',
        lineHeight: '16px'
    },
    responseType: {
        fontFamily: 'Roboto,RobotoDraft,Helvetica,Arial,sans-serif',
        fontSize: '13px',
        lineHeight: '16px'
    },
    selected: {
        backgroundColor: '#d8f3fd',
    },
    selectedText: {
        color: '#1a73e8',
        fontWeight: '700'
    },
    responseList: {
        maxHeight: '460px',
        overflowX: 'hidden',
        '& p': {
            margin: '0 !important'
        },
        "&::-webkit-scrollbar": {
            width: 5,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },
    btnContainer: {
        paddingLeft: '70px',
        paddingTop: '10px',
        
    },
    listContainer: {
        width: 250
    },
    discoveryLink: {
        cursor: 'pointer',
        textDecoration: 'underline',
        color: '#2ca01c',
        paddingTop: '5px',
        paddingBottom: '10px'
    },
    spinner: {
        padding: '20px',
        marginTop: '200px',
        marginBottom: '200px',
    }
}));


export default useStyles;
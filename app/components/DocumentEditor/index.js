/**
 * 
 * 
 * Document Editor
 * 
 */

import React, { useState, useEffect } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import Styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import { DocumentEditorContainerComponent, Toolbar } from '@syncfusion/ej2-react-documenteditor';
import { registerLicense } from "@syncfusion/ej2-base";
import ButtonSpinner from 'components/ButtonSpinner';
import ClipLoader from 'react-spinners/ClipLoader';
import Icons from 'components/Icons';
import InsertSymbol from './Symbols';
import '../../../src/editor.css';
import moment from 'moment';
import { Tooltip } from "@material-ui/core";
import Drawer from '@material-ui/core/Drawer';
import ListIcon from '@material-ui/icons/List';
import Spinner from 'components/Spinner';

{/* License */ }
registerLicense("Ngo9BigBOggjHTQxAR8/V1NAaF1cWGhIfEx1RHxQdld5ZFRHallYTnNWUj0eQnxTdEFjW35YcHVQRGRcV0xyVw==");

DocumentEditorContainerComponent.Inject(Toolbar);


function DocumentEditor({ title, children, className, show, btnLabel, onClose, infoNote, handleDownloadBtn, handleSubmitBtn, record, disableCloseModal, progress, showSaveBtn, discoveryHistory, handleSaveDocxFile, actions, dispatch }) {
    const classes = Styles();
    const { sfdt_s3key, type, s3_file_key } = record;

    const [showModal, setModalOpen] = useState(false);
    const [documentEditorInstance, setDocumentEditorInstance] = useState(null);
    const symbolRef = React.createRef();

    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    const smDown = useMediaQuery(theme.breakpoints.down('sm'));
    const mdDown = useMediaQuery(theme.breakpoints.down('md'));
    const [selectedValue, setSelectedValue] = useState(0);
    const [sfdtData, setSfdtData] = useState(false);
    const [version, setVersion] = useState(false);
    const currentVersionId = discoveryHistory && discoveryHistory.length && discoveryHistory[0].id || false;
    const documentHistory = record && record.generate_method == 'discovery' && (discoveryHistory && discoveryHistory.length > 1) || false
    const [documentLoader, setDocumentLoader] = useState(false);
    const [openDrawer, setOpenDrawer] = React.useState({ right: false });


    useEffect(() => {
        const handleKeyDown = (args) => {
            let filename = s3_file_key && s3_file_key.slice(s3_file_key.lastIndexOf('/') + 1, s3_file_key.length);
            let keyCode = args.event.which || args.event.keyCode;
            let isCtrlKey = (args.event.ctrlKey || args.event.metaKey) ? true : ((keyCode === 17) ? true : false);
            let isAltKey = args.event.altKey ? args.event.altKey : ((keyCode === 18) ? true : false);

            if (isCtrlKey && !isAltKey && keyCode === 83 && filename) {
                args.isHandled = true;
                filename = filename.replace('.docx', '');
                documentEditorInstance.documentEditor.save(filename, 'Docx');
                args.event.preventDefault();
            }
        };

        if (documentEditorInstance?.documentEditor) {
            documentEditorInstance.documentEditor.keyDown = handleKeyDown;
            onLoadDefault();
        }

        return () => {
            if (documentEditorInstance?.documentEditor) {
                documentEditorInstance.documentEditor.keyDown = null;
            }
        };
    }, [documentEditorInstance, sfdtData, type]);

    useEffect(() => {
            dispatch(actions.getSFDTtext(sfdt_s3key, setDocumentLoader, setSfdtData));
    }, [sfdt_s3key]);


    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setOpenDrawer({ ...openDrawer, [anchor]: open });
    };

    const closeModal = () => {
        setModalOpen(false);
        setSfdtData(false);
        if (onClose)
            onClose();
    }  

    function onLoadDefault() {
        //SFDT
        let jsonData = sfdtData && typeof(sfdtData) == 'string' ? JSON.parse(sfdtData) : sfdtData || false;
        if (jsonData) {
            documentEditorInstance?.documentEditor?.open(JSON.stringify(jsonData));
            if (documentEditorInstance?.documentEditor) {
                documentEditorInstance.documentEditor.zoomFactor = 0.8;
                if (documentEditorInstance?.documentChange) {
                    documentEditorInstance.documentChange = () => {
                        documentEditorInstance.documentEditor.focusIn();
                    };
                }
            }
        }
    }

    function onCreate() {
        // creating Custom Options
        let menuItems = [
            {
                text: 'Search In Google',
                id: 'search_in_google',
                iconCss: 'e-icons e-de-ctnr-find',
            },
        ];
        // adding Custom Options
        documentEditorInstance.documentEditor.contextMenu.addCustomMenu(menuItems, false);
        //  custom options hide/show functionality
        documentEditorInstance.documentEditor.customContextMenuBeforeOpen = (args) => {
            let search = document.getElementById(args.ids[0]);
            search.style.display = 'none';
        };
    }

    const handleInsertSymbol = (val) => {
        if (documentEditorInstance) {
            documentEditorInstance.documentEditor.editor.insertText(val);
        }
    }

    // let dropDownItems = {
    //     prefixIcon: classes.prefixIcon,
    //     tooltipText: "Menu Symbol",
    //     text: `<span><div>𝝮</div><div>symbols</div></span>`,
    //     id: "dropDwn",
    // }

    let items = [
        // dropDownItems,
        // "Separator",
        "Undo",
        "Redo",
        "Separator",
        "Find",
        "Image",
        "Table",
        // "Hyperlink",
        "Separator",
        "Header",
        "Footer",
        "Separator",
        "PageSetup",
        "PageNumber",
        "Separator",
        // "Break",
        "InsertFootnote",
        "InsertEndnote",
        "Separator",
        // "Comments",
        "TrackChanges",
        "Separator"
    ];

    const onToolbarClick = (args) => {
        switch (args.item.id) {
            case "dropDwn":
                symbolRef.current.click();
                break;
            default:
                break;
        }
    };

    const handleSelectVersion = (sfdtS3Key, index, status) => {
        dispatch(actions.getSFDTtext(sfdtS3Key, setDocumentLoader, setSfdtData));
        setSelectedValue(index);
        setVersion(status);
    }

    return (
        <>
            <Grid container className={className}>
                {children && children(() => setModalOpen(!showModal))}
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    open={showModal || show}
                    className={classes.modal}
                    onClose={!disableCloseModal ? onClose : null}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}>
                    <Fade in={showModal || show}>
                        <Paper className={classes.paper}>
                            <Grid container className={classes.header} justify="space-between">
                                <Grid item xs={10}>
                                    <Typography component="span" className={classes.title}>{title || ''}</Typography>
                                </Grid>
                                <Grid item xs={2} style={{ textAlign: 'end' }}>
                                    <CloseIcon onClick={closeModal} className={classes.closeIcon} />
                                </Grid>
                            </Grid>
                            <Grid container item xs={12} wrap='wrap'>
                                {infoNote && <Grid className={classes.messageGrid} xs={12} md={documentHistory ? 10 : 12}>
                                    <Typography component="span" className={classes.messageNote}>{infoNote || ''}</Typography>
                                </Grid>}
                                {documentHistory ? <Grid container justify='flex-end' xs={12} md={2}>
                                    <Typography component="span" className={classes.discoveryLink} onClick={toggleDrawer('right', true)}>Discovery History</Typography>
                                </Grid> : null}
                            </Grid>
                            <Grid container style={{ display: 'flex' }}>
                                <Grid item style={{ width: '100%' }}>
                                    <div className='control-pane'>
                                        <div className='control-section'>
                                            <div id="documenteditor_container_body">
                                                {documentLoader ? <Spinner className={classes.spinner} /> : <DocumentEditorContainerComponent
                                                    id="container"
                                                    ref={(scope) => {
                                                        setDocumentEditorInstance(scope);
                                                    }}
                                                    height={'70vh'}
                                                    toolbarItems={items}
                                                    serviceUrl="https://ej2services.syncfusion.com/production/web-services/api/documenteditor/"
                                                    enableToolbar={true}
                                                    showPropertiesPane={false}
                                                    toolbarClick={onToolbarClick.bind(this)}
                                                    created={onCreate}
                                                >
                                                    <div className={classes.symbol}>
                                                        <InsertSymbol
                                                            editorInstance={documentEditorInstance}
                                                            handleSymbol={handleInsertSymbol}
                                                        >{(open) =>
                                                        (<Grid
                                                            ref={symbolRef}
                                                            onClick={open}
                                                            className={classes.symbolBtn}
                                                        >
                                                            <Grid className={classes.omegaIcon}>𝝮</Grid>
                                                            {(!mdDown || !smDown) && <Grid>{"Symbols"}</Grid>}
                                                        </Grid>)
                                                            }
                                                        </InsertSymbol>
                                                    </div>
                                                </DocumentEditorContainerComponent>}

                                            </div>
                                        </div>
                                    </div>
                                </Grid>
                            </Grid>
                            <Grid container className={classes.footer}>
                                <Grid item xs={12}>
                                    <Grid container justify="flex-end">
                                        <Grid item xs={6} style={{ alignItems: "center", display: "flex" }}>
                                            {sm ? <Button
                                                type="button"
                                                variant="contained"
                                                color="primary"
                                                className={classes.button}
                                                onClick={() => handleDownloadBtn(documentEditorInstance, version)}>
                                                {progress == 'download' ? <ButtonSpinner /> : 'Download'}
                                            </Button> : (progress == 'download') ? <ClipLoader size={30} color="#2ca01c" /> : <Icons type={'download'} className={classes.icons} onClick={() => handleDownloadBtn(documentEditorInstance, version)} />}
                                        </Grid>
                                        <Grid item xs={6} className={classes.mailServeBtn}>
                                            {showSaveBtn ? (sm ? <Button
                                                type="button"
                                                variant="contained"
                                                color="primary"
                                                className={classes.button}
                                                onClick={() => handleSubmitBtn(documentEditorInstance, version)}
                                                style={{ marginRight: '10px' }}>
                                                {progress == 'save' ? <ButtonSpinner /> : btnLabel}
                                            </Button> : (progress == 'save') ? <ClipLoader size={30} color="#2ca01c" /> : <Icons type={'Save'} className={classes.icons} onClick={() => handleSubmitBtn(documentEditorInstance, version)} />) : sm ? <Button
                                                type="button"
                                                variant="contained"
                                                color="primary"
                                                className={classes.button}
                                                style={{ marginRight: '10px' }}
                                                onClick={() => handleSaveDocxFile(documentEditorInstance, version)}>
                                                {progress == 'save' ? <ButtonSpinner /> : 'Save'}
                                            </Button> : (progress == 'save') ? <ClipLoader size={30} color="#2ca01c" /> : <Icons type={'Save'} className={classes.icons} onClick={() => handleSaveDocxFile(documentEditorInstance, version)} />}
                                            {sm && <Button
                                                type="button"
                                                variant="contained"
                                                onClick={closeModal}
                                                className={classes.button}>
                                                Cancel
                                            </Button>}
                                        </Grid>
                                        {/* {anchorEl && <Menu
                                            id="basic-menu"
                                            anchorEl={anchorEl}
                                            open={true}
                                            onClose={handleClose}
                                            MenuListProps={{
                                                'aria-labelledby': 'basic-button',
                                            }}
                                        >
                                            <MenuItem onClick={handleClose}>Profile</MenuItem>
                                            <MenuItem onClick={handleClose}>My account</MenuItem>
                                            <MenuItem onClick={handleClose}>Logout</MenuItem>
                                        </Menu>} */}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Fade>
                </Modal>
                <Drawer anchor={'right'} open={openDrawer['right']} onClose={toggleDrawer('right', false)}>
                    {documentHistory ? <Grid item xs={12} className={classes.listContainer} role="presentation">
                        <Grid className={classes.versionHeader} style={{ paddingTop: '5px', paddingBottom: '10px' }}>
                            <Typography variant="h6" style={{ paddingLeft: '18px', paddingBottom: '5px', fontWeight: 'bold', paddingTop: '10px' }}>Discovery History</Typography>
                        </Grid>
                        <Grid className={classes.responseList}>
                            {discoveryHistory.map((history, index) => {
                                const verison = (history && history.id) === currentVersionId ? "Current Version" : "Previous Version";
                                return <Grid key={index} className={`${classes.list} ${selectedValue == index ? classes.selected : ""}`} onClick={() => {
                                    toggleDrawer('right', false);
                                    handleSelectVersion(history.sfdt_s3key, index, verison);
                                }}>
                                    <Grid item style={{ padding: '10px 18px 3px 25px', cursor: 'pointer' }}>
                                        {history?.doc_filename ? <Tooltip title={`${history.doc_filename}`} placement="left">
                                            <Typography className={`${classes.date} ${selectedValue == index ? classes.selectedText : ''}`}>
                                                {`${(history?.doc_filename).toString().substring(0, 20)}...`}
                                            </Typography>
                                        </Tooltip> : null}
                                        {(<Grid item className={`${classes.currentVersion} ${selectedValue == index ? classes.selectedText : ''}`}>
                                            {verison}
                                        </Grid>)}
                                        {(<Grid item className={`${classes.currentVersion} ${selectedValue == index ? classes.selectedText : ''}`}>
                                            {history.versioning_timeStamp ? moment(history.versioning_timeStamp).format('MM/DD/YY hh:mm A') : "--/--"}
                                        </Grid>)}
                                    </Grid>
                                </Grid>
                            })}

                        </Grid>
                    </Grid> : null}
                </Drawer>
            </Grid>
        </>
    );
}

export default React.memo(DocumentEditor);
/***
 *
 * Multi Input Field
 *
 */


import React from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import lodash from 'lodash';
import { InputAdornment } from '@material-ui/core';


export default function ({ input, label, placeholder, disabled, style, variant, className, errorStyle, inputLimit, defaultNote, asterisks, meta: { touched, error, warning } }) {
 
    const classes = Styles();
    const { name, value, onChange } = input;
    const filter = createFilterOptions();
    var filteredOptions = [];

    let inputSplit = input && input.value && input.value !== '' && input.value.replace(/\s*,\s*/g, ",").split(',') || ''; 

    let inputValues = inputSplit && inputSplit.length > 0 && Array.isArray(inputSplit) && inputSplit || [];
    const multiOptions =  inputValues && inputValues.length > 0 && inputValues.map(el => Object.assign({}, { label: el, value: el })) || [];

    const handleClick = () => {
        const opt = multiOptions.concat(filteredOptions);
        onChange(lodash.uniqBy(opt, 'label').map(_ => _.label).join(','));
    }

    const filterOptions = (options, params) => {
        const { inputValue } = params;
        const filtered = filter(options, params);

        const isExisting = filtered && filtered.find((option) => inputValue
            .toLowerCase()
            .trim()
            .includes(option.label.toLowerCase().trim()) || inputValue === option.label);

        if (!isExisting || inputValue != '') {
            filtered.push({
                label: inputValue,
            });
            filteredOptions = filtered;
        }
        return filtered;
    };

    return (
        <div style={style || {}}>
            <Autocomplete
                PopperComponent={() => null}
                id="size-small-standard-multi"
                size="small"
                multiple
                freeSolo
                options={multiOptions}
                disableClearable
                onChange={(e, val) => onChange(val.map(_ => _.label).join(','))}
                value={multiOptions}
                onKeyDown={(evt) => {
                    if (['Enter', ','].includes(evt.key) && multiOptions.length < (inputLimit || 5)) {
                        evt.preventDefault();
                        const opt = multiOptions.concat(filteredOptions);
                        onChange(lodash.uniqBy(opt, 'label').map(_ => _.label).join(','));
                    }
                }}
                onBlur={(evt) => {
                    if(evt.type === 'blur' && multiOptions.length < (inputLimit || 5)) {
                        const opt = multiOptions.concat(filteredOptions);
                        onChange(lodash.uniqBy(opt, 'label').map(_ => _.label).join(','));
                    }
                }}
                getOptionLabel={(option) => option.label}
                filterOptions={filterOptions}
                renderInput={(params) => (
                    <>
                        {defaultNote ? <span><strong>{defaultNote}</strong></span> : null}
                        <TextField
                            {...params}
                            type="text"
                            name={name}
                            label={asterisks ? <span className={classes.textSize}>{label} <span className={classes.mandatory}>*</span></span> : <span className={classes.textSize} >{label}</span>}
                            placeholder={placeholder}
                            onKeyPress={e => {
                                if (multiOptions && multiOptions.length >= (inputLimit || 5)) {
                                    e.preventDefault();
                                }
                            }}
                            disabled={disabled}
                            className={className || classes.fieldColor}
                            fullWidth
                            InputProps={{
                                ...params.InputProps,
                                endAdornment: (
                                    <InputAdornment onClick={() => handleClick()} position="end">
                                        {multiOptions && multiOptions.length < (inputLimit || 5) ?
                                            <AddCircleRoundedIcon style={{ fill: "#2ca01c", cursor: 'pointer' }} /> : null}
                                        {params.InputProps.endAdornment}
                                    </InputAdornment>)
                            }}
                            variant={variant || "standard"} />
                    </>
                )}
            />
            <div style={errorStyle || {}} className={classes.error}>
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
     </div>
 
   )
}
 
import React from 'react';
import PropTypes from 'prop-types';
import { usePagination, DOTS } from './usePagination';
import { reduxForm } from 'redux-form';
import { Grid } from '@material-ui/core';
import Styles from './styles';
import Spinner from 'components/Spinner';
import lodash from 'lodash';

export function RenderPagination(props) {
  const {
    onPageChange,
    totalCount,
    siblingCount = 1,
    currentPage,
    pageSize,
    pageLoader,
    className,
    records,
    children,
    standardForm,
    documentType
  } = props;
  const classes = Styles();

  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  if (currentPage === 0 || paginationRange.length < 1) {
    return null;
  }
  let lastPage = paginationRange[paginationRange.length - 1];

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  };

  const onNext = () => {
    if (currentPage != lastPage) {
      onPageChange(currentPage + 1);
      scrollToTop();
    }
  };

  const onPrevious = () => {
    if (currentPage != 1) {
      onPageChange(currentPage - 1);
      scrollToTop();
    }
  };

  const firstPageIndex = (currentPage - 1) * pageSize;
  const lastPageIndex = firstPageIndex + pageSize;
  let filteredQuestions; 
  let questions;
  let groupedQuestion = Object.keys(records).reduce((result, key) => {
    result[key] = records[key];
    return result;
  }, {});

  filteredQuestions = Object.keys(records)
    .slice(firstPageIndex, lastPageIndex)
    .reduce((result, key) => {
      result[key] = records[key];
      return result;
    }, {});

    if(!standardForm){
      if(documentType && documentType === 'odd') {
        questions =
        (filteredQuestions &&
          filteredQuestions &&
          lodash.groupBy(filteredQuestions, 'question_category_id')) ||
        {};
        groupedQuestion = groupedQuestion && lodash.groupBy(groupedQuestion, 'question_category_id') || {};
      } else {
        questions =
        (filteredQuestions &&
          filteredQuestions &&
          lodash.groupBy(filteredQuestions, 'question_number_text')) ||
        {};
        groupedQuestion = groupedQuestion && lodash.groupBy(groupedQuestion, 'question_number_text') || {};
      }
    } else{
      questions = filteredQuestions
    }

  return (
    <Grid style={{ width : '100%'}}>
      <Grid item>
          <Grid>
            {pageLoader ? (
              <Spinner loading={true} className={classes.spinner} />
            ) : (
              children && children(questions, groupedQuestion)
            )}
          </Grid>
          <Grid className={classes.footer}>
            <Grid className="react-table-counter">
              <p>
                {Object.keys(records).length > 0
                  ? `Showing ${pageSize * (currentPage - 1) + 1} to ${pageSize *
                      (currentPage - 1) + Object.entries(records).slice(firstPageIndex,lastPageIndex).length} of ${totalCount} entries.`
                  : `No entries to show.`}
              </p>
            </Grid>
          <Grid className="react-table-pagination-root">
            <ul className="react-table-pagination-list" style={{ float: 'right' }}>
              <li
                className="react-table-pagination-button previous"
                onClick={onPrevious}
              >
                <Grid>Back</Grid>
              </li>
              {paginationRange.map((pageNumber, pageIndex) => {
                  if (pageNumber === DOTS) {
                    return <li key={pageIndex} className="react-table-pagination-button dots">&#8230;</li>;
                  }
                return (
                  <li
                    key={pageIndex}
                    className={
                      pageNumber === currentPage
                        ? 'react-table-pagination-button active'
                        : 'react-table-pagination-button'
                    }
                    onClick={() => {
                      onPageChange(pageNumber);
                      scrollToTop();
                    }}
                  >
                    {pageNumber}
                  </li>
                );
              })}
              <li className="react-table-pagination-button next" onClick={onNext}>
                <Grid>Next</Grid>
              </li>
            </ul>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

RenderPagination.propTypes = {
  children: PropTypes.func,
};

export default reduxForm({
  form: 'paginationRecord',
  touchOnChange: true,
  destroyOnUnmount: false,
})(RenderPagination);

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({    
    footer : {
        width:'100%',
        bottom : 0
    },
    spinner: {
        padding: '20px',
        marginTop: '200px',
        marginBottom: '200px',
    }
}))

export default useStyles;

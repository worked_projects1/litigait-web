/**
 * 
 *  Modal Record Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { normalize, disableOnSubmitForm } from 'utils/tools';
import Styles from './styles';
import history from 'utils/history';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ModalRecordForm(props) {
    const { title, message, messageRegular, handleSubmit, submitting, children, fields, metaData, className, style, btnLabel, onOpen, onClose, error, show, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, disableContainer, enableSubmitBtn, disableCancelBtn, progress, footerBtn, confirmPopUpClose, paperClassName, enableScroll, page, details, customNote, footerBtn1, disableCancelButton, instructionNote, footerCancelBtn, disableSubmitBtn, footerBtnFunc1, footerBtnFunc, alertOnSubmitClose, priceAlertInfo, footerSubmitBtn, form, onSubmitbtn, AlertBtnLabel1, AlertBtnLabel2, disableCloseModal, submitBtn, hideFields } = props;
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    const lg = useMediaQuery(theme.breakpoints.up('lg'));

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
        if(page === 'clients') {
            history.push({ pathname: '/clientCategories/clients', state : Object.assign({}, { ...history.location.state }, { respondCase: false })})
        }
    }

    const handleSubmitRecord = (e) => {
        const isDisableOnSubmit = disableOnSubmitForm.find(e => form.startsWith(e));
        if(isDisableOnSubmit) {
            e.preventDefault();
        } else {
            handleSubmit(e);
        }
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={!disableCloseModal ? closeModal : null}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                    <form onSubmit={(e) => handleSubmitRecord(e)} className={classes.form} noValidate>
                        <Grid container className={classes.header} justify="space-between">
                            <Grid item xs={10}>
                                <Typography component="span" className={classes.title}>{title || ''}</Typography>
                            </Grid>
                            <Grid item xs={2} style={{ textAlign: 'end' }}>
                                {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                            </Grid>
                        </Grid>
                        {customNote ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{customNote || ''}</Typography>
                            </Grid> : null}
                        {message ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{message || ''}</Typography>
                            </Grid> : null}
                        {messageRegular ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.messageRegular}>{messageRegular || ''}</Typography>
                            </Grid> : null}
                        {instructionNote ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.messageRegular}>{instructionNote}</Typography>
                            </Grid> : null}
                        {priceAlertInfo ?
                        <Grid className={classes.messageGrid}>
                            <Typography component="span" className={classes.messageRegular}>
                                <span dangerouslySetInnerHTML={{ __html: priceAlertInfo || '' }} />
                            </Typography>
                        </Grid> : null}
                        <Grid container className={customNote ? classes.body1 : classes.body}>
                            {details ? 
                                <Grid item xs={4} className={classes.details}>
                                    {details}
                                </Grid> : null}
                            {!hideFields && <Grid item xs={details ? 8 : 12} className={enableScroll ? enableScroll : null}>
                                <Grid container spacing={3}>
                                    {(fields || []).map((field, index) => {
                                        const InputComponent = ImplementationFor[field.type];
                                        return <Grid key={index} item xs={field?.gridSize || 12} style={field.style || null}>
                                            {field.type !== 'fieldArray' ?
                                                <Field
                                                    name={field.value}
                                                    label={field.label}
                                                    type="text"
                                                    metaData={metaData}
                                                    component={InputComponent}
                                                    required={field.required}
                                                    normalize={normalize(field)}
                                                    disabled={field.disableOptons && field.disableOptons.edit}
                                                    formName={props?.form}
                                                    formFieldChange={props?.change}
                                                    {...field} /> : 
                                                    <FieldArray
                                                        name={field.value}
                                                        label={field.label}
                                                        type="text"
                                                        fieldArray={field.fieldArray}
                                                        metaData={metaData}
                                                        component={InputComponent}
                                                        required={field.required}
                                                        normalize={normalize(field)}
                                                        ImplementationFor={ImplementationFor}
                                                        disabled={field.disableOptons && field.disableOptons.edit}
                                                        {...field} />}
                                        </Grid>
                                    })}
                                </Grid>
                            </Grid> || null}
                            {error ? <Grid item xs={12} className={classes.error}> <Error errorMessage={error} /></Grid> : null}
                        </Grid>
                        {notes ? <Grid >
                            <Typography component="span" className={classes.note}>{notes || ''}</Typography>
                        </Grid> : null}
                        <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                            {footerBtnFunc && typeof footerBtnFunc === 'function' && footerBtnFunc(setModalOpen) || null}
                            {footerBtnFunc1 && typeof footerBtnFunc1 === 'function' && footerBtnFunc1(setModalOpen) || null}
                            {footerBtn1 && React.createElement(footerBtn1) || null}
                            {footerBtn && React.createElement(footerBtn, Object.assign({}, { lg })) || null}
                            {footerSubmitBtn && typeof footerSubmitBtn === 'function' && footerSubmitBtn(setModalOpen, props) || null}
                            {submitBtn && typeof submitBtn === 'function' ? React.createElement(submitBtn, Object.assign({}, { ...props, classes })) : null}
                            {!disableSubmitBtn && confirmButton ?
                                <AlertDialog
                                    description={confirmMessage}
                                    onConfirm={() => {
                                        handleSubmit();
                                        if(alertOnSubmitClose && onSubmitClose) {
                                            closeModal();
                                        }
                                    }}
                                    onConfirmPopUpClose={confirmPopUpClose}
                                    btnLabel1={AlertBtnLabel1 || 'Yes'}
                                    btnLabel2={AlertBtnLabel2 || 'No'} >
                                    {(open) => <Button
                                        type="button"
                                        variant="contained"
                                        onClick={open}
                                        disabled={page && page === 'casesEdit' ? submitting : !enableSubmitBtn && (pristine || invalid)}
                                        color="primary"
                                        className={classes.button}>
                                        {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                    </Button>}
                                </AlertDialog> : !disableSubmitBtn && onSubmitbtn ? <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    disabled={page && page === 'casesEdit' ? submitting : !enableSubmitBtn && (pristine || invalid)}
                                    className={classes.button}
                                    onClick={() => {
                                        handleSubmit();
                                        if(!invalid && onSubmitClose) {
                                            closeModal();
                                        }
                                    }}>
                                    {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                </Button> :
                                !disableSubmitBtn ? <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={page && page === 'casesEdit' ? submitting : !enableSubmitBtn && (pristine || invalid)}
                                    className={classes.button}
                                    onClick={!invalid && onSubmitClose ? closeModal : null}>
                                    {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                </Button> : true}
                            {(!disableCancelBtn && !disableCancelButton) ? <Button
                                type="button"
                                variant="contained"
                                onClick={closeModal}
                                className={classes.button}>
                                Cancel
                            </Button> : true}
                            {footerCancelBtn && typeof footerCancelBtn === 'function' && React.createElement(footerCancelBtn, classes) || null}
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ModalRecordForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'modalRecord',
    validate,
    enableReinitialize: true,
    touchOnChange: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(ModalRecordForm);


import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        margin: "0px 15px",
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '12px',
        minWidth: '30%',
        outline: 'none',
        paddingBottom: '35px',
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        width: '210px',
        outline: 'none',
        textTransform: 'capitalize'
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitleOptions: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitle: {
        margin: '5px',
        marginTop: '10px',
        paddingLeft: '20px',
        paddingRight: '20px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    gridButton: {
        textAlign: 'center'
    }
}));


export default useStyles;
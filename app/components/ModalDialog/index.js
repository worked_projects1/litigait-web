/**
 * 
 *  Modal Dialog
 * 
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import Styles from './styles';
import ButtonSpinner from 'components/ButtonSpinner';

function ModalDialog({ title, children, fields, metaData, className, style, headerStyle, btnStyle, titleStyle, footerStyle, options, show, onClose, onConfirmPopUpClose, heading, disableContainer, footerLink, btnLoader }) {
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const handleClose = () => {
        setModalOpen(false);
        if (onClose) {
            onClose();
        }
    }

    const handleSubmit = (option) => {
        option.action(setModalOpen, option.value)
        if (onConfirmPopUpClose) {
            handleClose();
        }
    }

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show}>
                <Paper className={classes.paper}>
                    <Grid container justify="flex-end" className={classes.close}>
                        <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                    </Grid>
                    {typeof heading === 'function' && heading() || heading && <Grid className={classes.gridHeader} style={headerStyle || {}}>
                        <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
                    </Grid> || null}
                    <Grid className={options ? classes.gridTitleOptions : classes.gridTitle} style={titleStyle || {}}>
                        <Typography variant="subtitle1" gutterBottom component="span">{title || ''}</Typography>
                    </Grid>
                    <Grid container justify="center" direction="column" style={footerStyle || {}}>
                        {(options || []).map((option, index) => {
                            return <Grid key={index} item className={classes.gridButton}>
                                <Button type="button" variant="contained" disabled={option.disable} color={option.label == 'Cancel' ? "gray" : "primary"} className={classes.button} style={btnStyle} onClick={option.action == 'close' ? () => setModalOpen(false) : () => handleSubmit(option)}>
                                    {(btnLoader && btnLoader === option.value) ?  <ButtonSpinner size={25} />  : option.label}
                                </Button>
                            </Grid>
                        })}
                    </Grid>
                    {footerLink && React.createElement(footerLink) || null}
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ModalDialog.propTypes = {
    title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default ModalDialog;
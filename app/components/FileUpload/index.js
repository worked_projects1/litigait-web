/**
 * 
 *  File Upload
 * 
 */

import React, { useCallback, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import PublishIcon from '@material-ui/icons/Publish';
import DoneIcon from '@material-ui/icons/Done';
import styles from './styles';
import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
import ProgressProvider from 'components/ProgressProvider';
import 'react-circular-progressbar/dist/styles.css';
import { Grid, Button, Paper, Typography } from '@material-ui/core';
import Error from 'components/Error';
import remotes from 'blocks/upload/remotes.js';
import { capitalizeFirstLetterOnly, capitalizeFirstWord } from 'utils/tools';
import { getContentType, FilesList } from './utils';
import Icons from 'components/Icons';

function FileUpload({ input, contentType = '*', label, upload, multiple, max, secret, viewFiles, meta: { touched, error, warning }, errorType }) {
    const [uploadFiles, setuploadFiles] = useState(false);
    const [uploadError, setuploadError] = useState(false);
    const [uploadPercentage, setPercentage] = useState(false);
    const [uploadedFile, setUploadedFile] = useState(1);
    const [totalFiles, setTotalFiles] = useState(false);

    const inputRef = useRef(false);
    const labelValue = label === 'MODIFIED TEMPLATE' || 'Custom Document Template' ? capitalizeFirstWord(label) : capitalizeFirstLetterOnly(label);
    const accept = contentType;

    const onUpload = (files, index) => {
        if (!files || files.length <= index) {
            return false;
        }

        const ajax = new XMLHttpRequest();
        const Upload = secret ? remotes.getSecretSignature : remotes.getSignature;
        const fileName = files[index].name;
        const fileContentType = getContentType(fileName);
        Upload(fileName, fileContentType)
            .then((result) => {
                const { uploadURL, public_url, s3_file_key } = result;
                if (uploadURL) {
                    ajax.upload.addEventListener("progress", (e) => setPercentage(Math.round((e.loaded / e.total) * 100)), false);
                    ajax.addEventListener("load", () => {
                        const inputVal = secret ? Object.assign({}, { key: s3_file_key, file_name: fileName }) : public_url;
                        if (index === 0) {
                            inputRef.current = [inputVal];
                        } else {
                            inputRef.current.push(inputVal);
                        }
                        if ((files.length - 1) === index) {
                            input.onChange(multiple ? inputRef.current : inputRef.current[0]);
                            inputRef.current = false;
                            setUploadedFile(1);
                            setPercentage(false);
                            setTotalFiles(false);
                        } else {
                            setUploadedFile(index + 2);
                            setPercentage(false);
                            onUpload(files, index + 1);
                        }
                    }, false);
                    ajax.addEventListener("error", () => setuploadError('Upload Failed'), false);
                    ajax.addEventListener("abort", () => setuploadError('Upload Failed'), false);
                    ajax.open("PUT", uploadURL, true);
                    ajax.setRequestHeader("Content-Type", fileContentType);
                    ajax.send(files[index]);
                } else {
                    setuploadError('Upload Failed');
                }
            })
            .catch(() => {
                setuploadError('Upload Failed');
            });
    }

    const onDrop = useCallback((uploadedFiles) => {
        let acceptedFiles = uploadedFiles.map(file => {
            let name = file.name.replace(/[^a-zA-Z.0-9-_\s+]/g, "");
            name = name.replace(/[\+]/g, "");
            return new File([file], name, { type: file.type, lastModified: file.lastModified })
        });

        setuploadFiles(false);
        setuploadError(false);
        setPercentage(false);
        setUploadedFile(1);
        setTotalFiles(false);
        input.onChange(false);
        inputRef.current = false;

        if(acceptedFiles && acceptedFiles.length) {
            if(acceptedFiles.every(file => file && file.size === 0)) {
                setuploadError(`You cannot upload the 0 kb files.`);
                return;
            } 
        }

        acceptedFiles = acceptedFiles.filter(file => file && file.size !== 0);

        if (max && acceptedFiles && max < acceptedFiles.length) {
            errorType ? setuploadError(`You cannot upload more than ${max} summary files`) : setuploadError(`If you need to upload more than ${max} files, do it in batches of upto ${max}`);
        } else if (upload) {
            inputRef.current = [];
            setTotalFiles(acceptedFiles.length);
            onUpload(acceptedFiles, 0);
        } else {
            setuploadFiles(acceptedFiles);
            setTimeout(() => input.onChange(acceptedFiles), 1000);
        }
    }, [setuploadFiles, onUpload, setuploadError, upload, input, uploadFiles]);

    const onDropRejected = useCallback((error) => {
        setuploadError(error[0].errors[0].message);
    }, []);

    const handleFileDelete = (fileIndex) => {
        const Files = input.value && input.value.length > 0 && input.value.filter((e, i) => i !== fileIndex) || [];
        if(Files && Array.isArray(Files) && Files.length > 0){
            setuploadFiles(Files);
            input.onChange(Files);
        } else {
            setuploadFiles(false);
            input.onChange(false);
        }
    }

    const { getRootProps, open, getInputProps } = useDropzone({ onDrop, accept, multiple, onDropRejected });
    const classes = styles();

    return <Grid container style={{ width: '100%' }}>
        {input.value && contentType.indexOf('image') > -1 ?
            <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container}>
                <input {...getInputProps()} />
                <img src={input.value} className={classes.image} />
            </Grid> :
            <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container}>
                <input {...getInputProps()} />
                <Paper className={classes.upload}>
                    <ProgressProvider values={input.value ? [100] : uploadFiles && !uploadError ? [0, 50, 100] : [0]}>
                        {percentage => (<CircularProgressbarWithChildren
                            value={uploadPercentage || percentage}
                            className={classes.progress}
                            styles={buildStyles({
                                pathColor: '#2ca01c',
                                pathTransition:
                                    percentage === 0
                                        ? "none"
                                        : "stroke-dashoffset 0.5s ease 0s"
                            })}>
                            {(uploadPercentage || percentage) && !input.value ? <div className={classes.percentage}>{uploadPercentage || percentage}%</div> : input.value && upload ? <DoneIcon className={classes.icon} /> : <PublishIcon className={classes.icon} />}
                        </CircularProgressbarWithChildren>)}
                    </ProgressProvider>
                </Paper>
                <Grid className={classes.animate}>
                    <div className={classes.circle} style={{ animationDelay: '0s' }} />
                    <div className={classes.circle} style={{ animationDelay: '1s' }} />
                    <div className={classes.circle} style={{ animationDelay: '2s' }} />
                </Grid>
            </Grid>}
        <Grid item xs={12}>
            <Grid container className={classes.container} justify="center" direction="column">
                {!input.value ? <Typography component="span">{uploadPercentage || inputRef.current ? `Uploading ${uploadedFile} of ${totalFiles} ${label === 'MODIFIED TEMPLATE' ? labelValue : label}` : `Drag ${label === 'MODIFIED TEMPLATE' ? labelValue : label === 'files' ? 'files or folders' : label} to upload, or`}</Typography> : null}
                {!inputRef.current ?
                    <Typography component="span">
                        <Button type="button" variant="contained" color="primary" onClick={open} className={classes.button}>
                            {input.value && `Change ${labelValue}` || `Choose ${labelValue}`}
                        </Button>
                    </Typography> : null}
                {input.value && !upload && viewFiles ? <FilesList
                    style={{ justifyContent: 'center', marginTop: '18px' }}
                    onDelete={handleFileDelete}
                    files={input.value}>
                    {(openModal) =>
                        <Typography onClick={openModal} component="span" variant="subtitle2" className={classes.template}>
                        {`View ${labelValue} To Upload:`} <Icons type="Description" className={classes.description} />
                    </Typography>}
                </FilesList> : null}
            </Grid>
        </Grid>
        {(uploadError || (touched && (error || warning))) && !uploadFiles && !uploadPercentage && uploadedFile ? <Grid container className={classes.container}>
            <Error errorMessage={uploadError || error || warning} />
        </Grid> : null}
    </Grid>
}

FileUpload.propTypes = {
    children: PropTypes.func,
    accept: PropTypes.string,
};

export default FileUpload;
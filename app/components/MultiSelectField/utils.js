import React from 'react';

export function infoIconToEndAdornment(endAdornment, element) {
    const children = React.Children.toArray(endAdornment.props.children);
    element && children.push(element);
    return React.cloneElement(endAdornment, {}, children);
}
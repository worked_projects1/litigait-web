/**
 * 
 * Multi Select Field
 * 
 */

import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Styles from './styles';
import { FormControl, TextField} from '@material-ui/core';
import { infoIconToEndAdornment } from './utils';

export default function ({ input, label, required, placeholder, metaData, options, disabled, valueType, selectAll, style, element, asterisks, meta: { touched, error, warning } }) {
    const classes = Styles();

    const { id, name, value } = input;
    const isPreDefinedSet = Array.isArray(options);
    const multiOptions = isPreDefinedSet ? options : metaData[options] || [];
    const fieldValue = valueType === 'array' ? value || [] : value && value.split(',').reduce((a, el) => {
        const opt = multiOptions.find(_ => _.value.toString() === el);
        if(opt)
            a.push(opt)

        return a;
    }, []) || [];

    return (
        <div className={classes.selectField} style={style || {}}>
            <FormControl className={classes.formControl}>
                <Autocomplete
                    multiple
                    id={id}
                    name={name}
                    options={selectAll && multiOptions.length > 0 ? [{ label: 'All', value: '' }].concat(multiOptions) : multiOptions}
                    getOptionLabel={(option) => option.label}
                    value={fieldValue}
                    disabled={disabled}
                    onChange={(e, val) => input.onChange(e.target.textContent === 'All' && valueType === 'array' ? multiOptions : e.target.textContent === 'All' ? multiOptions.map(_ => _.value).join(',') : valueType === 'array' ? val : val.map(_ => _.value).join(','))}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            className={classes.fieldColor}
                            variant="standard"
                            label={asterisks ? <span>
                            {label} <span className={classes.mandatory}>*</span>
                            </span> : <span>{label}</span>
                            }
                            placeholder={placeholder}
                            InputProps={{
                                ...params.InputProps,
                                endAdornment: element ? infoIconToEndAdornment(params.InputProps.endAdornment, element) : params.InputProps.endAdornment
                            }}
                        />
                    )}
                />
            </FormControl>
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>
    )
}


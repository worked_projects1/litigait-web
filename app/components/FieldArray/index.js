/**
 * 
 * Field Array
 * 
 */

import React, { useEffect } from 'react';
import { Field } from 'redux-form';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import { normalize } from 'utils/tools';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';


function FieldArray(props) {

    const { metaData, fields, title, max, fieldArray, btnLabel, ImplementationFor, defaultField, meta: { touched, error, submitFailed } } = props;
    const classes = Styles();

    useEffect(() => {
        if(fields && fields.length <= 0 && defaultField) {
            fields.push({})
        }
    }, []);

    return (
        <Grid>
            {fields ? fields.map((el, index) => {
                return <Grid className={classes.container} key={index}>
                    <span className={classes.remove}>
                        <CancelRoundedIcon style={{ fill: "#2ca01c" }} onClick={() => fields.remove(index)}/>
                    </span>
                    <Typography style={{ fontWeight: 400 }}>{title} {index + 1}</Typography>
                    <Grid container spacing={3}>
                        {(fieldArray || []).map((field, index) => {
                            const InputComponent = ImplementationFor[field.type];
                            return <Grid key={index} item xs={12} style={field.style || null}>
                                <Field
                                    name={`${el}.${field.value}`}
                                    label={field.label}
                                    type={field.type}
                                    metaData={metaData}
                                    component={InputComponent}
                                    normalize={normalize(field)}
                                    disabled={field.disableOptons && field.disableOptons.create}
                                    {...field} />
                            </Grid>
                        })}
                    </Grid>   
                </Grid>
            }): 'No fields found'}
            {fields && fields.length < (max || 5) ? <span onClick={() => fields.push({})}>
                <AddCircleRoundedIcon style={{ fill: "#2ca01c", cursor: 'pointer'}} />
                <Button type="button">{btnLabel}</Button>
            </span> : null}
            <Grid className={classes.error}>
                {error && <span>{error}</span>}
            </Grid>
        </Grid>
    )
}

export default FieldArray;

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        border: '1px solid #eaeaea',
        padding: '10px',
        margin: '8px',
    },
    remove: {
        position: 'relative',
        top: '-18px',
        left: '-13px',
        width: '0',
        height: '0',
        cursor: 'pointer'
    },
    error: {
        color: 'red',
        marginTop: '10px',
        marginBottom: '10px'
    }
  
}));

export default useStyles;

/**
 * 
 * Custom Signature Form
 * 
 */
import React, { useState, useEffect } from 'react';
import { Grid, Typography, FormControlLabel, Button, FormLabel, FormControl, Checkbox,RadioGroup, Radio } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import SignatureCanvas from 'react-signature-canvas';
import ClearIcon from '@material-ui/icons/Clear';
import Styles from './styles';


export default function CustomSignatureForm({ input, signature, errorStyle, showRequired, change, meta: { touched, error, warning }}) {

    const classes = Styles()
    const signPad = React.createRef();
    const [width, setWidth] = useState(window.innerWidth);
    const [signValue, setSignValue] = useState('user_signature');
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    useEffect(() => {
        let mounted = true;
        signature && input.onChange(signature);
        change('bodyScroll', false);
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);


    const handleChange = (event) => {
        setSignValue(event.target.value);
        if(event.target.value === 'user_signature') {
            change('bodyScroll', false);
            input.onChange(signature)
        } else if(event.target.value === 'custom_signature'){
            change('bodyScroll', true);
            input.onChange('')
        }
    };

    const handleClear = () => {
        signPad.current.clear();
        input.onChange('');
    }

    return (<Grid container>
        {signature ? <Grid item xs={12}>
            <FormControl>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue="user_signature"
                    name="radio-buttons-group"
                    value={signValue}
                    onChange={handleChange}>
                    <Grid container alignItems="center">
                        <Grid item xs={12}>
                            <FormControlLabel value="user_signature" className={classes.signLabel} control={<Radio style={{ color: "green" }} />} label={<img className={classes.signatureImg} src={signature} alt='No signature' />} />
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: '10px' }}>
                        <FormControlLabel
                            value="custom_signature"
                            control={<Radio style={{ color: "green", alignItems: 'center' }} />}
                            label="Use custom signature" />
                    </Grid>
                </RadioGroup>
            </FormControl>
            {signValue === 'custom_signature' ? <Grid item style={{ color: "#464444" }}>
                <Grid container style={{ marginBottom: '12px' }} direction="row" justify="space-between">
                    <Grid style={{ display: 'inline-grid', maxWidth: '570px', alignContent: 'end' }}>
                        <Typography variant="inherit">Draw your signature here</Typography>
                    </Grid>
                    <Grid>
                        <Button variant="contained" className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>Clear</Button>
                    </Grid>
                </Grid>
                <Grid item xs={12} className={classes.signature}>
                    <SignatureCanvas penColor='black'
                        canvasProps={{ width: !sm ? width - 80 : 460, height: 200, className: 'signCanvas' }}
                        ref={signPad}
                        onEnd={(e) => {
                            input.onChange(signPad.current.getTrimmedCanvas().toDataURL('image/png'))
                        }} />
                </Grid>
                <div style={errorStyle || {}} className={classes.error}>
                    {showRequired && touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </Grid> : null}
        </Grid> : 
        <Grid container>
            <Grid item xs={12} style={{ color: "#464444", marginBottom: '12px' }}>
                <Grid container direction="row" justify={"space-between"}>
                    <Grid style={{ display: 'inline-grid', maxWidth: '570px', alignContent: 'end' }}>
                        <Typography variant="inherit">Draw your signature here</Typography>
                    </Grid>
                    <Grid>
                        <Button variant="contained" className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>Clear</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} className={classes.signature}>
                <SignatureCanvas penColor='black'
                    canvasProps={{ width: !sm ? width - 80 : 460, height: 200, className: 'signCanvas' }}
                    ref={signPad}
                    onEnd={(e) => {
                        input.onChange(signPad.current.getTrimmedCanvas().toDataURL('image/png'))
                    }} />
            </Grid>
            <div style={errorStyle || {}} className={classes.error}>
                {showRequired && touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </Grid>}
    </Grid>)
}

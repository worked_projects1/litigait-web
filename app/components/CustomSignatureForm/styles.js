import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({

    btnClear: {
        opacity: "0.5"
    },
    error: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: 'red'
    },
    signatureImg: {
        width: '40%',
        height: '40%',
    }, 
    signature: {
        backgroundColor: "rgb(128 128 128 / 38%)", 
        marginBottom: '10px'
    },
    signLabel: {
        marginRight: '0px'
    }
}));


export default useStyles;

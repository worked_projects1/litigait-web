/**
 * 
 * Reset Password Form 
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';

import { Grid, Button } from '@material-ui/core';
import Error from '../Error';
import Success from 'components/Success';

import PasswordField from 'components/PasswordField';
import ButtonSpinner from 'components/ButtonSpinner';

import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ResetPasswordForm(props) {

    const { handleSubmit, pristine, submitting, error, success, destroy } = props;
    const classes = Styles();

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Field name="new_password" label="Password" component={PasswordField} type="text" required autoFocus />
                </Grid>
                <Grid item xs={12}>
                    <Field name="confirm_password" label="Confirm Password" component={PasswordField} type="text" required />
                </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit} >
                {submitting ?
                    <ButtonSpinner /> :
                    <Grid>
                        Reset Password
                    </Grid>}
            </Button>
            <Grid item xs={12}>
                {error ? <Error errorMessage={error} /> : null}
                {success && success.reset ? <Success successMessage={success.reset} /> : null}
            </Grid>
        </form>
    )
}


const validate = (values) => {

    const errors = {}

    const sequence = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678)+/ig

    const identical = /^(?!.*(.)\1\1.*).*$/igm

    const commonNames = ["123456", "password", "123456789", "12345678", "12345",
        "111111", "1234567", "sunshine", "qwerty", "iloveyou", "princess", "admin", "welcome",
        "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&amp;*", "charlie",
        "aa123456", "donald", "password1", "qwerty123"
    ]

    const requiredFields = ['new_password', 'confirm_password'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (values.new_password && values.new_password.length < 8) {
        errors.new_password = 'Password must be at least 8 characters'
    }

    if (values.new_password && values.new_password.length >= 8 && !/^(?=.*[\d#?!@$%^&*-]).{8,}$/i.test(values.new_password)) {
        errors.new_password = 'Must contain at least one numeric or special character '
    }

    if (values.new_password && sequence.test(values.new_password) || !identical.test(values.new_password)) {
        errors.new_password = 'Avoid consecutive sequential and identical characters'
    }

    commonNames.forEach(field => {
        if (values.new_password == field) {
            errors.new_password = "Password is easily guessable"
        }
    })

    if (values.new_password && values.confirm_password && values.confirm_password != values.new_password) {
        errors.confirm_password = 'Password Mismatch'
    }

    return errors
}


export default reduxForm({
    form: 'reset',
    validate,
    touchOnChange: true,
})(ResetPasswordForm);
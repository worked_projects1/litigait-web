

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#2ca01c',
    textTransform: 'none',
    "&:hover": {
      backgroundColor: '#2ca01c',
    }
  }
}));


export default useStyles;
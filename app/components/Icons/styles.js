

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  svg: {
    width: '25px',
    height: '25px'
  }
}));


export default useStyles;
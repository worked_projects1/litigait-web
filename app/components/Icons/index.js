/**
 * 
 * Icons
 * 
 */

import React from 'react';
import GroupIcon from '@material-ui/icons/Group';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PortraitIcon from '@material-ui/icons/Portrait';
import FilterNoneIcon from '@material-ui/icons/FilterNone';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import UpdateIcon from '@material-ui/icons/Update';
import DeviceHubIcon from '@material-ui/icons/DeviceHub';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import SchoolIcon from '@material-ui/icons/School';
import CategoryIcon from '@material-ui/icons/Category';
import MonetizationOnOutlinedIcon from '@material-ui/icons/MonetizationOnOutlined';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import SettingsIcon from '@material-ui/icons/Settings';
import PlayCircleFilledOutlinedIcon from '@material-ui/icons/PlayCircleFilledOutlined';
import ChromeReaderModeIcon from '@material-ui/icons/ChromeReaderMode';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';
import ChevronRightRoundedIcon from '@material-ui/icons/ChevronRightRounded';
import ChevronLeftRoundedIcon from '@material-ui/icons/ChevronLeftRounded';
import PermPhoneMsgIcon from '@material-ui/icons/PermPhoneMsg';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import DescriptionIcon from '@material-ui/icons/Description';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import EditIcon from '@material-ui/icons/Edit';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import SearchIcon from '@material-ui/icons/Search';
import DomainIcon from '@material-ui/icons/Domain';
import SecurityIcon from '@material-ui/icons/Security';
import ArchiveIcon from '@material-ui/icons/Archive';
import ArchiveOutlinedIcon from '@material-ui/icons/ArchiveOutlined';
import UnarchiveOutlinedIcon from '@material-ui/icons/UnarchiveOutlined';
import MonetizationOn from '@material-ui/icons/MonetizationOn';
import RemoveShoppingCartSharpIcon from '@material-ui/icons/RemoveShoppingCartSharp';
import HistoryIcon from '@material-ui/icons/History';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import LiveHelpOutlinedIcon from '@material-ui/icons/LiveHelpOutlined';
import GetAppIcon from '@material-ui/icons/GetApp';
import SendIcon from '@material-ui/icons/Send';
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined';
import VideoLibraryOutlinedIcon from '@material-ui/icons/VideoLibraryOutlined';
import SaveIcon from '@material-ui/icons/Save';
import SVG from 'react-inlinesvg';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function Icons(props) {

    const classes = Styles();

    switch (props.type) {
        case 'Group':
            return <GroupIcon {...props} />
        case 'ShoppingCart':
        case 'SubscriptionRenewals':
            return <ShoppingCartIcon {...props} />
        case 'Business':
            return <BusinessCenterIcon {...props} />
        case 'Portrait':
            return <PortraitIcon {...props} />
        case 'FilterNone':
            return <FilterNoneIcon {...props} />
        case 'PeopleAlt':
            return <PeopleAltIcon {...props} />
        case 'Update':
            return <UpdateIcon {...props} />
        case 'DeviceHub':
            return <DeviceHubIcon {...props} />
        case 'ShowChart':
            return <ShowChartIcon {...props} />
        case 'School':
            return <SchoolIcon {...props} />
        case 'Dollar':
            return <MonetizationOnOutlinedIcon {...props} />
        case 'Category':
            return <CategoryIcon {...props} />
        case 'ArrowRight':
            return <SubdirectoryArrowRightIcon {...props} />
        case 'SettingsIcon':
            return <SettingsIcon {...props} />
        case 'PlayCircleFilledOutlinedIcon':
            return <PlayCircleFilledOutlinedIcon {...props} />
        case 'ChromeReaderModeIcon':
            return <ChromeReaderModeIcon {...props} />
        case 'LocalHospitalIcon':
            return <LocalHospitalIcon {...props} />
        case 'ContactSupportIcon':
            return <ContactSupportIcon {...props} />
        case 'WbIncandescentIcon':
            return <WbIncandescentIcon {...props} />
        case 'ChevronRightIcon':
            return <ChevronRightRoundedIcon {...props} />
        case 'ChevronLeftIcon':
            return <ChevronLeftRoundedIcon {...props} />
        case 'PermPhoneMsgIcon':
            return <PermPhoneMsgIcon {...props} />
        case 'AccountCircleIcon':
            return <AccountCircleIcon {...props} />
        case 'Description':
            return <DescriptionIcon {...props} />
        case 'LockOpen':
            return <LockOpenIcon {...props} />
        case 'Edit':
            return <EditIcon {...props} />
        case 'Info':
            return <InfoOutlinedIcon {...props} />
        case 'Dashboard':
            return <SVG src={require('images/icons/Dashboard.svg')} className={classes.svg} />
        case 'Practice':
            return <SVG src={require('images/icons/Practice.svg')} className={classes.svg} />
        case 'Clients':
            return <SVG src={require('images/icons/Client-new2.svg')} className={classes.svg} />
        case 'Cases':
            return <SVG src={require('images/icons/Cases.svg')} className={classes.svg} />
        case 'Orders':
            return <SVG src={require('images/icons/Orders.svg')} className={classes.svg} />
        case 'Users':
            return <SVG src={require('images/icons/Users.svg')} className={classes.svg} />
        case 'Objections':
            return <SVG src={require('images/icons/Objection-new.svg')} className={classes.svg} />
        case 'SupportRequest':
            return <SVG src={require('images/icons/SupportRequest.svg')} className={classes.svg} />
        case 'MedicalHistory':
            return <SVG src={require('images/icons/MedicalHistory.svg')} className={classes.svg} />
        case 'Practice':
            return <SVG src={require('images/icons/Practice.svg')} className={classes.svg} />
        case 'Language':
            return <SVG src={require('images/icons/Language.svg')} className={classes.svg} />
        case 'Settings':
            return <SVG src={require('images/icons/Settings.svg')} className={classes.svg} />
        case 'Prompt':
            return <SVG src={require('images/icons/Prompt.svg')} className={classes.svg} />
        case 'Emoji':
            return <EmojiObjectsIcon {...props} />
        case 'Back':
            return <ArrowBackIcon {...props}/>
        case 'Duplicate':
            return <FileCopyIcon {...props}/>
        case 'Search':
            return <SearchIcon {...props}/>
        case 'State':
            return <DomainIcon {...props}/>
        case 'FormOtp':
            return <SecurityIcon {...props}/>
        case 'Archive':
            return <ArchiveIcon {...props}/>
        case 'UnArchive':
            return <UnarchiveOutlinedIcon {...props}/>
        case 'CustomTemplate':
            return <SVG src={require('images/icons/CustomTemplate.svg')} style={{ width: '22px', height: '22px' }} />
        case 'PropoundTemplate':
            return <SVG src={require('images/icons/PropoundTemplate.svg')} style={{ width: '22px', height: '22px', marginLeft: '2px' }} />
        case 'Pricing':
            return <MonetizationOn {...props}/>
        case 'Cancel':
            return <RemoveShoppingCartSharpIcon {...props}/>
        case 'Discount':
            return <HistoryIcon {...props}/>
        case 'Income':
            return <AccountBalanceIcon {...props}/>
        case 'quickCreate':
            return <SVG src={require('images/icons/quickCreate.svg')} className={classes.svg} />
        case 'Help':
            return <LiveHelpOutlinedIcon {...props}/>
        case 'ArchiveOutlined':
            return <ArchiveOutlinedIcon {...props} />
        case 'download':
            return <GetAppIcon {...props}/>
        case 'send':
            return <SendIcon {...props} />
        case 'Video':
            return <VideoLibraryOutlinedIcon {...props}/>
        case 'Faqs':
            return <HelpOutlineOutlinedIcon {...props}/>
        case 'Save':
            return <SaveIcon {...props}/>
        case 'HashFile':
            return <DescriptionIcon {...props}/>
        default:
            return <DashboardIcon {...props} />
    }
}
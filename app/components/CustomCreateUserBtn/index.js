
/**
 * 
 * Custom CreateUser Btn
 * 
 * 
 */



import React from 'react';
import 'react-circular-progressbar/dist/styles.css';
import { Grid, Button } from '@material-ui/core';
import AlertDialog from 'components/AlertDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import Styles from './styles';



function CustomCreateUserBtn(props) {
    const classes = Styles();

    const { pristine, submitting, customDescriptionMessage, existingPasswordModal, selectedUserRecord, formData, setExistingPasswordModal, dispatch, actions } = props;

    const handleCreateUser = () => {
        dispatch(actions.createRecord(selectedUserRecord, formData));
    }

    return (
        <>
            <AlertDialog
                description={customDescriptionMessage}
                openDialog={existingPasswordModal}
                closeDialog={() => setExistingPasswordModal(false)}
                onConfirm={handleCreateUser}
                onConfirmPopUpClose={true}
                btnLabel1='YES'
                btnLabel2='NO' />
            <Button
                type="submit"
                disabled={pristine || submitting}
                variant="contained"
                color="primary"
                className={classes.submitBtn}>
                {submitting && <ButtonSpinner /> || 'Create'}
            </Button>

        </>
    );
}

export default CustomCreateUserBtn;


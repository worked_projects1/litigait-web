import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    fileName: {
        fontSize: "14px",
        fontFamily: "Avenir-Regular",
        fontWeight: 500,
    },
    editIcon: {
        width: "25px",
        cursor: "pointer",
        '& :hover': {
            backgroundColor: 'transparent'
        },
    },
    prevIcon: {
        width: "25px",
        cursor: "pointer",
        marginRight: "5px"
    },
    saveIcon: {
        width: "25px",
        cursor: "pointer",
        color: "#2ca01c !important",
    },
    customInput: {
        padding: '3px 0px',
        fontFamily: 'Avenir-Regular',
        fontSize: '14px',
        '&:disabled': {
            color: 'rgba(0, 0, 0, 0.87) !important'
        }
    },
    customTextBox: {
        paddingBottom: '0px',
        paddingRight: '6px',
        borderColor: '#2ca01c !important',
        '& :after': {
            borderColor: '#2ca01c',
        },
        '& :before': {
            borderColor: '#2ca01c',
        },
        '& :hover': {
            borderColor: '#2ca01c'
        },
        '& .Mui-focused fieldset': {
            borderColor: '#2ca01c',
        },
        '& .MuiInputBase-marginDense': {
            padding: "0px"
        }
    },
}));


export default useStyles;

/***
 *
 * Multi Email Field
 *
 */


import React from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import lodash from 'lodash';


export default function ({ input, label, placeholder, disabled, style, variant, className, errorStyle, asterisks, meta: { touched, error, warning } }) {
 
    const classes = Styles();
    const { name, value, onChange } = input;
    const filter = createFilterOptions();
    var filteredOptions = [];

    let inputSplit = input && input.value && input.value !== '' && input.value.split(',') || ''; 
    let inputValues = inputSplit && inputSplit.length > 0 && Array.isArray(inputSplit) && inputSplit || [];
    const multiOptions =  inputValues && inputValues.length > 0 && inputValues.map(el => Object.assign({}, { label: el, value: el })) || [];
    
    const validateEmail = email => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,}))$/;
        return re.test(email);
    }

    return (
        <div style={style || {}}>
            <Autocomplete
                PopperComponent={() => null}
                multiple
                id="size-small-standard-multi"
                size="small"
                freeSolo
                disabled={disabled}
                options={multiOptions}
                onKeyDown={(evt) => {
                    if (['Enter', 'Tab', ','].includes(evt.key)) {
                        evt.preventDefault();
                        const opt = multiOptions.concat(filteredOptions);
                        onChange(lodash.uniqBy(opt, 'label').map(_ => _.label).join(','));
                    }
                }}
                onChange={(e, val) => onChange(val.map(_ => _.label).join(','))}
                value={multiOptions}
                getOptionLabel={(option) => option.label}
                filterOptions={(options, params) => {
                    const filtered = filter(options, params);
                    const { inputValue } = params;

                    // Suggest the creation of a new value
                    const isExisting = multiOptions.some((option) => option.label.includes(inputValue) || inputValue === option.label);
                    if(validateEmail(inputValue)) {
                        if(multiOptions && multiOptions.length <= 0) {
                            onChange(inputValue);
                        } else if(inputValue !== '' && !isExisting) {
                            filtered.push({
                              label: inputValue,
                            });
                            filteredOptions = filtered;
                        }
                    }
                    return filtered;
                }}
                renderInput={(params) => (
                    <TextField 
                        {...params} 
                        type="text"
                        name={name}
                        label={asterisks ? <span className={classes.textSize}>{label} <span className={classes.mandatory}>*</span></span> : <span className={classes.textSize} >{label}</span>}
                        placeholder={placeholder}
                        disabled={disabled}
                        className={className || classes.fieldColor}
                        fullWidth
                        variant={variant || "standard"} />
                )}
            />
            <div style={errorStyle || {}} className={classes.error}>
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
     </div>
 
   )
}
 
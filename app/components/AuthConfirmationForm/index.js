
/**
 * 
 * Auth Confirmation Form
 * 
 */


import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { Grid, Button, Typography } from '@material-ui/core';

import InputField from 'components/InputField';
import CheckboxField from 'components/CheckboxField';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from '../Error';

import Styles from './styles';


function AuthConfirmationForm({ handleSubmit, submitting, errorMessage, clearCache, destroy, locationState = {}, iframe }) {

    const classes = Styles();

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Grid container direction="row" justify="center" alignItems="center" style={{ marginBottom: '20px' }}>
                <a href={process.env.ENV === 'production' ? `https://www.esquiretek.com/` : `https://staging.esquiretek.com/`} >
                    <img src={require('images/icons/new_logo.jpg')} style={{ width: '80%' }} />
                </a>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Field name="practice_name" label="Practice Name" component={InputField} type="text" required autoFocus />
                </Grid>
                <Grid item xs={12}>
                    <Field name="name" label="Attorney Name" component={InputField} type="text" required />
                </Grid>
                <Grid item xs={12}>
                    <Field name="email" label="Email" disabled component={InputField} type="text" required />
                </Grid>
                <Grid item xs={12} className={classes.div}>                    
                    <Typography variant="body2" className={classes.marginTopMedium}>
                        By registering, you agree to accept our
                    </Typography>
                    <Grid>
                        <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/privacy.html' style={{ color: '#0077c5' }} target='_blank'>Privacy Policy</a> & <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/terms.html' style={{ color: '#0077c5' }} target='_blank'>Terms of Use</a>
                    </Grid>
                </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit} >
                {submitting && <ButtonSpinner /> || 'Register'}
            </Button>
            {errorMessage && <Error errorMessage={errorMessage} onClose={clearCache} /> || null}
            <Grid className={classes.div}>
                <Typography variant="body2" className={classes.marginTopMedium}>
                    Already have an account?
                </Typography>
                <Grid item xs className={classes.signIn}>
                    <Link
                        to={{
                            pathname: iframe ? 'signin' : '/',
                            state: Object.assign({}, { ...locationState }, { form: 'login' })
                        }}
                        className={classes.linkColor}
                        onClick={clearCache}>
                        Sign In
                    </Link>
                </Grid>
            </Grid>
        </form>
    )
}

const validate = (values) => {

    const errors = {}


    const requiredFields = ['practice_name', 'name', 'email', 'privacy_policy_terms_of_use'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    return errors
}

export default reduxForm({
    form: 'AuthConfirmation',
    validate,
    touchOnChange: true,
})(AuthConfirmationForm);
/**
 *
 *   Custom Filter Form
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Grid, Link, Tooltip, Switch, FormControlLabel, FormGroup, Checkbox } from '@material-ui/core';
import validate from 'utils/validation';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';
import ModalForm from 'components/ModalRecordForm';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { compose } from 'redux';
import moment from 'moment';

const fieldBox = ({ input, label, style, labelPlacement, size, customStyle, handleActiveRecord, name }) => {

    const { value } = input;

    return (
        <div style={style || {}}>
            <FormGroup>
                <FormControlLabel
                    control={
                        <Switch
                            checked={value || false}
                            size={size || false}
                            onClick={(e) => handleActiveRecord({ 'active': e.target.checked }, 'active')}
                            color="primary"
                        />
                    }
                    label={label}
                    labelPlacement={labelPlacement}
                    style={customStyle || {}}
                />
            </FormGroup>
        </div>
    )
}

/**
 * 
 * @param {object} props 
 * @returns 
 */
function CustomFilterForm(props) {
    const { fields, metaData, destroy, columns, handleCreatedDate, name, handleClearFilter, initialCreatedDate, initialActiveRecord, handleActiveRecord, title, handleInActiveRecord, filter, handleFilter, practicesCustomFilter } = props;

    const classes = Styles();

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    const clearBtn = initialActiveRecord && Object.keys(initialActiveRecord).length > 0 && initialActiveRecord.active || initialCreatedDate && Object.keys(initialCreatedDate).length > 0 || filter && filter?.inactive_accounts || false;

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <Grid container>
            <Grid item xs={12} className={classes.itemDiv} >
                <Grid container style={{ display: 'flex', alignItems: 'center' }}>
                    {!filter?.inactive_accounts ? <ModalForm
                        initialValues={initialCreatedDate || {}}
                        title={`Custom Date Filter`}
                        fields={columns.filter(_ => _.editRecord && !_.disableRecord)}
                        form={`createdDate_${name}`}
                        onSubmit={handleCreatedDate.bind(this)}
                        onSubmitClose
                        btnLabel="Submit"
                        metaData={metaData}
                        disableContainer
                        className={classes.flexDiv}>
                        {(open) => {
                            return <Grid item className={classes.createdDate} onClick={open}>
                                {title}: {initialCreatedDate && Object.keys(initialCreatedDate).length > 0 && initialCreatedDate.from_date && initialCreatedDate.to_date && <span className={classes.date}>{`${moment(initialCreatedDate.from_date).format('MM/DD/YY')} - ${moment(initialCreatedDate.to_date).format('MM/DD/YY')} `}</span> || <Tooltip title="Select Custom Date" placement="top-start"><CalendarTodayIcon className={classes.calendarIcon} /></Tooltip>}
                            </Grid>
                        }}
                    </ModalForm> : null}
                    {(fields || []).map((field, index) => {
                        return <Grid key={index} item className={classes.switchBtn}>
                            <Field
                                handleActiveRecord={handleActiveRecord}
                                name={field.value}
                                label={field.label}
                                style={{ display: 'flex' }}
                                metaData={metaData}
                                component={fieldBox}
                                required={field.required}
                                disabled={
                                    field.disableOptons &&
                                    field.disableOptons.edit
                                }
                                {...field}
                            />
                        </Grid>
                    })}
                    {name === 'practices' && !filter?.inactive_accounts ? <Grid className={classes.link} onClick={() => handleInActiveRecord(true, 'inactive_accounts')}>Inactive Accounts</Grid> : null}
                    {name === 'practices' && filter?.inactive_accounts ? <Grid style={{ marginLeft: '12px', marginTop: '2px' }}>Inactive for : </Grid> : null}
                    {name === 'practices' && filter?.inactive_accounts ? <Grid className={classes.dropdownGrid}>
                        <select name={practicesCustomFilter.value} className={classes.filter} defaultValue={!filter?.duration ? 'month' : filter?.duration} onChange={handleFilter}>
                            {((practicesCustomFilter && Array.isArray(practicesCustomFilter.options) && practicesCustomFilter.options.length > 0 && practicesCustomFilter.options)).map((option, i) => <option key={i} disabled={option.disabled} value={option.value} >{option.label}</option>)}
                        </select>
                    </Grid> : null}
                    {clearBtn && <Grid item className={classes.linkBtn}>
                        <Link
                            component="button"
                            variant="body2"
                            underline='always'
                            className={classes.button2}
                            onClick={handleClearFilter}
                        >
                            Clear
                        </Link>
                    </Grid>}
                </Grid>
            </Grid>
        </Grid>
    );
}

CustomFilterForm.propTypes = {
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default compose(
    reduxForm({
        form: 'customFilterForm',
        validate,
        enableReinitialize: true,
        keepDirtyOnReinitialize: true,
        destroyOnUnmount: true,
        touchOnChange: true
    })
)(CustomFilterForm);

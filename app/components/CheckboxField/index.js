
/**
 * 
 * Checkbox Field
 * 
 */

import React from 'react';
import { FormControlLabel, Checkbox } from '@material-ui/core';
import Styles from './styles';

export default function ({ input, id, label, required, styles, note, warning, labelStyle, meta: { touched, error } }) {

    const classes = Styles();
    const setStyles = label == 'Remember Me' ? classes.formControlLabel : null;
    const setFontSize = label != 'Remember Me' ? classes.textSize : null;


    return (<div>
            <FormControlLabel className={setStyles}
                control={<Checkbox
                    id={id}
                    style={styles ? styles : { color: "grey" }}
                    checked={input.value || false}
                    onChange={(e) => input.onChange(e.target.checked)} />}
                label={<span className={setFontSize} style={labelStyle || {}} dangerouslySetInnerHTML={{ __html: label }} />} />
            {note && <div className={classes.textSize}>{note}</div> || null}
            <div className={classes.error}>
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>)
}
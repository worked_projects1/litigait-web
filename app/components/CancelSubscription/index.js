/**
 * 
 * Cancel Subscription
 * 
 */

import React, { useState } from 'react';
import { Grid, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import Skeleton from '@material-ui/lab/Skeleton';
import AlertDialog from '../../components/AlertDialog';
import Styles from './styles';
import moment from 'moment';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function (props) {
    const { record = {}, actions = {}, dispatch, loading, formDetails } = props;
    const [loader, setLoader] = useState(false);
    const [openAlertDialog, setOpenAlertDialog] = useState(false);
    const classes = Styles();
    const subscriptionDetails = record && record.subscription && Array.isArray(record.subscription) && record.subscription.length > 0 && record.subscription.filter(_ => _.subscription_id && !_.cancel_at_period_end) || [];
    const respondingSubscription = subscriptionDetails && subscriptionDetails.filter(e => e.plan_category === 'responding') || false;
    const propoundingSubscription = subscriptionDetails && subscriptionDetails.filter(e => e.plan_category === 'propounding') || false;
    const respondingSubscriptionRecord = respondingSubscription && respondingSubscription[0] && typeof respondingSubscription[0] === 'object' && Object.keys(respondingSubscription[0]).length > 0 && respondingSubscription[0] || false;
    const propoundingSubscriptionRecord = propoundingSubscription && propoundingSubscription[0] && typeof propoundingSubscription[0] === 'object' && Object.keys(propoundingSubscription[0]).length > 0 && propoundingSubscription[0] || false;

    const isPropoundingSubscriptions = propoundingSubscriptionRecord && propoundingSubscriptionRecord && propoundingSubscriptionRecord.subscription_id && !propoundingSubscriptionRecord.cancel_at_period_end || false;

    const confirmationMessage = respondingSubscriptionRecord && isPropoundingSubscriptions ? `Do you want to cancel auto-renewal for this practice? This account is pre-paid for ${respondingSubscriptionRecord.plan_category} & ${propoundingSubscriptionRecord.plan_category} till ${moment.unix(respondingSubscriptionRecord.current_period_end).format('MM/DD/YYYY')} - users in this practice can continue to use the service till this date.` || `Do you want to cancel auto-renewal for this practice?` : respondingSubscriptionRecord && `Do you want to cancel auto-renewal for this practice? This account is pre-paid for ${respondingSubscriptionRecord.plan_category} till ${moment.unix(respondingSubscriptionRecord.current_period_end).format('MM/DD/YYYY')} - users in this practice can continue to use the service till this date.` || `Do you want to cancel auto-renewal for this practice?`;

    const handleCancelSubscription = (type) => {
        setLoader(type);
        dispatch(actions.practiceAdminCancelSubscription(Object.assign({}, { subscriptionData: subscriptionDetails, id: record.id }), respondingSubscriptionRecord, setLoader, actions.loadRecord));
    }

    return (<Grid container className={classes.root}>
        <Grid item xs={12}>
            <Grid container direction="row">
                <Grid item xs={6}>
                    <div>
                        <div className={classes.label}>PRACTICE NAME:</div>
                        <p className={classes.value}>{loading ? <Skeleton animation="wave" width={80} /> : record.name || '-'}</p>
                    </div>
                </Grid>
                <Grid item xs={6} style={{ textAlign: 'right' }}>
                    {loading ? <Button type="button" variant="contained" className={classes.Button}>
                        <Skeleton animation="wave" width={80} />
                    </Button> : subscriptionDetails && subscriptionDetails.length > 0 && <Button type="button" variant="contained" className={classes.Button} onClick={() => setOpenAlertDialog(true)} >
                        {loader == 'dialog' ? <ButtonSpinner color='#2ca01c' /> : 'Cancel Subscription'}
                    </Button>}
                </Grid>
                <AlertDialog
                    description={confirmationMessage}
                    openDialog={openAlertDialog}
                    closeDialog={() => setOpenAlertDialog(false)}
                    onConfirm={() => handleCancelSubscription('dialog')}
                    onConfirmPopUpClose={true}
                    btnLabel1='YES'
                    btnLabel2='NO' />
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <hr className={classes.hr} />
        </Grid>
    </Grid>)
}
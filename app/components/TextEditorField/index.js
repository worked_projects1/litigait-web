/**
*
* Text Editor Field
*
*/

import React, { useEffect, useState } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import Styles from './styles';


export default function ({ input, label, onInputChange, style, memo, placeholder, errorText, maxLength, meta: { touched, error, warning } }) {

    const classes = Styles();
    const { value, onChange } = input;
    const [initvalue, setInitValue] = useState(value);

    const htmlRegExp = /<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g;
    const regxExp = /(<p[^>]+?>|<p>|<\/p>)/img;
    let initialChar = initvalue && initvalue.replace(htmlRegExp, '');
    const [charCount, setCharCount] = useState(initialChar && initialChar.length || false);

    useEffect(() => {
        let mount = true;
        setInitValue(value);
        return () => mount = false;
    }, [value])


    const handleCharCounter = (count) => {
        setCharCount(count);
    }

    const editorProps = process.env.NODE_ENV !== 'development' ? {
        tinymceScriptSrc: '/tinymce/tinymce.min.js'
    } : {
        apiKey: '7ys21v9wrbugm5rxe361937m4ipnfhuk6rltnjj5z47f12po'
    };

    return (
        <div style={style || {}}>
            {label && <label>{label}</label> || null}
            <Editor
                value={initvalue || ''}
                {...editorProps}
                init={{
                    selector: 'textarea',
                    placeholder: placeholder || null,
                    content_style: 'body { font-size: 14px; font-family: Times New Roman;} p { margin: 0; }',
                    statusbar: false,
                    height: 320,
                    menubar: false,
                    toolbar:
                        'fontselect bold italic underline forecolor backcolor',
                    fontsize_formats: "8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px",
                    font_formats: 'Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Avenir Regular = Avenir-Regular; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats',
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor textcolor',
                        'textcolor colorpicker',
                        'link image code',
                        'paste'
                    ],
                    color_map: [
                        '#ffff00', 'yellow',
                        '#00ff00', 'green',
                        '#00ffff', 'cyan',
                        '#ff00ff', 'magenta',
                        '#0000ff', 'blue',
                        '#ff0000', 'red',
                        '#000080', 'darkBlue',
                        '#008080', 'darkCyan',
                        '#008000', 'darkGreen',
                        '#800080', 'purple',
                        '#800000', 'darkRed',
                        '#808000', 'darkYellow',
                        '#808080', 'darkGrey',
                        '#c0c0c0', 'lightGrey'
                    ],
                    color_cols: 5,
                    custom_colors: false,
                    paste_as_text: true,
                    branding: false,
                    contextmenu: false,
                    browser_spellcheck: true,
                    setup: (ed) => {
                        const allowedKeys = [8, 16, 17, 18, 20, 33, 34, 35, 36, 37, 38, 39, 40, 46];
                        ed.on('keydown', function (e) {
                            const editorText = tinymce.activeEditor.getContent();
                            const contentLength = editorText && editorText.replace(htmlRegExp, '').length;

                            if (allowedKeys.indexOf(e.keyCode) != -1 || ((e.ctrlKey || e.metaKey) && e.keyCode == 67) || ((e.ctrlKey || e.metaKey) && e.keyCode == 88)) return true;
                            if (contentLength >= maxLength) {
                                e.preventDefault();
                                e.stopPropagation();
                                return false;
                            }
                            return true;
                        });

                        ed.on('keyup', function (e) {
                            const editorText = tinymce.activeEditor.getContent();
                            const contentLength = editorText && editorText.replace(htmlRegExp, '').length;

                            if (allowedKeys.indexOf(e.keyCode) != -1) return true;
                            if (contentLength < maxLength) {
                                handleCharCounter(contentLength);
                            }
                        });

                        ed.on('contextmenu', function (e) {
                            const editorText = tinymce.activeEditor.getContent();
                            const contentLength = editorText && editorText.replace(htmlRegExp, '').length;

                            if (contentLength > maxLength) {
                                e.preventDefault();
                                e.stopPropagation();
                                return false;
                            } else {
                                handleCharCounter(contentLength);
                                return true;
                            }
                        });
                    },
                    paste_preprocess: function (plugin, args) {
                        const editorText = tinymce.activeEditor.getContent();
                        const contentLength = editorText && editorText.replace(htmlRegExp, '').length;

                        let argsContent = args.content.replace(regxExp, '');
                        if (contentLength + argsContent.length > maxLength) {
                            args.content = '';
                        }
                        handleCharCounter(contentLength + args.content.length);
                    }
                }}
                onEditorChange={(newValue, editor) => {
                    setInitValue(newValue);
                    if (memo) {
                        onChange(newValue);
                        const simpleText = newValue && newValue.replace(htmlRegExp, '');
                        handleCharCounter(simpleText && simpleText.length);
                    }

                    if (onInputChange)
                        onInputChange(newValue);

                }}
                onBlur={() => !memo ? onChange(initvalue) : null}

            />
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
            {charCount && charCount > 0 && charCount >= maxLength && <div className={classes.error}>{errorText}</div> || null}
        </div>

    )
}

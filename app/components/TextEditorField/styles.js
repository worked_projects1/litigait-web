

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  textSize: {
    fontSize: '14px',
  },
  error: {
    fontSize: '14px',
    color: 'red'
  }
}));


export default useStyles;

/***
 *
 * Stripe Field
 *
 */


import React from 'react';
import Styles from './styles';
import { CardElement } from '@stripe/react-stripe-js';


export default function ({ input, label, autoFocus, type, disabled, prefix, meta: { touched, error, warning }, showRequired }) {

  const classes = Styles();
  const { onChange } = input;
  const [errorMessage, setErrorMessage] = React.useState(false);

  const CARD_OPTIONS = {
    iconStyle: 'solid',
    style: {
      base: {
        iconColor: '#2ca01c',
        color: 'black',
        fontWeight: 500,
        fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',
        ':-webkit-autofill': {
          color: 'black',
        },
        '::placeholder': {
          color: 'grey',
        },
      },
      invalid: {
        iconColor: '#F20056',
        color: 'black',
      },
    },
  };

  return (
    <div className={classes.stripeField}>
      <CardElement
        options={CARD_OPTIONS}
        onChange={(e) => {
          setErrorMessage(e.error && e.error.message);
          onChange(e)
        }
        } />
      <div className={classes.error}>{(showRequired && ((error && <span>{error}</span>)
        || (warning && <span>{warning}</span>))) || errorMessage}</div>
    </div>

  )
}



import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  stripeField: {
    marginTop: '20px'
  },
  error: {
    fontSize: '14px',
    color: 'red',
    paddingTop: '2px',
    marginTop: '15px',
    borderTop: '1px solid #2ca01c'
  }
}));


export default useStyles;
import React, { useState } from 'react';
import Styles from './styles';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import moment from 'moment';

const customTheme = createMuiTheme({
  overrides: {
    MuiPickersBasePicker: {
      pickerView: {
        maxWidth: '245px',
        minWidth: '200px',
        minHeight: '205px'
      }
    },
    MuiPickersDay: {
      day: {
        margin: '0px',
        width: '25px',
        height: '25px'
      },
      daySelected: {
        backgroundColor: '#2CA01C !important'
      }
    },
    MuiTypography: {
      fontFamily: 'Avenir-Regular',
      body1: {
        fontSize: '14px'
      },
      body2: {
        fontSize: '11px',
      },
      caption: {
        fontSize: '11px'
      }
    },
    MuiPickersCalendarHeader: {
      dayLabel: {
        margin: '0px',
        width: '25px'
      }
    },
    MuiPickersCalendar: {
      transitionContainer: {
        minHeight: '140px'
      }
    },
    MuiFormControl: {
      marginNormal: {
        marginTop: '0px',
        marginBottom: '0px'
      }
    },
    MuiIconButton: {
      root: {
        padding: '0px'
      }
    },
    MuiButton: {
      containedPrimary: {
        backgroundColor: '#2ca01c !important',
        '&:disabled': {
          backgroundColor: '#2ca01c9c !important',
          color: 'white !important'
        }
      }
    }
  }
});

function Calendar(props) {
  const { input, label, dateFormat, className, minDate, maxDate } = props
  const { name, value, onChange } = input;
  const [open, setOpen] = useState(false)
  const [shrink, setShrink] = useState(false)
  const classes = Styles();

  return (
    <div>
      <ThemeProvider theme={customTheme}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            className={classes.fieldColor}
            name={name}
            fullWidth
            disableToolbar={true}
            allowKeyboardControl={true}
            autoOk={true}
            open={open}
            onClick={() => setOpen(true)}
            onClose={() => setOpen(false)}
            format={dateFormat || 'MM/dd/yyyy'}
            margin="normal"
            minDate={minDate || undefined}
            maxDate={maxDate || undefined}
            minDateMessage={false}
            variant='inline'
            value={value}
            label={<span className={className ? className : classes.textSize} >{label}</span>}
            onChange={(val) => onChange(val)}
            onSelect={() => setShrink(true)}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
            InputLabelProps={{
              shrink: value ? true : shrink,
            }}
            InputProps={{
              readOnly: true,
              classes: {
                input: className ? className : classes.textSize,
              }
            }}
            PopoverProps={{
              anchorOrigin: { horizontal: "center", vertical: "left" },
              transformOrigin: { horizontal: "center", vertical: "left" }
            }}
            error={false}
            helperText={null}
            keyboardIcon={<CalendarTodayIcon className={classes.calendarIcon} onClick={() => setOpen(true)} />}
          />
        </MuiPickersUtilsProvider>
      </ThemeProvider>
    </div>
  );
}

export default Calendar;

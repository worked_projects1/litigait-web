
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    
  calendarIcon: {
    width: '22px',
    height: '22px',
    display: 'none'
  },  
  textSize: {
    fontSize: '14px',
    fontFamily: 'Avenir-Regular'
  },
  fieldColor: {
    width: '100%',
    fontSize: '10px',
    '& :after': {
      borderBottomColor: '#2ca01c',
      fontSize: '10px',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
      fontSize: '10px',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
      fontSize: '10px',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
      fontSize: '10px',
    },
  }
}));

export default useStyles;

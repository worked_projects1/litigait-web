import React, { useState } from 'react';
import { Document, Page } from 'react-pdf';
import Styles from './styles';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { pdfjs } from 'react-pdf';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${
  pdfjs.version
}/pdf.worker.js`;

function PdfViewer({ pdf, view, onChange }) {
  const [pageNumber, setPageNumber] = useState(1);
  const classes = Styles();

  const onDocumentLoadSuccess = ({ numPages }) => {
    onChange(Object.assign({}, { file_name: pdf.name, numPages }));
    setPageNumber(1);
  };

  return (
    <Grid container className={view ? classes.view : null}>
      <Document
        file={pdf}
        options={{ workerSrc: '/pdf.worker.js' }}
        onLoadSuccess={onDocumentLoadSuccess}
      >
        <Page pageNumber={pageNumber} />
      </Document>
    </Grid>
  );
}

PdfViewer.propTypes = {
  title: PropTypes.string,
  children: PropTypes.func,
  error: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
};

export default PdfViewer;

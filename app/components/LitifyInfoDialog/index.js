/**
 * 
 * Litify Info Dialog
 * 
 */

import React from "react";
import { Grid, Button, Dialog, DialogActions, DialogContent, DialogContentText } from '@material-ui/core';
import Styles from './styles';

function LitifyInfoDialog(props) {
    const classes = Styles();
    const { actions, dispatch, litifyInfoDialog } = props;
    const { error_from, case_type } = litifyInfoDialog;

    const litifyOpen = case_type && Array.isArray(case_type) && case_type?.length > 0 ? true : false;
    let caseTypeName = litifyOpen && case_type && '"' + case_type.join('", "') + '"';
    const stepsArr = caseTypeName && [`Log in to your Litify account`, `Go to the "Case Type" section`, `Click on ${caseTypeName}`, `Ensure that the "is Available" checkbox is checked`, `Save the changes`]

    return (
        <div>
            <Dialog
                open={litifyOpen || false}
                onClose={() => dispatch(actions.litifyInfoDialog(false))}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogContent>
                    <DialogContentText id="alert-dialog-description" className={classes.description}>
                        <>
                            <Grid item xs={12}>
                                {`Sorry! We are unable to update the ${error_from || ''} information due to insufficient permissions. If you would like to grant us permission to make updates, please follow the below steps and try again.`}
                            </Grid><br />
                            <Grid container spacing={1} className={classes.container}>
                                {stepsArr && stepsArr.map((el, index) => (
                                    <Grid item key={index} xs={12} className={classes.steps}>
                                        <img className={classes.checkIcon} src={require('images/icons/checked.png')} alt="check icon" />
                                        <Grid className={classes.text}>{el}</Grid>
                                    </Grid>
                                ))}
                            </Grid>
                        </>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => dispatch(actions.litifyInfoDialog(false))} color="primary" className={classes.button}>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    )
}

export default LitifyInfoDialog;
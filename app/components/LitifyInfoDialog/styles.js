import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    button: {
        color: '#2DA01D',
        fontWeight: 'bold',
        fontFamily: 'Avenir-Bold',
    },
    description: {
        color: '#000000',
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    checkIcon: {
        width: "20px",
        height: "20px",
        marginRight: "15px",
        color: '#2ca01c'
    },
    container: {
        padding: "0px 10px",
        fontSize: '15px'
    },
    steps: {
        display: "flex",
        alignItems: "center"
    },
    text: {
        marginTop: '4px'
    }
}));


export default useStyles;
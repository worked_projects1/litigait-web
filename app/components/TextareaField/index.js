
/***
 *
 * Textarea Field
 *
 */


import React from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';


export default function ({ input, label, autoFocus, type, disabled, prefix, rows, className, placeholder, variant, style, defaultBlur, conditionalBlur, charLimit, meta: { touched, error, warning } }) {

    const classes = Styles();
    const { id, name, value, onChange } = input;

    const InputChange = defaultBlur ? Object.assign({}, {
        onBlur: (e) => onChange(e.target.value),
        defaultValue: value || ''
    }) : Object.assign({}, {
        onChange: (e) => onChange(e.target.value),
        value: value || ''
    });


    return (
        <div style={style || {}}>
            {!variant && <label>{label}</label> || null}
            <TextField
                className={className || classes.fieldColor}
                multiline
                name={name}
                id={id}
                label={variant && label || ''}
                variant={variant || 'filled'}
                placeholder={placeholder}
                type={"input"}
                rows={rows || 6}
                rowsMax={18}
                fullWidth
                onClick={(e) => {
                    if(conditionalBlur) {
                        e.target.blur()
                    }
                }}
                onKeyPress={e => {
                    if(charLimit && value && value.length >= charLimit) {
                        e.preventDefault();
                    }
                }}
                onPasteCapture={e => {
                    if(charLimit) { 
                        e.preventDefault();
                    }
                }}
                {...InputChange}
                autoFocus={autoFocus}
                disabled={disabled}
                InputProps={{
                    classes: {
                        input: classes.textSize,
                        padding: 0,
                    },

                }}
            />
            <div className={classes.error} style={variant && { color: '#ffff' } || {}}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>

    )
}



import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4),
  },
  inputField: {
    background: '#ffff',
    width: '275px',
    marginTop: '5px',
    fontFamily: 'ClearSans-Light !important',
    '& .MuiInputLabel-outlined > span': {
      fontFamily: 'ClearSans-Light',
      color: '#0a0a0a'
    },
    '& .MuiOutlinedInput-root': {
      height: '40px'
    },
    '& .MuiInputLabel-outlined': {
      marginTop: '-8px'
    },
    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: '#ffff'
    },
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: '#ffff'
    },
    '& .Mui-focused > span': {
      color: '#ffff !important',
      marginLeft: '-16px',
      fontSize: '16px'
    },
    '& .MuiOutlinedInput-notchedOutline': {
      height:'0px'
    },
    '& .MuiInputBase-root > fieldset': {
      border: 'none'
    }

  },
  submit: {
    width: '275px',
    marginTop: '15px',
    backgroundColor: '#ffff !important',
    color: '#e95d0c !important',
    boxShadow: 'none',
    border: '2px solid transparent',
    fontFamily: 'clearSans-Regular',
    letterSpacing: '2px',
    fontSize: '14px',
    textTransform: 'none',
    marginBottom: '15px',
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      backgroundColor: '#2ca01c',
      boxShadow: 'none',
    }
  },
  text: {
    color: '#fff',
    fontFamily: 'clearSans-Light',
    letterSpacing: '1px',
    marginBottom: '10px',
    "& > h4": {
      width: '275px',
      fontSize: '11px',
      fontWeight: 'initial',
      textAlign: 'center'
    }
  },
  demo: {
    color: '#fff',
    fontFamily: 'clearSans-Light',
    textAlign: 'center',
    letterSpacing: '5px',
    "& > h1": {
      width: '275px',
      fontSize: '22px',
      marginBottom: '0px'
    }
  },
  close: {
    width: '17px',
    position: 'relative',
    top: '-30px',
    cursor: 'pointer'
  }
}));


export default useStyles;
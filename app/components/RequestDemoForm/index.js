
/**
 * 
 * Request Demo Form 
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';

import { Grid, Button } from '@material-ui/core';

import InputField from 'components/InputField';
import PhoneNumberField from 'components/PhoneNumberField';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from '../Error';
import Success from '../Success';

import Styles from './styles';
import history from 'utils/history';
import { normalize } from 'utils/tools';

function RequestDemoForm({ handleSubmit, submitting, success, error, destroy, change, locationState, clearCache }) {

    const classes = Styles();

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Grid container direction="row" alignItems="start">
                    <img src={require('images/home/close.png')} className={classes.close} onClick={() => {
                        history.push({
                            pathname: '/',
                            state: Object.assign({}, { ...locationState }, { form: false })
                          })
                    }}/>
            </Grid>
            <Grid container spacing={2} alignItems="center" direction="column">
                <Grid item xs={12}>
                    <img src={require('images/home/logo2.png')} style={{ width: '85px' }} />
                </Grid>
                <Grid item xs={12} className={classes.demo}>
                    <h1>REQUEST A DEMO</h1>
                </Grid>
                <Grid item xs={12} className={classes.text}>
                    <h4>Allow us to show you exactly how much time you will save with EsquireTek.</h4>
                </Grid>
                <Grid item xs={12}>
                    <Field name="first_name" label="First Name*" component={InputField} type="text" variant="outlined" className={classes.inputField} errorStyle={{ color: '#ffff' }} required />
                </Grid>
                <Grid item xs={12}>
                    <Field name="last_name" label="Last Name*" component={InputField} type="text" variant="outlined" className={classes.inputField} errorStyle={{ color: '#ffff' }} required />
                </Grid>
                <Grid item xs={12}>
                    <Field name="email" label="Email*" component={InputField} type="text" variant="outlined" className={classes.inputField} errorStyle={{ color: '#ffff' }} required />
                </Grid>
                <Grid item xs={12}>
                    <Field name="phone" label="Phone Number*" component={PhoneNumberField} type="text" variant="outlined" className={classes.inputField} errorStyle={{ color: '#ffff' }} normalize={normalize({max: 14})} required />
                </Grid>
                <Grid item xs={12}>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        className={classes.submit} >
                        {submitting && <ButtonSpinner color="#e95d0c" /> || 'Request Demo'}
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    {error && <Error errorMessage={error} style={{ width: '275px', marginBottom: '15px' }} onClose={() => change('_error', null)} /> || success && success.requestDemoForm && <Success successMessage={success.requestDemoForm} style={{width: '275px', marginBottom: '15px'}} onClick={clearCache} /> || null}
                </Grid>
            </Grid>
        </form>
    )
}

const validate = (values) => {

    const errors = {}

    const requiredFields = ['first_name', 'last_name', 'email', 'phone'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
    ) {
        errors.email = 'Invalid Email'
    }

    return errors
}

export default reduxForm({
    form: 'requestDemo',
    validate,
    touchOnChange: true,
})(RequestDemoForm);
/**
*
* TableWrapper
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import SpicyDatatable from 'spicy-datatable';
import Grid from '@material-ui/core/Grid';
import Skeleton from '@material-ui/lab/Skeleton';

class TableWrapper extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        records: PropTypes.array,
        columns: PropTypes.array,
        children: PropTypes.bool,
        path: PropTypes.string,
        locationState: PropTypes.object,
        view: PropTypes.bool
    };


    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    render() {
        const { name, records, columns, children, path, locationState = {}, view, pageLimit, sm, metaData = {}, tableKey, loading } = this.props;
        const activePath = location.pathname;
        const tableColumns = children ? columns.filter((column) => column.visible && column.viewMode) : columns.filter((column) => column.visible);


        let rows = records.map((record) => Object.assign(
            {},
            record, {
            onClickHandler: view ? this.handleClick.bind(this, record, locationState) : null,
            isActive: activePath === `${path}/${record.id}` || activePath === `${path}/${record.id}/edit`,
        }),
        );

        tableColumns.forEach((column) => {
            switch (column.type) {
                case 'upload':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                loading ? <Skeleton animation="wave" /> :
                                    column.html ? column.html(row, metaData) :
                                        <img
                                            src={`${row[column.value] || ''}`}
                                            role="presentation" style={{ height: 64, padding: 4 }}
                                        />,
                        },
                    ));
                    break;
                case 'download':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                loading ? <Skeleton animation="wave" /> :
                                    column.html ? column.html(row, metaData) :
                                        row[column.value]
                        },
                    ));
                    break;
                case 'checkbox':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                loading ? <Skeleton animation="wave" /> :
                                    column.html ? column.html(row, metaData) :
                                        row[column.value] && 'Yes' || 'No',
                        },
                    ));
                    break;
                case 'select':
                case 'multiSelect':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                loading ? <Skeleton animation="wave" /> :
                                    column.html ? column.html(row, metaData) :
                                        row[column.value],
                        },
                    ));
                    break;
                default:
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                loading ? <Skeleton animation="wave" /> :
                                    column.html ? column.html(row, metaData) :
                                        column.limit && row[column.value] && row[column.value].length > 60 ? `${row[column.value].toString().substring(0, 60)}...` :
                                            row[column.value] != null ? row[column.value] : '' || '',
                        },
                    ));
                    break;
            }
        });

        return (
            <Grid container>
                <Grid item xs={12} className={sm ? name : null}>
                    <SpicyDatatable
                        tableKey={tableKey || `${path}_${Math.random()}`}
                        config={{ itemsPerPageOptions: pageLimit || [25, 50, 100] }}
                        columns={tableColumns.map((column) => Object.assign(
                            {},
                            { key: column.value, label: column.sort ? column.label + '⇅' : column.label, sort: column.sort }
                        ))}
                        rows={rows}
                    />
                </Grid>
            </Grid>
        );
    }

    handleClick(record, data, el) {
        const { id } = record || {};
        const targetId = el && el.target && el.target.id || false;
        const { path, history, locationState, schemaId, loading } = this.props;
        if ((id && !targetId && !loading) || (id && targetId && targetId !== id && !loading))
            history.push({
                pathname: ['/cases', '/rest/medical-history-summery'].includes(path) ? `${path}/${id}/form` : `${path}/${id}`,
                state: ['/cases'].includes(path) ? Object.assign({}, { ...locationState }, { id, caseRecord: record }) : schemaId ? Object.assign({}, { ...locationState }, { id, schemaId }) : Object.assign({}, { ...locationState }, { id })
            });
    }


}

export default TableWrapper;

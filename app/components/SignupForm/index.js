
/**
 * 
 * Signup Form
 * 
 */


import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { Grid, Button, Typography } from '@material-ui/core';

import InputField from 'components/InputField';
import ButtonSpinner from 'components/ButtonSpinner';
import IntegrationButtons from 'components/IntegrationButtons';
import Error from '../Error';

import Styles from './styles';


function SignupForm({ handleSubmit, submitting, errorMessage, clearCache, destroy, oauthIntegration, iframe, locationState = {} }) {

    const classes = Styles();
    const { google, microsoft } = oauthIntegration;

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <>
            <form onSubmit={handleSubmit} className={classes.form} noValidate >
                <Grid container direction="row" justify="center" alignItems="center" style={{ marginBottom: '20px' }}>
                    <a href={process.env.ENV === 'production' ? `https://www.esquiretek.com/` : `https://staging.esquiretek.com/`} >
                        <img src={require('images/icons/new_logo.jpg')} style={{ width: '80%' }} />
                    </a>
                </Grid>
                <Grid item xs={12} className={classes.div}>
                    <Grid>
                        <Typography variant="body2" gutterBottom component="span"><b>{'Create a Free Account'}</b></Typography>
                    </Grid>
                    <Typography variant="body2" gutterBottom component="span">{'Automate discovery and modernize your law firm.'}</Typography>
                </Grid>
                <IntegrationButtons
                    googleBtnLabel={'Sign-up with Google'}
                    microsoftBtnLabel={'Sign-up with Microsoft'}
                    googleAuth={google}
                    microsoftAuth={microsoft} />
                <Grid className='divider' style={{marginTop: '20px'}}>
                    <span>OR</span>
                </Grid>
                <Grid item xs={12} className={classes.signupTitle}>
                    <b>Sign up with email</b>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Field name="email" label="Email" component={InputField} type="text" required />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit} >
                    {submitting && <ButtonSpinner /> || 'Get Started for Free'}
                </Button>
                {errorMessage && <Error errorMessage={errorMessage} onClose={clearCache} /> || null}
                <Grid item xs={12} className={classes.div}>                    
                    <Typography variant="body2" className={classes.marginTopMedium}>
                        By continuing, you agree to our
                    </Typography>
                    <Grid>
                        <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/terms.html' style={{ color: '#0077c5' }} target='_blank'>Terms & Conditions</a> and <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/privacy.html' style={{ color: '#0077c5' }} target='_blank'>Privacy Policy</a>
                    </Grid>
                </Grid>                
                <Grid className={classes.div}>
                    <Typography variant="body2" className={classes.marginTopMedium}>
                        Already have an account?
                    </Typography>
                    <Grid item xs className={classes.signIn}>
                        <Link
                            to={{
                                pathname: iframe ? 'signin' : '/',
                                state: Object.assign({}, { ...locationState }, { form: 'login' })
                            }}
                            className={classes.linkColor}
                            onClick={clearCache}>
                            Sign In
                        </Link>
                    </Grid>
                </Grid>
            </form>
        </>
    )
}

const validate = (values) => {

    const errors = {}

    const requiredFields = ['email'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
    ) {
        errors.email = 'Invalid Email'
    }

    return errors
}

export default reduxForm({
    form: 'signup',
    validate,
    touchOnChange: true,
})(SignupForm);
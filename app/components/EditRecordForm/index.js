/**
 * 
 * Edit Record Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import validate from 'utils/validation'
import ButtonSpinner from 'components/ButtonSpinner';
import AlertDialog from 'components/AlertDialog';
import Error from '../Error';
import { normalize, disableOnSubmitForm } from 'utils/tools';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function editRecordForm(props) {

    const classes = Styles();
    const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, confirmButton, confirmMessage, btnLabel, invalid, destroy, spinner, cancelBtn, updateBtn, submitBtn, enableSubmitBtn, form, AlertBtnLabel1, AlertBtnLabel2, updatePracticeUserBtn } = props;
    const forms = ['serving_pos_date', 'opposing_counsel', 'serving_attorney', 'propounder_opposing_counsel'];
    const posForms = ['opposing_counsel', 'propounder_opposing_counsel'];

    const handleSubmitRecord = (e) => {
        const isDisableOnSubmit = disableOnSubmitForm.find(e => form.startsWith(e));
        if(isDisableOnSubmit) {
            e.preventDefault();
        } else {
            handleSubmit(e);
        }
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    return (<div>
        <form onSubmit={(e) => handleSubmitRecord(e)} className={forms.includes(props.form) ? classes.forms : classes.form} noValidate >
            <Grid container spacing={3}>
                {(fields || []).map((field, index) => {
                    const InputComponent = ImplementationFor[field.type];
                    return <Grid key={index} item xs={12} style={field.style || null}>
                        {field.type !== 'fieldArray' ?
                            <Field
                                name={field.value}
                                label={field.label}
                                type="text"
                                metaData={metaData}
                                component={InputComponent}
                                required={field.required}
                                disabled={field.disableOptons && field.disableOptons.edit}
                                normalize={normalize(field)}
                                {...field} /> : 
                                <FieldArray
                                    name={field.value}
                                    label={field.label}
                                    type="text"
                                    fieldArray={field.fieldArray}
                                    metaData={metaData}
                                    component={InputComponent}
                                    required={field.required}
                                    normalize={normalize(field)}
                                    ImplementationFor={ImplementationFor}
                                    disabled={field.disableOptons && field.disableOptons.edit}
                                    {...field} />}
                    </Grid>
                })}
                <Grid item xs={12}>
                    {error && <Error errorMessage={error} /> || ''}
                </Grid>
            </Grid>
            <Grid className={classes.footer}>
                {updatePracticeUserBtn && typeof updatePracticeUserBtn === 'function' ? React.createElement(updatePracticeUserBtn, Object.assign({}, { ...props, classes, btnLabel: 'Update' })) : submitBtn && typeof submitBtn === 'function' ? React.createElement(submitBtn, Object.assign({}, { ...props, classes })) : confirmButton ? <AlertDialog
                        description={confirmMessage}
                        onConfirm={() => handleSubmit()}
                        onConfirmPopUpClose={true}
                        btnLabel1={AlertBtnLabel1 || 'Yes'}
                        btnLabel2={AlertBtnLabel2 || 'No'} >
                        {(open) => <Button
                            type="button"
                            disabled={posForms.includes(props.form) ? (pristine || submitting) : !enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid)}
                            variant="contained"
                            onClick={!invalid ? open : null}
                            color="primary"
                            className={classes.submitBtn}>
                            {(submitting || spinner) && <ButtonSpinner /> || btnLabel || 'submit'}
                        </Button>}
                    </AlertDialog> :
                <Button
                    type="submit"
                    disabled={posForms.includes(props.form) ? (pristine || submitting) : !enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid)}
                    variant="contained"
                    color="primary"
                    className={updateBtn ? updateBtn : classes.submitBtn}>
                    {(submitting || spinner) && <ButtonSpinner /> || btnLabel || 'Update'}
                </Button>}
                {cancelBtn && React.createElement(cancelBtn) ||
                    <Link to={path && locationState && { pathname: path, state: { ...locationState } } || null}>
                        <Button
                            type="button"
                            variant="contained"
                            color="primary"
                            className={classes.cancelBtn}>
                            Cancel
                        </Button>
                    </Link>}
            </Grid>
        </form>
    </div>)

}


export default reduxForm({
    form: 'EditRecord',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
})(editRecordForm);
/**
 * 
 * Inline Record Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm, change } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import validate from 'utils/validation'
import ButtonSpinner from 'components/ButtonSpinner';
import Snackbar from 'components/Snackbar';
import { normalize } from 'utils/tools';

function InlineRecordForm(props) {

    const classes = Styles();
    const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, name, invalid, destroy, change, btnStyle } = props;
    
    useEffect(() => {
        return () => destroy();
    }, []);

    return (<form onSubmit={handleSubmit} className={classes.form} noValidate >
        <Grid container spacing={3}>
            {(fields || []).map((field, index) => {
                const InputComponent = ImplementationFor[field.type];
                return <Field
                    key={index}
                    name={field.value}
                    label={field.label}
                    type="text"
                    metaData={metaData}
                    component={InputComponent}
                    required={field.required}
                    disabled={field.disableOptons && field.disableOptons.edit}
                    normalize={normalize(field)}
                    onChange={(val, temp, temp1, name) => {
                        if (name === 'assigned_to') {
                            change('status', 'Assigned');
                        }
                    }}
                    {...field} />
            })}
            <Grid xs={12} sm={"auto"} className={btnStyle || null}>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={pristine || submitting}
                    className={classes.submitBtn}>
                    {submitting && <ButtonSpinner /> || 'Update'}
                </Button>
            </Grid>
            <Snackbar show={error ? true : false} text={error} severity={'error'} />
        </Grid>
    </form>)

}


export default reduxForm({
    form: 'InlineRecordForm',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
})(InlineRecordForm);
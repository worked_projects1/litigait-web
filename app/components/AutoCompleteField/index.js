/**
 * 
 * Auto Complete Field
 * 
 */


import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Styles from './styles';
import { FormControl, TextField } from '@material-ui/core';

export default function ({ input, label, required, placeholder, metaData, options, disabled, style, asterisks, formFieldChange, formName, meta: { touched, error, warning } }) {
    const classes = Styles();

    const { id, name, value, onChange } = input;
    const isPreDefinedSet = Array.isArray(options);
    const Options = isPreDefinedSet ? options : metaData[options] || [];
    const requiredInput = asterisks ? { required: true } : { required: false };

    const handleChange = (value) => {
        onChange(value);
        if(formName && formName.indexOf('documentDetailForm') > -1 || formName?.indexOf('clientCaseInfoColumns_') > -1) {
            if(name == 'state') {
                formFieldChange && formFieldChange('document_type', '');
            }
        }
    }

    return (
        <div className={classes.selectField} style={style || {}}>
            <FormControl className={classes.formControl}>
                <Autocomplete
                    id={id}
                    name={name}
                    options={Options}
                    getOptionLabel={(option) => option.label || ''}
                    getOptionDisabled={(option) => option.disabled || false}
                    value={value && Options.find(_ => _.value.toString() === value.toString()) || {}}
                    autoComplete
                    includeInputInList
                    disabled={disabled}                     
                    onChange={(e, target) => target && target.value && handleChange(target.value) || value}
                    ListboxProps={{className:classes.bodyScroll}}
                    renderInput={(params) => (
                        <TextField
                            {...params}                             
                            variant="standard"
                            className={classes.fieldColor}
                            label={label}
                            {...requiredInput}
                            placeholder={placeholder}
                        />
                    )}
                />
            </FormControl>
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>
    )
}


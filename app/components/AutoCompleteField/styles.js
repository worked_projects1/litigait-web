

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%'
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
  fieldColor: {
    '& label': {
      fontSize: '14px'
    },
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.MuiFormLabel-root.Mui-focused': {
      borderColor: '#2ca01c',
      color: '#2ca01c'
    }
  },
  selectField: {
    '& .MuiInput-underline:after': {
      borderBottomColor: '#2ca01c',
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: '#2ca01c',
    }

  },
  bodyScroll: {
    maxHeight: 250,
    overflowY: 'auto',
    paddingLeft: '0px',
    margin: '8px 0 8px 0',
    overflowX: 'hidden',
    "&::-webkit-scrollbar": {
        width: 10,
    },
    "&::-webkit-scrollbar-track": {
        boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
        borderRadius: '30px'
    },
    "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#2ca01c",
        borderRadius: '30px'
    }
  },
  mandatory: { 
    color: 'red', 
    fontSize: '16px' 
  }
}));


export default useStyles;
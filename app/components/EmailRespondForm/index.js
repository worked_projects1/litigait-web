/**
 * 
 * Email Respond Forms
 * 
 */

import React, { useState, useRef } from 'react';
import Styles from './styles';
import { Checkbox, Grid, Typography, Tooltip, TextField, InputAdornment, IconButton } from '@material-ui/core';
import { respondingEmailDocumentFileName } from 'utils/tools';
import DocumentEditor from 'components/DocumentEditor';
import ClipLoader from 'react-spinners/ClipLoader';
import lodash from 'lodash';
import Icons from 'components/Icons';
import { download as downloadDocument } from 'utils/tools';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectUser } from 'blocks/session/selectors';


function EmailRespondForm(props) {
    const { inputArr, options, recordProps, handleChange, documentStatus, user } = props;
    const { caseInfo, actions, dispatch } = recordProps;

    const classes = Styles()
    const [docLoader, setDocLoader] = useState(false);
    const [selectedForm, setSelectedForm] = useState(false);
    const [loader, setLoader] = useState(false);
    const [show, setShow] = useState(false);
    const [editFile, setEditFile] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [filename, setFilename] = useState(false);
    const documentEditedAccessIds = user && user.show_document_editor_ids && typeof user.show_document_editor_ids === 'string' && user.show_document_editor_ids.split(',') || [];
    const documentEditedAccess = user && user.practiceDetails && documentEditedAccessIds && documentEditedAccessIds.includes(user.practiceDetails.id) || false;

    const customRef = useRef(null);

    const groupedBy = options && options.length > 0 && lodash.groupBy(options, 'document_type');
    const legalFormId = documentStatus ? documentStatus : selectedForm && Object.keys(selectedForm).length > 0 && selectedForm.legalform_id || false;
    let submitRecord = legalFormId && options && options.length > 0 && options.filter(el => el.legalform_id == legalFormId) || false;
    submitRecord = submitRecord?.length > 0 && submitRecord[0] || {};

    const handleSaveDocxFile = async (editorData) => {
        const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;
        if (editorData) {
            if (documentEdited) {
                setDocLoader('save');
                const blob = await editorData.documentEditor.saveAsBlob('Docx');
                const exportedDocument = blob;
                const myFile = new File([exportedDocument], '', {
                    type: exportedDocument.type,
                });
                dispatch(actions.saveDocumentEditor(Object.assign({}, submitRecord, { case_id: caseInfo.id }), myFile, setDocLoader, false, 'save', caseInfo));
            } else {
                const s3Key = selectedForm && selectedForm.discovery_s3_file_key || false;
                if (s3Key) {
                    let docFilename = s3Key.slice(s3Key.lastIndexOf('/') + 1, s3Key.length);
                    docFilename = docFilename.replace('.docx', '');
                    if (editorData?.documentEditor && docFilename) {
                        editorData.documentEditor.save(docFilename, "Docx");
                    }
                    setTimeout(() => {
                        handleCloseEditor();
                    }, 1000);
                }
            }
        }
    }

    const handleDownloadDocxFile = async (editorData) => {
        if (editorData) {
            const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;

            if (documentEdited) {
                setDocLoader('download');
                const blob = await editorData.documentEditor.saveAsBlob('Docx');
                const exportedDocument = blob;
                const myFile = new File([exportedDocument], '', {
                    type: exportedDocument.type,
                });
                dispatch(actions.saveDocumentEditor(Object.assign({}, submitRecord, { case_id: caseInfo.id }), myFile, setDocLoader, false, false, caseInfo));
            } else {
                const finalDocument = selectedForm && selectedForm.finalDocument || false;
                if (finalDocument) {
                    downloadDocument(finalDocument);
                }
            }
        }
    }

    const handleEmailDocumentPreview = (data, open) => {
        setSelectedForm(data);
        if (data?.sfdt_s3key) {
            setShow(true);
        } else if (data) {
            setLoader(Object.assign({}, {id : data.legalform_id, type : "document_preview"}));
            dispatch(actions.loadDocumentEditor(data, false, false, setLoader));
        }
    }

    const handleCloseEditor = () => {
        if (documentStatus) {
            dispatch(actions.documentEditorStatus(false))
        } else {
            setShow(false);
        }
    }

    const handleChangeFilename = (evt) => {
        const { value } = evt.target;
        setFilename(value);
    }

    const handleEditFilename = (data) => {
        setEditFile(data.id);
        setTimeout(() => {
            customRef.current.focus();
        }, 100);
    }

    const handleSaveFilename = (data) => {
        if (data) {
            if (filename) {
                setLoader(Object.assign({}, {id : data.id, type : "save"}));
                const submitRecord = Object.assign({}, { case_id: caseInfo.id, filename: filename, s3_file_key: data.discovery_s3_file_key, pdf_s3_file_key: data.pdf_s3_file_key, legalform_id: data.legalform_id, document_type: data.document_type });
                dispatch(actions.saveDiscoveryFilename(submitRecord, setEditFile, setLoader));
            } else {
                setEditFile(false);
            }
        }
    }

    return (
        <Grid item xs={12}>
            <Grid container direction="column">
                {options.map((opt, index) => {
                    const docFileName = groupedBy && Object.keys(groupedBy).length > 0 && groupedBy[opt.document_type] && groupedBy[opt.document_type].length > 1 ? respondingEmailDocumentFileName(opt.document_type, opt.pdf_filename, opt.orderId) : respondingEmailDocumentFileName(opt.document_type, opt.pdf_filename);
                    return <Grid container direction="row" alignItems="center" style={{ marginBottom: '10px' }}>
                        <Grid>
                            <Checkbox
                                key={index}
                                style={{ color: "#2ca01c", padding: 0, marginRight: '10px' }}
                                value={opt.id}
                                checked={(inputArr && inputArr.some(el => opt.id == el.id)) ? true : false}
                                onChange={(e) => handleChange(opt, e.target.checked)} />
                        </Grid>
                        <Grid item xs>
                            {(editFile == opt.id) ? <TextField
                                type='text'
                                onChange={handleChangeFilename}
                                className={classes.customTextBox}
                                fullWidth
                                size='small'
                                variant="standard"
                                multiline
                                inputRef={customRef}
                                autoFocus
                                InputProps={{
                                    defaultValue: opt.pdf_filename,
                                    endAdornment:
                                        <InputAdornment position="end">
                                            {(loader && loader?.id == opt.id && loader?.type == "save") ? <ClipLoader color="#2ca01c" size={20} /> : <IconButton style={{ padding: "0px" }}>
                                                <Icons type="Save" onClick={() => handleSaveFilename(opt, docFileName)} className={classes.saveIcon} />
                                            </IconButton>}                                          
                                        </InputAdornment>,
                                    classes: {
                                        input: classes.customInput
                                    }
                                }}
                            /> :
                                <Grid className={classes.fileName}>
                                    {docFileName}
                                </Grid>
                            }
                        </Grid>
                        <Grid style={{alignItems:"center", display:"flex"}}>
                            {(editFile != opt.id) ? <Icons type="Edit" onClick={() => handleEditFilename(opt)} className={classes.editIcon} /> : null}
                            {documentEditedAccess ? (loader && loader?.id == opt.legalform_id && loader?.type == "document_preview") ? <ClipLoader color="#2ca01c" size={20} /> :
                                <Tooltip placement='top-start' title={<span style={{ fontSize: '12px' }}>Preview and Edit Document.</span>}>
                                    <img src={require('images/icons/download.png')} onClick={() => handleEmailDocumentPreview(opt)} className={classes.prevIcon} />
                                </Tooltip> : null}
                        </Grid>
                    </Grid>
                })}
            </Grid>
            {(documentStatus || show) && <DocumentEditor
                actions={actions}
                dispatch={dispatch}
                disableCloseModal
                progress={docLoader}
                show={documentStatus || show}
                record={Object.assign({}, submitRecord, { s3_file_key: submitRecord.discovery_s3_file_key })}
                title={'Document Preview'}
                btnLabel="Save Document"
                showSaveBtn
                infoNote={"E-serve opposing counsel directly from EsquireTek after reviewing your document. Edits can be made within the window below."}
                onClose={handleCloseEditor}
                handleSubmitBtn={handleSaveDocxFile}
                handleDownloadBtn={handleDownloadDocxFile} />}
        </Grid>
    )
}


const mapStateToProps = createStructuredSelector({
    user: selectUser(),
});

export default connect(mapStateToProps)(EmailRespondForm);
/**
 * 
 * Password Field
 * 
 */

import React, { useState } from 'react';
import { FormControl, InputLabel, Input, InputAdornment, IconButton } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Styles from './styles';

export default function ({ input, label, required, autoCompleteOff, meta: { touched, error, warning } }) {
    const classes = Styles();
    const [showPassword, setshowPassword] = useState(false);

    const handleClickShowPassword = () => {
        setshowPassword(!showPassword);
    }

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };


    return (
        <div>
            <FormControl className={classes.fieldColor}>
                <InputLabel >{<span className={classes.textSize} >{label}</span>}</InputLabel>
                <Input
                    type={showPassword ? 'text' : 'password'}
                    defaultValue={input.value || ''}
                    onChange={(e) => input.onChange(e.target.value)}
                    autoComplete={autoCompleteOff && "new-password"}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}>
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>
    )

}
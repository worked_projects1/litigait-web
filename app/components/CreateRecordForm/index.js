/**
 * 
 * Create Record Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { ImplementationFor } from './utils';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import Error from '../Error';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import { normalize } from 'utils/tools';
import AlertDialog from 'components/AlertDialog';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function createRecordForm(props) {
  const classes = Styles();

  const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, destroy, invalid, submitButtonView, confirmMessage, customCreateButton, userLimitCustomCreateButton, createPracticeUserBtn } = props;


  useEffect(() => {
    return () => destroy();
  }, []);


  return (<div>
    <form onSubmit={handleSubmit} className={classes.form} >
      <Grid container spacing={3}>
        {(fields || []).map((field, index) => {
          const InputComponent = ImplementationFor[field.type];
          return <Grid key={index} item xs={12} style={field.style || null}>
            <Field
              name={field.value}
              label={field.label}
              type={field.type}
              metaData={metaData}
              component={InputComponent}
              normalize={normalize(field)}
              disabled={field.disableOptons && field.disableOptons.create}
              {...field} />
          </Grid>
        })}
        <Grid item xs={12}>
          {error && <Error errorMessage={error} /> || ''}
        </Grid>
      </Grid>
      <Grid container>
        {!props?.existingPasswordModal && createPracticeUserBtn && typeof createPracticeUserBtn === 'function' ? React.createElement(createPracticeUserBtn, Object.assign({}, { ...props, classes, btnLabel: 'Create' })) : userLimitCustomCreateButton && confirmMessage ? React.createElement(userLimitCustomCreateButton, Object.assign({}, { ...props, classes })) : submitButtonView ?
          React.createElement(submitButtonView, props) : customCreateButton ? React.createElement(customCreateButton, props) :
          confirmMessage ?  <AlertDialog
            description={confirmMessage}
            onConfirmPopUpClose={true}
            btnLabel1='OK'>
            {(open) => 
              <Button
                  type="button"
                  disabled={pristine || submitting}
                  variant="contained"
                  color="primary"
                  onClick={open}
                  className={classes.submitBtn} >
                  {submitting && <ButtonSpinner /> || 'Create'}
                  </Button>}
            </AlertDialog> :
          <Button
            type="submit"
            disabled={pristine || submitting}
            variant="contained"
            color="primary"
            className={classes.submitBtn}>
            {submitting && <ButtonSpinner /> || 'Create'}
          </Button>}
        <Link to={{ pathname: path, state: { ...locationState } }}>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className={classes.cancelBtn}>
            Cancel
          </Button>
        </Link>
      </Grid>
    </form>
  </div>)

}


export default reduxForm({
  form: 'createRecord',
  validate,
  touchOnChange: true
})(createRecordForm);
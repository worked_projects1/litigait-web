/**
 * 
 * GlobalFilter
 * 
 */


import React from 'react';
import { useAsyncDebounce } from 'react-table';

function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
  headersData,
  onChangeData
}) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = React.useState(globalFilter);
  const { search } = headersData || {};
  
  return (
    <div>
      <label className="react-tableoptions-search--label">
        <input
          className="react-tableoptions-search--input"
          type="text"
          placeholder="Search…"
          value={value === '' || value ? value : search || ''}
          onChange={e => setValue(e.target.value)}
          onKeyDown={e => {
            if(e.key === 'Enter') {
              onChangeData({ offset: 0, limit: 25, search: value, sort: false, page: 1 });
              setValue(false);
            }
          }}
        />
        <button className="searchBtn" onClick={() => {
          onChangeData({ offset: 0, limit: 25, search: value, sort: false, page: 1 });
          setValue(false);
          }}>Search</button>
        <button className="searchBtn" onClick={() => {
          setValue(false);
          onChangeData({ offset: 0, limit: 25, search: false, sort: false, page: 1 });
        }}>Clear</button>
      </label>
    </div>
  );
}

export default GlobalFilter;

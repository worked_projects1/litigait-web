/**
 *
 * Table
 *
 */

import React, { useState, useEffect } from 'react';
import {
  useTable,
  useGlobalFilter,
  useSortBy,
  usePagination,
} from 'react-table';
import GlobalFilter from './GlobalFilter';
import Skeleton from '@material-ui/lab/Skeleton';

function Table({ columns, data, totalPageCount, onChangeData, headersData, loading }) {
  const {
    state,
    rows,
    preGlobalFilteredRows,
    setGlobalFilter,
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 25 },
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
  );

  const [pageCountData, setPageCountData] = useState(totalPageCount);
  const totalPageOptions = totalPageCount && pageSize ? Math.ceil(parseInt(totalPageCount) / parseInt(pageSize)) : 0;
  const currentPageIndex = headersData && headersData.page && (headersData.page - 1) || 0;
  const sortData = headersData && headersData.sort && JSON.parse(headersData.sort) || false;

  const [visiblePages, setVisiblePages] = useState(
    getVisiblePages(0, totalPageOptions)
  );

  useEffect(() => {
    if (totalPageCount.toString() !== pageCountData.toString()) {
      setPageCountData(totalPageCount);
      const visiblePages = getVisiblePages(0, totalPageOptions);
      setVisiblePages(filterPages(visiblePages, totalPageOptions));
    }
  }, [totalPageCount, pageCountData, totalPageOptions])

  const navigationBtn = page => {
    gotoPage(page - 1);
    changePage(page);
    onChangeData({ offset: (page - 1) * pageSize, page, limit: pageSize });
  };

  const changePage = page => {
    const visiblePages = getVisiblePages(page, totalPageOptions);
    setVisiblePages(filterPages(visiblePages, totalPageOptions));
  };

  function filterPages(visiblePages, totalPages) {
    return visiblePages.filter(page => page <= totalPages);
  }

  function getVisiblePages(page, total) {
    if (total < 7) {
      return filterPages([1, 2, 3, 4, 5, 6], total);
    } else {
      if (page % 3 >= 0 && page > 2 && page + 1 < total) {
        return [1, page - 1, page, page + 1, total];
      } else if (page % 3 >= 0 && page > 2 && page + 1 >= total) {
        return [1, total - 3, total - 2, total - 1, total];
      } else {
        return [1, 2, 3, total];
      }
    }
  }

  const handlePreviousPage = () => {
    if (currentPageIndex > 0)
      navigationBtn(currentPageIndex);
  }

  const handleNextPage = () => {
    if (((currentPageIndex + 1) * pageSize) < totalPageCount)
      navigationBtn(currentPageIndex + 2);
  }

  const handleSort = (e) => {
    onChangeData({ offset: 0, limit: 25, sort: JSON.stringify({ column: e.sortColumn, type: e.isSortedDesc ? 'DESC' : 'ASC' }), page: 1 });
  }

  return (
    <>
      <div className="react-datatableoptions-search">
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={state.globalFilter}
          setGlobalFilter={setGlobalFilter}
          onChangeData={onChangeData}
          headersData={headersData}
        />
      </div>
      <table {...getTableProps()} className="react-table">
        <thead>
          {headerGroups.map((headerGroup, groupKey) => (
            <tr key={groupKey} {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, columnKey) => {
                return (
                  <th
                    key={columnKey}
                    {...(column.sort
                      ? {
                        ...column.getHeaderProps(
                          column.getSortByToggleProps(),
                        ),
                      }
                      : { ...column.getHeaderProps() })}
                    style={{ ...(column.sort ? { cursor: 'pointer' } : null) }}
                    onClick={column.sort ? () => handleSort(Object.assign({}, column, { isSortedDesc: sortData && sortData.column && sortData.column === column.sortColumn && sortData.type === 'ASC' ? true : false })) : null} >
                    <span style={{ fontSize: '0.6rem' }}>
                      {column.sort
                        ? column.isSorted
                          ? column.isSortedDesc
                            ? '▼ '
                            : '▲ '
                          : ''
                        : ''}
                    </span>
                    <span>{column.render('Header')}</span>
                  </th>
                );
              })}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, rowKey) => {
            prepareRow(row);
            return (
              <tr
                key={rowKey}
                {...row.getRowProps()}
                style={{
                  ...(row.original.onClickHandler !== null && !loading
                    ? { cursor: 'pointer' }
                    : null)
                }}
                onClick={() => row.original.onClickHandler && !loading ? row.original.onClickHandler(row.original) : null}>
                {row.cells.map(cell => {
                  return (
                    <td {...cell.getCellProps()}>{loading ? <Skeleton animation="wave" /> : cell.render('Cell')}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      <div className="react-table-counter">
        <p>
          {rows.length > 0
            ? `Showing ${pageSize * currentPageIndex + 1} to ${pageSize * currentPageIndex +
            page.length} of ${totalPageCount} entries.`
            : `No entries to show.`}
        </p>
      </div>
      <div className="react-table-pagination-root">
        <ul className="react-table-pagination-list" style={{ float: 'right' }}>
          <li
            className="react-table-pagination-button previous"
            onClick={handlePreviousPage}
            disabled={!canPreviousPage}>
            Back
          </li>
          {visiblePages.map((page, index, array) => (
            <li
              key={index}
              className={
                currentPageIndex === page - 1
                  ? 'react-table-pagination-button active'
                  : 'react-table-pagination-button'
              }
              onClick={() => navigationBtn(page)}>
              <span>{array[index - 1] + 1 < page ? `${page}` : page}</span>
            </li>
          ))}
          <li
            className="react-table-pagination-button next"
            onClick={handleNextPage}
            disabled={!canNextPage} >
            Next
          </li>
        </ul>
      </div>
    </>
  );
}

export default Table;

/**
 *
 * ReactTableWrapper
 *
 */

import React, { Component } from 'react';
import Table from './Table';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { respondingPlansArray, propoundingPlansArray } from 'utils/plans';
const propoundingEnabledStates = ['CA']; // 'FL'


class ReactTableWrapper extends Component {

    static propTypes = {
        records: PropTypes.array,
        columns: PropTypes.array,
        children: PropTypes.bool,
        path: PropTypes.string,
        locationState: PropTypes.object,
        view: PropTypes.bool
    };


    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    render() {
        const { name, records, columns, children, path, locationState = {}, view, sm, metaData = {}, totalPageCount, onChangeData, headersData, loading } = this.props;
        const activePath = location.pathname;
        const tableColumns = children ? columns.filter((column) => column.visible && column.viewMode) : columns.filter((column) => column.visible);

        let rows = records.map((record) => Object.assign(
            {},
            record, {
            onClickHandler: view ? this.handleClick.bind(this, record, locationState) : null,
            isActive: activePath === `${path}/${record.id}` || activePath === `${path}/${record.id}/edit`,
        }),
        );

        tableColumns.forEach((column) => {
            switch (column.type) {
                case 'upload':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) :
                                    <img
                                        src={`${row[column.value] || ''}`}
                                        role="presentation" style={{ height: 64, padding: 4 }}
                                    />,
                        },
                    ));
                    break;
                case 'download':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: column.html ? column.html(row, metaData) : row[column.value]
                        },
                    ));
                    break;
                case 'checkbox':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) : row[column.value] && 'Yes' || 'No',
                        },
                    ));
                    break;
                case 'select':
                case 'multiSelect':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: column.html ? column.html(row, metaData) : row[column.value],
                        },
                    ));
                    break;
                default:
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) : column.limit && row[column.value] && row[column.value].length > 60 ? `${row[column.value].toString().substring(0, 60)}...` : row[column.value] != null ? row[column.value] : '' || '',
                        },
                    ));
                    break;
            }
        });

        return (
            <Grid container>
                <Grid item xs={12} className={sm ? name : null}>
                    <Table
                        columns={tableColumns.map((column) => Object.assign(
                            {},
                            {
                                Header: column.sort ? column.label + '⇅' : column.label,
                                accessor: column.value,
                                sort: column.sort,
                                sortType: 'basic',
                                sortColumn: column.sortColumn
                            }
                        ))}
                        data={rows}
                        totalPageCount={totalPageCount || rows.length}
                        onChangeData={onChangeData}
                        headersData={headersData}
                        loading={loading}
                    />
                </Grid>
            </Grid>
        );
    }

    handleClick(record, data, el) {
        if(this.props.manageArchiveCase){
            return;
        }
        const { id } = record || {};
        const targetId = el && el.target && el.target.id || false;
        const { path, history, locationState, schemaId, user } = this.props;
        const isPropoundPage = this.isPropound(user, record) || false;

        if ((id && !targetId) || (id && targetId && targetId !== id))
            history.push({
                pathname: ['/rest/medical-history-summery'].includes(path) ? `${path}/${id}/form` : ['/casesCategories/cases'].includes(path) && isPropoundPage ? `${path}/${id}/forms` : `${path}/${id}/form` || `${path}/${id}`,
                state: ['/casesCategories/cases'].includes(path) ? Object.assign({}, { ...locationState }, { id, caseRecord: record }) : schemaId ? Object.assign({}, { ...locationState }, { id, schemaId }) : Object.assign({}, { ...locationState }, { id })
            });
    }

    isPropound(user, record) {
        const respondingPlanType = user && user.plan_type &&  typeof user.plan_type === 'object' && user.plan_type['responding'] || false; 
        const propoundingPlanType = user && user.plan_type &&  typeof user.plan_type === 'object' && user.plan_type['propounding'] || false;
        const isRespondingPlan = respondingPlanType && respondingPlansArray.includes(respondingPlanType) || false;
        const isPropoundingPlan = propoundingPlanType && propoundingPlansArray.includes(propoundingPlanType) || false;
        const isPropoundPage = (isRespondingPlan && isPropoundingPlan || isPropoundingPlan) && record && record.state && propoundingEnabledStates.includes(record.state) || false;

        return isPropoundPage;
    }

}

export default ReactTableWrapper;

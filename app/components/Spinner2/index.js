
/**
 * 
 * Spinner 2
 * 
 */

import React from 'react';
import ScaleLoader from "react-spinners/ScaleLoader";
import Grid from '@material-ui/core/Grid';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function (props) {

    return (<Grid container style={props.style} className={props.className}>
        {props && props.showHeight && <Grid item xs={12} style={{ height: '280px' }} /> || ''}
        <Grid item xs={12} style={{ textAlign: 'center' }}>
            <div className={`skCubeGrid1 skCubeGrid`}>
                <div className={`skCube skCube1`}></div>
                <div className={`skCube skCube2`}></div>
                <div className={`skCube skCube3`}></div>
                <div className={`skCube skCube4`}></div>
                <div className={`skCube skCube5`}></div>
                <div className={`skCube skCube6`}></div>
                <div className={`skCube skCube7`}></div>
                <div className={`skCube skCube8`}></div>
                <div className={`skCube skCube9`}></div>
            </div>
        </Grid>
    </Grid>)
}
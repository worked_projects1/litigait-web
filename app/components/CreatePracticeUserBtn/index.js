/**
 *
 * Create Practice User Btn
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ButtonSpinner from 'components/ButtonSpinner';
import AlertDialog from 'components/AlertDialog';
import Button from '@material-ui/core/Button';

function CreatePracticeUserBtn(props) {
  const { handleSubmit, invalid, submitting, spinner, pristine, classes: styleClasses, enableSubmitBtn, disableSubmitBtn, actions, dispatch, formRecord, form, btnLabel } = props;

  const [showPopup, setShowPopup] = useState(false);

  const openAlertDialog = ({ submitRecord, dialog, form, handleSubmitForm }) => {
    let submitRecords = Object.assign({}, { ...submitRecord });
    dispatch(actions.licenseValidation(Object.assign({}, submitRecords), form, dialog, handleSubmitForm));
  }

  return (
    <>
      <AlertDialog
        description={'This practice account has reached the maximum number of users allowed. To add new users, please contact the practice admin to purchase additional license.'}
        onConfirm={() => setShowPopup(false)}
        btnLabel1={'OK'}
        openDialog={showPopup}
        closeDialog={() => setShowPopup(false)}
        onConfirmPopUpClose >
        {(openDialog) => {
          return <Button type="button" variant="contained" color="primary" className={styleClasses.submitBtn} onClick={() => openAlertDialog(Object.assign({}, { dialog: setShowPopup, submitRecord: formRecord, form: form, handleSubmitForm: handleSubmit }))} disabled={disableSubmitBtn || !enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid)}>
            {(submitting || spinner) && <ButtonSpinner /> || btnLabel || 'Create'}
          </Button>
        }}
      </AlertDialog>
    </>
  );
}

CreatePracticeUserBtn.propTypes = {
  name: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  classes: PropTypes.object,  
  spinner: PropTypes.bool,  
};

export default CreatePracticeUserBtn;

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({

    btnClear: {
        opacity: "0.5"
    },
    error: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: 'red'
    },
    signatureImg: {
        width: '60%',
        height: '50%',
    }, 
    signature: {
        backgroundColor: "rgb(128 128 128 / 38%)", 
        marginBottom: '10px'
    },
    signatureDiv: {
        marginLeft: '25px',
        marginTop: '10px'
    }
}));


export default useStyles;

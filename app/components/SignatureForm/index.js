/**
 * 
 * Signature Form
 * 
 */


import React, { useState, useEffect } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import SignatureCanvas from 'react-signature-canvas';
import ClearIcon from '@material-ui/icons/Clear';
import Styles from './styles';


export default function Signature({ input, showSignature, signature, errorStyle, meta: { touched, error, warning }}) {

    const classes = Styles();
    const signPad = React.createRef();
    const [width, setWidth] = useState(window.innerWidth);
    const [sign, setSign] = useState(showSignature || false);
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    useEffect(() => {
        let mounted = true;
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);


    const handleClear = () => {
        if(!sign) {
            signPad.current.clear();
            input.onChange('');
        }
        setSign(false);
    }

    return (
        <Grid container>
            <Grid item xs={12} style={{ color: "#464444", marginBottom: '12px' }}>
                <Grid container direction="row" justify={!sign ? "space-between" : "flex-end"}>
                    {!sign ? <Grid style={{ display: 'inline-grid', maxWidth: '570px' }}>
                        <Typography variant="inherit">Draw your signature here</Typography>
                    </Grid> : null}
                    <Grid>
                        <Button variant="contained" className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>Clear</Button>
                    </Grid>
                </Grid>
            </Grid>
            {sign && signature ? <Grid className={classes.signatureDiv}>
                <img src={signature} className={classes.signatureImg} alt='No signature' />
            </Grid> :
            <Grid item xs={12} className={classes.signature}>
                <SignatureCanvas penColor='black'
                    canvasProps={{ width: !sm ? width - 80 : 460, height: 200, className: 'signCanvas' }}
                    ref={signPad}
                    onEnd={(e) => {
                        input.onChange(signPad.current.getTrimmedCanvas().toDataURL('image/png'))
                    }} />
            </Grid>}
            <div style={errorStyle || {}} className={classes.error}>
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </Grid>
    )
}

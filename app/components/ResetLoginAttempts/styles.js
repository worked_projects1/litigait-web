

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: '12px', 
        marginBottom: '12px'
    },
    Button: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Bold',
        backgroundColor: '#fff !important',
        boxShadow: 'none',
        color: '#47AC39',
        fontSize: '16px',
        paddingLeft: '8px',
        "&:hover": {
            backgroundColor: '#fff !important',
            boxShadow: 'none',
        }
    },
    label: {
        fontWeight: 'bold',
        fontFamily: 'Avenir-Bold',
        textTransform: 'uppercase',
        marginRight: '2em'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none'
    },
    value: {
        marginTop: '0px'
    },
    clear: {
        color: '#47AC39',
        marginRight: '2px',
        marginTop: '-6px'
    }
}));


export default useStyles;
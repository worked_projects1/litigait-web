/**
 * 
 * Reset Login Attempts
 * 
 */

import React, { useState, useRef, useEffect } from 'react';
import { Grid, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import Icons from 'components/Icons';
import { useDidUpdate } from 'utils/tools';
import Skeleton from '@material-ui/lab/Skeleton';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function (props) {

    const { record = {}, actions = {}, dispatch, error, success, loading } = props;
    const [spinner, setSpinner] = useState(false);
    const classes = Styles();

    const handleClearLockdown = () => {
        setSpinner(true);
        dispatch(actions.resetLoginAttempt(record));
    }

    useDidUpdate(() => {
        if(error || success){
            setSpinner(false);
        }
    }, [error, success])

    return (<Grid container className={classes.root}>
        <Grid item xs={12}>
            <Grid container direction="row">
                <Grid item xs={6}>
                    <div>
                        <div className={classes.label}>NAME:</div>
                        <p className={classes.value}>{loading ? <Skeleton animation="wave" width={80}/> : record.name || '-'}</p>
                    </div>
                </Grid>
                <Grid item xs={6} style={{ textAlign: 'right' }}>
                    {spinner ? <ButtonSpinner color='#2ca01c' /> :
                        <Button type="button" variant="contained" className={classes.Button} onClick={handleClearLockdown}>
                            <Icons type="LockOpen" className={classes.clear} /> Clear Lockdown
                        </Button>}
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <hr className={classes.hr} />
        </Grid>
    </Grid>)
}
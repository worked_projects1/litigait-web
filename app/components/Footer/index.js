/**
 * 
 * Footer
 * 
 */

import React from 'react';
import { Grid } from '@material-ui/core';
import TwitterIcon from '@material-ui/icons/Twitter';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import { Link } from 'react-router-dom';
import useStyles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function Footer(props) {

  const classes = useStyles();


  return (<Grid item xs={12} className={classes.section5}>
    <Grid container alignItems="center" justify="space-between">
      <Grid item md={4}>
        © EsquireTek. All Rights Reserved 2020
          </Grid>
      <Grid item md={4} className={classes.icons} >
        <FacebookIcon />
        <InstagramIcon />
        <TwitterIcon />
      </Grid>
      <Grid item md={4} style={{ textAlign: 'right' }}>
        <a href="https://esquiretek-policy.s3-us-west-1.amazonaws.com/terms.html" target="_blank" style={{ marginRight: '12px', color: 'black' }} >
          Terms of Use
            </a>
        <a href="https://esquiretek-policy.s3-us-west-1.amazonaws.com/privacy.html" target="_blank" style={{ color: 'black' }}>
          Privacy Policy
            </a>
      </Grid>
    </Grid>
  </Grid>
  )
}
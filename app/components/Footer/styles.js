



import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    section5: {
        paddingRight: '25px',
        paddingLeft: '25px',
        paddingTop: '4px',
        paddingBottom: '4px',
        fontFamily: 'ClearSans-Regular',
        fontSize: '12px',
        position: 'relative',
        bottom: '0',
        width: '100%'
    },
    icons: {
        textAlign: 'center',
        '& svg': {
            marginLeft: '10px',
            padding: '3px',
            cursor: 'pointer'
        }
    }
}));


export default useStyles;
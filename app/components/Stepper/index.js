/**
 * 
 * Stepper
 * 
 */


import React from 'react';
import Grid from '@material-ui/core/Grid';
import Error from 'components/Error';
import Success from 'components/Success';
import 'react-circular-progressbar/dist/styles.css';
import Modal from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import Backdrop from '@material-ui/core/Backdrop';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ProgressProvider from 'components/ProgressProvider';
import Spinner from 'components/Spinner';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ButtonSpinner from 'components/ButtonSpinner';
import Styles from './styles';

const progressArr = [2, 5, 8, 12, 16, 22, 28, 34, 42, 50, 58];
const percentageArr = [
    [2, 5, 8, 12, 16, 22, 28, 34, 42, 50, 58],
    [58, 60, 62, 64, 65, 66, 67, 68, 69, 70, 71],
    [72, 76, 80, 85, 88, 90, 92, 93, 94, 95, 96, 97, 98]
];

function StepperWrapper({ title, show, close, cancel, steps = [], active, error, success, successMessage, errorBtnText, errorBtnAction, loader, disableCancelBtn }) {

    const classes = Styles();
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    return <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={show}
        className={classes.modal}
        onClose={!disableCancelBtn ? close.bind(this) : null}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
            timeout: 500,
        }}>
        <Fade in={show}>
            <Paper className={classes.paper}>
                <Grid container className={classes.header} justify="space-between">
                    <Typography component="span">{title || ''}</Typography>
                    {!disableCancelBtn ? <CloseIcon onClick={close.bind(this)} className={classes.closeIcon} /> : null}
                </Grid>
                <Grid container className={classes.stepper}>
                    <Grid item xs={12}>
                        <Grid container>
                            {steps.map((label, stepIndex) => (
                                <ProgressProvider key={stepIndex} interval={active === 2 ? 30000 : 2000} values={stepIndex === active ? progressArr : [0]} clearInterval={error ? true : false}>
                                    {percentage => (<Grid item xs={(steps.length + 1) % 12}>
                                        <Grid className={classes.step}>
                                            <span style={{ backgroundColor: stepIndex <= active ? '#2ca01c' : '#e7e7e7' }} />

                                            <div style={{ backgroundColor: stepIndex <= active ? '#53ca42' : 'transparent', width: `${stepIndex < active ? 100 : percentage}%` }} className={classes.loader} />
                                        </Grid>
                                        <Grid className={classes.content}>
                                            {sm && label || null}

                                            {stepIndex === (steps.length - 1) &&
                                                <div className={classes.dot} style={{ backgroundColor: steps.length === active ? '#2ca01c' : '#e7e7e7', top: `${sm ? '-45' : '-24'}px` }} /> || null}

                                            {stepIndex === (steps.length - 1) && sm &&
                                                <div style={{ position: 'relative', top: '-40px', left: '100%' }}>Completed</div> || null}
                                        </Grid>
                                    </Grid>)}
                                </ProgressProvider>
                            ))}
                        </Grid>
                    </Grid>

                    {steps.length != active && !error && !success &&
                        <Grid item xs={12} className={classes.spinner}>
                            <ProgressProvider interval={active === 2 ? 30000 : 2000} values={percentageArr[active] || [0]} changeInterval={active}>
                                {percentage => (
                                    <Grid>{percentage}% Completed</Grid>
                                )}
                            </ProgressProvider>
                            <Spinner style={{ marginTop: '10px' }} loading={true} />
                        </Grid> || null}
                    <Grid container justify='center'>
                        {steps.length != active && error &&
                            <Grid item xs={12} sm={errorBtnText ? 6 : 4} className={classes.error}>
                                <Error errorMessage={error} />
                            </Grid>
                            || null}
                    </Grid>
                    <Grid container justify='center'>
                        {(steps.length === active || success) &&
                            <Grid item xs={12} sm={errorBtnText ? 6 : 4} className={classes.success}>
                                <Success successMessage={success || successMessage} />
                            </Grid> || null}
                    </Grid>
                    {((!error && !disableCancelBtn) || error || steps.length === active || success) &&
                        <Grid item xs={12} style={{ textAlign: 'center', marginTop: '25px' }}>
                            <Button type="button" variant="contained" color="primary" onClick={() => !error && !success && steps.length != active ? cancel() : error && errorBtnAction ? errorBtnAction() : close()} className={classes.button} style={{ width: '200px' }}>
                                {loader && !success && <ButtonSpinner /> || (!error && !success && steps.length != active) && 'Cancel Process' || error && errorBtnText || 'Close'}
                            </Button>
                        </Grid> || null}
                </Grid>
            </Paper>
        </Fade>
    </Modal>
}


export default StepperWrapper;
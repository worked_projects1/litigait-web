

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& .MuiPaper-root': {
            width: '80% !important',
        }
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        paddingLeft: '50px',
        paddingRight: '50px',
        [theme.breakpoints.down('xs')]: {
            paddingLeft: '25px',
            paddingRight: '25px',
        }
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        width: '110px'
    },
    btnGrid: {
        textAlign: 'center'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    success: {
        marginTop: '10px',
        '& .MuiAlert-filledSuccess': {
            width: '100% !important'
        }
    },
    error: {
        marginTop: '10px',
        '& .MuiAlert-filledError': {
            color: 'black',
            border: '2px solid #f44336',
            background: 'transparent',
            width: '100% !important'
        },
        '& .MuiAlert-filledSuccess': {
            width: '100% !important'
        }
    },
    step: {
        display: 'flex', flex: '0 1', width: '100%', backgroundColor: '#e7e7e7', height: '7px',
        '& span': {
            position: 'relative', width: '20px', height: '20px', borderRadius: '100%', top: '-6px', left: '-1px'
        }
    },
    loader: {
        position: 'relative', width: '12px', left: '-2px'
    },
    content: {
        position: 'relative', left: '-40px', paddingTop: '12px'
    },
    stepper: {
        padding: '25px',
        marginTop: '50px',
        [theme.breakpoints.down('xs')]: {
            paddingLeft: '10px',
            paddingRight: '10px',
        }
    },
    dot: {
        position: 'relative', width: '20px', height: '20px', borderRadius: '100%', top: '-45px', left: '100%', marginLeft: '25px'
    },
    spinner: {
        height: '100px', marginTop: '25px', padding: '25px', textAlign: 'center'
    }
}));


export default useStyles;
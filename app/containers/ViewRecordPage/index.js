

/**
 * 
 * Vew Record Page
 * 
 */

import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import AlertDialog from '../../components/AlertDialog';
import Snackbar from 'components/Snackbar';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import ButtonSpinner from 'components/ButtonSpinner';
import Skeleton from '@material-ui/lab/Skeleton';
import { selectUser } from 'blocks/session/selectors';
import { userBasedRespondingPlans } from 'utils/plans';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {boolean} decoratorView 
 */
export default function (name, path, columns, actions, selectors, decoratorView) {

    const {
        selectRecord,
        selectRecordsMetaData,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectPageLoader,
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewRecordPage(props) {

        const classes = Styles();
        const { record, error, success, loading, progress, dispatch, location = {}, match = {}, metaData = {}, queryParams, user } = props;
        const schemaId = location && location.state && location.state.schemaId || false;
        const section = columns && typeof columns === 'object' && schemaId && columns.section || false;
        const sectionColumns = section && schemaId && section.find(_ => _.schemaId === schemaId) || false;
        const noDeleteOptions = ['respondingCustomTemplate.view', 'propoundingCustomTemplate.view', 'state.view'];
        const noEditOptions = ['feeWaiver.view', 'discountCode.view'];
        const planType = user?.plan_type?.responding;

        const deleteRecord = () => {
            dispatch(actions.deleteRecord(record.id, false, queryParams));
        };

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecord(match.params.id, queryParams));
            dispatch(actions.loadRecords(false, queryParams));
            return () => mounted = false;
        }, [match.params.id]);

        let displayName;
        if (name == 'clients.view') {
            displayName = "client";
        } else if (name == 'cases.view') {
            displayName = "case";
        } else if (name == 'orders.view') {
            displayName = "order";
        } else if (name == 'users.view' || name == 'esquireTekUsers.view' || name == 'practiceUsers.view') {
            displayName = "user";
        } else if (name == 'objections.view') {
            displayName = "objection";
        } else if (name == 'questionsTranslations.view') {
            displayName = "translations";
        } else if (name == 'discountCode.view'){
            displayName = "promo code";
        }

        let deleteConfirmation = `Are you sure you want to delete this ${displayName || ""}?`;
        if (name == 'clients.view' && record && record.totalCases > 0) {
            deleteConfirmation = `This client has case records. Deleting this client will delete all case records and documents attached to this client. Do you want to continue?`;
        }

        if (name == 'users.view') {
            if(planType && userBasedRespondingPlans.includes(planType) && user?.practiceDetails?.billing_type === "limited_users_billing") {
                if(user?.total_no_of_user_count > 5 || user?.purchased_license_count > 5) {
                    deleteConfirmation = `Are you sure you want to delete this ${displayName || ""}? License fees will not be charged for deleted users starting next billing cycle.`;
                }
            }
        }

        const columValue = (column, row) => {
            switch (column.type) {
                case 'upload':
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row, metaData) :
                            (<img
                                src={row[column.value] || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                                role="presentation"
                                className={classes.img}
                            />);
                default:
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row, metaData) :
                            (<p
                                className={classes.value}
                                dangerouslySetInnerHTML={{ __html: !row[column.value] && row[column.value] != '0' ? '-' : row[column.value] || '-' }}
                            />);
            }
        };


        return (
            <Grid container>
                <Grid item xs={12}>
                    <Grid container className={classes.header}>
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Link to={{ pathname: path, state: { ...location.state } }} className={classes.link}>
                                <FormattedMessage {...messages.close} />
                            </Link>}
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Grid className={classes.section}>
                                {!noEditOptions.includes(name) && <Link to={{ pathname: `${path}/${record.id}/edit`, state: { ...location.state } }} className={classes.link}>
                                    <FormattedMessage {...messages.edit} />
                                </Link>}
                                {!noDeleteOptions.includes(name) ? <AlertDialog
                                    description={deleteConfirmation}
                                    onConfirm={deleteRecord}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='Delete'
                                    btnLabel2='Cancel' >
                                    {(open) => <Button type="button" variant="contained" className={classes.Button} onClick={open} >
                                        {progress && <ButtonSpinner color='#2ca01c' /> || <FormattedMessage {...messages.delete} />}
                                    </Button>}
                                </AlertDialog> : null}
                            </Grid>}
                    </Grid>
                    <Grid item xs={12}>
                        {decoratorView && record && React.createElement(decoratorView, Object.assign({}, props, { actions }))}
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            {(section && sectionColumns && sectionColumns.columns || columns || []).map((column) =>
                            (column.viewRecord ?
                                <Grid item xs={12} key={column.id}>
                                    <div>
                                        <div className={classes.label}>{column.label}:</div>
                                        {columValue(column, record)}
                                        <hr className={classes.hr} />
                                    </div>
                                </Grid> : null)
                            )}
                        </Grid>
                    </Grid>
                </Grid>
                <Snackbar show={error || success ? true : false} text={error || success} severity={error ? 'error' : 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>);
    }

    ViewRecordPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        metaData: selectRecordsMetaData(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress(),
        user: selectUser()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewRecordPage);

}
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    note: {
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    btwn: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    button2: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular'
    },
    table: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        marginTop: '40px',
        overflow: 'auto'
    },
    bodyScroll: {
        maxHeight: '400px',
        overflowY: 'scroll',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 8,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    }
}));


export default useStyles;
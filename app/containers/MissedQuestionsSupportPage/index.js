/**
 *
 * MissedQuestionsSupportPage
 *
 */


import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import ModalForm from 'components/ModalRecordForm';
import Snackbar from 'components/Snackbar';


/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, filterColumns, view, excel) {

    const { missedQuestionsFormField = [], deleteQuestionsWithHashFormField = [] } = columns;
    const {
        selectLoading,
        selectError,
        selectSuccess,
    } = selectors;

    /**
     * @param {object} props 
     */
    function MissedQuestionsSupportPage(props) {

        const classes = Styles();
        const [showUploadQuestionsForm, setShowUploadQuestionsForm] = useState(false);
        const [showDeleteHashQuestionsForm, setShowDeleteHashQuestionsForm] = useState(false);
        const { dispatch, success, error } = props;


        const handleForm = (data) => {
            dispatch(actions.updateQuestions(Object.assign({}, data, { form: `Update_Question_Form` }), setShowUploadQuestionsForm));
        }

        const handleDeleteFile = (data) => {
            dispatch(actions.DeleteHashWithFile(Object.assign({}, data, { form: `Delete_Question_Form` }), setShowDeleteHashQuestionsForm));
        }

        return <Grid container>
            <Grid item xs={12}>
                <Grid container alignItems="center" direction="column">
                    <Grid item xs={12}>
                        <ModalForm
                            initialValues={{}}
                            show={showUploadQuestionsForm}
                            title="Update Questions"
                            fields={missedQuestionsFormField}
                            form={`Update_Question_Form`}
                            onSubmit={handleForm.bind(this)}
                            disableContainer
                            onClose={() => setShowUploadQuestionsForm(false)}
                            btnLabel="Update"
                            enableScroll={classes.bodyScroll}
                            footerStyle={{ border: 'none' }}
                            style={{ paddingRight: '0px' }}
                        />
                        <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={() => setShowUploadQuestionsForm(true)}>
                            Update Questions With File
                        </Button>
                    </Grid>
                    <Grid item xs={12} className={classes.btwn}>
                        <Typography variant="subtitle2" className={classes.note}>
                            OR
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <ModalForm
                            initialValues={{}}
                            show={showDeleteHashQuestionsForm}
                            title="Clear Cache With File"
                            fields={deleteQuestionsWithHashFormField}
                            form={`Delete_Question_Form`}
                            onSubmit={handleDeleteFile.bind(this)}
                            disableContainer
                            onClose={() => setShowDeleteHashQuestionsForm(false)}
                            btnLabel="Submit"
                        />
                        <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={() => setShowDeleteHashQuestionsForm(true)}>Clear Cache With File</Button>
                    </Grid>

                    <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
                </Grid>
            </Grid>
        </Grid>
    }

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        error: selectError(),
        success: selectSuccess(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(MissedQuestionsSupportPage);

}

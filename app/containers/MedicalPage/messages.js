/*
 * MedicalPage Messages
 *
 * This contains all the text for the MedicalPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.MedicalPage';

export default defineMessages({
  create: {
    id: `${scope}.create`,
    defaultMessage: 'Add New Medical History',
  },
  edit: {
    id: `${scope}.edit`,
    defaultMessage: 'Edit',
  },
  delete: {
    id: `${scope}.delete`,
    defaultMessage: 'Delete',
  },
  error: {
    id: `${scope}.error`,
    defaultMessage: 'There was an error loading the resource. Please try again.',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Back',
  },
  send: {
    id: `${scope}.send`,
    defaultMessage: 'Send',
  },
  resend: {
    id: `${scope}.resend`,
    defaultMessage: 'Resend',
  },
  sendNew: {
    id: `${scope}.sendNew`,
    defaultMessage: 'Send New',
  }
});

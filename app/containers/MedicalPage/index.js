/**
 *
 * Medical Page
 *
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { ImplementationTable, RecordsData } from '../../utils/tools';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import schema from 'routes/schema';
import ModalForm from 'components/ModalRecordForm';
import moment from 'moment';
import 'moment-timezone';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, filterColumns, view, excel) {

    const { medicalHistoryColumns, excelExportColumns } = columns;

    const {
        selectLoading,
        selectRecords,
        selectError,
        selectSuccess,
        selectRecordsMetaData,
        selectProgress,
        selectTotalPageCount,
        selectHeaders
    } = selectors;

    /**
     * @param {object} props 
     */
    function MedicalPage(props) {

        const classes = Styles();
        const { dispatch, records, children, location = {}, history, error, metaData, loading, success, progress, totalPageCount, headers = {}, pathData } = props;
        const { usersOptions = [] } = metaData;
        const { pathname, state = {} } = location;
        const activeChildren = path !== pathname;
        const fullView = pathname.indexOf('form') > -1 ? true : false;
        const [filter, setFilter] = useState(false);
        const [excelExport, setExcelExport] = useState(false);
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const TableWrapper = ImplementationTable['reactTable'];
        const { title } = pathData;

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords(true));
            return () => mounted = false;
        }, []);

        if (error) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        const filterRecords = filter && records.filter(item => Object.keys(filter).filter(key => ((item[key] && item[key] === filter[key]) || filter[key] === '')).length === Object.keys(filter).length) || records;

        const handlePageData = (data) => {
            dispatch(actions.setHeadersData(data));
            dispatch(actions.loadRecords(true));
        }

        const handleFilter = (e) => {
            setFilter(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value }));
            handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: JSON.stringify(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value })), search: false, sort: false, page: 1 }));
        }

        const handleExcelExport = (data, dispatch, { form }) => {
            let timeZone = moment.tz.guess();

            const fromDate = moment(data && data.from_date).format('YYYY-MM-DD 00:00:00');
            const endDate = moment(data && data.to_date).format('YYYY-MM-DD 23:59:59');

            const utcFromDate = timeZone && moment.tz(fromDate, timeZone).utc().format('YYYY-MM-DD HH:mm:ss') || moment.tz(fromDate, 'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm:ss');
            const utcEndDate = timeZone && moment.tz(endDate, timeZone).utc().format('YYYY-MM-DD HH:mm:ss') || moment.tz(endDate, 'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm:ss');

            const submitData = Object.assign({}, { from_date: utcFromDate }, { to_date: utcEndDate });
            dispatch(actions.excelExport(submitData, form, false, medicalHistoryColumns, title, setExcelExport, name, usersOptions));
        }

        return !fullView ? (<Grid container>
            <Grid item xs={12}>
                <Grid container>
                    {filterColumns && filterColumns[name] && filterColumns[name].length > 0 ?
                        <Grid item xs={6}>
                            {filterColumns[name].map((filterColumn, index) =>
                                <Grid key={index} className={classes.dropdownGrid}>
                                    <select className={classes.filter} name={filterColumn.value} defaultValue={""} onChange={handleFilter}>
                                        {filterColumn.options.map((option, i) => <option key={i} disabled={option.disabled} value={option.value}>{option.label}</option>)}
                                    </select>
                                </Grid>)}
                        </Grid> : null}
                    {excel ? <Grid item xs={6}>
                        <Grid container justify={sm ? "flex-end" : "flex-start"} direction="row">
                            <Button type="button" className={classes.exportXlsxBtn} variant="contained" color="primary" onClick={() => setExcelExport(true)}>Download Excel</Button>
                            <ModalForm
                                title={title}
                                customNote={`Filter by request date`}
                                fields={excelExportColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                form={`excelExport_${name}`}
                                show={excelExport}
                                onClose={() => setExcelExport(false)}
                                onSubmit={handleExcelExport.bind(this)}
                                btnLabel="Submit"
                                disableContainer
                                style={{ marginRight: '10px' }} />
                        </Grid>
                    </Grid> : null}
                </Grid>
            </Grid>
            <Grid item xs={12} md={activeChildren ? 6 : 12} className={classes.table}>
                <TableWrapper
                    records={loading ? RecordsData : filterRecords.map(record => Object.assign({}, { ...record }, { assigned_to: usersOptions.filter(_ => record.assigned_to && record.assigned_to != 'N/A' && _.value === record.assigned_to).reduce((a, el) => el.label || record.assigned_to, record.assigned_to) }))}
                    columns={medicalHistoryColumns}
                    children={activeChildren}
                    path={path}
                    name={name}
                    history={history}
                    locationState={location.state}
                    view={view}
                    totalPageCount={totalPageCount}
                    onChangeData={handlePageData}
                    headersData={headers}
                    loading={loading}
                />
            </Grid>
            {activeChildren ?
                <Grid item xs={12} md={6}>
                    <div className="children">
                        {children}
                    </div>
                </Grid> : null}
        </Grid>) :
            <Grid container className="ViewMedicalPage">
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
    }

    MedicalPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        headers: PropTypes.object,
        history: PropTypes.object,
        headers: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        totalPageCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number])
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        records: selectRecords(),
        error: selectError(),
        success: selectSuccess(),
        metaData: selectRecordsMetaData(),
        progress: selectProgress(),
        headers: selectHeaders(),
        totalPageCount: selectTotalPageCount()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(MedicalPage);

}

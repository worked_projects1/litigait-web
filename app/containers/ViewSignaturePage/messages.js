/*
 * ViewSignaturePage Messages
 *
 * This contains all the text for the ViewSignaturePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewSignaturePage';

export default defineMessages({
    notFound: {
        id: `${scope}.notFound`,
        defaultMessage: 'No Questions Found.',
    },
    lawyerResponseText: {
        id: `${scope}.lawyerResponseText`,
        defaultMessage: 'Attorney Response: ',
    },
    confirm: {
        id: `${scope}.confirm`,
        defaultMessage: 'Confirm & TekSign',
    },
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    }
});

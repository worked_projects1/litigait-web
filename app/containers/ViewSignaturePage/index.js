/**
 * 
 * View Cases Signature Page
 * 
 */

import React, { memo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Styles from './styles';
import PropTypes from 'prop-types';
import Snackbar from 'components/Snackbar';
import { Grid, Button, Typography  } from '@material-ui/core';
import { selectUser } from 'blocks/session/selectors';
import Spinner from 'components/Spinner';
import { FormattedMessage } from 'react-intl';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { getOffset, isPropound, isRespondEntityUrl, planAlertMessage } from 'utils/tools';
import Icons from 'components/Icons';
import AlertDialog from 'components/AlertDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import messages from './messages';
import { getState } from 'containers/ViewCasesPage/utils';
import ModalForm from 'components/ModalRecordForm';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import ReactHtmlParser from 'react-html-parser';
import lodash from 'lodash';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 */

const DocumentButton = ({ columns, value, classes, updateBilling, billing, record, tekSignLabel, label, loading, generateDocument, openDocument, type, title, document, setDocument, style, DiscType, location, languageType, setLanguageType, descriptionMessage }) => {
    return <ModalForm
    disableContainer
    onSubmitClose
    onSubmitbtn
    fields={columns.filter(_ => _.editRecord)}
    form={`billingForm_${type}_${value}`}
    className={classes.form}
    footerStyle={{ borderTop: 'none' }}
    style={style}
    btnLabel={`Pay $${billing && billing.price || 0}`}
    message="Please provide your credit card details to process this order. Card details will be securely saved for future orders as well."
    onSubmit={updateBilling.bind(this, getState(record, label), type)}>
    {(openForm) =>
        <AlertDialog
            description={`You will be charged $${billing && billing.price || 0} for your TekSign order. Do you want to continue?`}
            onConfirm={() => generateDocument(type)}
            onConfirmPopUpClose={true}
            closeDialog={() => setDocument(false)}
            btnLabel1='Yes'
            btnLabel2='No' >
            {(openDialog) => 
                <AlertDialog
                    description={descriptionMessage}
                    onConfirm={() => setDocument(false)}
                    onConfirmPopUpClose={true}
                    closeDialog={() => setDocument(false)}
                    btnLabel1='OK' >
                    {(openAlert) => {
                        return <AlertDialog
                        description={`Do you want to send finalized response to client for signature(TekSign)?`}
                        onConfirm={() => openDocument(Object.assign({}, { form: openForm, dialog: openDialog, submitRecord: getState(record, tekSignLabel), generatedType: type, generateDocument: () => generateDocument(type), freeTrialDialog: openAlert }))}
                        onConfirmPopUpClose={true}
                        heading={value && value.toUpperCase() === 'FROGS' && DiscType && (DiscType === 'Disc001' || DiscType === 'Disc004' || DiscType === 'Disc002' || DiscType === 'FL145') ? () => (<Grid container direction="row" justify="space-between" alignItems="center" className={classes.heading}>
                            <Grid>
                                <select name="languageType" defaultValue={languageType} onChange={(e) => setLanguageType(e.target.value)} className={classes.languageType}>
                                    <option value="en">English</option>
                                    <option value="es">Spanish</option>
                                    <option value="vi">Vietnamese</option>
                                </select>
                            </Grid>
                        </Grid>) : value && value.toUpperCase() === 'FROGS' && DiscType && (DiscType === 'Disc003' || DiscType === 'Disc005') ? () => (<Grid container direction="row" justify="space-between" alignItems="center" className={classes.heading}>
                            <Grid>
                                <select name="languageType" defaultValue={languageType} onChange={(e) => setLanguageType(e.target.value)} className={classes.languageType}>
                                    <option value="en">English</option>
                                    <option value="vi">Vietnamese</option>
                                </select>
                            </Grid>
                        </Grid>) : null}
                        btnLabel1='Yes'
                        btnLabel2='No' >
                        {(open) =>
                            <Grid item xs={12} className={classes.icon}>
                                <Button type="button" variant="contained" color="primary" disabled={record.questions.length === 0} className={classes.docButton} onClick={open}>
                                    {loading && <ButtonSpinner /> || <FormattedMessage {...messages.confirm} />}
                                </Button>
                            </Grid>
                        }
                    </AlertDialog>
                    }}
                </AlertDialog>
            }
        </AlertDialog>
    }
    </ModalForm>
};

export default function (name, path, columns, actions, selectors, filterColumns) {
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectBilling, selectRecordsMetaData, selectSessionExpand } = selectors;

    const { legalColumns, uploadColumns, standardFormColumns } = columns;

    /**
     * @param {object} props 
     */
    function ViewSignaturePage(props) {

        const { dispatch, match, success, error, record = {}, metaData, billing, loading, location, expand, history, user } = props;
        const { client_id, id: case_id, attorneys } = record;
        const documentType = match && match.params && match.params.type || false;
        const questions = documentType && documentType === 'odd' ? record && record.questions && lodash.groupBy(record.questions, 'question_category_id') : record && record.questions && lodash.groupBy(record.questions, 'question_number_text') || {};
        const questionsType = record && record.questions && record.questions.length > 0 && Object.keys(lodash.groupBy(record.questions, 'question_type')).filter(e => e !== 'null') || [];

        const [document, setDocument] = React.useState();
        const [progress, setProgress] = useState(false);
        const elements = useElements();
        const stripe = useStripe();

        const [pageLoader, setPageLoader] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [position, setPosition] = useState(false);
        const [margin, setMargin] = useState(0);
        const [languageType, setLanguageType] = useState('en');
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const classes = Styles();
        const navbarWidth = expand ? 240 : 60;
        const planType = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['responding'] || false;
        const descriptionMessage = planAlertMessage(planType, user?.subscription_details);

        useEffect(() => {
            let mounted = true;
            setPageLoader(true);
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            setTimeout(() => {
                dispatch(actions.loadForm(Object.assign({}, { 
                    id: match.params.id, 
                    document_type: match.params.type.toUpperCase(), 
                    client_response_status_filter: 'All', 
                    lawyer_response_status_filter: 'Final', 
                    legalforms_id: match.params.legalforms_id })));
                setPageLoader(false);
            }, 5000);

            window.addEventListener('resize', () => {
                setMargin(getOffset('esquiretek-header') - 10);
                setPosition((getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')) - 2);
                setWidth(window.innerWidth);
            });
            return () => mounted = false;
        }, [match.params.id]);

        if (loading || pageLoader || (record && !record.questions)) {
            return <Spinner style={!md ? { position: 'relative', left: '0', top: `${position || getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')}px` } : { position: 'relative', top: `${position || getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')}px` }} className={classes.spinner} />;
        }

        const openDocument = ({ form, dialog, submitRecord, generatedType, generateDocument, freeTrialDialog }) => {
            setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
            dispatch(actions.loadBillingDetails(form, dialog, setProgress, Object.assign({}, submitRecord, { legalforms_id: match.params.legalforms_id }), generatedType, generateDocument, false, freeTrialDialog));
        }

        const generateDocument = (generatedType) => {
            setProgress(generatedType);
            dispatch(actions.sendVerification(Object.assign({}, { client_id: client_id, case_id, document_type: match.params.type, legalforms_id: match.params.legalforms_id, TargetLanguageCode: languageType, plan_type: planType, state: record.state })));
        }

        const handleBilling = async (submitRecord, generatedType, data, dispatch, { form }) => {
            const result = await stripe.confirmCardSetup(billing.client_secret, {
                payment_method: {
                    card: elements.getElement(CardElement),
                    billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                }
            });

            dispatch(actions.updateBillingDetails(result, Object.assign({}, { client_id: client_id, case_id, document_type: match.params.type, legalforms_id: match.params.legalforms_id, TargetLanguageCode: languageType }), setProgress, generatedType, form));

        }

        const DiscType = questionsType && questionsType.length > 0 ? questionsType[0] : standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
            if (!q && record && record.questions && record.questions.length > 0) {
                const arr = record.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
                if (arr)
                    q = nc;
            }
            return q;
        }, false);

        const lawyers = attorneys && typeof attorneys === 'string' && attorneys.split(',').reduce((a, el) => {
            const opt = metaData && metaData['users'] && metaData['users'].find(_ => _.id.toString() === el);
            if (opt)
                a.push(opt)

            return a;
        }, []) || [];


        return (
            <Grid container className={classes.body} >
                <Grid item xs={12} id="esquiretek-signature-header" className={classes.header} style={!md ? { left: '0', width: `${width}px`, top: `${position || (getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')) - 12}px` } : { width: `${width - navbarWidth}px`, top: `${position || (getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')) - 2}px`, left: navbarWidth }}>
                    <Grid container>
                        <Grid item xs={12}>
                            <Grid container justify="center">
                                <DocumentButton
                                    columns={uploadColumns}
                                    value={match.params.type}
                                    classes={classes}
                                    updateBilling={handleBilling}
                                    billing={billing}
                                    record={Object.assign({}, record, { lawyers })}
                                    style={!md && { justifyContent: 'center' } || {}}
                                    label={match.params.type.toUpperCase()}
                                    tekSignLabel="TEKSIGN"
                                    loading={props.progress}
                                    generateDocument={generateDocument}
                                    openDocument={openDocument}
                                    title="TekSignTest"
                                    type="teksign"
                                    document={document}
                                    setDocument={setDocument}
                                    DiscType={DiscType}
                                    location={location}
                                    setLanguageType={setLanguageType}
                                    languageType={languageType}
                                    descriptionMessage={descriptionMessage}
                                />
                                <Button type="button" onClick={() => history.push({ pathname: isRespondEntityUrl(user, Object.assign({}, record, { value: match.params.type }), 'details'), state: Object.assign({}, { ...location.state }) })} variant="contained" className={classes.button}>
                                    Close
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} style={{ marginTop: `${margin || (getOffset('esquiretek-header') + 122) || 0}px` }}>
                    <Grid container direction="column">
                        {questions && Object.keys(questions).length > 0 ? (Object.keys(questions) || []).map((key, index) => {

                            let consultation = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['is_consultation_set'] || false;
                            let consultationSet = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['consultation_set_no'] && lodash.groupBy(questions[key], 'consultation_set_no') || [];

                            const category = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['question_category'] || false;
                            const questionCategory = documentType && documentType === 'odd' && category || false;
                        
                            return <React.Fragment key={index}>
                                {questionCategory ? <Typography style={{ fontWeight: 'bold', paddingLeft: '12px', textDecoration: 'underline', fontSize: '16px', textTransform: 'capitalize' }}>Category - {questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['question_category'].toLowerCase()}</Typography> : null}
                                {questions[key] && questions[key].length > 0 && questions[key][0]['question_section_text'] ?
                                    <Typography variant="subtitle2" style={{ padding: '12px' }}>
                                        <b>{questions[key][0]['question_number_text']}. {questions[key][0]['document_type'] == 'FROGS' ? (questions[key][0]['question_text']).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : questions[key][0]['question_text']}</b>
                                    </Typography>
                                    : null}
                                <Grid container>
                                    {!consultation && (questions[key] && questions[key].length > 0 && (questions[key] || []).map((form, formIndex) => (<React.Fragment key={formIndex}>
                                        <Grid item xs={12} className={classes.question}>
                                        {form.question_section ? <b>{form.question_section} &nbsp;&nbsp; {(form.question_section_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>
                                            :
                                        <b>{form.question_number_text}. &nbsp;&nbsp; {(form.question_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>}
                                        </Grid>
                                        <Grid item xs={12} className={classes.response}>
                                            <Grid container direction="row">
                                                <Icons type={'ArrowRight'} style={{ marginBottom: '3px', marginRight: '3px' }} />
                                                <Grid className={classes.text}>
                                                    {form.lawyer_response_text && form.lawyer_response_text.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{ReactHtmlParser(line.replace(/<[^>]*(>|$)|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g, ''))}</span> : <p key={pos} className={classes.lineResponse}>{ReactHtmlParser(line.replace(/<[^>]*(>|$)|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g, ''))}</p>)}
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </React.Fragment>)) || null)}
                                    {consultation && (Object.keys(consultationSet || []).map(keySet => (
                                        <React.Fragment>
                                            {consultationSet && consultationSet[keySet] && consultationSet[keySet].length > 0 && consultation ? <Grid item>
                                                <Typography variant="subtitle2" style={{ padding: '12px 12px 0 12px' }}>
                                                    <b>{consultationSet[keySet][0]['document_type'] === 'IDC' ? `Person ` : `Consultation `}{consultationSet[keySet][0]['consultation_set_no']}</b>
                                                </Typography>
                                            </Grid> : null}
                                        {consultationSet[keySet] && consultationSet[keySet].length > 0 && (consultationSet[keySet] || []).map((form, formIndex) => 
                                            <React.Fragment key={formIndex}>
                                                <Grid item xs={12} className={classes.question}>
                                                {form.question_section ? <b>{form.question_section} &nbsp;&nbsp; {(form.question_section_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>
                                                    :
                                                <b>{form.question_number_text}. &nbsp;&nbsp; {(form.question_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>}
                                                </Grid>
                                                <Grid item xs={12} className={classes.response}>
                                                    <Grid container direction="row">
                                                        <Icons type={'ArrowRight'} style={{ marginBottom: '3px', marginRight: '3px' }} />
                                                        <Grid className={classes.text}>
                                                            {form.lawyer_response_text && form.lawyer_response_text.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{ReactHtmlParser(line.replace(/<[^>]*(>|$)|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g, ''))}</span> : <p key={pos} className={classes.lineResponse}>{ReactHtmlParser(line.replace(/<[^>]*(>|$)|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g, ''))}</p>)}
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </React.Fragment>
                                        )}  
                                    </React.Fragment>
                                    )) || null)}
                                </Grid>
                            </React.Fragment>}) : <Grid container justify="center" className={classes.notFound}>
                                <FormattedMessage {...messages.notFound} />
                        </Grid>}
                    </Grid>
                </Grid>
                <Snackbar show={error || success ? true : false} text={error || success} severity={error ? 'error' : 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid >
        )


    }

    ViewSignaturePage.propTypes = {
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        metaData: selectRecordsMetaData(),
        loading: selectPageLoader(),
        success: selectSuccess(),
        progress: selectProgress(),
        billing: selectBilling(),
        user: selectUser(),
        expand: selectSessionExpand()
    });

    const withConnect = connect(
        mapStateToProps,
    );

    return compose(
        withConnect,
        memo
    )(ViewSignaturePage);

}
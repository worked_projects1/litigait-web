import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    body: {
        padding: '20px',
        paddingTop: '0px',
        marginTop: '10px'
    },
    notFound: {
        marginTop: '15px'
    },
    question: {
        padding: '12px',
        fontSize: '16px'
    },
    response: {
        paddingLeft: '8px',
        paddingRight: '8px',
        paddingBottom: '25px'
    },
    text: {
        paddingTop: '4px',
        fontSize: '15px',
        // '& > span': {
            '& > p': {
                margin: '0px !important'
            }
        // }
    },
    button: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginLeft: '20px'
    },
    header: {
        position: 'fixed',
        height: 'auto',
        zIndex: '999',
        background: '#fff',
        top: '48px',
        left: '60px',
        padding: '25px',
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
        boxShadow: '0px 3px 4px -1px lightgrey'
    },
    line: {
        margin: '4px'
    },
    lineResponse: {
        marginTop: '10px'
    },
    spinner :{
        width: '100%'
    },
    heading: {
        marginTop: '20px',
        marginBottom: '20px',
        paddingRight: '6px',
        paddingLeft: '15px'
    },
    languageType: {
        padding: '2px 12px 2px 12px',
        border: '2px solid gray',
        borderRadius: '28px',
        paddingLeft: '20px',
        paddingRight: '30px',
        fontFamily: 'Avenir-Bold',
        outline: 'none',
        fontSize: '13px',
        position: 'relative',
        background: 'transparent'
    }
}));


export default useStyles;
/**
 *
 * Activation Fee Categories Page
 *
 */



import React, { useEffect, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Tabs, Tab, Paper } from '@material-ui/core';
import Styles from './styles';
import { matchPath } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {

    const {
        selectLoading,
        selectUpdateError,
        selectSuccess
    } = selectors;

    /**
     * @param {object} props 
     */
    function ActivationFeeCategoriesPage(props) {

        const classes = Styles();
        const { dispatch, location = {}, children, history } = props;
        const selectedTab = location && location.pathname && matchPath(location.pathname, { path: `/globalSettings/:tab/activationCategories/:subTab` }) && matchPath(location.pathname, { path: `/globalSettings/:tab/activationCategories/:subTab` }).params.subTab || false;
        const sections = columns && typeof columns === 'object' && columns.section || false;

        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadGlobalSettings(true));
            if (!selectedTab) {
                history.push({ pathname: `${path}/0/rest/fee-waiver`, state: location.state });
            }
            return () => mounted = false;
        }, [selectedTab]);



        const handleTabs = (e, tabIndex) => {
            const currentSection = sections.find((s, i) => i === tabIndex);
            history.push({ pathname: currentSection ? `${path}/${tabIndex}${currentSection.path}` : `${path}/${tabIndex}`, state: location.state });
        }

        return <Grid container id={name} className={classes.container}>
            <Grid item xs={12}>
                <Paper square className={classes.paper}>
                    <Tabs
                        value={parseInt(selectedTab)}
                        onChange={handleTabs}
                        variant={md ? "fullWidth" : "standard"}
                        indicatorColor="primary"
                        textColor="primary">
                        {sections.map((s, i) => <Tab key={i} label={s.label} wrapped />)}
                    </Tabs>
                </Paper>
            </Grid>
            <Grid item xs={12} className={classes.categoryChildren}>
                {children}
            </Grid>
        </Grid>


    }

    ActivationFeeCategoriesPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        error: selectUpdateError(),
        success: selectSuccess()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ActivationFeeCategoriesPage);

}

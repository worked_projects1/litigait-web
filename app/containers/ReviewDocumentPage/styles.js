

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    pdfView: {
        margin: 0,
        padding: 0,
        border: 0,
        width: '50%',
        height: '85vh',
        overflow: 'hidden',
    },
    imgSection: {
        marginTop: '50px'
    },
    uploadImage: {
        display: 'flex',
        width: '200px',
        margin: 'auto',
        backgroundRepeat: 'no-repeat',
    },
    uploadBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        marginRight: '15px',
        textTransform: 'none !important'
    },
    spinner: {
        padding: '20px',
        marginTop: '100px',
    },
    form: {
        display: 'inline',
        textAlign: 'center',
    },
    saveBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        marginRight: '0',
        marginLeft: '15px'
    },
    backBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        backgroundColor: 'gray !important',
    },
    children: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        padding: '18px',
        marginLeft: '20px'
    },
    message: {
        fontFamily: 'Avenir-Bold',
        fontSize: '16px',
        paddingTop: '18px',
    },
}));


export default useStyles;
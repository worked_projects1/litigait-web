import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        '& .MuiSvgIcon-root': {
            width: '1.4em !important',
            height: '1.4em !important'
        },
        '& .MuiStepConnector-alternativeLabel': {
            top: '17px !important'
        }
    }
}));

export default useStyles;
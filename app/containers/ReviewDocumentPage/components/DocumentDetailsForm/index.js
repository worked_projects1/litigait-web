/**
 * 
 * Document Details Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { ImplementationFor } from './utils';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import Error from 'components/Error';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import { normalize } from 'utils/tools';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function documentDetailsForm(props) {
  const classes = Styles();

  const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, destroy, invalid, handleBack, activeStep } = props;

  return (<div>
    <form onSubmit={handleSubmit} className={classes.form} >
      <Grid container spacing={3}>
        {(fields || []).map((field, index) => {
          const InputComponent = ImplementationFor[field.type];
          return <Grid key={index} item xs={12} style={field.style || null}>
            {field.type !== 'fieldArray' ?
              <Field
                name={field.value}
                label={field.label}
                type="text"
                metaData={metaData}
                component={InputComponent}
                required={field.required}
                disabled={field.disableOptons && field.disableOptons.edit}
                normalize={normalize(field)}
                formName={props?.form}
                formFieldChange={props?.change}
                {...field} /> :
              <FieldArray
                name={field.value}
                label={field.label}
                type="text"
                fieldArray={field.fieldArray}
                metaData={metaData}
                component={InputComponent}
                required={field.required}
                normalize={normalize(field)}
                ImplementationFor={ImplementationFor}
                disabled={field.disableOptons && field.disableOptons.edit}
                {...field} />}
          </Grid>
        })}
        <Grid item xs={12}>
          {error && <Error errorMessage={error} /> || ''}
        </Grid>
      </Grid>
      <Grid container spacing={2} direction="row" justify="flex-end" alignItems="center" className={classes.btnGrid}>
        {activeStep != 0 ? <Button
          type="button"
          variant="contained"
          color="primary"
          onClick={handleBack}
          className={classes.cancelBtn}>
          Back
        </Button> : null}
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.submitBtn}>
          {submitting && <ButtonSpinner /> || 'Next'}
        </Button>
      </Grid>
    </form>
  </div>)

}



export default reduxForm({
    form: 'documentDetailsForm',
    validate,
    enableReinitialize: true,
    touchOnChange: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(documentDetailsForm);
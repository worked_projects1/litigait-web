import InputField from 'components/InputField';
import CheckboxField from 'components/CheckboxField';
import SelectField from 'components/SelectField';
import DatePicker from 'components/DatePicker';
import FileUpload from 'components/FileUpload';
import PasswordField from 'components/PasswordField';
import StripeField from 'components/StripeField';
import PhoneNumberField from 'components/PhoneNumberField';
import TextareaField from 'components/TextareaField';
import ComponentField from 'components/ComponentField';
import MultiSelectField from 'components/MultiSelectField';
import SwitchField from 'components/SwitchField';
import TextEditorField from 'components/TextEditorField';
import Calendar from 'components/Calendar';
import AutoCompleteField from 'components/AutoCompleteField';
import MultiEmailField from 'components/MultiEmailField';
import FieldArray from 'components/FieldArray';
import MutiInputField from 'components/MutiInputField';
import SignatureForm from 'components/SignatureForm';
import CustomSignatureForm from 'components/CustomSignatureForm';
import CustomFileUpload from 'components/CustomFileUpload';

export const ImplementationFor = {
    input: InputField,
    number: InputField,
    checkbox: CheckboxField,
    select: SelectField,
    date: DatePicker,
    upload: FileUpload,
    password: PasswordField,
    stripe: StripeField,
    phone: PhoneNumberField,
    textarea: TextareaField,
    component: ComponentField,
    multiSelect: MultiSelectField,
    switch: SwitchField,
    textEditor: TextEditorField,
    calendar: Calendar,
    autoComplete: AutoCompleteField,
    multiEmail: MultiEmailField,
    fieldArray: FieldArray,
    multiInput: MutiInputField,
    signature: SignatureForm,
    customSignature: CustomSignatureForm,
    customFileUpload: CustomFileUpload
};
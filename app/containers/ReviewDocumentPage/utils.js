/**
 * 
 * Utils 
 * 
 */


/**
 * 
 * @param {object} record 
 * @param {object} formRecord 
 * @returns 
 */
export function caseTitle(record, formRecord) {
  let case_title;
  if(record && record.case_plaintiff_name && record.case_defendant_name) {
    const case_plaintiff_name = record && record.case_plaintiff_name && record.case_plaintiff_name.replace(/\s+/g, '') || false;
    const case_defendant_name = record && record.case_defendant_name && record.case_defendant_name.replace(/\s+/g, '') || false;
    let title = record && record.case_title && record.case_title.replace(/\s+/g, '') || false;
    title = title && title.trim();
    let splitStr = title && title.toLowerCase().includes('vs.') ? title.split('vs.') : title && title.toLowerCase().includes('v.') ? title.split('v.') : false;
  
    const case_title_check = splitStr && Array.isArray(splitStr) && splitStr.length > 0 && splitStr.includes(case_plaintiff_name) && splitStr.includes(case_defendant_name) || false;
  
    const plaintiff_name_edit = formRecord && formRecord.case_plaintiff_name || false;
    const defendant_name_edit = formRecord && formRecord.case_defendant_name || false;
    case_title = formRecord && case_title_check && case_title_check ? (plaintiff_name_edit && defendant_name_edit ? `${plaintiff_name_edit} v. ${defendant_name_edit}` : plaintiff_name_edit ? plaintiff_name_edit : defendant_name_edit ? defendant_name_edit : formRecord.case_title) : record.case_title;
  } else {
    const plaintiff_name = formRecord && formRecord.case_plaintiff_name || false;
    const defendant_name = formRecord && formRecord.case_defendant_name || false;
    case_title = plaintiff_name && defendant_name ? `${plaintiff_name} v. ${defendant_name}` : '';
  }
  return case_title;
}

export const clientNameSplitFunc = (record, formRecord) => {
  if (record) {
    let clientName = record && record.name && record.name.split(" ");
    let first_name, last_name, middle_name;

    if (clientName && clientName.length == 1) {
      [first_name] = clientName;
      first_name = record && record.first_name && record.first_name || first_name;
    } else if (clientName && clientName.length == 2) {
      [first_name, last_name] = clientName;
      first_name = record && record.first_name && record.first_name || first_name;
      last_name = record && record.last_name && record.last_name || last_name;
    } else if (clientName && clientName.length == 3) {
      [first_name, middle_name, last_name] = clientName;
      first_name = record && record.first_name && record.first_name || first_name;
      middle_name = record && record.middle_name && record.middle_name || middle_name;
      last_name = record && record.last_name && record.last_name || last_name;
    } else if (clientName && clientName.length > 3) {
      [first_name, middle_name, ...last_name] = clientName;
      first_name = record && record.first_name && record.first_name || first_name;
      middle_name = record && record.middle_name && record.middle_name || middle_name;
      last_name = record && record.last_name && record.last_name || last_name.join(' ');
    }
    return formRecord && formRecord || Object.assign({}, { first_name, middle_name, last_name });
  }
}
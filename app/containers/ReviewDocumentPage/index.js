/**
 * 
 * Review DocumentPage
 * 
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@material-ui/core';
import Spinner from 'components/Spinner';
import { caseTitle, clientNameSplitFunc } from './utils';
import Styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import CustomStepper from './components/Stepper';
import DocumentDetailsForm from './components/DocumentDetailsForm';
import { useDidUpdate, stateListPOS } from 'utils/tools';
import Snackbar from 'components/Snackbar';
import AlertDialog from 'components/AlertDialog';
import ModalForm from 'components/ModalRecordForm';

import { selectUser, selectActiveSession, selectForm } from 'blocks/session/selectors';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {
    const { createDocumentFormColumns, quickCreateDocumentInfoFormColumns } = columns;
    const {
        selectLoading,
        selectRecords,
        selectRecord,
        selectRecordsMetaData,
        selectError,
        selectTotalPageCount,
        selectHeaders,
        selectFilter,
        selectSuccess,
        selectUpdateError,
        selectPageCustomLoader
    } = selectors;


    /**
     * @param {object} props 
     */
    function ReviewDocumentPage(props) {

        const { loading, location = {}, dispatch, user, metaData, match, error, success, record, formRecord, pageCustomLoader } = props;
        const classes = Styles();
        const [activeStep, setActiveStep] = useState(0);
        const [formLoader, setFormLoader] = useState(false);
        const [showAlert, setShowAlert] = useState(false);
        const [dialog, setDialog] = useState(false);
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));

        const documentInfo = record?.document_data || false;
        const steps = record && record?.state && stateListPOS(record?.state) && documentInfo.defendant_practice_details ? ['Case info', 'Document Info', 'Client Info', 'Counsel Info'] : ['Case info', 'Document Info', 'Client Info'];
        const attorneys = user && metaData && metaData['users'] && metaData['users'].length > 0 && metaData['users'].reduce((acc, el) => {
            if(el && el.name && el.id === user.id) {
                acc.push(el);
            }
            return acc;
        }, []) || false;

        let documentRecord = record && documentInfo ? Object.assign({}, {
            form_0: {
                case_plaintiff_name: documentInfo?.case_info.case_plaintiff_name,
                case_defendant_name: documentInfo?.case_info.case_defendant_name,
                case_title: documentInfo?.case_info.case_title,
                case_number: documentInfo?.case_info.case_number,
            }, 
            form_1: {
                county: documentInfo?.case_info.county,
                state: documentInfo?.case_info.state,
                document_type: record && record.document_type && record.document_type.toUpperCase(),
                set_number: record?.set_number,
            }, 
            form_2: {
                attorneys: attorneys && attorneys.length > 0 ? user?.id : null
            },
            form_3: {
                opposing_counsel: documentInfo?.defendant_practice_info && typeof documentInfo?.defendant_practice_info === 'object' && Object.keys(documentInfo?.defendant_practice_info).length > 0 && [documentInfo?.defendant_practice_info] || null
            },
        }) : {};

        const public_url = record?.public_url || false;
        const formValues = formRecord && formRecord[`documentDetailForm_${record.id}_${activeStep}`] && formRecord[`documentDetailForm_${record.id}_${activeStep}`].values;
        const case_titles = formValues && documentRecord && caseTitle(documentRecord[`form_${activeStep}`], formValues);
        const clientName = documentInfo && documentInfo.client_info && documentInfo.client_info.name && clientNameSplitFunc(documentInfo.client_info, formValues) || Object.assign({}, { first_name: formValues?.first_name, middle_name: formValues?.middle_name, last_name: formValues?.last_name });

        let initialRecord = documentRecord && documentRecord[`form_${activeStep}`];
        const section = user && createDocumentFormColumns && typeof createDocumentFormColumns === 'function' ? createDocumentFormColumns(formValues, metaData, initialRecord, record).section : createDocumentFormColumns;

        let initialValues = ![1,2,3].includes(parseInt(activeStep)) ? Object.assign({}, { ...initialRecord }, { first_name: clientName?.first_name, middle_name: clientName?.middle_name, last_name: clientName?.last_name, case_title: case_titles }) : Object.assign({}, { ...initialRecord });
        const quickCreateDocument = user && quickCreateDocumentInfoFormColumns && typeof quickCreateDocumentInfoFormColumns === 'function' ? quickCreateDocumentInfoFormColumns(record).columns : createDocumentFormColumns;

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords(true));
            dispatch(actions.loadRecord(match.params.id));
            return mounted = false;
        }, [match.params.id]);

        useDidUpdate(() => {
            if (record && !documentInfo) {
                dispatch(actions.loadRecord(match.params.id));
            }
        }, [record, actions, match, dispatch]);

        const handleSubmit = (data, dispatch, { form }) => {
            if(activeStep === steps.length - 1) {
                let submitRecord = Object.assign({}, { 
                    id: record.id, 
                    questions: documentInfo?.questions, 
                    legalform_filename: record?.legalform_filename, 
                    pdf_filename: record?.pdf_filename, 
                    practice_id: record?.practice_id, 
                    user_id: record?.user_id, 
                    s3_file_key: record?.s3_file_key
                });
                dispatch(actions.saveDocumentDetails(submitRecord, form));
            } else {
                setFormLoader(true);
                setActiveStep((prevActiveStep) => prevActiveStep + 1);
                setTimeout(() => {
                    setFormLoader(false);
                }, 1000);
            }
        }
        
        const handleBack = () => {
            setFormLoader(true);
            setActiveStep((prevActiveStep) => prevActiveStep - 1);
            setTimeout(() => {
                setFormLoader(false);
            }, 1000);
        };

        const handleOnClose = () => {
            setShowAlert(true);
        }

        const handleCloseAlert = () => {
            dispatch(actions.destoryQuickCreateDocument(record, 'quickCreateDocumentForm'));
        }

        const handleConfirmAlert = () => {
            setShowAlert(false);
            setDialog(true);
        }
        
        const handleUpdate = () => {
            let submitRecord = Object.assign({}, { ...record }, { 
                extraction_id: record?.id,
                questions: documentInfo?.questions, 
                case_number: documentInfo?.case_info.case_number
            });
            dispatch(actions.saveDocumentQuestions(submitRecord));
            
        }

        const handleSaveDocumentQuestion = (data, dispatch, { form }) => {
            let submitRecord = Object.assign({}, { ...data }, { 
                extraction_id: record?.id,
                questions: documentInfo?.questions, 
                case_number: documentInfo?.case_info.case_number 
            });
            dispatch(actions.saveDocumentInfoQuestions(submitRecord, form));

        }


        if (loading || pageCustomLoader || (!error && documentRecord && Object.keys(documentRecord).length <= 0)) {
            return <Spinner className={classes.spinner} />;
        }

        return <Grid container xs={12} spacing={2}>
            <Grid item xs={!sm ? 12 : 6} className={classes.pdfView}>
                {public_url && <object height="100%" width="100%" type="application/pdf" data={public_url} />}
            </Grid>
            <Grid item xs={!sm ? 12 : 6}>
                <Grid item xs={12}>
                    <Grid className={classes.children}>
                        <CustomStepper activeStep={activeStep} steps={steps}/>
                        <Grid>
                            {formLoader ? <Spinner loading={true} className={classes.spinner} /> :  documentRecord && Object.keys(documentRecord).length > 0 ? (
                                <>
                                    {activeStep === 2 ? <Grid className={classes.messageGrid}>
                                        <Typography component="span" className={classes.message}>
                                            {`Please enter the contact information for the person providing responses`}
                                        </Typography>
                                    </Grid> : null}
                                    {activeStep === 3 ? <Grid className={classes.messageGrid}>
                                        <Typography component="span" className={classes.message}>
                                            {`We found the opposing counsel info from the uploaded document. Please review and make any corrections if required`}
                                        </Typography>
                                    </Grid> : null}
                                    <DocumentDetailsForm
                                        initialValues={initialValues || {}}
                                        form={`documentDetailForm_${record.id}_${activeStep}`}
                                        metaData={metaData}
                                        activeStep={activeStep}
                                        fields={section && section[activeStep] && section[activeStep].columns && section[activeStep].columns.filter(_ => _.editRecord && !_.disableRecord)}
                                        onSubmit={handleSubmit}
                                        keepDirtyOnReinitialize={true}
                                        handleBack={handleBack} />
                                </>
                                
                                ) : null}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {!dialog && !showAlert && record?.case_exists && record?.document_type && <AlertDialog
                description={`The case already exists, do you want to upload this file into the existing case?`}
                openDialog={true}
                onConfirm={() => handleUpdate()}
                alertOnClose={() => handleOnClose()}
                onConfirmPopUpClose
                btnLabel1='YES'
                btnLabel2='NO' />}
            {!dialog && showAlert && record?.case_exists && <AlertDialog
                description={`Do you still want to create a new case with the same case number?`}
                openDialog={true}
                onConfirm={() => handleConfirmAlert()}
                alertOnClose={() => handleCloseAlert()}
                onConfirmPopUpClose
                btnLabel1='YES'
                btnLabel2='CANCEL' />}
            {record?.case_exists && !dialog && !showAlert && !record?.document_type && <ModalForm
                initialValues={Object.assign({}, { ...record })}
                messageRegular={`The case already exists, do you want to upload this file into the existing case?`}
                fields={quickCreateDocument.filter(_ => _.editRecord && !_.disableRecord)}
                show
                form={'quickCreateDocumentForm'}
                onSubmit={handleSaveDocumentQuestion}
                metaData={metaData}
                btnLabel={'Submit'}
                onClose={() => handleOnClose()}
            />}
            {!showAlert && record?.state === 'CA' && record?.document_type.toUpperCase() === 'FROGS' && <AlertDialog
                description={`Form Interrogatories are not supported in Quick Create, please upload another type of document. Thank You`}
                openDialog={true}
                onConfirm={() => handleCloseAlert()}
                alertOnClose={() => handleCloseAlert()}
                onConfirmPopUpClose
                btnLabel1='OK' />}
            <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid>
    }

    ReviewDocumentPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        headers: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        records: PropTypes.array,
        user: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
    };

    const mapStateToProps = createStructuredSelector({
        headers: selectHeaders(),
        loading: selectLoading(),
        user: selectUser(),
        metaData: selectRecordsMetaData(),
        record: selectRecord(),
        error: selectUpdateError(),
        formRecord: selectForm(),
        pageCustomLoader: selectPageCustomLoader()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(ReviewDocumentPage);

}
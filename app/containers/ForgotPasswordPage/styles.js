

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  component: {
    padding: '25px',
  }
}));


export default useStyles;
/*
 * ForgotPasswordPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Box, Grid } from '@material-ui/core';

import { selectLoggedIn, selectUser, selectError, selectSuccess } from 'blocks/session/selectors';
import ForgotPasswordForm from 'components/ForgotPasswordForm';
import { forgotPassword, clearCache } from 'blocks/session/actions';
import Copyright from 'components/Copyright';
import useStyles from './styles';

import { Helmet } from 'react-helmet';

/**
 * @param {object} props 
 */
export function ForgotPasswordPage(props) {
    
    const { error, location, dispatch, success } = props;
    const classes = useStyles();

    let err = null;

    if (error && error.login && error.login.response
        && error.login.response.data && error.login.response.data.error) {
        const data = error.login.response.data;

        if (typeof data.error === 'string') {
            err = data['error'];
        } else {
            err = data['error'][Object.keys(data.error)[0]][0];
        }
    }

    const handleSubmit = (data, dispatch, { form }) => {
        dispatch(forgotPassword(data, form))
    }

    const clearSessionCache = () => {
        dispatch(clearCache());
    }

    return <Grid container component="main" className={classes.root}>
        <Helmet
            title="EsquireTek"
            meta={[
                { name: 'description', content: 'Forgot Password Page' },
            ]}
        />
        <div className={classes.component}>
            <ForgotPasswordForm
                form={`ForgotPasswordForm`}
                onSubmit={handleSubmit.bind(this)}
                errorMessage={err}
                clearCache={clearSessionCache}
                success={success}
                iframe
                locationState={location.state} />
            <Box mt={5}>
                <Copyright textColor='#000' />
            </Box>
        </div>
    </Grid>
}

ForgotPasswordPage.propTypes = {
    children: PropTypes.object,
    dispatch: PropTypes.func,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    history: PropTypes.object,
    location: PropTypes.object,
    loggedIn: PropTypes.bool,
    match: PropTypes.object,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
    loggedIn: selectLoggedIn(),
    user: selectUser(),
    error: selectError(),
    success: selectSuccess()
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withConnect,
    memo,
)(ForgotPasswordPage);

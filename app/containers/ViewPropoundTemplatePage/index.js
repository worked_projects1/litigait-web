

/**
 * 
 * View Propound Template Page
 * 
 */

import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import AlertDialog from '../../components/AlertDialog'
import Snackbar from 'components/Snackbar';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import ButtonSpinner from 'components/ButtonSpinner';
import Skeleton from '@material-ui/lab/Skeleton';


/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {

    const {
        selectRecord,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectPageLoader
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewPropoundTemplatePage(props) {

        const { record, error, success, dispatch, location = {}, progress, match, loading } = props;

        const classes = Styles();

        const deleteRecord = () => {
            dispatch(actions.deleteRecord(record.id));
        };

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            return () => mounted = false;
        }, [match.params.id]);


        const columValue = (column, row) => {
            switch (column.type) {
                case 'upload':
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row) :
                            (<img
                                src={row[column.value] || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                                role="presentation"
                                className={classes.img}
                            />);
                case 'checkbox':
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row) :
                            <p className={classes.value}>{row[column.value]}</p>;
                case 'select':
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row) :
                            <p className={classes.value}>{row[column.value]}</p>;
                default:
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row) :
                            <p className={classes.value}>{row[column.value] != null && row[column.value] != '' ? row[column.value] : '-'}</p>;
            }
        };

        return (
            <Grid container>
                <Grid item xs={12}>
                    <Grid container className={classes.header}>
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Link to={{ pathname: path, state: { ...location.state } }} className={classes.link}>
                                <FormattedMessage {...messages.close} />
                            </Link>}
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Grid className={classes.section}>
                                <Link to={{ pathname: `${path}/${record.id}/propoundQuestions/${record.document_type}`, state: Object.assign({}, { ...location.state }, { disableNote: true }) }} className={classes.link}>
                                    <FormattedMessage {...messages.edit} />
                                </Link>
                                <AlertDialog
                                    description={`Do you want to delete this propound template?`}
                                    onConfirm={deleteRecord}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='Delete'
                                    btnLabel2='Cancel' >
                                    {(open) => <Button type="button" variant="contained" className={classes.Button} onClick={open} >
                                        {progress && <ButtonSpinner color='#2ca01c' /> || <FormattedMessage {...messages.delete} />}
                                    </Button>}
                                </AlertDialog>
                            </Grid>}
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            {(columns || []).map((column) =>
                            (column.viewRecord ?
                                <Grid item xs={12} key={column.id}>
                                    <div>
                                        <div className={classes.label}>{column.label}:</div>
                                        {columValue(column, record)}
                                        <hr className={classes.hr} />
                                    </div>
                                </Grid> : null))}
                        </Grid>
                    </Grid>
                </Grid>
                <Snackbar show={error || success ? true : false} text={error || success} severity={error ? 'error' : 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>);
    }

    ViewPropoundTemplatePage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress()
    });

    const withConnect = connect(
        mapStateToProps,
    );

    return compose(
        withConnect,
        memo
    )(ViewPropoundTemplatePage);

}
/*
 * ViewPropoundPage Messages
 *
 * This contains all the text for the ViewPropoundPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewPropoundPage';

export default defineMessages({
    edit: {
        id: `${scope}.edit`,
        defaultMessage: 'Edit',
    },
    delete: {
        id: `${scope}.delete`,
        defaultMessage: 'Delete',
    },
    close: {
        id: `${scope}.close`,
        defaultMessage: 'Close',
    }
});

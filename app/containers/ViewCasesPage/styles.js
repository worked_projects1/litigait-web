import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    casesPage: {
        marginTop: '85px',
        '@global': {
            '.MuiTypography-subtitle1': {
                fontSize: '0.84rem'
            }
        }
    },
    paper: {
        flexGrow: 1,
        maxWidth: '75%',
        maxHeight: '58px',
        boxShadow: 'none',
        '@global': {
            '.MuiTab-textColorPrimary.Mui-selected': {
                color: '#2ca01c'
            },
            '.MuiButtonBase-root': {
                width: '135px'
            },
            '.MuiButtonBase-root > span': {
                fontFamily: 'Avenir-Bold'
            },
            '.MuiTabs-indicator': {
                backgroundColor: '#2ca01c'
            }
        }
    },
    categoryChildren: {
        marginTop: '25px'
    },
    spinner: {
        padding: '20px',
        marginTop: '200px',
        marginBottom: '200px',
    },
    bodyScroll: {
        maxHeight: '500px',
        overflowY: 'auto',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 6,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },
    paperClassName: {
        minWidth: '480px !important'
    }
}));

export default useStyles;
import { AsYouType } from 'libphonenumber-js';
import moment from 'moment';

export function getStepper(type) {
    switch (type) {
        case 'uploadDoc':
            return {
                stepperId: 1,
                title: 'Upload Propounded Discovery',
                steps: ['Uploading the file', 'Processing Document', 'Extracting Questions'],
                successMessage: 'Questions Extracted'
            }
        case 'template':
            return {
                stepperId: 2,
                title: 'Generate Template',
                steps: ['Fetching Uploaded Questions', 'Generating Template', 'Template Created'],
                successMessage: 'Document Downloaded',
            }
        case 'final':
            return {
                stepperId: 3,
                title: 'Generate Final Doc',
                steps: ['Fetching Uploaded Questions', 'Generating Final Doc', 'Final Doc Created'],
                successMessage: 'Document Downloaded',
            }
    }
}



export function getFormLabel(type){
    switch (type) {
        case 'Disc001':
            return 'Unlimited Jurisdiction'; 
        case 'Disc004':
            return 'Limited Jurisdiction';
        case 'Disc002':
            return 'Employment Form';
        case 'Disc005':
            return 'Construction Form'; 
        case 'FL145':
            return 'Family Law Form';
        case 'Disc003':
            return 'Unlawful Detainer Form';
        case 'InitialDisclosure':
            return 'Initial Disclosures';
        case 'FamilyLawDisclosure':
            return 'Family Law Initial Disclosures';     
        default:
            return false;       
    }
}

export function getState(record, document_type, type) {
    const state = Object.assign({}, {...record},  { document_type });
    switch (type) {
        case 'SentToClient':
            return Object.assign({}, state, { lawyer_response_status_filter: 'All', client_response_status_filter: 'SentToClient' })
        case 'ClientResponseAvailable':
            return Object.assign({}, state, { lawyer_response_status_filter: 'All', client_response_status_filter: 'ClientResponseAvailable' })
        case 'Final':
            return Object.assign({}, state, { lawyer_response_status_filter: 'Final', client_response_status_filter: 'All' })
        default:
            return Object.assign({}, state, { lawyer_response_status_filter: 'All', client_response_status_filter: 'All' })
    }
}

export function setInitialValue(record, formValue) {
    let title = record && record.case_title || false;
    let finalRecord = Object.assign({}, {...record});
    if(finalRecord?.case_title && finalRecord?.case_plaintiff_name && finalRecord?.case_defendant_name){     
        finalRecord = finalRecord;
    } else {
        if (title) {
            let splitStr = title && title.includes('vs.') ? title.split('vs.') : title && title.includes('v.') ? title.split('v.') : false;
            let plaintiff_name;
            let defendant_name;
            if (splitStr) {
                const case_title_check = splitStr && Array.isArray(splitStr) && splitStr.length > 0 && splitStr && splitStr.map(e => e.replace(/^\s+|\s+$/gm, '')) || false;
                plaintiff_name = case_title_check && case_title_check[0] && case_title_check[0].charAt(0).toUpperCase() + case_title_check[0].substring(1) || false;
                defendant_name = case_title_check && case_title_check[1] && case_title_check[1].charAt(0).toUpperCase() + case_title_check[1].substring(1) || false;
            } else {
                plaintiff_name = finalRecord?.case_plaintiff_name;
                defendant_name = finalRecord?.case_defendant_name;
            }    
            finalRecord = Object.assign({}, { ...record }, { case_plaintiff_name: plaintiff_name || null, case_defendant_name: defendant_name || null })
        } else {
            const clientCaseInfoForm = formValue && Object.keys(formValue).length > 0 && formValue || {};
            const plaintiff_name = clientCaseInfoForm && clientCaseInfoForm.case_plaintiff_name || false;
            const defendant_name = clientCaseInfoForm && clientCaseInfoForm.case_defendant_name || false;
            const case_title = plaintiff_name && defendant_name ? `${plaintiff_name} v. ${defendant_name}` : plaintiff_name ? plaintiff_name : defendant_name ? defendant_name : '';
            finalRecord = Object.assign({}, { ...record }, { case_title })
        }
    }
    return finalRecord;
}


export const clientWithCasesFunction = ({ data, form, dispatch, mycaseSessionDiff, actions, record, filevineDetails, clioDetails, mycase_case_from, case_from }) => {

    let submitRecord = Object.assign({}, { ...data });
    if (submitRecord && submitRecord.client_phone) {
        const phoneType = new AsYouType('US');
        phoneType.input(submitRecord.client_phone);
        const phoneVal = phoneType.getNumber().nationalNumber;
        submitRecord.client_phone = phoneVal || submitRecord.client_phone;
    }
    submitRecord.date_of_loss = submitRecord.date_of_loss && new Date(moment(submitRecord.date_of_loss).format('MM/DD/YYYY'));

    if (mycase_case_from && !mycaseSessionDiff) {
        dispatch(actions.mycaseRedirectAuthUrI(true, true));
    } else if (case_from && record && record.case_from == 'filevine' && !filevineDetails) {
        dispatch(actions.setFilevineSessionForm(true))
    } else if (case_from && record && record.case_from == 'litify') {
        dispatch(actions.createLitifyClientCase(Object.assign({}, submitRecord), form));
    } else if (case_from && record && record.case_from == 'clio' && !clioDetails) {
        dispatch(actions.clioRedirectAuthUrI(true, true));
    } else {
        dispatch(actions.createClientCase(Object.assign({}, submitRecord), form));
    }
}


const discTypes = ["Disc001", "Disc004", "Disc002", "FL145", "Disc003"];

export function getSavedTemplateData(templateData, formType){
    let savedTemplate = [];
    if(formType == 'frogs'){
        const templateName = templateData && templateData.length && templateData.filter((temp) => discTypes.includes(temp.disc_type)) || [];
        savedTemplate = templateName && templateName.length && templateName.map((c) => Object.assign({}, {label: c.label, value: c.value, disc_type : c.disc_type}));
    } else if(formType == 'idc'){
        const templateName = templateData && templateData.length && templateData.filter((temp) => temp.disc_type == "InitialDisclosure") || [];
        savedTemplate = templateName && templateName.length && templateName.map((c) => Object.assign({}, {label: c.label, value: c.value, disc_type : c.disc_type}));
    }
    return savedTemplate;
}
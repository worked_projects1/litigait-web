

/**
 *
 * View Cases Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { reset } from 'redux-form';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Styles from './styles';
import UploadBox from './components/UploadBox';
import CountBox from './components/CountBox';
import Stepper from 'components/Stepper';
import { getStepper, getFormLabel, setInitialValue, clientWithCasesFunction, getSavedTemplateData } from './utils';
import Spinner from 'components/Spinner';
import Snackbar from 'components/Snackbar';
import ModalDialog from 'components/ModalDialog';
import MedicalHistoryBox from './components/MedicalHistoryBox';
import MedicalHistoryForm from './components/MedicalHistoryForm';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import { getOffset, useDidUpdate, mycaseSessionValidation } from 'utils/tools';
import moment from 'moment';
import { selectForm } from 'blocks/session/selectors';
import ModalForm from 'components/ModalRecordForm';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import validate from './validation';
import { AsYouType } from 'libphonenumber-js';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import FilevineSessionForm from 'components/FilevineSessionForm';
import LitifyInfoDialog from 'components/LitifyInfoDialog';
import ModalAlertDialog from 'components/ModalAlertDialog';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} legalForms 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, legalForms, actions, selectors) {
    const { standardFormColumns, disclosureFormColumns, templateNameForm, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal } = columns;

    const { selectRecord, selectUpdateError, selectSuccess, selectKey, selectPageLoader, selectBilling, selectProgress, selectUser, selectRecordsMetaData, selectRecords, selectContentNotFound, selectFilevineSessionForm, selectLitifyInfoDialog } = selectors;

    /**
     * @param {object} props 
     */
    function ViewCasesPage(props) {

        const { record, dispatch, match, active, loading, error, success, history, location, billing, user = {}, metaData = {}, records = [], formDetails = {}, contentNotFound } = props;
        const classes = Styles();
        const [progress, setProgress] = useState(false);
        const [docType, setDocType] = useState(false);
        const [stepperLoader, setStepperLoader] = useState(false);
        const [position, setPosition] = useState(0);
        const { title, steps, successMessage } = progress && getStepper(progress) || {};
        const stripe = useStripe();
        const elements = useElements();
        const [medicalHistoryData, setMedicalHistory] = useState(false);
        const [standardFileName, setStandardFileName] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [summaryConfirmation, setSummaryConfirmation] = useState(false);
        const [showTemplateNameForm, setShowTemplateNameForm] = useState(false);
        const [selectedFormType, setSelectedFormType] = useState(false);
        const [mailHistoryData, setMailhistoryData] = useState(false);
        const [emailSentDate, setEmailSentDate] = useState(false);
        const breakPointWidth = '1000';
        const updateWidthAndHeight = () => {
            setWidth(window.innerWidth);
        };
        const { forms, federalForms, uploadColumns, medicalHistory, medicalHistoryColumns, billingForm, changeFormColumns, editFormColumns, calendarDaysColumns, newYorkForms } = legalForms(record || {}, metaData || []);
        const formsType = record && record.federal ? federalForms : record ? record.state && record.state === 'NY' ? newYorkForms : forms : [] || [];
        const { plan_type } = user;
        const planType = plan_type && typeof plan_type === 'object' && plan_type['responding'] || false;
        const medicalHistorySingalComment = medicalHistoryData && medicalHistoryData.length > 0 && medicalHistoryData.filter(el => el.note);
        const medicalHistoryFiles = medicalHistorySingalComment && medicalHistorySingalComment.length > 0 ? medicalHistoryData.map(el => Object.assign({}, el, { comment: medicalHistorySingalComment[0].comment })) : medicalHistoryData || false;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));

        const templateData = metaData && metaData.templateNameOptions || [];
        const savedTemplate = record && record.state && record.state === 'CA' ? getSavedTemplateData(templateData, selectedFormType) : metaData && metaData.templateNameOptions || false;
        const useSavedTemplate = savedTemplate && savedTemplate.length > 0 ? true : false;
        // const useSavedTemplate = metaData && metaData.templateNameOptions && metaData.templateNameOptions.length > 0 ? true : false;
        const clientCaseColumns = metaData && user && record && clientCaseForm && typeof clientCaseForm === 'function' && clientCaseForm() && clientCaseForm(record, user, metaData).columns;
        const quickCreateClientCaseColumns = metaData && user && record && quickCreateClientCaseForm && typeof quickCreateClientCaseForm === 'function' && quickCreateClientCaseForm() && quickCreateClientCaseForm(record).columns;
        const case_from = record && record.case_from && ['filevine', 'mycase', 'quick_create', 'litify', 'clio'].includes(record.case_from) || false;
        const clientCaseField = case_from && record && record.case_from == 'quick_create' && quickCreateClientCaseColumns || clientCaseColumns;
        const validation = validate && record && typeof validate === 'function' && validate(record, clientCaseField);
        const clientCaseFormValue = formDetails && formDetails[`client_case_${record.id}`] && formDetails[`client_case_${record.id}`].values;
        const initialRecord = record && setInitialValue && setInitialValue(record, clientCaseFormValue);
        const showRequiredFieldsPopup = case_from && validation && Object.keys(validation) && Object.keys(validation).length > 0 || false;
        const mycase_case_from = case_from && record && record.case_from == 'mycase' || false;
        const { mycaseSessionDiff } = user && mycaseSessionValidation(user);
        const filevineDetails = user && user.filevineDetails || false;
        const clioDetails = user && user.clioDetails || false;

        useEffect(() => {
            dispatch(actions.loadRecords());
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.autoSaveForm(Object.assign({}, { case_id: match.params.id }), ['detailForm_', 'objectionForm_']));
            window.addEventListener('resize', () => setPosition(getOffset('esquiretek-cases-header')));
            window.addEventListener("resize", updateWidthAndHeight);
            return () => window.removeEventListener("resize", updateWidthAndHeight);
        }, [match.params.id]);

        // useDidUpdate(() => {
        //     if (records && records.length > 0 && record && !record.frogs) {
        //         dispatch(actions.loadRecord(match.params.id));
        //     }
        // }, [records, match, actions, dispatch])

        const handleStandardForm = (Modal, standard_type) => {
            history.push({
                pathname: `${path}/${record.id}/standardForm`,
                state: Object.assign({}, { ...location.state }, { filename: standardFileName || false, questions: standardFormColumns[standard_type], formLabel: getFormLabel(standard_type), defaultQuestions: [], document_type : selectedFormType })
            });
        }

        const handleDisclosureForm = (Modal, disclosure_type, formType) => {
            history.push({
                pathname: `${path}/${record.id}/standardForm`,
                state: Object.assign({}, { ...location.state }, { filename: standardFileName || false, questions: disclosureFormColumns[disclosure_type], formLabel: getFormLabel(disclosure_type), defaultQuestions: [], document_type : formType || selectedFormType })
            });
        }

        const standardFormOptions = [
            {
                action: handleStandardForm,
                value: 'Disc001',
                label: 'Unlimited Jurisdiction (DISC-001)'
            },
            {
                action: handleStandardForm,
                value: 'Disc004',
                label: 'Limited Jurisdiction (DISC-004)'
            },
            {
                action: handleStandardForm,
                value: 'Disc002',
                label: 'Employment Form (DISC-002)'
            },
            {
                action: handleStandardForm,
                value: 'Disc005',
                label: 'Construction Form (DISC-005)'
            },
            {
                action: handleStandardForm,
                value: 'FL145',
                label: 'Family Law Form (FL-145)'
            },
            {
                action: handleStandardForm,
                value: 'Disc003',
                label: 'Unlawful Detainer Form (DISC-003)'
            }
        ];

        const getdisclosureFormOptions = () => {
            if (record.state && record.state === 'CA') {
                if(useSavedTemplate){
                    return [{
                        action: handleDisclosureForm,
                        value: 'CAInitialDisclosure',
                        label: 'General Initial Disclosures'
                    },
                    {
                        action: () => setShowTemplateNameForm(true),
                        value: 'UseSavedTemplates',
                        label: 'Use Saved Templates'
                    }
                    ]
                }else {
                    return [{
                        action: handleDisclosureForm,
                        value: 'CAInitialDisclosure',
                        label: 'General Initial Disclosures'
                    }
                    ]
                }              
            } else {
                return useSavedTemplate ? [
                    {
                        action: handleDisclosureForm,
                        value: 'InitialDisclosure',
                        label: 'General Initial Disclosures'
                    },
                    {
                        action: handleDisclosureForm,
                        value: 'FamilyLawDisclosure',
                        label: 'Family Law Initial Disclosures'
                    },
                    {
                        action: () => setShowTemplateNameForm(true),
                        value: 'UseSavedTemplates',
                        label: 'Use Saved Templates'
                    },
                ] : [
                    {
                        action: handleDisclosureForm,
                        value: 'InitialDisclosure',
                        label: 'General Initial Disclosures'
                    },
                    {
                        action: handleDisclosureForm,
                        value: 'FamilyLawDisclosure',
                        label: 'Family Law Initial Disclosures'
                    }
                ];
            }
        }
        const disclosureFormOptions = getdisclosureFormOptions();

        const handleScrollToBottom = () => {
            window.scrollTo({
                top: document.documentElement.scrollHeight,
                behavior: "smooth"
            })
        }

        const handleMedicalHistory = (data, dispatch, { form }) => {
            dispatch(actions.createMedicalHistory(data && data.medicalHistory, form, setMedicalHistory, record && record.medical_history && record.medical_history.documentStatus === 'Work in progress' ? setSummaryConfirmation : false));
        }

        const handleSubmit = (data, dispatch, { form }) => {
            if (data && data.document_type == 'medical_history') {
                const submitData = data.s3_file_key && data.s3_file_key.length > 0 && data.s3_file_key.map(file => Object.assign({}, {
                    file_name: file.name,
                    comment: '',
                    client_id: data.client_id,
                    case_id: data.id,
                    uploadFile: file,
                    summery_doc_status: 'pending'
                }));
                setMedicalHistory(submitData);
                dispatch(reset(form));
            } else {
                setProgress('uploadDoc');
                setStandardFileName(data.document_type === 'frogs' ? data.filename : false);
                dispatch(actions.uploadForm(Object.assign({}, {
                    practice_id: record.practice_id,
                    case_id: record.id,
                    client_id: record.client_id,
                    file_name: data.document_file[0].name,
                    document_file: data.document_file,
                    document_type: form.split('_')[1],
                    content_type: 'application/pdf',
                    filename: data.filename,
                    state: record.state,
                    federal: record.federal || false,
                    federal_district: record.federal_district || false
                }), form));
            }
        }

        const handleBilling = async (submitRecord, generatedType, data, dispatch, { form }) => {
            const result = await stripe.confirmCardSetup(billing.client_secret, {
                payment_method: {
                    card: elements.getElement(CardElement),
                    billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                }
            });

            dispatch(actions.updateBillingDetails(result, submitRecord, setProgress, generatedType, form));
        }

        const handleDeleteForm = (data) => {
            dispatch(actions.deleteForm(Object.assign({}, data, { case_id: record.id }), actions.loadRecord));
        }

        const handleDeleteShred = (data) => {
            dispatch(actions.shred(Object.assign({}, data, { case_id: record.id }), actions.loadRecord));
        }

        const handleDeleteDocument = (data) => {
            dispatch(actions.deleteMedicalHistory(Object.assign({}, { ...data }, { case_id: record.id })));
        }

        const handleViewDocument = (data) => {
            dispatch(actions.viewMedicalHistory(Object.assign({}, { s3_file_key: data.s3_file_key })));
        }

        const handleEditForm = (data) => {
            dispatch(actions.editForm(data));
        }

        const generateSummaryDoc = (submitRecord, generatedType) => {
            dispatch(actions.generateMedicalSummary(Object.assign({}, {
                practice_id: record.practice_id,
                case_id: record.id,
                client_id: record.client_id,
                case_number: record.case_number,
                case_title: record.case_title,
                status: 'New Request',
                assigned_to: '',
                request_date: moment().format('MM/DD/YYYY HH:mm:ss'),
                practice_name: record.practice_name,
                summery_url: '',
                plan_type: planType
            })));
        }

        const handleCloseStandardForm = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
        }


        const handleRequestHelp = () => {
            setStepperLoader(true);
            dispatch(actions.requestHelp({
                client_id: record.client_id,
                case_id: record.id,
                document_type: docType,
                document_s3_key: record.document_s3_key,
                legalforms_id: record.legalforms_id,
                hash_id : record?.hash_id
            }, setStepperLoader));
        }



        const handleCancelStepper = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
            setStepperLoader(false);
            dispatch(actions.cancelForm(Object.assign({}, {
                case_id: record.id,
                client_id: record.client_id,
                status: 'cancelled',
                document_type: docType,
                s3_file_key: record.document_s3_key,
                id: record.legalforms_id
            }), actions.loadRecord))
        }

        const handleCloseStepper = () => {
            setProgress(false);
            setStepperLoader(false);
            if(!error && !success) {
                dispatch(actions.cancelForm(Object.assign({}, {
                    case_id: record.id,
                    client_id: record.client_id,
                    status: 'cancelled',
                    document_type: docType,
                    s3_file_key: record.document_s3_key,
                    id: record.legalforms_id
                }), actions.loadRecord));
            }
        }

        const openDocument = (form, dialog, submitRecord, generatedType, freeTrialDialog) => {
            dispatch(actions.loadBillingDetails(form, dialog, setProgress, submitRecord, generatedType, false, false, freeTrialDialog));
        }

        const handleComment = (data) => {
            dispatch(actions.saveComment(data, setMedicalHistory));
        }

        const handleFrogsTemplateQuestionForm = (submitData, form) => {
            const submitRecord = Object.assign({}, { ...record }, { ...submitData });
            dispatch(actions.questionsTemplate(submitRecord, form, getFormLabel, standardFormColumns));
        }

        const handleTemplateDisclosureForm = (submitData, dispatch, { form }) => {
            const submitRecord = Object.assign({}, { ...record }, { ...submitData });
            dispatch(actions.questionsTemplate(submitRecord, form, getFormLabel, disclosureFormColumns));
        }

        const handleClientWithCases = (data, dispatch, { form }) => {
            clientWithCasesFunction(Object.assign({}, { data, form, dispatch, record, filevineDetails, clioDetails, mycaseSessionDiff, actions, mycase_case_from, case_from }));
        }

        const handleShowEmailHistory = (emailsLogData, mailSentDate) => {
            setMailhistoryData(emailsLogData);
            setEmailSentDate(mailSentDate);
        }

        if (loading || (record && (!showRequiredFieldsPopup && !record.frogs && !record.sprogs)) || (!showRequiredFieldsPopup && formsType && !formsType.filter(_ => _.viewForm).length)) {
            return <Spinner className={classes.spinner} />
        }

        return (<Grid container className={classes.casesPage} style={{ marginTop: `${position || getOffset('esquiretek-cases-header') || 0}px` }}>
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    {formsType && formsType.filter(_ => _.viewForm).map((form, key) => {
                        return (<Grid key={key} item xs={12} md={width < breakPointWidth ? 6 : 4} style={form.style || null} className={classes.cardsPadding}>
                            {record && record[form.value] && record[form.value].length > 0 ?
                                <CountBox
                                    name={name}
                                    path={path}
                                    record={record}
                                    columns={uploadColumns}
                                    changeFormColumns={changeFormColumns}
                                    editFormColumns={editFormColumns}
                                    calendarDaysColumns={calendarDaysColumns}
                                    handleEditForm={handleEditForm}
                                    handleSubmit={handleSubmit}
                                    deleteForm={handleDeleteForm}
                                    deleteShred={handleDeleteShred}
                                    progress={props.progress}
                                    docType={(type) => setDocType(type)}
                                    locationState={location.state}
                                    standardFormOptions={standardFormOptions}
                                    disclosureFormOptions={disclosureFormOptions}
                                    metaData={Object.assign({}, metaData, {templateNameOptions : savedTemplate})}
                                    setResponseDate={(r, form, setDueDate) => dispatch(actions.setResponseDate(r, form, setDueDate))}
                                    forms={formDetails}
                                    templateNameForm={templateNameForm}
                                    handleTemplateQuestionForm={handleFrogsTemplateQuestionForm}
                                    user={user}
                                    useSavedTemplate={useSavedTemplate}
                                    setSelectedFormType={setSelectedFormType}
                                    handleDisclosureForm={handleDisclosureForm}
                                    handleShowEmailHistory={handleShowEmailHistory}
                                    templateData={templateData}
                                    {...form} /> :
                                <UploadBox
                                    name={name}
                                    path={path}
                                    record={record}
                                    columns={uploadColumns}
                                    handleSubmit={handleSubmit}
                                    standardFormOptions={standardFormOptions}
                                    docType={(type) => setDocType(type)}
                                    locationState={location.state}
                                    disclosureFormOptions={disclosureFormOptions}
                                    templateNameForm={templateNameForm}
                                    handleTemplateQuestionForm={handleFrogsTemplateQuestionForm}
                                    metaData={Object.assign({}, metaData, {templateNameOptions : savedTemplate})}
                                    useSavedTemplate={useSavedTemplate}
                                    setSelectedFormType={setSelectedFormType}
                                    handleDisclosureForm={handleDisclosureForm}
                                    templateData={templateData}
                                    {...form} />}
                        </Grid>)
                    }) || null}
                    {medicalHistory.filter(_ => _.viewForm).map((form, key) => {
                        return (<Grid key={key} item xs={12} md={width < breakPointWidth ? 6 : 4} style={form.style || null}>
                            <MedicalHistoryBox
                                name={name}
                                path={path}
                                record={record}
                                columns={medicalHistoryColumns}
                                handleSubmit={handleSubmit}
                                handleDeleteDocument={handleDeleteDocument}
                                handleViewDocument={handleViewDocument}
                                standardFormOptions={standardFormOptions}
                                docType={(type) => setDocType(type)}
                                locationState={location.state}
                                progress={props.progress}
                                billingForm={billingForm}
                                type={match.params.type}
                                handleBilling={handleBilling}
                                billing={billing}
                                generateSummaryDoc={generateSummaryDoc}
                                openDocument={openDocument}
                                user={user}
                                summaryConfirmation={summaryConfirmation}
                                setSummaryConfirmation={setSummaryConfirmation}
                                handleScrollToBottom={handleScrollToBottom}
                                {...form} />
                        </Grid>)
                    })}
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Stepper
                    title={title}
                    active={active}
                    show={progress ? true : false}
                    close={handleCloseStepper}
                    steps={steps}
                    error={error}
                    success={success}
                    loader={stepperLoader}
                    disableCancelBtn={record && record.legalforms_id ? false : true}
                    errorBtnText={!contentNotFound ? "Request Help" : false}
                    errorBtnAction={!contentNotFound ? handleRequestHelp : false}
                    cancel={handleCancelStepper}
                    successMessage={successMessage} />
            </Grid>
            <Grid>
                <ModalDialog
                    title={"Error occurred while processing the document you have uploaded. Please choose Unlimited Jurisdiction or Limited Jurisdiction if you want to manually select questions from the Judicial Council Form (instead of uploading a document with selected questions)."}
                    className={classes.form}
                    options={standardFormOptions}
                    btnStyle={{ width: '270px' }}
                    onClose={handleCloseStandardForm}
                    show={error && docType == 'FROGS'} />
            </Grid>
            <Grid>
                <MedicalHistoryForm
                    initialValues={medicalHistoryData && medicalHistoryData.length > 0 ? Object.assign({}, { medicalHistory: medicalHistoryFiles }) : {}}
                    show={medicalHistoryData && medicalHistoryData.length > 0 ? true : false}
                    onSubmit={handleMedicalHistory}
                    onChange={(e) => setMedicalHistory(e && e.medicalHistory)}
                    handleClose={() => setMedicalHistory(false)}
                    handleComment={handleComment}
                />
            </Grid>
            <Grid>
                <ModalForm
                    title={'Choose Template'}
                    fields={templateNameForm.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`templateForm_${record.id}`}
                    className={classes.form}
                    show={showTemplateNameForm}
                    onClose={() => setShowTemplateNameForm(false)}
                    onSubmit={handleTemplateDisclosureForm.bind(this)}
                    btnLabel="Submit"
                    style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}
                    footerStyle={{ border: 'none' }}
                    confirmPopUpClose
                    metaData={Object.assign({}, metaData, {templateNameOptions : savedTemplate})}
                    />
            </Grid>
            <Grid>
                <ModalForm
                    initialValues={initialRecord}
                    title={'Client & Case Details'}
                    fields={clientCaseField.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`client_case_${record.id}`}
                    className={classes.form}
                    show={showRequiredFieldsPopup}
                    confirmMessage={mycase_case_from ?(!mycaseSessionDiff ? `We couldn’t update this change to your MyCase account due to session expired. Please click Activate button that will redirect to MyCase site. Once you logged in, you can try to update the case again.` : false) : false}
                    confirmButton={mycase_case_from ?(mycaseSessionDiff ? false : true) : false}
                    onSubmit={handleClientWithCases.bind(this)}
                    btnLabel="Update"
                    disableCancelBtn
                    enableSubmitBtn
                    keepDirtyOnReinitialize={true}
                    enableScroll={classes.bodyScroll}
                    footerStyle={{ border: 'none' }}
                    style={{ paddingRight: '0px' }}
                    footerCancelBtn={(StyleClasses) => {
                        return (<Link to={{ pathname: path, state: { ...location.state }}}>
                            <Button
                                type="button"
                                variant="contained"
                                className={StyleClasses.button}>
                                Cancel
                            </Button>
                        </Link>)
                    }}
                    metaData={metaData} />
            </Grid>
            <Grid>
                <ModalAlertDialog
                    description={emailSentHistoryModal}
                    show={mailHistoryData ? true : false}
                    disableSubmitBtn={true}
                    disableCancelButton={true}
                    enableScroll={classes.bodyScroll}
                    title={`Document was sent by email on ${emailSentDate && moment(emailSentDate).format('MM/DD/YY')} to the following addresses`}
                    paperClassName={classes.paperClassName}
                    mailHistoryData={mailHistoryData}
                    closeDialog={() => setMailhistoryData(false)}
                    onClose={() => setMailhistoryData(false)}
                />
            </Grid>
            <Grid>
                {!loading && <FilevineSessionForm
                    formRecord={clientCaseFormValue}
                    record={record}
                    actions={actions}
                    dispatch={dispatch}
                    createClientCase={true}
                    {...props}/>}
            </Grid>
            <Grid>
                {!loading && <LitifyInfoDialog
                    record={record}
                    actions={actions}
                    dispatch={dispatch}
                    {...props}
                />}
            </Grid>
            {!progress ? <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} /> : null}
        </Grid >);
    }

    ViewCasesPage.propTypes = {
        active: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object,
        filevineSessionForm: PropTypes.bool,
        litifyInfoDialog: PropTypes.bool,
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        active: selectKey(),
        loading: selectPageLoader(),
        billing: selectBilling(),
        progress: selectProgress(),
        user: selectUser(),
        metaData: selectRecordsMetaData(),
        records: selectRecords(),
        formDetails: selectForm(),
        contentNotFound: selectContentNotFound(),
        filevineSessionForm: selectFilevineSessionForm(),
        litifyInfoDialog: selectLitifyInfoDialog(),
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewCasesPage);

}

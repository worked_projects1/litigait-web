const validate = (values, fields) => {
  
  const requiredFields = fields.reduce((a, el) => {
    if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object')) {
      a.push(el.value)
    }
    return a;
  }, []);
  
  const errors = {};


  requiredFields.forEach(field => {
    if ((!values[field] && field != 'dob' && typeof values[field] === 'boolean') || (!values[field] && field != 'dob' && values[field] != '0')) {
      errors[field] = 'Required'
    } else if (field === 'date_of_loss') {
      errors[field] = dateValidation(values[field]);
    }
  })
  
  if (values.name && values.name.length < 4) {
    errors.name = 'The name must be at least 4 characters.'
  }

  if (values.case_title && values.case_title.length < 4) {
    errors.case_title = 'The case title must be at least 4 characters.'
  }

  return errors
}

const dateValidation = (val) => {
  var date = val;
  var result = '';
  if (!isNaN(Date.parse(date))) {

  } else {
    result = 'Invalid date'
  }
  return result
}

export default validate;

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    body: {
        paddingTop: '12px'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        fontSize: '0.84rem'
    },
    cards: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        overflow: 'auto',
        minHeight: '504px',
        '@global': {
            '.MuiCardContent-root:last-child': {
                paddingBottom: '16px'
            }
        },
        "@media (max-width: 1000px)": {
            minHeight: '502px !important',
        }
    },
    form: {
        justifyContent: 'center',
        paddingTop: '10px',
    },
    label: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontSize: '15px !important'
    },
    contentGrid: {
        minHeight: '210px'
    },
    contentLabel: {
        textTransform: 'none'
    },

}));

export default useStyles;
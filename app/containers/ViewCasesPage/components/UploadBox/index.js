
/**
 * 
 * Upload Box 
 * 
 */

import React from 'react';
import ModalForm from 'components/ModalRecordForm';
import { Grid, Button, Typography, Card, CardContent } from '@material-ui/core';
import Styles from './styles';
import ModalDialog from 'components/ModalDialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

function UploadBox({ label, value, title, content, columns, record, handleSubmit, btnLabel, btnLabel1, standardFormOptions, docType, btnLabel2, disclosureFormOptions, templateNameForm, handleTemplateQuestionForm, metaData, btnLabel3, disableCancelButton, useSavedTemplate, setSelectedFormType, formType, handleDisclosureForm, templateData }) {

    const [showTemplateNameForm, setShowTemplateNameForm] = React.useState(false);
    const classes = Styles();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const handleUpload = (modal, value) => {
        if(setSelectedFormType)  setSelectedFormType(value)
        modal();
        if (docType) {
            docType(label);
        }
    }

    const handleDefaultQuestion = (submitData, dispatch, { form}) => {
        handleTemplateQuestionForm(submitData, form);
    }

    return <Card className={classes.cards}>
        <CardContent>
            <Typography variant="subtitle1" className={classes.label} style={{ textTransform: 'none' }}>
                {title}
            </Typography>
            <Grid container className={classes.body}>
                <Grid item xs={12} className={classes.contentGrid}>
                    <Typography component="span" variant="subtitle1" style={{ textTransform: 'none' }}>
                        {content}
                    </Typography>
                </Grid>
                <Grid item xs={12} lg={12} >
                    {
                        btnLabel2 && (record.state === 'TX' || (record.state === 'CA' && value == 'idc')) ? (
                            <Grid style={{ display: 'flex' }} className={classes.form}>
                                <ModalDialog
                                    className={classes.form}
                                    style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}
                                    btnStyle={{ width: '270px' }}
                                    disableContainer
                                    options={disclosureFormOptions}>
                                    {(open) => <Grid>
                                        <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => {
                                            if (setSelectedFormType) setSelectedFormType(value);
                                            const CA_template = templateData && templateData.find((temp) => temp.disc_type == "InitialDisclosure") || false;
                                            const isCATemplateExist = CA_template && Object.keys(CA_template).length ? true : false;
                                            if (record.state === 'CA' && value === 'idc' && !isCATemplateExist) {
                                                handleDisclosureForm(false, 'CAInitialDisclosure', value);
                                            } else {
                                                open();
                                            }
                                        }}>
                                            {btnLabel2}
                                        </Button>
                                    </Grid>}
                                </ModalDialog>
                            </Grid>
                        ) : 
                        <ModalForm
                            initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value }) || {}}
                            fields={columns.filter(_ => _.editRecord)}
                            form={`forms_${value}`}
                            btnLabel="Upload"
                            className={classes.form}
                            onSubmitClose={true}
                            disableCancelButton={disableCancelButton ? true : false}
                            footerStyle={sm ? { borderTop: 'none'} : {borderTop: 'none', justifyContent: 'center'}}
                            footerBtn={() => btnLabel1 && record.state === 'CA' ? <ModalDialog
                                title={"This will create a form with questions from standard Unlimited Jurisdiction, Limited Jurisdiction, Employment Form, Construction Form, Family Law Form or Unlawful Detainer Form. Use this feature if you want to manually select questions from the Judicial Council Form (instead of uploading a document with selected questions)."}
                                className={classes.form}
                                style={sm ? { paddingTop: '10px', paddingRight: '10px' } : {paddingTop: '10px'}}
                                btnStyle={{ width: '270px' }}
                                disableContainer
                                options={standardFormOptions}>
                                {(open) => <Grid>
                                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>
                                        {btnLabel1}
                                    </Button>
                                </Grid>}
                            </ModalDialog> : null}
                            footerBtn1={() => btnLabel3 && record.state === 'CA' && useSavedTemplate ? 
                            <Grid style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}>
                              <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => setShowTemplateNameForm(true)}>
                                {btnLabel3}
                              </Button>
                            </Grid> : null}
                            onSubmit={handleSubmit.bind(this)}>
                            {(open) =>
                                <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handleUpload(open, value)}>
                                    {btnLabel}
                                </Button>}
                        </ModalForm> }
                </Grid>
            </Grid>
            <ModalForm
                title={'Choose Template'}
                fields={templateNameForm.filter(_ => _.editRecord && !_.disableRecord)}
                form={`templateForm_${record.id}`}
                show={showTemplateNameForm}
                onClose={() => setShowTemplateNameForm(false)}
                onSubmit={handleDefaultQuestion.bind(this)}
                btnLabel="Submit"
                style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}
                footerStyle={{ border: 'none' }}
                confirmPopUpClose
                metaData={metaData} />
        </CardContent>
    </Card>
}



export default UploadBox;
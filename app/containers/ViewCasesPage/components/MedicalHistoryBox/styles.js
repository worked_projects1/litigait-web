import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    body: {
        paddingTop: '12px',
        paddingBottom: '12px',
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        minWidth: '226px',
        fontSize: '0.84rem'
    },
    cards: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        overflow: 'auto',
        minHeight: '500px',
        '@global': {
            '.MuiCardContent-root:last-child': {
                paddingBottom: '16px'
            }
        },
        "@media (max-width: 1000px)": {
            minHeight: '500px !important',
        }
    },
    form: {
        justifyContent: 'center',
        marginTop: '12px'
    },
    label: {
        fontWeight: 'bold',
        textTransform: 'none !important',
        fontSize: '15px !important'
    },
    labelNoTransform: {
        textTransform: 'none !important'
    },
    alignRight: {
        textAlign: 'right'
    },
    contentLabel: {
        textTransform: 'none'
    },
    count: {
        display: 'flex',
        justifyContent: 'space-between',
        '& a:first-child': {
            marginRight: '14px',
            color: 'blue'
        }
    },
    sourceCount: {
        cursor: 'pointer',
        color: 'blue',
        fontSize: '16px'
    },
    pageCount: {
        fontSize: '16px'
    },
    sourceNumber: {
        justifyContent: 'flex-end'
    },
    btnSummary: {
        textAlign: 'center',
    },
    uploadedDocs: {
        justifyContent: 'center'
    },
    btnDocs: {
        display: 'flex',
        justifyContent: 'space-around',
        padding: '0px !important',
        paddingTop: '8px !important',
        marginTop: '10px',
        alignItems: 'center'
    },
    template: {
        color: 'gray',
        cursor: 'pointer',
        fontSize: '0.84rem'
    },
    description: {
        color: '#2ca01c'
    },
    confirmBtn: {
        color: '#2DA01D',
        fontWeight: 'bold',
        fontFamily: 'Avenir-Bold',
    }
}));

export default useStyles;
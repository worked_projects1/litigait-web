/**
 * 
 * Count Box
 * 
 */


import React from 'react';
import { Link } from 'react-router-dom';
import SVG from 'react-inlinesvg';
import ModalForm from 'components/ModalRecordForm';
import PopOverForm from 'components/PopOverForm';
import Icons from 'components/Icons';
import { Grid, Button, Typography, Card, CardContent } from '@material-ui/core';
import moment from 'moment';
import { getState, validation } from './utils';
import AlertDialog from 'components/AlertDialog';
import Styles from './styles';
import ClipLoader from 'react-spinners/ClipLoader';
import ModalDialog from 'components/ModalDialog';
import { calculateDaysBetweenDates, isPropound, isRespondEntityUrl, stateListPOS } from 'utils/tools';
import { Tooltip } from "@material-ui/core";
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';


function CountBox(props) {

  const { label, value, title, record, deleteForm, deleteShred, progress, type, locationState, columns, changeFormColumns, editFormColumns, docType, handleSubmit, handleEditForm, metaData, standardFormOptions, btnLabel1, btnLabel2, setResponseDate, calendarDaysColumns, forms, disclosureFormOptions, templateNameForm, handleTemplateQuestionForm, btnLabel3, user, disableCancelButton, useSavedTemplate, handleShowEmailHistory, setSelectedFormType, handleDisclosureForm, templateData } = props;
  const [document, setDocument] = React.useState();
  const [shredDocument, setShredDocument] = React.useState();
  const [legalFormId, setLegalFormId] = React.useState(false);
  const [visiblePopOver, setVisiblePopOver] = React.useState(false);
  const [diffDays, setDiffDays] = React.useState(false);
  const [dueDate, setDueDate] = React.useState(false);
  const [showTemplateNameForm, setShowTemplateNameForm] = React.useState(false);
  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const sm = useMediaQuery(theme.breakpoints.up('sm'));
  const classes = Styles();

  const selectedForm = legalFormId && record && record[value] && record[value].length > 0 && record[value].find(_ => _.legalform_id.toString() === legalFormId.toString()) || record && record[value] && record[value][0] || {};

  const {
    QuestionsCount,
    QuestionsRespondedByClientCount,
    QuestionsResponsesFinalizedCount,
    QuestionsSentToClientCount,
    questionsUploadedDate,
    questionSentToClientDate,
    responsesByClientDate,
    responsesFinalizedDate,
    generatedDocument,
    finalDocument,
    client_signature,
    filename,
    response_deadline_enddate,
    response_deadline_startdate,
    posDocument,
    email_sent_date,
    email_viewed_date,
    pdf_file_url,
    email_sent_to
  } = selectedForm;

  const calendarForms = selectedForm && forms && forms[`calendar_forms_${title}`] && forms[`calendar_forms_${title}`].values || false;

  const isPropoundPractice = isPropound(user, record);

  const handleDelete = () => {
    setDocument(Object.assign({}, { value, type }));
    deleteForm(Object.assign({}, { document_type: label, legalforms_id: selectedForm.legalform_id }));
  }

  const handleShred = () => {
    setShredDocument(Object.assign({}, { value, type }));
    deleteShred(Object.assign({}, { document_type: value, id: selectedForm.legalform_id }));
  }

  const handleUpload = (modal, formType) => {
    setSelectedFormType(formType)
    modal();
    if (docType) {
      docType(label);
    }
  }

  const handleLegalForm = (formData) => {
    if (formData && formData.legalFormId && selectedForm && selectedForm.legalform_id && formData.legalFormId.toString() != selectedForm.legalform_id.toString()) {
      setLegalFormId(formData.legalFormId);
      setVisiblePopOver(true);
      setTimeout(() => setVisiblePopOver(false), 8);
    }
  }

  const handleChangeDate = (value) => {
    if (value.proofOfServiceDate && value.numberOfDays && value.numberOfDays > 0) {
      const responseDueDate = moment(value.proofOfServiceDate, "YYYY-MM-DD").clone().add(value.numberOfDays, 'days');      
      setDueDate(responseDueDate);
    }
  }

  const handleSubmitDate = (submitData, dispatch, { form }) => {
      if (dueDate && moment(dueDate).format('YYYY-MM-DD') > moment(submitData.proofOfServiceDate).format('YYYY-MM-DD')) {
        const submitRecord = Object.assign({}, { start_date: moment(submitData.proofOfServiceDate).format('YYYY-MM-DD'), end_date: moment(dueDate).format('YYYY-MM-DD'), legalforms_id: selectedForm.legalform_id, case_id: record.id, client_id: record.client_id, practice_id: record.practice_id, document_type: value });
        setResponseDate(submitRecord, form, setDueDate);
      }
  }

  const handleDefaultQuestion = (submitData, dispatch, { form}) => {
    handleTemplateQuestionForm(submitData, form);
  }

  return <Card className={classes.cards}>
    <CardContent>
      <Grid container style={{ alignItems: 'center' }}>
        <Typography variant="subtitle1" className={classes.label}>
          {title}
        </Typography>
        <div className={classes.setResponseIcon}>
          <ModalForm
            initialValues={response_deadline_startdate && response_deadline_enddate ? { proofOfServiceDate: moment(response_deadline_startdate), numberOfDays: calculateDaysBetweenDates(response_deadline_enddate, response_deadline_startdate)} : {}}
            title="Set Due Date"
            fields={calendarDaysColumns.filter(_ => _.editRecord)}
            form={`calendar_forms_${title}`}
            onSubmitClose={true}
            btnLabel="Update"
            className={classes.form}
            disableContainer
            footerStyle={{ borderTop: 'none' }}
            footerBtn={() => (<Grid container justify={lg ? "flex-start" : "flex-end"} className={lg ? classes.footerContent : null}>
              <label>Due Date: </label>
              <div className={classes.dueDate}>{calendarForms && calendarForms.proofOfServiceDate && calendarForms.numberOfDays > 0 && dueDate ? moment(dueDate).format('MM/DD') : '--/--'}</div>              
              {calendarForms && calendarForms.proofOfServiceDate && calendarForms.numberOfDays !== 0 && dueDate && <label>(Calculated based on values above)</label>}
            </Grid>)}
            onChange={handleChangeDate}
            onSubmit={handleSubmitDate}
          >
            {open => (
              <span className={classes.setResponseXdays} onClick={open}>
                <Tooltip title="Set Due Date" placement="top-start">
                  <CalendarTodayIcon
                    className={classes.calendarIcon}
                    value={response_deadline_enddate && response_deadline_startdate && setDiffDays(calculateDaysBetweenDates(response_deadline_enddate, moment().format('YYYY-MM-DD')))}
                  />
                </Tooltip>
                {response_deadline_enddate && response_deadline_startdate &&
                  <Tooltip title={diffDays < 1 ? 'Response due' : diffDays == 1 ? 'Response due in ' + diffDays + ' day' : 'Response due in ' + diffDays + ' days'} placement="top-start">
                    <span className={classes.setResponseDate}>
                      <span style={{ color: (diffDays <= 3 && diffDays >= 1) ? '#FF8C00' : diffDays < 1 ? 'red' : diffDays > 3 ? 'black' : 'black' }}>{moment(response_deadline_enddate).format('MM/DD')}</span>
                      {diffDays && diffDays > 0 && <span className={classes.setnumdays}>({diffDays}d)</span>}
                    </span>
                  </Tooltip>}
              </span>
            )}
          </ModalForm>
        </div>
      </Grid>
      {record && record[value] && record[value].length > 0 ?
        <Grid container style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
          <Grid>
            <Typography variant="subtitle1">
              {filename}
              <span>
                <ModalForm
                  initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value, filename, legalFormId: selectedForm.legalform_id }) || {}}
                  fields={editFormColumns.filter(_ => _.editRecord)}
                  form={`editForms_${value}`}
                  btnLabel="Update"
                  className={classes.form}
                  onSubmitClose={true}
                  disableContainer
                  validate={(f, p) => validation(f, Object.assign({}, p, { record, value }))}
                  onSubmit={handleEditForm.bind(this)}
                  footerStyle={{ border: 'none' }}>
                  {(open) => <Icons type="Edit" className={classes.editFileName} onClick={open} />}
                </ModalForm>
              </span>
            </Typography>
          </Grid>
          {!visiblePopOver ? <PopOverForm
            initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value, legalFormId: selectedForm.legalform_id }) || {}}
            title="Select Form"
            fields={changeFormColumns.filter(_ => _.editRecord)}
            form={`files_forms_${value}`}
            className={classes.form}
            onSubmitClose={true}
            disableCancelBtn
            disableHeader
            disableFooter
            disableContainer
            onChange={handleLegalForm}
            paperStyle={{ padding: '0px', paddingRight: '10px', paddingLeft: '10px', minWidth: '150px', overflow: 'hidden' }}
            metaData={Object.assign({}, { filesOptions: record && record[value] && record[value].length > 0 && record[value].map(r => Object.assign({}, { value: r && r.legalform_id || '', label: r && r.filename || '' })) || [] })}>
            {(open) =>
              record[value].length > 1 && <Typography variant="subtitle1" className={classes.filesCount} onClick={open}>
                {record[value].length} Forms
               </Typography>}
          </PopOverForm> : null}
        </Grid> : null}
      <Grid container spacing={2} className={classes.body}>
        <Grid item xs={12} className={classes.count}>
          <Typography component="div" variant="subtitle1">
            <Link
              to={{
                pathname: isRespondEntityUrl(user, Object.assign({}, record, { value }), 'details'),
                state: Object.assign({}, locationState, { legalforms_id: selectedForm.legalform_id, tabValue: isPropoundPractice ? '1/respond' : false }, getState(record.id, label))
              }}
              className={classes.quesCount}>
              {QuestionsCount}
            </Link>
             {value && value == 'frogs' && record.state === 'TX' ? `Questions Selected` : `Questions in the uploaded doc`}
             </Typography>
          <Typography component="div" variant="subtitle1">
            Uploaded {questionsUploadedDate && moment(questionsUploadedDate).format('MM/DD') || '--/--'}
          </Typography>
        </Grid>
        <Grid item xs={12} className={classes.count}>
          <Typography component="div" variant="subtitle1">
            <Link
              to={{
                pathname: isRespondEntityUrl(user, Object.assign({}, record, { value }), 'details'),
                state: Object.assign({}, locationState, { legalforms_id: selectedForm.legalform_id, tabValue: isPropoundPractice ? '1/respond' : false }, getState(record.id, label, 'SentToClient'))
              }}
              className={classes.quesCount}>
              {QuestionsSentToClientCount}
            </Link>
             Questions sent to client
           </Typography>
          <Typography component="div" variant="subtitle1">
            Sent {questionSentToClientDate && moment(questionSentToClientDate).format('MM/DD') || '--/--'}
          </Typography>
        </Grid>
        <Grid item xs={12} className={classes.count}>
          <Typography component="div" variant="subtitle1" style={client_signature ? { display: 'flex' } : null}>
            <Link
              to={{
                pathname: isRespondEntityUrl(user, Object.assign({}, record, { value }), 'details'),
                state: Object.assign({}, locationState, { legalforms_id: selectedForm.legalform_id, tabValue: isPropoundPractice ? '1/respond' : false }, getState(record.id, label, 'ClientResponseAvailable'))
              }}
              className={classes.quesCount}>
              {QuestionsRespondedByClientCount}
            </Link>
             Responded by client
           </Typography>
          <Typography component="div" variant="subtitle1">
            Updated {responsesByClientDate && moment(responsesByClientDate).format('MM/DD') || '--/--'}
          </Typography>
        </Grid>
        <Grid item xs={12} className={classes.count}>
          <Typography component="div" variant="subtitle1">
            <Link
              to={{
                pathname: isRespondEntityUrl(user, Object.assign({}, record, { value }), 'details'),
                state: Object.assign({}, locationState, { legalforms_id: selectedForm.legalform_id, tabValue: isPropoundPractice ? '1/respond' : false }, getState(record.id, label, 'Final'))
              }}
              className={classes.quesCount}>
              {QuestionsResponsesFinalizedCount}
            </Link>
             Responses finalized
             {QuestionsResponsesFinalizedCount > 0 && client_signature && <img src={require('images/icons/pdf2.png')} id={`${value}_signature`} className={classes.attachPdf} onClick={() => window.open(client_signature, '_blank')} />}
          </Typography>
          <Typography component="div" variant="subtitle1">
            Updated {responsesFinalizedDate && moment(responsesFinalizedDate).format('MM/DD') || '--/--'}
          </Typography>
        </Grid>
        <Grid item xs={12} className={classes.count}>
          <Typography component="div" variant="subtitle1">
           {email_sent_date && <Link
              to={{
                pathname: isRespondEntityUrl(user, Object.assign({}, record, { value }), 'details'),
                state: Object.assign({}, locationState, { legalforms_id: selectedForm.legalform_id, tabValue: isPropoundPractice ? '1/respond' : false }, getState(record.id, label))
              }}
              className={classes.quesCount}>
              {QuestionsCount}
            </Link> || <Typography className={classes.quesCountTemp} component="span" variant="subtitle1">
                {0}
              </Typography>}
            {`Document Emailed`}
            {email_sent_date && <img src={require('images/icons/pdf2.png')} id={`${value}_pdf`} className={classes.attachPdf} onClick={() => window.open(pdf_file_url, '_blank')} />}
            {(email_sent_date && email_sent_to) && <img src={require('images/icons/maillog.svg')} onClick={() => handleShowEmailHistory(email_sent_to, email_sent_date)} className={classes.mailIcon} />}
          </Typography>
          <Typography component="div" variant="subtitle1">{`Sent ${email_sent_date && moment(email_sent_date).format('MM/DD') || '--/--'}, Read ${email_viewed_date && moment(email_viewed_date).format('MM/DD') || '--/--'}`}</Typography>
        </Grid>
      </Grid>
      <Grid item xs={12} className={record[value].length > 1 ? classes.btnDiv : classes.marginTop}>
        <Grid container>
          <Grid item xs={12} md={record && record[value] && record[value].length <= 4 ? 6 : 12} className={classes.formDiv}>
            <Link
              to={{
                pathname: isRespondEntityUrl(user, Object.assign({}, record, { value }), 'details'),
                state: Object.assign({}, locationState, { legalforms_id: selectedForm.legalform_id, tabValue: isPropoundPractice ? '1/respond' : false }, getState(record.id, label))
              }}
              className={classes.openForm}>
              <Button type="button" variant="contained" color="primary" className={classes.button}>
                View Form
               </Button>
            </Link>
          </Grid>
          {record && record[value] && record[value].length <= 4 ?
            <Grid item xs={12} md={6} className={classes.formDiv}>
              {
                btnLabel2 && (record.state === 'TX' || (record.state === 'CA' && value === 'idc') ) ? (
                  <Grid>
                    <ModalDialog
                      className={classes.form}
                      style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}
                      btnStyle={{ width: '270px' }}
                      disableContainer
                      options={disclosureFormOptions}>
                      {(open) => <Grid style={{ display: 'flex' }}>
                          <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => {
                            if(setSelectedFormType) setSelectedFormType(value);
                            const CA_template = templateData && templateData.find((temp) => temp.disc_type == 
                            "InitialDisclosure") || false;
                            const isCATemplateExist = CA_template && Object.keys(CA_template).length ? true : false;
                            if(record.state === 'CA' && value === 'idc' && !isCATemplateExist){
                              handleDisclosureForm(false, 'CAInitialDisclosure', value);
                            }else{
                              open();
                            }
                            
                          }}>
                            {`Add New Form`}
                          </Button>
                        </Grid>
                      }
                    </ModalDialog>
                  </Grid>
                ) : 
                <ModalForm
                  initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value }) || {}}
                  fields={columns.filter(_ => _.editRecord)}
                  form={`forms_${value}`}
                  btnLabel="Upload"
                  className={classes.form}
                  onSubmitClose={true}
                  validate={(f, p) => validation(f, Object.assign({}, p, { record, value }))}
                  footerStyle={sm ? { borderTop: 'none'} : {borderTop: 'none', justifyContent: 'center'}}
                  disableCancelButton={disableCancelButton ? true : false}
                  footerBtn={() => btnLabel1 && record.state === 'CA' ? <ModalDialog
                    title={"This will create a form with questions from standard Unlimited Jurisdiction, Limited Jurisdiction, Employment Form, Construction Form, Family Law Form or Unlawful Detainer Form. Use this feature if you want to manually select questions from the Judicial Council Form (instead of uploading a document with selected questions)."}
                    className={classes.formUpload}
                    style={sm ? { paddingTop: '10px', paddingRight: '10px' } : {paddingTop: '10px'}}
                    btnStyle={{ width: '270px' }}
                    disableContainer
                    options={standardFormOptions}>
                    {(open) => <Grid>
                      <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>
                        {btnLabel1}
                      </Button>
                    </Grid>}
                  </ModalDialog> : null}
                  footerBtn1={() => btnLabel3 && record.state === 'CA' && useSavedTemplate ? 
                    <Grid style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}>
                      <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => setShowTemplateNameForm(true)}>
                        {btnLabel3}
                      </Button>
                    </Grid> : null}
                  onSubmit={handleSubmit.bind(this)}>
                  {(open) =>
                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handleUpload(open, value)}>
                      Add New Form
                  </Button>}
                </ModalForm>
              }
            </Grid> : null}
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.icons} style={!finalDocument ? { marginTop: '28px' } : null}>
        <Grid container direction="row">
          {record.state !== 'TX' ? <Grid item xs={12} lg={4} className={classes.btnDivIcons}>
            <Typography component="span" variant="subtitle2" className={classes.template} onClick={() => generatedDocument ? window.open(generatedDocument, '_self') : null}>
              Shell: {generatedDocument ? <img src={require('images/icons/download.png')} className={classes.attachWord} /> : '-'}
            </Typography>
          </Grid> : null}
          <Grid item xs={12} lg={record && record.state && record.state === 'TX' ? 6 : 4} className={classes.btnDivIcons}>
            <Typography component="span" variant="subtitle2" className={classes.template} onClick={() => finalDocument ? window.open(finalDocument, '_self') : null}>
              Discovery: {finalDocument ? <img src={require('images/icons/download.png')} className={classes.attachWord} /> : '-'}
            </Typography>
          </Grid>
          <Grid item xs={12} lg={record && record.state && record.state === 'TX' ? 6 : 4} className={classes.btnDivIcons} >
            <AlertDialog
              description={`Deleting this form will delete all questions and answers for this form. Use this feature if you uploaded a wrong form and you want to replace it with a new form. ${record && record[value] && record[value].length > 1 ? `Do you want to delete form "${filename}"?` : 'Do you want to proceed?'}`}
              onConfirm={handleDelete}
              onConfirmPopUpClose={true}
              btnLabel1='Delete'
              btnLabel2='Cancel' >
              {(open) => <Typography component="span" variant="subtitle2" className={`${classes.template} deleteForm`} onClick={open}>
                Delete form  {document && document.value === value && document.type === type && progress && <ClipLoader color="#2ca01c" /> || <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} />}
              </Typography>}
            </AlertDialog>
          </Grid>
          {record && record.state && stateListPOS(record.state) && record.state !== 'FEDERAL' ? 
            <Grid item xs={12} lg={6} className={classes.btnDivIcons}>
              <Typography component="span" variant="subtitle2" className={classes.template} onClick={() => posDocument ? window.open(posDocument, '_self') : null}>
                Proof of Service: {posDocument ? <img src={require('images/icons/download.png')} className={classes.attachWord} /> : '-'}
              </Typography>
            </Grid> 
          : null}
          {finalDocument ? (
            <Grid item xs={12} lg={record && record.state && stateListPOS(record.state) && record.state !== 'FEDERAL' ? 6 : 12} className={classes.btnDivIcons} style={{ justifyContent: 'center' }}>
              <AlertDialog
                description={`This will permanently delete the generated discovery document. Discovery questions and responses will still be retained (use delete form to delete all data for this form). Do you want to proceed?`}
                onConfirm={handleShred}
                onConfirmPopUpClose={true}
                btnLabel1="Yes"
                btnLabel2="No">
                {open => (<Typography
                  component="span"
                  variant="subtitle2"
                  className={`${classes.template} deleteShred`}
                  onClick={open}>
                  Shred Discovery{' '}
                  {(shredDocument &&
                    shredDocument.value === value &&
                    shredDocument.type === type &&
                    progress && <ClipLoader color="#2ca01c" size={20} />) || (
                      <SVG
                        src={require('images/icons/shred.svg')}
                        className={classes.attachShred}
                      />
                    )}
                </Typography>)}
              </AlertDialog>
            </Grid>
          ) : null}
        </Grid>
      </Grid>
      <ModalForm
        title={'Choose Template'}
        fields={templateNameForm.filter(_ => _.editRecord && !_.disableRecord)}
        form={`templateForm_${record.id}`}
        className={classes.form}
        show={showTemplateNameForm}
        onClose={() => setShowTemplateNameForm(false)}
        onSubmit={handleDefaultQuestion.bind(this)}
        btnLabel="Submit"
        style={sm ? { paddingTop: '10px', paddingRight: '10px' } : {paddingTop: '10px'}}
        footerStyle={{ border: 'none' }}
        confirmPopUpClose
        metaData={metaData} />
    </CardContent>
  </Card >
}



export default CountBox;

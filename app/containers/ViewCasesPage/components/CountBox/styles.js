import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    body: {
        paddingTop: '12px',
        paddingBottom: '12px'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        fontSize: '0.84rem',
        "@media (max-width: 959px)": {
            padding: '6px 20px !important'
        }
    },
    cards: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        overflow: 'auto',
        minHeight: '504px',
        '@global': {
            '.MuiCardContent-root:last-child': {
                paddingBottom: '16px'
            }
        },
        "@media (max-width: 1000px)": {
            minHeight: '502px !important',
        }
    },
    attach: {
        width: '19px',
        height: '19px'
    },
    attachWord: {
        width: '30px',
        height: '30px',
        marginBottom: '2px'
    },
    attachDelete: {
        width: '20px',
        height: '20px',
        paddingLeft: '1px',
        marginBottom: '4px',
    },
    attachFormIcon: {
        width: '24px',
        height: '24px',
        paddingLeft: '1px',
        marginBottom: '4px',
        marginRight: '10px'
    },
    count: {
        display: 'flex',
        justifyContent: 'space-between',
        '& a:first-child': {
            marginRight: '14px',
            color: 'blue'
        }
    },
    btnDiv: {
        padding: '0px !important',
        marginTop: '14px'
    },
    formDiv: {
        display: 'flex',
        justifyContent: 'center',
        paddingTop: '4px'
    },
    btnDivIcons: {
        display: 'flex',
        justifyContent: 'center',
        padding: '0px !important',
        paddingTop: '8px !important',
        marginTop: '10px',
        alignItems: 'center'
    },
    filesCount: {
        display: 'inline-block',
        textAlign: 'right',
        cursor: 'pointer',
        color: '#4A2CFF',
        paddingLeft: '10px'
    },
    singleCount: {
        display: 'inline-block',
        textAlign: 'right',
        paddingLeft: '10px'
    },
    quesCount: {
        minWidth: '20px',
        maxWidth: '30px',
        display: 'inline-block',
        textAlign: 'right',
        cursor: 'pointer'
    },
    form: {
        display: 'inline',
        textAlign: 'center',
    },
    label: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontSize: '12px !important'
    },
    template: {
        color: 'gray',
        cursor: 'pointer',
        fontSize: '0.84rem'
    },
    openForm: {
        // display: 'flex',
        width: 'auto'
    },
    deleteLoader: {
        width: '20px',
        height: '20px'
    },
    attachPdf: {
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        marginLeft: '10px'
    },
    mailIcon: {
        width: '18px',
        height: '21px',
        cursor: 'pointer',
        marginLeft: '6px'
    },
    icons: {
        paddingRight: '12px',
        paddingLeft: '12px'
    },
    attachShred: {
        width: '30px',
        height: '30px',
        paddingLeft: '1px',
        marginBottom: '4px',
    },
    editFileName: {
        paddingLeft: '6px',
        height: '16px',
        cursor: 'pointer',
        marginBottom: '4px',
        color: '#2CA01C'
    },
    marginTop: {
        marginTop: '38px'
    },
    setResponseIcon: {
        marginLeft: 'auto',
        fontWeight: 'bold',
        display: 'flex',
        textTransform: 'none !important'
    },
    calendarIcon: {
        width: '22px',
        height: '22px',
        color: 'rgba(0, 0, 0, 0.54)',
        cursor: 'pointer'
    },
    setResponseDate: {
        marginLeft: '2px',
        fontSize: '11px !important',
        fontWeight: 'bold',
        fontFamily: 'Avenir-Regular',
        marginTop:'5px',
        cursor: 'pointer'
    },
    setResponseXdays: {
        marginLeft: '2px',
        fontSize: '11px !important',
        fontWeight: 'bold',
        fontFamily: 'Avenir-Regular',
        cursor: 'pointer',
        display: 'flex'
    }, 
    setnumdays: {
        marginLeft: '2px',
        color:'#4A2CFF'
    }, 
    dueDate: {
        fontWeight: 'bold',
        marginLeft: '5px', 
        marginRight: '5px'
    },
    footerContent: {
        position: 'relative', 
        top: '0px', 
        padding: '20px 0px',
        fontSize: '14px',
    },
    formUpload: {
        textAlign: 'center'
    },
    modalPaper: {
        width: '50% !important',
        [theme.breakpoints.down('xs')]: {
            width: '92% !important'
        }
    },
    quesCountTemp: {
        minWidth: '20px',
        maxWidth: '30px',
        display: 'inline-block',
        textAlign: 'right',
        marginRight: '14px',
        color: 'blue',
        fontSize: '0.84rem'
    }
}));

export default useStyles;

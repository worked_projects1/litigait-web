/*
 * RecordsPage Messages
 *
 * This contains all the text for the RecordsPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.RecordsPage';

export default defineMessages({
  error: {
    id: `${scope}.error`,
    defaultMessage: 'There was an error loading the resource. Please try again.',
  }
});

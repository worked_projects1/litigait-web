/**
 * 
 * Support Request Page
 * 
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import SVG from 'react-inlinesvg';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { ImplementationTable, RecordsData } from 'utils/tools';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} create 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, filterColumns, create, view) {

    const {
        selectLoading,
        selectRecords,
        selectRecordsMetaData,
        selectError
    } = selectors;


    /**
     * @param {object} props 
     */
    function SupportRequestPage(props) {
        
        const classes = Styles();
        const { dispatch, records, children, location = {}, history, error, loading, metaData = {} } = props;
        const { pathname } = location;
        const [filter, setFilter] = useState(false);
        const activeChildren = path !== pathname;
        const section = columns && typeof columns === 'object' && columns.section || false;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const TableWrapper = ImplementationTable.default;

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords(true));
            return () => mounted = false;
        }, []);

        let displayName = 'Support Request';

        if (error) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        return !activeChildren ? <Grid container>
            <Grid item xs={12}>
                <Grid container >
                    <Grid item xs={6}>
                        {filterColumns && filterColumns[name] && filterColumns[name].length > 0 ?
                            <Grid container>
                                {filterColumns[name].map((filterColumn, index) =>
                                    <Grid key={index} className={classes.dropdownGrid}>
                                        <select className={classes.filter} defaultValue={""} onChange={(e) => setFilter(Object.assign({}, { ...filter }, { [filterColumn.value]: e.target.value }))}>
                                            {filterColumn.options.map((option, i) => <option key={i} disabled={option.disabled} value={option.value}>{option.label}</option>)}
                                        </select>
                                    </Grid>)}
                            </Grid> : null}
                    </Grid>
                    <Grid item xs={filterColumns && filterColumns[name] && filterColumns[name].length > 0 ? 6 : 12} className={classes.addBtn}>
                        <Grid container justify="flex-end">
                            {create && section ?
                                <Grid className={classes.createGrid}>
                                    {section.map((tab, index) =>
                                        <div key={index} className={classes.createDiv}>
                                            <Link to={{ pathname: `${path}/create`, state: Object.assign({}, { ...location.state }, { schemaId: tab.schemaId }) }}>
                                                <Button
                                                    type="button"
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.create} >
                                                    Add New {tab.addNewLabel}
                                                </Button>
                                            </Link>
                                        </div>)}
                                </Grid> :
                                create ? <Link to={{ pathname: `${path}/create`, state: { ...location.state } }}>
                                    <Button
                                        type="button"
                                        variant="contained"
                                        color="primary"
                                        className={classes.create} >
                                        Add New {displayName}
                                    </Button>
                                </Link> : null}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} md={activeChildren ? 6 : 12}>
                {section ? section.map((tab, index) =>
                    <div key={index} className={classes.section}>
                        <Typography component="h1" variant="h5">{tab.name}</Typography>
                        <Grid className={classes.tableSection}>
                            <TableWrapper
                                schemaId={tab.schemaId}
                                records={loading ? RecordsData : tab.filter && tab.filter(records)}
                                columns={tab.columns}
                                children={activeChildren}
                                path={path}
                                name={name}
                                history={history}
                                locationState={location.state}
                                view={view}
                                sm={sm}
                                metaData={metaData}
                                pageLimit={[10, 25, 100]}
                                loading={loading}
                            />
                        </Grid>
                    </div>) :
                    <div className={classes.table}>
                        <TableWrapper
                            records={loading ? RecordsData : filter && records.filter(item => Object.keys(filter).filter(key => ((item[key] && item[key] === filter[key]) || filter[key] === '')).length === Object.keys(filter).length) || records}
                            columns={columns}
                            children={activeChildren}
                            path={path}
                            name={name}
                            history={history}
                            locationState={location.state}
                            view={view}
                            metaData={metaData}
                            sm={sm}
                            loading={loading}
                        />
                    </div>}
            </Grid>
            {activeChildren ?
                <Grid item xs={12} md={6} style={section && { marginTop: '32px' } || {}}>
                    <div className="children">
                        {children}
                    </div>
                </Grid> : null}
        </Grid> : children
    }

    SupportRequestPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        records: PropTypes.array
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        records: selectRecords(),
        metaData: selectRecordsMetaData(),
        error: selectError(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(SupportRequestPage);

}
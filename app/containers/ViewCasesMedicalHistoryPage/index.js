

/**
 *
 * View Cases MedicalHistory Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { reset } from 'redux-form';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Styles from './styles';
import Spinner from 'components/Spinner';
import Snackbar from 'components/Snackbar';
import MedicalHistoryBox from './components/MedicalHistoryBox';
import MedicalHistoryForm from './components/MedicalHistoryForm';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import { getOffset, useDidUpdate } from 'utils/tools';
import moment from 'moment';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} legalForms 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, legalForms, actions, selectors) {
    
    const { selectRecord, selectUpdateError, selectSuccess, selectKey, selectPageLoader, selectBilling, selectProgress, selectUser, selectRecordsMetaData, selectRecords } = selectors;

    /**
     * @param {object} props 
     */
    function ViewCasesMedicalHistoryPage(props) {
        
        const { record, dispatch, match, loading, error, success, history, location, billing, user = {}, metaData = {}, records = [] } = props;
        const classes = Styles();
        const [progress, setProgress] = useState(false);
        const [docType, setDocType] = useState(false);
        const stripe = useStripe();
        const elements = useElements();
        const [medicalHistoryData, setMedicalHistory] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [summaryConfirmation, setSummaryConfirmation] = useState(false);
        const breakPointWidth = '1000';
        const updateWidthAndHeight = () => {
            setWidth(window.innerWidth);
        };
        const { medicalHistory, medicalHistoryColumns, billingForm } = legalForms(record || {}, metaData || []);
        const { plan_type } = user;
        const planType = plan_type && typeof plan_type === 'object' && plan_type['responding'] || false;
        const medicalHistorySingalComment = medicalHistoryData && medicalHistoryData.length > 0 && medicalHistoryData.filter(el => el.note);
        const medicalHistoryFiles = medicalHistorySingalComment && medicalHistorySingalComment.length > 0 ? medicalHistoryData.map(el => Object.assign({}, el, { comment: medicalHistorySingalComment[0].comment })) : medicalHistoryData || false;

        useEffect(() => {
            dispatch(actions.loadRecords());
            dispatch(actions.loadRecord(match.params.id));
            window.addEventListener("resize", updateWidthAndHeight);
            return () => window.removeEventListener("resize", updateWidthAndHeight);
        }, [match.params.id]);

        // useDidUpdate(() => {
        //     if (records && records.length > 0 && record && !record.frogs) {
        //         dispatch(actions.loadRecord(match.params.id));
        //     }
        // }, [records, match, actions, dispatch])

        const handleScrollToBottom = () => {
            window.scrollTo({
                top: document.documentElement.scrollHeight,
                behavior: "smooth"
            })
        }

        const handleMedicalHistory = (data, dispatch, { form }) => {
            dispatch(actions.createMedicalHistory(data && data.medicalHistory, form, setMedicalHistory, record && record.medical_history && record.medical_history.documentStatus === 'Work in progress' ? setSummaryConfirmation : false));
        }

        const handleSubmit = (data, dispatch, { form }) => {
            if (data && data.document_type == 'medical_history') {
                const submitData = data.s3_file_key && data.s3_file_key.length > 0 && data.s3_file_key.map(file => Object.assign({}, {
                    file_name: file.name,
                    comment: '',
                    client_id: data.client_id,
                    case_id: data.id,
                    uploadFile: file,
                    summery_doc_status: 'pending'
                }));
                setMedicalHistory(submitData);
                dispatch(reset(form));
            }
        }

        const handleBilling = async (submitRecord, generatedType, data, dispatch, { form }) => {
            const result = await stripe.confirmCardSetup(billing.client_secret, {
                payment_method: {
                    card: elements.getElement(CardElement),
                    billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                }
            });

            dispatch(actions.updateBillingDetails(result, submitRecord, setProgress, generatedType, form));
        }

        const handleDeleteDocument = (data) => {
            dispatch(actions.deleteMedicalHistory(Object.assign({}, { ...data }, { case_id: record.id })));
        }

        const handleViewDocument = (data) => {
            dispatch(actions.viewMedicalHistory(Object.assign({}, { s3_file_key: data.s3_file_key })));
        }

        const generateSummaryDoc = (submitRecord, generatedType) => {
            dispatch(actions.generateMedicalSummary(Object.assign({}, {
                practice_id: record.practice_id,
                case_id: record.id,
                client_id: record.client_id,
                case_number: record.case_number,
                case_title: record.case_title,
                status: 'New Request',
                assigned_to: '',
                request_date: moment().format('MM/DD/YYYY HH:mm:ss'),
                practice_name: record.practice_name,
                summery_url: '',
                plan_type: planType
            })));
        }

        const openDocument = (form, dialog, submitRecord, generatedType, freeTrialDialog) => {
            dispatch(actions.loadBillingDetails(form, dialog, setProgress, submitRecord, generatedType, false, false, freeTrialDialog));
        }

        const handleComment = (data) => {
            dispatch(actions.saveComment(data, setMedicalHistory));
        }

        if (loading || (record && !record.frogs)) {
            return <Spinner className={classes.spinner} />
        }

        return (<Grid container className={classes.casesPage} style={{ marginTop: `30px` }}>
            <Grid item xs={12}>
                <Grid container>
                    {medicalHistory.filter(_ => _.viewForm).map((form, key) => {
                        return (<Grid key={key} item xs={12} md={width < breakPointWidth ? 6 : 4} style={form.style || null}>
                            <MedicalHistoryBox
                                name={name}
                                path={path}
                                record={record}
                                columns={medicalHistoryColumns}
                                handleSubmit={handleSubmit}
                                handleDeleteDocument={handleDeleteDocument}
                                handleViewDocument={handleViewDocument}
                                docType={(type) => setDocType(type)}
                                locationState={location.state}
                                progress={props.progress}
                                billingForm={billingForm}
                                type={match.params.type}
                                handleBilling={handleBilling}
                                billing={billing}
                                generateSummaryDoc={generateSummaryDoc}
                                openDocument={openDocument}
                                user={user}
                                summaryConfirmation={summaryConfirmation}
                                setSummaryConfirmation={setSummaryConfirmation}
                                handleScrollToBottom={handleScrollToBottom}
                                {...form} />
                        </Grid>)
                    })}
                </Grid>
            </Grid>
            <Grid>
                <MedicalHistoryForm
                    initialValues={medicalHistoryData && medicalHistoryData.length > 0 ? Object.assign({}, { medicalHistory: medicalHistoryFiles }) : {}}
                    show={medicalHistoryData && medicalHistoryData.length > 0 ? true : false}
                    onSubmit={handleMedicalHistory}
                    onChange={(e) => setMedicalHistory(e && e.medicalHistory)}
                    handleClose={() => setMedicalHistory(false)}
                    handleComment={handleComment}
                />
            </Grid>
            {!progress ? <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} /> : null}
        </Grid >);
    }

    ViewCasesMedicalHistoryPage.propTypes = {
        active: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        active: selectKey(),
        loading: selectPageLoader(),
        billing: selectBilling(),
        progress: selectProgress(),
        user: selectUser(),
        metaData: selectRecordsMetaData(),
        records: selectRecords(),
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewCasesMedicalHistoryPage);

}

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    casesPage: {
        marginTop: '85px',
        '@global': {
            '.MuiTypography-subtitle1': {
                fontSize: '0.84rem'
            }
        }
    },
    spinner: {
        padding: '20px',
        marginTop: '50px'
    },
    form: {
        justifyContent: 'center',
        marginTop: '21px'
    },
    cardsPadding: {
        padding: '8px !important'
    }

}));

export default useStyles;
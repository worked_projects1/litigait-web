
export function getStepper(type) {
    switch (type) {
        case 'uploadDoc':
            return {
                stepperId: 1,
                title: 'Upload Propounded Discovery',
                steps: ['Uploading the file', 'Processing Document', 'Extracting Questions'],
                successMessage: 'Questions Extracted'
            }
        case 'template':
            return {
                stepperId: 2,
                title: 'Generate Template',
                steps: ['Fetching Uploaded Questions', 'Generating Template', 'Template Created'],
                successMessage: 'Document Downloaded',
            }
        case 'final':
            return {
                stepperId: 3,
                title: 'Generate Final Doc',
                steps: ['Fetching Uploaded Questions', 'Generating Final Doc', 'Final Doc Created'],
                successMessage: 'Document Downloaded',
            }
    }
}



export function getFormLabel(type){
    switch (type) {
        case 'Disc001':
            return 'Unlimited Jurisdiction'; 
        case 'Disc004':
            return 'Limited Jurisdiction';
        case 'Disc002':
            return 'Employment Form';
        case 'Disc005':
            return 'Construction Form'; 
        case 'FL145':
            return 'Family Law Form';
        case 'Disc003':
            return 'Unlawful Detainer Form';
        case 'InitialDisclosure':
            return 'Initial Disclosures';
        case 'FamilyLawDisclosure':
            return 'Family Law Initial Disclosures';     
        default:
            return false;       
    }
}

/**
 * 
 * Document Button
 * 
 */

import React from 'react';
import ModalForm from 'components/ModalRecordForm';
import { Button } from '@material-ui/core';
import AlertDialog from 'components/AlertDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import { getState } from 'containers/ViewCasesPage/utils';
import { planAlertMessage } from 'utils/tools';

export default ({ columns, value, classes, updateBilling, billing, record, label, loading, generateDocument, openDocument, type, title, document, setDocument, style, disabled, user = {}, summaryConfirmation, setSummaryConfirmation }) => {

    const open = (openForm, openDialog, record, type, openFreeTrialAlert) => {
        openDocument(openForm, openDialog, record, type, openFreeTrialAlert);
        setDocument(Object.assign({}, { value, type }));
        setSummaryConfirmation(false)
    }

    const totalPages = record && record.medical_history && record.medical_history.pagesCount || 0;
    const notes = value === 'MEDICAL_HISTORY' ? `Price calculated based on ${totalPages} pages` : false;
    const planType = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['responding'] || false;
    const billingPreviousAmount = billing && billing.medicalHistory_previous_price && parseFloat(billing.medicalHistory_previous_price);

    const getNotes = () => {
        return <span>({notes})</span>
    }

    const descriptionMessage = planAlertMessage(planType, user?.subscription_details);

    return (<ModalForm
        fields={columns.filter(_ => _.editRecord)}
        form={`billingForm_${type}_${value}`}
        className={classes.form}
        footerStyle={{ borderTop: 'none' }}
        style={style}
        notes={notes}
        onSubmitClose
        onSubmitbtn
        btnLabel={`Pay $${billing && billing.price || 0}`}
        message="Please provide your credit card details to process this order. Card details will be securely saved for future orders as well."
        onSubmit={updateBilling.bind(this, getState(record, label), type)}
    >
        {(openForm) =>
            <AlertDialog
                description={descriptionMessage}
                onConfirm={() => setDocument(false)}
                onConfirmPopUpClose={true}
                closeDialog={() => setDocument(false)}
                btnLabel1='OK' >
                {(openFreeTrialAlert) => {
                    return <AlertDialog
                        description={billing && billing.custom_quote_needed ? `Thank you for your order. The uploaded documents exceed ${billing && billing.pricing_tier && billing.pricing_tier.from} pages and will require a manual review and estimate. \nPlease allow a few hours for us to conduct the review and we will email you cost and time estimate shortly.` : value === 'MEDICAL_HISTORY' && (billingPreviousAmount !== 0 && billing.previous_uploaded_docs_pagesCount !== 0) && billing.price > 0 ? `You have uploaded more documents after you ordered for your summary doc. Your updated price for this order is $${billing && billing.medical_history_total_price} (for ${billing && billing.total_uploaded_docs_pagesCount} total number of pages). You have already paid $${billing.medicalHistory_previous_price}, you will be charged the balance of $${billing.price} (for ${billing.pagesCount} number of pages)` : value === 'MEDICAL_HISTORY' && (billingPreviousAmount !== 0 && billing.previous_uploaded_docs_pagesCount !== 0) ? `You have uploaded more documents after you ordered for your summary doc. Your updated price for this order is $${billing && billing.medical_history_total_price} (for ${billing && billing.total_uploaded_docs_pagesCount} total number of pages). You have already paid $${billing.medicalHistory_previous_price}, you will be charged $${billing.price} (for ${billing.pagesCount} number of pages)` : value === 'MEDICAL_HISTORY' && (planType === 'monthly' || planType === 'yearly') && (billingPreviousAmount === 0 && billing.previous_uploaded_docs_pagesCount === 0) ? `Thank you for being a ${planType} subscriber. Your pricing will be $${billing && billing.price || 0} (for ${totalPages} number of pages). This pricing includes your 10% discount.` : `You will be charged $${billing && billing.price || 0} for your order. Do you want to continue? <br/> (${notes ? notes : null})`}
                        onConfirm={() => generateDocument(getState(record, label), type)}
                        onConfirmPopUpClose={true}
                        btnLabel1={billing && billing.custom_quote_needed ? null : 'Yes'}
                        btnLabel2={billing && billing.custom_quote_needed ? 'OK' : 'No'} >
                        {(openDialog) =>
                            <>
                                <AlertDialog
                                    description={'Have you completed adding additional docs?'}
                                    openDialog={summaryConfirmation}
                                    closeDialog={() => setSummaryConfirmation(false)}
                                    onConfirm={() => setSummaryConfirmation(false)}
                                    onConfirmPopUpClose
                                    btnLabel2='No'
                                    footerBtn={() => <Button type="button" color="primary" className={classes.confirmBtn} disabled={disabled} onClick={() => open(openForm, openDialog, getState(record, label), type, openFreeTrialAlert)}>
                                        {document && document.value === value && document.type === type && loading && <ButtonSpinner /> || 'YES'}
                                    </Button>} />
                                <Button type="button" variant="contained" color="primary" className={classes.button} disabled={disabled} onClick={() => open(openForm, openDialog, getState(record, label), type, openFreeTrialAlert)}>
                                    {document && document.value === value && document.type === type && loading && <ButtonSpinner /> || title}
                                </Button>
                            </>}
                    </AlertDialog>
                }}</AlertDialog>}
    </ModalForm>)
};


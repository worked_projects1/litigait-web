/**
 * 
 * 
 * Medical History Form
 * 
 */

import React from 'react';
import { reduxForm, Field, FieldArray } from 'redux-form';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button, FormControlLabel, Checkbox } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import SVG from 'react-inlinesvg';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import ButtonSpinner from 'components/ButtonSpinner';
import Styles from './styles';
import AlertDialog from 'components/AlertDialog';

const renderMedicalHistory = ({ fields, meta: { error, submitFailed }, handleComment, showPopup, setShowPopup }) => {

    const InputComponent = ImplementationFor['textarea'];
    const classes = Styles();
    
    return <Grid container className={classes.renderMedicalHistory}>
        {fields && fields.length > 0 ? fields.map((field, index) => {
            const record = fields.get(index);
            return (<Grid key={index} item xs={12} className={classes.medicalRow}>
                <Grid container direction="row">
                    <Grid item xs={12}>
                        <Grid container direction="column">
                            <Grid item xs={12}>
                                <Typography variant="subtitle1" >{record.file_name}</Typography>
                            </Grid>
                            {fields && fields.length > 1 && record && !record.disabled ?
                                <Grid className={classes.checkboxField}>
                                    <AlertDialog
                                        description={record.note ? `Selecting this will separate notes for the files. Do you want to continue?` : `Do you want to make this a single note for all files? Selecting this will clear all notes from other files.`}
                                        onConfirm={() => handleComment(Object.assign({}, record, { note: !record.note }))}
                                        onConfirmPopUpClose
                                        btnLabel1='Confirm'
                                        btnLabel2='Cancel'>
                                        {(open) => <FormControlLabel
                                            control={<Checkbox
                                                style={{ color: "#2ca01c" }}
                                                checked={record.note ? true : false}
                                                onChange={open} />}
                                            label={<span>{`Make this note for all files`}</span>} />}
                                    </AlertDialog>
                                </Grid> : null}  
                            <Grid container direction="row">
                                <Grid item xs={11} onClick={() => record.disabled && setShowPopup(index)}>  
                                    <Field
                                        name={`${field}.comment`}
                                        component={InputComponent}
                                        placeholder='If there are any password protected files. Please note that here.'
                                        rows={2}
                                        conditionalBlur={record.disabled ? true : false}
                                        charLimit={800}
                                    />
                                </Grid>
                                <Grid item xs={1} className={classes.deleteDiv}>
                                    <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} onClick={() => fields.remove(index)} />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <AlertDialog
                    description={`Selecting this will separate notes for the files. Do you want to continue?`}
                    openDialog={index === showPopup}
                    closeDialog={() => setShowPopup(false)}
                    onConfirm={() => {
                        handleComment(Object.assign({}, record, { note: false }))
                        setShowPopup(false)
                    }}
                    onConfirmPopUpClose
                    btnLabel1='Confirm'
                    btnLabel2='Cancel' />
            </Grid>)
        }) : <Typography variant="h6" gutterBottom component="span" style={{textAlign: 'center'}}><b>No Files Found.</b></Typography>}
    </Grid>
}

function MedicalHistoryForm(props) {
    const { show, handleClose, heading, className, style, handleSubmit, pristine, submitting, handleComment } = props;
    const classes = Styles();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const [showPopup, setShowPopup] = React.useState(false);

    return (<Grid container className={className} style={style}>
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={show || false}
            className={classes.modal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={show}>
                <Paper className={classes.paper} style={!md ? { maxWidth: '80%' } : { maxWidth: '50%' }}>
                    <form onSubmit={handleSubmit} className={classes.form} noValidate >
                        {heading ? <Grid className={classes.gridTitle}>
                            <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
                        </Grid> : null}
                        <Grid className={classes.gridList}>
                            <FieldArray 
                                name="medicalHistory" 
                                component={renderMedicalHistory} 
                                handleComment={handleComment} 
                                showPopup={showPopup}
                                setShowPopup={setShowPopup}
                            />
                        </Grid>
                        <Grid container justify="flex-end" className={classes.footer}>
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                className={classes.button}>
                                {submitting && <ButtonSpinner /> || 'Upload'}
                            </Button>
                            <Button
                                type="button"
                                variant="contained"
                                onClick={handleClose}
                                className={classes.button}>
                                Cancel
                            </Button>
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>)
}


export default reduxForm({
    form: 'medicalHistoryForm',
    enableReinitialize: true
})(MedicalHistoryForm);


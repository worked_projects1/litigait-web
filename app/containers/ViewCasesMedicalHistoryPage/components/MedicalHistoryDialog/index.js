/**
 * 
 *  Medical History Dialog
 * 
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import Styles from './styles';
import SVG from 'react-inlinesvg';
import AlertDialog from 'components/AlertDialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import ButtonSpinner from 'components/ButtonSpinner';

function MedicalHistoryDialog({ title, children, fields, metaData, className, style, btnStyle, show, onClose, onConfirmPopUpClose, heading, handleDeleteDocument, handleViewDocument, progress }) {
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [spinnerType, setSpinnerType] = useState(false);
    const [viewDoc, setViewDoc] = useState(false);
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));

    const handleClose = () => {
        setModalOpen(false);
        if (onClose) {
            onClose();
        }
    }

    const handleView = (field) => {
        setViewDoc(field.id);
        handleViewDocument(field);
    }

    const handleDelete = (field) => {
        setSpinnerType(field.id);
        handleDeleteDocument(field);
    }

    return <Grid container className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show}>
                <Paper className={classes.paper} style={!md ? { maxWidth: '80%' } : { maxWidth: '50%' }}>
                    <Grid container className={classes.header} justify="flex-end">
                        <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                    </Grid>
                    {heading ? <Grid className={classes.gridTitle}>
                        <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
                    </Grid> : null}
                    <Grid className={classes.gridList}>
                        {fields && fields.length > 0 && fields.map((field, index) => {
                            return <Grid key={index} container className={classes.gridContainer}>
                                <Grid item xs={10} style={{ paddingRight: '4px', marginTop: '10px', marginBottom: '10px' }}>
                                    <Typography variant="subtitle1" >{field.file_name}</Typography>
                                    {field.comment ? <Typography variant="subtitle1" style={{ fontSize: '14px' }}><b>Comment: </b>{field.comment}</Typography> : null}
                                </Grid>
                                <Grid item xs={2} className={classes.gridDelete}>
                                    {progress && viewDoc === field.id ? <ButtonSpinner color="#47AC39" className="terms" style={{ width: 'auto', minWidth: '0px' }} /> :
                                        <img src={require('images/icons/pdf2.png')} className={classes.attachPdf} onClick={() => handleView(field)} />}
                                    {field && field.summery_doc_status === "pending" ? <AlertDialog
                                        description={`Do you want to delete this document?`}
                                        onConfirm={() => handleDelete(field)}
                                        onConfirmPopUpClose={true}
                                        btnLabel1='Delete'
                                        btnLabel2='Cancel' >
                                        {(open) =>
                                            progress && spinnerType === field.id ? <ButtonSpinner color="#47AC39" className="terms" style={{ width: 'auto', minWidth: '0px', marginLeft: '8px' }} /> :
                                                <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} disabled onClick={open} />
                                        }
                                    </AlertDialog> : null}
                                </Grid>
                                <Grid item xs={12} ><hr className={classes.hr} /></Grid>
                            </Grid>
                        }) || <Grid>No Medical Document Found.</Grid>}
                    </Grid>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

MedicalHistoryDialog.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default MedicalHistoryDialog;
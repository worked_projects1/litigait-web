

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '12px',
        minWidth: '40%',
        maxWidth: '50%',
        outline: 'none',
        paddingBottom: '35px',
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px'
    },

    gridTitle: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitle1: {
        margin: '5px',
        marginTop: '10px',
        paddingLeft: '20px',
        paddingRight: '20px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    attachDelete: {
        width: '20px',
        height: '20px',
        cursor: 'pointer',
        marginLeft: '14px'
    },
    attachPdf: {
        width: '22px',
        height: 'auto',
        cursor: 'pointer',
        marginLeft: '10px'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none'
    },
    gridContainer: {
        width: 'auto',
        alignItems: 'center',
    },
    gridDelete: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    gridList: {
        padding: '14px',
        overflow: 'auto',
        height: 'auto',
        maxHeight: '450px'
    }
}));


export default useStyles;
/**
 * 
 *  Medical Summary Dialog
 * 
 */

 import React, { useState } from 'react';
 import Styles from './styles';
 import { Backdrop, Modal, Paper, Fade, Grid, Typography } from '@material-ui/core';
 import PropTypes from 'prop-types';
 import CloseIcon from '@material-ui/icons/Close';
 import useMediaQuery from '@material-ui/core/useMediaQuery';
 import { useTheme } from '@material-ui/core/styles';
 import { viewPDF } from 'utils/tools';

 
 function MedicalSummaryDialog(props){
     const {children, show, onClose, heading, fields, record} = props     
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const theme = useTheme();
     const md = useMediaQuery(theme.breakpoints.up('md'));
     
     const handleClose = () => {
         setModalOpen(false);
         if (onClose) {
             onClose();
         }
     }
 
     return <Grid>
         {children && children(() => setModalOpen(!showModal))}
         <Modal
             aria-labelledby="transition-modal-title"
             aria-describedby="transition-modal-description"
             open={showModal || show || false}
             className={classes.modal}
             onClose={handleClose}
             closeAfterTransition
             BackdropComponent={Backdrop}
             BackdropProps={{
                 timeout: 500,
             }}>
             <Fade in={showModal || show}>
                 <Paper className={classes.paper} style={!md ? { maxWidth: '80%' } : { maxWidth: '50%' }}>
                     <Grid container className={classes.header} justify="flex-end">
                         {heading ? <Grid className={classes.gridTitle}>
                             <Typography variant="h6" gutterBottom component="span" className={classes.title}><b>{heading || ''}</b></Typography>
                         </Grid> : null}
                         <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                     </Grid>                    
                     <Grid className={classes.gridList}>
                         {fields && fields.length > 0 && fields.map((field, index) => {
                             return <Grid key={index} container className={classes.gridContainer}>                            
                                 <Grid item xs={10} style={{ paddingRight: '4px', marginTop: '10px', marginBottom: '10px' }}>                                
                                     <Typography variant="subtitle1" >{field.file_name}</Typography>                                    
                                 </Grid>
                                 <Grid item xs={2} className={classes.gridDelete}>
                                     <img src={require('images/icons/pdf2.png')} className={classes.attachPdf} onClick={() => viewPDF(field.public_url)} />                                                                 
                                 </Grid>
                                 <Grid item xs={12} ><hr className={classes.hr} /></Grid>                                
                             </Grid>                            
                         }) || <Grid>No Medical Document Found.</Grid>}
                     </Grid>
                 </Paper>
             </Fade> 
         </Modal>
     </Grid>
 }
 MedicalSummaryDialog.propTypes = {
     children: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default MedicalSummaryDialog;
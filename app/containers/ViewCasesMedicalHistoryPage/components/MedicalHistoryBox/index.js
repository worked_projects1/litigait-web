
/**
 * 
 * Medical History Box
 * 
 */

import React, { useEffect } from 'react';
import ModalForm from 'components/ModalRecordForm';
import { Grid, Button, Typography, Card, CardContent } from '@material-ui/core';
import Styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import MedicalHistoryDialog from '../MedicalHistoryDialog';
import DocumentButton from '../DocumentButton';
import Icons from 'components/Icons';
import MedicalSummaryDialog from '../MedicalSummaryDialog';
import AlertDialog from 'components/AlertDialog';

function MedicalHistoryBox({ label, value, title, content, columns, record, handleSubmit, btnLabel, btnLabel1, btnAdditional, docType, handleDeleteDocument, handleViewDocument, progress, type, billingForm, handleBilling, billing, locationState, generateSummaryDoc, openDocument, user = {}, summaryConfirmation, setSummaryConfirmation, handleScrollToBottom }) {
    const classes = Styles();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const [document, setDocument] = React.useState();

    const medicalHistory = record && record.medical_history;

    const handleUpload = (modal) => {
        modal();
        if (docType) {
            docType(label);
        }
    }
    

    useEffect(() => {
        if(summaryConfirmation) {
            handleScrollToBottom()
        }
    }, [summaryConfirmation]);

    return <Card className={classes.cards}>
        <CardContent>
            <Typography variant="subtitle1" className={classes.label}>
                {title}
            </Typography>
            <Grid container spacing={2} className={classes.body}>
                <Grid item xs={12} style={medicalHistory && medicalHistory.documentCount > 0 ? null : { minHeight: '216px' }}>
                    <Grid container direction='row'>
                        <Grid item xs={8}>
                            <Typography component="span" variant="subtitle1" className={classes.labelNoTransform}>
                                Number of source documents
                            </Typography>
                        </Grid>
                        <Grid item xs={4} className={classes.alignRight}>
                            <MedicalHistoryDialog
                                className={classes.sourceNumber}
                                handleDeleteDocument={handleDeleteDocument}
                                handleViewDocument={handleViewDocument}
                                progress={progress}
                                showDelete={(medicalHistory && (medicalHistory.documentStatus === 'No source documents' || medicalHistory.documentStatus === 'Summary not requested')) || false}
                                fields={record && record.medical_history && record.medical_history.uploadedDocuments}>
                                {(open) =>
                                    <Typography component="span" variant="subtitle1" className={classes.sourceCount} onClick={open}>
                                        {medicalHistory && medicalHistory.documentCount || '0'}
                                    </Typography>
                                }
                            </MedicalHistoryDialog>
                        </Grid>
                    </Grid>
                    <Grid container direction='row' style={{ marginTop: '12px' }}>
                        <Grid item xs={8}>
                            <Typography component="span" variant="subtitle1" className={classes.labelNoTransform}>
                                Number of pages (in uploaded documents)
                            </Typography>
                        </Grid>
                        <Grid item xs={4} className={classes.alignRight}>
                            <Typography component="span" variant="subtitle1" className={classes.pageCount}>
                                {medicalHistory && medicalHistory.pagesCount || '0'}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction='row' style={{ marginTop: '12px' }}>
                        <Grid item xs={8}>
                            <Typography component="span" variant="subtitle1" className={classes.labelNoTransform}>
                                Status
                            </Typography>
                        </Grid>
                        <Grid item xs={4} className={classes.alignRight}>
                            <Typography component="span" variant="subtitle1" className={classes.labelNoTransform}>
                                {medicalHistory && medicalHistory.documentStatus || '0'}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
                {medicalHistory && medicalHistory.summery_documents && medicalHistory.summery_documents != '' ?
                    <Grid item xs={12} style={{ textAlign: 'center' }}>
                        <MedicalSummaryDialog
                            fields={medicalHistory && medicalHistory.summery_documents}
                            record={medicalHistory}
                            heading={'Summary Documents'}>
                            {(open) => <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>
                                {medicalHistory.summery_documents.length > 1 ? 'Download Summery Docs' : 'Download Summery Doc'}
                            </Button>}
                        </MedicalSummaryDialog>
                    </Grid> :
                    <Grid container>
                        <Grid item xs={12} lg={12} >
                        {medicalHistory && medicalHistory.documentStatus != 'No source documents' && medicalHistory.documentStatus != 'Summary not requested' ? <ModalForm
                                initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value }) || {}}
                                fields={columns.filter(item => item.editRecord)}
                                form={`forms_${value}`}
                                btnLabel="Next"
                                className={classes.form}
                                progress={progress}
                                onSubmitClose
                                onSubmit={handleSubmit.bind(this)}>
                                {(open) =>
                                    <AlertDialog
                                        description={'Currently you have an order pending for summary doc. Do you want to add additional documents to this summary doc order?'}
                                        onConfirm={() => handleUpload(open)}
                                        onConfirmPopUpClose={true}
                                        btnLabel1='Yes'
                                        btnLabel2='No' >{(openDialog) => <Button type="button" variant="contained" color="primary" className={classes.button} onClick={openDialog}>
                                            {btnAdditional}
                                        </Button>}
                                    </AlertDialog>}
                            </ModalForm>
                            :                            
                            <ModalForm
                                initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value }) || {}}
                                fields={columns.filter(item => item.editRecord)}
                                form={`forms_${value}`}
                                btnLabel="Next"
                                className={classes.form}
                                progress={progress}
                                onSubmitClose
                                onSubmit={handleSubmit.bind(this)}>
                                {(open) =>
                                    <Button type="button" variant="contained" color="primary" className={classes.button} disabled={(medicalHistory && medicalHistory.documentStatus != 'No source documents' && medicalHistory.documentStatus != 'Summary not requested') || false} onClick={() => handleUpload(open)}>
                                        {btnLabel}
                                    </Button>}
                            </ModalForm>}
                        </Grid>
                        {medicalHistory && medicalHistory.documentCount > 0 ?
                            <React.Fragment>
                                <Grid item xs={12} lg={12} className={classes.btnSummary}>
                                    <DocumentButton
                                        columns={billingForm}
                                        value={"MEDICAL_HISTORY"}
                                        classes={classes}
                                        updateBilling={handleBilling}
                                        billing={billing}
                                        record={record}
                                        style={!md && { justifyContent: 'center' } || {}}
                                        label={"MEDICAL_HISTORY"}
                                        loading={progress}
                                        generateDocument={generateSummaryDoc}
                                        openDocument={openDocument}
                                        title={btnLabel1}
                                        type="Medical"
                                        document={document}
                                        setDocument={setDocument}
                                        user={user}
                                        disabled={(medicalHistory && medicalHistory.documentStatus != 'No source documents' && medicalHistory.documentStatus != 'Summary not requested') && !medicalHistory.uploadedDocuments.map(_ => _.summery_doc_status).includes('pending') || false}
                                        summaryConfirmation={summaryConfirmation}
                                        setSummaryConfirmation={setSummaryConfirmation} />
                                </Grid>
                                <Grid item xs={12} lg={12} className={classes.btnDocs}>
                                    <MedicalHistoryDialog
                                        className={classes.uploadedDocs}
                                        handleDeleteDocument={handleDeleteDocument}
                                        handleViewDocument={handleViewDocument}
                                        progress={progress}
                                        showDelete={(medicalHistory && (medicalHistory.documentStatus === 'No source documents' || medicalHistory.documentStatus === 'Summary not requested')) || false}
                                        fields={record && record.medical_history && record.medical_history.uploadedDocuments}>
                                        {(open) => <Typography onClick={open} component="span" variant="subtitle2" className={classes.template}>
                                            See Uploaded Docs: <Icons type="Description" className={classes.description} />
                                        </Typography>}
                                    </MedicalHistoryDialog>
                                </Grid>
                            </React.Fragment>
                            : null}
                    </Grid>}
            </Grid>
        </CardContent>
    </Card>
}



export default MedicalHistoryBox;
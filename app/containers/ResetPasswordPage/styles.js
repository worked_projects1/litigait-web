

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#2ca01c',
    textTransform: 'none',
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      backgroundColor: '#2ca01c',
    }
  },
  section: {
    textAlign: 'center',
    marginTop: '60px'
  },
  title: {
    textAlign: 'center',
    marginTop: '40px'
  },
  resetGrid: {
    textAlign: 'center',
    marginTop: '20px'
  },
  resetTitle: {
    fontSize: '16px',
    textTransform: 'none'
  }
}));


export default useStyles;
/*
 * ResetPasswordPage Messages
 *
 * This contains all the text for the ResetPasswordPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ResetPasswordPage';

export default defineMessages({
    title: {
        id: `${scope}.title`,
        defaultMessage: 'EsquireTek',
    },
    login: {
        id: `${scope}.login`,
        defaultMessage: 'Go to Login',
    },
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    }
});

/*
 * 
 * Reset Password Page
 * 
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { CssBaseline, Grid, Typography, Box, Button } from '@material-ui/core';
import { Helmet } from 'react-helmet';
import { resetPassword } from 'blocks/session/actions';
import { selectError, selectSuccess } from 'blocks/session/selectors';
import ResetPasswordForm from 'components/ResetPasswordForm';
import Copyright from 'components/Copyright';
import { setAuthToken } from 'utils/api';
import { Link } from 'react-router-dom';
import Styles from './styles';
import { FormattedMessage } from 'react-intl';
import withQueryParams from 'react-router-query-params';
import messages from './messages';
import history from 'utils/history';

/**
 * @param {object} props 
 */
export function ResetPasswordPage(props) {
    
    const { success, queryParams = {} } = props;
    const { token, email } = queryParams;
    const classes = Styles();

    const handleSubmit = (data, dispatch, { form }) => {
        setAuthToken(`JWT ${token}`);
        dispatch(resetPassword(data, form));
    }

    return (
        <Grid container component="main" justify="center">
            <Helmet
                title="EsquireTek"
                meta={[
                    { name: 'description', content: 'Reset Password' },
                ]}
            />
            <CssBaseline />
            {success && !success.reset ? <Grid container justify="center">
                <Grid item xs={12} className={classes.title}>
                    <Typography component="h1" variant="h5">
                        <FormattedMessage {...messages.title} />
                    </Typography>
                </Grid>
                <Grid item xs={12} className={classes.resetGrid}>
                    <Typography component="h1" variant="h5" className={classes.resetTitle}>
                        {`Reset Password ${email && `for ${email}` || ''}`}
                    </Typography>
                </Grid>
                <Grid item md={4} sm={6} xs={8}>
                    <ResetPasswordForm
                        onSubmit={handleSubmit.bind(this)}
                        success={success} />
                </Grid>
            </Grid> : null}

            {success && success.reset ? <Grid container justify="center">
                <Grid item xs={12} className={classes.section}>
                    <Typography component="h1" variant="h5">
                        {success && success.reset}
                    </Typography>
                </Grid>
                <Grid item md={4} sm={6} xs={8} className={classes.section}>
                    <Button
                        onClick={() => {
                            if (['production', 'staging'].includes(process.env.ENV)) {
                                window.location.href = (process.env.ENV === 'production') ? `https://app.esquiretek.com` : `https://stagingapp.esquiretek.com`;
                            } else {
                                history.push({
                                    pathname: '/',
                                    state: {
                                        form: 'login'
                                    }
                                })
                            }
                        }}
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit} >
                        <FormattedMessage {...messages.login} />
                    </Button>
                </Grid>
            </Grid> : null}
            <Grid item xs={12}>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </Grid>
        </Grid>
    );
}

ResetPasswordPage.propTypes = {
    children: PropTypes.object,
    dispatch: PropTypes.func,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    history: PropTypes.object,
    location: PropTypes.object,
    match: PropTypes.object,
    pathData: PropTypes.object,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool])
};


const mapStateToProps = createStructuredSelector({
    error: selectError(),
    success: selectSuccess()
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withConnect,
    memo,
    withQueryParams()
)(ResetPasswordPage);

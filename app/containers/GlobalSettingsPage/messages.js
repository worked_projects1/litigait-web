/*
 * GlobalSettingsPage Messages
 *
 * This contains all the text for the NotFoundPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.GlobalSettingsPage';

export default defineMessages({
  edit: {
    id: `${scope}.edit`,
    defaultMessage: 'Change Pricing',
  },
  error: {
    id: `${scope}.error`,
    defaultMessage: 'There was an error loading the resource. Please try again.',
  }
});

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
        flexGrow: 1,
        maxWidth: '95%',
        maxHeight: '58px',
        boxShadow: 'none',
        '@global': {
          '.MuiTab-textColorPrimary.Mui-selected': {
            color: '#2ca01c'
          },
          '.MuiButtonBase-root': {
            width: '120px'
          },
          '.MuiButtonBase-root > span': {
            fontFamily: 'Avenir-Bold'
          },
          '.MuiTabs-indicator': {
            backgroundColor: '#2ca01c'
          }
        }
      },
}));

export default useStyles;
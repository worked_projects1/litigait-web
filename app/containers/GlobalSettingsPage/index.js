/**
 *
 * Global Settings Page
 *
 */



import React, { useEffect, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Tabs, Tab, Paper } from '@material-ui/core';
import Styles from './styles';
import { matchPath } from 'react-router-dom';
import { selectUser } from 'blocks/session/selectors';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {

    const {
        selectLoading,
        selectUpdateError,
        selectSuccess
    } = selectors;

    /**
     * @param {object} props 
     */
    function GlobalSettings(props) {

        const classes = Styles();
        const { dispatch, location = {}, children, history, user } = props;
        const selectedTab = location && location.pathname && matchPath(location.pathname, { path: `/globalSettings/:tab` }) && matchPath(location.pathname, { path: `/globalSettings/:tab` }).params.tab || false;
        const sections = columns && typeof columns === 'function' && columns(user).section || false;

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadGlobalSettings(true));
            if (!selectedTab) {
                history.push({ pathname: `${path}/0/rest/plans`, state: location.state });
            }
            return () => mounted = false;
        }, [selectedTab]);



        const handleTabs = (e, tabIndex) => {
            const currentSection = sections.find((s, i) => i === tabIndex);
            history.push({ pathname: currentSection ? `${path}/${tabIndex}${currentSection.path}` : `${path}/${tabIndex}`, state: location.state });
        }

        return <Grid container id={name}>
            <Grid item xs={12}>
                <Paper square className={classes.paper}>
                    <Tabs
                        value={parseInt(selectedTab)}
                        onChange={handleTabs}
                        variant="fullWidth"
                        indicatorColor="primary"
                        textColor="primary">
                        {sections.map((s, i) => s.admin && <Tab key={i} label={s.label} wrapped />)}
                    </Tabs>
                </Paper>
            </Grid>
            <Grid item xs={12}>
                {children}
            </Grid>
        </Grid>


    }

    GlobalSettings.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        error: selectUpdateError(),
        success: selectSuccess(),
        user: selectUser(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(GlobalSettings);

}

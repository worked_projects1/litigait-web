
/**
 *
 * Create Record Page
 *
 */


import React, { useState, useEffect, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import CreateRecordForm from 'components/CreateRecordForm';
import { set } from './utils';
import moment from 'moment';
import { AsYouType } from 'libphonenumber-js';
import { getFormValues } from 'redux-form';
import { resolveColumns } from 'utils/tools';
import lodash from 'lodash';
import { selectUser } from 'blocks/session/selectors';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, decoratorView, submitButtonView, customConfirmationMessage, customCreateButton, customDescription, userLimitCustomCreateButton, userLimitationCustomConfirmMessage, createPracticeUserBtn) {

    const { selectRecordsMetaData, selectRecords } = selectors;

    /**
     * @param {object} record 
     * @param {function} dispatch 
     * @param {object} param2 
     */
    function handleCreate(queryParams, user, checkExistingPassword, record, dispatch, { form }) {
        let submitRecord = Object.assign({}, { ...record });
        if (name === 'clients.create' || name === 'other-parties.create') {
            if (submitRecord.phone) {
                const phoneType = new AsYouType('US');
                phoneType.input(submitRecord.phone);
                const phoneVal = phoneType.getNumber().nationalNumber;
                submitRecord.phone = phoneVal || submitRecord.phone;
            }
            submitRecord.dob = submitRecord.dob && new Date(moment(submitRecord.dob).format('MM/DD/YYYY'));
        } else if (name === 'practices.create') {
            if (submitRecord.phone) {
                const phoneType = new AsYouType('US');
                phoneType.input(submitRecord.phone);
                const phoneVal = phoneType.getNumber().nationalNumber;
                submitRecord.phone = phoneVal || submitRecord.phone;
            }
            if(submitRecord.fax){
                const faxType = new AsYouType('US');
                faxType.input(record.fax);
                const faxVal = faxType.getNumber().nationalNumber;
                record.fax = faxVal || record.fax;
            }
        } else if (name === 'cases.create') {
            if(submitRecord.federal) {
                submitRecord.state = 'FEDERAL';
            }
            if(submitRecord.opposing_counsel_info) {
                submitRecord.opposing_counsel = [{
                    opposing_counsel_attorney_name: submitRecord.opposing_counsel_attorney_name, 
                    opposing_counsel_office_name: submitRecord.opposing_counsel_office_name, 
                    opposing_counsel_street: submitRecord.opposing_counsel_street, 
                    opposing_counsel_city: submitRecord.opposing_counsel_city, 
                    opposing_counsel_state: submitRecord.opposing_counsel_state, 
                    opposing_counsel_zip_code: submitRecord.opposing_counsel_zip_code, 
                    opposing_counsel_email: submitRecord.opposing_counsel_email}];
            }
            submitRecord.date_of_loss = submitRecord.date_of_loss && new Date(moment(submitRecord.date_of_loss).format('MM/DD/YYYY'));
        } else if(name === 'federalObjections.create'){               
            submitRecord.state = 'FEDERAL';
        } else if(name === 'users.create' || name === 'esquireTekUsers.create' || name === 'practiceUsers.create'){
            let userCreatedIn = (user && user.role === 'lawyer' || user.role === 'paralegal') ? 'customer_admin' : ['manager', 'superAdmin', 'operator'].includes(user.role) ? 'super_admin' : '';
            submitRecord = Object.assign({}, submitRecord, { userCreatedIn, notification: true });
        }
        (name === 'respondingTemplate.create' || name === 'propoundingTemplate.create') ? dispatch(actions.createCustomTemplate(submitRecord, form, actions.loadRecords, queryParams)) : (name === 'users.create' || name === 'practiceUsers.create') ? dispatch(actions.createUser(submitRecord, form, checkExistingPassword)) : dispatch(actions.createRecord(submitRecord, form));
    }

    /**
     * @param {object} props 
     */
    function CreateRecordPage(props) {
      
        const [loading, setLoading] = useState(true);
        const { location = {}, dispatch, metaData = {}, formRecord = {}, queryParams, user = {}, records } = props;
        const record = (Object.keys(formRecord).length > 0 && formRecord) || (location && location.state && location.state.formRecord) || {};
        const fieldsMetData =location && location.state && location.state.metaData && lodash.merge(metaData, location.state.metaData) || metaData;
        const fields = resolveColumns(columns, record, location.state, metaData).filter(_ => _.editRecord && !_.edit);
        const initialValues = set(fields);

        const client_id = initialValues && initialValues.client_id || record && record.client_id || null;
        const initialRecords = name === 'cases.create' ? Object.assign({}, initialValues, { client_id }) : initialValues
        const reInitialize = name === 'cases.create' ? true : false;
        const confirmMessage = user?.practiceDetails?.billing_type === 'limited_users_billing' ? userLimitationCustomConfirmMessage && typeof userLimitationCustomConfirmMessage === 'function' ? userLimitationCustomConfirmMessage(user, records) : false :  customConfirmationMessage && typeof customConfirmationMessage === 'function' ? customConfirmationMessage(user, records) : false;

        const [existingPasswordModal, setExistingPasswordModal] = useState(false);
        const [selectedUserRecord, setSelectedUserRecord] = useState(false);
        const [form, selectForm] = useState(false);

        const customDescriptionMessage = customDescription && typeof(customDescription) == "function" && customDescription(metaData, formRecord) || false;
       
        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords(false, queryParams));
            setLoading(true);
            setTimeout(() => setLoading(false), 200);
            return () => mounted = false;
        }, []);

        const checkExistingPassword = (record,form) => {
            setExistingPasswordModal(true);
            setSelectedUserRecord(record);
            selectForm(form);
        }

        return (<div>
            {decoratorView && React.createElement(decoratorView, props)}
            {!loading ? <CreateRecordForm
                initialValues={initialRecords}
                name={name}
                path={path}
                form={`Add_${name}`}
                metaData={fieldsMetData}
                fields={fields}
                enableReinitialize={reInitialize}
                keepDirtyOnReinitialize={reInitialize}
                onSubmit={handleCreate.bind(this, queryParams, user, checkExistingPassword)}
                locationState={location.state}
                submitButtonView={submitButtonView}
                confirmMessage={confirmMessage}
                customCreateButton={customCreateButton}
                customDescriptionMessage = {customDescriptionMessage}
                existingPasswordModal={existingPasswordModal}
                setExistingPasswordModal={setExistingPasswordModal}
                selectedUserRecord={selectedUserRecord}
                formData={form}
                actions = {actions}
                records={records} 
                userLimitCustomCreateButton={userLimitCustomCreateButton}               
                createPracticeUserBtn={createPracticeUserBtn}
                formRecord={formRecord}  
                dispatch={dispatch}             
            /> : 'Loading...'}
        </div>
        )
    }

    CreateRecordPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        formRecord: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        records: PropTypes.array
    };

    const mapStateToProps = createStructuredSelector({
        metaData: selectRecordsMetaData(),
        formRecord: getFormValues(`Add_${name}`),
        user: selectUser(),
        records: selectRecords()
    });

    const withConnect = connect(
        mapStateToProps
    );


    return compose(
        withConnect,
        memo
    )(CreateRecordPage);
}

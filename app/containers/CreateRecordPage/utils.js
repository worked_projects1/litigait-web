

/**
 * @param {array} columns 
 */
export function set(columns) {
    let Columns = columns && Object.assign([], columns);
    return Columns.reduce((a, el) => el.defaultValue ? Object.assign({}, { [el.value]: el.defaultValue }) : a, {});
}

export const clientNameDetails = (record) => {
    if (record) {
        let clientName = record && record.name && record.name.split(" ");
        let first_name, last_name, middle_name;

        let street = record?.street || record?.address;
        if (record.first_name || record.last_name || record.middle_name) {
            first_name = record && record.first_name && record.first_name;
            middle_name = record && record.middle_name && record.middle_name;
            last_name = record && record.last_name && record.last_name;
        } else if (clientName && clientName.length > 0) {
            if (clientName && clientName.length == 1) {
                [first_name] = clientName;
                first_name = first_name;
            } else if (clientName && clientName.length == 2) {
                [first_name, last_name] = clientName;
                first_name = first_name;
                last_name = last_name;
            } else if (clientName && clientName.length == 3) {
                [first_name, middle_name, last_name] = clientName;
                first_name = first_name;
                middle_name = middle_name;
                last_name = last_name;
            } else if (clientName && clientName.length > 3) {
                [first_name, middle_name, ...last_name] = clientName;
                first_name = first_name;
                middle_name = middle_name;
                last_name = last_name.join(' ');
            }
        }
        return Object.assign({}, record, { last_name, first_name, middle_name, street });
    }
}
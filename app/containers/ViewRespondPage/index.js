

/**
 *
 * View Respond Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { reset } from 'redux-form';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Styles from './styles';
import UploadBox from '../ViewCasesPage/components/UploadBox';
import CountBox from '../ViewCasesPage/components/CountBox';
import Stepper from 'components/Stepper';
import { getStepper, getFormLabel, getSavedTemplateData } from './utils';
import Spinner from 'components/Spinner';
import Snackbar from 'components/Snackbar';
import ModalDialog from 'components/ModalDialog';
import { getOffset, useDidUpdate, mycaseSessionValidation } from 'utils/tools';
import moment from 'moment';
import { selectForm } from 'blocks/session/selectors';
import validate from '../ViewCasesPage/validation';
import { AsYouType } from 'libphonenumber-js';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import ModalForm from 'components/ModalRecordForm';
import { setInitialValue, clientWithCasesFunction } from '../ViewCasesPage/utils';
import FilevineSessionForm from 'components/FilevineSessionForm';
import LitifyInfoDialog from 'components/LitifyInfoDialog';
import ModalAlertDialog from 'components/ModalAlertDialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} legalForms 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, legalForms, actions, selectors) {
    const { standardFormColumns, disclosureFormColumns, templateNameForm, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal } = columns;
    
    
    const { selectRecord, selectUpdateError, selectSuccess, selectKey, selectPageLoader, selectBilling, selectProgress, selectUser, selectRecordsMetaData, selectRecords, selectContentNotFound, selectFilevineSessionForm, selectLitifyInfoDialog, selectQuestionsNotFound } = selectors;

    /**
     * @param {object} props 
     */
    function ViewRespondPage(props) {
        const { record, dispatch, match, active, loading, error, success, history, location, billing, user = {}, metaData = {}, records = [], formDetails = {}, contentNotFound, questionsNotFound } = props;
        const classes = Styles();
        const [progress, setProgress] = useState(false);
        const [docType, setDocType] = useState(false);
        const [stepperLoader, setStepperLoader] = useState(false);
        const [position, setPosition] = useState(0);
        const { title, steps, successMessage } = progress && getStepper(progress) || {};
        const [standardFileName, setStandardFileName] = useState(false);
        const [mailHistoryData, setMailhistoryData] = useState(false);
        const [emailSentDate, setEmailSentDate] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [showTemplateNameForm, setShowTemplateNameForm] = useState(false);
        const [selectedFormType, setSelectedFormType] = useState(false);
        const breakPointWidth = '1000';
        const updateWidthAndHeight = () => {
            setWidth(window.innerWidth);
        };
        const { respondForms, federalForms, uploadColumns, changeFormColumns, editFormColumns, calendarDaysColumns, newYorkForms } = legalForms(record || {}, metaData || []);
        const formsType = record && record.federal ? federalForms : record ? record.state && record.state === 'NY' ? newYorkForms : respondForms : [] || [];
        const templateData = metaData && metaData.templateNameOptions || [];
        const savedTemplate = record && record.state && record.state === 'CA' ? getSavedTemplateData(templateData, selectedFormType) : metaData && metaData.templateNameOptions;
        // const useSavedTemplate = metaData && metaData.templateNameOptions && metaData.templateNameOptions.length > 0 ? true : false;
        const useSavedTemplate = savedTemplate && savedTemplate.length > 0 ? true : false;
        const clientCaseColumns = metaData && user && record && clientCaseForm && typeof clientCaseForm === 'function' && clientCaseForm() && clientCaseForm(record, user, metaData).columns;
        const quickCreateClientCaseColumns = metaData && user && record && quickCreateClientCaseForm && typeof quickCreateClientCaseForm === 'function' && quickCreateClientCaseForm() && quickCreateClientCaseForm(record).columns;
        const case_from = record && record.case_from && ['filevine', 'mycase', 'quick_create', 'litify'].includes(record.case_from) || false;
        const clientCaseField = case_from && record && record.case_from == 'quick_create' && quickCreateClientCaseColumns || clientCaseColumns;
        const validation = validate && record && typeof validate === 'function' && validate(record, clientCaseField);
        const clientCaseFormValue = formDetails && formDetails[`client_case_${record.id}`] && formDetails[`client_case_${record.id}`].values;
        const initialRecord = record && setInitialValue && setInitialValue(record, clientCaseFormValue);
        const showRequiredFieldsPopup = case_from && validation && Object.keys(validation) && Object.keys(validation).length > 0 || false;
        const mycase_case_from = case_from && record && record.case_from == 'mycase' || false;
        const { mycaseSessionDiff } = user && mycaseSessionValidation(user);
        const filevineDetails = user && user.filevineDetails || false;
        const clioDetails = user && user.clioDetails || false;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));

        

        const updatePosition = () => {
            setPosition(getOffset('esquiretek-cases-header'))
        }

        useEffect(() => {
            dispatch(actions.loadRecords());
            dispatch(actions.loadRecord(match.params.id));
            window.addEventListener('resize', updatePosition);
            dispatch(actions.autoSaveForm(Object.assign({}, { case_id: match.params.id }), ['detailForm_', 'objectionForm_']));
            window.addEventListener("resize", updateWidthAndHeight);
            return () => {
                window.removeEventListener("resize", () => {
                    updateWidthAndHeight();
                    updatePosition();
                });
            }
        }, [match.params.id]);

        // useDidUpdate(() => {
        //     if (records && records.length > 0 && record && !record.frogs) {
        //         dispatch(actions.loadRecord(match.params.id));
        //     }
        // }, [records, match, actions, dispatch])

        const handleStandardForm = (Modal, standard_type) => {
            history.push({
                pathname: `${path}/${record.id}/forms/respond/standardForm`,
                state: Object.assign({}, { ...location.state }, { filename: standardFileName || false, formType :  selectedFormType, questions: standardFormColumns[standard_type], formLabel: getFormLabel(standard_type), defaultQuestions: [], tabValue: '1/respond', document_type : selectedFormType })
            });
        }
        
        const handleDisclosureForm = (Modal, disclosure_type, formType) => {
            history.push({
                pathname: `${path}/${record.id}/forms/respond/standardForm`,
                state: Object.assign({}, { ...location.state }, { filename: standardFileName || false, questions: disclosureFormColumns[disclosure_type], formLabel: getFormLabel(disclosure_type), defaultQuestions: [], tabValue: '1/respond', document_type : formType || selectedFormType })
            });
        }

        const standardFormOptions = [
            {
                action: handleStandardForm,
                value: 'Disc001',
                label: 'Unlimited Jurisdiction (DISC-001)'
            },
            {
                action: handleStandardForm,
                value: 'Disc004',
                label: 'Limited Jurisdiction (DISC-004)'
            },
            {
                action: handleStandardForm,
                value: 'Disc002',
                label: 'Employment Form (DISC-002)'
            },
            {
                action: handleStandardForm,
                value: 'Disc005',
                label: 'Construction Form (DISC-005)'
            },
            {
                action: handleStandardForm,
                value: 'FL145',
                label: 'Family Law Form (FL-145)'
            },
            {
                action: handleStandardForm,
                value: 'Disc003',
                label: 'Unlawful Detainer Form (DISC-003)'
            }
        ];

        const getdisclosureFormOptions = () => {
            if (record.state && record.state === 'CA') {
                if(useSavedTemplate){
                    return [{
                        action: handleDisclosureForm,
                        value: 'CAInitialDisclosure',
                        label: 'General Initial Disclosures'
                    },
                    {
                        action: () => setShowTemplateNameForm(true),
                        value: 'UseSavedTemplates',
                        label: 'Use Saved Templates'
                    }
                    ]
                }else {
                    return [{
                        action: handleDisclosureForm,
                        value: 'CAInitialDisclosure',
                        label: 'General Initial Disclosures'
                    }
                    ]
                }
                
            } else {
                return [
                    {
                        action: handleDisclosureForm,
                        value: 'InitialDisclosure',
                        label: 'General Initial Disclosures'
                    },
                    {
                        action: handleDisclosureForm,
                        value: 'FamilyLawDisclosure',
                        label: 'Family Law Initial Disclosures'
                    }
                ]
            }
        }

        const disclosureFormOptions = getdisclosureFormOptions();

        const handleSubmit = (data, dispatch, { form }) => {
            
            setProgress('uploadDoc');
            setStandardFileName(data.document_type === 'frogs' ? data.filename : false);
            dispatch(actions.uploadForm(Object.assign({}, {
                practice_id: record.practice_id,
                case_id: record.id,
                client_id: record.client_id,
                file_name: data.document_file[0].name,
                document_file: data.document_file,
                document_type: form.split('_')[1],
                content_type: 'application/pdf',
                filename: data.filename,
                state: record.state,
                federal: record.federal || false,
                federal_district: record.federal_district || false
            }), form));
        }

        const handleTemplateDisclosureForm = (submitData, dispatch, { form }) => {
            const submitRecord = Object.assign({}, { ...record }, { ...submitData });
            dispatch(actions.questionsTemplate(submitRecord, form, getFormLabel, disclosureFormColumns));
        }

        const handleDeleteForm = (data) => {
            dispatch(actions.deleteForm(Object.assign({}, data, { case_id: record.id }), actions.loadRecord));
        }

        const handleDeleteShred = (data) => {
            dispatch(actions.shred(Object.assign({}, data, { case_id: record.id }), actions.loadRecord));
        }

        const handleEditForm = (data) => {
            dispatch(actions.editForm(data));
        }

        const handleCloseStandardForm = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
        }


        const handleRequestHelp = () => {
            setStepperLoader(true);
            dispatch(actions.requestHelp({
                client_id: record.client_id,
                case_id: record.id,
                document_type: docType,
                document_s3_key: record.document_s3_key,
                legalforms_id: record.legalforms_id,
                hash_id : record?.hash_id
            }, setStepperLoader));
        }

        const handleCancelStepper = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
            setStepperLoader(false);
            dispatch(actions.cancelForm(Object.assign({}, {
                case_id: record.id,
                client_id: record.client_id,
                status: 'cancelled',
                document_type: docType,
                s3_file_key: record.document_s3_key,
                id: record.legalforms_id
            }), actions.loadRecord))
        }

        const handleCloseStepper = () => {
            setProgress(false);
            setStepperLoader(false);
            if(!error && !success) {
                dispatch(actions.cancelForm(Object.assign({}, {
                    case_id: record.id,
                    client_id: record.client_id,
                    status: 'cancelled',
                    document_type: docType,
                    s3_file_key: record.document_s3_key,
                    id: record.legalforms_id
                }), actions.loadRecord));
            }
        }

        const handleFrogsTemplateQuestionForm = (submitData, form) => {
            const submitRecord = Object.assign({}, { ...record }, { ...submitData });
            dispatch(actions.questionsTemplate(submitRecord, form, getFormLabel, standardFormColumns));
        }

        const handleClientWithCases = (data, dispatch, { form }) => {
            clientWithCasesFunction(Object.assign({}, { data, form, dispatch, record, filevineDetails, clioDetails, mycaseSessionDiff, actions, mycase_case_from, case_from }));
        }

        const handleShowEmailHistory = (emailsLogData, mailSentDate) => {
            setMailhistoryData(emailsLogData);
            setEmailSentDate(mailSentDate);
        }

        if (loading || (record && !record.frogs) || (!showRequiredFieldsPopup && formsType && !formsType.filter(_ => _.viewForm).length)) {
            return <Spinner className={classes.spinner} />
        }

        return (<Grid container className={classes.casesPage} style={{ marginTop: `30px` }}>
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    {formsType && formsType.filter(_ => _.viewForm).map((form, key) => {
                        return (<Grid key={key} item xs={12} md={width < breakPointWidth ? 6 : 4} style={form.style || null} className={classes.cardsPadding}>
                            {record && record[form.value] && record[form.value].length > 0 ?
                                <CountBox
                                    name={name}
                                    path={path}
                                    record={record}
                                    columns={uploadColumns}
                                    changeFormColumns={changeFormColumns}
                                    editFormColumns={editFormColumns}
                                    calendarDaysColumns={calendarDaysColumns}
                                    handleEditForm={handleEditForm}
                                    handleSubmit={handleSubmit}
                                    deleteForm={handleDeleteForm}
                                    deleteShred={handleDeleteShred}
                                    progress={props.progress}
                                    docType={(type) => setDocType(type)}
                                    locationState={location.state}
                                    standardFormOptions={standardFormOptions}
                                    disclosureFormOptions={disclosureFormOptions}
                                    metaData={Object.assign({}, metaData, {templateNameOptions : savedTemplate})}
                                    setResponseDate={(r,form, setDueDate) => dispatch(actions.setResponseDate(r, form, setDueDate))}
                                    forms={formDetails}
                                    match={match}
                                    templateNameForm={templateNameForm}
                                    handleTemplateQuestionForm={handleFrogsTemplateQuestionForm}
                                    user={user}
                                    useSavedTemplate={useSavedTemplate}
                                    emailSentHistoryModal={emailSentHistoryModal}
                                    handleShowEmailHistory={handleShowEmailHistory}
                                    setSelectedFormType={setSelectedFormType}
                                    handleDisclosureForm={handleDisclosureForm}
                                    templateData={templateData}
                                    {...form} /> :
                                <UploadBox
                                    name={name}
                                    path={path}
                                    record={record}
                                    columns={uploadColumns}
                                    handleSubmit={handleSubmit}
                                    standardFormOptions={standardFormOptions}
                                    docType={(type) => setDocType(type)}
                                    locationState={location.state}
                                    disclosureFormOptions={disclosureFormOptions}
                                    templateNameForm={templateNameForm}
                                    handleTemplateQuestionForm={handleFrogsTemplateQuestionForm}
                                    metaData={Object.assign({}, metaData, {templateNameOptions : savedTemplate})}
                                    useSavedTemplate={useSavedTemplate}
                                    setSelectedFormType={setSelectedFormType}
                                    handleDisclosureForm={handleDisclosureForm}
                                    templateData={templateData}
                                    {...form} />}
                        </Grid>)
                    }) || null}
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Stepper
                    title={title}
                    active={active}
                    show={progress ? true : false}
                    close={handleCloseStepper}
                    steps={steps}
                    error={error}
                    success={success}
                    loader={stepperLoader}
                    disableCancelBtn={record && record.legalforms_id ? false : true}
                    errorBtnText={(!contentNotFound && !questionsNotFound) ? "Request Help" : false}
                    errorBtnAction={(!contentNotFound && !questionsNotFound) ? handleRequestHelp : false}
                    cancel={handleCancelStepper}
                    successMessage={successMessage} />
            </Grid>
            <Grid>
                <ModalDialog
                    title={"Error occurred while processing the document you have uploaded. Please choose Unlimited Jurisdiction or Limited Jurisdiction if you want to manually select questions from the Judicial Council Form (instead of uploading a document with selected questions)."}
                    className={classes.form}
                    options={standardFormOptions}
                    btnStyle={{ width: '270px' }}
                    onClose={handleCloseStandardForm}
                    show={error && docType == 'FROGS'} />
            </Grid>
            <Grid>
                <ModalForm
                    initialValues={initialRecord}
                    title={'Client & Case Details'}
                    fields={clientCaseField.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`client_case_${record.id}`}
                    className={classes.form}
                    show={showRequiredFieldsPopup}
                    confirmMessage={mycase_case_from ? (!mycaseSessionDiff ? `We couldn’t update this change to your MyCase account due to session expired. Please click Activate button that will redirect to MyCase site. Once you logged in, you can try to update the case again.` : false) : false}
                    confirmButton={mycase_case_from ? (mycaseSessionDiff ? false : true) : false}
                    onSubmit={handleClientWithCases.bind(this)}
                    btnLabel="Update"
                    disableCancelBtn
                    enableSubmitBtn
                    keepDirtyOnReinitialize={true}
                    enableScroll={classes.bodyScroll}
                    footerStyle={{ border: 'none' }}
                    style={{ paddingRight: '0px' }}
                    footerCancelBtn={(StyleClasses) => {
                        return (<Link to={{ pathname: path, state: { ...location.state }}}>
                            <Button
                                type="button"
                                variant="contained"
                                className={StyleClasses.button}>
                                Cancel
                            </Button>
                        </Link>)
                    }}
                    metaData={metaData} />
            </Grid>
            <Grid>
                <ModalForm
                    title={'Choose Template'}
                    fields={templateNameForm.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`templateForm_${record.id}`}
                    className={classes.form}
                    show={showTemplateNameForm}
                    onClose={() => setShowTemplateNameForm(false)}
                    onSubmit={handleTemplateDisclosureForm.bind(this)}
                    btnLabel="Submit"
                    style={sm ? { paddingTop: '10px', paddingRight: '10px' } : { paddingTop: '10px' }}
                    footerStyle={{ border: 'none' }}
                    confirmPopUpClose
                    metaData={Object.assign({}, metaData, {templateNameOptions : savedTemplate})}
                    />
            </Grid>
            <Grid>
                <ModalAlertDialog
                    description={emailSentHistoryModal}
                    show={mailHistoryData ? true : false}
                    disableSubmitBtn={true}
                    disableCancelButton={true}
                    enableScroll={classes.bodyScroll}
                    title={`Document was sent by email on ${emailSentDate && moment(emailSentDate).format('MM/DD/YY')} to the following addresses`}
                    paperClassName={classes.paperClassName}
                    mailHistoryData={mailHistoryData}
                    closeDialog={() => setMailhistoryData(false)}
                    onClose={() => setMailhistoryData(false)}
                />
            </Grid>
            <Grid>
                {!loading && <FilevineSessionForm
                    formRecord={clientCaseFormValue}
                    record={record}
                    actions={actions}
                    dispatch={dispatch}
                    createClientCase
                    {...props} />}
            </Grid>
            <Grid>
                {!loading && <LitifyInfoDialog
                    record={record}
                    actions={actions}
                    dispatch={dispatch}
                    {...props}
                />}
            </Grid>
            {!progress ? <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} /> : null}
        </Grid >);
    }

    ViewRespondPage.propTypes = {
        active: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object,
        filevineSessionForm: PropTypes.bool,
        litifyInfoDialog: PropTypes.bool,
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        active: selectKey(),
        loading: selectPageLoader(),
        billing: selectBilling(),
        progress: selectProgress(),
        user: selectUser(),
        metaData: selectRecordsMetaData(),
        records: selectRecords(),
        formDetails: selectForm(),
        contentNotFound: selectContentNotFound(),
        filevineSessionForm: selectFilevineSessionForm(),
        litifyInfoDialog: selectLitifyInfoDialog(),
        questionsNotFound : selectQuestionsNotFound()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewRespondPage);

}

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    casesPage: {
        marginTop: '85px',
        '@global': {
            '.MuiTypography-subtitle1': {
                fontSize: '0.84rem'
            }
        }
    },
    spinner: {
        padding: '20px',
        marginTop: '50px'
    },
    form: {
        justifyContent: 'center',
        marginTop: '21px'
    },
    cardsPadding: {
        padding: '8px !important'
    },
    bodyScroll: {
        maxHeight: '500px',
        overflowY: 'auto',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 6,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },
    paperClassName : {
        minWidth : '480px !important'
    }

}));

export default useStyles;
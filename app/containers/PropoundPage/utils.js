
export function getStepper(type) {
    switch (type) {
        case 'uploadDoc':
            return {
                stepperId: 1,
                title: 'Upload Propounded Template',
                steps: ['Uploading the file', 'Processing Document', 'Extracting Questions'],
                successMessage: 'Questions Extracted'
            }
        case 'template':
            return {
                stepperId: 2,
                title: 'Generate Template',
                steps: ['Fetching Uploaded Questions', 'Generating Template', 'Template Created'],
                successMessage: 'Document Downloaded',
            }
        case 'final':
            return {
                stepperId: 3,
                title: 'Generate Final Doc',
                steps: ['Fetching Uploaded Questions', 'Generating Final Doc', 'Final Doc Created'],
                successMessage: 'Document Downloaded',
            }
    }
}



export const validation = (formData, propsData) => {

    const { records = {}, fields, form } = propsData || {};

    const requiredFields = fields.reduce((a, el) => {
      if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object' && ((el.mandatory.create && form.indexOf('create') > -1) || (el.mandatory.edit && form.indexOf('edit') > -1)))) {
        a.push(el.value)
      }
      return a;
    }, []);


    const errors = {};
    requiredFields.forEach(field => {
      if ((!formData[field] && field != 'dob' && typeof formData[field] === 'boolean') || (!formData[field] && field != 'dob' && formData[field] != '0')) {
        errors[field] = 'Required'
      } 
    })

    const duplicateFileName = formData.file_name && records && records.length > 0 && records.find(_ => _.file_name && _.file_name.toString() === formData.file_name.toString()) || false;

    if(duplicateFileName){
      errors.file_name = 'File Name Already Exists.';
    }

    if(formData.file_name && formData.file_name.length > 40){
        errors.file_name = 'Only 40 characters allowed.';
    }

    return errors;
  }


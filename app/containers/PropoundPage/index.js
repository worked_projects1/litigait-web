/**
 *
 * Propound Page
 *
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import ModalForm from 'components/ModalRecordForm';
import Styles from './styles';
import Stepper from 'components/Stepper';
import { getStepper } from './utils';
import { validation } from './utils';
import { ImplementationTable, RecordsData } from '../../utils/tools';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';



/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {object} filterColumns 
 */
export default function (name, path, columns, actions, selectors, filterColumns) {

    const { propoundColumns, propoundUploadTemplateColumns } = columns;
    const {
        selectLoading,
        selectRecords,
        selectRecord,
        selectSuccess,
        selectUpdateError,
        selectRecordsMetaData,
        selectUser,
        selectKey,
        selectContentNotFound,
        selectQuestionsNotFound
    } = selectors;

    /**
     * @param {object} props 
     */
    function PropoundPage(props) {

        const { records, record, dispatch, location, history, error, success, pathData, children, metaData, loading, match, queryParams, user, active, contentNotFound, questionsNotFound } = props;

        const { practiceDetails } = user;
        const pathname = location && location.pathname || false;
        const activeChildren = path !== pathname;
        const standardForm = (pathname.indexOf('propoundQuestions') > -1) ? true : false;
        const classes = Styles();
        const [progress, setProgress] = useState(false);
        const [stepperLoader, setStepperLoader] = useState(false);
        const TableWrapper = ImplementationTable.default;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const { title, steps, successMessage } = progress && getStepper(progress) || {};

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords(true));
            return () => mounted = false;
        }, []);

        const handleSubmit = (data, dispatch, { form }) => {
            const file = data.s3_file_key && data.s3_file_key.length > 0 && data.s3_file_key[0] || false;
            const submitData = Object.assign({}, {
                upload_document_name: file.name,
                uploadFile: file,
                document_type: data.document_type,
                file_name: data.file_name,
                state: data.state,
                content_type: file.type,
                practice_id: practiceDetails.id
            });
            setProgress('uploadDoc');
            dispatch(actions.uploadPropoundTemplate(submitData, form, setProgress));
        }

        const handleCancelStepper = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
            setStepperLoader(false);
            dispatch(actions.cancelPropoundTemplate(Object.assign({}, {
                status: 'cancelled',
                document_type: record.document_type,
                propound_template_id: record.id,
                practice_id: record.practice_id
            }), actions.loadRecords))
        }

        const handleCloseStepper = () => {
            dispatch(actions.loadRecordsCacheHit());
            dispatch(actions.loadRecords());
            setProgress(false);
            setStepperLoader(false);
            if(!error && !success) {
                dispatch(actions.cancelPropoundTemplate(Object.assign({}, {
                    status: 'cancelled',
                    document_type: record.document_type,
                    propound_template_id: record.id,
                    practice_id: record.practice_id
                }), actions.loadRecords))
            }
        }

        const handleRequestHelp = () => {
            setStepperLoader(true);
            dispatch(actions.propoundRequestHelp({
                practice_id: record.practice_id,
                propound_template_id: record.id,
                document_type: record.document_type,
                document_s3_key: record.document_s3_key,
                file_name: record.file_name,
                state: record.state,
                upload_document_name: record.upload_document_name,
                hash_id: record?.hash_id
            }, setStepperLoader));
        }

        return !standardForm ? (
            <Grid container>
                <Grid item xs={12}>
                    <Grid item xs={12} className={classes.addBtn}>
                        <Grid container direction="row" alignItems="center">
                            <ModalForm
                                fields={propoundUploadTemplateColumns.filter(_ => _.editRecord)}
                                form={`propoundForm`}
                                btnLabel="Upload"
                                className={classes.uploadBtn}
                                onSubmitClose={true}
                                metaData={metaData}
                                validate={(f, p) => validation(f, Object.assign({}, p, { records }))}
                                onSubmit={handleSubmit}>
                                {(open) =>
                                    <Button type="button" variant="contained" color="primary" className={classes.create} onClick={open}>
                                    Add New Template
                                </Button>}
                            </ModalForm>
                        </Grid>
                    </Grid>
                    <Grid container direction={!sm ? "column-reverse" : null} wrap={!sm ? "nowrap" : null}>
                        <Grid item xs={12} md={activeChildren ? 6 : 12}>
                            <Grid item xs={12}>
                                <div className={`propound-table ${classes.table}`}>
                                    <TableWrapper
                                        records={loading ? RecordsData : records && records.filter(record => record.questions && record.questions.length > 0 && record)}
                                        columns={propoundColumns}
                                        children={activeChildren}
                                        path={path}
                                        name={name}
                                        history={history}
                                        locationState={location.state}
                                        sm={sm}
                                        view={true}
                                        metaData={metaData}
                                        pageLimit={[10, 25, 100]}
                                        loading={loading}
                                    />
                                </div>
                            </Grid>
                        </Grid>
                        {activeChildren ?
                            <Grid item xs={12} md={6}>
                                <div className="children">
                                    {children}
                                </div>
                            </Grid> : null}
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Stepper
                        title={title}
                        active={active}
                        show={progress ? true : false}
                        close={handleCloseStepper}
                        steps={steps}
                        error={error}
                        success={success}
                        loader={stepperLoader}
                        disableCancelBtn={record && record.propound_template_id ? false : true}
                        errorBtnText={(!contentNotFound && !questionsNotFound) ? "Request Help": false}
                        errorBtnAction={(!contentNotFound && !questionsNotFound) ? handleRequestHelp : false}
                        cancel={handleCancelStepper}
                        successMessage={successMessage} />
                </Grid>
            </Grid>
        ) : (<Grid container className="ViewPropoundQuestionPage">
                <Grid item xs={12} className={classes.children}>
                    {children}
                </Grid>
            </Grid>
        )
        
    }

    PropoundPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        records: selectRecords(),
        record: selectRecord(),
        success: selectSuccess(),
        error: selectUpdateError(),
        metaData: selectRecordsMetaData(),
        user: selectUser(),
        active: selectKey(),
        contentNotFound: selectContentNotFound(),
        questionsNotFound : selectQuestionsNotFound()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(PropoundPage);

}

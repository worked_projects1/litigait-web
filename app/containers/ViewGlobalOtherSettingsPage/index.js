/**
 *
 * View Global Other Settings Page
 *
 */

import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import EditRecordForm from 'components/EditRecordForm';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Icons from 'components/Icons';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment';



/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, view) {

    const {
        selectLoading,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectGlobalSettings,
        selectRecordsMetaData,
        selectUser
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewGlobalOtherSettingsPage(props) {
        const classes = Styles();
        const { dispatch, location = {}, loading, error, success, progress, metaData = {}, children, match, user } = props;        
        const [showForm, setShowForm] = useState(false);
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const sections = columns && typeof columns === 'function' && columns(user).section || false;

        const practiceNameList = (practicesDetails) => {
            const practiceRecords = metaData && metaData['practicesOptions'] && metaData['practicesOptions'] || [];
            const practiceName = practicesDetails && practicesDetails.length > 0 && practicesDetails.reduce((a, el) => {
                const opt = practiceRecords.find(_ => _.value.toString() === el);
                if (opt)
                    a.push(opt);
                return a;
            }, []) || [];

            const options = practiceRecords && practiceRecords.length > 0 && practicesDetails && practicesDetails.length > 0 ? practiceRecords.length == practicesDetails.length ? 'All' : practiceName.map(_ => _.label).join(', ') : '';

            return options;
        }

        async function handleEdit(record, dispatch, { form }) {
            const submitRecord = Object.assign({}, { ...record });
            if(record) {
                if(record.propound_template_practices) {
                    let propound_template_practices = record.propound_template_practices && typeof record.propound_template_practices === 'string' && record.propound_template_practices.split(',');
                    const practiceName = practiceNameList(propound_template_practices);
                    submitRecord.propound_template_practices_name = practiceName;
                } else if (!record.propound_template_practices) {
                    submitRecord.propound_template_practices_name = '';
                }
                if (record.tekasyougo_practices) {
                    let tekasyougo_practices = record.tekasyougo_practices && typeof record.tekasyougo_practices === 'string' && record.tekasyougo_practices.split(',');
                    const practiceName = practiceNameList(tekasyougo_practices);
                    submitRecord.tekasyougo_practices_name = practiceName;
                } else if (!record.tekasyougo_practices) {
                    submitRecord.tekasyougo_practices_name = '';
                }

                if (record.show_document_editor_ids) {
                    let show_document_editor_ids = record.show_document_editor_ids && typeof record.show_document_editor_ids === 'string' && record.show_document_editor_ids.split(',');
                    const practiceName = practiceNameList(show_document_editor_ids);
                    submitRecord.show_document_editor_practice_name = practiceName;
                } else if (!record.show_document_editor_ids) {
                    submitRecord.show_document_editor_practice_name = '';
                }

                submitRecord.old_pricing_till = submitRecord.old_pricing_till && new Date(moment(submitRecord.old_pricing_till));
                submitRecord.new_pricings_from = submitRecord.new_pricings_from && new Date(moment(submitRecord.new_pricings_from));
            }
            dispatch(actions.updateGlobalSettings(submitRecord, user, form));
            setShowForm(false);
        }

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadGlobalSettings(true));
            return () => mounted = false;
        }, []);

        const columValue = (column, row) => {
            switch (column.type) {
                case 'upload':
                    return column.html && column.html(row, metaData) || (
                        <img
                            src={row[column.value] || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                            role="presentation"
                            className={classes.img}
                        />
                    );
                case 'checkbox':
                    return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value]}</p>;
                case 'select':
                    return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value]}</p>;
                case 'textarea':
                    return column.html && column.html(row, metaData) || <p className={classes.textarea}>{row[column.value] != null && row[column.value] != '' ? lineBreak(row[column.value]) : '-'}</p>;
                default:
                    return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value] != null && row[column.value] != '' ? row[column.value] : '-'}</p>;
            }
        };

        const lineBreak = (val) => {
            return val && val.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos}>{line}</p>)
        }

        if(progress){
            return <Spinner className={classes.spinner} />;
        }

        return sections && sections.map((tab, index) =>
            tab.view && (<Grid key={index} className={classes.section} container>
                <Grid item xs={12}>
                    <Grid container justify="space-between">
                        <Typography component="h1" variant="h5" className={classes.name}>
                            {tab.name}
                        </Typography>
                        {tab.edit && view ?
                            sm ? <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                disabled={progress}
                                onClick={() => setShowForm(tab.schemaId)}
                                className={classes.create} >
                                {tab.label && typeof tab.label === 'function' ? tab.label(props[tab.value]) : `Change ${tab.name}`}
                            </Button> : <Icons type='Edit' className={classes.icons} onClick={() => setShowForm(tab.schemaId)} /> : null
                        }
                    </Grid>
                </Grid>                
                <Grid item xs={12} md={showForm ? 6 : 12} className={classes.columns}>
                    {(typeof tab.columns === 'function' && tab.columns(props[tab.value]) || tab.columns).filter(_ => _.viewRecord).map((col, index) => (
                        <Grid key={index} >
                            <Grid className={classes.settings}><Grid container className={classes.row}>
                                <Grid item xs={6} md={showForm ? 4 : 4} className={classes.label}>
                                    {col.label}:
                                </Grid>
                                <Grid item xs={6} md={showForm ? 8 : 8} className={classes.value}>
                                    {progress ? <Skeleton animation="wave" height={40} width={sm ? 280 : 80} /> :
                                        columValue(col, props[tab.value] && tab.filter(props[tab.value]))}
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid item xs={12} md={showForm ? 11 : 8} ><hr className={classes.hr} /></Grid>
                        </Grid>
                    ))}
                </Grid>
                {!loading && showForm === tab.schemaId ?
                    <Grid item xs={12} md={showForm ? 6 : 12} className={classes.table}>
                        <Grid className={classes.edit}>
                            <EditRecordForm
                                initialValues={tab.filter(props[tab.value]) || tab.initialValues || {}}
                                form={`OtherSettingsForm_${tab.schemaId}`}
                                name={name}
                                path={path}
                                metaData={metaData}
                                fields={tab.columns.filter(_ => _.editRecord)}
                                confirmButton={true}
                                confirmMessage={tab.confirmMessage}
                                btnLabel="Update"
                                onSubmit={handleEdit.bind(this)}
                                locationState={location.state}
                                cancelBtn={() => <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setShowForm(false)}
                                    className={classes.cancelBtn}>
                                    Cancel
                                </Button>}
                            />
                        </Grid>
                    </Grid> : null}
                <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>) || null)
    }

    ViewGlobalOtherSettingsPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        pricing: PropTypes.object,
        progress: PropTypes.bool,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        metaData: PropTypes.object,
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        pricing: selectGlobalSettings(),
        error: selectUpdateError(),
        success: selectSuccess(),
        progress: selectProgress(),
        metaData: selectRecordsMetaData(),
        user: selectUser()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewGlobalOtherSettingsPage);

}

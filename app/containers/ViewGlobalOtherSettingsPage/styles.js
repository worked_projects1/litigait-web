import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    create: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular'
    },
    form: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        padding: '18px',
        marginTop: '40px'
    },
    settings: {
        paddingLeft: '25px',
        paddingRight: '25px'
    },
    row: {
        alignItems: 'center'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none'
    },
    table: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        marginTop: '40px',
        overflow: 'auto',
        marginTop: '30px',
    },
    edit: {
        padding: '20px',
        paddingTop: '0px'
    },
    img: {
        height: 64
    },
    section: {
        marginTop: '40px'
    },
    section2: {
        marginTop: '60px'
    },
    label: {
        fontWeight: 'bold',
        fontSize: '16px'
    },
    value: {
        paddingLeft: '25px',
        fontWeight: 'bold',
        fontSize: '14px'
    },
    textarea: {
        fontWeight: 'bold',
        fontSize: '14px',
        paddingLeft: '0px',
    },
    columns: {
        paddingTop: '25px'
    },
    name: {
        fontSize: '22px'
    },
    spinner: {
        marginTop: '75px'
    },
    icons: {
        height: '30px',
        color: '#2CA01C',
        cursor: 'pointer'
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        backgroundColor: 'gray !important'
    }
}));

export default useStyles;
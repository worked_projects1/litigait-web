

/**
 *
 * View Propounding Questions Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import QuestionBox from './components/QuestionBox';
import ButtonSpinner from 'components/ButtonSpinner';
import { useDidUpdate, autoIncrementQuestions } from 'utils/tools';

const Default_Question = [Object.assign({}, { question_id: 1, question_number: '1', question_number_text: '1', question_text: '', question_options: '' })];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectSessionExpand } = selectors;

    const { questionFormColumns } = columns;

    /**
     * @param {object} data 
     * @param {function} dispatch 
     */
    function handleForm(data, dispatch) {
        const submitData = Object.assign({}, {
            practice_id: data.practice_id,
            file_name: data.file_name,
            questions: data.questions,
            document_type: data.document_type,
            content_type: 'application/pdf',
            from: 'questions_form',
            form: 'questionsForm',
            upload_document_name: data.upload_document_name,
            propound_template_id: data.id,
            state: data.state
        });
        dispatch(actions.savePropoundQuestionsTemplate(submitData));
    }

    /**
     * @param {object} props 
     */
    function ViewPropoundQuestionsPage(props) {

        const { location, record, dispatch, success, error, loading, progress, expand, history, match } = props;
        const classes = Styles();

        const [loadForm, setLoadForm] = useState(true);
        const [loadQuestions, setLoadQuestions] = useState(false);
        const [editable, setEditable] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [questions, setQuestions] = useState(record && record.questions || []);

        const questionRecords = questions;

        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const navbarWidth = expand ? 240 : 60;
        const disableNote = location && location.state && location.state.disableNote || false;

        useEffect(() => {
            let mounted = true;
            window.addEventListener('resize', () => setWidth(window.innerWidth));
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            setLoadForm(true);
            if (match && match.params.id) {
                dispatch(actions.loadPropoundTemplate(Object.assign({}, {
                    id: match.params.id,
                    document_type: match.params.type
                }), setLoadForm, setQuestions));
            }
            return () => mounted = false;
        }, [match.params.id]);

        useDidUpdate(() => {
            if (record && record.questions && !record.questions.length && !questions.length) {
                dispatch(actions.loadPropoundTemplate(Object.assign({}, {
                    id: match.params.id,
                    document_type: match.params.type,
                }), setLoadForm, setQuestions));
            }
        }, [record, actions, match, dispatch]);

        const handleChange = (data, type) => {
            let index = 0;
            const changedVal = (questionRecords || []).reduce((a, el, i) => {
                if (el.question_id.toString() === data.question_id.toString()) {
                    if (type === 'input') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: data.question_number.toString(), question_number_text: data.question_number.toString(), question_text: data.question_text }));
                    } else if (type == 'add') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: "", question_number_text: "", question_options: "", question_text: "" }));
                    } else if (type != 'delete') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: index.toString(), question_number_text: index.toString() }));
                    }
                } else {
                    index++;
                    a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                }

                return a;
            }, []);

            if (type === 'delete') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal);
                setLoadQuestions(true);
                setTimeout(() => {
                    setLoadQuestions(false);
                    setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
                }, 500);
            } else if(type == 'add') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal);
                setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
            } else {
                setQuestions(changedVal && changedVal.length > 0 ? changedVal : Default_Question);
            }

        }

        const handleCancel = () => {
            if (editable) {
                setQuestions(record && record.questions || [])
            }
            setEditable(!editable);
        }

        const handleClose = () => {
            history.push({ pathname: `/rest/propound`, state: Object.assign({}, { ...location.state }) });
            dispatch(actions.loadRecords(true));
        }


        return (<Grid container className={classes.questionsFormPage}>
            {!loading && record && questionRecords && questionRecords.length > 0 && !loadForm && Object.keys(record).length > 0 ?
                <Grid container>
                    <Grid item xs={12} className={classes.header} style={!md ? { left: '0', width: `${width}px` } : { width: `${width - navbarWidth}px`, left: navbarWidth }}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container justify="center">
                                    {editable && <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handleForm(Object.assign({}, { ...record }, { document_type: (location && location.state && location.state.document_type) || record.document_type, questions: questionRecords, filename: location.state && location.state.filename, legalform_id: location.state && location.state.legalform_id }), dispatch)}>
                                        {progress && <ButtonSpinner /> || 'Save'}
                                    </Button> || null}
                                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleCancel} disabled={progress}>
                                        {editable && 'Cancel' || 'Edit'}
                                    </Button>
                                    <Button type="button" onClick={handleClose} variant="contained" className={classes.btnClose} disabled={progress}>
                                        Close
                                    </Button>
                                </Grid>
                            </Grid>
                            {!disableNote ? <Grid item xs={12} className={classes.notes}>
                                <b>Please check if the questions listed below matches the document, use edit option to make modifications if any.</b>
                            </Grid> : null}
                        </Grid>
                    </Grid>
                    <Grid item xs={12} style={{ marginTop: disableNote ? '95px' : '132px' }}>
                        <Grid container spacing={5}>
                            {!loadQuestions && (questionRecords || []).map((r, i) => (
                                <Grid key={i} item xs={12}>
                                    {editable && <QuestionBox
                                        name={name}
                                        path={path}
                                        fields={questionFormColumns}
                                        record={r}
                                        handleChange={handleChange}
                                    /> || <Grid container direction="row" style={{ fontSize: '16px' }}>
                                            <Grid item style={{ width: '3%' }}><b>{`${r.question_number}.`}</b></Grid>
                                            <Grid item style={{ width: '97%' }}>{r.question_text || 'No Text'}</Grid>
                                        </Grid>}
                                </Grid>)) || <Spinner className={classes.spinner} style={{ marginTop: '62px' }} />}
                        </Grid>
                    </Grid>
                </Grid> : <Spinner className={classes.spinner} />}
            <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid >);
    }

    ViewPropoundQuestionsPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress(),
        expand: selectSessionExpand()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewPropoundQuestionsPage);

}

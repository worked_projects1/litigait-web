/***
 * 
 * Form Provider
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';

function FormProvider(props) {
    const { field, className, handleSubmit, metaData = {}, destroy, record, onInputChange } = props;

    const InputComponent = ImplementationFor[field.type];

    useEffect(() => {
        return () => destroy();
    }, []);

    return (<form onSubmit={handleSubmit}>
        <Field
            name={field.value}
            type={field.type}
            onChange={(val) => record[field.value].toString() !== val.toString() ? onInputChange(Object.assign({}, { ...record }, { [field.value]: val })) : null}
            className={className}
            metaData={metaData}
            defaultBlur
            component={InputComponent}
            {...field} />
    </form>)
}


export default reduxForm({
    form: 'propoundQuestionForm',
    enableReinitialize: true,
    touchOnChange: true,
})(FormProvider);
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    create: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular'
    },
    settings: {
        paddingLeft: '25px',
        paddingRight: '25px'
    },
    row: {
        paddingTop: '15px'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none',
        margin: '0px'
    },
    table: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        marginTop: '40px',
        overflow: 'auto',
        marginTop: '30px',
    },
    edit: {
        padding: '20px',
        paddingTop: '0px'
    },
    section: {
        marginTop: '40px'
    },
    label: {
        fontWeight: 'bold',
        paddingRight: '25px',
    },
    value: {
        fontWeight: 'bold',
        fontSize: '14px',
    },
    dollar: {
        paddingBottom: '2px',
        paddingRight: '2px'
    },
    dollarDiv: {
        display: 'flex',
        alignItems: 'center'
    },
    icons: {
        marginLeft: '14px',
        height: '32px',
        color: '#2CA01C',
        cursor: 'pointer'
    },
}));

export default useStyles;
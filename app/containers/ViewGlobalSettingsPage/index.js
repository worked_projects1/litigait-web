/**
 *
 * View Global Settings Page
 *
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography, Tabs, Tab, Card, CardHeader, CardContent } from '@material-ui/core';
import Styles from './styles';
import EditRecordForm from 'components/EditRecordForm';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Icons from 'components/Icons';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, view) {

    const {
        selectLoading,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectGlobalSettings
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewGlobalSettings(props) {
        
        const classes = Styles();
        const { dispatch, location = {}, loading, error, success, progress, children, match } = props;
        const [showForm, setShowForm] = useState(false);
        const [showMedicalValidation, setShowMedicalValidation] = useState(false);
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));

        async function handleEdit(record, dispatch, { form }) {

            let error = false;
            if (showForm === 2 || showForm === 4) {
                dispatch(actions.updateGlobalSettings(record, form));
                setShowForm(false);
            } else if (showForm === 3) {
                setShowMedicalValidation(false);
                error = false;
                record.medical_history_pricing_tier.map((item, pos, rows) => {
                    if (rows.length - 1 != pos) {
                        if ((item.to >= item.from) && (pos == 0 || (item.from - rows[pos - 1].to === 1)) && item.price >= 0) {

                        } else {
                            error = true;
                            setShowMedicalValidation("Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.");
                        }
                    }
                })
                if (!error) {
                    dispatch(actions.updateGlobalSettings(record, form));
                    setShowForm(false);
                } else {
                    setTimeout(() => {
                        setShowMedicalValidation(false);
                    }, 2000);
                }
            }
        }

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadGlobalSettings(true));
            return () => mounted = false;
        }, []);

        const columValue = (column, value) => {
            switch (column.type) {
                case 'upload':
                    return (
                        <img
                            src={value || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                            role="presentation"
                            className={classes.img}
                        />
                    );
                case 'textarea':
                    return <p className={classes.terms}>{value != null && value != '' ? lineBreak(value) : '-'}</p>;
                case 'component':
                    return value && Array.isArray(value) && value.length > 0 ? <TableContainer>
                        <Table aria-label="spanning table" className={"medicalPrice"} style={{ maxWidth: '450px' }} >
                            <TableBody >
                                {value.map((r, i) => <TableRow key={i}>
                                    <TableCell style={{ paddingLeft: '0px' }}><b>{`Tier ${i + 1}: Pages`}</b><span style={{ marginLeft: '40px' }}>{i === (value.length - 1) ? `more than ${r.from} ` : r.from}</span>
                                        {i === (value.length - 1) ? null : <span> - </span>}
                                        {i === (value.length - 1) ? null : <span>{r.to}  </span>}
                                    </TableCell>

                                    <TableCell colSpan={i === (value.length - 1) ? 3 : 1}>
                                        {i === 0 ? 'Free' : i === (value.length - 1) ? 'Call for quote and order' : <div>${r.price}</div>}</TableCell>
                                </TableRow>)}
                            </TableBody>
                        </Table>
                    </TableContainer> : <p className={classes.value}>{value != null && value != '' ? value : '-'}</p>
                default:
                    return <p className={classes.value}>{value != null && value != '' ? value : '0'}</p>;
            }
        };

        const lineBreak = (val) => {
            return val && val.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos}>{line}</p>)
        }

        const section = columns && typeof columns === 'function' && columns().section || false;
        const tab = section && match && match.params && match.params.tab && section[match.params.tab] || section && section[0] || false;
        
        if(progress){
            return <Spinner className={classes.spinner} />;
        }

        return <Grid container>
            <Grid item xs={12}>
                {tab && tab.view && (<Grid className={classes.section} container>
                    <Grid item sm={6} xs={8}>
                        <Grid container justify="space-between">
                            <Typography component="h1" variant="h5" className={classes.name}>
                                {tab.name}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item sm={6} xs={4}>
                        <Grid container justify={"flex-end"}>
                            {tab.edit && view ? sm ? <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                onClick={() => setShowForm(tab.schemaId)}
                                className={classes.create} >
                                {`Change ${tab.name}`}
                            </Button> : <Icons type='Edit' className={classes.icons} onClick={() => setShowForm(tab.schemaId)} /> : null}
                        </Grid>
                    </Grid>
                        <Grid item xs={12} md={showForm ? 6 : 12} className={classes.columns}>
                            {tab.columns.filter(_ => _.viewRecord).map((col, index) => {
                                const columnVal = (col.value.indexOf('.') > -1 && col.value.split('.')) || col.value;

                                const fieldVal = (Array.isArray(columnVal) && props[tab.value] && tab.filter(props[tab.value]) && tab.filter(props[tab.value])[columnVal[0]] && tab.filter(props[tab.value])[columnVal[0]][columnVal[1]]) || (props[tab.value] && tab.filter(props[tab.value]) && tab.filter(props[tab.value])[col.value]);

                                return (
                                    <Grid key={index} >
                                        <Grid className={classes.settings}>
                                            {col.label === 'Minimum Pricing' ? <Grid container className={classes.row} alignItems={"center"}>
                                                <Grid item xs={col.label ? 3 : 12} md={!col.label ? 12 : showForm ? 2 : 7} className={classes.value} style={!col.label ? { paddingLeft: '0px' } : null}>
                                                    <Table aria-label="spanning table" className={"medicalPrice"} style={{ width: '450px' }} >
                                                        <TableBody >
                                                            <TableRow>
                                                                <TableCell style={{ paddingLeft: '0px', width: '255px' }}>
                                                                    <b>Minimum Pricing</b>
                                                                </TableCell>
                                                                <TableCell colSpan={1}>${fieldVal}</TableCell>
                                                            </TableRow>
                                                        </TableBody>
                                                    </Table> </Grid></Grid> : <Grid container className={col.label == '' ? null : classes.row} alignItems={"center"}>
                                                {col.label ? <Grid item xs={9} md={showForm ? 10 : 5} className={classes.label}>
                                                    {col.label}
                                                </Grid> : null}
                                                <Grid item xs={col.label ? 3 : 12} md={!col.label ? 12 : showForm ? 2 : 7} className={classes.value} style={!col.label ? { paddingLeft: '0px' } : null}>
                                                    {col.docType || col.dollar ? <div className={classes.dollarDiv}><span className={classes.dollar}>$</span><span>{columValue(col, fieldVal)}</span></div> : columValue(col, fieldVal)}
                                                </Grid>
                                            </Grid>}
                                        </Grid>
                                        {col.type !== 'component' && col.label !== 'Minimum Pricing' && <Grid item xs={12} md={showForm ? 11 : 6} ><hr className={classes.hr} /></Grid> || null}
                                    </Grid>
                                )
                            })}
                        </Grid>
                        {!loading && showForm === tab.schemaId ?
                            <Grid item xs={12} md={showForm ? 6 : 12} className={classes.table}>
                                <Grid className={classes.edit}>
                                    <EditRecordForm
                                        initialValues={tab.filter(props[tab.value]) || tab.initialValues || {}}
                                        form={`SettingsForm_${tab.schemaId}`}
                                        name={name}
                                        path={path}
                                        fields={tab.columns.filter(_ => _.editRecord)}
                                        confirmButton={true}
                                        confirmMessage={tab.confirmMessage}
                                        btnLabel="Update"
                                        onSubmit={handleEdit.bind(this)}
                                        locationState={location.state}
                                    />
                                </Grid>
                            </Grid> : null}
                        <Snackbar show={error || success || showMedicalValidation ? true : false} text={error || success || showMedicalValidation} severity={showMedicalValidation && 'error' || error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
                    </Grid>) || null}
            </Grid>
            <Grid item xs={12} style={tab && tab.style || {}}>
                {children}
            </Grid>
        </Grid>


    }

    ViewGlobalSettings.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        pricing: PropTypes.object,
        progress: PropTypes.bool,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        pricing: selectGlobalSettings(),
        error: selectUpdateError(),
        success: selectSuccess(),
        progress: selectProgress(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewGlobalSettings);

}

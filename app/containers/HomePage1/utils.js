

import LoginForm from 'components/LoginForm';
import RegisterForm from 'components/RegisterForm';
import RequestDemoForm from 'components/RequestDemoForm';
import ForgotPasswordForm from 'components/ForgotPasswordForm';
import LoginFailureForm from 'components/LoginFailureForm';
import StripeForm from 'components/StripeForm';
import VerificationForm from 'components/VerificationForm';
import AuthConfirmationForm from 'components/AuthConfirmationForm';
import SignupForm from 'components/SignupForm';

export const ImplementationForm = {
  login: LoginForm,
  register: RegisterForm,
  forgot: ForgotPasswordForm,
  requestDemo: RequestDemoForm,
  loginFailure: LoginFailureForm,
  verifyOtp: VerificationForm,
  stripe: StripeForm,
  authConfirm: AuthConfirmationForm,
  signup: SignupForm
};
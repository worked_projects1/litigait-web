import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    flexGrow: 1
  },
  component: {
    padding: '25px',
    background: 'white',
    height: '100%'
  },
  FormPage: {
      display: 'flex',
      alignItems: 'flex-end',
      flexDirection: 'column',
      backgroundImage: `url(${require('images/home/HomePage1.jpg')})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundAttachment: 'fixed'
  }
}));


export default useStyles;

/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';

import { CssBaseline, Box, Grid } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import { selectLoggedIn, selectUser, selectError, selectSuccess, selectPayment, selectLoading, selectMetaData, selectRespondDetails, selectDiscountDetail, selectAuthUserDetails, selectAuthPageLoader } from 'blocks/session/selectors';
import { logIn, signUp, forgotPassword, requestDemo, clearCache, setupStripePayment, loadSettingsPlan, verifyOtp, loadAppVersion, signUpDiscountCode as signUpDiscountCodeAction, googleAuth, microsoftAuth, authSignUp, emailValidation, webAuthentication } from 'blocks/session/actions';
import useStyles from './styles';

import Copyright from 'components/Copyright';
import Spinner from 'components/Spinner';
import ModalForm from 'components/ModalRecordForm';

import { Helmet } from 'react-helmet';
import { ImplementationForm } from './utils';
import withQueryParams from 'react-router-query-params';

import schema from 'routes/schema';
import store2 from 'store2';

/**
 * @param {object} props 
 */
export function HomePage1(props) {

    const { error, location, dispatch, success, match, queryParams, payment, loading, metaData = {}, respondDetail, authUserDetails, history, authPageLoader } = props;
    const { state = {} } = location;
    const { form, identifier, secret, user_from } = state;
    const classes = useStyles();
    const elements = useElements();
    const stripe = useStripe();
    const [pageLoader, setPageLoader] = useState(false);
    const [authenticate, setAuthenticate] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const pageType = match.path.includes("signin") ? 'login' : match.path.includes("login") ? 'login' : match.path.includes("signup") ? 'signup' : match.path.includes("forgot") ? 'forgot' : false;

    const formState = form || pageType || 'login';
    const Component = formState && ImplementationForm[formState];

    const pathname = location && location.pathname || '/';
    const respondPage = pathname && pathname.includes('respond') ? true : false;
    const initialValues = respondPage && respondDetail ? form === 'register' && Object.assign({}, { practice_name: respondDetail.responder_practice_name }) : form && form === 'authConfirm' && authUserDetails ? Object.assign({}, { ...authUserDetails }) : form === 'register' ? state && state.email && { email: state.email } : {};
    const integrationParams = pathname && pathname.includes('litify') ? pathname : false;
    const webAuthenticationForm = schema && schema().webAuthentication().columns;

    useEffect(() => {
        let mounted = true;
        setPageLoader(true);
        dispatch(loadSettingsPlan());
        dispatch(loadAppVersion());
        const storedPassword = store2.get('web_authenticate');
        if (!storedPassword && ['staging'].includes(process.env.ENV)) {
            setAuthenticate(true);
        }
        setTimeout(() => setPageLoader(false), 3000);
        return () => mounted = false;
    }, []);

    let err = null;

    if ((error && error.login && error.login.response
        && error.login.response.data && error.login.response.data.error) || (error[formState])) {
        const data = (error && error.login && error.login.response && error.login.response.data) || error[formState];

        if ((typeof data.error === 'string') || (typeof data === 'string')) {
            err = data && data['error'] || data;
        } else {
            err = data['error'] && data['error'][Object.keys(data.error)[0]] && data['error'][Object.keys(data.error)[0]][0];
        }

    }

    const getStripeCard = async ({ dispatch, data, form }) => {
        const result = await stripe.confirmCardSetup(payment && payment.client_secret || location && location.state && location.state.payment && location.state.payment.client_secret || '', {
            payment_method: {
                card: elements.getElement(CardElement),
                billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
            }
        });
        dispatch(setupStripePayment(Object.assign({}, data, result), form, location && location.state || {}));
    }

    const handleSubmit = (data, dispatch, { form }) => {
        if (formState === 'login') {
            dispatch(logIn(data.email, data.password, queryParams, form, respondPage, integrationParams));
        } else if (formState === 'register') {
            dispatch(signUp(Object.assign({}, data, { role: 'lawyer', notification: false, createdUserIn: 'signUp', respondCase: respondPage, token_id: respondDetail.id || false, privacy_policy_terms_of_use: true }), form, integrationParams));
        } else if (formState === 'authConfirm' && user_from) {
            dispatch(authSignUp(Object.assign({}, data, { role: 'lawyer', notification: false, createdUserIn: 'signUp', respondCase: false, token_id: false, user_from, privacy_policy_terms_of_use: true }), form));
        } else if (formState === 'forgot') {
            dispatch(forgotPassword(data, form));
        } else if (formState === 'requestDemo') {
            dispatch(requestDemo(data, form));
        } else if (formState === 'loginFailure' && identifier && secret) {
            dispatch(logIn(identifier, secret, queryParams, form));
        } else if (formState === 'stripe') {
            const filter_plan_category = data && data.plan_id && metaData && typeof metaData === 'object' && metaData['plans'] && metaData['plans'].length > 0 && metaData['plans'].filter(_ => _.plan_id === data.plan_id) || false;
            const plan_category = filter_plan_category && filter_plan_category.length > 0 && filter_plan_category[0] && filter_plan_category[0].plan_category || false;

            if (data && data.plan_id && data.plan_id && !data.name && (!data.cardDetails || data.cardDetails && typeof data.cardDetails === 'object' && data['cardDetails'] && !data['cardDetails']['complete'])) {
                dispatch(setupStripePayment(Object.assign({}, data, { page: 'register', plan_id: 'free_trial', selected_plan: data.plan_id, plan_category: plan_category, discount_code: data.discount_code }), form, location && location.state || {}));
            } else {
                getStripeCard({ dispatch, data: Object.assign({}, data, { page: 'register', selected_plan: data.plan_id, plan_category: plan_category }), form });
            }
        } else if (formState === 'verifyOtp' && identifier) {
            dispatch(verifyOtp(identifier, data.verifyInput, form, user_from, integrationParams))
        } else if (formState === 'signup') {
            const routePath = pathname && pathname.includes('signup') ? pathname : false
            dispatch(emailValidation(data, routePath, form, integrationParams));
        }
    }

    const clearSessionCache = () => {
        dispatch(clearCache());
    }

    const openAlertDialog = ({ submitRecord, dialog, form, cardDetails, handleSubmitForm }) => {
        dispatch(signUpDiscountCodeAction(Object.assign({}, submitRecord), form, dialog, cardDetails, handleSubmitForm));
    }

    const handleWebAuthenticate = (data, dispatch, { form }) => {
        dispatch(webAuthentication(data, form, setAuthenticate));
    }

    const google = () => {
        // OAuth2.0 secret
        const googleClientId = process.env.GOOGLE_CLIENT_ID;
        const googleRedirectUri = process.env.GOOGLE_REDIRECT_URI;
        const url = `https://accounts.google.com/o/oauth2/v2/auth?client_id=${googleClientId}&scope=openid%20profile%20email&redirect_uri=${googleRedirectUri}/google-oauth2&prompt=consent&access_type=offline&response_type=code&include_granted_scopes=true&enable_serial_consent=true&service=lso&o2v=2&flowName=GeneralOAuthFlow`;
        window.open(url, '_self');
    };

    const microsoft = () => {
        window.open(`${process.env.API_URL}/rest/microsoft/callback`, '_self');
    }

    if (pageLoader || authPageLoader) {
        return <Spinner loading={true} showHeight />
    }

    return (<Grid container component="main" className={classes.root}>
        <Helmet
            title="EsquireTek"
            meta={[
                { name: 'description', content: 'Home Page' },
            ]}
        />
        <Grid item xs={12} className={classes.FormPage}>
            <div className={classes.component} style={{ width: md ? `420px` : `100%` }}>
                {formState &&
                    <React.Fragment>
                        <Component
                            initialValues={initialValues}
                            form={`${formState}Form`}
                            onSubmit={handleSubmit.bind(this)}
                            errorMessage={err}
                            clearCache={clearSessionCache}
                            success={success}
                            locationState={state}
                            metaData={metaData}
                            loading={loading}
                            openAlertDialog={openAlertDialog}
                            oauthIntegration={Object.assign({}, { google, microsoft })}
                            actions={Object.assign({}, { googleAuth, microsoftAuth })}
                            {...props} />
                        <Box mt={5}>
                            <Copyright textColor={formState === 'requestDemo' ? '#ffff' : '#000'} />
                        </Box>
                    </React.Fragment> || null}
            </div>
        </Grid>
        <CssBaseline />
        {authenticate && (<ModalForm
            title={"Authentication Required"}
            fields={webAuthenticationForm.filter(_ => _.editRecord && !_.disableRecord)}
            form={`webAuthentication`}
            show
            disableCancelBtn
            disableContainer
            onSubmit={handleWebAuthenticate}
            btnLabel="Submit" />)}
    </Grid>);
}

HomePage1.propTypes = {
    children: PropTypes.object,
    dispatch: PropTypes.func,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    loggedIn: PropTypes.bool,
    match: PropTypes.object,
    metaData: PropTypes.object,
    pathData: PropTypes.object,
    payment: PropTypes.object,
    queryParams: PropTypes.object,
    setQueryParams: PropTypes.func,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    user: PropTypes.object,
    authPageLoader: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
    loggedIn: selectLoggedIn(),
    user: selectUser(),
    error: selectError(),
    success: selectSuccess(),
    payment: selectPayment(),
    loading: selectLoading(),
    metaData: selectMetaData(),
    respondDetail: selectRespondDetails(),
    discountDetail: selectDiscountDetail(),
    authUserDetails: selectAuthUserDetails(),
    authPageLoader: selectAuthPageLoader(),
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withConnect,
    memo,
    withQueryParams()
)(HomePage1);

/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import history from 'utils/history';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';

import { CssBaseline, Box, Grid } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import { selectLoggedIn, selectUser, selectError, selectSuccess, selectPayment, selectLoading, selectMetaData } from 'blocks/session/selectors';
import { logIn, signUp, forgotPassword, requestSupport, requestDemo, clearCache, setupStripePayment, loadSettingsPlan, verifyOtp } from 'blocks/session/actions';
import Copyright from 'components/Copyright';
import useStyles from './styles';

import { Helmet } from 'react-helmet';
import Header from 'components/Header';
import HeaderLinks from 'components/HeaderLinks';
import Drawer from '@material-ui/core/Drawer';
import Footer from 'components/Footer';
import Spinner from 'components/Spinner';

import { ImplementationForm, ImplementationPage } from './utils';

import withQueryParams from 'react-router-query-params';

/**
 * @param {object} props 
 */
export function HomePage(props) {

  const { error, location, dispatch, success, match, queryParams, payment, loading, metaData = {} } = props;
  const { state = {} } = location;
  const { form, page, identifier, secret } = state;
  const classes = useStyles();
  const elements = useElements();
  const stripe = useStripe();

  const theme = useTheme();
  const sm = useMediaQuery(theme.breakpoints.up('sm'));
  const pageType = match.path.includes("signin") ? 'login' : match.path.includes("login") ? 'login' : match.path.includes("signup") ? 'register' : match.path.includes("forgot") ? 'forgot' : false;

  const formState = form || pageType || false;
  const pageState = page || 'overview';

  const Component = formState && ImplementationForm[formState];
  const Page = pageState && ImplementationPage[pageState];

  const [pageLoader, setPageLoader] = useState(false);

  useEffect(() => {
    let mounted = true;
    setPageLoader(true);
    dispatch(loadSettingsPlan());
    setTimeout(() => setPageLoader(false), 3000);
    return () => mounted = false;
  }, []);

  let err = null;

  if ((error && error.login && error.login.response
    && error.login.response.data && error.login.response.data.error) || (error[formState])) {
    const data = (error && error.login && error.login.response && error.login.response.data) || error[formState];
    
    if ((typeof data.error === 'string') || (typeof data === 'string')) {
      err = data && data['error'] || data;
    } else {
      err = data['error'] && data['error'][Object.keys(data.error)[0]] && data['error'][Object.keys(data.error)[0]][0];
    }

  }
  
  const getStripeCard = async ({ dispatch, data, form }) => {
    const result = await stripe.confirmCardSetup(payment && payment.client_secret || location && location.state && location.state.payment && location.state.payment.client_secret || '', {
      payment_method: {
        card: elements.getElement(CardElement),
        billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
      }
    });
    dispatch(setupStripePayment(Object.assign({}, data, result), form, location && location.state || {}));
  }

  const handleSubmit = (data, dispatch, { form }) => {
    if (formState === 'login') {
      dispatch(logIn(data.email, data.password, queryParams, form));
    } else if (formState === 'register') {
      dispatch(signUp(Object.assign({}, data, { role: 'lawyer' }), form));
    } else if (formState === 'forgot') {
      dispatch(forgotPassword(data, form));
    } else if (formState === 'requestDemo') {
      dispatch(requestDemo(data, form));
    } else if (formState === 'loginFailure' && identifier && secret) {
      dispatch(logIn(identifier, secret, queryParams, form));
    } else if (formState === 'stripe') {
      if(data && data.plan_id && data.plan_id === 'free_trial'){
        dispatch(setupStripePayment(Object.assign({}, data, { page: 'register'}), form, location && location.state || {}));
      } else {
        getStripeCard({ dispatch, data: Object.assign({}, data, { page: 'register'}), form });
      }
    } else if(formState === 'verifyOtp' && identifier){
      dispatch(verifyOtp(identifier, data.verifyInput, form))
    }

  }

  const clearSessionCache = () => {
    dispatch(clearCache());
  }

  if (pageLoader) {
    return <Spinner loading={true} showHeight />
  }

  return (<Grid container component="main" className={classes.root}>
    <Helmet
      title="EsquireTek"
      meta={[
        { name: 'description', content: 'Home Page' },
      ]}
    />
    <Header
      color="transparent"
      brand={<img src={require(`images/home/logo1.png`)} style={{ width: sm ? '375px' : '150px' }} />}
      rightLinks={<HeaderLinks locationState={state} />}
      fixed
      changeColorOnScroll={{
        height: 200,
        color: "white"
      }}
      {...props}
    />
    <CssBaseline />
    <Page
      locationState={state}
      errorMessage={err}
      clearCache={clearSessionCache}
      success={success}
    />
    <Footer />
    <Drawer className={formState === 'requestDemo' && classes.componentDrawer || null} anchor={'right'} open={['login', 'register', 'forgot', 'requestDemo', 'loginFailure', 'stripe', 'verifyOtp'].includes(formState) ? true : false} onClose={() => {
      if(location && location.pathname === '/'){
        history.push({
          pathname: '/',
          state: Object.assign({}, { ...state }, { form: false })
        })
      }
    }}>
      <div className={classes.component} style={{ width: sm ? `420px` : `100%` }}>
        {formState &&
          <Component
            form={`${formState}Form`}
            onSubmit={handleSubmit.bind(this)}
            errorMessage={err}
            clearCache={clearSessionCache}
            success={success}
            locationState={state}
            metaData={metaData}
            loading={loading} /> || null}
      </div>
      <div>
        <Box mt={5}>
          <Copyright textColor={formState === 'requestDemo' ? '#ffff' : '#000'} />
        </Box>
      </div>
    </Drawer>
  </Grid>);
}

HomePage.propTypes = {
  children: PropTypes.object,
  dispatch: PropTypes.func,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  history: PropTypes.object,
  location: PropTypes.object,
  loggedIn: PropTypes.bool,
  loading: PropTypes.bool,
  match: PropTypes.object,
  metaData: PropTypes.object,
  queryParams: PropTypes.object,
  setQueryParams: PropTypes.func,
  payment: PropTypes.object,
  pathData: PropTypes.object,
  success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  loggedIn: selectLoggedIn(),
  user: selectUser(),
  error: selectError(),
  success: selectSuccess(),
  payment: selectPayment(),
  loading: selectLoading(),
  metaData: selectMetaData()
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
  withQueryParams()
)(HomePage);

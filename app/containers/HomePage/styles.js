

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${require('images/login/app_bg.jpg')})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '80%',
    marginBottom: '30px',
    padding: '25px'
  },
  title: {
    marginLeft: theme.spacing(0.5),
    fontFamily: 'Avenir-Bold'
  },
  main: {
    backgroundColor: '#FFFEFC'
  },
  button: {
    backgroundColor: 'transparent !important',
    border: '2px solid #e95d0c',
    boxShadow: 'none',
    borderRadius: 'initial',
    textTransform: 'none',
    color: '#e95d0c',
    minWidth: '150px',
    marginTop: '25px',
    fontFamily: 'ClearSans-Regular',
    '&:hover': {
      boxShadow: 'none',
      fontSize: '0.875rem'
    }
  },
  signupBtn: {
    backgroundColor: '#ffff !important',
    border: '2px solid #e95d0c',
    boxShadow: 'none',
    borderRadius: 'initial',
    textTransform: 'none',
    color: '#e95d0c',
    minWidth: '180px',
    fontFamily: 'ClearSans-Regular',
    '&:hover': {
      boxShadow: 'none',
    }
  },
  base: {
    marginTop: '150px', padding: '25px', paddingBottom: '0px'
  },
  contact: {
    marginTop: '120px', paddingBottom: '0px'
  },
  section: {
    textAlign: 'center',
    '& > h2': {
      color: '#e95d0c', fontFamily: 'ClearSans-Regular', fontWeight: 'normal', fontSize: '20px', letterSpacing: '1px', paddingRight: '25px', paddingLeft: '25px'
    },
    '& > h1': {
      color: '#848485', fontFamily: 'ClearSans-Regular', fontWeight: 'normal', fontSize: '28px', letterSpacing: '4px'
    },
    '& > h3': {
      color: '#848485', fontFamily: 'ClearSans-Regular', fontWeight: 'normal', fontSize: '14px',
    },
    '& > p': {
      color: '#0a0a0a', fontFamily: 'ClearSans-Light'
    }
  },
  section2: {
    padding: '25px', textAlign: 'center',
    '& > h2': {
      color: '#e95d0c', fontFamily: 'ClearSans-Regular', fontWeight: 'normal', fontSize: '20px', letterSpacing: '1px', paddingRight: '25px', paddingLeft: '25px'
    },
    '& > h1': {
      color: '#848485', fontFamily: 'ClearSans-Regular', fontWeight: 'normal', fontSize: '28px', letterSpacing: '4px'
    },
    '& > p': {
      color: '#0a0a0a', fontFamily: 'ClearSans-Light', margin: 'auto',
      "@media (min-width: 960px)": {
        maxWidth: '80%'
      }
    }
  },
  section3: {
    '& img': {
      width: '80px', height: '80px', margin: '18px'
    },
    '& h2': {
      color: '#e95d0c', letterSpacing: '1px', fontFamily: 'ClearSans-Regular', fontWeight: 'initial', marginBottom: '0px'
    },
    '& p': {
      fontFamily: 'ClearSans-Light', marginTop: '0px', fontSize: '15px', paddingRight: '25px'
    }
  },
  section4: {
    padding: '25px', textAlign: 'center', backgroundColor: '#EA5D0D', backgroundImage: `url(${require('images/home/white-design.png')})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'right', backgroundSize: '48%',
    '& > h1': {
      fontFamily: 'ClearSans-Regular', fontWeight: 'normal', color: '#ffff', fontSize: '28px', letterSpacing: '4px'
    }
  },
  component: {
    padding: '25px',
  },
  drawer: {
    height: '100%',
  },
  paddTop: {
    paddingTop: '25px'
  },
  sectionImage: {
    backgroundImage: `url(${require('images/home/gallery.png')})`,
    backgroundSize: 'cover',
    minHeight: '460px',
    backgroundRepeat: 'no-repeat',
    backgroundColor: 'transparent',
    width: '100%'
  },
  componentDrawer: {
    '& .MuiPaper-root': {
      backgroundColor: '#ea5d0d'
    }
  }
}));


export default useStyles;
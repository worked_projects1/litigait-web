/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'EsquireTek',
  },
  error: {
    id: `${scope}.error`,
    defaultMessage: 'There was an error loading the resource. Please try again.',
  }
});

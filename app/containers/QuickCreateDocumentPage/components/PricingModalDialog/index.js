/**
 * 
 *  Pricing Modal Dialog
 * 
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button, Card, CardActions, CardContent, Divider, CardHeader, Avatar, CardMedia, CardActionArea } from '@material-ui/core';
import Styles from './styles';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import cx from 'clsx';
import Icons from '../../../ViewCasesDetailPage/components/PricingModalDialog/utils';

function PricingModalDialog({ children, className, style, show, onClose, onConfirmPopUpClose, disableContainer, paperStyle, onOpen, freePlan, paidPlan, upShellColumns, user }) {
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.down('sm'));
    const xs = useMediaQuery(theme.breakpoints.down('xs'));
    const xl = useMediaQuery(theme.breakpoints.up('xl'));
    const { freeTrialFeatures, unlimitedFeatures, guaranteeText, description, details, reviewTitle, reviewDetails, headLine } = upShellColumns;
    const [height, setHeight] = useState(window.innerHeight);
    const billingType = user?.practiceDetails?.billing_type === 'limited_users_billing';

    const listItemRender = (icon, text, strike) => (
        <Grid container className={classes.listContainer} key={text}>
            <Grid>{icon}</Grid>
            <Grid className={!strike ? cx(classes.list, classes.listBold) : cx(classes.list, classes.listStrick)}>
                <span style={{ textDecoration: strike ? 'line-through' : 'none' }}>{text}</span>
            </Grid>
        </Grid>);

    const checkboxIcon = <CheckBoxIcon className={classes.checkboxIcon} />;

    const checkboxOutline = <CheckBoxOutlinedIcon className={classes.checkboxIcon} />;

    const closeIcon = <CloseIcon style={{ fontSize: 18, stroke: 'gray', strokeWidth: '2px' }} />;

    const descriptionFooter = (
        <Typography component="span" className={classes.demoLink}>
            <span className={classes.demoLinkText} onClick={() => window.open('https://www.esquiretek.com/request-a-demo/', '_blank')}>Request a Demo</span>
        </Typography>
    );

    const handleClose = () => {
        setModalOpen(false);
        if (onClose) {
            onClose();
        }
    }

    const updateHeight = () => {
        setHeight(window.innerHeight);
    }

    useEffect(() => {
        window.addEventListener('resize', () => updateHeight());
        return window.removeEventListener('resize', () => updateHeight())
    }, []);


    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleClose}
            onRendered={onOpen}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show}>
                <Paper className={classes.paper} style={paperStyle || {}}>
                    <Grid container justify="flex-end" className={classes.close}>
                        <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                    </Grid>
                    <Grid container justify="center">
                        <Grid item >
                            <Typography component="h1" variant="h5" className={classes.title}>
                                {headLine}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container className={!xl ? classes.totalGrid : xl && height < 690 ? classes.totalGrid : null} style={{ marginTop: '20px', marginBottom: '20px'}}>
                        <Grid container className={classes.innerMainGrid} >
                            <Grid item xl={7}>
                                <Grid container className={classes.firstGrid} direction={sm || xs ? 'column' : 'row'} item xs={12}>
                                    <Grid className={classes.cardGrid}>
                                        <Grid item xs={12}>
                                            <Grid className={classes.welcomeChild}></Grid>
                                        </Grid>
                                        <Card className={`${classes.card} ${classes.trial}`}>
                                            <CardContent className={classes.headerContent} style={{ color: 'black' }}>
                                                <Grid item xs={12} className={classes.welcomeNote}>
                                                    <Grid>
                                                        <Typography className={classes.freeTrial} variant="subtitle1" component='span'
                                                        >TEK TRIAL
                                                        </Typography>
                                                    </Grid>
                                                    <Grid className={classes.price}>FREE</Grid>
                                                    <Grid className={classes.billedPlan}>2 Document limits</Grid>
                                                </Grid>
                                            </CardContent>
                                            <Divider style={{ background: theme?.palette?.cardElementColor?.main }} />
                                            <CardContent className={`${classes.cardContent} ${billingType ? classes.freeTrialCardList1 : classes.freeTrialCardList}`}>
                                                {freeTrialFeatures?.map(el => listItemRender(!el?.strike ? checkboxIcon : closeIcon, el?.label, el?.strike))}
                                            </CardContent>
                                            <CardActions className={billingType ? classes.freeTrialCard1 : classes.freeTrialCard}>
                                                <Grid className={classes.startedBtnGrid}>
                                                    <Button className={classes.startedBtn} style={{ background: theme?.palette?.cardElementColor?.main }} onClick={freePlan}>Get Started</Button>
                                                </Grid>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                    <Grid className={classes.cardGrid} style={{ marginTop: sm || xs ? '25px' : '0px' }}>
                                        <Grid item xs={12} className={classes.welcome} style={{ textAlign: 'center' }}>
                                            <Grid className={`${classes.welcomeChild} ${classes.bestValue}`}>Best Value</Grid>
                                        </Grid>
                                        <Card className={`${classes.card} ${classes.yearly}`}>
                                            <CardContent className={classes.headerContent} style={{ color: 'black', paddingBottom: '8px' }}>
                                                <Grid item xs={12} className={classes.welcomeNote}>
                                                    <Grid>
                                                        <Typography className={classes.tekYearly} variant="subtitle1" component='span'
                                                        >TEK YEARLY
                                                        </Typography>
                                                    </Grid>
                                                    <Grid className={classes.price}>$425</Grid>
                                                    <Grid className={classes.billedPlan}>
                                                        Billed Annually
                                                    </Grid>
                                                    {billingType ? <Grid item style={{ marginBottom: '5px', marginTop: '5px' }}>
                                                        <Grid>Additional users</Grid>
                                                        <Grid>$79/user/month</Grid>
                                                    </Grid> : null}
                                                </Grid>
                                            </CardContent>
                                            <Grid className={classes.centerAlign} style={{ height: '20px' }}>
                                                <Grid item className={classes.saveMoney}>
                                                    <Icons type="fire" classes={classes.fireEmoji} /> save $840/year
                                                </Grid>
                                            </Grid>
                                            <CardContent className={`${classes.cardContent} ${classes.yearlyCardList}`}>
                                                {unlimitedFeatures?.map(el => listItemRender(checkboxOutline, el?.label, el?.strike))}
                                                <Grid container className={classes.listContainer}>
                                                    <Grid item xs={12} className={classes.activationFeeLable}>
                                                        $249 Activation Fee
                                                    </Grid>
                                                </Grid>
                                            </CardContent>
                                            <CardActions className={classes.cardActions}>
                                                <Grid className={classes.startedBtnGrid}>
                                                    <Button className={classes.startedBtn} style={{ background: theme?.palette?.cardElementColor?.main }} onClick={()=> paidPlan('yearly')}>Get Started</Button>
                                                </Grid>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                    <Grid className={classes.cardGrid}>
                                        <Grid item xs={12} className={classes.welcome}>
                                            <Grid className={classes.welcomeChild}></Grid>
                                        </Grid>
                                        <Card className={`${classes.card} ${classes.monthly}`}>
                                            <CardContent className={classes.headerContent} style={{ color: 'black' }}>
                                                <Grid item xs={12} className={classes.welcomeNote}>
                                                    <Grid>
                                                        <Typography className={classes.monthlyText} variant="subtitle1" component='span'>
                                                            TEK MONTHLY
                                                        </Typography>
                                                    </Grid>
                                                    <Grid className={classes.price}>$495</Grid>
                                                    <Grid className={classes.billedPlan}>Billed Monthly</Grid>
                                                    {billingType ? <Grid item style={{ marginBottom: '5px', marginTop: '5px' }}>
                                                        <Grid>Additional users</Grid>
                                                        <Grid>$89/user/month</Grid>
                                                    </Grid> : null}
                                                </Grid>
                                            </CardContent>
                                            <Divider style={{ background: theme?.palette?.cardElementColor?.main }} />
                                            <CardContent className={`${classes.cardContent} ${classes.monthlyCardList}`} style={{  }}>
                                                {unlimitedFeatures?.map(el => listItemRender(checkboxOutline, el?.label, el?.strike))}
                                                <Grid container className={classes.listContainer}>
                                                    <Grid item xs={12} className={classes.activationFeeLable}>
                                                        $249 Activation Fee
                                                    </Grid>
                                                </Grid>
                                            </CardContent>
                                            <CardActions className={billingType ? classes.tekMonthlyCard1 : classes.tekMonthlyCard}>
                                                <Grid className={classes.startedBtnGrid}>
                                                    <Button className={classes.startedBtn} style={{ background: theme?.palette?.cardElementColor?.main }} onClick={()=> paidPlan('monthly')}>Get Started</Button>
                                                </Grid>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                </Grid>
                                <Grid className={classes.secGrid} item xs={12}>
                                    <Grid container className={classes.secGrid}>
                                        <Grid>
                                            <Icons type="guarantee" classes={classes.guaranteeLogo} />
                                        </Grid>
                                        <Grid style={{ textAlign: (sm || xl) ? 'center' : 'left', padding: '0px 16px', lineHeight: '1' }}>
                                            <Typography className={classes.guaranteeText}>{guaranteeText}</Typography>
                                            <Typography className={classes.guaranteeDescription} style={{ textAlign: (sm || xl) ? 'center' : 'left', color: "black" }}>
                                                <span dangerouslySetInnerHTML={{ __html: description }} /><br></br>
                                            </Typography>
                                            {xl && descriptionFooter || null}
                                        </Grid>
                                    </Grid>
                                    {!xl && descriptionFooter || null}
                                </Grid>
                            </Grid>
                            <Grid item xl={5}>
                                <Grid container={xl ? false : true} item xs={12} justify='center' style={{ marginTop: '20px' }}>
                                    <Grid item xs={12} className={`${classes.centerAlign} ${classes.ratingContainer}`}>
                                        {Array.from(Array(5)).map((opt, index) => {
                                            return <Grid key={index} style={{ marginRight: '10px' }}>
                                                <Icons type={'rating'} classes={classes.ratingLogo} />
                                            </Grid>
                                        })}
                                    </Grid>
                                    <Typography className={classes.reviewTitle}>{reviewTitle}</Typography>
                                    <Grid container item xs={12} justify='center' spacing={2}>
                                        {reviewDetails?.map((e, i) => {
                                            return (<Grid item key={i}>
                                                <Card className={classes.root} variant="outlined">
                                                    <Grid item className={classes.centerAlign} style={{ padding: '10px', height: '80px' }}>
                                                        <Icons type={e.logo} style={e.style} />
                                                    </Grid>
                                                    <CardContent>
                                                        <Grid container className={classes.centerAlign} direction='column'>
                                                            <Grid item xs={12} className={classes.centerAlign} style={{ marginBottom: '10px' }}>
                                                                {Array.from(Array(5)).map((opt, index) => {
                                                                    return <Grid key={index} style={{ marginRight: '3px' }}>
                                                                        <Icons type={e.rating} classes={classes.rating} />
                                                                    </Grid>
                                                                })}
                                                            </Grid>
                                                            <Grid>
                                                                <span className={classes.reviewDetails} dangerouslySetInnerHTML={{ __html: e.comment }} /> 
                                                            </Grid>
                                                        </Grid>
                                                    </CardContent>
                                                </Card>
                                            </Grid>)
                                        })}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid >
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

PricingModalDialog.propTypes = {
    title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default PricingModalDialog;
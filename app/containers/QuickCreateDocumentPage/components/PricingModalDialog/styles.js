

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    title: {
        fontSize: '22px'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        margin: "0px 15px",
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '12px',
        minWidth: '30%',
        outline: 'none',
        paddingBottom: '35px',
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        width: '210px',
        outline: 'none',
        textTransform: 'capitalize'
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitleOptions: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitle: {
        margin: '5px',
        marginTop: '10px',
        paddingLeft: '20px',
        paddingRight: '20px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    gridButton: {
        textAlign: 'center'
    },
    imgSection: {
        marginTop: '50px',
        marginBottom: '50px'
    },
    uploadImage: {
        display: 'flex',
        width: '200px',
        margin: 'auto',
        backgroundRepeat: 'no-repeat',
    },
    uploadBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        marginRight: '15px',
        textTransform: 'none !important'
    },
    spinner: {
        padding: '20px',
        marginTop: '100px',
    },
    form: {
        display: 'inline',
        textAlign: 'center',
    },
    linkColor: {
        color: '#0077c5',
        textDecoration: 'underline'
    },
    welcomeNote: {
        textAlign: 'center',
    },
    checkedIcon: {
        width: '18px',
        height: '18px',
        marginRight: '10px'
    },
    itemDiv: {
        marginTop: '10px',
        marginBottom: '20px'
    },
    bottomDiv: {
        marginBottom: '15px'
    },
    steps: {
        marginBottom: '5px'
    },
    cardContent: {
        fontSize: '12px',
        padding: '10px',
        color: '#000'
    },
    card: {
        height: "90%",
        minWidth: '225px',
        padding: theme.spacing(1.5),
        paddingTop: '8px',
        [theme.breakpoints.up('sm')]: {
            padding: theme.spacing(1.5),
        },
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(1.5),
        },
        [theme.breakpoints.up('lg')]: {
            padding: theme.spacing(2),
        },

    },
    startedBtn: {
        fontFamily: 'Avenir-Bold',
        color: '#fff',
        padding: '6px 17px',
        width: '100%',
        borderRadius: '28px',
        '&:hover': {
            backgroundColor: 'green !important'
        },
    },
    startedBtnGrid: {
        textAlign: 'center',
        width: '100%'
    },
    cardGrid: {
        margin: '0px 12px',
    },
    headerContent: {
        paddingTop: '0px',
        padding: '0px 12px 12px 12px;'
    },
    cardActions: {
        marginBottom: '15px',
    },
    freeTrialCardList: {
        paddingTop: '22px',
        [theme.breakpoints.up('lg')]: {
            paddingTop: '32px',
        }
    },
    monthlyCardList: {
        paddingTop: '24px',
        [theme.breakpoints.up('lg')]: {
            paddingTop: '30px',
        }
    },
    yearlyCardList: {
        paddingTop: '10px',
    },
    freeTrialCard: {
        marginTop: '10px',
        marginBottom: '15px'
    },
    tekMonthlyCard: {
        marginBottom: '15px',
        [theme.breakpoints.up('lg')]: {
            marginTop: '3px',
            marginBottom: '22px',
        }
    },
    saveGrid: {
        color: 'black',
        background: '#d3d3d3',
        fontSize: '12px',
        borderRadius: '15px',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    list: {
        alignSelf: 'flex-end',
        display: 'flex',
        paddingTop: '2px',
        marginLeft: '6px',
    },
    listStrick: {
        color: 'gray'
    },
    listBold: {
        fontWeight: 'bold'
    },
    firstGrid: {
        display: 'flex',
        paddingBottom: '6px',
        justifyContent: 'center'
    },
    secGrid: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '6px'
    },
    guaranteeDescription: {
        fontSize: '0.6875rem',
        textAlign: 'center',
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9375rem'
        }
    },
    guaranteeLogo: {
        height: '75px',
    },
    totalGrid: {
        marginTop : "5px",
        justifyContent: "center",
        maxHeight: '80vh',
        overflowY: 'auto',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 8,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },
    listContainer: {
        flexWrap: 'nowrap',
        fontSize: '12px',
        [theme.breakpoints.up("lg")]: {
            fontSize: '14px',
        }
    },
    welcomeChild: {
        fontSize: '15px',
        height: '30px',
    },
    guaranteeImg: {
        width: '80px',
        height: '80px',
    },
    fire: {
        width: '10px',
        height: '15px'
    },
    centerAlign: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    saveMoney: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        fontSize: '13px',
        letterSpacing: '1px',
        backgroundColor: '#d3d3d3 !important',
        padding: '1%',
        width: '200px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fireEmoji: {
        width: '13px',
        height: '16px',
        marginRight: '3px'
    },
    innerMainGrid: {
        justifyContent: 'center'
    },
    planBody: {
        textAlign: 'left',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 'max-content',
        maxHeight: '500px',
        overflowX: 'hidden',
        overflowY: "auto",
        "&::WebkitScrollbar": {
            width: 8,
        },
        "&::WebkitScrollbarTrack": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::WebkitScrollbarThumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },
    billedPlan: {
        fontSize: '12px',
        fontWeight: 'bold',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    activationFeeLable: {
        paddingLeft: "18px",
        paddingTop: "10px",
        fontWeight: "bold"
    },
    ratingLogo: {
        width: '33px'
    },
    rating: {
        width: '15px',
        [theme.breakpoints.up('lg')]: {
            width: '20px',
        }
    },
    reviewDetails: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: '12px', 
        textAlign: 'center',
        fontSize: '13px',
        fontWeight: 700,
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem',
        }
    },
    ratingContainer: {
        dispaly: 'inline-block', 
        margin: '-3px', 
        paddingBottom: '5px'
    },
    root: {
        width: 230,
        height: 245,
        [theme.breakpoints.up('lg')]: {
            width: '270px'
        }
    },
    trial: {
        border: `2px solid ${theme?.palette?.cardBorderColor?.main}`, 
        background: theme?.palette?.cardBackgroundColor?.main, 
        boxShadow: '0px 0px 8px #adadad', 
        paddingBottom: '0px',
    },
    monthly: {
        border: `2px solid ${theme?.palette?.cardBorderColor?.main}`, 
        background: theme?.palette?.cardBackgroundColor?.main, 
        boxShadow: '0px 0px 8px #adadad', 
        paddingBottom: '0px',
    },
    yearly: {
        border: `2px solid ${theme?.palette?.cardBorderColor2?.main}`, 
        background: "#fff", 
        boxShadow: '0px 4px 10px #adadad', 
        borderRadius: '0px 0px 4px 4px', 
        paddingBottom: '0px',
    },
    bestValue: {
        lineHeight: '2rem', 
        background: theme?.palette?.cardBackgroundColor2?.main, 
        color: '#fff', 
        fontFamily: 'Avenir-Bold', 
        borderRadius: '4px 4px 0px 0px',
        height: '30px',
        [theme.breakpoints.down('sm')]: {
            height: '100%'
        },
    },
    guaranteeText: {
        fontSize: '15px', 
        fontFamily: 'Avenir-Bold', 
        color: theme?.palette?.cardElementColor?.main,
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.125rem'
        },
    },
    monthlyText: { 
        fontSize: '18px', 
        color: theme?.palette?.cardElementColor?.main, 
        fontFamily: 'Avenir-Bold' 
    },
    reviewTitle: { 
        fontSize: '20px', 
        fontWeight: 'bold', 
        margin: '20px' ,
        textAlign: 'center',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.4375rem'
        },
    },
    tekYearly: {
        fontSize: '18px', 
        color: 'black', 
        fontFamily: 'Avenir-Bold',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.375rem'
        },
    },
    freeTrial: {
        fontSize: '18px', 
        color: theme?.palette?.cardElementColor?.main, 
        fontFamily: 'Avenir-Bold'
    },
    price: { 
        fontSize: '23px', 
        fontFamily: 'Avenir-Bold', 
        [theme.breakpoints.up('lg')]: {
            fontSize: '35px'
        },
    },
    checkboxIcon: { 
        color: theme?.palette?.cardElementColor?.main, 
        fontSize: '18px',
        [theme.breakpoints.up('lg')]: {
            fontSize: '24px'
        }
    },
    demoLink: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#2ca01c',
        fontWeight: 700,
        fontSize: '14px', 
        textAlign: 'center',
      },
    demoLinkText: {
        cursor: 'pointer',
        textDecoration: 'underline',
        '&:hover': {
            textDecoration: 'none'
        }
    },
    freeTrialCardList1: {
        paddingTop: '83px'
    },
    freeTrialCard1: {
        marginTop: '34px',
        marginBottom: '15px',
        "@media (min-width: 960px) and (max-width: 1279px)": {
            marginTop: '19px',
            marginBottom: '26px'
        },
        "@media (min-width: 1281px)": {
            marginTop: '34px',
            marginBottom: '15px',
        }
    },
    tekMonthlyCard1: {
        marginBottom: '15px',
        [theme.breakpoints.up('lg')]: {
            marginTop: '3px',
            marginBottom: '35px',
        }
    },
}));


export default useStyles;
/**
 * 
 * Quick Create Document Page
 * 
 */



import React, { useEffect, memo, useState, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';
import ModalForm from 'components/ModalRecordForm';
import { validation } from './utils';
import Styles from './styles';
import { getStepper } from 'utils/utils';
import Stepper from 'components/Stepper';
import Snackbar from 'components/Snackbar';
import PricingModalDialog from './components/PricingModalDialog';
import store2 from 'store2';
import { Link } from 'react-router-dom';
import { selectForm, selectUser, selectActiveSession } from 'blocks/session/selectors';
import { planConfirmationMessage, useDidUpdate } from 'utils/tools';
import { SubscriptionSubmitButton } from 'containers/ViewCasesDetailPage/components/DocumentButton';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import Spinner from 'components/Spinner';
import Icons from '../ViewCasesDetailPage/components/PricingModalDialog/utils';
import AlertDialog from 'components/AlertDialog';
import moment from 'moment';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, legalForms, actions, selectors) {

    const {
        selectLoading,
        selectRecord,
        selectRecordsMetaData,
        selectHeaders,
        selectSuccess,
        selectUpdateError,
        selectKey,
        selectProgress,
        selectContentNotFound,
        sessionError,
        selectSettingsDiscount,
        selectSubscriptionBilling,
        selectSubscriptionSuccess,
        selectQuickCreateDescription
    } = selectors;

    const { subscriptionDetailsColumns, upShellColumns, quickCreateDescriptionModal } = columns;
    const { uploadDocumentColumns } = legalForms();

    /**
     * @param {object} props 
     */
    function QuickCreateDocumentPage(props) {

        const { location = {}, dispatch, children, user, active, error, success, record, contentNotFound, metaData, loading, forms, discountDetails, billing, loader, subscriptionSuccess, activeSession, quickCreateDescription } = props;
        const classes = Styles();
        const gettingStarted = store2.get('getting_started');
        const [progress, setProgress] = useState(false);
        const [stepperLoader, setStepperLoader] = useState(false);
        const [openModel, setOpenModel] = useState(false);
        const [getStarted, setGetStarted] = useState(gettingStarted);
        const [pageLoader, setPageLoader] = useState(false);
        const [planType, setPlanType] = useState(false);
        const [showQuickCreateDescription, setShowQuickCreateDescription] = useState(user.quick_create_notification);
        const elements = useElements();
        const stripe = useStripe();
        const modalOpenRef = useRef(null);
        const { pathname } = location;
        const fullView = pathname.indexOf('reviewDocument') > -1 ? true : false;
        const standardForm = (pathname.indexOf('standardForm') > -1) || (pathname.indexOf('questionsForm') > -1) ? true : false;

        const { title, steps, successMessage } = progress && getStepper(progress) || {};

        const subscriptionFormValues = forms && forms[`initialSubscriptionForm`] && forms[`initialSubscriptionForm`].values || false;
        const cardDetails = subscriptionFormValues && subscriptionFormValues.plan_id && subscriptionFormValues.cardDetails && subscriptionFormValues.cardDetails['complete'] || false;
        const plans = metaData && metaData['respondingPlansOptions'] && metaData['respondingPlansOptions'].length > 0 && metaData['respondingPlansOptions'] || [];

        const currentPlan = subscriptionFormValues && plans && Array.isArray(plans) && plans.length > 0 && plans.find(_ => _.value === subscriptionFormValues['plan_id']) || false;
        const oneTimeActivationFee = user && user.practiceDetails && user.practiceDetails.one_time_activation_fee || false;
        const freePlan = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['responding'] && user.plan_type['responding'] === 'free_trial' || false;

        const errorMessage = sessionError && sessionError.litify;

        let selectedPlan = planType && plans?.filter(el => el.plan_type.includes(planType)) || false;
        selectedPlan = selectedPlan?.[0]?.plan_id || false;
        let upShellFeatureColumns = upShellColumns && user && typeof upShellColumns === 'function' ? upShellColumns(user) : upShellColumns;
        const passwordChanges = (moment().diff(user?.lastPasswordChangeDate, 'days') >= 90 || user?.temporary_password) ? true : false;

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords(true));
            dispatch(actions.loadBillingDetails(false, false, false, { document_type: 'FROGS' }, 'template'));
            return mounted = false;
        }, []);

        useEffect(()=>{
            setShowQuickCreateDescription(user?.quick_create_notification);
        }, [user]);

        const handleModel = () => {
            setOpenModel(true);
        }

        const handleRequestHelp = () => {
            setStepperLoader(true);
            dispatch(actions.extractionRequestHelp(record, setStepperLoader));
        }

        const handleSubmitUploadDoc = (data, dispatch, { form }) => {
            setProgress('uploadDoc');
            dispatch(actions.uploadDocumentForm(Object.assign({}, {
                user_id: user?.id,
                practice_id: user?.practiceDetails?.id,
                pdf_filename: data.document_file[0].name,
                document_file: data.document_file,
                content_type: 'application/pdf',
                legalform_filename: data.filename,
                quickCreate: true,
            }), setProgress, form));
        }

        const handleCancelStepper = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
            setStepperLoader(false);
            dispatch(actions.cancelDocumentForm(Object.assign({}, {
                user_id: user?.id,
                practice_id: user?.practiceDetails?.id,
                id: record?.id,
                status: 'cancelled',
            }), actions.loadRecords));
        }

        const handleCloseStepper = () => {
            dispatch(actions.loadRecordsCacheHit());
            setProgress(false);
            setStepperLoader(false);
            if (!error && !success) {
                dispatch(actions.cancelDocumentForm(Object.assign({}, {
                    user_id: user?.id,
                    practice_id: user?.practiceDetails?.id,
                    id: record?.id,
                    status: 'cancelled',
                }), actions.loadRecords));
            }
        }

        const handleGettingStarted = () => {
            store2.remove('getting_started');
            setGetStarted(false);
            setOpenModel(true);
        }

        const handleCloseGettingStarted = () => {
            store2.remove('getting_started');
            setGetStarted(false);
        }

        const confirmMessage = planConfirmationMessage(currentPlan, discountDetails, oneTimeActivationFee, subscriptionFormValues, 'responding', 'upshell', false, false, props, false);

        const handleSubscription = (type, openSubscriptionForm) => {
            setPlanType(type)
            handleCloseGettingStarted();
            openSubscriptionForm();
        }

        const handleUpdatePlanDetails = async (data, dispatch, { form }) => {
            let result = await stripe.confirmCardSetup(billing.client_secret, {
                payment_method: {
                    card: elements.getElement(CardElement),
                    billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                }
            });

            const { respondingPlansOptions: plans = [] } = metaData;
            const selectedPlan = data && data['plan_id'] && plans && Array.isArray(plans) && plans.length > 0 && plans.find(_ => _.plan_id === data['plan_id']) || false;

            dispatch(actions.initialSubscription(result, Object.assign({}, { ...data }, { plan_category: selectedPlan.plan_category }), form, actions, modalOpenRef.current));
        }

        const openAlertDialog = ({ submitRecord, dialog, form, cardDetails, handleSubmitForm }) => {
            let submitRecords = Object.assign({}, { ...submitRecord, type: 'responding', practice_id: user?.practiceDetails?.id });
            if(user?.practiceDetails?.billing_type === 'limited_users_billing') {
                dispatch(actions.subscriptionLicenseValidation(Object.assign({}, submitRecords), form, dialog, cardDetails, handleSubmitForm));
            } else {
                dispatch(actions.settingsDiscountCode(Object.assign({}, submitRecords), form, dialog, cardDetails, handleSubmitForm));
            }
        }

        const subscriptionColumns = subscriptionDetailsColumns && typeof subscriptionDetailsColumns === 'function' && subscriptionDetailsColumns().columns && subscriptionDetailsColumns(record, billing).columns.filter(_ => _.editRecord && !_.disableRecord);


        const handleShowQuickCreateDescription = () => {
            if (quickCreateDescription){
                dispatch(actions.updateQuickCreateNotification(user.id));
                setShowQuickCreateDescription(false);
                
            }else{
                setShowQuickCreateDescription(false);
            }
        }

        if (standardForm) {
            return children;
        }

        if (!billing?.client_secret && !error && !errorMessage && user && !user?.id) {
            return <Spinner className={classes.spinner} />;
        }

        return !standardForm && !fullView ? (<Grid container>
            <Grid item xs={12}>
                <Grid item xs={12} className={classes.imgSection}>
                    <img onClick={handleModel} className={classes.uploadImage} src={require('images/integrations/upload_docs1.png')} />
                </Grid>
                <Grid item xs={12}>
                    <Grid style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', width: '85%', margin: 'auto' }}>
                        <Typography>Upload your document to automatically extract questions. This will also create a new case and client within our system.</Typography>
                        <Typography>(For existing cases, upload from the <Link to={{ pathname: '/casesCategories/cases', state: { ...location.state } }} className={classes.linkColor}>Cases page</Link> to ensure all docs related to a case are in one place.)</Typography>
                    </Grid>
                    <Grid style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', width: '85%', margin: 'auto', marginTop: '10px' }}>
                        <Typography className={classes.contentTxt}>California Form Interrogatories are not supported via Quick Create.</Typography>
                        <Typography className={classes.contentTxt}>You must create a new client or a new case to upload Form Interrogatories. Thank you for your understanding.</Typography>
                    </Grid>
                    <Grid style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Button type="button" variant="contained" color="primary" className={classes.uploadBtn} onClick={handleModel}>Upload Document</Button>
                    </Grid>
                    <ModalForm
                        fields={uploadDocumentColumns && uploadDocumentColumns.filter(_ => _.editRecord)}
                        form={`upload_document_form`}
                        btnLabel="Upload"
                        show={openModel && !showQuickCreateDescription}
                        className={classes.form}
                        metaData={metaData}
                        onClose={() => setOpenModel(false)}
                        onSubmitClose={true}
                        validate={(f, p) => validation(f, Object.assign({}, p))}
                        footerStyle={{ borderTop: 'none' }}
                        onSubmit={handleSubmitUploadDoc.bind(this)} />
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Stepper
                    title={title}
                    active={active}
                    show={progress ? true : false}
                    close={handleCloseStepper}
                    steps={steps}
                    error={error}
                    success={success}
                    loader={stepperLoader}
                    disableCancelBtn={record?.id ? false : true}
                    errorBtnText={!contentNotFound ? "Request Help" : false}
                    errorBtnAction={!contentNotFound ? handleRequestHelp : false}
                    cancel={handleCancelStepper}
                    successMessage={successMessage} />
            </Grid>
            <Grid item xs={12}>
                <ModalForm
                    initialValues={Object.assign({}, { plan_id: selectedPlan || false })}
                    title={'Subscription'}
                    fields={subscriptionColumns}
                    form={`initialSubscriptionForm`}
                    onSubmit={handleUpdatePlanDetails.bind(this)}
                    btnLabel="Submit"
                    style={{ paddingRight: '0px' }}
                    confirmButton={true}
                    confirmPopUpClose
                    disableSubmitBtn={true}
                    metaData={metaData}
                    progress={loader}
                    paperClassName={classes.modalPaper}
                    disableCloseModal={loader ? true : false}
                    disableCancelBtn={loader ? true : false}
                    footerBtn={({ lg }) => {
                        return (<Grid container justify={lg ? "flex-start" : "flex-end"} style={lg ? { display: 'flex', alignItems: 'center', position: 'relative', top: '44px' } : { display: 'flex', alignItems: 'center' }}>
                            <Icons type={'guarantee'} style={{ width: '53px' }} />
                            <Typography style={{ fontSize: '13px' }}>{'30-Day Money Back Guarantee'}</Typography>
                        </Grid>)
                    }}
                    footerSubmitBtn={(setModalOpen, props) => {
                        const { handleSubmit } = props;
                        return <SubscriptionSubmitButton
                            classes={classes}
                            handleSubmitForm={() => {
                                handleSubmit();
                                modalOpenRef.current = setModalOpen;
                            }}
                            {...props}
                            loading={loader}
                            metaData={metaData}
                            formValues={subscriptionFormValues}
                            cardDetails={cardDetails}
                            currentPlan={currentPlan}
                            oneTimeActivationFee={oneTimeActivationFee}
                            discountDetails={discountDetails}
                            openAlertDialog={openAlertDialog}
                            handleSubmit={handleSubmit}
                            confirmationMessage={confirmMessage}
                            enableSubmitBtn
                        />
                    }} >
                    {(openSubscriptionForm) => {
                        return <PricingModalDialog
                            show={!loading && user && activeSession && Object.keys(user).length > 0 && getStarted && freePlan && !user?.notification && !showQuickCreateDescription}
                            paidPlan={(type) => handleSubscription(type, openSubscriptionForm)}
                            freePlan={() => handleGettingStarted()}
                            className={classes.form}
                            scrollBarTitle={true}
                            paperStyle={{ maxWidth: '80%', paddingBottom: '12px' }}
                            onClose={handleCloseGettingStarted}
                            upShellColumns={upShellFeatureColumns} 
                            user={user}/>
                    }}
                </ModalForm>
            </Grid>
            <AlertDialog
                description={quickCreateDescriptionModal}
                openDialog={!loading && user && activeSession && Object.keys(user).length > 0 && user?.id && showQuickCreateDescription && quickCreateDescriptionModal && !user?.notification && !passwordChanges}
                closeDialog={() => setShowQuickCreateDescription(false)}
                onConfirm={handleShowQuickCreateDescription}
                onConfirmPopUpClose={true}
                btnLabel1='OK'
                actions={actions}
                dispatch={dispatch}
                disableCloseModal
            />
            {!progress ? <Snackbar show={error || errorMessage || success || subscriptionSuccess ? true : false} text={error || errorMessage || success || subscriptionSuccess} severity={(error || errorMessage) && 'error' || 'success'} handleClose={() => errorMessage ? dispatch(actions.clearCache()) : dispatch(actions.loadRecordsCacheHit())} /> : null}
        </Grid>) : (
            <Grid container>
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
        )
    }

    QuickCreateDocumentPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        headers: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        records: PropTypes.array,
        totalPageCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
        filterObjection: PropTypes.object,
        user: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
    };

    const mapStateToProps = createStructuredSelector({
        headers: selectHeaders(),
        loading: selectLoading(),
        user: selectUser(),
        active: selectKey(),
        loader: selectProgress(),
        error: selectUpdateError(),
        success: selectSuccess(),
        contentNotFound: selectContentNotFound(),
        metaData: selectRecordsMetaData(),
        record: selectRecord(),
        forms: selectForm(),
        discountDetails: selectSettingsDiscount(),
        billing: selectSubscriptionBilling(),
        subscriptionSuccess: selectSubscriptionSuccess(),
        activeSession: selectActiveSession(),
        quickCreateDescription: selectQuickCreateDescription()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(QuickCreateDocumentPage);

}
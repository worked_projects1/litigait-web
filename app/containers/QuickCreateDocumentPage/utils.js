export const validation = (formData, propsData) => {

    const { fields, form } = propsData || {};
    const requiredFields = fields.reduce((a, el) => {
      if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object' && ((el.mandatory.create && form.indexOf('create') > -1) || (el.mandatory.edit && form.indexOf('edit') > -1)))) {
        a.push(el.value)
      }
      return a;
    }, []);
    const errors = {};

    requiredFields.forEach(field => {
      if ((!formData[field] && field != 'dob' && typeof formData[field] === 'boolean') || (!formData[field] && field != 'dob' && formData[field] != '0')) {
        errors[field] = 'Required'
      } 
    })

    if(formData.filename && formData.filename.length > 40){
        errors.filename = 'Only 40 characters allowed.';
    }
    
    return errors;
}
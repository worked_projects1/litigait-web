

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    imgSection: {
        marginTop: '50px',
    },
    uploadImage: {
        display: 'flex',
        width: '200px',
        margin: 'auto',
        backgroundRepeat: 'no-repeat',
        cursor: 'pointer'
    },
    uploadBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        marginRight: '15px',
        textTransform: 'none !important'
    },
    spinner: {
        padding: '20px',
        marginTop: '100px',
    },
    form: {
        display: 'inline',
        textAlign: 'center',
    },
    linkColor: {
        color: '#0077c5',
        textDecoration: 'underline'
    },
    welcomeNote: {
        fontSize: '26px',
        fontFamily: 'Avenir-Bold',
        textAlign: 'center',
        margin: '15px 0px'
    },
    checkedIcon: {
        width: '18px',
        height: '18px',
        marginRight: '10px'
    },
    itemDiv: {
        marginTop: '10px',
        marginBottom: '20px'
    },
    bottomDiv: {
        marginBottom: '15px'
    },
    steps: {
        marginBottom: '5px'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    modalPaper: {
        width: '50% !important',
        [theme.breakpoints.down('xs')]: {
            width: '92% !important'
        }
    },
    contentTxt: {
        fontWeight : 'bold'
    }
}));


export default useStyles;
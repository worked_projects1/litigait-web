/*
 * ViewCasesDetailPage Messages
 *
 * This contains all the text for the ViewCasesDetailPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewCasesDetailPage';

export default defineMessages({
    notFound: {
        id: `${scope}.notFound`,
        defaultMessage: 'No Questions Found.',
    },
    sendQuestions: {
        id: `${scope}.sendQuestions`,
        defaultMessage: 'Send Questions',
    },
    sendVerification: {
        id: `${scope}.sendVerification`,
        defaultMessage: 'TekSign',
    },
    saveAllAsFinal: {
        id: `${scope}.saveAllAsFinal`,
        defaultMessage: 'Save all as final'
    },
    uploadDiscovery: {
        id: `${scope}.UploadDiscovery`,
        defaultMessage: 'Upload Discovery'
    },
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    },
    emailDocument: {
        id: `${scope}.emailDocument`,
        defaultMessage: 'Email Document',
    }
});


export const customMessage = {
    clientResponse: `This flag helps you identify if client response to a question is modified after attorney response was drafted.
        You can clear this flag once you review and incorporate modified inputs from the client.
        <br/> <br/>
        Do you want to clear the 'Edited' flag for this question?`,
    modifiedLawyerResponse: '** See Client Response with modifications by client **',
    notModifiedLawyerResponse: '** See Client Response - Client did not make any modifications **',
    subGroup: ({ record, question_number_text, question_number }) => {
        return record.subgroup ? `Enabling this will split responses to subquestions for ${question_number_text || question_number}. Do you want to continue?` : `Do you want to make this a single response for question ${question_number_text || question_number}? Selecting this will clear all Attorney responses for other subquestions.`
    },
    subGroupSplit: ({ question_number_text, question_number }) => {
        return `Enabling this will split responses to subquestions for ${question_number_text || question_number}. Do you want to continue?`
    },
    deleteObjection: `Do you want to delete these common objections from all sub-questions? You can still add objections to select sub-questions.`
}

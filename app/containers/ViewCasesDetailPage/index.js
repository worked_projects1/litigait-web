/**
 * 
 * View Cases Detail Page
 * 
 */

import React, { memo, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Styles, { footerStyle } from './styles';
import PropTypes from 'prop-types';
import SVG from 'react-inlinesvg';
import ResponseBox from './components/ResponseBox';
import ResponseForm from './components/ResponseForm';
import Spinner from 'components/Spinner';
import Snackbar from 'components/Snackbar';
import { Grid, Button, Hidden, FormControlLabel, Checkbox, Tabs, Tab, Paper, Typography, TextField, InputAdornment, IconButton, Popover } from '@material-ui/core';
import ModalDialog from 'components/ModalDialog';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import ButtonSpinner from 'components/ButtonSpinner';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import ModalForm from 'components/ModalRecordForm';
import AlertDialog from 'components/AlertDialog';
import Stepper from 'components/Stepper';
import { getStepper } from 'containers/ViewCasesPage/utils';
import { selectUser } from 'blocks/session/selectors';
import { getOffset, useDidUpdate, isPropound, planAlertMessage, stateListPOS, mycaseSessionValidation } from 'utils/tools';
import { Link } from 'react-router-dom';
import RenderPagination from 'components/RenderPagination';
import { reset } from 'redux-form';
import { selectForm } from 'blocks/session/selectors';
import EditIcon from '@material-ui/icons/Edit';
import POS from './components/POS';
import Icons from 'components/Icons';
import lodash from 'lodash';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import { consultationQuestions, handleSearch, diffBetweenDates, attorneySignatureList, addInitialDiscQuestions } from './utils';
import ClipLoader from 'react-spinners/ClipLoader';
import moment from 'moment';
import UploadDiscoveryButton from './components/UploadDiscoveryButton'
import DocumentButton from './components/DocumentButton';
import CalendarForm from './components/CalendarForm';
import DocumentEditor from 'components/DocumentEditor';
import ObjectionBox from './components/ObjectionBox';
import { download as downloadDocument } from 'utils/tools';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 */
export default function (name, path, columns, actions, selectors, filterColumns, customNote) {

    const { legalColumns, uploadColumns, standardFormColumns, partiesColumns, POSColumns, modifiedCustomTemplateOptions, filevineForm, subscriptionDetailsColumns, pricingNotificationStatus, respondEmailDocumentColumns, selectQuestionTemplateColumns, userRecordFormColumns, upShellColumns, objectionColumns, customLegalColumns } = columns;

    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectBilling, selectKey, selectRecordsMetaData, selectSessionExpand, selectRecords, selectSettingsDiscount, selectSubscriptionSuccess, selectDocumentStatus, selectEServePopup, selectFrogsQuestions, selectDynamicFrogsQuestions, selectDiscoveryHistory } = selectors;

    /**
     * @param {object} props 
     */
    function ViewCasesDetailPage(props) {

        const { record: legalFormRecord = {}, match, dispatch, location, error, success, metaData, loading, billing, active, user = {}, expand, records = [], forms = {}, discountDetails = [], subscriptionSuccess, documentStatus, eServePopup, frogsQuestions, dynamicFrogsQuestions, discoveryHistory } = props;
        const classes = Styles();
        const [sendAllQuestionsPopUp, setSendAllQuestionsPopUp] = useState(true);
        const [showResponseBox, setShowResponseBox] = useState(true);
        const [selectedQuestions, setselectedQuestions] = useState("");
        const [childRef, setChildRef] = useState(false);
        const { state = {} } = location || {};
        const { client_response_status_filter = 'All', lawyer_response_status_filter = 'All' } = state;
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const lg = useMediaQuery(theme.breakpoints.up('lg'));
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const [progress, setProgress] = useState(false);
        const [document, setDocument] = React.useState();
        const [filter, setFilter] = React.useState({ client_response_status_filter, lawyer_response_status_filter });
        const elements = useElements();
        const stripe = useStripe();
        const [width, setWidth] = useState(window.innerWidth);
        const [position, setPosition] = useState(false);
        const [margin, setMargin] = useState(0);
        const [languageType, setLanguageType] = useState('en');
        const { attorneys } = legalFormRecord;
        const navbarWidth = expand ? 240 : 60;
        const [partyIds, setPartyIds] = useState(false);
        const [showSendQuestions, setSendQuestions] = useState(false);
        const [legalFormId, setLegalFormId] = React.useState(false);
        const [currentPage, setCurrentPage] = useState(1);
        const pageSize = 25;
        const [pageLoader, setPageLoader] = useState(false);
        const [closeEditor, setCloseEditor] = useState(false);
        const [formLoader, setFormLoader] = useState(false);
        const { name, practiceDetails = {}, plan_type, email, tekasyougo_practices } = user;
        const planType = plan_type && typeof plan_type === 'object' && plan_type['responding'] || false;
        const defaultMessage = `Your Lawyers at ${practiceDetails.name} are requesting your responses.`;
        const [customMessage, setCustomMessage] = useState(defaultMessage);
        const [editMessage, setEditMessage] = useState(false);
        const customRef = useRef(null);
        const [loaderType, setLoaderType] = useState(false);
        const [searchInput, setSearchInput] = useState(false);
        const [anchorEl, setAnchorEl] = useState(null);
        const [btnLoader, setBtnLoader] = useState(false);
        const [shareLawyerResponse, setShareLawyerResponse] = useState(false);
        const [showFileVineForm, setShowFileVineForm] = useState(false);
        const [showFilevinePopup, setShowFilevinePopup] = useState(false);
        const [showMyCasePopup, setShowMyCasePopup] = useState(false);
        const [modifiedTemplateSignature, setModifiedTemplateSignatureRecord] = useState(false);
        const [errorMessage, setErrorMessage] = useState(false);
        const [showAlertDialog, setShowAlertDialog] = useState(false);
        const [loader, setLoader] = useState(false);
        const [reminderMeanTime, setReminderMeanTime] = useState(7);
        const [reminderError, setReminderError] = useState(false);
        const totalMeanTime = reminderMeanTime && parseInt(reminderMeanTime) + 60; //60days
        const endDate = totalMeanTime && moment().add(totalMeanTime, 'days').format('MM/DD/YYYY');
        const [reminderTillDate, setReminderTillDate] = useState(endDate);
        const [showLitifyPopup, setShowLitifyPopup] = useState(false);
        const [showClioPopup, setShowClioPopup] = useState(false);
        const [openDocEditor, setOpenDocEditor] = useState(false);
        const [docLoader, setDocLoader] = useState(false);
        const [emailPopup, setEmailPopup] = useState(false);
        const [openNext, setOpenNext] = useState(false);
        const finalDocumentRef = useRef(null);
        const posDocumentRef = useRef(null);
        const customObject = Object.assign({}, { finalDocumentRef, posDocumentRef, openNext, setOpenNext })
        const [modalBtnLoader, setModalBtnLoader] = useState(false);
        const [docGenModal, setDocGenModal] = useState(false);
        const [discData, setDiscData] = useState(false);

        const minDate = reminderMeanTime && moment().add(reminderMeanTime, 'days').format('MM/DD/YYYY') || moment();
        const maxDate = totalMeanTime && moment().add(totalMeanTime, 'days').format('MM/DD/YYYY') || false;
        const initialDateRecord = reminderTillDate && Object.assign({}, { reminder_till_date: moment(reminderTillDate).format('MM/DD/YYYY') }) || {};

        const record = legalFormRecord && searchInput && searchInput !== '' && handleSearch(searchInput, legalFormRecord) || legalFormRecord;

        const questions = record && record.questions && Array.isArray(record.questions) && record.questions.length > 0 && record.questions || [];

        const questionsType = questions && questions.length > 0 && Object.keys(lodash.groupBy(questions, 'question_type')).filter(e => e !== 'null') || [];

        const selectedRespondForms = match && match.params && match.params.type && record && record[match.params.type] && record[match.params.type].length > 0 ? record[match.params.type] : [];

        const selectedForm = legalFormId && match && match.params && match.params.type && record && record[match.params.type] && record[match.params.type].length > 0 ? record[match.params.type].find(_ => _.legalform_id.toString() === legalFormId.toString()) : record && state && state.legalforms_id && match && match.params && match.params.type && record[match.params.type] && record[match.params.type].length > 0 ? record[match.params.type].find(_ => _.legalform_id.toString() === state.legalforms_id.toString()) : record && match && match.params && match.params.type && record[match.params.type] ? record[match.params.type][0] : {};

        const selectedTab = legalFormId && match && match.params && match.params.type && record && record[match.params.type] && record[match.params.type].length > 0 ? record[match.params.type].findIndex(_ => _.legalform_id.toString() === legalFormId.toString()) : record && state && state.legalforms_id && match && match.params && match.params.type && record[match.params.type] && record[match.params.type].length > 0 ? record[match.params.type].findIndex(_ => _.legalform_id.toString() === state.legalforms_id.toString()) : 0;

        const resendQuestions = match && match.params && match.params.type && record && record[match.params.type] && record[match.params.type].length > 0 && record[match.params.type][selectedTab] && record[match.params.type][selectedTab]['QuestionsRespondedByClientLastSetofCount'] !== record[match.params.type][selectedTab]['QuestionsSentToClientLastSetofCount'];

        const servingDetails = match && match.params && match.params.type && record && record[match.params.type] && record[match.params.type].length > 0 && record[match.params.type][selectedTab] && record[match.params.type][selectedTab] || {};

        const lawyers = attorneys && typeof attorneys === 'string' && attorneys.split(',').reduce((a, el) => {
            const opt = metaData && metaData['users'] && metaData['users'].find(_ => _.id.toString() === el);
            if (opt)
                a.push(opt)

            return a;
        }, []) || [];

        const characterSize = legalColumns.filter(_ => _.charLimit)[0].charLimit;

        const isPropoundPractice = isPropound(user, record);

        const posInitialValue = record && user && practiceDetails && servingDetails && Object.keys(record).length > 0 && Object.keys(user).length > 0 && Object.keys(practiceDetails).length > 0 && Object.keys(servingDetails).length > 0 && Object.assign({}, record, { legalforms_id: servingDetails.legalform_id, document_type: match && match.params && match.params.type.toUpperCase(), serving_attorney_name: servingDetails.serving_attorney_name ? servingDetails.serving_attorney_name : practiceDetails.name, serving_attorney_street: servingDetails.serving_attorney_street ? servingDetails.serving_attorney_street : practiceDetails.street, serving_attorney_city: servingDetails.serving_attorney_city ? servingDetails.serving_attorney_city : practiceDetails.city, serving_attorney_state: servingDetails.serving_attorney_state ? servingDetails.serving_attorney_state : practiceDetails.state, serving_attorney_zip_code: servingDetails.serving_attorney_zip_code ? servingDetails.serving_attorney_zip_code : practiceDetails.zip_code, serving_attorney_email: servingDetails.serving_attorney_email ? servingDetails.serving_attorney_email : user.email, serving_date: servingDetails.serving_date || null }) || {};

        const modified_template_s3_file_key_doc_type = `modified_template_s3_file_key_${match.params.type}`;

        const customModifiedTemplate = metaData && metaData['modifiedTemplateOptions'] && metaData['modifiedTemplateOptions'].length > 0 && metaData['modifiedTemplateOptions'].reduce((a, el) => {
            const option = record && (el.state == record.state || el.state == "") && el[modified_template_s3_file_key_doc_type];

            if (option) {
                a.push(Object.assign({}, { label: el.file_name, value: el.id, [modified_template_s3_file_key_doc_type]: el[modified_template_s3_file_key_doc_type], status: el.status, state: el.state }))
            }
            return a;
        }, []) || [];

        const customTemplate = customModifiedTemplate && customModifiedTemplate.length == 1 && customModifiedTemplate && customModifiedTemplate[0] || false;

        const validSubscription = planType && !['tek_as_you_go', 'pay_as_you_go', 'free_trial'].includes(planType);

        const { myCaseCredentials, mycaseSessionDiff } = user && mycaseSessionValidation(user);

        const docType = ['idc', 'frogs', 'sprogs', 'rfpd', 'rfa'];

        let upShellFeatureColumns = upShellColumns && user && typeof upShellColumns === 'function' ? upShellColumns(user) : upShellColumns;

        const respondingFormDetails = docType
            .map((el) => {
                if (el) {
                    return record && Object.keys(record).length > 0 && record[el] && record[el].length > 0 && record[el].map(elm => elm && elm.finalDocument && elm.posDocument && Object.assign({}, elm, { id: elm.legalform_id, document_type: el })).filter(x => x);
                }
            }).reduce((a, el) => {
                if (el && Array.isArray(el) && el.length > 0) {
                    const finalRecord = el.map((res, index) => {
                        if(res){
                            return Object.assign({}, { ...res }, { orderId: index + 1 })
                        }
                    })
                    a.push(...finalRecord);
                }
                return a;
            }, []);

        const respondSendEmailColumns = respondEmailDocumentColumns && respondEmailDocumentColumns(respondingFormDetails, record, actions, dispatch, documentStatus).columns; 

        const isUploadDoc = record && record.case_from && ['filevine', 'mycase'].includes(record.case_from) && selectedForm && selectedForm.discovery_s3_file_key || false;

        const descriptionMessage = planAlertMessage(planType, user?.subscription_details);

        const freeTrialMessage = `Your free trial has no documents remaining.<br /><br />Join 1000+ legal professionals who automate their discovery using EsquireTek, avoiding time spent on tedious manual tasks.<br /><br />Sign up for a subscription today and receive 10% off with code TIMEISMONEY.<br /><br />This offer expires in 24 hours.<br /><br />`;

        const defaultLimit = 7;
        
        const defaultEndDate = defaultLimit && moment().add(defaultLimit + 60, 'days').format('MM/DD/YYYY');

        const responseHistoryLimit = user?.response_history_limit && parseInt(user?.response_history_limit) || 100;

        const { attorneySignature } = metaData && record && attorneySignatureList({ metaData, record });

        useEffect(() => {
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            if (state && state.legalforms_id) {
                dispatch(actions.loadForm(Object.assign({}, {
                    id: match.params.id,
                    document_type: match.params.type,
                    lawyer_response_status_filter,
                    client_response_status_filter,
                    legalforms_id: state && state.legalforms_id
                })));
            }
            dispatch(actions.loadFrogsQuestions(Object.assign({}, { case_id: false }))); // clear the frogs questions in store.
            // const autoInterval = setInterval(() => {
            //     dispatch(actions.autoSaveForm('detailForm_'));
            // }, 30000);
            window.addEventListener('resize', () => {
                setMargin((getOffset('esquiretek-cases-header') + getOffset('esquiretek-detail-header') - 120));
                setPosition((getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')) - 2);
                setWidth(window.innerWidth);
            });
            // return () => clearInterval(autoInterval);
        }, [match.params.id]);

        useDidUpdate(() => {
            setMargin((getOffset('esquiretek-cases-header') + getOffset('esquiretek-detail-header') - 120));
            setPosition((getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')) - 2);
            setWidth(window.innerWidth);
        }, [expand]);

        useDidUpdate(() => {
            if (records && records.length > 0 && record && !record.frogs && !record.sprogs) {
                dispatch(actions.loadRecord(match.params.id));
            }

            if(records && records?.length > 0 && record && match.params.type && match.params.type == 'rfa' && !frogsQuestions && record && record?.state === 'CA') {
                dispatch(actions.loadFrogsQuestions(Object.assign({}, { case_id: match.params.id })));
            }

            if (records && records.length > 0 && record && (record.frogs || record.sprogs) && !record.questions) {
                dispatch(actions.loadForm(Object.assign({}, {
                    id: match.params.id,
                    document_type: match.params.type,
                    lawyer_response_status_filter,
                    client_response_status_filter,
                    legalforms_id: (state && state.legalforms_id) || (record && match && match.params && match.params.type && record[match.params.type] && record[match.params.type][0]['legalform_id'])
                })));
            }
        }, [records, record, actions, dispatch, match])

        const handleFilter = (e) => {
            const { name, value } = e.target;
            setFilter(Object.assign({}, { ...filter }, { [name]: value }));
            setCurrentPage(1);
            setFormLoader(true);
            setSearchInput(false)
            dispatch(actions.loadForm(Object.assign({}, {
                id: match.params.id,
                client_id: record.client_id,
                document_type: match.params.type,
                lawyer_response_status_filter,
                client_response_status_filter,
                legalforms_id: selectedForm.legalform_id
            }, Object.assign({}, { ...filter }, { [name]: value })), setFormLoader))
        }

        const handleReload = () => {
            setCurrentPage(1);
            setFormLoader(true);
            dispatch(actions.loadForm(Object.assign({}, {
                id: match.params.id,
                document_type: match.params.type,
                lawyer_response_status_filter,
                client_response_status_filter,
                legalforms_id: selectedForm.legalform_id,
                ...filter
            }), setFormLoader));
        }

        const handleTab = (e, tabIndex) => {
            if (record && record[match.params.type] && record[match.params.type].length > 0 && record[match.params.type][tabIndex]) {
                setCurrentPage(1);
                setFormLoader(true);
                setLegalFormId(record[match.params.type][tabIndex]['legalform_id']);
                dispatch(actions.loadForm(Object.assign({}, {
                    id: match.params.id,
                    document_type: match.params.type,
                    lawyer_response_status_filter,
                    client_response_status_filter,
                    legalforms_id: record[match.params.type][tabIndex]['legalform_id'],
                    client_id: record.client_id,
                    ...filter
                }), setFormLoader));
                setSearchInput(false)
            }
        }

        const totalUploadedQuestions = record && record.questions && record.questions.length;

        const translateCustomMessage = () => {
            const message = (customMessage === defaultMessage && languageType === 'es') ? `Sus abogados en ${practiceDetails.name} solicitan sus respuestas.` : (customMessage === defaultMessage && languageType === 'vi') ? `Các Luật sư của bạn tại ${practiceDetails.name} đang yêu cầu phản hồi của bạn.` : customMessage
            return message;
        }

        const handleSendAllQuestion = () => {
            setSendQuestions(false);
            dispatch(actions.sendQuestions(Object.assign({}, { case_id: record.id, client_id: (partyIds && Array.isArray(partyIds) && partyIds.length > 0 && partyIds.includes(record.client_id)) || !partyIds ? record.client_id : false, document_type: match.params.type, sending_type: 'all', TargetLanguageCode: languageType, party_ids: partyIds && Array.isArray(partyIds) && partyIds.length > 0 && partyIds.filter(c => c !== record.client_id) || [], legalforms_id: selectedForm.legalform_id, custom_message: customMessage && translateCustomMessage() || defaultMessage, reference_id: filter && filter.party_id || record.client_id, share_attorney_response: shareLawyerResponse, reminder_till_date: reminderTillDate && moment(reminderTillDate).format('YYYY-MM-DD 23:59:59') || false, reminder_meantime: reminderMeanTime || false })));
            setPartyIds(false);
            setEditMessage(false);
            setShareLawyerResponse(false);
            setReminderMeanTime(defaultLimit);
            setReminderTillDate(defaultEndDate);
        }

        const handleSendAll = () => {
            if (reminderMeanTime || reminderTillDate) {
                if (reminderMeanTime && reminderTillDate && !isNaN(Date.parse(reminderTillDate))) {
                    let end_date = moment(reminderTillDate).format("MM/DD/YYYY");
                    let start_date = moment().format("MM/DD/YYYY");
                    let diff = diffBetweenDates(start_date, end_date, 'days');
                    if (parseInt(reminderMeanTime) && diff >= parseInt(reminderMeanTime)) {
                        handleSendAllQuestion();
                    } else {
                        setErrorMessage('Please enter the valid reminder days and end date');
                    }
                }
                else {
                    setReminderError('Required');
                }
            } else {
                handleSendAllQuestion();
            }
        }

        const handleSendSelectedQuestions = () => {
            childRef.handleSubmit();
            // setSendQuestions(false);
            // setShowResponseBox(true);
            // setSendAllQuestionsPopUp(true);
            setEditMessage(false);
        }

        const handleSelectQuestions = () => {
            setSendQuestions(false);
            setShowResponseBox(false);
            setSendAllQuestionsPopUp(false);
        }

        const handleClearSelection = () => {
            setPartyIds(false);
            setShowResponseBox(true);
            setSendAllQuestionsPopUp(true);
            setselectedQuestions(false);
            setSendQuestions(false);
            setReminderError(false);
            setReminderMeanTime(defaultLimit);
            setReminderTillDate(defaultEndDate);
            dispatch(reset('selectResponseForm'));
        }

        const sendSelectedQuestions = (submitRecord) => {
            setSendAllQuestionsPopUp(false);
            dispatch(actions.sendQuestions(Object.assign({}, { case_id: submitRecord.id, client_id: (partyIds && Array.isArray(partyIds) && partyIds.length > 0 && partyIds.includes(submitRecord.client_id)) || !partyIds ? submitRecord.client_id : false, document_type: match.params.type, sending_type: 'selected_questions', ids: submitRecord.selectedQuestions, document_ids: submitRecord.document_ids, TargetLanguageCode: languageType, party_ids: partyIds && Array.isArray(partyIds) && partyIds.length > 0 && partyIds.filter(c => c !== submitRecord.client_id) || [], legalforms_id: selectedForm.legalform_id, custom_message: customMessage && translateCustomMessage() || defaultMessage, reference_id: filter && filter.party_id || record.client_id, share_attorney_response: shareLawyerResponse, reminder_till_date: reminderTillDate && moment(reminderTillDate).format('YYYY-MM-DD 23:59:59') || false, reminder_meantime: reminderMeanTime || false })));
            setPartyIds(false);
            setShareLawyerResponse(false);
            setSendQuestions(false);
            setShowResponseBox(true);
            setSendAllQuestionsPopUp(true);
            setReminderMeanTime(defaultLimit);
            setReminderTillDate(defaultEndDate);
        }
        
        const handleSubmit = (submitRecord, dispatch, { form }) => {
            if (submitRecord && submitRecord.selectedQuestions && submitRecord.selectedQuestions.length > 0) {
                if (reminderMeanTime || reminderTillDate) {
                    if (reminderMeanTime && reminderTillDate && !isNaN(Date.parse(reminderTillDate))) {
                        let end_date = moment(reminderTillDate).format("MM/DD/YYYY");
                        let start_date = moment().format("MM/DD/YYYY");
                        let diff = diffBetweenDates(start_date, end_date, 'days');
                        if (parseInt(reminderMeanTime) && diff >= parseInt(reminderMeanTime)) {
                            sendSelectedQuestions(submitRecord);
                        } else {
                            setErrorMessage('Please enter the valid reminder days and end date');
                        }
                    } else {
                        setReminderError('Required');
                    }
                } else {
                    sendSelectedQuestions(submitRecord);
                }
            }
        }

        const handleRecipients = (recipientsRecord, dispatch, { form }) => {
            if (recipientsRecord && recipientsRecord.party_ids) {
                setPartyIds(recipientsRecord.party_ids.split(','));
                setSendQuestions(true);
                setDocument(Object.assign({}, { type: 'SendQuestions' }));
            }
        }

        const getNoQuesSelectedOptions = [
            {
                action: handleSendAll,
                label: 'Yes, send all',
                disable: reminderError
            },
            {
                action: handleSelectQuestions,
                label: 'Select Questions to send'
            }
        ];

        const getQuesSelectedOptions = [
            {
                action: handleSendSelectedQuestions,
                label: 'Send selected questions',
                disable: reminderError
            },
            {
                action: handleClearSelection,
                label: 'Cancel and clear selection'
            }
        ];

        const sendAllQuestionTxt = () => {
            return <span>Do you want to send all <b>{totalUploadedQuestions}</b> questions to client?</span>;
        }

        const sendSelectedQuestionTxt = () => {
            return <span>Do you want to send <b>{selectedQuestions || 0}</b> questions to client?</span>;
        }

        const openDocument = ({ form, dialog, submitRecord, generatedType, posModal, freeTrialDialog, openTemplateForm, openGeorgiaTemplate, subscriptionAlert, openRecordForm }) => {
            setModifiedTemplateSignatureRecord(false);
            setLoaderType(generatedType);
            setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
            const submitRecords = modifiedTempRecord(submitRecord);
            const signature = signatureFunc(submitRecord, generatedType);
            submitRecords.signature = signature;
            setModifiedTemplateSignatureRecord(submitRecords);
            dispatch(actions.loadBillingDetails(form, dialog, setProgress, Object.assign({}, submitRecords, { legalforms_id: selectedForm.legalform_id }), generatedType, false, posModal, freeTrialDialog, signature, setShowFilevinePopup, setShowMyCasePopup, subscriptionAlert, openTemplateForm, openGeorgiaTemplate, openRecordForm, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
        }

        const openExistingDiscovery = (setModal, value) => {
            setOpenDocEditor(true);
            setDocGenModal(false);
        }

        const createNewDiscovery = () => {
            setDocGenModal(false);
            openDocument(discData);
        }


        const editActions = [
            {
                value: 'add_new',
                action: createNewDiscovery,
                label: 'Create New Discovery'
            },
            {
                value: 'edit_exist',
                action: openExistingDiscovery,
                label: 'Edit Existing Discovery'
            }
        ];

        const handleCreateDiscovery = (discoveryData) => {
            const documentEditedAccessIds = user && user.show_document_editor_ids && typeof user.show_document_editor_ids === 'string' && user.show_document_editor_ids.split(',') || [];
              const documentEditedAccess = user && user.practiceDetails && documentEditedAccessIds && documentEditedAccessIds.includes(user.practiceDetails.id) || false;
            if(documentEditedAccess) {
                setDiscData(discoveryData);
                const { generatedType } = discoveryData;
                setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
                setLoaderType(generatedType);         
                const { legalform_id } = selectedForm;
                dispatch(actions.fetchDiscoveryHistory(legalform_id, setDocGenModal, discoveryData, openDocument));
            } else {
                openDocument(discoveryData);
            }
        }

        const signatureFunc = (submitRecord, generatedType) => {
            const signature = Object.assign({}, { lawyer_signature: null, pos_signature: null });
            if(submitRecord?.signature) {
                if(user?.role && user?.role === 'paralegal') {
                    if(generatedType && generatedType === 'pos') {
                        signature.pos_signature = submitRecord?.signature || null;
                    } else {
                        if(generatedType && generatedType != 'template' &&attorneySignature && attorneySignature?.length === 1) {
                            signature.lawyer_signature = attorneySignature[0] && attorneySignature[0]?.value || null;
                        } else {
                            signature.lawyer_signature = submitRecord?.attorney_signature || null;
                        }
                        signature.lawyer_signature = submitRecord?.signature || null;
                    }
                } else {
                    signature.lawyer_signature = submitRecord?.signature || null;
                }
            } else {
                if(user?.role && user?.role === 'paralegal') {
                    if(generatedType && generatedType != 'template' && attorneySignature && attorneySignature?.length === 1) {
                        signature.lawyer_signature = attorneySignature[0] && attorneySignature[0]?.value || null;
                    } 
                }
            }
            return signature;
        }

        const handleBilling = async (submitRecord, generatedType, posModal, data, dispatch, { form }) => {
            const result = await stripe.confirmCardSetup(billing.client_secret, {
                payment_method: {
                    card: elements.getElement(CardElement),
                    billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                }
            });
            const previousRecord = Object.assign({} , { ...modifiedTemplateSignature });
            const signature = signatureFunc(previousRecord, generatedType);
            
            previousRecord.signature = signature;
            setModifiedTemplateSignatureRecord(false);
            dispatch(actions.updateBillingDetails(result, Object.assign({}, submitRecord, { ...previousRecord, legalforms_id: selectedForm.legalform_id }), setProgress, generatedType, form, posModal, setShowFilevinePopup, setShowMyCasePopup, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
            
        }

        const generateDocument = (submitRecord = {}, generatedType) => {
            setProgress(generatedType);
            const previousRecord = Object.assign({} , { ...modifiedTemplateSignature });
            const signature = signatureFunc(previousRecord, generatedType);
            
            dispatch(actions.fetchForm(Object.assign({}, submitRecord, { ...modifiedTemplateSignature, legalforms_id: selectedForm.legalform_id, discovery_s3_file_key: selectedForm.discovery_s3_file_key }), generatedType, setShowFilevinePopup, setShowMyCasePopup, setProgress, false, signature, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
            setModifiedTemplateSignatureRecord(false);
        }

        const tekAsYouGoFunc = () => {
            const practicesTekAsYouGo = user && tekasyougo_practices && typeof tekasyougo_practices === 'string' && tekasyougo_practices.split(',') || [];
            const validUser = user && practiceDetails && practicesTekAsYouGo && !practicesTekAsYouGo.includes(practiceDetails.id) && planType && ['tek_as_you_go', 'pay_as_you_go'].includes(planType) || false;
            return validUser;
        }

        const handleQuestions = (open) => {
            if ((planType && planType == 'free_trial')) {
                setLoader(true);
                dispatch(actions.inValidSubscriptions(true, setShowAlertDialog, metaData, partyIds, open, setSendQuestions, setDocument, setLoader));
            } else if (planType && tekAsYouGoFunc() || !planType) {
                setShowAlertDialog(true);
            } else {
                if (metaData && metaData['parties'] && metaData['parties'].length > 0 && !partyIds) {
                    open();
                } else {
                    setSendQuestions(true);
                    setDocument(Object.assign({}, { type: 'SendQuestions' }));
                }
            }
        }

        const handleTranslate = (translateData, setSpinner) => {
            dispatch(actions.requestTranslation(Object.assign({}, { ...translateData }, { case_id: record.id }), setSpinner));
        }

        const handleMergeTranslate = (translateData) => {
            const clientTranslation = translateData && translateData.translation && translateData.translation.split('\n');

            if (clientTranslation && clientTranslation.length > 0) {
                const clientTranslationText = clientTranslation.map(el => {
                    if (el == '') {
                        return `<p>&nbsp;</p>`
                    } else {
                        return `<p>${el}</p>`
                    }
                }).join("");
                dispatch(actions.mergeTranslation(Object.assign({}, { ...translateData }, { case_id: record.id, translation: translateData.lawyer_response_text ? `<br/>${clientTranslationText}` : `${clientTranslationText}` }), characterSize));
            } else {
                dispatch(actions.mergeTranslation(Object.assign({}, { ...translateData }, { case_id: record.id, translation: translateData.lawyer_response_text ? `<br/><p>${translateData.translation}</p>` : `<p>${translateData.translation}</p>` }), characterSize));
            }
        }

        const handleCopyToLawyerResponse = (copyData, openForm, detailFormValue) => {
            const clientResponse = copyData.client_response_text.split('\n');
            const lawyerText = openForm ? detailFormValue.lawyer_response_text : copyData.lawyer_response_text;
            if (clientResponse && clientResponse.length > 0) {
                const clientText = clientResponse.map(el => {
                    if (el == '') {
                        return `<p>&nbsp;</p>`
                    } else {
                        return `<p>${el}</p>`
                    }
                }).join("");
                dispatch(actions.copyToLawyerResponse(Object.assign({}, copyData, { client_response_text: lawyerText ? `<br/>${clientText}` : `${clientText}` }), openForm, characterSize));
            } else {
                dispatch(actions.copyToLawyerResponse(Object.assign({}, copyData, { client_response_text: lawyerText ? `<br/><p>${copyData.client_response_text}</p>` : `<p>${copyData.client_response_text}</p>` }), openForm, characterSize));
            }
        }

        const handleEditedFlag = (data) => {
            dispatch(actions.updateEditedFlag(data));
        }

        const DiscType = questionsType && questionsType.length > 0 ? questionsType[0] : standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
            if (!q && record && record.questions && record.questions.length > 0) {
                const arr = record.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
                if (arr)
                    q = nc;
            }
            return q;
        }, false);

        const handleResendQuestion = () => {
            setSendQuestions(false);
            dispatch(actions.resendQuestions(Object.assign({}, { case_id: record.id, client_id: (partyIds && Array.isArray(partyIds) && partyIds.length > 0 && partyIds.includes(record.client_id)) || !partyIds ? record.client_id : false, document_type: match.params.type, legalforms_id: selectedForm.legalform_id })));
            setPartyIds(false);
        }

        const handlePagination = (page, index) => {
            setCurrentPage(page);
            setPageLoader(true);
            setTimeout(() => setPageLoader(false), 2000);
        }

        const handleSaveAll = () => {
            setCloseEditor(true);
            setTimeout(() => dispatch(actions.saveAllForm('detailForm_', Object.assign({}, { case_id: record.id, document_type: match.params.type, legalforms_id: selectedForm.legalform_id, questions: record.questions }), (status) => setTimeout(() => setCloseEditor(status), 3000))), 2000);
        }

        const handleSubGroup = (r, setOpenForm) => {
            dispatch(actions.saveSubGroupForm(Object.assign({}, r, { legalforms_id: selectedForm.legalform_id }), setOpenForm));
        }

        const handleChangeMessage = (e) => {
            const { name, value } = e.target;
            setCustomMessage(value);
        }

        const handleClick = (event) => {
            setAnchorEl(event.currentTarget);
        };

        const handleClose = () => {
            setAnchorEl(null);
        };

        const handleCopyAllToLawyerResponse = () => {
            setCloseEditor(true);
            setTimeout(() => dispatch(actions.copyAllToLawyerResponse(Object.assign({}, { case_id: record.id, document_type: match.params.type, legalforms_id: selectedForm.legalform_id, questions: record.questions }), (status) => setTimeout(() => setCloseEditor(status), 3000))), 2000)
        }

        const handleAddNewQuestion = (data, type) => {
            const submitRecord = data && data[0];
            if (type && type == 'add') {
                const questionNum = data && data.length && data[0].question_number || false;
                setBtnLoader(questionNum);
                let initialDisclosureQuestions = data && addInitialDiscQuestions(data);
                dispatch(actions.addConsultation(Object.assign({}, {
                    id: record.id,
                    case_id: record.id,
                    practice_id: record.practice_id,
                    client_id: record.client_id,
                    legalforms_id: selectedForm.legalform_id,
                    questions: initialDisclosureQuestions
                }), setBtnLoader));
            }
            else if (type && type === 'delete') {
                setFormLoader(true);
                dispatch(actions.deleteConsultation(Object.assign({}, {
                    id: record.id,
                    case_id: record.id,
                    practice_id: record.practice_id,
                    client_id: record.client_id,
                    legalforms_id: selectedForm.legalform_id,
                    party_id: filter && filter.party_id || false,
                    document_type: submitRecord.document_type,
                    question_ids: data && data.map(el => el.id)
                }), setFormLoader));
            }
        }

        const handleConsultation = (data, type) => {
            const submitRecord = data && data[0];
            if (type && type == 'add') {
                setBtnLoader(true);
                let consulationQuestion = data && consultationQuestions(data)
                dispatch(actions.addConsultation(Object.assign({}, {
                    id: record.id,
                    case_id: record.id,
                    practice_id: record.practice_id,
                    client_id: record.client_id,
                    legalforms_id: selectedForm.legalform_id,
                    questions: consulationQuestion
                }), setBtnLoader));
            }
            else if (type && type === 'delete') {
                setFormLoader(true);
                dispatch(actions.deleteConsultation(Object.assign({}, {
                    id: record.id,
                    case_id: record.id,
                    practice_id: record.practice_id,
                    client_id: record.client_id,
                    legalforms_id: selectedForm.legalform_id,
                    party_id: filter && filter.party_id || false,
                    document_type: submitRecord.document_type,
                    question_ids: data && data.map(el => el.id)
                }), setFormLoader));
            }

        }

        const handleMyCaseUploadDocument = () => {
            setDocument(Object.assign({}, { value: 'mycase' }));
            setLoaderType(false);
            dispatch(actions.uploadMyCaseDocument(Object.assign({}, { ...record, ...selectedForm }, { document_type: match.params.type, myCaseDetails: mycaseSessionDiff })));
        }

        const handleFilevineUploadDocument = () => {
            setDocument(Object.assign({}, { value: 'filevine' }));
            setLoaderType(false);
            const submitRecord = Object.assign({}, { integration_case_id: record.integration_case_id, document_type: match.params.type, s3_file_key: selectedForm.discovery_s3_file_key });
            dispatch(actions.verifyFilevineUploadDocumentSession(submitRecord, setShowFileVineForm, setDocument));
        }

        const handleFilevineSession = (data, dispatch, { form }) => {
            setDocument(Object.assign({}, { value: 'filevine' }));
            const submitRecord = Object.assign({}, { ...data }, { uploadDocumentDetails: { integration_case_id: record.integration_case_id, document_type: match.params.type, s3_file_key: selectedForm.discovery_s3_file_key } });
            dispatch(actions.createNewFilevineSession(submitRecord, form));
        }

        const handleUpdatePlanDetails = async({ openCardDetailForm, dialog, submitRecord, generatedType, posModal, freeTrialDialog, openTemplateForm, openGeorgiaTemplate }, data, dispatch, { form }) => {
            let result;
            if(billing && billing['credit_card_details_available']){
                result = Object.assign({}, billing)
            } else {
                result = await stripe.confirmCardSetup(billing.client_secret, {
                    payment_method: {
                        card: elements.getElement(CardElement),
                        billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                    }
                });
            }

            const { respondingPlansOptions: plans = [] } = metaData;

            const selectedPlan = data && data['plan_id'] && plans && Array.isArray(plans) && plans.length > 0 && plans.find(_ => _.plan_id === data['plan_id']) || false;

            dispatch(actions.updatePlanDetails(result, Object.assign({}, { ...data }, { plan_category: selectedPlan.plan_category }), form, Object.assign({}, submitRecord, { legalforms_id: selectedForm.legalform_id }), openCardDetailForm, dialog, setProgress, generatedType, posModal, freeTrialDialog, setShowFilevinePopup, setShowMyCasePopup, openTemplateForm, openGeorgiaTemplate, actions, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
            setModifiedTemplateSignatureRecord(false); 
        }

        const handleRespondSendEmail = (generatedType, data, dispatch, { form }) => {
            setLoaderType(generatedType);
            setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
            const submitRecord = Object.assign({}, data, { case_id: record.id, opposing_counsel: record.opposing_counsel, attach_document_id: selectedForm && selectedForm.legalform_id, case_from: record?.case_from });

            submitRecord.selected_forms = respondingFormDetails && respondingFormDetails.length > 0 && respondingFormDetails.reduce((a, el) => {
                const opt = data && data.selected_forms.some((elm) => elm.id == el.id);
                if (opt)
                    a.push({ id: el.id, orderId: el.orderId, document_type: el.document_type })
                return a;
            }, []) || data.selected_forms;
            dispatch(actions.sendRespondQuestions(submitRecord, form, setDocument, setEmailPopup, setShowFilevinePopup, setShowMyCasePopup, setShowLitifyPopup, setShowClioPopup));
        }

        const modifiedTempRecord = (submitRecord) => {
            let finalRecord;
            if (customTemplate && Object.keys(customTemplate).length > 0) {
                finalRecord = Object.assign({}, submitRecord, { [modified_template_s3_file_key_doc_type]: customTemplate[modified_template_s3_file_key_doc_type], modified_template_status: customTemplate.status, custom_template_id: customTemplate?.value });
            } else {
                finalRecord = submitRecord;
            }
            return finalRecord
        }

        const modifiedTemplateRecords = (submitRecords, data) => {
            let record = submitRecords;
            if (data && data.custom_template) {
                const modified_template_value = customModifiedTemplate && customModifiedTemplate.length > 0 && customModifiedTemplate.reduce((a, el) => {
                    if (el && data && data.custom_template === el.value) {
                        const objValue = Object.assign({}, { modified_template_s3_file_key_value: el[modified_template_s3_file_key_doc_type], status: el.status, custom_template_id: el.value });
                        a.push(objValue);
                    }
                    return a;
                }, []);

                record[modified_template_s3_file_key_doc_type] = modified_template_value && modified_template_value[0] && modified_template_value[0].modified_template_s3_file_key_value;
                record.modified_template_status = modified_template_value && modified_template_value[0] && modified_template_value[0].status;
                record.custom_template_id = modified_template_value && modified_template_value[0] && modified_template_value[0].custom_template_id;
            } else {
                record = modifiedTempRecord(submitRecords);
            }
            return record;
        }

        const handleCustomTemplateSignature = ({ form, dialog, submitRecord, generatedType, freeTrialDialog, posModal }, data) => {
            setModifiedTemplateSignatureRecord(false);
            let submitRecords = Object.assign({}, { ...submitRecord });
            if (submitRecords) {
                submitRecords = modifiedTemplateRecords(submitRecords, data);
                
                submitRecords.doc_gen_type = data && data.selectTemplate;
                const signature = signatureFunc(data, generatedType);
                submitRecords.signature = signature;
                
                setLoaderType(generatedType);
                setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
                setModifiedTemplateSignatureRecord(submitRecords);
                dispatch(actions.loadBillingDetails(form, dialog, setProgress, Object.assign({}, submitRecords, { legalforms_id: selectedForm.legalform_id }), generatedType, false, posModal, freeTrialDialog, signature, setShowFilevinePopup, setShowMyCasePopup, false, false, false, false, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
            }
        }

        const handleModifiedTemplate = ({ form, dialog, submitRecord, generatedType, posModal, freeTrialDialog }, data) => {
            setModifiedTemplateSignatureRecord(false);
            let submitRecords = Object.assign({}, { ...submitRecord });
            if (submitRecords) {
                submitRecords = modifiedTemplateRecords(submitRecords, data);
                submitRecords.doc_gen_type = data && data.selectTemplate;
                const signature = signatureFunc(submitRecords, generatedType);
                submitRecords.signature = signature;
                setLoaderType(generatedType);
                setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
                setModifiedTemplateSignatureRecord(submitRecords);
                dispatch(actions.loadBillingDetails(form, dialog, setProgress, Object.assign({}, submitRecords, { legalforms_id: selectedForm.legalform_id }), generatedType, false, posModal, freeTrialDialog, false, setShowFilevinePopup, setShowMyCasePopup, false, false, false, false, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
            }
        }

        const handleEmailDocument = () => {
            if (record && selectedForm && selectedForm.discovery_s3_file_key && selectedForm.pdf_s3_file_key && selectedForm.posDocument) {
                setEmailPopup(true);
            } else if (selectedForm && !selectedForm.discovery_s3_file_key && !selectedForm.pdf_s3_file_key && !selectedForm.posDocument) {
                posDocumentRef.current.openDialog();
                setOpenNext({ openFinalDocument: true });
            } else if (selectedForm && !selectedForm.posDocument) {
                posDocumentRef.current.openDialog();
                setOpenNext({  openEmailPopup: true, setEmailPopup });
            } else if (selectedForm && !selectedForm.discovery_s3_file_key && !selectedForm.pdf_s3_file_key) {
                finalDocumentRef?.current?.openDocument();
            }
        }

        const openAlertDialog = ({ submitRecord, dialog, form, cardDetails, handleSubmitForm }) => {
            let submitRecords = Object.assign({}, { ...submitRecord, type: 'responding', practice_id: practiceDetails?.id });
            if(practiceDetails?.billing_type === 'limited_users_billing') {
                dispatch(actions.subscriptionLicenseValidation(Object.assign({}, submitRecords), form, dialog, cardDetails, handleSubmitForm));
            } else {
                dispatch(actions.settingsDiscountCode(Object.assign({}, submitRecords), form, dialog, cardDetails, handleSubmitForm));
            }
        }

        const handleUpdateUserRecord = (record, userRecord, formName) => {
            const { form, dialog, freeTrialDialog, generatedType, openGeorgiaTemplate, openTemplateForm, posModal, submitRecord, subscriptionAlert } = record;

            let submitRecords = Object.assign({}, { ...submitRecord });
            let formUserRecord;

            setLoaderType(generatedType);
            setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));

            submitRecords = Object.assign({}, submitRecord, { legalforms_id: selectedForm.legalform_id });            

            if (userRecord && Object.keys(userRecord).length > 0 && userRecord.user_name) {
                const attorneys_record = submitRecords && submitRecords.attorneys && typeof submitRecords.attorneys == 'string' && submitRecords.attorneys.length > 0 && submitRecords.attorneys.split(',') || false;
                let attorneysId = attorneys_record && Array.isArray(attorneys_record) && attorneys_record.length > 0 && !attorneys_record.includes(user.id) && attorneys_record.push(user.id) && attorneys_record || user.id;
                attorneysId = attorneysId && Array.isArray(attorneysId) && attorneysId.length > 0 && attorneysId.join(',') || attorneysId;
                formUserRecord = Object.assign({}, userRecord, { case_id: submitRecords.id, attorneys: attorneysId });
            } else {
                formUserRecord = Object.assign({}, userRecord, { case_id: submitRecords.id })
            }

            dispatch(actions.updateUserRecord(Object.assign({}, { form, dialog, setProgress, submitRecords, generatedType, posModal, freeTrialDialog, setShowFilevinePopup, setShowMyCasePopup, openTemplateForm, subscriptionAlert, openGeorgiaTemplate, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor }), formUserRecord, formName, customObject));
        }
       
        const handleReminderData = (evt) => {
            const { value } = evt.target;
            setReminderError(false);
            setReminderMeanTime(value);
            if (value > 30) {
                setReminderError('Reminder days should not be more than 30')
            }
        }

        const handleLitifyUploadDocument = () => {
            setDocument(Object.assign({}, { value: 'litify' }));
            setLoaderType(false);
            const submitRecord = Object.assign({}, { integration_case_id: record.integration_case_id, document_type: match.params.type, s3_file_key: selectedForm.discovery_s3_file_key, case_defendant_name: record.case_defendant_name, case_plaintiff_name: record.case_plaintiff_name });
            dispatch(actions.litifyUploadDocument(submitRecord));
        }

        const handleClioUploadDocument = () => {
            setDocument(Object.assign({}, { value: 'clio' }));
            setLoaderType(false);
            const submitRecord = Object.assign({}, { integration_case_id: record.integration_case_id, document_type: match.params.type, s3_file_key: selectedForm.discovery_s3_file_key });
            dispatch(actions.clioUploadDocument(submitRecord));
        }

        const handleDownloadDocxFile = async (editorData, version) => {
            const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;
            const submitRecord = Object.assign({}, selectedForm, { document_type: match.params.type, case_id: record.id });
            if (documentEdited || (version && version != 'Current Version')) {
                setDocLoader('download');
                const blob = await editorData.documentEditor.saveAsBlob('Docx');
                const exportedDocument = blob;
                const myFile = new File([exportedDocument], '', {
                    type: exportedDocument.type,
                });
                dispatch(actions.saveDocumentEditor(submitRecord, myFile, setDocLoader, false, false, record));
            } else {
                const finalDocument = selectedForm && selectedForm.finalDocument || false;
                if (finalDocument) {
                    downloadDocument(finalDocument);
                } 
            }
        }

        const handleSaveAndEmailDocxFile = async (editorData, version) => {
            const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;
            // if (selectedForm && !selectedForm.posDocument) {
            //     setErrorMessage('Document is saved. For E-Serve, the Proof of Service (PoS) has not been generated. Please generate it and use the Email Document feature.');
            // } else {
                if (documentEdited || (version && version != 'Current Version')) {
                    const submitRecord = Object.assign({}, selectedForm, { document_type: match.params.type, case_id: record.id  });
                    setDocLoader('save');
                    const blob = await editorData.documentEditor.saveAsBlob('Docx');
                    const exportedDocument = blob;
                    const myFile = new File([exportedDocument], '', {
                        type: exportedDocument.type,
                    });

                    dispatch(actions.saveDocumentEditor(submitRecord, myFile, setDocLoader, setOpenDocEditor, 'save', record, setEmailPopup, setShowFilevinePopup, setShowMyCasePopup, setShowLitifyPopup, setShowClioPopup, customObject));
                } else {
                    // const s3Key = selectedForm && selectedForm.discovery_s3_file_key || false;
                    // if (s3Key) {
                    //     const docFilename = s3Key.slice(s3Key.lastIndexOf('/') + 1, s3Key.length);
                    //     if (editorData?.documentEditor) {
                    //         editorData.documentEditor.save(docFilename, "Docx");
                    //     }
                    // }
                    const finalDocument = selectedForm && selectedForm.finalDocument || false;
                    if (finalDocument) {
                        downloadDocument(finalDocument);
                    } 
                    setOpenDocEditor(false);
                    if (selectedForm && !selectedForm.posDocument) {
                        posDocumentRef.current.openDialog();
                        setOpenNext({openEmailPopup: true, setEmailPopup})
                    } else { 
                        setEmailPopup(true);
                    }       
                }
            // }
        }
        
        const handleSaveDocxFile = async (editorData, version) => {
            const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;
            const submitRecord = Object.assign({}, selectedForm, { document_type: match.params.type, case_id: record.id });
            if (documentEdited || (version && version != 'Current Version')) {
                setDocLoader('save');
                const blob = await editorData.documentEditor.saveAsBlob('Docx');
                const exportedDocument = blob;
                const myFile = new File([exportedDocument], '', {
                    type: exportedDocument.type,
                });
                dispatch(actions.saveDocumentEditor(submitRecord, myFile, setDocLoader, false, false, record));
            } else {
                const finalDocument = selectedForm && selectedForm.finalDocument || false;
                if (finalDocument) {
                    downloadDocument(finalDocument);
                } 
            }
        }

        const handleIntegrationAlert = () => {
            if (record && record.case_from === 'filevine') {
                setShowFilevinePopup(true);
            } else if (record && record.case_from === 'mycase') {
                setShowMyCasePopup(true);
            } else if (record && record.case_from === 'litify') {
                setShowLitifyPopup(true);
            } else if (record && record.case_from === 'clio') {
                setShowClioPopup(true);
            }
        }

        const handleCloseDocEditor = () => {
            setOpenDocEditor(false);
            handleIntegrationAlert();
        }

        const handleCloseEmailModal = () => {
            setEmailPopup(false);
            setOpenNext(false);
            if (eServePopup) {
                handleIntegrationAlert();
                dispatch(actions.eServePopupStatus(false));
            }
        }

        const handleDuplicateSet = (data) => {
            const submitRecord = data && data[0];
            setFormLoader(true);
            dispatch(actions.deleteDuplicateSet(Object.assign({}, {
                id: record.id,
                case_id: record.id,
                practice_id: record.practice_id,
                client_id: record.client_id,
                legalforms_id: selectedForm.legalform_id,
                party_id: filter && filter.party_id || false,
                document_type: submitRecord.document_type,
                ids: data && data.map(el => el.id),
                connected_document_ids: data && data[0] && data[0]?.connected_document_ids
            }), setFormLoader));
        }

        const { title, steps, successMessage } = progress && getStepper(progress) || {};

        const loadMetaData = (!metaData?.customerObjections || !metaData?.customerObjections?.length);

        if (loading || closeEditor || (record && !record.questions) || loadMetaData) {
            return <Spinner className={classes.spinner} />;
        }

        return (
            <Grid container className={classes.body}>
                <Grid container id="esquiretek-detail-header" className={classes.actions} style={!md ? { left: '0', width: `${width}px`, top: `${position || getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')}px` } : { width: `${width - navbarWidth}px`, top: `${position || getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')}px`, left: navbarWidth }}>
                    <Grid item xs={12} lg={isUploadDoc ? 7 : 6}>
                        <Grid container
                            direction="row"
                            justify={!lg && 'center' || null}
                            alignItems="center"
                            className={classes.btnActions}>
                            <Grid item className={classes.item}>
                                <ModalForm
                                    title={"Choose Recipients"}
                                    fields={partiesColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                    form={`recipients_${name}`}
                                    onSubmit={handleRecipients.bind(this)}
                                    metaData={metaData}
                                    disableCancelBtn
                                    onSubmitClose
                                    btnLabel="Next">
                                    {(open) =>
                                    (<Grid item xs={12} className={classes.icon}>
                                        <Button type="button" variant="contained" color="primary" className={classes.docButton} onClick={() => handleQuestions(open)} >
                                            {(document && document.type === 'SendQuestions' && props.progress || loader) && <ButtonSpinner color={'#2ca01c'} /> || <SVG src={require(`images/icons/questions2.svg`)} style={{ height: 'auto' }} /> || <FormattedMessage {...messages.sendQuestions} />}
                                        </Button>
                                        {sm && <Grid className={classes.title}>
                                            <FormattedMessage {...messages.sendQuestions} />
                                        </Grid> || null}
                                    </Grid>)}
                                </ModalForm>
                                <AlertDialog
                                    description={descriptionMessage}
                                    openDialog={showAlertDialog}
                                    closeDialog={() => setShowAlertDialog(false)}
                                    onConfirm={() => setShowAlertDialog(false)}
                                    onConfirmPopUpClose
                                    btnLabel1='OK' />
                                <ModalDialog
                                    show={showSendQuestions}
                                    className={classes.form}
                                    style={!md && { justifyContent: 'center' } || {}}
                                    onClose={() => {
                                        setDocument(false);
                                        setSendQuestions(false);
                                        setEditMessage(false);
                                    }}
                                    heading={() =>
                                        <Grid container direction="column">
                                            <Grid item xs={12}>
                                                {match && match.params && match.params.type && match.params.type.toUpperCase() === 'FROGS' && DiscType && (DiscType === 'Disc001' || DiscType === 'Disc004' || DiscType === 'Disc002' || DiscType === 'FL145') ? <Grid container direction="row" justify="space-between" alignItems="center" className={classes.heading}>
                                                    <Grid>
                                                        <select name="languageType" defaultValue={languageType} onChange={(e) => setLanguageType(e.target.value)} className={classes.languageType}>
                                                            <option value="en">English</option>
                                                            <option value="es">Spanish</option>
                                                            <option value="vi">Vietnamese</option>
                                                        </select>
                                                    </Grid>
                                                </Grid> : match && match.params && match.params.type && match.params.type.toUpperCase() === 'FROGS' && DiscType && (DiscType === 'Disc003' || DiscType === 'Disc005') ? <Grid container direction="row" justify="space-between" alignItems="center" className={classes.heading}>
                                                    <Grid>
                                                        <select name="languageType" defaultValue={languageType} onChange={(e) => setLanguageType(e.target.value)} className={classes.languageType}>
                                                            <option value="en">English</option>
                                                            <option value="vi">Vietnamese</option>
                                                        </select>
                                                    </Grid>
                                                </Grid> : null}
                                            </Grid>
                                            <Grid item xs={12} className={classes.questionsDetail}>
                                                <Typography align='center'>
                                                    {sendAllQuestionsPopUp ? sendAllQuestionTxt() : sendSelectedQuestionTxt()}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography className={classes.customTitle}>Message To Client</Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    type='text'
                                                    onChange={handleChangeMessage}
                                                    value={customMessage || ''}
                                                    className={classes.customTextBox}
                                                    fullWidth
                                                    size='small'
                                                    variant="standard"
                                                    multiline
                                                    disabled={editMessage ? false : true}
                                                    inputRef={customRef}
                                                    autoFocus
                                                    InputProps={{
                                                        disableUnderline: editMessage ? false : true,
                                                        endAdornment:
                                                            <InputAdornment position="end">
                                                                <IconButton
                                                                    aria-label="toggle password visibility"
                                                                    className={classes.editIcon}
                                                                    onClick={() => {
                                                                        setEditMessage(!editMessage);
                                                                        setTimeout(() => {
                                                                            customRef.current.focus();
                                                                        }, 100);
                                                                    }}
                                                                >
                                                                    <EditIcon />
                                                                </IconButton>
                                                            </InputAdornment>,
                                                        defaultValue: `Your Lawyers at ${practiceDetails.name} are requesting your responses. Please click the link to access the questionnaire.`,
                                                        classes: {
                                                            input: classes.customInput
                                                        }
                                                    }}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Grid container spacing={1} alignItems="center" className={classes.sendReminder}>
                                                    <Grid item>
                                                        <Typography className={classes.reminderText}>Send a reminder every</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <TextField
                                                            name="reminder_meantime"
                                                            fullWidth
                                                            type="number"
                                                            variant="standard"
                                                            value={reminderMeanTime}
                                                            InputProps={{
                                                                inputProps: {
                                                                    max: 30, min: 1
                                                                }
                                                            }}
                                                            className={classes.reminderDays}
                                                            onChange={handleReminderData}
                                                            style={{ width: '50px' }}
                                                        />
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography className={classes.reminderText}>days until</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <CalendarForm
                                                            initialValues={initialDateRecord}
                                                            name="reminder_till_date"
                                                            className={classes.tillDate}
                                                            minDate={minDate}
                                                            maxDate={maxDate}
                                                            handleTillDateChange={(data) => {
                                                                if (reminderMeanTime && reminderMeanTime < 30) {
                                                                    setReminderError(false);
                                                                }
                                                                setReminderTillDate(data);
                                                            }}
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    {reminderError && <div className={classes.error}>
                                                        <span>{reminderError}</span>
                                                    </div> || ''}
                                                </Grid>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Grid container direction="row" alignItems="center" className={classes.customCheckBox}>
                                                    <Grid item sm={12} xs={12} style={{ display: 'flex' }}>
                                                        <FormControlLabel
                                                            name="shareLawyerResponse"
                                                            style={{ marginRight: '0px' }}
                                                            control={<Checkbox
                                                                style={{ color: "#2ca01c" }}
                                                                onChange={(evt) => setShareLawyerResponse(evt.target.checked)}
                                                            />}
                                                            label={<Typography align="center" variant="subtitle1" className={classes.shareAttorney}>Share attorney response draft with client</Typography>} />
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>}
                                    footerLink={resendQuestions ? () =>
                                        <AlertDialog
                                            description={`Do you want to resend last set of questions sent to client?`}
                                            onConfirm={() => handleResendQuestion()}
                                            onConfirmPopUpClose={true}
                                            btnLabel1='Yes'
                                            btnLabel2='No' >
                                            {(open) =>
                                            (<Grid className={classes.resendLink}>
                                                <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={open}>
                                                    Resend last set of questions sent to client
                                                </Button>
                                            </Grid>)
                                            }
                                        </AlertDialog> : null}
                                    footerStyle={!md ? footerStyle['md'] : footerStyle['lg']}
                                    options={sendAllQuestionsPopUp ? getNoQuesSelectedOptions : getQuesSelectedOptions} />
                            </Grid>
                            <Grid item className={classes.item}>
                                <Link to={{ pathname: isPropoundPractice ? `/casesCategories/cases/${match.params.id}/forms/signature/${match.params.type}/${selectedForm.legalform_id}` : `/casesCategories/cases/${match.params.id}/signature/${match.params.type}/${selectedForm.legalform_id}`, state: location.state }}>
                                    <Grid item xs={12} className={classes.icon}>
                                        <Button type="button" variant="contained" color="primary" className={classes.docButton}>
                                            <SVG src={require(`images/icons/verification.svg`)} style={{ height: 'auto' }} />
                                        </Button>
                                        {sm && <Grid className={classes.title}>
                                            <FormattedMessage {...messages.sendVerification} />
                                        </Grid> || null}
                                    </Grid>
                                </Link> 
                            </Grid>
                            {record && record.state !== 'TX' ? <Grid item className={classes.item}>
                                <DocumentButton
                                    columns={uploadColumns}
                                    value={match.params.type}
                                    classes={classes}
                                    updateBilling={handleBilling}
                                    billing={billing}
                                    record={Object.assign({}, record, { lawyers })}
                                    style={!md && { justifyContent: 'center' } || {}}
                                    label={match && match.params && match.params.type && match.params.type.toUpperCase()}
                                    loading={props.progress}
                                    generateDocument={generateDocument}
                                    openDocument={openDocument}
                                    title="Create Shell"
                                    type="template"
                                    document={document}
                                    sm={sm}
                                    setDocument={setDocument}
                                    modifiedCustomTemplateOptions={modifiedCustomTemplateOptions}
                                    validSubscription={validSubscription}
                                    descriptionMessage={descriptionMessage}
                                    user={user}
                                    metaData={Object.assign({}, { ...metaData }, { modifiedTemplateOptions: customModifiedTemplate })}
                                    forms={forms}
                                    handleUpdatePlanDetails={handleUpdatePlanDetails} 
                                    handleCustomTemplateSignature={handleCustomTemplateSignature}
                                    customModifiedTemplate={customModifiedTemplate}
                                    handleModifiedTemplate={handleModifiedTemplate}
                                    selectQuestionTemplateColumns={selectQuestionTemplateColumns}
                                    subscriptionDetailsColumns={subscriptionDetailsColumns}
                                    freeTrialMessage={freeTrialMessage}
                                    discountDetails={discountDetails}
                                    openAlertDialog={openAlertDialog}
                                    userRecordFormColumns={userRecordFormColumns}
                                    handleUpdateUserRecord={handleUpdateUserRecord}
                                    upShellColumns={upShellFeatureColumns} />
                            </Grid> : null}
                            <Grid item className={classes.item}>
                                <DocumentButton
                                    columns={uploadColumns}
                                    value={match.params.type}
                                    classes={classes}
                                    updateBilling={handleBilling}
                                    billing={billing}
                                    record={Object.assign({}, record, { lawyers })}
                                    label={match && match.params && match.params.type && match.params.type.toUpperCase()}
                                    loading={props.progress}
                                    style={!md && { justifyContent: 'center' } || {}}
                                    generateDocument={generateDocument}
                                    openDocument={handleCreateDiscovery}
                                    title="Create Discovery"
                                    type="final"
                                    document={document}
                                    sm={sm}
                                    setDocument={setDocument}
                                    modifiedCustomTemplateOptions={modifiedCustomTemplateOptions}
                                    validSubscription={validSubscription}
                                    descriptionMessage={descriptionMessage}
                                    user={user}
                                    metaData={Object.assign({}, { ...metaData }, { modifiedTemplateOptions: customModifiedTemplate })}
                                    forms={forms} 
                                    handleUpdatePlanDetails={handleUpdatePlanDetails} 
                                    handleCustomTemplateSignature={handleCustomTemplateSignature}
                                    customModifiedTemplate={customModifiedTemplate}
                                    handleModifiedTemplate={handleModifiedTemplate}
                                    selectQuestionTemplateColumns={selectQuestionTemplateColumns}
                                    subscriptionDetailsColumns={subscriptionDetailsColumns}
                                    freeTrialMessage={freeTrialMessage}
                                    discountDetails={discountDetails}
                                    openAlertDialog={openAlertDialog}
                                    userRecordFormColumns={userRecordFormColumns}
                                    handleUpdateUserRecord={handleUpdateUserRecord}
                                    upShellColumns={upShellFeatureColumns} 
                                    finalDocumentRef={finalDocumentRef}/>
                            </Grid>
                            {record && record.state && stateListPOS(record.state) && record.state !== 'FEDERAL' ? <Grid item className={classes.item}>
                                <POS
                                    fields={POSColumns}
                                    record={posInitialValue}
                                    actions={actions}
                                    metaData={metaData}
                                    style={{ textAlign: 'center' }}
                                    footerBtn={(formRecord, setModalOpen, validate) => <DocumentButton
                                        columns={uploadColumns}
                                        value={match.params.type}
                                        classes={classes}
                                        updateBilling={handleBilling}
                                        billing={billing}
                                        record={Object.assign({}, formRecord, { lawyers })}
                                        label={match && match.params && match.params.type && match.params.type.toUpperCase()}
                                        loading={props.progress}
                                        style={!md && { justifyContent: 'center' } || {}}
                                        closeModal={setModalOpen}
                                        validate={validate}
                                        generateDocument={() => dispatch(actions.generatePOSDocument(formRecord, setModalOpen, customObject))}
                                        openDocument={openDocument}
                                        title="Generate PoS"
                                        type="pos"
                                        document={document}
                                        sm={sm}
                                        setDocument={setDocument}
                                        modifiedCustomTemplateOptions={modifiedCustomTemplateOptions}
                                        descriptionMessage={descriptionMessage} 
                                        user={user}
                                        metaData={Object.assign({}, { ...metaData }, { modifiedTemplateOptions: customModifiedTemplate })}
                                        forms={forms}
                                        handleUpdatePlanDetails={handleUpdatePlanDetails}
                                        handleCustomTemplateSignature={handleCustomTemplateSignature}
                                        customModifiedTemplate={customModifiedTemplate}
                                        handleModifiedTemplate={handleModifiedTemplate} 
                                        selectQuestionTemplateColumns={selectQuestionTemplateColumns}
                                        subscriptionDetailsColumns={subscriptionDetailsColumns}
                                        freeTrialMessage={freeTrialMessage}
                                        discountDetails={discountDetails}
                                        openAlertDialog={openAlertDialog} 
                                        userRecordFormColumns={userRecordFormColumns}
                                        handleUpdateUserRecord={handleUpdateUserRecord}
                                        upShellColumns={upShellFeatureColumns}/>}>
                                    {(openDialog) => {
                                        posDocumentRef.current = { openDialog };
                                        return <Grid item xs={12}>
                                            <Button type="button" variant="contained" color="primary" className={classes.docButton} onClick={openDialog}>
                                                {loaderType === 'pos' && document && document.value === match.params.type && document.type === "pos" && props.progress && <ButtonSpinner color='#2ca01c' /> || <SVG src={require(`images/icons/PoS.svg`)} style={{ height: 'auto' }} />}
                                            </Button>
                                            {sm && <Grid className={classes.title}>Create PoS</Grid> || null}
                                        </Grid>
                                    }}
                                </POS>
                            </Grid> : null}
                            <Grid item className={classes.item}>
                                <AlertDialog
                                    description={`This will mark all lawyer responses (draft, notes not saved as drafts) as final. Do you want to proceed?`}
                                    onConfirm={() => handleSaveAll()}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='Yes'
                                    btnLabel2='No'>
                                    {(open) =>
                                    (<Grid item xs={12} className={classes.icon} onClick={open}>
                                        <Button type="button" variant="contained" color="primary" className={classes.docButton}>
                                            <SVG src={require(`images/icons/save.svg`)} style={{ height: 'auto' }} />
                                        </Button>
                                        {sm && <Grid className={classes.title}>
                                            <FormattedMessage {...messages.saveAllAsFinal} />
                                        </Grid> || null}
                                    </Grid>)}
                                </AlertDialog>
                            </Grid>
                            {record && record.state && stateListPOS(record.state) && record.state !== 'FEDERAL' ? <Grid item className={classes.item}>
                                <ModalForm
                                    initialValues={Object.assign({}, { to_opposing_counsel_email: record && record['opposing_counsel'] && record['opposing_counsel'].map(r => r.opposing_counsel_email).join(','), cc_responder_email: email, selected_forms: selectedRespondForms && selectedRespondForms.length > 0 && selectedRespondForms.map((elm, index) => Object.assign({}, elm, { id: elm.legalform_id, orderId: index + 1 })), attach_documents: selectedRespondForms && selectedRespondForms[0] && selectedRespondForms[0].attach_documents || [], case_title: record?.case_title })}
                                    title={"E-Serve Opposing Counsel"}
                                    onClose={handleCloseEmailModal}
                                    show={emailPopup}
                                    fields={respondSendEmailColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                    form={`respondEmailDocument`}
                                    onSubmit={handleRespondSendEmail.bind(this, 'SendEmail')}
                                    metaData={metaData}
                                    keepDirtyOnReinitialize={true}                                    
                                    enableSubmitBtn
                                    btnLabel="Send Email"
                                    enableScroll={classes.bodyScroll}
                                    footerStyle={{ border: 'none' }}
                                    style={{ paddingRight: '0px' }} />
                                <Grid item xs={12} className={classes.icon}>
                                    <Button type="button" variant="contained" color="primary" className={classes.docButton} onClick={handleEmailDocument} >
                                        {document && document.type === 'SendEmail' && props.progress && <ButtonSpinner color={'#2ca01c'} /> || <SVG src={require(`images/icons/mail.svg`)} style={{ height: 'auto' }} /> || <FormattedMessage {...messages.emailDocument} />}
                                    </Button>
                                    {sm && <Grid className={classes.title}>
                                        <FormattedMessage {...messages.emailDocument} />
                                    </Grid> || null}
                                </Grid>
                            </Grid> : null}
                            {record && record.case_from && record.case_from === 'mycase' && selectedForm && selectedForm.discovery_s3_file_key && <Grid item>
                                <UploadDiscoveryButton 
                                    description={mycaseSessionDiff ? `Do you want to upload the generated discovery document to MyCase?`: `Your MyCase session is expired, please click Activate button that will redirect to MyCase site. Once you logged in, you can try the Upload Discovery option again to upload the document to MyCase.`} 
                                    handleSubmit={handleMyCaseUploadDocument}
                                    btnLabel1={mycaseSessionDiff ? 'Yes' : 'Activate'}
                                    btnLabel2={mycaseSessionDiff ? 'NO' : 'Cancel'} 
                                    onConfirmPopUpClose={true} 
                                    sm={sm}
                                    type="upload"
                                    classes={classes} 
                                    progress={props.progress}
                                    document={document} />
                                <AlertDialog 
                                    description={mycaseSessionDiff ? `Do you want to upload the generated discovery document to MyCase?`: `Your MyCase session is expired, please click Activate button that will redirect to MyCase site. Once you logged in, you can try the Upload Discovery option again to upload the document to MyCase.`}
                                    openDialog={showMyCasePopup}
                                    closeDialog={() => setShowMyCasePopup(false)}
                                    onConfirm={handleMyCaseUploadDocument}
                                    btnLabel1={mycaseSessionDiff ? 'Yes' : 'Activate'}
                                    btnLabel2={mycaseSessionDiff ? 'NO' : 'Cancel'} 
                                    onConfirmPopUpClose />
                            </Grid>}
                            {record && record.case_from && record.case_from === 'filevine' && selectedForm && selectedForm.discovery_s3_file_key && <Grid item>
                                    <UploadDiscoveryButton 
                                        description={`Do you want to upload the generated discovery document to Filevine?`} 
                                        handleSubmit={handleFilevineUploadDocument}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'NO'}
                                        onConfirmPopUpClose={true} 
                                        sm={sm}
                                        type="upload"
                                        classes={classes} 
                                        progress={props.progress}
                                        document={document} />
                                    <AlertDialog 
                                        description={`Do you want to upload the generated discovery document to Filevine?`}
                                        openDialog={showFilevinePopup}
                                        closeDialog={() => setShowFilevinePopup(false)}
                                        onConfirm={handleFilevineUploadDocument}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'NO'}
                                        onConfirmPopUpClose />
                            </Grid>}
                            {record && record.case_from && record.case_from === 'litify' && selectedForm && selectedForm.discovery_s3_file_key && <Grid item>
                                    <UploadDiscoveryButton 
                                        description={`Do you want to upload the generated discovery document to Litify?`} 
                                        handleSubmit={handleLitifyUploadDocument}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'NO'}
                                        onConfirmPopUpClose={true} 
                                        sm={sm}
                                        type="upload"
                                        classes={classes} 
                                        progress={props.progress}
                                        document={document} />
                                    <AlertDialog 
                                        description={`Do you want to upload the generated discovery document to Litify?`}
                                        openDialog={showLitifyPopup}
                                        closeDialog={() => setShowLitifyPopup(false)}
                                        onConfirm={handleLitifyUploadDocument}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'NO'}
                                        onConfirmPopUpClose />
                            </Grid>}
                            {record && record.case_from && record.case_from === 'clio' && selectedForm && selectedForm.discovery_s3_file_key && <Grid item>
                                <UploadDiscoveryButton
                                    description={`Do you want to upload the generated discovery document to Clio?`}
                                    handleSubmit={handleClioUploadDocument}
                                    btnLabel1={'Yes'}
                                    btnLabel2={'NO'}
                                    onConfirmPopUpClose={true}
                                    sm={sm}
                                    type="upload"
                                    classes={classes}
                                    progress={props.progress}
                                    document={document} />
                                <AlertDialog
                                    description={`Do you want to upload the generated discovery document to Clio?`}
                                    openDialog={showClioPopup}
                                    closeDialog={() => setShowClioPopup(false)}
                                    onConfirm={handleClioUploadDocument}
                                    btnLabel1={'Yes'}
                                    btnLabel2={'NO'}
                                    onConfirmPopUpClose />
                            </Grid>}
                        </Grid>
                    </Grid>
                    <Hidden only={['xs']}>
                        <Grid container item xs={12} lg={isUploadDoc ? 5 : 6}
                            direction="row"
                            justify={lg && 'flex-end' || 'center'}
                            alignItems="center"
                            style={!lg && { marginTop: '20px' } || null}>
                            {filterColumns && filterColumns.lawyerFilter ? <Grid>
                                <select name="lawyer_response_status_filter" defaultValue={lawyer_response_status_filter} className={classes.filter} onChange={handleFilter}>
                                    {filterColumns.lawyerFilter.options.map((a, i) => <option key={i} disabled={a.disabled} value={a.value}>{a.label}</option>)}
                                </select>
                            </Grid> : null}
                            {filterColumns && filterColumns.clientFilter ? <Grid style={sm && { marginLeft: '20px' } || null}>
                                <select name="client_response_status_filter" defaultValue={client_response_status_filter} className={classes.filter} onChange={handleFilter}>
                                    {filterColumns.clientFilter.options.map((a, i) => <option key={i} disabled={a.disabled} value={a.value}>{a.label}</option>)}
                                </select>
                            </Grid> : null}
                            {filterColumns && filterColumns.partiesFilter && metaData && metaData.parties && metaData.parties.length > 0 ? <Grid style={sm && { marginLeft: '20px' } || null}>
                                <select name="party_id" className={classes.filter} onChange={handleFilter}>
                                    <option disabled selected>Select Recipients</option>
                                    {metaData && metaData[filterColumns.partiesFilter.options] && metaData[filterColumns.partiesFilter.options].map((a, i) => <option key={i} disabled={a.disabled} value={a.value}>{a.label}</option>)}
                                </select>
                            </Grid> : null}
                        </Grid>
                    </Hidden>
                </Grid>
                <Grid container className={classes.responseBox} style={{ marginTop: `${margin || ((getOffset('esquiretek-cases-header') + getOffset('esquiretek-detail-header') - 120)) || 0}px`, position: 'relative' }}>
                    {record && match && match.params && match.params.type && record[match.params.type] && record[match.params.type].length > 0 ?
                        <Grid item xs={12}>
                            <Paper square className={record[match.params.type].length > 1 ? classes.paper : classes.paperNew}>
                                <Tabs
                                    value={selectedTab}
                                    onChange={handleTab}
                                    variant="fullWidth"
                                    indicatorColor="primary"
                                    textColor="primary">
                                    {(record[match.params.type] || []).map((form, index) => <Tab key={index} label={form && form.filename || ''} />)}
                                </Tabs>
                            </Paper>
                        </Grid> : null}
                    <Grid item style={{ position: 'absolute', right: '0', top: record && match && match.params && match.params.type && record[match.params.type] && record[match.params.type].length > 1 ? '8px' : '0' }} justify="flex-end">
                        <Grid item className="searchBox" onClick={handleClick}>
                            {!anchorEl ? <Icons type="Search" className={classes.searchIcon} /> : null}
                        </Grid>
                        <Popover
                            id={anchorEl ? 'simple-popover' : undefined}
                            open={Boolean(anchorEl)}
                            anchorEl={anchorEl}
                            onClose={handleClose}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            disableScrollLock={true}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                        >
                            <Grid>
                                <label className="react-tableoptions-search--label">
                                    <input
                                        className="react-tableoptions-search--input"
                                        type="text"
                                        placeholder="Search…"
                                        value={searchInput || ''}
                                        onChange={e => {
                                            setSearchInput(e.target.value);
                                            setCurrentPage(1);
                                        }}
                                    />
                                </label>
                            </Grid>
                        </Popover>
                    </Grid>
                    {!formLoader && questions && questions.filter(_ => _.TargetLanguageCode === 'en' && _.client_response_text && _.client_response_text !== null && _.client_response_text).length > 0 ?
                        <Grid xs={12} style={{ marginTop: record && match && match.params && match.params.type && record[match.params.type] && record[match.params.type].length > 1 ? '5px' : '35px' }} className={classes.lawyerResponseText}>
                            <AlertDialog
                                description={`This will copy client responses for all questions to the corresponding attorney response boxes. This feature is best used when individual responses are not already copied (If you have already copied some client responses, this can create/append duplicate text)<br /><br />Would you like to copy all client responses to corresponding attorney response boxes?`}
                                onConfirm={() => handleCopyAllToLawyerResponse()}
                                onConfirmPopUpClose
                                btnLabel1='YES'
                                btnLabel2='NO'>
                                {(open) =>
                                    <Typography variant="subtitle2" className={classes.copyLawyerResponse} onClick={open}>
                                        <b>Copy All To Attorney Response</b>
                                    </Typography>
                                }
                            </AlertDialog>
                        </Grid> : null}
                    {formLoader ? <Spinner className={classes.spinner} /> :
                        questions && questions.length > 0 ?
                            <React.Fragment>
                                <RenderPagination
                                    className="pagination-bar"
                                    records={questions}
                                    currentPage={currentPage}
                                    totalCount={questions.length}
                                    pageLoader={pageLoader}
                                    pageSize={pageSize}
                                    documentType={match.params.type}
                                    onPageChange={page => handlePagination(page)}>{(questions, groupedQuestion) => {
                                        return <Grid>
                                            {showResponseBox ?
                                                (Object.keys(questions) || []).map((key) => {
                                                    const consultation = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['is_consultation_set'] || false;
                                                    const consultationSet = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['consultation_set_no'] && lodash.groupBy(questions[key], 'consultation_set_no') || [];
                                                    
                                                    
                                                    const duplicateSet = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['duplicate_set_no'] && !questions[key][0]['is_consultation_set'] && lodash.groupBy(questions[key], 'duplicate_set_no') || [];

                                                    const allConsultationSetQuestion = groupedQuestion && groupedQuestion[key] && groupedQuestion[key].length > 0 && groupedQuestion[key][0]['is_consultation_set'] && groupedQuestion[key][0]['consultation_set_no'] && lodash.groupBy(groupedQuestion[key], 'consultation_set_no') || [];
                                                    
                                                    const allDuplicateSetQuestion = groupedQuestion && groupedQuestion[key] && groupedQuestion[key].length > 0 && groupedQuestion[key][0] && !questions[key][0]['is_consultation_set'] && groupedQuestion[key][0]['duplicate_set_no'] && lodash.groupBy(groupedQuestion[key], 'duplicate_set_no') || [];

                                                    const showDuplicate = allDuplicateSetQuestion && Object.keys(allDuplicateSetQuestion) && Object.keys(allDuplicateSetQuestion).length > 1;

                                                    const frogsDocumentWithSubGroup = questions[key][0]['document_type'] == 'FROGS' && questions[key] && questions[key].length > 1 ? true : false;

                                                    const category = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['question_category'] || false;

                                                    const questionCategory = match && match.params && match.params.type && match.params.type === 'odd' && category || false;

                                                    return <Grid key={key} item xs={12} style={{ marginTop: '15px' }}>
                                                        {questionCategory ? <Typography style={{ fontWeight: 'bold', paddingLeft: '12px', textDecoration: 'underline', fontSize: '16px', textTransform: 'capitalize' }}>Category - {questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['question_category'].toLowerCase()}</Typography> : null}
                                                        <Grid container direction="column">
                                                            {questions[key] && questions[key].length > 0 && questions[key][0]['question_section_text'] ?
                                                                <Typography variant="subtitle2" style={frogsDocumentWithSubGroup ? { padding: '12px', paddingBottom: '2px' } : { padding: '12px' }}>
                                                                    <b>{questions[key][0]['question_number_text']}. {questions[key][0]['document_type'] == 'FROGS' ? (questions[key][0]['question_text']).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : questions[key][0]['question_text']}</b>
                                                                </Typography> : null}
                                                                {frogsDocumentWithSubGroup ? <ObjectionBox
                                                                    case_title={record.case_title}
                                                                    actions={actions}
                                                                    dispatch={dispatch}
                                                                    questionNumber={key + 1}
                                                                    metaData={metaData}
                                                                    translate={handleTranslate}
                                                                    mergeTranslate={handleMergeTranslate}
                                                                    fields={questions[key] && groupedQuestion[key] && questions[key].length > 0 && groupedQuestion[key].length > 0 && groupedQuestion[key][0]['question_section_text'] && groupedQuestion[key].find(_ => _.subgroup) ? objectionColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !questions?.[key][0].subgroup })) : objectionColumns.filter(_ => _.viewRecord)}
                                                                    reloadForm={handleReload}
                                                                    handleSubGroup={handleSubGroup}
                                                                    user={user}
                                                                    forms={forms}
                                                                    copyToLawyerResponse={handleCopyToLawyerResponse}
                                                                    record={Object.assign({}, questions?.[key][0], { subgroupIds: questions?.[key]?.map(_ => _.id)})}
                                                                    name={name}
                                                                    filterColumns={filterColumns}
                                                                    federal={record.federal}
                                                                    state={record.state}
                                                                    updateEditedFlag={handleEditedFlag}
                                                                    responseHistoryLimit={responseHistoryLimit}
                                                                    showHistoryOption={true}
                                                                    attorneyResponseTracking={record.attorney_response_tracking} /> : null} 
                                                            {consultation ?
                                                                <Typography variant="subtitle2" style={{ paddingLeft: '12px' }}>
                                                                    <b>Note: </b>{match.params.type === 'idc' ? `If you have more than one, please click 'Add New' at the end of this question to record each of them.` : `If you have more than one consultation, examination or treatment, please use "Add new consultation" at the end of this question to record each of them.`}
                                                                </Typography> : null}
                                                            <Grid item xs={12}>
                                                            {!consultation && !showDuplicate && (questions[key] && questions[key].length > 0 && (questions[key] || []).map((form, index) => <ResponseBox
                                                                    key={index}
                                                                    case_title={record.case_title}
                                                                    actions={actions}
                                                                    dispatch={dispatch}
                                                                    questionNumber={key + 1}
                                                                    metaData={metaData}
                                                                    translate={handleTranslate}
                                                                    mergeTranslate={handleMergeTranslate}
                                                                    fields={questions[key] && groupedQuestion[key] && questions[key].length > 0 && groupedQuestion[key].length > 0 && groupedQuestion[key][0]['question_section_text'] && groupedQuestion[key].find(_ => _.subgroup) ? legalColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !form.subgroup })) : legalColumns.filter(_ => _.viewRecord)}
                                                                    reloadForm={handleReload}
                                                                    handleSubGroup={handleSubGroup}
                                                                    user={user}
                                                                    forms={forms}
                                                                    copyToLawyerResponse={handleCopyToLawyerResponse}
                                                                    record={form}
                                                                    name={name}
                                                                    filterColumns={filterColumns}
                                                                    federal={record.federal}
                                                                    state={record.state}
                                                                    updateEditedFlag={handleEditedFlag}
                                                                    responseHistoryLimit={responseHistoryLimit}
                                                                    showHistoryOption={true}
                                                                    attorneyResponseTracking={record.attorney_response_tracking}
                                                                    frogsQuestions={frogsQuestions}
                                                                    dynamicFrogsQuestions={dynamicFrogsQuestions}
                                                                    legalColumns={legalColumns}
                                                                    customLegalColumns={customLegalColumns}
                                                                    objectionColumns={objectionColumns} />) || null)}
                                                                {consultation && Object.keys(consultationSet || []).map((keySet, i) =>
                                                                    <>{consultationSet && consultationSet[keySet] && consultationSet[keySet].length > 0 && consultation ?
                                                                        <Grid className={classes.consultation}>
                                                                            {match.params.type === 'frogs' ? <Typography variant="subtitle2" style={{ padding: '0 12px 0 12px' }}>
                                                                                <b>Consultation {consultationSet[keySet][0]['consultation_set_no']}</b>
                                                                            </Typography> : <Typography variant="subtitle2" style={{ padding: '0 12px 0 12px' }}>
                                                                                <b>Person {consultationSet[keySet][0]['consultation_set_no']}</b>
                                                                            </Typography>}
                                                                            {match.params.type == 'idc' && consultationSet[keySet] && consultationSet[keySet][0]['consultation_set_no'] !== 1 ? <AlertDialog
                                                                                description="Do you want to delete this question set?"
                                                                                onConfirm={() => handleAddNewQuestion(allConsultationSetQuestion[keySet], 'delete')}
                                                                                onConfirmPopUpClose
                                                                                btnLabel1='YES'
                                                                                btnLabel2='NO'>
                                                                                {(open) => <Typography component="span" variant="subtitle2" className={classes.deleteConsultion} onClick={open}>
                                                                                    <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} />
                                                                                </Typography>}
                                                                            </AlertDialog> : consultationSet[keySet] && consultationSet[keySet][0]['consultation_set_no'] !== 1 ? <AlertDialog
                                                                                description="Do you want to delete this consultation?"
                                                                                onConfirm={() => handleConsultation(allConsultationSetQuestion[keySet], 'delete')}
                                                                                onConfirmPopUpClose
                                                                                btnLabel1='YES'
                                                                                btnLabel2='NO'>
                                                                                {(open) => <Typography component="span" variant="subtitle2" className={classes.deleteConsultion} onClick={open}>
                                                                                    <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} />
                                                                                </Typography>}
                                                                            </AlertDialog> : null}
                                                                        </Grid> : null}
                                                                        {(consultationSet[keySet] && consultationSet[keySet].length > 0 && (consultationSet[keySet] || []).map((res, i) =>
                                                                            <ResponseBox
                                                                                key={i}
                                                                                consultationQuestions={consultationSet[keySet]}
                                                                                case_title={record.case_title}
                                                                                actions={actions}
                                                                                dispatch={dispatch}
                                                                                metaData={metaData}
                                                                                translate={handleTranslate}
                                                                                mergeTranslate={handleMergeTranslate}
                                                                                fields={consultationSet[keySet] && consultationSet[keySet].length > 0 && consultationSet[keySet][0]['question_section_text'] && consultationSet[keySet].find(_ => _.subgroup) ? legalColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !res.subgroup })) : legalColumns.filter(_ => _.viewRecord)}
                                                                                reloadForm={handleReload}
                                                                                handleSubGroup={handleSubGroup}
                                                                                user={user}
                                                                                forms={forms}
                                                                                copyToLawyerResponse={handleCopyToLawyerResponse}
                                                                                record={res}
                                                                                name={name}
                                                                                filterColumns={filterColumns}
                                                                                federal={record.federal}
                                                                                state={record.state}
                                                                                updateEditedFlag={handleEditedFlag}
                                                                                responseHistoryLimit={responseHistoryLimit}
                                                                                showHistoryOption={true}
                                                                                attorneyResponseTracking={record.attorney_response_tracking}
                                                                                frogsQuestions={frogsQuestions}
                                                                                dynamicFrogsQuestions={dynamicFrogsQuestions}
                                                                                legalColumns={legalColumns}
                                                                                customLegalColumns={customLegalColumns}
                                                                                objectionColumns={objectionColumns} />
                                                                        ) || null)}
                                                                    </>
                                                                )}
                                                                {showDuplicate && Object.keys(duplicateSet || []).map((keySet, i) =>
                                                                    <>{duplicateSet && duplicateSet[keySet] && duplicateSet[keySet].length > 0 ?
                                                                        <Grid className={classes.consultation}>
                                                                            <Typography variant="subtitle2" style={{ padding: '0 12px 0 12px' }}>
                                                                                <b>Set {duplicateSet[keySet][0]['duplicate_set_no']}</b>
                                                                            </Typography>
                                                                            {duplicateSet[keySet] && duplicateSet[keySet][0]['duplicate_set_no'] !== 1 && <AlertDialog
                                                                                description={`Do you want to delete this set ${keySet}?`}
                                                                                onConfirm={() => handleDuplicateSet(allDuplicateSetQuestion[keySet])}
                                                                                onConfirmPopUpClose
                                                                                btnLabel1='YES'
                                                                                btnLabel2='NO'>
                                                                                {(open) => <Typography component="span" variant="subtitle2" className={classes.deleteConsultion} onClick={open}>
                                                                                    <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} />
                                                                                </Typography>}
                                                                            </AlertDialog>}
                                                                        </Grid> : null}
                                                                        {(duplicateSet[keySet] && duplicateSet[keySet].length > 0 && (duplicateSet[keySet] || []).map((res, i) =>
                                                                            <ResponseBox
                                                                                key={i}
                                                                                case_title={record.case_title}
                                                                                actions={actions}
                                                                                dispatch={dispatch}
                                                                                metaData={metaData}
                                                                                translate={handleTranslate}
                                                                                mergeTranslate={handleMergeTranslate}
                                                                                fields={duplicateSet[keySet] && duplicateSet[keySet].length > 0 && duplicateSet[keySet][0]['question_section_text'] && duplicateSet[keySet].find(_ => _.subgroup) ? legalColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !res.subgroup })) : legalColumns.filter(_ => _.viewRecord)}
                                                                                reloadForm={handleReload}
                                                                                handleSubGroup={handleSubGroup}
                                                                                user={user}
                                                                                forms={forms}
                                                                                copyToLawyerResponse={handleCopyToLawyerResponse}
                                                                                record={res}
                                                                                name={name}
                                                                                filterColumns={filterColumns}
                                                                                federal={record.federal}
                                                                                state={record.state}
                                                                                updateEditedFlag={handleEditedFlag}
                                                                                responseHistoryLimit={responseHistoryLimit}
                                                                                showHistoryOption={true}
                                                                                attorneyResponseTracking={record.attorney_response_tracking}
                                                                                frogsQuestions={frogsQuestions}
                                                                                dynamicFrogsQuestions={dynamicFrogsQuestions}
                                                                                legalColumns={legalColumns}
                                                                                customLegalColumns={customLegalColumns}
                                                                                objectionColumns={objectionColumns}/>
                                                                        ) || null)}
                                                                    </>
                                                                )}
                                                            </Grid>

                                                            {consultation && match.params.type === 'idc' ? <Grid item justify="flex-start">
                                                                <span onClick={!btnLoader ? () => handleAddNewQuestion(questions[key], 'add') : null}>
                                                                    <AddCircleRoundedIcon style={{ fill: "#2ca01c", cursor: 'pointer' }} />
                                                                    <Button type="button">{(questions[key][0].question_number == btnLoader) ? <ClipLoader color="#2ca01c" size={20} /> : match.params.type === 'idc' ? `Add new` : `Add new consultation`}</Button>
                                                                </span>
                                                            </Grid> : consultation ? <Grid item justify="flex-start">
                                                                <span onClick={!btnLoader ? () => handleConsultation(questions[key], 'add') : null}>
                                                                    <AddCircleRoundedIcon style={{ fill: "#2ca01c", cursor: 'pointer' }} />
                                                                    <Button type="button">{btnLoader ? <ClipLoader color="#2ca01c" size={20} /> : match.params.type === 'idc' ? `Add new` : `Add new consultation`}</Button>
                                                                </span>
                                                            </Grid> : null}

                                                            {/* {consultation ? <Grid item justify="flex-start">
                                                                <span onClick={!btnLoader ? match.params.type === 'idc' ? () => handleAddNewQuestion(questions[key], 'add') : () => handleConsultation(questions[key], 'add') : null}>
                                                                    <AddCircleRoundedIcon style={{ fill: "#2ca01c", cursor: 'pointer' }} />
                                                                    <Button type="button">{btnLoader ? <ClipLoader color="#2ca01c" size={20} /> : match.params.type === 'idc' ? `Add new` : `Add new consultation`}</Button>
                                                                </span>
                                                            </Grid> : null} */}

                                                        </Grid>
                                                    </Grid>
                                                }) :
                                                <Grid>
                                                    <ResponseForm
                                                        initialValues={record || {}}
                                                        form={`selectResponseForm`}
                                                        name={name}
                                                        path={path}
                                                        metaData={metaData}
                                                        translate={handleTranslate}
                                                        mergeTranslate={handleMergeTranslate}
                                                        columns={{ legalColumns, objectionColumns }}
                                                        fields={legalColumns.filter(_ => _.viewRecord)}
                                                        onSubmit={handleSubmit.bind(this)}
                                                        locationState={location.state}
                                                        questions={questions}
                                                        groupedQuestion={groupedQuestion}
                                                        record={record}
                                                        case_title={record.case_title}
                                                        setRef={data => setChildRef(data)}
                                                        clearSelection={handleClearSelection}
                                                        selectedQuestions={data => setselectedQuestions(data.length)}
                                                        handleSubGroup={handleSubGroup}
                                                        copyToLawyerResponse={handleCopyToLawyerResponse} 
                                                        documentType={match.params.type}/>
                                                </Grid>}
                                        </Grid>
                                    }}
                                </RenderPagination>
                            </React.Fragment> :
                            <Grid item xs={12} className={classes.notFound}>
                                <FormattedMessage {...messages.notFound} />
                            </Grid>}
                </Grid>
                <Grid item xs={12}>
                    {!loading ? <Stepper
                        title={title}
                        active={active}
                        show={progress ? true : false}
                        close={() => setProgress(false)}
                        steps={steps}
                        error={error}
                        disableCancelBtn
                        successMessage={successMessage} /> : null}
                </Grid>
                <Grid item>
                    <ModalForm
                        initialValues={Object.assign({}, { is_store_filevine_secret: true }) || {}}
                        show={showFileVineForm}
                        title={'Filevine Integration'}
                        fields={filevineForm && filevineForm.filter(_ => _.editRecord && !_.disableRecord)}
                        form={`filevineForm`}
                        onSubmit={handleFilevineSession.bind(this)}
                        btnLabel="Submit"
                        instructionNote={customNote}
                        onClose={() => setShowFileVineForm(false)}
                        onSubmitClose={true}
                        style={{ paddingRight: '0px' }} />
                </Grid>
                <Grid item>
                    <ModalDialog
                        show={docGenModal}  
                        style={!md && { justifyContent: 'center' } || {}}
                        title={"Discovery is already generated for this case. Do you want to edit the existing discovery or create a new discovery?"}
                        options={editActions}
                        onClose={() => setDocGenModal(false)}
                        btnLoader={modalBtnLoader}
                        disableContainer />
                </Grid>
                {openDocEditor ? <Grid item>
                    <DocumentEditor
                        actions={actions}
                        dispatch={dispatch}
                        disableCloseModal
                        progress={docLoader}
                        show={openDocEditor}
                        record={Object.assign({}, selectedForm, { type: match.params.type, s3_file_key: selectedForm.discovery_s3_file_key,  generate_method : "discovery" })}
                        title={'Document Preview'}
                        btnLabel={"Save & E-Serve"}
                        showSaveBtn={record && record.state && stateListPOS(record.state) ? true : false}
                        infoNote={"E-serve opposing counsel directly from EsquireTek after reviewing your document. Edits can be made within the window below."}
                        onClose={handleCloseDocEditor}
                        handleSubmitBtn={handleSaveAndEmailDocxFile}
                        handleDownloadBtn={handleDownloadDocxFile}
                        handleSaveDocxFile={handleSaveDocxFile}
                        discoveryHistory={discoveryHistory}
                    />
                </Grid> : null}             
                {!progress ? <Snackbar show={error || success || errorMessage || subscriptionSuccess ? true : false} text={error || success || errorMessage || subscriptionSuccess} severity={error || errorMessage ? 'error' : 'success'} handleClose={() => { setErrorMessage(false); dispatch(actions.loadRecordsCacheHit()) }} /> : null}
            </Grid >
        )


    }

    ViewCasesDetailPage.propTypes = {
        active: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        forms: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        forms: selectForm(),
        metaData: selectRecordsMetaData(),
        loading: selectPageLoader(),
        success: selectSuccess(),
        billing: selectBilling(),
        progress: selectProgress(),
        active: selectKey(),
        user: selectUser(),
        expand: selectSessionExpand(),
        records: selectRecords(),
        discountDetails: selectSettingsDiscount(),
        subscriptionSuccess: selectSubscriptionSuccess(),
        documentStatus: selectDocumentStatus(),
        eServePopup: selectEServePopup(),
        frogsQuestions: selectFrogsQuestions(),
        dynamicFrogsQuestions: selectDynamicFrogsQuestions(),
        discoveryHistory: selectDiscoveryHistory()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewCasesDetailPage);

}
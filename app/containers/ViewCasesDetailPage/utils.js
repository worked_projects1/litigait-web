import moment from "moment";


export const getNoQuesSelectedOptions1 = ['Yes, send all', 'Select questions to send', 'Cancel'];

export const getQuesSelectedOptions1 = ['Send selected questions', 'Cancel and clear selection'];


export const getNoQuesSelectedOptions = [
    {
        action: 'handleSendAll',
        label: 'Yes, send all'
    },
    {
        action: 'handleSelectQuestions',
        label: 'Select Questions to send'
    },
    {
        action: 'close',
        label: 'Cancel'
    }
];

export const getQuesSelectedOptions = [
    {
        action: 'handleSendSelectedQuestions',
        label: 'Send selected questions'
    },
    {
        action: 'handleClearSelection',
        label: 'Cancel and clear selection'
    }
];


export const handleSearch = (val, record) => {
  let filteredQuestion;  
  if(val !== '') {
    let value =  parseFloat(val);
    if(isNaN(value)) {
      filteredQuestion = record && record.questions && record.questions.length > 0 && record.questions.filter(el => {
        return el.question_text.toLowerCase().trim().includes(val.toString().toLowerCase().trim());
      });
    } else {
      filteredQuestion = record && record.questions && record.questions.length > 0 && record.questions.filter(el => {
        return el.question_number_text.toLowerCase().includes(val.toString().toLowerCase());
      });
    }
    return Object.assign({}, record, { questions: filteredQuestion });
  }    
}

const defaultInitialDisclosureQuestions = () => {
  return [
    {
        "question_number": 1,
        "question_number_text": "1",
        "question_text": "The names, addresses, telephone numbers, and email addresses of all persons likely to have discoverable information, along with the subjects of that information, that the disclosing party may use to support their claims or defenses, or that is relevant to the subject matter of the action.",
        "question_id": 1,
        "question_options": null,
        "question_section_id": "1",
        "question_section": "(a)",
      "question_section_text": "The name, address, telephone number, and email address;",
      "question_type": "InitialDisclosure",
      "is_consultation_set": 1,
      "consultation_set_no": 1,
      "is_duplicate": 0,
      "duplicate_set_no": 1
        
    },
    {
        "question_number": 1,
        "question_number_text": "1",
        "question_text": "The names, addresses, telephone numbers, and email addresses of all persons likely to have discoverable information, along with the subjects of that information, that the disclosing party may use to support their claims or defenses, or that is relevant to the subject matter of the action.",
        "question_id": 2,
        "question_options": null,
        "question_section_id": "2",
        "question_section": "(b)",
        "question_section_text": "subject of discoverable information",
        "question_type": "InitialDisclosure",
        "is_consultation_set": 1,
        "consultation_set_no" : 1,
        "is_duplicate": 0,
        "duplicate_set_no": 1
    },
    {
        "question_number": 2,
        "question_number_text": "2",
        "question_text": "A copy, or a description by category and location, of all documents, electronically stored information, and tangible things that the disclosing party has in its possession, custody, or control and may use to support its claims or defenses, or that is relevant to the subject matter of the action.",
        "question_id": 3,
        "question_options": null,
        "question_section_id": "1",
        "question_section": "(a)",
        "question_section_text": "Description by category and cation of document, electronically stored information, or tangible thing.",
        "question_type": "InitialDisclosure",
        "is_consultation_set": 1,
        "consultation_set_no" : 1,
        "is_duplicate": 0,
        "duplicate_set_no": 1
    }
]
}


/**
 * Frogs disc001 6.4 questions
 */
 const defaultConsultationQuestions = () => {
  let question_text = "Did you receive any consultation or examination (except from expert witnesses covered by Code of Civil Procedure sections 2034.210-2034.310) or treatment from a HEALTH CARE PROVIDER for any injury you attribute to the INCIDENT? If so, for each HEALTH CARE PROVIDER state:";

  return [
      {
          question_id: 83,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "1",
          question_section: "(a)",
          question_section_text: "the name, ADDRESS, and telephone number;",
          is_duplicate: 0,
          duplicate_set_no: 1
      },
      {
          question_id: 84,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "2",
          question_section: "(b)",
          question_section_text: "the type of consultation, examination, or treatment provided;",
          question_options: null,
          is_duplicate: 0,
          duplicate_set_no: 1
      },
      {
          question_id: 85,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "3",
          question_section: "(c)",
          question_section_text: "the dates you received consultation, examination, or treatment; and",
          question_options: null,
          is_duplicate: 0,
          duplicate_set_no: 1
      },
      {
          question_id: 86,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "4",
          question_section: "(d)",
          question_section_text: "the charges to date.",
          question_options: null,
          is_duplicate: 0,
          duplicate_set_no: 1
      }
  ]
}

export const addInitialDiscQuestions = (record) => {
  // const selectedQuestions 
  let data = record && record.length > 0 && defaultInitialDisclosureQuestions();
  const questionsRecord = record && record.length > 0 && record.sort((a,b) => a.consultation_set_no - b.consultation_set_no)
  const selectedQuestions = questionsRecord && questionsRecord[(questionsRecord && questionsRecord.length) - 1];
  const addSelectedQuestions =  data && data.length && data.filter((el) => el.question_number == selectedQuestions?.question_number);
  let questions;
  if (addSelectedQuestions && record) {
    questions = addSelectedQuestions.map((el) => Object.assign({}, el, {
      id: Date.now().toString(36) + Math.random().toString(36).substr(2),
      case_id: selectedQuestions.case_id,
      practice_id: selectedQuestions.practice_id,
      client_id: selectedQuestions.client_id,
      legalforms_id: selectedQuestions.legalforms_id,
      document_type: selectedQuestions.document_type,
      party_id: selectedQuestions.party_id || false,
      consultation_set_no: selectedQuestions.consultation_set_no + 1,
      is_consultation_set: 1,
      subgroup: null,
      lawyer_response_text: null,
      lawyer_response_status: "NotStarted",
      client_response_text: null,
      client_response_status: "NotSetToClient",
      file_upload_status: null,
      uploaded_documents: null,
      last_sent_to_client: null,
      last_updated_by_lawyer: null,
      last_updated_by_client: null,
      TargetLanguageCode: null,
      is_the_client_response_edited: null,
      consultation_createdby: null,
      total_consultation_count: null
    }))
  }
  return questions;
}

export const consultationQuestions = (record) => {
  let data = record && record.length > 0 && defaultConsultationQuestions();
  let questions;
  if (data && record) {
      questions =
          data &&
          data.map((el) =>
              Object.assign({}, el, {
                id: Date.now().toString(36) + Math.random().toString(36).substr(2),
                case_id: record[0].case_id,
                practice_id: record[0].practice_id,
                client_id: record[0].client_id,
                legalforms_id: record[0].legalforms_id,
                document_type: record[0].document_type,
                party_id: record[0].party_id || false,
                consultation_set_no: record[0].total_consultation_count + 1,
                is_consultation_set: 1,
                subgroup: null,
                lawyer_response_text: null,
                lawyer_response_status: "NotStarted",
                client_response_text: null,
                client_response_status: "NotSetToClient",
                file_upload_status: null,
                uploaded_documents: null,
                last_sent_to_client: null,
                last_updated_by_lawyer: null,
                last_updated_by_client: null,
                TargetLanguageCode: null,
                is_the_client_response_edited: null,
                consultation_createdby: null,
                total_consultation_count: null
              })
          );
  }

  return questions;
};

export const diffBetweenDates = (start, end, type) => {
  if (start && end) {
    let diff = moment(end).diff(start, type);
    return diff;
  }
}


export const attorneySignatureList = ({ record, metaData }) => {

  const userSignatures = metaData && metaData['attoneySignatureOptions'] && metaData['attoneySignatureOptions']?.length > 0 ? metaData['attoneySignatureOptions'] : false;
  let assignedAttorney = record && record?.attorneys && record?.attorneys?.split(',');
  assignedAttorney = assignedAttorney && assignedAttorney?.length > 0 && assignedAttorney || false;

  let attorneySignature = userSignatures && assignedAttorney && assignedAttorney.reduce((acc, el) => {
    let check = userSignatures.find(_ => _.value === el)
    if (check) {
      acc.push(check)
    }
    return acc;
  }, []).filter(_ => _.signature);
  attorneySignature = attorneySignature?.length > 0 && attorneySignature?.map(_ => Object.assign({}, { value: _.signature, label: _.name })) || false;

  return { attorneySignature };
}

export const handleClass = ({ status, form, value, classes }) => {
  if (status == 'NotStarted' || status == 'NotSetToClient') {
      return form ? classes.blackColorForm : value !== "client_response_text" ? classes.blackColorResp : classes.blackColor;
  } else if (status == 'Draft' || status == 'SentToClient') {
      return form ? classes.blueColorForm : value !== "client_response_text" ? classes.blueColorResp : classes.blueColor;
  } else if (status == 'Final' || status == 'ClientResponseAvailable') {
      return form ? classes.fieldColorForm : value !== "client_response_text" ? classes.fieldColorResp : classes.fieldColor;
  }
}


import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    filter: {
        padding: '8px',
        border: '2px solid gray',
        borderRadius: '28px',
        WebkitAppearance: 'none',
        paddingLeft: '14px',
        fontFamily: 'Avenir-Bold',
        outline: 'none',
        width: '194px',
        fontSize: '13px',
        position: 'relative',
        backgroundImage: `url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUwcHgiDQoJIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MCA1MCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8ZyBpZD0iTGF5ZXJfMSIgZGlzcGxheT0ibm9uZSI+DQoJDQoJCTxwYXRoIGRpc3BsYXk9ImlubGluZSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSINCgkJTTQ1LjM0MywyNy43MjJ2LTUuNDQzbC00LjExMy0wLjg4NWMtMC40MjQtMS45MTktMS4xNzktMy43MTItMi4yMDMtNS4zMThsMi4yODMtMy41MzVsLTMuODUtMy44NDlsLTMuNTMzLDIuMjgyDQoJCWMtMS42MDctMS4wMjUtMy40LTEuNzgxLTUuMzItMi4yMDZsLTAuODg1LTQuMTA5aC01LjQ0M2wtMC44ODUsNC4xMDljLTEuOTE5LDAuNDI1LTMuNzEzLDEuMTgxLTUuMzIsMi4yMDZMMTIuNTQsOC42OTENCgkJTDguNjkxLDEyLjU0bDIuMjgyLDMuNTM1Yy0xLjAyMywxLjYwNi0xLjc3OSwzLjM5OS0yLjIwMyw1LjMxOGwtNC4xMTMsMC44ODV2NS40NDNsNC4xMTYsMC44ODYNCgkJYzAuNDI1LDEuOTE3LDEuMTgsMy43MDgsMi4yMDMsNS4zMTNMOC42OTEsMzcuNDZsMy44NDksMy44NWwzLjU0MS0yLjI4N2MxLjYwNCwxLjAyMiwzLjM5NSwxLjc3Nyw1LjMxLDIuMjAxbDAuODg3LDQuMTE5aDUuNDQzDQoJCWwwLjg4Ny00LjExOWMxLjkxNi0wLjQyNCwzLjcwNi0xLjE3OSw1LjMxMS0yLjIwMWwzLjU0MSwyLjI4N2wzLjg1LTMuODVsLTIuMjg2LTMuNTM5YzEuMDIzLTEuNjA1LDEuNzc5LTMuMzk2LDIuMjA0LTUuMzEzDQoJCUw0NS4zNDMsMjcuNzIyeiIvPg0KPC9nPg0KPGcgaWQ9IkxheWVyXzIiIGRpc3BsYXk9Im5vbmUiPg0KCQ0KCQk8Y2lyY2xlIGRpc3BsYXk9ImlubGluZSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBjeD0iMjUiIGN5PSIyNSIgcj0iMTkuNSIvPg0KCTx0ZXh0IHRyYW5zZm9ybT0ibWF0cml4KDEgMCAwIDEgMTkuMTkxOSAzMy4zOTc1KSIgZGlzcGxheT0iaW5saW5lIiBmb250LWZhbWlseT0iJ015cmlhZFByby1SZWd1bGFyJyIgZm9udC1zaXplPSIyOC42MTA3Ij4/PC90ZXh0Pg0KPC9nPg0KPGcgaWQ9IkxheWVyXzMiIGRpc3BsYXk9Im5vbmUiPg0KCTxwb2x5Z29uIGRpc3BsYXk9ImlubGluZSIgcG9pbnRzPSIyNSwzNiAyNSwzNiAzLDE0IDQ3LDE0IAkiLz4NCjwvZz4NCjxnIGlkPSJMYXllcl80Ij4NCgk8Zz4NCgkJPHBvbHlsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzZCNkM3MiIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50cz0iNDcuOTgxLDEzLjE1NiAyNC4yOTQsMzYuODQ0IDI1LDM2LjEzOCANCgkJCTIuMDE5LDEzLjE1NiAJCSIvPg0KCTwvZz4NCgkNCgkJPHBhdGggZGlzcGxheT0ibm9uZSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNkI2QzcyIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSINCgkJTTQxLjkxOSw3LjU1bC0zMy45NCwzNC45TDQxLjkxOSw3LjU1eiIvPg0KCQ0KCQk8cGF0aCBkaXNwbGF5PSJub25lIiBmaWxsPSJub25lIiBzdHJva2U9IiM2QjZDNzIiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIGQ9Ig0KCQlNNy40NTYsNy41NWwzNC40NjMsMzQuOUw3LjQ1Niw3LjU1eiIvPg0KPC9nPg0KPGcgaWQ9IkxheWVyXzUiIGRpc3BsYXk9Im5vbmUiPg0KCTxnIGRpc3BsYXk9ImlubGluZSI+DQoJCTxwYXRoIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzFCNzVCMSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgZD0iDQoJCQlNMjYuODg0LDEyLjgyOWw4LjEzMS04LjEzYzIuOTMtMi45Myw3LjY4LTIuOTMsMTAuNjA5LDBjMS40NiwxLjQ2LDIuMiwzLjM4LDIuMiw1LjNzLTAuNzQsMy44NC0yLjIsNS4zMWwtMTIuOTg5LDEyLjk5DQoJCQljLTIuODgxLDIuODgtNy41MDEsMi45MzEtMTAuNDQxLDAuMTUiLz4NCgkJPHBhdGggZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMUI3NUIxIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSINCgkJCU0yMy4xMjQsMzcuMjA3bC04LjE1Miw4LjEwOGMtMi45MzgsMi45MjItNy42ODgsMi45MDktMTAuNjA5LTAuMDI4Yy0xLjQ1Ni0xLjQ2NC0yLjE5MS0zLjM4Ni0yLjE4Ni01LjMwNg0KCQkJYzAuMDA1LTEuOTIsMC43NS0zLjgzOCwyLjIxNS01LjMwNWwxMy4wMjQtMTIuOTU1YzIuODg5LTIuODcyLDcuNTA4LTIuOTExLDEwLjQ0MS0wLjEyMyIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJMYXllcl82IiBkaXNwbGF5PSJub25lIj4NCgk8ZyBkaXNwbGF5PSJpbmxpbmUiPg0KCQk8cGF0aCBmaWxsPSJub25lIiBzdHJva2U9IiNGRkZGRkYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSJNMzksNDEuNzkyYzAsMi4yMDktMS43OTEsNC00LDRIMTUNCgkJCWMtMi4yMDksMC00LTEuNzkxLTQtNHYtMThjMC0yLjIwOSwxLjc5MS00LDQtNGgyMGMyLjIwOSwwLDQsMS43OTEsNCw0VjQxLjc5MnoiLz4NCgkJPHBhdGggZmlsbD0ibm9uZSIgc3Ryb2tlPSIjRkZGRkZGIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgZD0iTTE1LjM3NSwxOS44NzZ2LTYuNzE1DQoJCQljMC00Ljk0NCw0LjAwOC04Ljk1Myw4Ljk1NC04Ljk1M2gxLjM0M2M0Ljk0NCwwLDguOTUzLDQuMDA5LDguOTUzLDguOTUzdjYuNzE1Ii8+DQoJCTxjaXJjbGUgZmlsbD0iI0ZGRkZGRiIgY3g9IjI1IiBjeT0iMzAuNzI5IiByPSIzLjA2MyIvPg0KCQkNCgkJCTxsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iI0ZGRkZGRiIgc3Ryb2tlLXdpZHRoPSIzIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgeDE9IjI1IiB5MT0iMzAuNzkyIiB4Mj0iMjUiIHkyPSIzOC4xNjciLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg==) !important`,
        backgroundRepeat: 'no-repeat !important',
        backgroundSize: '16px !important',
        backgroundPosition: '95% !important',
        backgroundColor: 'transparent !important',
        marginTop: '10px'

    },
    body: {
        padding: '20px',
        paddingTop: '0px',
        marginTop: '118px',
        [theme.breakpoints.down('xs')]: {
            padding: '8px',
        }
    },
    responseBox: {
        margin: '10px 0px',
    },
    spinner: {
        padding: '20px',
        marginTop: '100px',
    },
    docButton: {
        fontWeight: 'bold',
        borderRadius: '100%',
        textTransform: 'none',
        width: '46px',
        height: '46px',
        fontFamily: 'Avenir-Regular',
        fontSize: '13px',
        backgroundColor: 'white !important',
        boxShadow: 'none',
        padding: '6px',
        border: '2px solid #2ca01c',
        minWidth: '46px',
        '&:hover': {
            boxShadow: 'none'
        },
        [theme.breakpoints.down('xs')]: {
            width: '40px',
            height: '40px',
            minWidth: '40px',
            marginTop: '5px'
        }
    },
    btnActions: {
        marginTop: '12px'
    },
    notFound: {
        textAlign: 'center',
        margin: '25px',
        marginTop: '50px',
        fontWeight: 'bold',
        fontSize: '15px'
    },
    actions: {
        position: 'fixed',
        height: 'auto',
        zIndex: '999',
        top: '0',
        left: '60px',
        paddingLeft: '25px',
        paddingRight: '25px',
        paddingBottom: '20px',
        background: '#fff',
        boxShadow: '0px 3px 4px -1px lightgrey'
    },
    languageType: {
        padding: '2px 12px 2px 12px',
        border: '2px solid gray',
        borderRadius: '28px',
        paddingLeft: '20px',
        paddingRight: '30px',
        fontFamily: 'Avenir-Bold',
        outline: 'none',
        fontSize: '13px',
        position: 'relative',
        background: 'transparent'
    },
    heading: {
        marginTop: '20px',
        marginBottom: '20px',
        paddingRight: '6px',
        paddingLeft: '15px'
    },
    title: {
        fontSize: '11px',
        marginTop: '10px',
        color: '#1070C0'
    },
    icon: {
        textAlign: 'center'
    },
    item: {
        marginRight: '16px'
    },
    modalPaper: {
        minWidth: '50% !important',
    },
    paper: {
        flexGrow: 1,
        maxWidth: '75%',
        maxHeight: '58px',
        boxShadow: 'none',
        '@global': {
            '.MuiTab-textColorPrimary.Mui-selected': {
                color: '#2ca01c'
            },
            '.MuiButtonBase-root': {
                width: '120px'
            },
            '.MuiButtonBase-root > span': {
                fontFamily: 'Avenir-Bold'
            },
            '.MuiTabs-indicator': {
                backgroundColor: '#2ca01c'
            }
        }
    },
    paperNew: {
        flexGrow: 1,
        maxWidth: '50%',
        maxHeight: '58px',
        boxShadow: 'none',
        '@global': {
            '.MuiTab-textColorPrimary.Mui-selected': {
                color: '#2ca01c'
            },
            '.MuiButtonBase-root': {
                width: '120px'
            },
            '.MuiButtonBase-root > span': {
                fontFamily: 'Avenir-Bold'
            },
            '.MuiTabs-indicator': {
                backgroundColor: '#2ca01c'
            }
        }
    },
    resendLink: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        cursor: 'pointer',
        fontWeight: 'bold',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        textTransform: 'capitalize',
    },
    posButton: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    button2: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Bold',
        backgroundColor: '#fff !important',
        boxShadow: 'none',
        color: '#47AC39',
        fontSize: '15px',
        paddingTop: '2px',
        paddingBottom: '2px',
        '&:hover': {
            boxShadow: 'none'
        }
    },
    customInput: {
        padding: '0px',
        fontFamily: 'Avenir-Regular',
        fontSize: '16px',
        '&:disabled': {
            color: 'rgba(0, 0, 0, 0.87) !important'
        }
    },
    customTextBox: {
        padding: '30px',
        paddingBottom: '0px',
        paddingTop: '6px',
        borderColor: '#2ca01c !important',
        '& :after': {
            borderColor: '#2ca01c',
        },
        '& :before': {
            borderColor: '#2ca01c',
        },
        '& :hover': {
            borderColor: '#2ca01c'
        },
        '&.Mui-focused fieldset': {
            borderColor: '#2ca01c',
        }
    },
    editIcon: {
        padding: '12px 0px'
    },
    customTitle: {
        paddingLeft: '30px',
        paddingRight: '30px',
        fontWeight: 'bold',
        fontSize: '14px',
        paddingTop: '12px'
    },
    searchIcon: {
        width: '30px',
        height: '30px',
        color: '#2CA01C',
        cursor: 'pointer'
    },
    lawyerResponseText: {
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: '246px'
    },
    copyLawyerResponse: {
        color: '#47AC39',
        cursor: 'pointer',
        fontSize: '12px'
    },
    deleteConsultion: {
        color: 'gray',
        cursor: 'pointer',
        fontSize: '0.84rem',
        width: '20px !important',
        height: '20px !important',
        marginBottom: '-5px !important'
    },
    attachDelete: {
        width: '20px',
        height: '20px',
        paddingLeft: '1px',
        marginBottom: '4px',
    },
    consultation: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: '8px'
    },
    customCheckBox: {
        paddingLeft: '30px',
        paddingRight: '30px',
        paddingTop: '12px'
    },
    shareAttorney: {
        marginTop: '5px'
    },
    footerButton: {
        fontWeight: 'bold',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        marginRight: '12px',
        borderRadius: '28px',
        textTransform: 'capitalize'
    },
    footerButton1: {
        fontWeight: 'bold',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        borderRadius: '28px',
        textTransform: 'capitalize'
    },
    bodyScroll: {
        maxHeight: '400px',
        overflowY: 'scroll',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 12,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    reminderDays: {
        borderColor: '#2ca01c !important',
        '& :after': {
            borderColor: '#2ca01c',
        },
        '& :before': {
            borderColor: '#2ca01c',
        },
        '& :hover': {
            borderColor: '#2ca01c'
        },
        '&.Mui-focused fieldset': {
            borderColor: '#2ca01c',
        },
        '& .MuiInputLabel-root.Mui-focused': {
            color: '#2ca01c !important',
        },
        '& .MuiInputBase-input': {
            fontSize: '14px',
            fontFamily: 'Avenir-Regular',
            textAlign:'center'
        }
    },
    tillDate: {
        fontSize: '14px',
        fontFamily: 'Avenir-Regular',
        marginTop: '-16px',
        borderColor: '#2ca01c !important',
        width: '90px',
        textAlign:'center',
        '& :after': {
            borderColor: '#2ca01c',
        },
        '& :before': {
            borderColor: '#2ca01c',
        },
        '& :hover': {
            borderColor: '#2ca01c'
        },
        '&.Mui-focused fieldset': {
            borderColor: '#2ca01c',
        },
        '& .MuiInputLabel-root': {
            '.Mui-focused': {
                color: '#2ca01c !important',
            },
        }
    },
    reminderText: {
        fontSize: '14px',
        fontFamily: 'Avenir-Bold',
    },
    questionsDetail: {
        padding: '10px 0px'
    },
    sendReminder: {
        paddingLeft: '30px', 
        paddingRight: '30px', 
        paddingTop: '12px'
    },
    error: {
        fontSize: '14px',
        color: 'red',
        paddingLeft: '30px',
        paddingRight: '30px',
        paddingTop: '12px'
    },
    modalPaper1: {
        width: '50% !important',
        [theme.breakpoints.down('xs')]: {
            width: '92% !important'
        }
    },
}));

export const footerStyle = {
    lg: {
        justifyContent: 'space-around',
        flexDirection: 'row',
        minWidth: '500px',
        padding: '15px',
        paddingTop: '0px'
    },
    md: {
        justifyContent: 'center',
        flexDirection: 'column',
        minWidth: '0px',
        padding: '15px',
        paddingTop: '0px'
    }
};

export default useStyles;
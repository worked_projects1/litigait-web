/**
 *
 *  Modal Record Form
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { disableOnSubmitForm } from 'utils/tools';
import Styles from './styles';
import moment from 'moment';
import ReactHtmlParser from 'react-html-parser';
import * as Diff from 'diff';

/**
 *
 * @param {object} props
 * @returns
 */
function HistoryModalRecordForm(props) {
  
	const { title, handleSubmit, submitting, children, metaData, className, style, btnLabel, onOpen, onClose, error, show, pristine, invalid, onSubmitClose, destroy, disableContainer, enableSubmitBtn,disableCancelBtn, progress, paperClassName, page, form, responseHistory, dispatch, change } = props;

	const classes = Styles();
	const [showModal, setModalOpen] = useState(false);
	const [selectedValue, setSelectedValue] = useState(0);

	const theme = useTheme();
	const md = useMediaQuery(theme.breakpoints.up('md'));
	const usersList = metaData && metaData['usersList'] && metaData['usersList'].length > 0 && metaData['usersList']  || false;
	const currentEdit = responseHistory?.[selectedValue]?.timestamp;

	useEffect(() => {
		return () => destroy();
	}, []);

	const closeModal = () => {
		setModalOpen(false);
		if (onClose) onClose();
	};

  	const handleSubmitRecord = e => {
		const isDisableOnSubmit = disableOnSubmitForm.find(e => form.startsWith(e));
		if (isDisableOnSubmit) {
			e.preventDefault();
		} else {
			handleSubmit(e);
		}
  	};
	
	const handleSelector = (position, data) => {
		setSelectedValue(position);
		dispatch(change('response', data));
	};

	return (
		<Grid container={disableContainer ? false : true} className={className} style={style}>
			{children && children(() => setModalOpen(!showModal))}
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				open={showModal || show || false}
				className={classes.modal}
				onClose={closeModal}
				closeAfterTransition
				BackdropComponent={Backdrop}
				onRendered={onOpen}
				BackdropProps={{
				timeout: 500,
				}}
			>
				<Fade in={showModal || show || false}>
					<Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
						<form onSubmit={e => handleSubmitRecord(e)} className={classes.form} noValidate>
							<Grid container className={classes.header} justify="space-between">
								<Grid item xs={10}>
									<Typography component="span" className={classes.title}>
										{selectedValue === 0 ? `Current version with changes from previous version` : `Version saved on ${moment(currentEdit).format('MM/DD/YYYY, HH:mm A')}`}
									</Typography>
								</Grid>
								<Grid item xs={2} style={{ textAlign: 'end' }}>
									{!disableCancelBtn ? (<CloseIcon onClick={closeModal} className={classes.closeIcon}/>) : null}
								</Grid>
							</Grid>
							<Grid container className={classes.body}>
								<Grid item xs={12} style={{ display: 'flex' }}>
									<Grid item xs={12} md={8}>
										<Grid item xs={12} className={`${classes.response} ${classes.responseList}`}>
											{responseHistory?.length > 0 && Diff.diffLines((responseHistory[selectedValue + 1]?.response || ''), responseHistory[selectedValue]?.response).map(el => {
												let addedText;
												let removedText;
												let space = `<p id="space">&nbsp;</p>`;
												let status = responseHistory[selectedValue]?.status;
												const backgroundColor = status === 'Final' ? 'rgb(228 255 223)' : status === 'Draft' ? 'rgb(177 203 232)' : 
												'rgb(232 235 255)';
												const color = status === 'Final' ? 'rgb(84 183 70)' : status === 'Draft' ? '#1a73e8' : 
												'rgb(99 132 236)';
												if(el.added) {
													addedText = el?.value?.replaceAll('<p>&nbsp;</p>', space);
													addedText = addedText.replaceAll('<p>', `<p id="inserted" style="background: ${backgroundColor}; color: ${color};display: inline;">`);
												} 
												if(el.removed) {
													removedText = el?.value?.replaceAll('<p>&nbsp;</p>', space);
													removedText = removedText.replaceAll('<p>', `<p id="removed" style="color: ${color};text-decoration: line-through;display: inline;">`);
												}
												return el.added ? <>{ReactHtmlParser(addedText)}</> : el.removed ? <>{ReactHtmlParser(removedText)}</> : <>{ReactHtmlParser(el.value)}</>;
											})}
										</Grid>
										<Grid container justify="flex-end">
											<Button
												type="submit"
												variant="contained"
												color="primary"
												disabled={
												page && page === 'casesEdit'
													? submitting
													: !enableSubmitBtn && (pristine || invalid)
												}
												className={classes.button}
												onClick={!invalid && onSubmitClose ? closeModal : null}
											>
												{((submitting || progress) && <ButtonSpinner />) ||
												btnLabel ||
												'submit'}
											</Button>
										</Grid>
									</Grid>
									<Grid item xs={12} md={4} className={classes.versionHistory}>
										<Grid item className={classes.versionHeader}>
											<Typography variant="h6" style={{ padding: '15px' }}>Version history</Typography>
										</Grid>
										<Grid className={classes.responseList}>
											{responseHistory && responseHistory.length > 0 && responseHistory.map((e, i) => {
												let filterUser = usersList.filter(el => el.id === e.user_id);
												let userName = filterUser[0]?.label;
												return (
													<Grid item className={`${classes.list} ${selectedValue === i ? classes.selected : classes.hover} `} key={i} onClick={() => handleSelector(i, e)}>
														<Grid item style={{ padding: '12px 18px 3px 36px' }}>
															<Typography className={`${classes.date} ${selectedValue === i ? classes.selectedText: ''}`}>
																{moment(e.timestamp).format('MMMM D, h:mm A')}
															</Typography>
															{i == 0 && (<Grid item className={`${classes.currentVersion} ${selectedValue === i ? classes.selectedText : ''}`}>
																Current Version
															</Grid>)}
															<Grid item className={classes.userDetails}>
																<Typography className={classes.userName}>
																	{userName}
																</Typography>
															</Grid>
															<Grid item className={classes.responseType}>
																<Typography className={classes.responseType} style={{ color: e?.status === 'Final' ? 'rgb(44, 160, 28)' : e?.status === 'Draft' ? '#3275BF' : 'rgb(135 148 250)'}}>
																	{e?.status === 'Final' ?  'Saved as final' : e?.status === 'Draft' ? 'Saved as draft' : e?.status === 'auto_save' ?  `Saved automatically`: e?.status}
																</Typography>
															</Grid>
														</Grid>
													</Grid>
												);
											})}
										</Grid>
									</Grid>
								</Grid>
								{error ? (
									<Grid item xs={12} className={classes.error}>
										{' '}
										<Error errorMessage={error} />
									</Grid>
								) : null}
							</Grid>
						</form>
					</Paper>
				</Fade>
			</Modal>
		</Grid>
	);
}

HistoryModalRecordForm.propTypes = {
	title: PropTypes.string,
	children: PropTypes.func,
	handleSubmit: PropTypes.func,
	error: PropTypes.string,
	pristine: PropTypes.bool,
	submitting: PropTypes.bool,
	fields: PropTypes.array,
};

export default reduxForm({
	form: 'historyModalRecord',
	enableReinitialize: true,
	touchOnChange: true,
	destroyOnUnmount: false,
	forceUnregisterOnUnmount: true,
})(HistoryModalRecordForm);


/**
 * 
 * Response Form
 * 
 */

import React, { useEffect } from 'react';
import Styles from './styles';
import { Field, reduxForm } from 'redux-form';
import validate from 'utils/validation';
import ResponseBox from '../ResponseBox';
import { Checkbox, FormControlLabel, Grid, Typography } from '@material-ui/core';
import lodash from 'lodash';
import { compose } from 'redux';
import { connect } from 'react-redux';
import ObjectionBox from '../ObjectionBox';
 
const FieldBox = ({ input, meta: { touched, error }, recordQuestions, record, columns, classes, translate, mergeTranslate, showLanguage, change, formValues, handleSubGroup, groupedQuestion, copyToLawyerResponse, documentType }) => {
    const { legalColumns } = columns;
    let inputValue = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];
    let requestValue =  formValues && formValues.document_ids &&  Array.isArray(formValues.document_ids) && formValues.document_ids.length > 0 && formValues.document_ids || [];
    const questions = recordQuestions;

    const filterRecordQuestions = record.questions?.filter(e => e?.duplicate_set_no <= 1); 

    const handleChange = (val, checked) => {
        if (val === "all" && checked) {
            
            inputValue = filterRecordQuestions.reduce((a, el) => {
                a.push(el.id);
                return a;
            }, []);
            
        } else if (val === "all" && !checked) {
            inputValue = [];
            change('document_ids',[])
        } else if (checked) {
            val.split(',').map(x => {
                if (!inputValue.includes(x))
                    inputValue.push(x);
            });
        } else {
            inputValue = inputValue.filter(a => !val.split(',').includes(a))
            requestValue = requestValue.filter(a => !val.split(',').includes(a))
            change('document_ids',requestValue)
        }
 
        input.onChange(inputValue);
    }
 
    const handleRequestPhoto = (val, checked) => {
        if (checked) {
            val.split(',').map(x => {
                if (!requestValue.includes(x))
                    requestValue.push(x);
            });
        } 
        else {
            requestValue = requestValue.filter(a => !val.split(',').includes(a))
        }
        change('document_ids',requestValue)  
    }
 
    return (
        <Grid container>
            <Grid container item xs spacing={3} className={classes.section}>
                <Grid item xs={12} style={{ paddingTop: '0px', paddingBottom: '0px', paddingLeft: '7px' }}>
                    <Grid container direction="row" alignItems="center">
                        <FormControlLabel
                            control={<Checkbox
                                style={{ color: "#2ca01c", paddingTop: "5px" }}
                                checked={inputValue.length == filterRecordQuestions.length ? true : false}
                                onChange={(e) => handleChange("all", e.target.checked)} />}
                        />
                        <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                            Select All
                        </Typography>
                    </Grid>
                </Grid>
                {(Object.keys(questions) || []).map((key) => {
                    let consultation = questions[key] && questions[key].length > 0 && questions[key][0]['is_consultation_set'] || false;
                    let consultationSet = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['consultation_set_no'] && lodash.groupBy(questions[key], 'consultation_set_no') || [];

                    const category = questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['question_category'] || false;
                    const questionCategory = documentType && documentType === 'odd' && category || false;

                    return (<Grid item xs={12}>
                        {questionCategory ? <Typography style={{ fontWeight: 'bold', paddingLeft: '41px', textDecoration: 'underline', fontSize: '16px', textTransform: 'capitalize' }}>Category - {questions[key] && questions[key].length > 0 && questions[key][0] && questions[key][0]['question_category'].toLowerCase()}</Typography> : null}
                        <Grid container direction="column">
                            {questions[key] && questions[key].length > 0 && questions[key][0]['question_section_text'] ? <Grid style={{ width: '100%' }}>
                                <Grid container direction="row" alignItems="center">
                                    <FormControlLabel className={classes.checkboxField}
                                        control={<Checkbox
                                            style={{ color: "#2ca01c", padding: '4px', paddingTop: "2px" }}
                                            checked={questions[key].every(f => inputValue.includes(f.id)) ? true : false}
                                            onChange={(e) => handleChange(questions[key].map(f => f.id).join(','), e.target.checked)} />}
                                    />
                                    <Grid container item xs>
                                        <Typography variant="subtitle2">
                                            <b>{questions[key][0]['question_number_text']}. {questions[key][0]['document_type'] == 'FROGS' ? (questions[key][0]['question_text']).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : questions[key][0]['question_text']}</b>
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid> : null}
                            {/* {questions[key] && questions[key].length > 1 ? <Grid style={{ width: '100%' }}>
                                <Grid container className={classes.form}>
                                    <Grid container item xs spacing={2} style={{ marginLeft: '30px' }}>
                                        <ObjectionBox
                                            disableForm={true}
                                            fields={questions[key] && groupedQuestion[key] && questions[key].length > 0 && groupedQuestion[key].length > 0 && groupedQuestion[key][0]['question_section_text'] && groupedQuestion[key].find(_ => _.subgroup) ? objectionColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !questions?.[key][0].subgroup })) : objectionColumns.filter(_ => _.viewRecord)}
                                            record={Object.assign({}, questions?.[key][0], { subgroupIds: questions?.[key]?.map(_ => _.id)})} />
                                    </Grid>
                                </Grid>
                            </Grid>: null} */}
                            <Grid style={{ width: '100%' }}>
                                {!consultation && (questions[key] && questions[key].length > 0 ? (questions[key] || []).map((form, index) => (
                                    !(form && form?.is_duplicate && form?.duplicate_set_no > 1) ? <Grid key={index} container className={classes.form}>
                                        <FormControlLabel className={classes.checkboxField}
                                            control={<Checkbox
                                                style={{ color: "#2ca01c", padding: '4px', paddingTop: "2px" }}
                                                checked={inputValue.includes(form.id) ? true : false}
                                                onChange={(e) => handleChange(form.id, e.target.checked)} />}
                                        />
                                        <Grid container item xs spacing={2}>
                                            <ResponseBox
                                                showLanguage={showLanguage}
                                                disableForm={true}
                                                translate={translate}
                                                mergeTranslate={mergeTranslate}
                                                questionNumber={index + 1}
                                                fields={questions[key] && groupedQuestion[key] && questions[key].length > 0 && groupedQuestion[key].length > 0 && groupedQuestion[key][0]['question_section_text'] && groupedQuestion[key].find(_ => _.subgroup) ? legalColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !form.subgroup })) : legalColumns.filter(_ => _.viewRecord)}
                                                showCheckbox
                                                record={{ ...form }}
                                                handleSubGroup={handleSubGroup}
                                                copyToLawyerResponse={copyToLawyerResponse} 
                                                requestPhoto={() => (
                                                    <Grid container style={{ alignItems: 'center', paddingLeft: '0px' }}>  
                                                        <FormControlLabel
                                                            control={<Checkbox 
                                                            style={{ color: "#2ca01c", paddingTop: "2px" }}              
                                                            disabled={inputValue.includes(form.id) ? false : true} 
                                                            checked={inputValue.includes(form.id) && requestValue.includes(form.id) ? true : false}                                    
                                                            onChange={(e)=>handleRequestPhoto(form.id ,e.target.checked)} />}
                                                        />
                                                        <Typography variant="subtitle1" className={classes.uploadDoc}>
                                                            Request files from client
                                                        </Typography>
                                                    </Grid>
                                                )}
                                                />
                                        </Grid>
                                    </Grid>: null
                                )) : null)}
                                {consultation && Object.keys(consultationSet || []).map((keySet, i) => {
                                    return (<Grid style={{ paddingTop: '10px' }}>{consultationSet[keySet] && consultationSet[keySet].length > 0 ? <Grid style={{ width: '100%' }}>
                                            <Grid container direction="row" alignItems="center">
                                                <FormControlLabel className={classes.checkboxField}
                                                    control={<Checkbox
                                                        style={{ color: "#2ca01c", padding: '4px', paddingTop: "2px" }}
                                                        checked={consultationSet[keySet].every(f => inputValue.includes(f.id)) ? true : false}
                                                        onChange={(e) => handleChange(consultationSet[keySet].map(f => f.id).join(','), e.target.checked)} />}
                                                />
                                                <Grid container item xs>
                                                    <Typography variant="subtitle2">
                                                        <b>{consultationSet[keySet][0]['document_type'] === 'IDC' ? `Person ` : `Consultation `}{consultationSet[keySet][0]['consultation_set_no']}</b>
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid> : null}
                                        {(consultationSet[keySet] || []).map((res, index) => (
                                            <Grid key={index} container className={classes.form}>
                                                <FormControlLabel className={classes.checkboxField}
                                                    control={<Checkbox 
                                                        style={{ color: "#2ca01c", padding: '4px', paddingTop: "2px" }}
                                                        checked={inputValue.includes(res.id) ? true : false}
                                                        onChange={(e) => handleChange(res.id, e.target.checked)} />}
                                                />
                                                <Grid container item xs spacing={2}>
                                                    <ResponseBox
                                                        showLanguage={showLanguage}
                                                        consultationQuestions={consultationSet[keySet]}
                                                        disableForm={true}
                                                        translate={translate}
                                                        mergeTranslate={mergeTranslate}
                                                        questionNumber={index + 1}
                                                        fields={consultationSet[keySet] && consultationSet[keySet].length > 0 && consultationSet[keySet].length > 0 && consultationSet[keySet][0]['question_section_text'] && consultationSet[keySet].find(_ => _.subgroup) ? legalColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !res.subgroup })) : legalColumns.filter(_ => _.viewRecord)}
                                                        showCheckbox
                                                        record={{ ...res }}
                                                        handleSubGroup={handleSubGroup} 
                                                        copyToLawyerResponse={copyToLawyerResponse} 
                                                        requestPhoto={() => (
                                                            <Grid container style={{ alignItems: 'center', paddingLeft: '0px' }}>  
                                                                <FormControlLabel
                                                                    control={<Checkbox        
                                                                    style={{ color: "#2ca01c", paddingTop: "2px" }}              
                                                                    disabled={inputValue.includes(res.id) ? false : true} 
                                                                    checked={inputValue.includes(res.id) && requestValue.includes(res.id) ? true : false}                                    
                                                                    onChange={(e)=>handleRequestPhoto(res.id ,e.target.checked)} />}
                                                                />
                                                                <Typography variant="subtitle1" className={classes.uploadDoc}>
                                                                    Request files from client
                                                                </Typography>
                                                            </Grid>
                                                        )}
                                                        />
                                                </Grid>
                                            </Grid>
                                        ))}
                                    </Grid>)
                                }) || null}
                            </Grid>
                        </Grid>
                    </Grid>)
                })}
            </Grid>
        </Grid>
    )
}
 
 
 
function ResponseForm(props) {
 
    const { handleSubmit, questions, record, columns, setRef, selectedQuestions, destroy, translate, mergeTranslate, change, formValues, handleSubGroup, groupedQuestion, copyToLawyerResponse, documentType } = props;
 
    const classes = Styles();
 
    useEffect(() => {
        setRef(props);
    }, []);
 
    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate >
            <Field
                name="selectedQuestions"
                className={classes.chkBox}
                recordQuestions={questions}
                groupedQuestion={groupedQuestion}
                record={record}
                classes={classes}
                columns={columns}
                component={FieldBox}
                translate={translate}
                mergeTranslate={mergeTranslate}
                type="text"
                onChange={selectedQuestions}
                handleSubGroup={handleSubGroup}
                copyToLawyerResponse={copyToLawyerResponse}
                change={change}
                formValues={formValues}
                documentType={documentType} />
        </form>
    )
}
 
function mapStateToProps({ form }, props) {
    const formValues = form[props.form] && form[props.form].values || false;
 
    return {
        formValues
    }
}
 
 
export default compose(
    connect(mapStateToProps),
    reduxForm({
    form: 'ResponseForm',
    validate,
    touchOnChange: true,
    destroyOnUnmount: false,
}))(ResponseForm);
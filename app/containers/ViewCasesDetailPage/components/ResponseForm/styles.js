

import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({

  fieldColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '100px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    borderBottom: '2px solid #2ca01c',
    cursor: 'pointer'
  },
  blackColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '100px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    borderBottom: '2px solid black',
    cursor: 'pointer'
  },
  blueColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '100px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    borderBottom: '2px solid #3275BF',
    cursor: 'pointer'
  },


  fieldColorForm: {
    fontSize: '10px',
    '& :after': {
      borderBottom: '2px solid #2ca01c',
    },
    '& :before': {
      borderBottom: '2px solid #2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }

  },

  blackColorForm: {
    fontSize: '10px',
    '& :after': {
      borderBottom: '2px solid black',
    },
    '& :before': {
      borderBottom: '2px solid black',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: 'black',
    },
    '&.Mui-focused fieldset': {
      borderColor: 'black',
    }
  },

  blueColorForm: {
    fontSize: '10px',
    '& :after': {
      borderBottom: '2px solid #3275BF',
    },
    '& :before': {
      borderBottom: '2px solid #3275BF',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#3275BF',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#3275BF',
    }
  },

  textSize: {
    fontSize: '14px',
    padding: 0,
  },
  responseType: {
    fontSize: '12px',
    marginBottom: '4px',
  },
  responseField: {
    paddingTop: '0px',
  },
  btn: {
    fontSize: '12px',
    textTransform: 'none',
    padding: '4px 8px',
    margin: '6px 6px',
    fontWeight: 'bold',
  },
  btnGrid: {
    marginTop: '6px',
    marginBottom: '6px',
  },
  chkBox: {
    display: 'inline',
    color: "#2ca01c",
    marginTop: '-10px',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4)
  },
  button: {
    fontWeight: 'bold',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular'
  },
  section: {
    margin: '0px'
  },
  form: {
    marginTop: '10px'
  },
  checkboxField: {
    display: 'inline'
  },
  uploadDoc: {
    alignItems: 'center',
    fontSize: '14px',
    fontWeight: 'bold',
    paddingTop: '0px',
  }
}));


export default useStyles;
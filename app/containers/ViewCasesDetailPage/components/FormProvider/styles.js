

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  btn: {
    fontSize: '12px',
    textTransform: 'none',
    padding: '4px 8px',
    margin: '6px 6px',
    fontWeight: 'bold',
  },
  btnGrid: {
    marginTop: '6px',
    marginBottom: '6px',
  }
}));


export default useStyles;
/***
 * 
 * Form Provider
 * 
 */

import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Grid, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import ObjectionsModal from '../ObjectionsModal';
import Styles from './styles';
import TextEditorField from 'components/TextEditorField';
import { compose } from 'redux';
import { connect } from 'react-redux';
import AlertDialog from 'components/AlertDialog';
class FormProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ObjectionsText: null,
            prevSelection: [],
            lawyerStatus: [],
            showCustomLink: false
        }
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleObjections = this.handleObjections.bind(this);
        this.handleAlertConfirm = this.handleAlertConfirm.bind(this);
        this.myRef = React.createRef();
        this.containerRef = React.createRef();
        this.containerBtnRef = React.createRef();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if((nextProps?.showLink && !nextState.showCustomLink)) {
            this.setState({ showCustomLink: true });
            nextProps.setChildRef({
                ref: this.myRef, 
                containerRef: this.containerRef,
                containerBtnRef: this.containerBtnRef,
                setState: () => {
                    this.setState({ showCustomLink: false });
                }
            });
            return true;
        }
       
        if(nextProps.submitting || (this.props.submitFailed && this.props.error) || this.props.showForm) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        const { id, field, className, submitting, metaData = {}, state, federal, pageName, filterColumns, user, actions, dispatch, record, responseHistoryLimit, attorneyResponseTracking, formName, showForm, setShowForm, setOpenForm, destroy, objectionForm, linkComponent, showLink, formValues  } = this.props;
        const global_attorney_response_tracking = user?.practiceDetails?.global_attorney_response_tracking || false;
        const responseHistory = record?.responseHistory?.length >= responseHistoryLimit && global_attorney_response_tracking && attorneyResponseTracking ? true : false;
        const versionHistoryMessage = 'The version history limit has been exceeded. Saving a new version will result in the deletion of the oldest version from the queue.';
        let lawyer_response_text = formValues && formValues?.lawyer_response_text && formValues?.lawyer_response_text?.toLowerCase();

        const handleInputChanges = (val) => {
            this.setState({ ObjectionsText: val });
            const autoSaveForm = formName === 'objectionForm' ? 'autoSaveObjectionForm' : 'autoSaveForm';
            dispatch(actions.autoSaveFormDetails(Object.assign({}, { [autoSaveForm]: `${formName}_${id}` })));
        }

        return (<form onSubmit={(e) => e.preventDefault()}>
            {!showForm ? <>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Field 
                            name={field.value} 
                            type={field.type} 
                            className={className} 
                            metaData={metaData} 
                            component={TextEditorField} 
                            onChange={(val) => handleInputChanges(val)} 
                            onInputChange={(val) => handleInputChanges(val)} 
                            placeholder={`Enter the attorney response here (upto 50000 characters)`} 
                            errorText={`You have reached the limit of 50000 characters`}
                            maxLength={field.charLimit}
                            memo />
                    </Grid>
                </Grid>
                <Grid container xs={12} ref={this.containerRef} direction="row" justify={showLink ? "space-between" : "flex-end"} alignItems="flex-start" className='btnGrid' style={{ margin: 0, marginTop: '6px', marginBottom: '6px' }}>
                    {showLink ? <Grid item xs={6} ref={this.myRef} style={{ padding: 0 }}>
                        {linkComponent && typeof linkComponent === 'function' && React.createElement(linkComponent, Object.assign({}, { field, lawyer_response_text, state, document_type: record?.document_type }))}
                    </Grid> : null}
                    <Grid item xs={12} ref={this.containerBtnRef} style={{ display: 'flex', justifyContent: 'flex-end', flexWrap: 'wrap', padding: 0, marginRight: '-6px' }}>
                        <ObjectionsModal
                            title="Objections"
                            form={objectionForm ? `ObjectionsModalForm1_${id}` : `ObjectionsModalForm_${id}`}
                            handleObjections={this.handleObjections}
                            metaData={metaData}
                            state={state}
                            federal={federal}
                            filterColumns={filterColumns}
                            pageName={pageName}
                            user={user}
                            style={{ width: 'auto' }}>
                            {(open) => <Button className='btn' type="button" variant="contained" color="primary" onClick={open} style={{ fontSize: '12px', textTransform: 'none', padding: '4px 8px', margin: '6px 6px', fontWeight: 'bold' }}>Objections</Button>}
                        </ObjectionsModal>
                        <AlertDialog
                            description={versionHistoryMessage}
                            onConfirm={() => this.handleAlertConfirm('Draft')}
                            onConfirmPopUpClose={true}
                            btnLabel1='OK'>
                            {(open) => <Button className='btn' 
                            type="button" 
                            variant="contained" 
                            color="primary" 
                            onClick={responseHistory ? open : () => this.handleAlertConfirm('Draft')} 
                            style={{ fontSize: '12px', textTransform: 'none', padding: '4px 8px', margin: '6px 6px', fontWeight: 'bold' }}>
                                {(submitting && this.state.lawyerStatus && this.state.lawyerStatus === 'Draft') && <ButtonSpinner size={21} /> || 'Save as draft'}
                            </Button>}
                        </AlertDialog>
                        <AlertDialog
                            description={versionHistoryMessage}
                            onConfirm={() => this.handleAlertConfirm('Final')}
                            onConfirmPopUpClose={true}
                            btnLabel1='OK'>
                            {(open) => <Button className='btn' 
                            type="button" 
                            variant="contained" 
                            color="primary" 
                            onClick={responseHistory ? open :  () => this.handleAlertConfirm('Final')} 
                            style={{ fontSize: '12px', textTransform: 'none', padding: '4px 8px', margin: '6px 6px', fontWeight: 'bold' }}>
                                {(submitting && this.state.lawyerStatus && this.state.lawyerStatus === 'Final') && <ButtonSpinner size={21}/> || 'Save as final'}
                            </Button>}
                        </AlertDialog>
                    </Grid>
                    
                </Grid>
            </> : null}
            <ObjectionsModal
                title="Objections (Common to all sub-questions)"
                form={objectionForm ? `ObjectionsModalForm1_${id}` : `ObjectionsModalForm_${id}`}
                handleObjections={this.handleObjections}
                metaData={metaData}
                show={showForm}
                onClose={() => setShowForm(false)}
                setOpenForm={setOpenForm}
                state={state}
                federal={federal}
                filterColumns={filterColumns}
                pageName={pageName}
                isDestoryForm={true}
                formDestroy={destroy}
                user={user}
                style={{ width: 'auto' }} />
        </form>)
    }

    handleUpdate(status) {
        const { change, handleSubmit, fieldState } = this.props;
        this.setState({ lawyerStatus: status });
        change(fieldState, status);
        setTimeout(() => {
            handleSubmit();
        }, 1000);
    }


    handleAlertConfirm(type) {
        if(type === 'Final') {
            this.handleUpdate('Final');
        } else if (type === 'Draft') {
            this.handleUpdate('Draft');
        }
    }

    handleObjections(data) {
        
        const { metaData, record, change, user = {}, field, formValues, fieldValue, setShowForm } = this.props;
        const { customerObjections = [] } = metaData;
        const { practiceDetails = {} } = user;
        const htmlRegExp = /<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g;
        if(setShowForm) {
            setShowForm(false);
        }

        let parser = new DOMParser();
         // fieldValue - lawyer_response_text, lawyer_objection_text
        let newObjectionText = this.state.ObjectionsText || formValues && formValues?.[fieldValue];

        const currSelection = customerObjections.filter(_ => data.includes(_.id));
        const newSelection = customerObjections.filter(_ => data.includes(_.id) && !this.state.prevSelection.find(p => p.id === _.id)) || [];

        let selected = newSelection && newSelection.map(_ => practiceDetails && practiceDetails.objections ? _.objection_title + ": " + _.objection_text : _.objection_text);
        let objectionTitle = newSelection && newSelection.map(_ => practiceDetails && practiceDetails.objections ? _.objection_title : false);

        const paragraph = document.createElement('p');
        paragraph.innerHTML = '&nbsp;';


        selected = selected.map(item => {
            if (/<(?=.? .?\/ ?>|br|hr|input|!--|wbr)[a-z]+.*?>|<([a-z]+).*?<\/\1>/i.test(item)) {
                const doc = parser.parseFromString(item, 'text/html');
                const ptag = doc.getElementsByTagName('p');

                if (ptag && ptag.length > 1) {
                    const itemContent = doc.body['innerHTML'].split('\n');
                    item = itemContent.map((el, i) => {
                        let text = el.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                        if (practiceDetails && practiceDetails.objections && objectionTitle && text.includes(`${objectionTitle}: `)) {
                            var res = text.replace(`${objectionTitle}: `, '');
                            return `<p>${objectionTitle}: ${res}</p>`;
                        } else if (text === '&nbsp;') {
                            return `<p class="space">${paragraph.innerText}</p>`;
                        } else {
                            return `<p>${text}</p>`;
                        }
                    }).join("");
                    item = `<div>${item}</div>`;
                } else {
                    item = item.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                    item = `<p>${item}</p>`;
                }
                item = `${item}<br/>`;
            } else {
                item = `<p>${item}</p><br/>`;
            }
            return item
        }).reduce((a, el, index) => {
            if (index !== (selected.length - 1)) {
                a.push(el);
            } else {
                el = !newObjectionText && el.lastIndexOf('<br/>') ? el.split('<br/>').filter(x => x).map(item => {
                    a.push(item);
                }) : a.push(el);
            }
            return a;
        }, []);


        if (this.state.prevSelection && this.state.prevSelection.length > 0 && customerObjections.filter(el => !data.includes(el.id) && this.state.prevSelection.find(p => p.id === el.id)).length > 0) {

            function convertHTMLEntity(text) {
                const span = document.createElement('span');
                return text.replaceAll(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
                    span.innerHTML = entity;
                    return span.innerText;
                });
            }

            newObjectionText = convertHTMLEntity(newObjectionText);

            customerObjections.filter(el => !data.includes(el.id) && this.state.prevSelection.find(p => p.id === el.id)).map(_ => {

                if (/<(?=.? .?\/ ?>|br|hr|input|!--|wbr)[a-z]+.*?>|<([a-z]+).*?<\/\1>/i.test(_.objection_text)) {
                    let objectionText = _.objection_text.trim();
                    let objectionContent = practiceDetails && practiceDetails.objections ? _.objection_title + ": " + objectionText : objectionText;

                    let objectionParser = parser.parseFromString(objectionContent, 'text/html');
                    const ptag = objectionParser.getElementsByTagName('p');
                    if (ptag && ptag.length > 1) {
                        const content = objectionParser.body['innerHTML'].split('\n');
                        objectionContent = content.map((el, i) => {
                            let text = el.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            if (practiceDetails && practiceDetails.objections && _.objection_title && text.includes(`${_.objection_title}: `)) {
                                var res = text.replace(`${_.objection_title}: `, '');
                                return `<p>${_.objection_title}: ${res}</p>`;
                            }
                            else if (text === '&nbsp;') {
                                // const content_text = objectionParser.body['innerText'].split('\n');
                                // let innerText = content_text.map((m) => m)[i];
                                return `<p class="space">${paragraph.innerText}</p>`;
                            }
                            else {
                                return `<p>${text}</p>`;
                            }
                        }).join("\n");
                        objectionContent = `\n${objectionContent}\n`;
                    } else {
                        objectionContent = objectionContent.trim().replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                        objectionContent = `<p>${objectionContent}</p>`;
                    }
                    newObjectionText = newObjectionText.replaceAll(`<br />`, `<p>${paragraph.innerText}</p>`);
                    objectionContent = convertHTMLEntity(objectionContent);

                    newObjectionText = newObjectionText.replaceAll(objectionContent, '');
                } else {
                    let objectionText = _.objection_text.trim();
                    newObjectionText = newObjectionText.replaceAll(practiceDetails && practiceDetails.objections ? `<p>${_.objection_title}: ${objectionText}</p>` : `<p>${objectionText}</p>`, '');
                }

            });

            newObjectionText = newObjectionText.replaceAll(`<p>${paragraph.innerText}</p>`, '<p>&nbsp;</p>');
            const splitObjection = newObjectionText.split('<p>&nbsp;</p>');

            newObjectionText = splitObjection.map(item => {
                if (item.includes('<div>') || item.includes('</div>')) {
                    let div = item;

                    if ((div.includes('<div>\n<p>') || div.includes('<div><p>')) && !div.includes('</div>')) {
                        div = div.replaceAll('<div></div>', '');
                        div = div.replaceAll('<br/>', '');
                        div = div.replaceAll('<p></p>', '');
                        div = `${div}<br/>`;
                    } else {
                        div = div.replaceAll('<div></div>', '');
                        div = div.replaceAll('<br/>', '');
                        div = div.replaceAll('</div>', '</div><br/>');
                    }
                    return div;
                } else {
                    let res = item;
                    res = res.replaceAll('<p></p>', '');
                    res = res.replaceAll('<br/>', '');
                    res = res.replaceAll('</p>', '</p><br/>');
                    return res;
                }
            }).join("");

            newObjectionText = newObjectionText.replaceAll('<br></div>', '</div>');
            newObjectionText = newObjectionText.replaceAll('<br/></div>', '</div>');
            let str = newObjectionText.lastIndexOf('<br/>');
            newObjectionText = newObjectionText.slice(0, str);

        }
        //character limit changes start
        let textContent = newObjectionText;
        textContent = textContent && textContent.replace(htmlRegExp, '') || 0;
        textContent = textContent && textContent !== 0 && textContent.replaceAll('\n', '').length;

        const selectedObjections = selected && selected.join('\n\n');
        const objectionsLength = selectedObjections && selectedObjections.replace(htmlRegExp, '').length;
        const recordLength = textContent + objectionsLength;
        // character limit changes end

        if (field.charLimit >= recordLength) {
            let finalText = newObjectionText ? selected.join('\n\n') + '\n\n' + newObjectionText : selected.join('\n\n');
            finalText = finalText.trim().replace('\n\n\n\n', '\n\n');
            change(fieldValue, finalText.replace('\n\n\n', '\n\n'));
            this.setState({ ObjectionsText: finalText.replace('\n\n\n', '\n\n') });
            this.setState({ prevSelection: currSelection });
        }
    }
}

function mapStateToProps({ form }, props) {
    const formValues = form[props.form] && form[props.form].values || false;
    return {
        formValues
    }
}

export default compose(
    connect(mapStateToProps),
    reduxForm({
        form: 'casesDetailRecord',
        enableReinitialize: true,
        touchOnChange: true,
        destroyOnUnmount: false
}))(FormProvider);
/**
 * 
 * Objection Box 
 * 
 */

import React, { useState } from 'react';
import { Grid, Typography } from '@material-ui/core';
import FormProvider from '../FormProvider';
import ReactHtmlParser from 'react-html-parser';
import Styles from './styles';
import ButtonSpinner from 'components/ButtonSpinner';
import AlertDialog from 'components/AlertDialog';
import { handleClass } from '../../utils';
import { customMessage } from '../../messages';


function ObjectionBox(props) {

    const { record, fields, disableForm, actions, user, style, forms = {}, state, federal, dispatch, metaData, filterColumns, name, responseHistoryLimit, attorneyResponseTracking, customQuestions } = props;
    const classes = Styles();

    const { id } = record;

    const objectionFormRecord = forms[`objectionForm_${id}`] && forms[`objectionForm_${id}`].values || false;

    const [openForm, setOpenForm] = useState(false);
    const [showForm, setShowForm] = useState(false);
    const [loader, setLoader] = useState(false);
    const [showObjectAlert, setShowObjectAlert] = useState(false);

    const handleSubmit = (data, dispatch, { form }) => {
        customQuestions ?  dispatch(actions.updateFrogsObjectionForm(data, form, setOpenForm)) : dispatch(actions.updateObjectionForm(data, form, setOpenForm))
    }

    const handleDeleteObjection = (data) => {
        setLoader(true);
        const submitRecord = Object.assign({}, data, { lawyer_objection_text: '', lawyer_objection_status: 'NotStarted' });
        if(customQuestions) {
            dispatch(actions.deleteAttorneyFrogsObjection(submitRecord, setOpenForm, setLoader))
        } else {
            dispatch(actions.deleteAttorneyObjection(submitRecord, setOpenForm, setLoader))
        }
    }


    const handleResponseBox = (field) => {
        if (field.editRecord && !field.disabled) {
            setOpenForm(true);
            dispatch(actions.autoSaveForm(record, ['objectionForm_']));
            if (!record?.lawyer_objection_text) {
                setShowForm(true);
            }
        }
    }

    
    return (
        <Grid container style={style}>
            <Grid container item xs spacing={3} className={classes.section}>
                {(fields || []).map((field, index) => (
                    <Grid key={index} item xs={12} md={6} className={classes.field} style={record && !record[field.value] ? { paddingTop: 0, paddingBottom: 0 } : {}}>
                        {(record && !record[field.value] && !objectionFormRecord ? true :  false) ? <Grid item>
                            <Typography variant="subtitle2" className={classes.responseType} style={{ color: "#2ca01c", textDecoration: "underline", cursor: 'pointer', marginBottom: "5px", display: "inline" }} onClick={() => handleResponseBox(field)}>Add Objections (Common to all sub-questions)</Typography>
                        </Grid> : null}
                        {record && record[field.value] ? <Grid container direction="row" justify="space-between" alignItems="flex-end">
                            <Grid>
                                <Grid container alignItems="center">
                                    <Grid item style={{ marginRight: "12px" }}>
                                        <Typography variant="subtitle2" className={classes.responseType}>
                                            <b>{field.label}</b>
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            {record && record[field.value] && <Grid item onClick={() => setShowObjectAlert(true)}>
                                <Typography variant="subtitle2" className={classes.responseType} style={{ color: "#2ca01c", cursor: 'pointer' }}>
                                    {loader ? <ButtonSpinner color='#2ca01c' size={20} /> : 'Delete'}
                                </Typography>
                            </Grid>}
                        </Grid > : null}

                        <AlertDialog
                            description={customMessage?.deleteObjection}
                            openDialog={showObjectAlert}
                            closeDialog={() => setShowObjectAlert(false)}
                            onConfirm={() => handleDeleteObjection(record)}
                            onConfirmPopUpClose
                            btnLabel1='Yes'
                            btnLabel2='No' />

                        {(openForm || (objectionFormRecord && objectionFormRecord.case_id && objectionFormRecord.copyNote !== true)) && field.editRecord && !disableForm ? <Grid>
                            <FormProvider
                                id={id}
                                initialValues={objectionFormRecord && objectionFormRecord.case_id ? objectionFormRecord : record || {}}
                                form={`objectionForm_${id}`}
                                field={field}
                                className={handleClass(Object.assign({}, { status : record[field.status], form: 'form', classes }))}
                                onSubmit={handleSubmit}
                                user={user}
                                state={state}
                                federal={federal}
                                pageName={name}
                                filterColumns={filterColumns}
                                actions={actions}
                                dispatch={dispatch}
                                attorneyResponseTracking={attorneyResponseTracking}
                                responseHistoryLimit={responseHistoryLimit}
                                fieldValue="lawyer_objection_text"
                                formName="objectionForm"
                                fieldState="lawyer_objection_status"
                                showForm={showForm}
                                setShowForm={setShowForm}
                                setOpenForm={setOpenForm}
                                objectionForm={true}
                                {...props}
                            />
                        </Grid> : record && record[field.value] ?
                            <Grid className={handleClass(Object.assign({}, { status : record[field.status], form: false, value: field.value, classes }))} onClick={() => handleResponseBox(field)}>
                                {<span className={classes.responseText}>{record && record[field.value] && ReactHtmlParser(record[field.value])}</span>}
                            </Grid> : null}
                    </Grid>
                ))}
            </Grid>
        </Grid >
    )
}


export default ObjectionBox;

/**
 * 
 * Document Button
 * 
 */

import React, { useState } from 'react';
import ModalForm from 'components/ModalRecordForm';
import { Grid, Button, Typography} from '@material-ui/core';
import AlertDialog from 'components/AlertDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import { getState } from 'containers/ViewCasesPage/utils';
import SVG from 'react-inlinesvg';
import SectionModalForm from 'components/SectionModalRecordForm';
import schema from 'routes/schema';
import { planConfirmationMessage } from 'utils/tools';
import ModalPricingDialog from '../PricingModalDialog';
import Icons from '../PricingModalDialog/utils';
import { attorneySignatureList } from '../../utils';

export const SubscriptionSubmitButton = ({ classes, loading, openAlertDialog, submitting, handleSubmitForm, cardDetails, oneTimeActivationFee, invalid, pristine, enableSubmitBtn, formValues, form, confirmationMessage }) => {

    return <AlertDialog
        description={confirmationMessage}
        onConfirm={handleSubmitForm}
        btnLabel1='Yes'
        btnLabel2='No'
        onConfirmPopUpClose >
        {(openDialog) => {
            return <Grid style={{ marginRight: '12px' }}>
                <Button type="button" fullWidth variant="contained" color="primary" className={classes.button} onClick={() => openAlertDialog(Object.assign({}, { dialog: openDialog, submitRecord: formValues, form, cardDetails, oneTimeActivationFee, handleSubmitForm }))} disabled={!enableSubmitBtn && (pristine || invalid)}>
                {(submitting || loading) && <ButtonSpinner /> || 'Subscribe'}
            </Button>
            </Grid> 
            
        }}
    </AlertDialog>
};

export default ({ columns, value, classes, updateBilling, billing, record, label, loading, generateDocument, openDocument, type, title, document, setDocument, style, sm, closeModal, validate, descriptionMessage, metaData, customTemplateAndSignature, handleCustomTemplateSignature, modifiedCustomTemplateOptions, customModifiedTemplate, handleModifiedTemplate, selectQuestionTemplateColumns, forms, user, validSubscription, userRecordFormColumns, handleUpdateUserRecord, subscriptionDetailsColumns, handleUpdatePlanDetails, freeTrialMessage, discountDetails, openAlertDialog, upShellColumns, finalDocumentRef }) => {
    const [planType, setPlanType] = useState(false);
    const showCustomTemplate = type && type != 'pos' && customModifiedTemplate && Array.isArray(customModifiedTemplate) && customModifiedTemplate.length > 1 || false;
    const selectTemplate = record && record.state && record.state === 'GA';
    const userRole = user && user?.role;
    const { attorneySignature } = metaData && record && attorneySignatureList({ metaData, record });
    const showSignatureModal = userRole && userRole == 'paralegal' ? type && type != 'pos' ? attorneySignature && attorneySignature?.length > 1 || showCustomTemplate : type && type == 'pos' ? true : false : userRole && userRole == 'lawyer';

    const showSignatureSection = userRole && userRole == 'paralegal' ? type && type != 'pos' ? attorneySignature && attorneySignature?.length > 1 : type && type == 'pos' ? true : false : userRole && userRole == 'lawyer';

    const customTemplateAndSignatureColumns = schema().customTemplateSignature(user, customModifiedTemplate, validSubscription, forms, type, selectTemplate, Object.assign({ caseRecord: record, metaData, showSignature: showSignatureSection, attorneySignatureOptions: attorneySignature })).section;
    const userRecordFormFields = userRecordFormColumns && userRecordFormColumns(user).columns;
    const modifiedCustomTemplateColumns = modifiedCustomTemplateOptions(forms, type, selectTemplate);

    let customTemplateAndSignatureColumn = customTemplateAndSignatureColumns && customTemplateAndSignatureColumns.reduce((acc, el) => {
        if(el && el.editForm && el.columns) {
            acc.push(...el.columns)
        }
        return acc;
    }, []);

    const subscriptionFormValues = forms && forms[`subscriptionForm_${type}`] && forms[`subscriptionForm_${type}`].values || false;
    const cardDetails = subscriptionFormValues && subscriptionFormValues.plan_id && subscriptionFormValues.cardDetails && subscriptionFormValues.cardDetails['complete'] || billing.credit_card_details_available || false;
    const plans = metaData && metaData['respondingPlansOptions'] && metaData['respondingPlansOptions'].length > 0 && metaData['respondingPlansOptions'] || [];

    const currentPlan = subscriptionFormValues && plans && Array.isArray(plans) && plans.length > 0 && plans.find(_ => _.value === subscriptionFormValues['plan_id']) || false;
    const oneTimeActivationFee = user && user.practiceDetails && user.practiceDetails.one_time_activation_fee || false;

    const formValue = forms && forms[`respondCustomTemplateAndSignature_${type}`] && forms[`respondCustomTemplateAndSignature_${type}`].values || {};

    const confirmMessage = planConfirmationMessage(currentPlan, discountDetails, oneTimeActivationFee, subscriptionFormValues, 'responding', 'upshell', false, false, { user, metaData }, false);

    const handleSubscription = (setModalOpen, openSubscriptionForm, type) => {
        setPlanType(type);
        setModalOpen(false);
        openSubscriptionForm();
    }
    
    const messageRegular = <span>You have {user?.free_tier_count} free trial documents.
    <br/><br/>Enter the following information as you would like it to appear on your printed document.</span>;

    let selectedPlan = planType && plans?.filter(el => el.plan_type.includes(planType)) || false;
    selectedPlan = selectedPlan?.[0]?.plan_id || false;

    return (
        <ModalForm
            fields={columns.filter(_ => _.editRecord)}
            form={`billingForm_${type}_${value}`}
            className={classes.form}
            footerStyle={{ borderTop: 'none' }}
            style={style}
            btnLabel={`Pay $${billing && billing.price || 0}`}
            message="Free tier usage is complete. Please provide your credit card details to process this order. Card details will be securely saved for future orders as well."
            onSubmitClose
            onSubmitbtn
            onSubmit={updateBilling.bind(this, getState(record, label), type, closeModal)}>
            {(openCardDetailForm) => {
                return <AlertDialog
                    description={descriptionMessage}
                    onConfirm={() => setDocument(false)}
                    onConfirmPopUpClose={true}
                    closeDialog={() => setDocument(false)}
                    btnLabel1='OK' >
                    {(openFreeTrialAlert) => {
                        return <AlertDialog
                            description={`You will be charged $${billing && billing.price || 0} for your order. Do you want to continue?`}
                            onConfirm={() => generateDocument(getState(record, label), type)}
                            onConfirmPopUpClose={true}
                            closeDialog={() => setDocument(false)}
                            btnLabel1='Yes'
                            btnLabel2='No' >
                            {(openDialog) => {
                                return <ModalForm
                                    title={"Select Template"}
                                    fields={selectQuestionTemplateColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                    form={`selectTemplate_${type}`}
                                    metaData={metaData}
                                    onSubmit={(data) => handleModifiedTemplate(Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, freeTrialDialog: openFreeTrialAlert }), data)}
                                    onSubmitClose
                                    btnLabel={title}>
                                    {(openSelectTemplate) => {                                        
                                        const georgiaTemplate = !showCustomTemplate && selectTemplate && openSelectTemplate;
                                        return <ModalForm
                                            title={"Custom Template"}
                                            fields={modifiedCustomTemplateColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                            form={`responderCustomTemplate_${type}`}
                                            onSubmit={(data) => handleModifiedTemplate(Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, freeTrialDialog: openFreeTrialAlert }), data)}
                                            metaData={metaData}
                                            onSubmitClose
                                            btnLabel={title}
                                            paperClassName={classes.modalPaper}>
                                            {(openCustomTemplateForm) => {
                                                return <SectionModalForm
                                                    initialValues={Object.assign({}, { signature: "" })}
                                                    fields={customTemplateAndSignatureColumn.filter(_ => _.editRecord)}
                                                    section={customTemplateAndSignatureColumns && customTemplateAndSignatureColumns.filter((_) => _.editForm)}
                                                    form={`respondCustomTemplateAndSignature_${type}`}
                                                    btnLabel={title}
                                                    metaData={metaData}
                                                    onSubmitClose
                                                    enableSubmitBtn={(!showCustomTemplate && !selectTemplate) ? true : false}
                                                    paperClassName={classes.modalPaper}
                                                    enableScroll={(formValue && formValue.bodyScroll) && showCustomTemplate && selectTemplate ? classes.bodyScroll : false}
                                                    onSubmit={(data) => handleCustomTemplateSignature(Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, freeTrialDialog: openFreeTrialAlert, posModal: closeModal }), data)}>
                                                    {(openSectionForm) => {
                                                        const userRole = user && user?.role === 'lawyer' && type === 'pos';
                                                        const openTemplate =  type && type === 'template' ? showCustomTemplate && openCustomTemplateForm : openSectionForm;
                                                        const subscriptionColumns = subscriptionDetailsColumns && typeof subscriptionDetailsColumns === 'function' && subscriptionDetailsColumns().columns && subscriptionDetailsColumns(record, billing, subscriptionFormValues).columns.filter(_ => _.editRecord && !_.disableRecord)
                                                        return <ModalForm
                                                            initialValues={Object.assign({}, { plan_id: selectedPlan || false })}
                                                            title={'Subscription'}
                                                            fields={subscriptionColumns}
                                                            form={`subscriptionForm_${type}`}
                                                            onSubmit={handleUpdatePlanDetails.bind(this, { openCardDetailForm: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, posModal: closeModal || false, freeTrialDialog: openFreeTrialAlert, openTemplateForm: openTemplate || false, openGeorgiaTemplate: georgiaTemplate })}
                                                            btnLabel="Submit"
                                                            onSubmitClose={true}
                                                            style={{ paddingRight: '0px' }}
                                                            confirmButton={true}
                                                            alertOnSubmitClose
                                                            confirmPopUpClose
                                                            disableSubmitBtn={true}
                                                            metaData={metaData}
                                                            progress={loading}
                                                            paperClassName={classes.modalPaper1}
                                                            footerBtn={({ lg }) => (<Grid container justify={lg ? "flex-start" : "flex-end"} style={lg ? { display: 'flex', alignItems: 'center', position: 'relative', top: '44px' } : { display: 'flex', alignItems: 'center' }}>
                                                                <Icons type={'guarantee'} style={{ width: '53px' }}/>
                                                                <Typography style={{ fontSize: '13px' }}>{'30-Day Money Back Guarantee'}</Typography>
                                                            </Grid>)}
                                                            footerSubmitBtn={(setModalOpen, props) => {
                                                                const { handleSubmit } = props;
                                                                return <SubscriptionSubmitButton
                                                                    classes={classes}
                                                                    handleSubmitForm={() => {
                                                                        handleSubmit();
                                                                        setTimeout(() => {
                                                                            setModalOpen(false);
                                                                        }, 1000);
                                                                    }}
                                                                    {...props}
                                                                    loading={loading}
                                                                    metaData={metaData}
                                                                    formValues={subscriptionFormValues}
                                                                    cardDetails={cardDetails}
                                                                    currentPlan={currentPlan}
                                                                    oneTimeActivationFee={oneTimeActivationFee}
                                                                    discountDetails={discountDetails}
                                                                    openAlertDialog={openAlertDialog}
                                                                    handleSubmit={handleSubmit}
                                                                    confirmationMessage={confirmMessage}
                                                                    enableSubmitBtn
                                                                />
                                                            }}>
                                                            {(openSubscriptionForm) => {
                                                                return <ModalPricingDialog
                                                                    handleSubscription={handleSubscription}
                                                                    openSubscriptionForm={openSubscriptionForm}
                                                                    upShellColumns={upShellColumns}
                                                                    user={user}>
                                                                    {(openPriceAlertForm) => {
                                                                        const updateRecord = (type && type === 'pos') ? Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, posModal: closeModal, freeTrialDialog: openFreeTrialAlert, openTemplateForm: false, subscriptionAlert: openPriceAlertForm }) : Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, freeTrialDialog: openFreeTrialAlert, openTemplateForm: openTemplate, subscriptionAlert: openPriceAlertForm, openGeorgiaTemplate: georgiaTemplate })
                                                                        return <ModalForm
                                                                            messageRegular={messageRegular}
                                                                            fields={userRecordFormFields.filter(_ => _.editRecord && !_.disableRecord)}
                                                                            form={`userRecordForm_${type}`}
                                                                            onSubmit={(data, dispatch, { form }) => handleUpdateUserRecord(updateRecord, data, form)}
                                                                            metaData={metaData}
                                                                            onSubmitClose
                                                                            btnLabel={'Submit'}
                                                                            paperClassName={classes.modalPaper}>
                                                                            {(openRecordForm) => {
                                                                                const handleGenerateBtn = () => {
                                                                                    openDocument(Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, freeTrialDialog: openFreeTrialAlert, openTemplateForm: showSignatureModal ? openTemplate : false, openGeorgiaTemplate: georgiaTemplate, subscriptionAlert: openPriceAlertForm, openRecordForm }));
                                                                                }

                                                                                const handleGeneratePos = () => {
                                                                                    openDocument(Object.assign({}, { form: openCardDetailForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, posModal: closeModal, freeTrialDialog: openFreeTrialAlert, openTemplateForm: !userRole ? openTemplate : false, subscriptionAlert: openPriceAlertForm, openRecordForm }));
                                                                                }

                                                                                if(finalDocumentRef && type === 'final') {
                                                                                    finalDocumentRef.current = ({ openDocument: handleGenerateBtn });
                                                                                }
                                                                                
                                                                                
                                                                                return (type && type === 'pos') ? <Grid item xs={12} className={classes.icon}>
                                                                                    <Button type="button" variant="contained" color="primary" className={classes.posButton} onClick={() => {
                                                                                        if (!validate()) {
                                                                                            handleGeneratePos();
                                                                                        }
                                                                                    }}>
                                                                                        {document && document.value === value && document.type === type && loading && <ButtonSpinner /> || title}
                                                                                    </Button>
                                                                                </Grid> :
                                                                                    <Grid item xs={12} className={classes.icon}>
                                                                                        <Button type="button" variant="contained" color="primary" className={classes.docButton} onClick={() => {
                                                                                            handleGenerateBtn();
                                                                                        }}>
                                                                                            {document && document.value === value && document.type === type && loading && <ButtonSpinner color='#2ca01c' /> || type && <SVG src={require(`images/icons/${type === 'final' ? 'discovery' : 'shell'}.svg`)} style={{ height: 'auto' }} /> || title}
                                                                                        </Button>
                                                                                        {sm && <Grid className={classes.title}>{title}</Grid> || null}
                                                                                    </Grid>
                                                                            }}
                                                                        </ModalForm>
                                                                    }}
                                                                </ModalPricingDialog>
                                                            }}
                                                        </ModalForm>
                                                    }}
                                                </SectionModalForm>
                                            }}
                                        </ModalForm>
                                    }}
                                </ModalForm>
                            }}
                        </AlertDialog>
                    }}
                </AlertDialog>
            }}
        </ModalForm>
    )
}

/***
 * 
 * Input Field
 * 
 */


import React from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';

export default function ({ input, autoFocus, type, className, meta: { touched, error, warning } }) {

    const classes = Styles();
    const { id, name, value, onChange } = input;
    return (
        <div>
            <TextField
                className={className}
                multiline
                name={name}
                id={id}
                type={type}
                rows={6}
                rowsMax={8}
                fullWidth
                onChange={(e) => onChange(e.target.value)}
                value={value || ''}
                variant="filled"
                autoFocus={autoFocus}
                InputProps={{
                    classes: {
                        input: classes.textSize,
                        padding: 0,
                    },

                }}
            />
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>

    )
}
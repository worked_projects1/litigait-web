

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    inputField: {
        background: 'transparent',
        width: '350px',
        fontFamily: 'ClearSans-Light !important',
        '& .MuiInputLabel-outlined > span': {
            fontFamily: 'ClearSans-Light',
            color: '#0a0a0a'
        },
        '& .MuiOutlinedInput-root': {
            height: '45px'
        },
        '& .MuiInputLabel-outlined': {
            marginTop: '-4px'
        },
        '& .Mui-focused > span': {
            marginLeft: '14px',
            fontSize: '16px'
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: '#848485'
        },
        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: '#848485'
        },
        '& .MuiInputBase-root': {
            marginTop: '0px !important'
        }
    }
}));


export default useStyles;
/**
 * 
 * Calender Form
 * 
 */

import React, { useEffect } from 'react';
import { Grid, Button } from '@material-ui/core';
import { Field, reduxForm } from 'redux-form';
import Calendar from 'components/Calendar';
import Styles from './styles';

/**
* 
* @param {object} props 
* @returns 
*/

function CalendarForm(props) {
    const { handleTillDateChange, name, className } = props
    const classes = Styles();
    return (
        <>
            <Grid container direction="column">
                <Field name={name} onChange={handleTillDateChange} component={Calendar} type="text" variant="outlined" className={className} {...props}/>
            </Grid>
        </>
    )
}

export default reduxForm({
    form: 'calendarForm',
    touchOnChange: true,
})(CalendarForm);
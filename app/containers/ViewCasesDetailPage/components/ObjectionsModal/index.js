/**
 * 
 *  Objections Modal
 * 
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import { FormControlLabel, Checkbox } from '@material-ui/core';
import Styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';


const InputComponent = ({ input, classes, filterObjections }) => {

    const InputValue = input && input.value || [];
    const handleChange = (e) => {
        const { name, checked } = e.target;
        if (checked) {
            InputValue.push(name);
        }
        input.onChange(checked ? InputValue : InputValue.filter(k => k != name));
    }

    return (<Grid container spacing={3} className={classes.InputComponent}>
        {filterObjections && filterObjections.length > 0 && filterObjections.map((field, fieldIndex) => <Grid key={fieldIndex} item xs={12} sm={12} md={6} style={{ padding: '4px' }}>
            <FormControlLabel
                name={field.id}
                control={<Checkbox
                    style={{ color: "#2ca01c" }}
                    checked={input.value.includes(field.id) || false}
                    onChange={handleChange} />}
                label={<span>{field.objection_title}</span>} />
        </Grid>) || <Grid item xs={12}>No Objections Found.</Grid>}</Grid>)
}

function ObjectionsModal({ title, children, handleSubmit, className, style, submitting, pristine, invalid, handleObjections, metaData, state, user, federal, filterColumns, show, onClose, setOpenForm, isDestoryForm, formDestroy }) {
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const [objectionVal, setObjectionVal] = useState(false);
    const [searchValue, setSearchValue] = useState(false);

    const attorneys = user && metaData && metaData['objectionsFilterOptions'] && metaData['objectionsFilterOptions'].length > 0 && metaData['objectionsFilterOptions'].reduce((acc, el) => {
        if (el && el.label) {
            acc.push(el);
        }
        return acc;
    }, []) || false;
    const { customerObjections = [] } = metaData;
    const stateObjectionFilter = customerObjections.filter(item => (item.state && (federal === false || federal === null)) ? item.state === state : item.state === `FEDERAL`);
    const defaultAttorneyObjections = stateObjectionFilter?.length > 0 && stateObjectionFilter?.reduce((acc, el) => {
        const attorneyList = el?.attorneys && typeof el?.attorneys === 'string' && el?.attorneys != '' && el?.attorneys.split(',') || false;
        const attorney = attorneyList && user?.id && attorneyList.includes(user?.id)
        if (attorney) acc.push(el);
        return acc;
    }, []) || false;
    const defaultAttorney = defaultAttorneyObjections?.length > 0 ? true : false;
    const [filter, setFilter] = useState(defaultAttorney ? { attorneys: user?.id } : {});
    let filterObjections = filter?.attorneys && stateObjectionFilter?.reduce((acc, el) => {
        const attorneyList = el?.attorneys && typeof el?.attorneys === 'string' && el?.attorneys != '' && el?.attorneys.split(',') || false;
        const attorney = attorneyList && filter?.attorneys && attorneyList.includes(filter.attorneys)
        if (attorney) acc.push(el);
        return acc;
    }, []) || stateObjectionFilter;

    filterObjections = searchValue && searchValue != '' ? filterObjections.filter(_ => _.objection_title && _.objection_title.toLowerCase().includes(searchValue.toLowerCase())) : filterObjections;


    const handleClose = () => {
        if(onClose) {
            onClose();
        }
        setModalOpen(false);
        setSearchValue(false);
    }

    const handleCloseBtn = () => {
        if(isDestoryForm) {
            formDestroy();
            setOpenForm(false);
        }
        handleClose();
    }

    const handleSubmitBtn = () => {
        handleObjections(objectionVal);
        setModalOpen(false);
    }

    const handleFilter = (e) => {
        setFilter(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value }));
    }

    const InputChange = (e) => {
        setSearchValue(e);
    }

    return <Grid container className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleCloseBtn}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={classes.paper} style={!sm ? { width: '90%' } : !md ? { width: '60%' } : {}}>
                    <form onSubmit={(e) => e.preventDefault()} className={classes.form} noValidate>
                        <Grid container className={classes.header} justify="flex-end">
                            <Typography component="span" className={classes.title}>{title || ''}</Typography>
                            <CloseIcon onClick={handleCloseBtn} className={classes.closeIcon} />
                        </Grid>
                        {filterColumns && filterColumns['cases'] && filterColumns['cases'].length > 0 ? <Grid container xs={12} justify="space-between" className={classes.filterContainer}>
                            <Grid item alignItems="center" className={classes.filterOption}>
                                <TextField 
                                    placeholder="Search Objections"
                                    className={classes.fieldColor}
                                    type="text"
                                    value={searchValue || ''}
                                    onChange={(e) => InputChange(e.target.value)}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start" >
                                               <SearchIcon className={classes.searchIcon}/>
                                            </InputAdornment>
                                        ),
                                        endAdornment: searchValue && (
                                            <InputAdornment position="end" onClick={() => setSearchValue(false)}>
                                               <CloseIcon className={classes.closeIcon1}/>
                                            </InputAdornment>
                                        )
                                    }}/>
                            </Grid>
                            <Grid item alignItems="center" className={classes.filterOption}>
                                {filterColumns['cases'].map((filterColumn, index) =>
                                    <Grid key={index} className={classes.dropdownGrid}>
                                        <select name={filterColumn.value} className={classes.filter} defaultValue={filter?.attorneys || ''} onChange={handleFilter}>
                                            {(attorneys).map((option, i) => <option key={i} disabled={option.disabled} value={option.value}>{option.label}</option>)}
                                        </select>
                                    </Grid>)}
                            </Grid>
                        </Grid> : null}
                        <Grid style={{ paddingRight: '12px' }}>
                            <Field
                                name="Objections"
                                type="text"
                                component={InputComponent}
                                onChange={(data) => setObjectionVal(data)}
                                metaData={metaData}
                                classes={classes}
                                sm={sm}
                                stateType={state}
                                federal={federal}
                                filterObjections={filterObjections}
                                filter={filter} />
                        </Grid>
                        <Grid container className={classes.footer}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle1" gutterBottom component="span" style={{ fontSize: '14px' }}>
                                    Note: See working form for previously selected objections (previous selections are not shown here since they could be manually edited or removed)
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container justify="flex-end">
                                    <Button
                                        type="button"
                                        variant="contained"
                                        color="primary"
                                        disabled={pristine || invalid || objectionVal.length == 0}
                                        className={classes.button}
                                        onClick={handleSubmitBtn}
                                        style={{ marginRight: '20px', width: '120px' }}>
                                        {submitting && <ButtonSpinner /> || 'submit'}
                                    </Button>
                                    <Button
                                        type="button"
                                        variant="contained"
                                        onClick={handleCloseBtn}
                                        className={classes.button}
                                        style={{ width: '120px' }}>
                                        Cancel
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ObjectionsModal.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    metaData: PropTypes.object,
    state: PropTypes.string,
    federal: PropTypes.bool
};

export default reduxForm({
    form: 'ObjectionsModalForm',
})(ObjectionsModal);
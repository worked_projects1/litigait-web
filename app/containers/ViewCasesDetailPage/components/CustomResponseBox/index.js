/**
 * 
 * Custom Response Box 
 * 
 */

import React, { useState } from 'react';
import { Grid, Typography, FormControlLabel, Checkbox } from '@material-ui/core';
import FormProvider from '../FormProvider';
import ModalDialog from 'components/ModalDialog';
import ClipLoader from 'react-spinners/ClipLoader';
import ReactHtmlParser from 'react-html-parser';
import AlertDialog from 'components/AlertDialog';
import Styles, { titleStyle } from './styles';
import { Tooltip } from "@material-ui/core";
import moment from 'moment';
import HistoryModalRecordForm from '../HistoryModalRecordForm';
import HistoryIcon from '@material-ui/icons/History';
import { handleClass } from '../../utils';
import { customMessage } from '../../messages';


function CustomResponseBox(props) {

    const { record, fields, disableForm, translate, reloadForm, mergeTranslate, actions, user, copyToLawyerResponse, checked, showCheckbox, case_title, style, forms = {}, state, handleSubGroup, federal, updateEditedFlag, dispatch, metaData, filterColumns, showHistoryOption, name, responseHistoryLimit, attorneyResponseTracking, rfaRecord } = props;
    const classes = Styles();

    const { id, question_number, question_number_text, question_text, document_type, question_section, question_section_text, is_the_client_response_edited, last_updated_by_client } = record;

    const detailFormRecord = forms[`detailForm_${id}`] && forms[`detailForm_${id}`].values || false;
    const lastEdit = record?.responseHistory?.[0]?.timestamp;

    const [openForm, setOpenForm] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    const [editResponse, setEditResponse] = useState(false);
    const [showHistoryForm, setShowHistoryForm] = useState(false);

    const handleSubmit = (data, dispatch, { form }) => {
        dispatch(actions.updateFrogsForm(Object.assign({}, { data, rfaRecord }), form, setOpenForm))
    }

    const handleTranslation = (data) => {
        setSpinner(true);
        translate(data, setSpinner);
    }

    const handleCloseDialog = () => {
        setSpinner(false);
        reloadForm();
    }

    const handleClipboard = () => {
        setSpinner(false);
        mergeTranslate(record);
    }

    const buttonOptions = [
        {
            action: handleClipboard,
            value: 'clipboard',
            label: 'Copy to attorney response'
        }
    ];

    const handleResponseBox = (field) => {
        if(field.editRecord && !field.disabled) {
            setOpenForm(true); 
        } else if(field.editRecord && field.disabled) {
            setShowPopup(true);
        }
    }

    const handleRestoreResponse = (data, dispatch, { form }) => {
        const submitRecord = Object.assign({}, { ...data?.response });
        dispatch(actions.restoreResponseHistory(submitRecord, record, setShowHistoryForm, setOpenForm, form));
    }

    const handleCloseModel = () => {
        setShowHistoryForm(false);
    }

    return (
        <Grid container style={style}>
            <Grid container item xs spacing={3} className={classes.section}>
                <Grid item xs={12} style={{ paddingBottom: '0px' }}>
                    <Typography variant="subtitle2">
                        {question_section ? <b>{question_section} {document_type == 'FROGS' ? (question_section_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : (question_section_text)}</b>
                            :
                            <b>{question_number_text || question_number}. {document_type == 'FROGS' ? (question_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : (question_text)}</b>}
                    </Typography>
                </Grid>
                {(fields || []).map((field, index) => (
                    <Grid key={index} item xs={12} className={classes.field}>
                        <Grid container direction="row" justify="space-between" alignItems="flex-end">
                            <Grid>
                                <Grid container alignItems="center">
                                    <Grid item style={{ marginRight: "12px" }}>
                                        <Typography variant="subtitle2" className={classes.responseType}>
                                            {field.value === "client_response_text" && record && is_the_client_response_edited ? <b>{field.label}<Tooltip title={moment(record && last_updated_by_client).format('MM/DD/YYYY HH:mm')} placement="top-start">
                                                <span className={classes.edited} onClick={() => setEditResponse(true)}><span style={{ marginLeft: '2px' }}>*</span>&nbsp;Edited</span></Tooltip></b> : <b>{field.label}</b>}
                                        </Typography>
                                    </Grid>
                                    {question_section && field.editRecord && !field.disabled ?
                                        <Grid item className={classes.checkboxField}>
                                            <AlertDialog
                                                description={customMessage?.subGroup && typeof customMessage?.subGroup === 'function' && customMessage?.subGroup({ record, question_number_text, question_number})}
                                                onConfirm={() => handleSubGroup(Object.assign({}, record, { subgroup: !record.subgroup }), setOpenForm)}
                                                onConfirmPopUpClose
                                                btnLabel1='Confirm'
                                                btnLabel2='Cancel'>
                                                {(open) => <FormControlLabel
                                                    name={id}
                                                    control={<Checkbox
                                                        style={{ color: "#2ca01c" }}
                                                        checked={record.subgroup}
                                                        onChange={open} />}
                                                    label={<span>{`Make this a single response for question ${question_number_text || question_number}`}</span>} />}
                                            </AlertDialog>
                                        </Grid> : null}
                                </Grid>
                            </Grid>
                            {field.value === "lawyer_response_text" && record.responseHistory?.length > 0 && showHistoryOption && <Grid item style={{ cursor: 'pointer', color: "#2ca01c"}} onClick={() => setShowHistoryForm({ historyId: record.legalform_id })}>
                                <Typography variant="subtitle2" className={classes.responseType}>
                                    <Tooltip title={`Last saved ${moment(lastEdit).format('MM/DD/YYYY, HH:mm A')}`} placement="left">
                                        <HistoryIcon />
                                    </Tooltip>
                                </Typography>
                            </Grid>}

                            {field.value === "client_response_text" ?
                                <Grid className={classes.lawyerResponseText} style={{ justifyContent: 'flex-end' }}>

                                    {record[field.value] && record[field.value] != '' && record.TargetLanguageCode !== 'es' && record.TargetLanguageCode !== 'vi' && <Typography variant="subtitle2" className={classes.copyLawyerResponse} onClick={() => copyToLawyerResponse(record, openForm, detailFormRecord)}>
                                        <b>Copy To Attorney Response</b>
                                    </Typography> || null}

                                    {document_type == 'FROGS' && field.value === "client_response_text" && record[field.value] && record[field.value] != '' && (record.TargetLanguageCode === 'es' || record.TargetLanguageCode === 'vi') && <Typography variant="subtitle2" className={`${classes.translate} translateBtn`} onClick={() => handleTranslation({ id: record.id, text: record[field.value], source: record.TargetLanguageCode , target: 'en' })}><b>{spinner && !record.translation && <ClipLoader color="#2ca01c" /> || 'See Translation'}</b></Typography> || null}

                                </Grid> : null}

                            {showHistoryForm && typeof showHistoryForm === 'object' && <HistoryModalRecordForm
                                initialValues={Object.assign({}, { response: record?.responseHistory[0] })}
                                form={`responseHistory_${record.id}`}
                                show={showHistoryForm?.id === record.legalform_id}
                                onSubmit={handleRestoreResponse}
                                btnLabel={'Restore this version'}
                                onClose={handleCloseModel}
                                metaData={metaData}
                                filterColumns={filterColumns}
                                responseHistory={record?.responseHistory} />}

                            {record.translation && (record.TargetLanguageCode === 'es' || record.TargetLanguageCode === 'vi') && <ModalDialog
                                title={record.translation || ''}
                                className={classes.form}
                                titleStyle={titleStyle}
                                options={buttonOptions}
                                btnStyle={{ width: '270px' }}
                                onClose={handleCloseDialog}
                                show={record.translation && document_type == 'FROGS'}>
                            </ModalDialog> || null}
                        </Grid >
                        {(openForm || (detailFormRecord && detailFormRecord.case_id && detailFormRecord.copyNote !== true)) && field.editRecord && !disableForm ? <Grid>
                            <FormProvider
                                id={id}
                                initialValues={detailFormRecord && detailFormRecord.case_id ? detailFormRecord : record || {}}
                                form={`detailForm_${id}`}
                                field={field}
                                className={handleClass(Object.assign({}, { status : record[field.status], form: 'form', classes }))}
                                onSubmit={handleSubmit}
                                user={user}
                                state={state}
                                federal={federal}
                                pageName={name}
                                filterColumns={filterColumns}
                                actions={actions}
                                dispatch={dispatch}
                                attorneyResponseTracking={attorneyResponseTracking}
                                responseHistoryLimit={responseHistoryLimit}
                                fieldValue="lawyer_response_text"
                                formName="detailForm"
                                fieldState="lawyer_response_status"
                                {...props}
                            />
                        </Grid> :
                            <Grid className={handleClass(Object.assign({}, { status : record[field.status], form: false, value: field.value, classes }))} onClick={() => handleResponseBox(field)}>
                                {field.value === "client_response_text" && record && record.lawyer_response_text && record.client_response_text && record.share_attorney_response && record.client_modified_response ? <span>{customMessage?.modifiedLawyerResponse}<br/><br/></span> : field.value === "client_response_text" && record && record.lawyer_response_text && record.client_response_text && record.share_attorney_response && record.client_modified_response != null && !record.client_modified_response ? <span>{customMessage?.notModifiedLawyerResponse}<br/><br/></span> : null}
                                {field.value === "client_response_text" ? <span className={classes.responseText} style={{  whiteSpace: 'break-spaces' }}>{record && record[field.value] && (record[field.value])}</span> :  <span className={classes.responseText}>{record && record[field.value] && ReactHtmlParser(record[field.value])}</span>}
                            </Grid>}

                        <AlertDialog
                            description={customMessage?.subGroupSplit && typeof customMessage?.subGroupSplit === 'function' && customMessage?.subGroupSplit({ question_number_text, question_number })}
                            openDialog={showPopup}
                            closeDialog={() => setShowPopup(false)}
                            onConfirm={() => {
                                setOpenForm(true);
                                handleSubGroup(Object.assign({}, record, { subgroup: false }));
                            }}
                            onConfirmPopUpClose
                            btnLabel1='Confirm'
                            btnLabel2='Cancel' />

                        <AlertDialog
                            description={customMessage?.clientResponse}
                            openDialog={editResponse}
                            closeDialog={() => setEditResponse(false)}
                            onConfirm={() => updateEditedFlag(record)}
                            onConfirmPopUpClose
                            btnLabel1='Yes'
                            btnLabel2='No' />
                    </Grid>
                ))}
            </Grid>
        </Grid>
    )
}


export default CustomResponseBox;
/**
 * 
 *  Modal Record Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';
import CustomResponseBox from '../CustomResponseBox';
import ObjectionBox from '../ObjectionBox';
import lodash from 'lodash'

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ModalRecordForm(props) {
    const { title, handleSubmit, children, metaData, className, style, onOpen, onClose, error, show, destroy, disableContainer, paperClassName, disableCloseModal, disableCancelBtn, frogsQuestions, record, objectionColumns, name, filterColumns, handleEditedFlag, responseHistoryLimit, user, forms, handleCopyToLawyerResponse, handleReload, handleMergeTranslate, handleTranslate, dispatch, actions, legalColumns, state, federal, attorneyResponseTracking, dynamicFrogsQuestions, customLegalColumns } = props;

    const filterQuestions = dynamicFrogsQuestions && Object.keys(dynamicFrogsQuestions).length > 0 ? dynamicFrogsQuestions : false;
    const frogsFilteredQuestions = filterQuestions && filterQuestions?.forms && filterQuestions?.forms?.length > 0 && dynamicFrogsQuestions?.forms.reduce((acc, el) => {
        if(el.questions && el.questions?.length > 0) {
            acc.push(el);
        }
        return acc;
    }, []);

    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [selectedForm, setSelectedForm] = useState(frogsFilteredQuestions && frogsFilteredQuestions?.length > 0 && frogsFilteredQuestions[0] && frogsFilteredQuestions[0]?.legalforms_id || false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));

    let customQuestions = frogsFilteredQuestions && frogsFilteredQuestions?.length > 0 ? frogsFilteredQuestions?.filter(e => e.legalforms_id === selectedForm) : false
    customQuestions = customQuestions && Array.isArray(customQuestions) && customQuestions?.length > 0 ? customQuestions[0]?.questions : false;

    let filteredQuestions;
    let questions;
    let groupedQuestion = customQuestions && Object.keys(customQuestions).reduce((result, key) => {
        result[key] = customQuestions[key];
        return result;
    }, {});

    filteredQuestions = customQuestions && Object.keys(customQuestions)
        .reduce((result, key) => {
            result[key] = customQuestions[key];
            return result;
        }, {});


    questions =
        (filteredQuestions &&
            filteredQuestions &&
            lodash.groupBy(filteredQuestions, 'question_number_text')) ||
        {};
    groupedQuestion = groupedQuestion && lodash.groupBy(groupedQuestion, 'question_number_text') || {};


    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    const handleSubGroup = (r, setOpenForm) => {
        dispatch(actions.saveSubGroupFrogsForm(Object.assign({}, { data: r, rfaRecord: record }), setOpenForm));
    }

    const handleFilter = (e) => {
        const { name, value } = e.target;
        setSelectedForm(value);
    }

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={!disableCloseModal ? closeModal : null}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                    <Grid container className={classes.header} justify="space-between">
                        <Grid item xs={10}>
                            <Typography component="span" className={classes.title}>{title || ''}</Typography>
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                            {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                        </Grid>
                    </Grid>
                    <Grid container xs={12}>
                        {frogsFilteredQuestions && Array.isArray(frogsFilteredQuestions) && frogsFilteredQuestions?.length > 1 ? <Grid item xs={12} style={{ marginBottom: '14px' }}>
                            <select name="frogs_form" defaultValue={selectedForm || frogsFilteredQuestions[0]?.legalforms_id} className={classes.filter} onChange={handleFilter}>
                                {frogsFilteredQuestions.filter(_ => _.questions && _.questions.length > 0).map((a, i) => <option key={i} value={a.legalforms_id}>{a.filename}</option>)}
                            </select>
                        </Grid> : null}
                        <Grid item xs={12} className={classes.bodyScroll}>
                            {(Object.keys(questions) || []).map((key, i) => {
                                const frogsDocumentWithSubGroup = questions[key][0]['document_type'] == 'FROGS' && questions[key] && questions[key].length > 1 ? true : false;

                                return (
                                    <Grid key={i} item xs={12} style={{ marginTop: '15px' }}>
                                        <Grid container direction="column">
                                            {questions[key] && questions[key].length > 0 && questions[key][0]['question_section_text'] ?
                                                <Typography variant="subtitle2" style={frogsDocumentWithSubGroup ? { padding: '12px', paddingBottom: '2px' } : { padding: '12px' }}>
                                                    <b>{questions[key][0]['question_number_text']}. {questions[key][0]['document_type'] == 'FROGS' ? (questions[key][0]['question_text']).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : questions[key][0]['question_text']}</b>
                                                </Typography> : null}
                                        </Grid>
                                        <Grid item xs={12}>
                                            {(questions[key] && questions[key].length > 0 && (questions[key] || []).map((form, index) => <CustomResponseBox
                                                key={index}
                                                case_title={record.case_title}
                                                actions={actions}
                                                dispatch={dispatch}
                                                questionNumber={key + 1}
                                                metaData={metaData}
                                                translate={handleTranslate}
                                                mergeTranslate={handleMergeTranslate}
                                                fields={questions[key] && groupedQuestion[key] && questions[key].length > 0 && groupedQuestion[key].length > 0 && groupedQuestion[key][0]['question_section_text'] && groupedQuestion[key].find(_ => _.subgroup) ? customLegalColumns.filter(_ => _.viewRecord).map(r => Object.assign({}, r, { disabled: !form.subgroup })) : customLegalColumns.filter(_ => _.viewRecord)}
                                                reloadForm={handleReload}
                                                handleSubGroup={handleSubGroup}
                                                user={user}
                                                forms={forms}
                                                copyToLawyerResponse={handleCopyToLawyerResponse}
                                                record={form}
                                                name={name}
                                                filterColumns={filterColumns}
                                                federal={federal}
                                                state={state}
                                                updateEditedFlag={handleEditedFlag}
                                                responseHistoryLimit={responseHistoryLimit}
                                                showHistoryOption={true}
                                                rfaRecord={record}
                                                attorneyResponseTracking={attorneyResponseTracking} />) || null)}
                                        </Grid>
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </Grid>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ModalRecordForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'customModalRecord',
})(ModalRecordForm);
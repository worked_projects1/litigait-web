/**
 * 
 * Response Box 
 * 
 */

import React, { useEffect, useState, useRef } from 'react';
import { Grid, Typography, FormControlLabel, Checkbox } from '@material-ui/core';
import FormProvider from '../FormProvider';
import ModalDialog from 'components/ModalDialog';
import ClipLoader from 'react-spinners/ClipLoader';
import { generateZIP } from 'utils/tools';
import ReactHtmlParser from 'react-html-parser';
import AlertDialog from 'components/AlertDialog';
import Styles, { titleStyle } from './styles';
import { Tooltip } from "@material-ui/core";
import moment from 'moment';
import HistoryModalRecordForm from '../HistoryModalRecordForm';
import HistoryIcon from '@material-ui/icons/History';
import { handleClass } from '../../utils';
import { customMessage } from '../../messages';
import CustomModalForm from '../CustomModalForm';
import { getStepper } from 'utils/utils';
import Stepper from 'components/Stepper';

const denyKeyWordList = ['deny', 'denies', 'denied', 'denying', 'denied', 'refuse', 'reject', 'disavow', 'disclaim', 'repudiate', 'contradict', 'disown', 'renounce', 'disallow', 'disagree', 'negate', 'oppose', 'withhold', 'decline', 'contest', 'dissent', 'counter', 'resist', 'repel', 'abnegate', 'refusing', 'repudiating', 'renouncing', 'negating', 'opposing', 'declining', 'abnegating'];


function ResponseBox(props) {

    const { record, fields, disableForm, translate, reloadForm, mergeTranslate, actions, user, copyToLawyerResponse, checked, showCheckbox, case_title, style, requestPhoto, forms = {}, state, handleSubGroup, federal, updateEditedFlag, dispatch, metaData, filterColumns, showHistoryOption, name, responseHistoryLimit, attorneyResponseTracking, objectionColumns, legalColumns, frogsQuestions, consultationQuestions } = props;
    const classes = Styles();

    const { id, question_number, question_number_text, question_id, question_text, document_type, question_section, question_section_text, is_the_client_response_edited, last_updated_by_client, duplicate_set_no } = record;

    const detailFormRecord = forms[`detailForm_${id}`] && forms[`detailForm_${id}`].values || false;
    const lastEdit = record?.responseHistory?.[0]?.timestamp;

    const [openForm, setOpenForm] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [downloadSpinner, setDownloadSpinner] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    const [editResponse, setEditResponse] = useState(false);
    const [showHistoryForm, setShowHistoryForm] = useState(false);
    const [openPopupForm, setOpenPopupForm] = useState(false);
    const [childRef, setChildRef] = useState(false);
    const [progress, setProgress] = useState(false);
    const [stepperLoader, setStepperLoader] = useState(false);
    const [key, setKey] = useState(0);
    const [linkLoader, setLinkLoader] = useState(false);

    const controllerRef = useRef(new AbortController());
    const { title, steps, successMessage } = progress && getStepper(progress) || {};

    const showCustomQuestionsPopup = frogsQuestions && frogsQuestions?.length > 0 ? true : false;
    let lawyer_response_text = !detailFormRecord && record && record?.lawyer_response_text || false;
    lawyer_response_text = lawyer_response_text && lawyer_response_text.toLowerCase();

    let form_lawyer_response_value = detailFormRecord && detailFormRecord?.case_id && detailFormRecord?.lawyer_response_text || false;
    form_lawyer_response_value = form_lawyer_response_value && form_lawyer_response_value.toLowerCase();
    
    const showForgsLink = () => {
        let show = denyKeyWordList && form_lawyer_response_value && (denyKeyWordList.find(e => form_lawyer_response_value.includes(e)) ? true : false) && showCustomQuestionsPopup && ['rfa', 'RFA'].includes(document_type) && state && state === 'CA' || false;

        if(!show && childRef && childRef?.ref && childRef?.setState && childRef?.containerRef) {
            if (childRef?.ref?.current) {
                childRef.ref.current.style.display = 'none';
            } 
            if(childRef?.containerRef?.current) {
                childRef.containerRef.current.style.justifyContent = 'flex-end';
            }
            if(childRef?.containerBtnRef?.current) {
                let elementClasses = childRef?.containerBtnRef?.current.classList;
                if (elementClasses.contains('MuiGrid-grid-xs-6')) {
                    elementClasses.remove('MuiGrid-grid-xs-6');
                    elementClasses.add('MuiGrid-grid-xs-12');
                }
            }
            childRef?.setState();
        }

        if(show && childRef && childRef?.ref && childRef?.setState && childRef?.containerRef) {
            if(childRef?.containerBtnRef?.current) {
                childRef.ref.current.style.display = 'flex';
            } 
            
            if(childRef?.containerBtnRef?.current) {
                let elementClasses = childRef?.containerBtnRef?.current.classList;
                if (elementClasses.contains('MuiGrid-grid-xs-12')) {
                    elementClasses.remove('MuiGrid-grid-xs-12');
                    elementClasses.add('MuiGrid-grid-xs-6');
                }
            }
        }    
        return show;
    }

    const showLink = showForgsLink();

    useEffect(() => {
        if(!showLink && record && record?.id && detailFormRecord?.case_id === record?.case_id &&detailFormRecord && detailFormRecord?.id === record?.id && state === 'CA' && ['rfa', 'RFA'].includes(document_type)) {
            dispatch(actions.deleteConnectedQuestionSet(Object.assign({}, { id: record?.id })))
        }
    }, [showLink]);

    const handleAddFrogsQuestion = () => {
        setLinkLoader(true);
        dispatch(actions.addFrogsQuestions(Object.assign({}, { id: record?.id, legalforms_id: record?.legalforms_id, case_id: record?.case_id, document_type: record?.document_type }), setOpenPopupForm, setLinkLoader));
    }

    const linkComponent = ({ field, lawyer_response_text, state, document_type, linkLoader: loader }) => {
        return lawyer_response_text && denyKeyWordList && lawyer_response_text && (denyKeyWordList.find(e => lawyer_response_text.includes(e)) ? true : false) && showCustomQuestionsPopup && field.value === "lawyer_response_text" && ['rfa', 'RFA'].includes(document_type) && state && state === 'CA' ? 
        <Grid style={{ marginBottom: "5px", marginTop: '5px' }}>
                <Typography
                    className={classes.responseType}
                    variant="subtitle2"
                    onClick={handleAddFrogsQuestion}
                    style={{ color: "#2ca01c", textDecoration: "underline", cursor: 'pointer',  display: "inline" }}>{loader ? <ClipLoader color="#2ca01c" /> : `Click here if you wish to answer 17.1 questions in FROGS`}</Typography>
            </Grid>
         : null
    }

    const handleSubmit = (data, dispatch, { form }) => {
        dispatch(actions.updateForm(data, form, setOpenForm))
    }

    const handleTranslation = (data) => {
        setSpinner(true);
        translate(data, setSpinner);
    }

    const handleCloseDialog = () => {
        setSpinner(false);
        reloadForm();
    }

    const handleClipboard = () => {
        setSpinner(false);
        mergeTranslate(record);
    }

    const buttonOptions = [
        {
            action: handleClipboard,
            value: 'clipboard',
            label: 'Copy to attorney response'
        }
    ];

    const handleZip = () => {
        const controller = new AbortController();
        controllerRef.current = controller;
        setKey(0);
        setDownloadSpinner(true);
        setProgress('download_zip');
        const filename = `${case_title}_${record.document_type}_${record.question_number}`;
        const files = record && record.uploaded_documents || [];
        
        setTimeout(() => {
            generateZIP(filename, files, setDownloadSpinner, setProgress, setKey, controller)
        }, 2000);
    }

    const handleTerminateClick = () => {
        setProgress(false);
        setDownloadSpinner(false);
        controllerRef.current.abort();
      };
    
    const handleResponseBox = (field) => {
        if(field.editRecord && !field.disabled) {
            setOpenForm(true); 
            dispatch(actions.autoSaveForm(record, ['detailForm_']));
        } else if(field.editRecord && field.disabled) {
            setShowPopup(true);
        }
    }

    const handleRestoreResponse = (data, dispatch, { form }) => {
        const submitRecord = Object.assign({}, { ...data?.response });
        dispatch(actions.restoreResponseHistory(submitRecord, record, setShowHistoryForm, setOpenForm, form));
    }

    const handleCloseModel = () => {
        setShowHistoryForm(false);
    }

    return (
        <Grid container style={style}>
            <Grid container item xs spacing={3} className={classes.section}>
                <Grid item xs={12} style={{ paddingBottom: '0px' }}>
                    <Typography variant="subtitle2">
                        {question_section ? <b>{question_section} {document_type == 'FROGS' ? (question_section_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : (question_section_text)}</b>
                            :
                            <b>{question_number_text || question_number}. {document_type == 'FROGS' ? (question_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : (question_text)}</b>}
                    </Typography>
                </Grid>
                {(document_type == 'rfpd' || document_type == 'RFPD') && showCheckbox ? requestPhoto && React.createElement(requestPhoto) : null}
                {(fields || []).map((field, index) => (
                     (field.value === 'client_response_text' ? (duplicate_set_no == 1) : field.value === 'lawyer_response_text') ? <Grid key={index} item xs={12} md={6} className={classes.field}>
                        <Grid container direction="row" justify="space-between" alignItems="flex-end">
                            <Grid>
                                <Grid container alignItems="center">
                                    <Grid item style={{ marginRight: "12px" }}>
                                        <Typography variant="subtitle2" className={classes.responseType}>
                                            {field.value === "client_response_text" && record && is_the_client_response_edited ? <b>{field.label}<Tooltip title={moment(record && last_updated_by_client).format('MM/DD/YYYY HH:mm')} placement="top-start">
                                                <span className={classes.edited} onClick={() => setEditResponse(true)}><span style={{ marginLeft: '2px' }}>*</span>&nbsp;Edited</span></Tooltip></b> : <b>{field.label}</b>}
                                        </Typography>
                                    </Grid>
                                    {(consultationQuestions ? consultationQuestions?.length > 1 && question_section && field.editRecord && !field.disabled : (question_section && field.editRecord && !field.disabled)) ?
                                        <Grid item className={classes.checkboxField}>
                                            <AlertDialog
                                                description={customMessage?.subGroup && typeof customMessage?.subGroup === 'function' && customMessage?.subGroup({ record, question_number_text, question_number })}
                                                onConfirm={() => handleSubGroup(Object.assign({}, record, { subgroup: !record.subgroup }), setOpenForm)}
                                                onConfirmPopUpClose
                                                btnLabel1='Confirm'
                                                btnLabel2='Cancel'>
                                                {(open) => <FormControlLabel
                                                    name={id}
                                                    control={<Checkbox
                                                        style={{ color: "#2ca01c" }}
                                                        checked={record.subgroup}
                                                        onChange={open} />}
                                                    label={<span>{`Make this a single response for question ${question_number_text || question_number}`}</span>} />}
                                            </AlertDialog>
                                        </Grid> : null}
                                </Grid>
                            </Grid>
                            {field.value === "lawyer_response_text" && record.responseHistory?.length > 0 && showHistoryOption && <Grid item style={{ cursor: 'pointer', color: "#2ca01c"}} onClick={() => setShowHistoryForm({ historyId: record.legalform_id })}>
                                <Typography variant="subtitle2" className={classes.responseType}>
                                    <Tooltip title={`Last saved ${moment(lastEdit).format('MM/DD/YYYY, HH:mm A')}`} placement="left">
                                        <HistoryIcon />
                                    </Tooltip>
                                </Typography>
                            </Grid>}

                            {field.value === "client_response_text" ?
                                <Grid className={classes.lawyerResponseText} style={{ justifyContent: 'flex-end' }}>

                                    {record[field.value] && record[field.value] != '' && record.TargetLanguageCode !== 'es' && record.TargetLanguageCode !== 'vi' && <Typography variant="subtitle2" className={classes.copyLawyerResponse} onClick={() => copyToLawyerResponse(record, openForm, detailFormRecord)}>
                                        <b>Copy To Attorney Response</b>
                                    </Typography> || null}

                                    {document_type == 'FROGS' && field.value === "client_response_text" && record[field.value] && record[field.value] != '' && (record.TargetLanguageCode === 'es' || record.TargetLanguageCode === 'vi') && <Typography variant="subtitle2" className={`${classes.translate} translateBtn`} onClick={() => handleTranslation({ id: record.id, text: record[field.value], source: record.TargetLanguageCode , target: 'en' })}><b>{spinner && !record.translation && <ClipLoader color="#2ca01c" /> || 'See Translation'}</b></Typography> || null}

                                </Grid> : null}

                            {showHistoryForm && typeof showHistoryForm === 'object' && <HistoryModalRecordForm
                                initialValues={Object.assign({}, { response: record?.responseHistory[0] })}
                                form={`responseHistory_${record.id}`}
                                show={showHistoryForm?.id === record.legalform_id}
                                onSubmit={handleRestoreResponse}
                                btnLabel={'Restore this version'}
                                onClose={handleCloseModel}
                                metaData={metaData}
                                filterColumns={filterColumns}
                                responseHistory={record?.responseHistory} />}

                            {record.translation && (record.TargetLanguageCode === 'es' || record.TargetLanguageCode === 'vi') && <ModalDialog
                                title={record.translation || ''}
                                className={classes.form}
                                titleStyle={titleStyle}
                                options={buttonOptions}
                                btnStyle={{ width: '270px' }}
                                onClose={handleCloseDialog}
                                show={record.translation && document_type == 'FROGS'}>
                            </ModalDialog> || null}
                        </Grid >
                        {(openForm || (detailFormRecord && detailFormRecord.case_id && detailFormRecord.copyNote !== true)) && field.editRecord && !disableForm ? <Grid>
                            <FormProvider
                                id={id}
                                initialValues={detailFormRecord && detailFormRecord.case_id ? detailFormRecord : record || {}}
                                form={`detailForm_${id}`}
                                field={field}
                                className={handleClass(Object.assign({}, { status : record[field.status], form: 'form', classes }))}
                                onSubmit={handleSubmit}
                                user={user}
                                state={state}
                                federal={federal}
                                pageName={name}
                                filterColumns={filterColumns}
                                actions={actions}
                                dispatch={dispatch}
                                attorneyResponseTracking={attorneyResponseTracking}
                                responseHistoryLimit={responseHistoryLimit}
                                fieldValue="lawyer_response_text"
                                formName="detailForm"
                                fieldState="lawyer_response_status"
                                setChildRef={setChildRef}
                                showLink={showLink}
                                linkComponent={linkComponent}
                                {...props}
                            />
                        </Grid> :
                            <Grid className={handleClass(Object.assign({}, { status : record[field.status], form: false, value: field.value, classes }))} onClick={() => handleResponseBox(field)}>
                                {field.value === "client_response_text" && record && record.lawyer_response_text && record.client_response_text && record.share_attorney_response && record.client_modified_response ? <span>{customMessage?.modifiedLawyerResponse}<br/><br/></span> : field.value === "client_response_text" && record && record.lawyer_response_text && record.client_response_text && record.share_attorney_response && record.client_modified_response != null && !record.client_modified_response ? <span>{customMessage?.notModifiedLawyerResponse}<br/><br/></span> : null}
                                {field.value === "client_response_text" ? <span className={classes.responseText} style={{  whiteSpace: 'break-spaces' }}>{record && record[field.value] && (record[field.value])}</span> :  <span className={classes.responseText}>{record && record[field.value] && ReactHtmlParser(record[field.value])}</span>}
                            </Grid>}

                        <AlertDialog
                            description={customMessage?.subGroupSplit && typeof customMessage?.subGroupSplit === 'function' && customMessage?.subGroupSplit({ question_number_text, question_number })}
                            openDialog={showPopup}
                            closeDialog={() => setShowPopup(false)}
                            onConfirm={() => {
                                setOpenForm(true);
                                handleSubGroup(Object.assign({}, record, { subgroup: false }));
                            }}
                            onConfirmPopUpClose
                            btnLabel1='Confirm'
                            btnLabel2='Cancel' />
                        {linkComponent && React.createElement(linkComponent, Object.assign({}, { field, lawyer_response_text, state, document_type, linkLoader }))}
                            {openPopupForm && <CustomModalForm 
                                show={openPopupForm} 
                                onClose={() => setOpenPopupForm(false)}
                                {...props}
                                objectionColumns={objectionColumns}
                                onSubmitClose />}

                        <AlertDialog
                            description={customMessage?.clientResponse}
                            openDialog={editResponse}
                            closeDialog={() => setEditResponse(false)}
                            onConfirm={() => updateEditedFlag(record)}
                            onConfirmPopUpClose
                            btnLabel1='Yes'
                            btnLabel2='No' />

                        {(document_type == 'rfpd' || document_type == 'RFPD') && field.value === "client_response_text" && record.uploaded_documents && JSON.parse(record.uploaded_documents).length > 0 ?
                            <Typography variant="subtitle2" className={`${classes.download} translateBtn`} onClick={handleZip} >
                                <b>{downloadSpinner && <ClipLoader color="#2ca01c" /> || 'Download Files'}</b>
                            </Typography> : null}
                    </Grid> : null
                ))}
                <Stepper
                    title={title}
                    active={key}
                    show={progress ? true : false}
                    close={handleTerminateClick}
                    steps={steps}
                    loader={stepperLoader}
                    disableCancelBtn={false}
                    errorBtnText={false}
                    errorBtnAction={false}
                    cancel={handleTerminateClick}
                    successMessage={successMessage} />
            </Grid>
        </Grid >
    )
}


export default ResponseBox;
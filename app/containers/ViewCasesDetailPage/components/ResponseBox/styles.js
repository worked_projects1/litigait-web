

import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({

  fieldColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '130px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    borderBottom: '2px solid #2ca01c',
    cursor: 'pointer'
  },
  blackColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '130px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    borderBottom: '2px solid black',
    cursor: 'pointer'
  },
  blueColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '130px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    borderBottom: '2px solid #3275BF',
    cursor: 'pointer'
  },


  fieldColorForm: {
    fontSize: '10px',
    '& :after': {
      borderBottom: '2px solid #2ca01c',
    },
    '& :before': {
      borderBottom: '2px solid #2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }

  },

  blackColorForm: {
    fontSize: '10px',
    '& :after': {
      borderBottom: '2px solid black',
    },
    '& :before': {
      borderBottom: '2px solid black',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: 'black',
    },
    '&.Mui-focused fieldset': {
      borderColor: 'black',
    }
  },

  blueColorForm: {
    fontSize: '10px',
    '& :after': {
      borderBottom: '2px solid #3275BF',
    },
    '& :before': {
      borderBottom: '2px solid #3275BF',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#3275BF',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#3275BF',
    }
  },

  textSize: {
    fontSize: '14px',
    padding: 0,
  },
  responseType: {
    fontSize: '12px',
    marginBottom: '4px',
  },
  copyLawyerResponse: {
    color: '#47AC39',
    cursor: 'pointer',
    fontSize: '12px',
    marginBottom: '4px'
  },
  lawyerResponseText: {
    display: 'flex',
    justifyContent: 'space-between',
    minWidth: '246px'
  },
  responseField: {
    paddingTop: '0px',
  },
  btn: {
    fontSize: '12px',
    textTransform: 'none',
    padding: '4px 8px',
    margin: '6px 6px',
    fontWeight: 'bold',
  },
  btnGrid: {
    marginTop: '6px',
    marginBottom: '6px',
  },
  chkBox: {
    alignItems: 'normal',
    height: 'min-content',
    color: "#2ca01c",
    marginTop: '1px',
    width: '50px'
  },
  section: {
    margin: '0px'
  },
  field: {
    paddingTop: '0px'
  },
  line: {
    margin: '4px'
  },

  lineResponse: {
    marginTop: '10px'
  },
  translate: {
    fontSize: '12px',
    marginBottom: '4px',
    color: '#47AC39',
    cursor: 'pointer'
  },
  download: {
    fontSize: '12px',
    marginBottom: '4px',
    marginTop: '4px',
    color: '#47AC39',
    cursor: 'pointer',
    textAlign: 'end'
  },
  responseText: {
    '& > p': {
      margin: '0px !important'
    },
    '& > div': {
      '& > p': {
        margin: '0px !important'
      }
    }
  },
  fieldColorResp: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '130px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    border: '2px solid #2ca01c',
    cursor: 'pointer'
  },
  blackColorResp: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '130px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    border: '2px solid black',
    cursor: 'pointer'
  },
  blueColorResp: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '130px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '12px',
    border: '2px solid #3275BF',
    cursor: 'pointer'
  },
  checkboxField: {
    '@global': {
      '.MuiFormControlLabel-root': {
        [theme.breakpoints.down('xs')]: {
          marginRight: '0px',
        }
      },
      '.MuiFormControlLabel-label': {
        fontSize: '0.78rem',
        paddingBottom: '3px'
      },
      '.MuiSvgIcon-root': {
        height: '18px'
      },
      '.MuiCheckbox-colorSecondary': {
        paddingTop: '0px',
        paddingBottom: '4px',
        paddingRight: '2px'
      }
    }
  },
  edited : {
    color:'red',
    cursor: 'pointer'
  }
}));

export const titleStyle = {
  minHeight: '200px',
  display: 'flex',
  border: '3px dashed gray',
  borderRadius: '5px',
  padding: '15px',
  minWidth: '500px'
};


export default useStyles;
import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import messages from '../../messages';
import AlertDialog from 'components/AlertDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import SVG from 'react-inlinesvg';

const UploadDiscoveryButton = ({ description, handleSubmit, btnLabel1, btnLabel2, classes, progress, sm, document, onConfirmPopUpClose, type }) => {
    return <AlertDialog
    description={description}
    onConfirm={() => handleSubmit()}
    onConfirmPopUpClose={onConfirmPopUpClose}
    btnLabel1={btnLabel1}
    btnLabel2={btnLabel2}>
    {(openDialog) =>
        (<Grid item xs={12} className={classes.icon} onClick={openDialog}>
            <Button type="button" variant="contained" color="primary" className={classes.docButton}>
                {document && ['filevine', 'mycase', 'litify', 'clio'].includes(document.value) && progress && <ButtonSpinner color='#2ca01c' /> || type && <img src={require(`images/icons/file_upload.svg`)} alt={type} style={{ height: 'auto' }} />}
            </Button>
            {sm && <Grid className={classes.title}>
                <FormattedMessage {...messages.uploadDiscovery} />
            </Grid> || null}
        </Grid>)}
    </AlertDialog>
}

export default UploadDiscoveryButton;
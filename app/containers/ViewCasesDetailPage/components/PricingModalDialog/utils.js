/**
 * 
 * Icons
 * 
 */

import React from 'react';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function Icons(props) {

    const { type, classes, style } = props;

    switch (type) {
        case 'logo1':
            return <img src={require('images/pricing/logo1.png')} className={classes} />;
        case 'logo2': 
            return <img src={require('images/pricing/logo2.png')} className={classes} />;
        case 'logo3': 
            return <img src={require('images/pricing/logo3.png')} className={classes} />;
        case 'profile1':
            return <img src={require('images/pricing/profile1.png')} className={classes} />;
        case 'profile2': 
            return <img src={require('images/pricing/profile2.png')} className={classes} />;
        case 'profile3': 
            return <img src={require('images/pricing/profile3.png')} className={classes} />;
        case 'rating': 
            return <img src={require('images/pricing/rating1.png')} className={classes} />;
        case 'guarantee': 
            return <img src={require('images/pricing/guarantee_logo.png')} className={classes} style={style}/>;
        case 'review1': 
            return <img src={require('images/pricing/google.png')} className={classes} style={style} />;
        case 'review2': 
            return <img src={require('images/pricing/cap.png')} className={classes} style={style} />;
        case 'review3': 
            return <img src={require('images/pricing/circle.png')} className={classes} style={style} />;
        case 'rating1': 
            return <img src={require('images/pricing/rating1.png')} className={classes} />;
        case 'rating2': 
            return <img src={require('images/pricing/rating2.png')} className={classes} />;
        case 'rating3': 
            return <img src={require('images/pricing/rating3.png')} className={classes} />;
        case 'visa': 
            return <img src={require('images/pricing/visa.png')} className={classes} style={style} />;
        case 'masterCard': 
            return <img src={require('images/pricing/master.png')} className={classes} style={style} />;
        case 'amex': 
            return <img src={require('images/pricing/amex.png')} className={classes} style={style} />;
        case 'discover': 
            return <img src={require('images/pricing/discover.png')} className={classes} style={style} />;
        case 'cardO': 
            return <img src={require('images/pricing/card1.png')} className={classes} style={style} />;
        case 'unionPay': 
            return <img src={require('images/pricing/union_pay.png')} className={classes} style={style} />;
        case 'fire': 
            return <img src={require('images/pricing/fire_emoji.png')} className={classes} style={style} />;
        case 'stripe': 
            return <img src={require('images/pricing/stripe.png')} className={classes} style={style} />;
        default:
            return;
    }
}


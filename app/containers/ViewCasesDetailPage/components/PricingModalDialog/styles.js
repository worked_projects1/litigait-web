import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: 'none',
    boxShadow: theme.shadows[5],
    padding: '25px',
    minWidth: '40%',
    outline: 'none',
    width: '90%',
  },
  body: {
    marginTop: '5px',
  },
  cardYearly: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fffffffc'
  },
  cardMonthly: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ' #edffee',
    border: '3px solid #89d380',
    marginTop: '49px',
    boxShadow: 'none !important',
    padding: '0px !important',
    [theme.breakpoints.down('xs')]: {
      marginTop: 0
    }
  },
  tekPlanYearly: {
    fontWeight: 'bold',
    fontSize: '20px',
    marginTop: '10px',
    marginBottom: '15px',
    fontFamily: 'Avenir-Regular',
    textAlign: 'center',
    [theme.breakpoints.up('xl')]: {
      fontSize: '25px'
     }
  },
  tekPlanMonthly: {
    fontWeight: 'bold',
    fontSize: '20px',
    marginTop: '6px',
    marginBottom: '15px',
    fontFamily: 'Avenir-Regular',
    textAlign: 'center',
    [theme.breakpoints.up('xl')]: {
      fontSize : "25px"
    }
  },
  price: {
    fontWeight: 'bold',
    fontSize: '40px',
    marginBottom: '5px',
    fontFamily: 'Avenir-Regular',
    textAlign: 'center'
  },
  profile: {
    width: theme.spacing(7),
    height: theme.spacing(7),
    border: '3px solid #fff',
    borderRadius: '50px'
  },
  centerAlign: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  guaranteeGrid: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '12px'
  },
  guaranteeImg: {
    width: '135px',
    height: '86px'
  },
  cardAvatar: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: '2px solid #2ca01c',
    minHeight: '154px'
  },
  saveMoney: {
    fontWeight: 'bold',
    borderRadius: '28px',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular',
    fontSize: '13px',
    letterSpacing: '1px',
    backgroundColor: '#d3d3d3 !important',
    marginBottom: '15px',
    padding: '1px',
    width: '208px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonGetStart: {
    fontWeight: 'bold',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular',
    marginTop: '10px',
    textTransform: 'capitalize',
    width: '185px',
    borderRadius: '2px !important',
    marginTop: '20px',
    borderRadius: '28px',
    outline: 'none',
    '&:hover': {
      backgroundColor: 'green !important'
    },
    [theme.breakpoints.up('xl')]: {
      width : "285px",
      fontSize : "18px"
    }
  },
  divider: {
    backgroundColor: '#7dc972',
    fontWeight: "bold",
    fontSize: "50px",
    height: '2px',
    marginBottom: '27px',
    width: '208px',
    marginTop: '22px'
  },
  bestValue: {
    backgroundColor: '#248b16',
    color: '#fff',
    border: '3px solid #248b16'
  },
  checkboxYearly: {
    padding: 0,
    fontSize: '14px',
    fontWeight: 'bold',
    marginLeft: '17px'
  },
  checkboxMonthly: {
    padding: 2,
    fontSize: '14px',
    fontWeight: 'bold',
    marginLeft: '17px'
  },
  closeIcon: {
    cursor: 'pointer'
  },
  cardContent: {
    padding: '0px'
  },
  avatarGrid: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    zIndex: '99'
  },
  cardName: {
    marginTop: '12px',
    display: 'flex',
    flexDirection: 'column',
    position: 'absolute',
    top: 22
  },
  dividerCard: {
    backgroundColor: 'grey',
    height: '1px',
    width: '25px'
  },
  scrollBar: {
    maxHeight: '500px',
    overflowY: 'auto',
    overflowX: 'hidden',
    "@media (max-width: 653px)": {
      maxHeight: '218px !important',
    },
    "@media (min-width: 746px)": {
      maxHeight: '261px !important',
    },
    "@media (min-width: 870px)": {
      maxHeight: '322px !important',
    },
    "@media (min-width: 1044px)": {
      maxHeight: '406px !important',
    },
    "@media (min-width: 1187px)": {
      maxHeight: '472px !important',
    },
    "@media (min-width: 1306px)": {
      maxHeight: '534px !important',
    },
    "@media (min-width: 1451px)": {
      maxHeight: '634px !important',
    },
    "@media (min-width: 1500px)": {
      maxHeight: '715px !important',
    },
    "&::-webkit-scrollbar": {
      width: 8,
    },
    "&::-webkit-scrollbar-track": {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
      borderRadius: '30px'
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#2ca01c",
      borderRadius: '30px'
    }
  },
  fireEmoji: {
    width: '13px',
    height: '16px',
    marginRight: '3px',
    [theme.breakpoints.up('xl')]: {
      width: "19px",
      height: "26px"
    }
  },
  star: {
    width: '10px',
    height: '10px',
    marginLeft: '2px',
    marginBottom: '3px'
  },
  cardContent: {
    '& .MuiCardContent-root': {
      padding: '0px'
    }
  },
  billedPlan: {
    fontSize: '14px',
    display: 'flex',
    justifyContent: 'center',
    marginBottom: '15px',
    fontWeight: 'bold',
    [theme.breakpoints.up('xl')]: {
      fontSize : "20px"
    }
  },
  checkLabel: {
    fontSize: '14px',
    color: '#000',
    fontWeight: 'bold',
    paddingLeft: '8px',
    [theme.breakpoints.up('xl')]: {
      fontSize : "20px"
    }
  },
  avatarBorder: {
    border: '3px solid #86e179',
    borderRadius: '50%'
  },
  cardNameTypo: {
    fontSize: '11px',
    fontWeight: 'bold',
    [theme.breakpoints.up('xl')]: {
      fontSize : "12px"
    }
  },
  cardRole: {
    fontSize: '8px',
    fontWeight: 'bold',
    [theme.breakpoints.up('xl')]: {
      fontSize : "10px"
    }
  },
  cardParagraph: {
    fontSize: '10px',
    fontWeight: 'bold',
    textAlign: 'center',
    padding: '0 5px 0 5px',
    marginTop: '10px',
    [theme.breakpoints.up('lg')]: {
      fontSize : "12px"
    }
  },
  guaranteeTitle: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    color: "#2ca01c",
    fontSize: "16px",
    marginTop: '14px'
  },
  logo: {
    width: '87px'
  },
  cardParagraphGrid: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardBorder: {
    borderTop: '3px solid  #86e179',
    position: 'inherit',
    bottom: 35
  },
  activationFeeLabel : {
    paddingLeft: "37px", 
    paddingTop: "12px", 
    fontWeight: "bold",
    [theme.breakpoints.up('xl')]: {
      fontSize : "20px"
    }
  },
  demoLink: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#2ca01c',
    fontWeight: 700,
    fontSize: '14px', 
    textAlign: 'center',
  },
  demoLinkText: {
    cursor: 'pointer',
    textDecoration: 'underline',
    '&:hover': {
      textDecoration: 'none'
    }
  }
}));

export default useStyles;

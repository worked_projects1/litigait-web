import React, { useState } from 'react';
import Styles from './styles';
import { Modal, Paper, Grid, Card, Typography, CardContent, Checkbox, FormControlLabel, Divider, Avatar, useMediaQuery, useTheme, Fade, Button } from '@material-ui/core';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop } from '@material-ui/core';
import Icons from './utils';


export default function PricingModalDialog(props) {
    const { title, children, onOpen, onClose, show, disableCancelBtn, paperClassName, handleSubscription, openSubscriptionForm, upShellColumns, user } = props;
    const { features, details, guaranteeText, description, headLine } = upShellColumns;
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    const xl = useMediaQuery(theme.breakpoints.up('xl'));

    const billingType = user?.practiceDetails?.billing_type === 'limited_users_billing';

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    return (
        <div>
            {children && children(() => setModalOpen(!showModal))}
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={showModal || show || false}
                className={classes.modal}
                onClose={closeModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                onRendered={onOpen}
                BackdropProps={{
                    timeout: 500,
                }}>
                <Fade in={showModal || show || false}>
                    <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                        <Grid container className={classes.header} justify="space-between">
                            <Grid item xs={10}>
                                <Typography component="span" className={classes.logo}>{title || ''}</Typography>
                            </Grid>
                            <Grid item xs={2} style={{ textAlign: 'end' }}>
                                {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                            </Grid>

                            <Grid container justify="center">
                                <Grid item >
                                    <Typography component="h1" variant="h5" className={classes.title}>
                                        {headLine}
                                    </Typography>
                                </Grid>
                            </Grid>

                        </Grid>
                        <Grid container spacing={4} className={`${classes.body} ${classes.scrollBar}`} justify='center'>
                            <Grid item xs={12} md={6}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={6}>
                                        <Grid className={classes.bestValue}>
                                            <Typography
                                                component="div"
                                                style={{
                                                    fontWeight: 'bold',
                                                    fontSize: '16px',
                                                    padding: '8px'
                                                }}
                                                className={classes.centerAlign}>
                                                Best Value
                                            </Typography>
                                            <Card className={classes.cardYearly}>
                                                <CardContent>
                                                    <Typography
                                                        variant="h3"
                                                        className={classes.tekPlanYearly}>
                                                        TEK YEARLY
                                                    </Typography>
                                                    <Typography variant="h2" className={classes.price}>$425</Typography>
                                                    <Typography
                                                        component="span"
                                                        className={classes.billedPlan}>
                                                        Billed Annually
                                                    </Typography>
                                                    {billingType ? <Grid item style={{ marginBottom: '10px', textAlign: 'center' }}>
                                                        <div>Additional users</div>
                                                        <div>$79/user/month</div>
                                                    </Grid> : null}
                                                    <Grid className={classes.centerAlign}>
                                                        <Grid item className={classes.saveMoney}>
                                                            <Avatar className={classes.fireEmoji} src={require('images/pricing/fire_emoji.png')} /><Typography style={xl ? {fontSize : "20px"} : {}}>
                                                                save $840/year
                                                            </Typography>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid>
                                                        {features && features.map((item, i) => (<Grid key={i}>
                                                            <FormControlLabel
                                                                control={<Checkbox
                                                                    checkedIcon={<CheckBoxOutlinedIcon />}
                                                                    variant='outlined'
                                                                    style={{ color: "#2ca01c" }}
                                                                    disabled={true}
                                                                    className={classes.checkboxYearly}
                                                                    checked={true}
                                                                />}
                                                                label={<Typography component="span" className={classes.checkLabel}>{item}</Typography>} />
                                                        </Grid>
                                                        ))}
                                                    </Grid>

                                                    <Grid container className={classes.listContainer}>
                                                        <Grid item xs={12} className ={classes.activationFeeLabel} >
                                                            $249 Activation Fee
                                                        </Grid>
                                                    </Grid>
                                                    <Grid className={classes.centerAlign}>
                                                        <Button type="button" color="primary" variant="contained" className={classes.buttonGetStart} onClick={() => handleSubscription(setModalOpen, openSubscriptionForm, 'yearly')}>GET STARTED</Button>
                                                    </Grid>
                                                </CardContent>
                                            </Card>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Card className={classes.cardMonthly}>
                                            <CardContent className={classes.cardContent}>
                                                <Typography
                                                    style={{ color: "#2ca01c" }}
                                                    variant="h3"
                                                    className={classes.tekPlanMonthly}>
                                                    TEK MONTHLY
                                                </Typography>
                                                <Typography variant="h2" className={classes.price}>$495</Typography>
                                                <Typography
                                                    component="span"
                                                    className={classes.billedPlan}>
                                                    Billed Monthly
                                                </Typography>
                                                {billingType ? <Grid item style={{ marginBottom: '10px', textAlign: 'center' }}>
                                                    <div>Additional users</div>
                                                    <div>$89/user/month</div>
                                                </Grid> : null}
                                                <Grid>
                                                    <Divider className={classes.divider} />
                                                </Grid>
                                                <Grid>
                                                    {features && features.map((item, i) => (<Grid key={i}>
                                                        <FormControlLabel
                                                            control={<Checkbox
                                                                disabled={true}
                                                                size="small"
                                                                style={{ color: "#2ca01c" }}
                                                                className={classes.checkboxMonthly}
                                                                checked={true}
                                                            />}
                                                            label={<Typography
                                                                className={classes.checkLabel}>{item}</Typography>} />
                                                    </Grid>))}
                                                </Grid>

                                                <Grid container className={classes.listContainer}>
                                                    <Grid item xs={12} className ={classes.activationFeeLabel}>
                                                        $249 Activation Fee
                                                    </Grid>
                                                </Grid>

                                                <Grid className={classes.centerAlign}>
                                                    <Button type="button" color="primary" variant="contained" className={classes.buttonGetStart} onClick={() => handleSubscription(setModalOpen, openSubscriptionForm, 'monthly')}>GET STARTED</Button>
                                                </Grid>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} md={5}>
                                <Grid container spacing={2} justify='center'>
                                    {details && details.map((detail, i) => {
                                        return <Grid item xs={12} sm={6} key={i}>
                                            <Grid container className={classes.cardAvatar}>
                                                <Grid item xs={12}>
                                                    <Grid container style={{ position: 'relative' }}>
                                                        <Grid item xs={5} className={classes.avatarGrid}>
                                                            <div className={classes.avatarBorder}>
                                                                <Icons type={detail?.profile} classes={classes.profile} />
                                                            </div>
                                                        </Grid>
                                                        <Grid item xs={7}>
                                                            <Grid container>
                                                                <Grid item xs={12}>
                                                                    <Icons type={detail?.logo} classes={classes.logo} />
                                                                </Grid>
                                                                <Grid item xs={12} className={classes.cardName}>
                                                                    <Typography component="span"
                                                                        className={classes.cardNameTypo}>
                                                                        {detail.name}
                                                                    </Typography>
                                                                    <Typography
                                                                        className={classes.cardRole}>
                                                                        {detail.role}
                                                                    </Typography>
                                                                    <Typography>
                                                                        <Divider className={classes.dividerCard} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item xs={12} className={classes.cardBorder}>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12} className={classes.cardParagraphGrid}>
                                                    <Typography
                                                        component="span"
                                                        className={classes.cardParagraph}>
                                                        <span dangerouslySetInnerHTML={{ __html: detail.comment }} /><br></br>
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={12} className={classes.centerAlign} style={{ dispaly: 'inline-block', margin: '-3px', paddingBottom: '5px' }}>
                                                    {Array.from(Array(5)).map((opt, index) => {
                                                        return <Grid key={index}>
                                                            <Icons type={detail?.rating} classes={classes.star} />
                                                        </Grid>
                                                    })}
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    })}
                                </Grid>
                                <Grid className={classes.guaranteeGrid}>
                                    <Icons type="guarantee" classes={classes.guaranteeImg} />
                                </Grid>
                                <Typography component='span' style={{ fontWeight: 'bold', color: "#2ca01c", fontSize: "16px" }} className={classes.guaranteeTitle}>
                                    {guaranteeText}
                                </Typography>
                                <Typography component="span" style={{ fontSize: '14px', textAlign: 'center' }} className={classes.centerAlign}>
                                    <span style={{ fontSize: '13px' }} dangerouslySetInnerHTML={{ __html: description }} />
                                </Typography>
                                <Typography component="span" className={classes.demoLink}>
                                    <span className={classes.demoLinkText} onClick={() => window.open('https://www.esquiretek.com/request-a-demo/', '_blank')}>Request a Demo</span>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                </Fade>
            </Modal>
        </div >
    );
}


/**
 * 
 * Edit Record Page
 * 
 */


import React, { useState, useEffect, memo } from 'react';
import { connect } from 'react-redux';
import EditRecordForm from 'components/EditRecordForm';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import moment from 'moment';
import { AsYouType } from 'libphonenumber-js';
import { getFormValues } from 'redux-form';
import { resolveColumns, mycaseSessionValidation } from 'utils/tools';
import { validation, clientNameDetails } from '../CreateRecordPage/utils';
import { selectUser } from 'blocks/session/selectors';

const userEditName = ['users.edit', 'esquireTekUsers.edit', 'practiceUsers.edit'];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, decoratorView, updatePracticeUserBtn) {

    const { selectRecord, selectRecordsMetaData, selectRecords, selectFilevineSessionForm, selectLitifyInfoDialog } = selectors;
    function handleEdit(queryParams, integration, user, record, dispatch, { form }) {
        const filevineDetails = user && user.filevineDetails && user.filevineDetails || false;
        let submitRecord = Object.assign({}, { ...record });
        const { myCaseCredentials, mycaseSessionDiff, editPage  } = integration;
        if (name === 'clients.edit' || name === 'other-parties.edit') {
            if (submitRecord.phone) {
                const phoneType = new AsYouType('US');
                phoneType.input(submitRecord.phone);
                const phoneVal = phoneType.getNumber().nationalNumber;
                submitRecord.phone = phoneVal || submitRecord.phone;
            }
            delete submitRecord.name;
            delete submitRecord.address;
            submitRecord.createdAt = new Date(moment(submitRecord.createdAt).format('MM/DD/YYYY'));
            submitRecord.dob = submitRecord.dob && new Date(moment(submitRecord.dob).format('MM/DD/YYYY'));
            submitRecord.dob = submitRecord.dob == '' ? null : submitRecord.dob;
        } else if (name === 'practices.edit') {
            if (submitRecord.phone) {
                const phoneType = new AsYouType('US');
                phoneType.input(submitRecord.phone);
                const phoneVal = phoneType.getNumber().nationalNumber;
                submitRecord.phone = phoneVal || submitRecord.phone;
            }
            if (submitRecord.fax) {
                const faxType = new AsYouType('US');
                faxType.input(record.fax);
                const faxVal = faxType.getNumber().nationalNumber;
                record.fax = faxVal || record.fax;
            }
        } else if (name === 'discountCode.edit') {
            submitRecord = Object.assign({}, { id: submitRecord.id, discount_code: submitRecord.discount_code, discount_percentage: submitRecord.discount_percentage, plan_type: submitRecord.plan_type });
        } else if (userEditName.includes(name) && submitRecord.role === 'paralegal') {
            submitRecord.state_bar_number = '';
        }

        if (editPage && record?.client_from === 'mycase' && !mycaseSessionDiff) {
            dispatch(actions.mycaseRedirectAuthUrI(true, true));
        } else if (editPage && record?.client_from === 'filevine' && !filevineDetails) {
            dispatch(actions.setFilevineSessionForm(true));
        } else if (name === 'clients.edit' && record?.client_from === 'litify') {
            dispatch(actions.updateLitifyRecord(submitRecord, form));
        } else {
            (name === 'respondingCustomTemplate.edit' || name === 'propoundingCustomTemplate.edit') && submitRecord ? dispatch(actions.updatePracticeModifiedTemplate(submitRecord, form, queryParams)) : (name === 'respondingTemplate.edit' || name === 'propoundingTemplate.edit') ? dispatch(actions.updateCustomTemplate(submitRecord, form, actions.loadRecords, queryParams)) : dispatch(actions.updateRecord(submitRecord, form))
        }
    }

    /**
     * 
     * @param {object} props 
     */
    function EditRecordPage(props) {

        const { location = {}, record, metaData, dispatch, formRecord = {}, match, queryParams, records, user } = props;
        const [loading, setLoading] = useState(true);
        const fields = resolveColumns(columns, formRecord, location.state, metaData).filter(_ => _.editRecord);
        const editPage = ['clients.edit', 'other-parties.edit'].includes(name);
        const client_from = record?.client_from && ['mycase'].includes(record?.client_from) || false;
        const { myCaseCredentials, mycaseSessionDiff } = user && mycaseSessionValidation(user);
        const initialRecords = userEditName.includes(name) ? Object.assign({}, record, { password: '' }) : editPage ? clientNameDetails(record) : record || {};

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecord(match.params.id, queryParams));
            dispatch(actions.loadRecords(false, queryParams));
            setLoading(true);
            setTimeout(() => setLoading(false), 200);
            return () => mounted = false;
        }, [match.params.id]);

        return (<div>
            {decoratorView && decoratorView?.length > 0 && decoratorView.map((element) => React.createElement(element, Object.assign({}, props, {actions})))}
            {!loading && record && Object.keys(record).length > 0 ?
                <EditRecordForm
                    initialValues={initialRecords}
                    form={`editRecord_${record.id}`}
                    name={name}
                    path={path}
                    metaData={metaData}
                    fields={fields}
                    onSubmit={handleEdit.bind(this, queryParams, Object.assign({}, {  mycaseSessionDiff, editPage }), user)}
                    locationState={location.state}
                    records={records}
                    confirmMessage={client_from && editPage ? (!mycaseSessionDiff ? `We couldn’t update this change to your MyCase account due to session expired. Please click Activate button that will redirect to MyCase site. Once you logged in, you can try to update the client again.` : false) : false}
                    confirmButton={client_from && editPage ? (mycaseSessionDiff ? false : true ) : false}
                    formRecord={formRecord}
                    actions={actions}
                    dispatch={dispatch}
                    updatePracticeUserBtn={updatePracticeUserBtn}
                /> : 'Loading...'}
        </div>)

    }

    EditRecordPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        formRecord: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        record: PropTypes.object,
        filevineSessionForm: PropTypes.bool,
        litifyInfoDialog: PropTypes.bool,
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        formRecord: (state, props) => {
            const record = selectRecord()(state);
            return getFormValues(`editRecord_${record.id}`)(state)
        },
        metaData: selectRecordsMetaData(),
        records: selectRecords(),
        user: selectUser(),
        filevineSessionForm: selectFilevineSessionForm(),
        litifyInfoDialog: selectLitifyInfoDialog(),
    });

    const withConnect = connect(
        mapStateToProps,
    );

    return compose(
        withConnect,
        memo
    )(EditRecordPage);

}
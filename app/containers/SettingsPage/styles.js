import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    create: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular'
    },
    form: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        padding: '18px',
        marginTop: '40px'
    },
    settings: {
        paddingLeft: '25px',
        paddingRight: '25px'
    },
    row: {
        alignItems: 'center'
    },
    row1: {
        alignItems: 'center',
        maxWidth: '65%'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none'
    },
    table: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        marginTop: '40px',
        overflow: 'auto',
        marginTop: '30px',
    },
    edit: {
        padding: '20px',
        paddingTop: '0px'
    },
    img: {
        height: 64,
        paddingLeft: '30px',
    },
    section: {
        marginTop: '15px'
    },
    section2: {
        marginTop: '60px'
    },
    label: {
        fontWeight: 'bold',
        fontSize: '16px'
    },
    value: {
        paddingLeft: '25px',
        fontWeight: 'bold',
        fontSize: '14px'
    },
    textarea: {
        fontWeight: 'bold',
        fontSize: '14px',
        paddingLeft: '0px',
    },
    columns: {
        paddingTop: '25px'
    },
    name: {
        fontSize: '22px'
    },
    spinner: {
        marginTop: '75px'
    },
    icons: {
        height: '30px',
        color: '#2CA01C',
        cursor: 'pointer'
    },
    iconsGray: {
        height: '30px',
        color: 'gray',
        cursor: 'pointer',
        margin: '0px 10px'
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        backgroundColor: 'gray !important'
    },
    Button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular',
        backgroundColor: 'gray !important',
        marginRight: '10px'
    },
    footerCls: {
        display: 'flex',
        marginLeft: 'auto'
    },
    propoundNote: {
        paddingTop: '12px',
        fontSize: '14px',
        fontWeight: 'bold'
    },
    switchBtn: {
        '@global': {
            '.MuiSwitch-colorPrimary.Mui-checked': {
                color: '#2ca01c'
            },
            '.MuiSwitch-colorPrimary.Mui-checked + .MuiSwitch-track': {
                backgroundColor: '#2ca01c'
            }
        }
    },
    linkColor: {
        color: '#2ca01c',
        textDecoration: 'underline',
        cursor: 'pointer',
        '&:hover': {
            textDecoration: 'none'
        }
    }
}));

export default useStyles;
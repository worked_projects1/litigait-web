/**
 * 
 * Settings Page
 * 
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import EditRecordForm from 'components/EditRecordForm';
import Snackbar from 'components/Snackbar';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import Spinner from 'components/Spinner';
import { getFormValues, change } from 'redux-form';
import { AsYouType } from 'libphonenumber-js';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Icons from 'components/Icons';
import ButtonSpinner from 'components/ButtonSpinner';
import AlertDialog from 'components/AlertDialog';
import Skeleton from '@material-ui/lab/Skeleton';
import ModalForm from 'components/ModalRecordForm';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {

    const {
        selectLoading,
        selectPractice,
        selectUpdateError,
        selectSuccess,
        selectUser,
        selectVersion,
        selectBilling,
        selectProgress,
        selectRespondingSubscriptions,
        selectPropoundingSubscriptions,
        selectRecordsMetaData,
        selectSettingsDiscount,
        selectLicenseDetails
    } = selectors;

    /**
     * 
     * @param {object} props 
     * @returns 
     */
    function Settings(props) {

        const classes = Styles();
        const { dispatch, location = {}, user, loading, error, success, billing = {}, progress, responding_subscriptions = {}, propounding_subscriptions = {}, metaData = {}, responding_subscriptions_form = {}, propounding_subscriptions_form = {}, discountDetail, practice, licenseDetails } = props;
        const [showForm, setShowForm] = useState(false);
        const [formLoader, setFormLoader] = useState(false);
        const [showPopup, setShowPopup] = useState(false);
        const [licenseForm, setLicenseForm] = useState(false);
        const stripe = useStripe();
        const elements = useElements();
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const freePlans = ['tek_as_you_go', 'pay_as_you_go'];
        const changeFormNode = (showForm === 2 && responding_subscriptions_form && freePlans && freePlans.includes(responding_subscriptions_form.plan_id) && responding_subscriptions_form.terms) || (showForm === 3 && propounding_subscriptions_form && freePlans && freePlans.includes(propounding_subscriptions_form.plan_id) && propounding_subscriptions_form.terms);

        const formValues = showForm === 2 ? responding_subscriptions_form : showForm === 3 ? propounding_subscriptions_form : false;

        const planType = responding_subscriptions && responding_subscriptions['planDetails'] && responding_subscriptions['planDetails']['plan_type'];
        
        const { propoundingPlansOptions: propoundPlans = [], respondingPlansOptions: respondPlans = [] } = metaData;

        let playType = user?.plan_type?.responding;
        playType = playType === 'free_trial' || playType === '' || !playType;
        const licenseCount = user?.practiceDetails?.license_count || 0;

        async function handleEdit(record, dispatch, { form }) {
            if (showForm === 1) {

                setFormLoader(true);
                if (record.phone) {
                    const phoneType = new AsYouType('US');
                    phoneType.input(record.phone);
                    const phoneVal = phoneType.getNumber().nationalNumber;
                    record.phone = phoneVal || record.phone;
                }
                if (record.fax) {
                    const faxType = new AsYouType('US');
                    faxType.input(record.fax);
                    const faxVal = faxType.getNumber().nationalNumber;
                    record.fax = faxVal || record.fax;
                }
                dispatch(actions.updatePractice(record, form, setShowForm, setFormLoader));
            } else if (showForm === 2) {

                setFormLoader(true);
                const currentPlan = responding_subscriptions && responding_subscriptions['plan_id'] && respondPlans && Array.isArray(respondPlans) && respondPlans.length > 0 && respondPlans.find(_ => _.plan_id === responding_subscriptions['plan_id']) || false;

                const selectedPlan = record && record['plan_id'] && respondPlans && Array.isArray(respondPlans) && respondPlans.length > 0 && respondPlans.find(_ => _.plan_id === record['plan_id']) || false;

                if (currentPlan && currentPlan.validity === 0 || !currentPlan && selectedPlan && selectedPlan.validity) {
                    dispatch(actions.createSubscriptions(Object.assign({}, { plan_id: record.plan_id, page: 'settings', selected_plan: record.plan_id, plan_category: selectedPlan.plan_category, discount_code: record.discount_code, terms: true }), form, setShowForm, setFormLoader));
                } else if (selectedPlan && currentPlan && selectedPlan.validity && currentPlan.validity && selectedPlan.validity > currentPlan.validity) {
                    dispatch(actions.updateSubscriptions(Object.assign({}, record, { request_type: 'upgrade', new_plan_id: record.plan_id, page: 'settings', selected_plan: record.plan_id, plan_category: selectedPlan.plan_category, terms: true }), form, setShowForm, setFormLoader));
                } else if (selectedPlan && currentPlan && selectedPlan.validity && currentPlan.validity && selectedPlan.validity < currentPlan.validity) {
                    dispatch(actions.updateSubscriptions(Object.assign({}, record, { request_type: 'downgrade', new_plan_id: record.plan_id, page: 'settings', selected_plan: record.plan_id, plan_category: selectedPlan.plan_category, terms: true }), form, setShowForm, setFormLoader));
                } else if (selectedPlan && selectedPlan.validity === 0) {
                    dispatch(actions.cancelSubscriptions(Object.assign({}, record, { request_type: 'downgrade', new_plan_id: record.plan_id, cancel_at_period_end: true, page: 'settings', selected_plan: record.plan_id, plan_category: selectedPlan.plan_category }), form, setShowForm, setFormLoader));
                } else if (selectedPlan && currentPlan && selectedPlan.validity && currentPlan.validity && selectedPlan.validity == currentPlan.validity) {
                    dispatch(actions.updateSubscriptions(Object.assign({}, record, { request_type: 'upgrade', new_plan_id: record.plan_id, page: 'settings', selected_plan: record.plan_id, plan_category: selectedPlan.plan_category, terms: true }), form, setShowForm, setFormLoader));
                } else if (selectedPlan && !currentPlan && selectedPlan.validity) {
                    dispatch(actions.updateSubscriptions(Object.assign({}, record, { request_type: 'upgrade', new_plan_id: record.plan_id, page: 'settings', selected_plan: record.plan_id, plan_category: selectedPlan.plan_category, terms: true }), form, setShowForm, setFormLoader));
                } else {
                    setFormLoader(false);
                }
            } else if (showForm === 3) {
                setFormLoader(true);

                const currentRespondingPlan = responding_subscriptions && responding_subscriptions['plan_id'] && respondPlans && Array.isArray(respondPlans) && respondPlans.length > 0 && respondPlans.find(_ => _.plan_id === responding_subscriptions['plan_id']) || false;

                const currentPlan = propounding_subscriptions && propounding_subscriptions['plan_id'] && propoundPlans && Array.isArray(propoundPlans) && propoundPlans.length > 0 && propoundPlans.find(_ => _.plan_id === propounding_subscriptions['plan_id']) || false;
                const selectedPlan = currentRespondingPlan && currentRespondingPlan['plan_id'] && propoundPlans && Array.isArray(propoundPlans) && propoundPlans.length > 0 && propoundPlans.find(_ => _.validity === currentRespondingPlan['validity']) || false;

                if (selectedPlan && !currentPlan && selectedPlan.validity) {
                    dispatch(actions.createPropoundSubscriptions(Object.assign({}, { plan_id: selectedPlan.plan_id, page: 'settings', plan_category: selectedPlan.plan_category, discount_code: record.discount_code, terms: true }), form, setShowForm, setFormLoader));
                } else if (selectedPlan && currentPlan && selectedPlan.validity && currentPlan.validity && selectedPlan.validity > currentPlan.validity) {
                    dispatch(actions.updateSubscriptions(Object.assign({}, selectedPlan, { request_type: 'upgrade', new_plan_id: selectedPlan.plan_id, page: 'settings', plan_category: selectedPlan.plan_category, terms: true }), form, setShowForm, setFormLoader));
                } else {
                    setFormLoader(false);
                }
            } else if (showForm === 5) {
                const result = await stripe.confirmCardSetup(billing.client_secret, {
                    payment_method: {
                        card: elements.getElement(CardElement),
                        billing_details: Object.assign({}, { name: record.name, email: record.email, phone: record.phone }),
                    }
                });
                dispatch(actions.updateBillingDetails(result));
                setShowForm(false);
            }  else if (showForm === 6) {
                dispatch(actions.updateVersion(record, form));
                setShowForm(false);
            }
        }

        const openAlertDialog = ({ submitRecord, dialog, form, handleSubmitForm }) => {
            let submitRecords = Object.assign({}, { ...submitRecord }, { practice_id: practice?.id });
            if (form === 'SettingsForm_3') {
                const currentRespondingPlan = responding_subscriptions && responding_subscriptions['plan_id'] && respondPlans && Array.isArray(respondPlans) && respondPlans.length > 0 && respondPlans.find(_ => _.plan_id === responding_subscriptions['plan_id']) || false;

                const propoundPlan = currentRespondingPlan && currentRespondingPlan['plan_id'] && propoundPlans && Array.isArray(propoundPlans) && propoundPlans.length > 0 && propoundPlans.find(_ => _.validity === currentRespondingPlan['validity']) || false;
                submitRecords = Object.assign({}, { ...submitRecords, plan_id: propoundPlan.plan_id, type: 'propounding' })
            }
            if (form === 'SettingsForm_2') {
                submitRecords = Object.assign({}, { ...submitRecords, type: 'responding' })
            }
            if(practice?.billing_type === 'limited_users_billing') {
                dispatch(actions.subscriptionLicenseValidation(Object.assign({}, submitRecords), form, dialog, billing.credit_card_details_available, handleSubmitForm));
            } else {
                dispatch(actions.settingsDiscountCode(Object.assign({}, submitRecords), form, dialog, billing.credit_card_details_available, handleSubmitForm));
            }
            
        }

        const licenseAlertDialog = ({ submitRecord, dialog, form }) => {
            let submitRecords = Object.assign({}, { ...submitRecord }, { practice_id: practice?.id });
            dispatch(actions.addMoreLicenseValidation(submitRecords, dialog, form));
        }

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadPractice(user && user.practice_id));
            dispatch(actions.loadBillingDetails(false, false, false, { document_type: 'FROGS' }, 'template'));
            dispatch(actions.loadSubscriptions());
            return () => mounted = false;
        }, []);

        const columValue = (column, row) => {
            switch (column.type) {
                case 'upload':
                    return column.html && column.html(row, metaData) || (
                        <img
                            src={row[column.value] || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                            role="presentation"
                            className={classes.img}
                        />
                    );
                case 'checkbox':
                    return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value]}</p>;
                case 'select':
                    return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value]}</p>;
                case 'textarea':
                    return column.html && column.html(row, metaData) || <p className={classes.textarea}>{row[column.value] != null && row[column.value] != '' ? lineBreak(row[column.value]) : '-'}</p>;
                default:
                    return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value] != null && row[column.value] != '' ? row[column.value] : '-'}</p>;
            }
        };

        const lineBreak = (val) => {
            return val && val.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos}>{line}</p>)
        }

        const section = (columns && typeof columns === 'function' && columns(user, Object.assign({}, { ...props }, { actions })).section) || (columns && typeof columns === 'object' && columns.section) || false;

        if (showForm && changeFormNode) {
            dispatch(change(`SettingsForm_${showForm}`, 'terms', false));
        }

        const handleCancelSubscription = (record, type) => {
            setFormLoader(type);
            const propound = type.includes('propounding') ? true : false;
            dispatch(actions.practiceCancelSubscription(record, setFormLoader, propound));
        }

        const showCancelSubscriptions = (tabValue) => {
            return tabValue['stripe_subscription_id'].startsWith('sub_') && tabValue['stripe_subscription_data'] && !tabValue['stripe_subscription_data']['cancel_at_period_end'] || tabValue['stripe_subscription_id'].startsWith('pi_') && practice && !practice.is_propounding_canceled || false
        }

        const showLicense = (subscriptionData) => {
            return subscriptionData?.['stripe_subscription_id']?.startsWith('sub_') && subscriptionData?.['stripe_subscription_data'] && !subscriptionData?.['stripe_subscription_data']?.['cancel_at_period_end'] || false;
        }

        const handleBuyLiecense = (data, dispatch, { form }) => {
            dispatch(actions.addMoreLicense(Object.assign({}, { ...data }, { practice_id: practice?.id }), setLicenseForm, form));
        }

        return section && section.map((tab, index) =>
            tab.view && (<Grid key={index} className={index === 0 ? classes.section : classes.section2} container>
                <Grid item xs={12}>
                    <Grid container justify="space-between">
                        <Typography component="h1" variant="h5" className={classes.name}>
                            {tab?.name && typeof tab.name === 'function' ? tab.name() : tab.name}
                        </Typography>
                        {!progress ? tab.value && tab.value.includes('subscriptions') && props[`${tab.value}`] && props[`${tab.value}`]['stripe_subscription_id'] && showCancelSubscriptions(props[`${tab.value}`]) &&
                            <Grid className={classes.footerCls}>
                                <AlertDialog
                                    description={tab.alertMessage && props[`${tab.value}`] && typeof tab.alertMessage === 'function' && tab.alertMessage(props[tab.value], Object.assign({}, { responding_subscriptions, propounding_subscriptions })) || tab.confirmMessage}
                                    onConfirm={() => handleCancelSubscription(props[`${tab.value}`], tab.value)}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='YES'
                                    btnLabel2='NO'>
                                    {(open) =>
                                        sm ? <Button type="button" variant="contained" color="primary" className={classes.Button} onClick={open}>
                                            {formLoader == tab.value ? <ButtonSpinner /> : 'Cancel Subscription'}
                                        </Button> : (formLoader == tab.value ? <ButtonSpinner color='#2ca01c' /> : <Icons type='Cancel' className={classes.iconsGray} onClick={open} />)
                                    }
                                </AlertDialog>
                            </Grid> : null
                        }
                        {(tab.edit && typeof tab.edit === 'function' ? tab.edit(props[tab.value], responding_subscriptions) : tab.edit) ? tab.infoMessage && tab.customAlertMessage && (['tek_as_you_go', 'pay_as_you_go', 'free_trial'].includes(planType) || !planType) ?
                            <AlertDialog
                                description={tab.customAlertMessage && typeof tab.customAlertMessage === 'function' && tab.customAlertMessage(planType)}
                                onConfirm={() => setShowForm(false)}
                                onConfirmPopUpClose={true}
                                btnLabel1='OK'>
                                {(open) => sm ? <Button
                                        type="button"
                                        variant="contained"
                                        color="primary"
                                        disabled={progress}
                                        onClick={open}
                                        className={classes.create} >
                                        {tab.label && typeof tab.label === 'function' ? tab.label(props[tab.value]) : tab.viewBtnLabel ? `Change ${tab.viewBtnLabel}` : `Change ${tab.name}`}
                                    </Button> : <Icons type='Edit' className={classes.icons} onClick={open} />}
                            </AlertDialog> :
                            sm ? <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    disabled={progress}
                                    onClick={() => setShowForm(tab.schemaId)}
                                    className={classes.create} >
                                    {tab.label && typeof tab.label === 'function' ? tab.label(props[tab.value]) : tab.viewBtnLabel ? `Change ${tab.viewBtnLabel}` : `Change ${tab.name}`}
                                </Button> : <Icons type='Edit' className={classes.icons} onClick={() => setShowForm(tab.schemaId)}/> : null
                        }
                    </Grid>
                </Grid>
                <Grid item xs={12} md={showForm ? 6 : 12} className={classes.columns}>
                    {(typeof tab.columns === 'function' && tab.columns(props[tab.value]) || tab.columns).filter(_ => _.viewRecord).map((col, index) => {
                        const Component = col?.render || false;
                        return <Grid key={index}>
                            <Grid className={classes.settings}>
                                <Grid container className={showForm ? classes.row : classes.row1}>
                                    <Grid item xs={6} md={showForm ? 4 : 4} className={classes.label}>
                                        {col.label}:
                                    </Grid>
                                    <Grid item xs={6} md={showForm ? 8 : 8} className={classes.value}>
                                        {progress ? <Skeleton animation="wave" height={40} width={sm ? 280 : 80} /> : col.responseTracking ? Component && <Component record={props[tab.value] && tab.filter(props[tab.value])} classes={classes} /> || null : columValue(col, props[tab.value] && tab.filter(props[tab.value]))}
                                        {!progress && typeof showLicense === 'function' && showLicense(props[`${tab.value}`]) && col?.value === 'license_count' && col?.viewRecord ? <p className={classes.value}>
                                            <span 
                                                className={classes.linkColor} 
                                                onClick={() => {
                                                    setLicenseForm(true);
                                                    setShowForm(false);
                                                }}
                                            >
                                                {col.subLabel}
                                            </span>
                                        </p> : null}
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} md={showForm ? 11 : 8} ><hr className={classes.hr} /></Grid>
                        </Grid>
                    })}
                </Grid>
                {!loading && props[tab.value] && Object.keys(props[tab.value]).length > 0 && showForm === tab.schemaId ?
                    <Grid item xs={12} md={showForm ? 6 : 12} className={classes.table}>
                        <Grid className={classes.edit}>
                            <EditRecordForm
                                initialValues={tab.filter(props[tab.value]) || {}}
                                form={`SettingsForm_${tab.schemaId}`}
                                name={name}
                                path={path}
                                metaData={metaData}
                                fields={typeof tab.columns === 'function' && tab.columns(props[`${tab.value}_form`], props[tab.value], props).filter(_ => _.editRecord) || tab.columns.filter(_ => _.editRecord)}
                                confirmButton={true}
                                confirmMessage={tab.customConfirmMessage && props[`${tab.value}_form`] && tab.customConfirmMessage(props[tab.value], metaData, props[`${tab.value}_form`], discountDetail, responding_subscriptions, propounding_subscriptions, props) || tab.confirmMessage}
                                btnLabel={tab.label && typeof tab.label === 'function' ? tab.label(props[tab.value]) : "Update"}
                                onSubmit={handleEdit.bind(this)}
                                locationState={location.state}
                                spinner={formLoader}
                                enableSubmitBtn={tab && tab.enableSubmitBtn || false}
                                disableSubmitBtn={tab?.disableSubmitBtn && typeof tab?.disableSubmitBtn === 'function' ? tab?.disableSubmitBtn(props[`${tab.value}_form`], props[`${tab.value}`]) : tab?.disableSubmitBtn || false}
                                submitBtn={tab.discountCode && typeof tab.discountCode === 'function' && tab.discountCode(props[`${tab.value}_form`]) ? (submitBtnProps) => {
                                    const { confirmMessage, handleSubmit, invalid, submitting, spinner, pristine, btnLabel, classes: styleClasses, enableSubmitBtn, disableSubmitBtn } = submitBtnProps;
                                    return <AlertDialog
                                        description={confirmMessage}
                                        onConfirm={handleSubmit}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'No'}
                                        openDialog={showPopup}
                                        closeDialog={() => setShowPopup(false)}
                                        onConfirmPopUpClose >
                                        {(openDialog) => {
                                            return <Button type="button" variant="contained" color="primary" className={styleClasses.submitBtn} onClick={() => openAlertDialog(Object.assign({}, { dialog: setShowPopup, submitRecord: formValues, form: `SettingsForm_${tab.schemaId}`, handleSubmitForm: handleSubmit }))} disabled={disableSubmitBtn || !enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid) }>
                                                {(submitting || spinner) && <ButtonSpinner /> || btnLabel || 'submit'}
                                            </Button>
                                        }}
                                    </AlertDialog>
                                } : null}
                                cancelBtn={() => <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setShowForm(false)}
                                    className={classes.cancelBtn}>
                                    Cancel
                                </Button>}
                            />
                        </Grid>
                    </Grid> : null}
                    {!loading && props[tab.value] && Object.keys(props[tab.value]).length > 0 && tab?.buyLicenseColumns && licenseForm ? <ModalForm
                        title={'Buy Additional License'}
                        form="buyLicenseForm"
                        show={licenseForm}
                        fields={typeof tab.buyLicenseColumns === 'function' && tab.buyLicenseColumns(props[`license_form`], Object.assign({}, { license_count: licenseCount }), props).filter(_ => _.editRecord) || tab.buyLicenseColumns.filter(_ => _.editRecord)}
                        onSubmit={handleBuyLiecense}
                        btnLabel="Purchase"
                        style={{ paddingRight: '0px' }}
                        onClose={() => setLicenseForm(false)}
                        metaData={metaData}
                        confirmButton
                        confirmMessage={tab?.buyCustomLicenseConfirmationMessage && typeof tab?.buyCustomLicenseConfirmationMessage === 'function' && tab?.buyCustomLicenseConfirmationMessage(props?.license_form, licenseDetails, metaData, responding_subscriptions, propounding_subscriptions, props)}
                        disableSubmitBtn
                        submitBtn={(submitBtnProps) => {
                            const { confirmMessage, handleSubmit, invalid, submitting, spinner, pristine, btnLabel, classes: styleClasses } = submitBtnProps;
                            return <AlertDialog
                                description={confirmMessage}
                                onConfirm={handleSubmit}
                                btnLabel1={'Yes'}
                                btnLabel2={'No'}
                                openDialog={showPopup}
                                closeDialog={() => {
                                    setShowPopup(false);
                                    dispatch(actions.addMoreLicenseValidationSuccess({}));
                                }}
                                onConfirmPopUpClose >
                                {(openDialog) => {
                                    return <Button type="button" variant="contained" color="primary" className={styleClasses.submitBtn} onClick={() => licenseAlertDialog(Object.assign({}, { dialog: setShowPopup, submitRecord: props?.license_form, form: `buyLicenseForm` }))} disabled={pristine || submitting || (!pristine && invalid)}>
                                        {(submitting || spinner) && <ButtonSpinner /> || btnLabel || 'submit'}
                                    </Button>
                                }}
                            </AlertDialog>
                        }}
                        progress={loading} /> : null}
                <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>) || null)
    }

    Settings.propTypes = {
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        discountDetail: PropTypes.array,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        practice: PropTypes.object,
        progress: PropTypes.bool,
        propounding_subscriptions: PropTypes.object,
        propounding_subscriptions_form: PropTypes.object,
        responding_subscriptions: PropTypes.object,
        responding_subscriptions_form: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object,
        version: PropTypes.oneOfType([PropTypes.object, PropTypes.number])
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        practice: selectPractice(),
        user: selectUser(),
        version: selectVersion(),
        error: selectUpdateError(),
        success: selectSuccess(),
        billing: selectBilling(),
        progress: selectProgress(),
        responding_subscriptions: selectRespondingSubscriptions(),
        propounding_subscriptions: selectPropoundingSubscriptions(),
        metaData: selectRecordsMetaData(),
        responding_subscriptions_form: getFormValues(`SettingsForm_2`),
        propounding_subscriptions_form: getFormValues(`SettingsForm_3`),
        discountDetail: selectSettingsDiscount(),
        licenseDetails: selectLicenseDetails(),
        license_form: getFormValues(`buyLicenseForm`),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(Settings);

}
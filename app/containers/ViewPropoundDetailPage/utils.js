export const getNoQuesSelectedOptions1 = ['Yes, send all', 'Select questions to send', 'Cancel'];

export const getQuesSelectedOptions1 = ['Send selected questions', 'Cancel and clear selection'];


export const getNoQuesSelectedOptions = [
    {
        action: 'handleSendAll',
        label: 'Yes, send all'
    },
    {
        action: 'handleSelectQuestions',
        label: 'Select Questions to send'
    },
    {
        action: 'close',
        label: 'Cancel'
    }
];

export const getQuesSelectedOptions = [
    {
        action: 'handleSendSelectedQuestions',
        label: 'Send selected questions'
    },
    {
        action: 'handleClearSelection',
        label: 'Cancel and clear selection'
    }
];


export const handleSearch = (val, record) => {
  let filteredQuestion;  
  if(val !== '') {
    let value =  parseFloat(val);
    if(isNaN(value)) {
      filteredQuestion = record && record.questions && record.questions.length > 0 && record.questions.filter(el => {
        return el.question_text.toLowerCase().trim().includes(val.toString().toLowerCase().trim());
      });
    } else {
      filteredQuestion = record && record.questions && record.questions.length > 0 && record.questions.filter(el => {
        return el.question_number_text.toLowerCase().includes(val.toString().toLowerCase());
      });
    }
    return Object.assign({}, record, { questions: filteredQuestion });
  }    
}


/**
 * Frogs disc001 6.4 questions
 */
 const defaultConsultationQuestions = () => {
  let question_text = "Did you receive any consultation or examination (except from expert witnesses covered by Code of Civil Procedure sections 2034.210-2034.310) or treatment from a HEALTH CARE PROVIDER for any injury you attribute to the INCIDENT? If so, for each HEALTH CARE PROVIDER state:";

  return [
      {
          question_id: 83,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "1",
          question_section: "(a)",
          question_section_text: "the name, ADDRESS, and telephone number;",
          is_duplicate: 0,
          duplicate_set_no: 1
      },
      {
          question_id: 84,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "2",
          question_section: "(b)",
          question_section_text: "the type of consultation, examination, or treatment provided;",
          question_options: null,
          is_duplicate: 0,
          duplicate_set_no: 1
      },
      {
          question_id: 85,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "3",
          question_section: "(c)",
          question_section_text: "the dates you received consultation, examination, or treatment; and",
          question_options: null,
          is_duplicate: 0,
          duplicate_set_no: 1
      },
      {
          question_id: 86,
          question_number_text: "6.4",
          question_number: 6.4,
          question_text: `${question_text}`,
          question_section_id: "4",
          question_section: "(d)",
          question_section_text: "the charges to date.",
          question_options: null,
          is_duplicate: 0,
          duplicate_set_no: 1
      }
  ]
}

export const consultationQuestions = (record) => {
  let data = record && record.length > 0 && defaultConsultationQuestions();
  let questions;
  if (data && record) {
      questions =
          data &&
          data.map((el) =>
              Object.assign({}, el, {
                id: Date.now().toString(36) + Math.random().toString(36).substr(2),
                case_id: record[0].case_id,
                practice_id: record[0].practice_id,
                client_id: record[0].client_id,
                legalforms_id: record[0].legalforms_id,
                document_type: record[0].document_type,
                party_id: record[0].party_id || false,
                consultation_set_no: record[0].total_consultation_count + 1,
                is_consultation_set: 1,
                subgroup: null,
                lawyer_response_text: null,
                lawyer_response_status: "NotStarted",
                client_response_text: null,
                client_response_status: "NotSetToClient",
                file_upload_status: null,
                uploaded_documents: null,
                last_sent_to_client: null,
                last_updated_by_lawyer: null,
                last_updated_by_client: null,
                TargetLanguageCode: null,
                is_the_client_response_edited: null,
                consultation_createdby: null,
                total_consultation_count: null
              })
          );
  }

  return questions;
};
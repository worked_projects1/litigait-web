/*
 * ViewCasesDetailPage Messages
 *
 * This contains all the text for the ViewCasesDetailPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewCasesDetailPage';

export default defineMessages({
    notFound: {
        id: `${scope}.notFound`,
        defaultMessage: 'No Questions Found.',
    },
    emailDocument: {
        id: `${scope}.emailDocument`,
        defaultMessage: 'Email Document',
    },
    editQuestions: {
        id: `${scope}.editQuestions`,
        defaultMessage: 'Edit Questions',
    },
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    }
});

/**
 * 
 * View Propounding Detail Page
 * 
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Styles from './styles';
import PropTypes from 'prop-types';
import SVG from 'react-inlinesvg';
import Spinner from 'components/Spinner';
import Snackbar from 'components/Snackbar';
import { Grid, Button, Tabs, Tab, Paper } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import ButtonSpinner from 'components/ButtonSpinner';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import ModalForm from 'components/ModalRecordForm';
import AlertDialog from 'components/AlertDialog';
import Stepper from 'components/Stepper';
import { getStepper } from 'containers/ViewPropoundPage/utils';
import { selectUser } from 'blocks/session/selectors';
import { getOffset, useDidUpdate, isPropoundEntityUrl, autoIncrementQuestions, planAlertMessage } from 'utils/tools';
import { Link } from 'react-router-dom';
import RenderPagination from 'components/RenderPagination';
import { selectForm } from 'blocks/session/selectors';
import lodash from 'lodash';
import PropoundStandardForm from './components/PropoundStandardForm'
import schema from 'routes/schema';
import DocumentButton from './components/DocumentButton';
import DocumentEditor from 'components/DocumentEditor';
import { attorneySignatureList } from '../ViewCasesDetailPage/utils';
import { download as downloadDocument } from 'utils/tools';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 */

export default function (name, path, columns, actions, selectors, filterColumns) {
    const { uploadColumns, propoundEmailDocumentColumns, POSColumns } = columns;

    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectBilling, selectKey, selectRecordsMetaData, selectSessionExpand, selectRecords, selectDocumentStatus } = selectors;

    /**
     * @param {object} props 
     */
    function ViewPropoundDetailPage(props) {
        const { record, match, dispatch, location, error, success, metaData, loading, billing, active, user = {}, expand, records = [], forms = {}, documentStatus } = props;

        const classes = Styles();
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const lg = useMediaQuery(theme.breakpoints.up('lg'));
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const [progress, setProgress] = useState(false);
        const [document, setDocument] = useState(false);
        const [errorMessage, setErrorMessage] = useState(false);
        const [emailPopup, setEmailPopup] = useState(false);
        const elements = useElements();
        const stripe = useStripe();
        const [width, setWidth] = useState(window.innerWidth);
        const [position, setPosition] = useState(false);
        const [margin, setMargin] = useState(0);
        const { attorneys } = record;
        const navbarWidth = expand ? 240 : 60;
        const [currentPage, setCurrentPage] = useState(1);
        const pageSize = 25;
        const [pageLoader, setPageLoader] = useState(false);
        const { name, practiceDetails, email } = user;
        const planType = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['responding'] || false;
        const [questionsSelected, setQuestionsSelected] = useState([]);
        const { state = {} } = location || {};
        const defaultQuestions = location && location.state && location.state.defaultQuestions || [];
        const [openDocEditor, setOpenDocEditor] = useState(false);
        const [docLoader, setDocLoader] = useState(false);
        const [legalFormId, setLegalFormId] = useState(false);
        const [formLoader, setFormLoader] = useState(false);

        let questions = record && record.questions && Array.isArray(record.questions) && record.questions.length > 0 && record.questions || [];

        questions = questions && questions.length > 0 && lodash.groupBy(questions, 'question_number_text') || [];


        const lawyers = attorneys && typeof attorneys === 'string' && attorneys.split(',').reduce((a, el) => {
            const opt = metaData && metaData['users'] && metaData['users'].find(_ => _.id.toString() === el);
            if (opt)
                a.push(opt)

            return a;
        }, []) || [];

        const modified_template_s3_file_key_doc_type = `modified_template_s3_file_key_${match.params.type}`;

        const customModifiedTemplate = metaData && metaData['modifiedTemplateOptions'] && metaData['modifiedTemplateOptions'].length > 0 && metaData['modifiedTemplateOptions'].reduce((a, el) => {
            const option = record && (el.state == record.state || el.state == "") && el[modified_template_s3_file_key_doc_type];

            if (option) {
                a.push(Object.assign({}, { label: el.file_name, value: el.id, [modified_template_s3_file_key_doc_type]: el[modified_template_s3_file_key_doc_type], status: el.status, state: el.state }))
            }
            return a;
        }, []) || [];

        const singleCustomTemplate = customModifiedTemplate && customModifiedTemplate.length == 1 && customModifiedTemplate && customModifiedTemplate[0] || false;

        const validSubscription = planType && !['tek_as_you_go', 'pay_as_you_go'].includes(planType);

        const userRole = user && user?.role;
        const { attorneySignature } = metaData && record && attorneySignatureList({ metaData, record });
        const showSignature = userRole && userRole == 'paralegal' ? attorneySignature && attorneySignature?.length > 0 && true : userRole && userRole == 'lawyer';

        const customTemplateAndSignature = schema().propoundCustomTemplateSignature(user, customModifiedTemplate, validSubscription, Object.assign({}, { caseRecord: record, attorneySignature, metaData, showSignature, userRole })).section;

        const propoundFormdata = match && match.params && match.params.type && record && record.propounding_form_details && record.propounding_form_details[match.params.type] && record.propounding_form_details[match.params.type];

        const docType = ['sprogs', 'rfpd', 'rfa'];

        const propoundingFormDetails = docType
            .map((el) => {
                if (el) {
                    return record && record.propounding_form_details && record.propounding_form_details[el].length > 0 && record.propounding_form_details[el].map(elm => elm && elm.s3_file_key && Object.assign({}, { ...elm })).filter(x => x);
                }
            }).reduce((a, el) => {
                if (el && Array.isArray(el) && el.length > 0) {
                    const finalRecord = el.map((res, index) => {
                        if(res){
                            return Object.assign({}, { ...res }, { orderId: index + 1 })
                        }
                    })
                    a.push(...finalRecord);
                }
                return a;
            }, []);
        const sendEmailColumn = propoundEmailDocumentColumns && propoundEmailDocumentColumns(propoundingFormDetails, record, actions, dispatch, documentStatus).columns;

        const selectedForm = legalFormId && propoundFormdata && propoundFormdata.length > 0 ? propoundFormdata.find(_ => _.id.toString() === legalFormId.toString()) : record && state && state.legalforms_id && propoundFormdata && propoundFormdata.length > 0 ? propoundFormdata.find(_ => _.id.toString() === state.legalforms_id.toString()) : propoundFormdata && propoundFormdata.length > 0 ? propoundFormdata[0] : {};

        const selectedTab = legalFormId && propoundFormdata && propoundFormdata.length > 0 ? propoundFormdata.findIndex(_ => _.id.toString() === legalFormId.toString()) : record && state && state.legalforms_id && match && match.params && match.params.type && propoundFormdata && propoundFormdata?.length > 0 ? propoundFormdata.findIndex(_ => _.id.toString() === state.legalforms_id.toString()) : 0;

        useEffect(() => {
            dispatch(actions.loadPropoundRecord(match.params.id));
            dispatch(actions.loadRecords());
            dispatch(actions.loadPropoundForm(Object.assign({}, {
                case_id: match.params.id,
                document_type: match.params.type,
                id: match.params.pid
            })));
            window.addEventListener('resize', () => {
                setMargin((getOffset('esquiretek-cases-header') + getOffset('esquiretek-detail-header') - 120));
                setPosition((getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')) - 2);
                setWidth(window.innerWidth);
            });

        }, [match.params.id]);


        useDidUpdate(() => {
            if (records && records.length > 0 && record && record.frogs && !record.questions) {
                dispatch(actions.loadPropoundRecord(match.params.id));
                dispatch(actions.loadPropoundForm(Object.assign({}, {
                    case_id: match.params.id,
                    document_type: match.params.type,
                    id: match.params.pid
                })));
            }
        }, [records, record, actions, dispatch, match]);

        const modifiedTempRecord = (submitRecord) => {
            let finalRecord;
            if (singleCustomTemplate && Object.keys(singleCustomTemplate).length > 0) {
                finalRecord = Object.assign({}, submitRecord, { [modified_template_s3_file_key_doc_type]: singleCustomTemplate[modified_template_s3_file_key_doc_type], modified_template_status: singleCustomTemplate.status, custom_template_id: singleCustomTemplate?.value });
            } else {
                finalRecord = submitRecord;
            }
            return finalRecord
        }

        const openDocument = ({ form, dialog, submitRecord, generatedType, data, freeTrialDialog }) => {
            let submitRecords = Object.assign({}, { ...submitRecord });

            if (data && data.custom_template) { //multiple custom template
                const modified_template_value = customModifiedTemplate && customModifiedTemplate.length > 0 && customModifiedTemplate.reduce((a, el) => {
                    if (el && data && data.custom_template === el.value) {
                        const objValue = Object.assign({}, { modified_template_s3_file_key_value: el[modified_template_s3_file_key_doc_type], status: el.status });
                        a.push(objValue);
                    }
                    return a;
                }, []);
                submitRecords = Object.assign({}, submitRecords, { [modified_template_s3_file_key_doc_type]: modified_template_value && modified_template_value[0] && modified_template_value[0].modified_template_s3_file_key_value, modified_template_status: modified_template_value && modified_template_value[0] && modified_template_value[0].status, custom_template_id: modified_template_value[0].value });
            } else {
                submitRecords = modifiedTempRecord(submitRecords); //single custom template
            }

            // QuestionsAutoIncrement
            const questionsData = questionsSelected && questionsSelected.length > 0 && questionsSelected || submitRecord && submitRecord.questions.length && submitRecord.questions || [];

            let questionsRecord = (questionsData && submitRecord && submitRecord.questions && questionsData.length != submitRecord.questions.length) ? autoIncrementQuestions(questionsData) : questionsData;

            setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
            let signature = signatureFunc(data, generatedType);
            dispatch(actions.loadBillingDetails(form, dialog, setProgress, Object.assign({}, submitRecords, { propound_id: selectedForm?.id, questions: questionsRecord }), generatedType, false, false, freeTrialDialog, signature, false, false, false, false, false, false, false, false, setOpenDocEditor));
        }

        const signatureFunc = (signatureRecord, generatedType) => {
            const signature = Object.assign({}, { lawyer_signature: null, pos_signature: null });
            if(user?.role && user?.role === 'paralegal') {
                if(generatedType && generatedType === 'propound_doc') {
                    signature.pos_signature = signatureRecord?.signature || null;
                    if(attorneySignature && attorneySignature?.length === 1) {
                        signature.lawyer_signature = attorneySignature[0] && attorneySignature[0]?.value || null;
                    } else {
                        signature.lawyer_signature = signatureRecord?.attorney_signature || null;
                    }
                } else {
                    signature.lawyer_signature = signatureRecord?.signature || null;
                }
            } else {
                signature.lawyer_signature = signatureRecord?.signature || null;
            }
        
            return signature;
        }

        const handleBilling = async (submitRecord, generatedType, data, dispatch, { form }) => {
            const result = await stripe.confirmCardSetup(billing.client_secret, {
                payment_method: {
                    card: elements.getElement(CardElement),
                    billing_details: Object.assign({}, { name: data.name, email: data.email, phone: data.phone }),
                }
            });

            dispatch(actions.updateBillingDetails(result, Object.assign({}, submitRecord), setProgress, generatedType, form));
        }

        const generateDocument = (submitRecord = {}, generatedType) => {
            setProgress(generatedType);
            dispatch(actions.fetchForm(Object.assign({}, submitRecord), generatedType));
        }

        const { title, steps, successMessage } = progress && getStepper(progress) || {};

        if (loading || (record && !record.questions)) {
            return <Spinner className={classes.spinner} />;
        }

        const handlePagination = (page, index) => {
            setCurrentPage(page);
            setPageLoader(true);
            setTimeout(() => setPageLoader(false), 2000);
        }

        const handleSignature = (data, { form, dialog, submitRecord, generatedType, freeTrialDialog }) => {
            openDocument(Object.assign({}, { form, dialog, submitRecord, generatedType, data, freeTrialDialog }))
        }

        const handleEmailDocument = (generatedType) => {
            if (selectedForm && selectedForm?.s3_file_key) {
                setEmailPopup(true);
                setDocument(Object.assign({}, { value: match.params.type, type: generatedType }));
            } else {
                setErrorMessage('Document is not generated, please generate using Create Document and try again')
            }
        }

        const handleSendEmail = (data, dispatch, { form }) => {
            const submitRecord = Object.assign({}, data, { propounder_practice_id: record.practice_id, propounder_case_id: record.id, propounder_opposing_counsel: record.opposing_counsel, attach_document_id: propoundFormdata && propoundFormdata[0] && propoundFormdata[0].id });

            submitRecord.selected_forms = propoundingFormDetails && propoundingFormDetails.length > 0 && propoundingFormDetails.reduce((a, el) => {
                const opt = data && data.selected_forms.some((elm) => elm.id == el.id);
                if (opt)
                    a.push(el.id)
                return a;
            }, []) || data.selected_forms;

            dispatch(actions.sendPropoundQuestions(submitRecord, form, setDocument, setEmailPopup));
        }

        const handlePOS = (submitData, posModal, validate, signature) => {
            setDocument(false);
            if (!validate()) {
                dispatch(actions.savePropounderServingAttorney(Object.assign({}, submitData, { propound_id: selectedForm && selectedForm?.id, propounder_serving_attorney_city: submitData.propounder_serving_attorney_city, propounder_serving_attorney_email: submitData.propounder_serving_attorney_email, propounder_serving_attorney_name: submitData.propounder_serving_attorney_name, propounder_serving_attorney_state: submitData.propounder_serving_attorney_state, propounder_serving_attorney_street: submitData.propounder_serving_attorney_street, propounder_serving_attorney_zip_code: submitData.propounder_serving_attorney_zip_code, propounder_serving_date: submitData.propounder_serving_date }), posModal, signature));
            }
        }

        const handleSaveAndEmailDocxFile = async (editorData) => {
            const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;
            if (documentEdited) {
                setDocLoader('save');
                const blob = await editorData.documentEditor.saveAsBlob('Docx');
                const exportedDocument = blob;
                const submitRecord = Object.assign({}, selectedForm, { document_type: match.params.type });
                const myFile = new File([exportedDocument], '', {
                    type: exportedDocument.type,
                });
                dispatch(actions.savePropoundDocumentEditor(Object.assign({}, { record: submitRecord, myFile, setDocLoader, setDocEditor: setOpenDocEditor, setEmailPopup, optNode: 'save' })));
            } else {
                const s3Key = selectedForm && selectedForm.s3_file_key || false;
                if (s3Key) {
                    let docFilename = s3Key.slice(s3Key.lastIndexOf('/') + 1, s3Key.length);
                    docFilename = docFilename.replace('.docx', '');
                    if (editorData?.documentEditor) {
                        editorData.documentEditor.save(docFilename, "Docx");
                    }
                }
                setOpenDocEditor(false);
                setEmailPopup(true);
                setDocument(Object.assign({}, { value: match.params.type, type: 'SendQuestions' }));
            }
        }

        const handleDownloadDocxFile = async (editorData) => {
            const documentEdited = editorData?.documentEditor?.editorHistory?.undoStackIn?.length > 0;
            const submitRecord = Object.assign({}, selectedForm, { document_type: match.params.type });
            if (documentEdited) {
                setDocLoader('download');
                const blob = await editorData.documentEditor.saveAsBlob('Docx');
                const exportedDocument = blob;
                const myFile = new File([exportedDocument], '', {
                    type: exportedDocument.type,
                });
                dispatch(actions.savePropoundDocumentEditor(Object.assign({}, { record: submitRecord, myFile, setDocLoader })));
            } else {
                const document = selectedForm && selectedForm.public_url || false;
                if (document) {
                    downloadDocument(document);
                }
            }
        }

        const handleCloseEmailModal = () => {
            setEmailPopup(false);
        }

        const descriptionMessage = planAlertMessage(planType, user?.subscription_details);

        const handleTab = (e, tabIndex) => {
            if (propoundFormdata && propoundFormdata.length > 0 && propoundFormdata[tabIndex]) {
                setCurrentPage(1);
                setFormLoader(true);
                setLegalFormId(propoundFormdata[tabIndex]['id']);
                dispatch(actions.loadPropoundForm(Object.assign({}, {
                    case_id: match.params.id,
                    document_type: match.params.type,
                    id: propoundFormdata[tabIndex]['id']
                }), false, setFormLoader));
            }
        }

        return (
            <Grid container className={classes.body}>
                <Grid container id="esquiretek-detail-header" className={classes.actions} style={!md ? { left: '0', width: `${width}px`, top: `${position || getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')}px` } : { width: `${width - navbarWidth}px`, top: `${position || getOffset('esquiretek-header') + getOffset('esquiretek-cases-header')}px`, left: navbarWidth }}>
                    <Grid item xs={12} lg={5}>
                        <Grid container direction="row" justify={!lg && 'center' || null} alignItems="center" className={classes.btnActions}>
                            <Grid item className={classes.item}>
                                <AlertDialog
                                    description={descriptionMessage}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='OK'>
                                    {(open) =>
                                    (
                                        planType && planType != '' ? <DocumentButton
                                            columns={uploadColumns}
                                            value={match.params.type}
                                            classes={classes}
                                            updateBilling={handleBilling}
                                            billing={billing}
                                            record={Object.assign({}, record, { lawyers })}
                                            style={!md && { justifyContent: 'center' } || {}}
                                            label={match && match.params && match.params.type && match.params.type.toUpperCase()}
                                            loading={props.progress}
                                            generateDocument={generateDocument}
                                            openDocument={openDocument}
                                            title="Create Document"
                                            type="propound_doc"
                                            document={document}
                                            sm={sm}
                                            setDocument={setDocument}
                                            customTemplateAndSignature={customTemplateAndSignature}
                                            customModifiedTemplate={customModifiedTemplate}
                                            handleSignature={handleSignature}
                                            match={match}
                                            user={user}
                                            practiceDetails={practiceDetails}
                                            propoundFormdata={propoundFormdata}
                                            actions={actions}
                                            metaData={Object.assign({}, { ...metaData }, { modifiedTemplateOptions: customModifiedTemplate })}
                                            POSColumns={POSColumns}
                                            handlePOS={handlePOS}
                                            descriptionMessage={descriptionMessage} /> :
                                            <Grid item xs={12} className={classes.icon} onClick={open}>
                                                <Button type="button" variant="contained" color="primary" className={classes.docButton}>
                                                    <SVG src={require(`images/icons/discovery.svg`)} style={{ height: 'auto' }} />
                                                </Button>
                                                {sm && <Grid className={classes.title}>
                                                    {'Create Document'}
                                                </Grid> || null}
                                            </Grid>)}
                                </AlertDialog>
                            </Grid>
                            <Grid item className={classes.item}>
                                <ModalForm
                                    initialValues={Object.assign({}, { to_opposing_counsel_email: record && record['opposing_counsel'] && record['opposing_counsel'].map(r => r.opposing_counsel_email).join(','), cc_propounder_email: email, selected_forms: propoundFormdata.map(el => Object.assign({},el, {id: el.id})), attach_documents: propoundFormdata && propoundFormdata[0] && propoundFormdata[0].attach_documents || [] })}
                                    show={emailPopup}
                                    onClose={handleCloseEmailModal}
                                    title={"Email Document"}
                                    fields={sendEmailColumn.filter(_ => _.editRecord && !_.disableRecord)}
                                    form={`emailDocument`}
                                    onSubmit={handleSendEmail.bind(this)}
                                    metaData={metaData}
                                    keepDirtyOnReinitialize={true}                                    
                                    enableSubmitBtn
                                    btnLabel="Send Email"
                                    enableScroll={classes.bodyScroll}
                                    footerStyle={{ border: 'none' }}
                                    style={{ paddingRight: '0px' }} />
                                <Grid item xs={12} className={classes.icon}>
                                    <Button type="button" variant="contained" color="primary" className={classes.docButton} onClick={() => handleEmailDocument('SendQuestions')} >
                                        {document && document.type === 'SendQuestions' && props.progress && <ButtonSpinner color={'#2ca01c'} /> || <SVG src={require(`images/icons/mail.svg`)} style={{ height: 'auto' }} /> || <FormattedMessage {...messages.emailDocument} />}
                                    </Button>
                                    {sm && <Grid className={classes.title}>
                                        <FormattedMessage {...messages.emailDocument} />
                                    </Grid> || null}
                                </Grid>
                            </Grid>
                            {match && match.params.type && match.params.type.toLowerCase() !== 'frogs' && <Grid item className={classes.item}>
                                <Link to={{ pathname: isPropoundEntityUrl({ id: match.params.id, type: match.params.type, pid: match.params.pid }, 'questionsForm'), state: Object.assign({}, location.state, { tabValue: '0/propound' }) }}>
                                    <Grid item xs={12} className={classes.icon}>
                                        <Button type="button" variant="contained" color="primary" className={classes.docButton}>
                                            <SVG src={require(`images/icons/Edit.svg`)} style={{ height: 'auto' }} />
                                        </Button>
                                        {sm && <Grid className={classes.title}>
                                            <FormattedMessage {...messages.editQuestions} />
                                        </Grid> || null}
                                    </Grid>
                                </Link>
                            </Grid>}
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container className={classes.responseBox} style={{ marginTop: `${margin || ((getOffset('esquiretek-cases-header') + getOffset('esquiretek-detail-header') - 120)) || 0}px`, position: 'relative' }}>
                {propoundFormdata && propoundFormdata?.length > 0 ?
                    <Grid item xs={12}>
                        <Paper square className={propoundFormdata && propoundFormdata?.length > 1 ? classes.paper : classes.paperNew}>
                            <Tabs
                                value={selectedTab}
                                onChange={handleTab}
                                variant="fullWidth"
                                indicatorColor="primary"
                                textColor="primary">
                                {(propoundFormdata || []).map((form, index) => <Tab key={index} label={form && form.file_name || ''} />)}
                            </Tabs>
                        </Paper>
                    </Grid> : null}
                    {formLoader ? <Spinner className={classes.spinner} /> : questions && Object.keys(questions).length > 0 ?
                        <React.Fragment>
                            <RenderPagination
                                className="pagination-bar"
                                records={questions}
                                currentPage={currentPage}
                                totalCount={Object.keys(questions).length}
                                pageLoader={pageLoader}
                                pageSize={pageSize}
                                standardForm={true}
                                onPageChange={page => handlePagination(page)}>
                                {(renderRecord) => {
                                    return <PropoundStandardForm
                                        initialValues={Object.assign({}, { ...record }, { document_type: 'frogs', type: location.state && location.state.type, questions: defaultQuestions, filename: location.state && location.state.filename })}
                                        name={name}
                                        path={path}
                                        columns={columns}
                                        form={`propoundStandardForm_${record.id}`}
                                        questions={renderRecord}
                                        locationState={location.state}
                                        history={history}
                                        record={record}
                                        actions={actions}
                                        dispatch={dispatch}
                                        navbarWidth={navbarWidth}
                                        questionsSelected={questionsSelected}
                                        setQuestionsSelected={setQuestionsSelected}
                                        match={match}
                                    />
                                }}
                            </RenderPagination>
                        </React.Fragment> :
                        <Grid item xs={12} className={classes.notFound}>
                            <FormattedMessage {...messages.notFound} />
                        </Grid>}
                </Grid>
                <Grid item xs={12}>
                    {!loading ? <Stepper
                        title={title}
                        active={active}
                        show={progress ? true : false}
                        close={() => setProgress(false)}
                        steps={steps}
                        error={error}
                        disableCancelBtn
                        successMessage={successMessage} /> : null}
                </Grid>
                {openDocEditor ?  <Grid item>
                    <DocumentEditor
                        actions={actions}
                        dispatch={dispatch}
                        disableCloseModal
                        show={openDocEditor}
                        progress={docLoader}
                        record={Object.assign({}, selectedForm, { type: match.params.type })}
                        title={'Document Preview'}
                        btnLabel="Save & E-Serve"
                        showSaveBtn
                        infoNote={"E-serve opposing counsel directly from EsquireTek after reviewing your document. Edits can be made within the window below."}
                        onClose={() => setOpenDocEditor(false)}
                        handleSubmitBtn={handleSaveAndEmailDocxFile}
                        handleDownloadBtn={handleDownloadDocxFile}
                    />
                </Grid> : null }           
                {!progress ? <Snackbar show={error || success || errorMessage ? true : false} text={error || success || errorMessage} severity={(error || errorMessage) ? 'error' : 'success'} handleClose={() => { setErrorMessage(false); dispatch(actions.loadRecordsCacheHit()) }} /> : null}
            </Grid >
        )


    }

    ViewPropoundDetailPage.propTypes = {
        active: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        forms: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        forms: selectForm(),
        metaData: selectRecordsMetaData(),
        loading: selectPageLoader(),
        success: selectSuccess(),
        billing: selectBilling(),
        progress: selectProgress(),
        active: selectKey(),
        user: selectUser(),
        expand: selectSessionExpand(),
        records: selectRecords(),
        documentStatus: selectDocumentStatus(),
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewPropoundDetailPage);

}
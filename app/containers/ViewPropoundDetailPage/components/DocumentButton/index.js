/**
 * 
 * Document Button
 * 
 */

import React from 'react';
import ModalForm from 'components/ModalRecordForm';
import { Grid, Button } from '@material-ui/core';
import AlertDialog from 'components/AlertDialog';
import ButtonSpinner from 'components/ButtonSpinner';
import { getState } from 'containers/ViewCasesPage/utils';
import SVG from 'react-inlinesvg';
import POS from '../POS';
import SectionModalForm from 'components/SectionModalRecordForm';

const DocumentButton = ({ columns, value, classes, updateBilling, billing, record, label, loading, generateDocument, openDocument, type, title, document, setDocument, style, sm, validate, customTemplateAndSignature, handleSignature, POSColumns, user, practiceDetails, propoundFormdata, actions, metaData, handlePOS, match, descriptionMessage, customModifiedTemplate }) => {

    const showCustomTemplate = customModifiedTemplate && Array.isArray(customModifiedTemplate) && customModifiedTemplate.length > 1 || false;
    return (<ModalForm
        fields={columns.filter(_ => _.editRecord)}
        form={`billingForm_${type}_${value}`}
        className={classes.form}
        footerStyle={{ borderTop: 'none' }}
        style={style}
        btnLabel={`Pay $${billing && billing.price || 0}`}
        message="Free tier usage is complete. Please provide your credit card details to process this order. Card details will be securely saved for future orders as well."
        onSubmitClose
        onSubmitbtn
        onSubmit={updateBilling.bind(this, getState(record, label), type)}>
        {(openForm) =>
            <AlertDialog
                description={descriptionMessage}
                onConfirm={() => setDocument(false)}
                onConfirmPopUpClose={true}
                closeDialog={() => setDocument(false)}
                btnLabel1='OK' >
                {(openFreeTrialAlert) => {
                    return <AlertDialog
                        description={`You will be charged $${billing && billing.price || 0} for your order. Do you want to continue?`}
                        onConfirm={() => generateDocument(getState(record, label), type)}
                        onConfirmPopUpClose={true}
                        closeDialog={() => setDocument(false)}
                        btnLabel1='Yes'
                        btnLabel2='No' >
                        {(openDialog) =>
                            <SectionModalForm
                                initialValues={Object.assign({}, { signature: '' })}
                                fields={customTemplateAndSignature.map(el => Object.assign({}, ...el.columns))}
                                section={customTemplateAndSignature.filter(_ => _.editForm)}
                                form={`customTemplateAndSignature`}
                                btnLabel="Generate"
                                metaData={metaData}
                                onSubmitClose
                                enableSubmitBtn={!showCustomTemplate ? true : false}
                                className={classes.signatureForm}
                                onSubmit={(data) => handleSignature(data, Object.assign({}, { form: openForm, dialog: openDialog, submitRecord: getState(record, label), generatedType: type, freeTrialDialog: openFreeTrialAlert }))}>
                                {(openSign) =>
                                    <POS
                                        fields={POSColumns}
                                        record={record && user && practiceDetails && Object.assign({}, record, { document_type: propoundFormdata && propoundFormdata[0] && propoundFormdata[0].document_type, propounder_serving_attorney_name: propoundFormdata[0].serving_attorney_name ? propoundFormdata[0].propounder_serving_attorney_name : practiceDetails.name, propounder_serving_attorney_street: propoundFormdata[0].propounder_serving_attorney_street ? propoundFormdata[0].propounder_serving_attorney_street : practiceDetails.street, propounder_serving_attorney_city: propoundFormdata[0].propounder_serving_attorney_city ? propoundFormdata[0].propounder_serving_attorney_city : practiceDetails.city, propounder_serving_attorney_state: propoundFormdata[0].propounder_serving_attorney_state ? propoundFormdata[0].propounder_serving_attorney_state : practiceDetails.state, propounder_serving_attorney_zip_code: propoundFormdata[0].propounder_serving_attorney_zip_code ? propoundFormdata[0].propounder_serving_attorney_zip_code : practiceDetails.zip_code, propounder_serving_attorney_email: propoundFormdata[0].propounder_serving_attorney_email ? propoundFormdata[0].propounder_serving_attorney_email : user.email, propounder_serving_date: propoundFormdata[0].propounder_serving_date || null })}
                                        actions={actions}
                                        metaData={metaData}
                                        style={{ textAlign: 'center' }}
                                        match={match}
                                        footerBtn={(formRecord, setModalOpen, validate) => <Grid container justify={"flex-end"}>
                                            <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handlePOS(formRecord, setModalOpen, validate, openSign)}>
                                                {loading && <ButtonSpinner /> || 'Submit'}
                                            </Button>
                                        </Grid>}>
                                        {(openPOS) => {
                                            return <Grid item xs={12} className={classes.icon}>
                                                <Button type="button" variant="contained" color="primary" className={classes.docButton} onClick={openPOS}>
                                                    {document && document.value === value && document.type === type && loading && <ButtonSpinner color='#2ca01c' /> || type && <SVG src={require(`images/icons/discovery.svg`)} style={{ height: 'auto' }} /> || title}
                                                </Button>
                                                {sm && <Grid className={classes.title}>{title}</Grid> || null}
                                            </Grid>
                                        }}
                                    </POS>
                                }
                            </SectionModalForm>}
                    </AlertDialog>
                }}
            </AlertDialog>}
    </ModalForm>)
};

export default DocumentButton;
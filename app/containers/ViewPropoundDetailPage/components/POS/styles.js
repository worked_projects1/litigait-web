import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',        
        '& .MuiPaper-root': {
            minWidth: '50% !important'
        },        
    },
    container: {
        border: '1px solid #eaeaea',
        padding: '10px',
        margin: '8px',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '25%'
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px',
        maxHeight: '400px',
        overflowY: 'scroll',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: '8px',
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    },    
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    title: {
        fontFamily: 'Avenir-Bold',
        fontSize: '18px',
    },
    message: {
        fontFamily: 'Avenir-Bold',
        fontSize: '16px',
        paddingTop: '18px',
    },
    messageRegular: {
        fontSize: '16px',
        paddingTop: '18px',
    },
    closeIcon: {
        cursor: 'pointer'
    },
    messageGrid: {
        paddingTop: '10px'
    },
    error: {
        marginTop: '10px'
    },
    note: {
        fontSize: '16px',
    },
    subTitle: {
        fontWeight: 'bold'
    },
    caseInfo: {
        display: 'flex',
        padding: '10px 0px'
    },
    editButton: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Bold',
        backgroundColor: '#fff !important',
        boxShadow: 'none',
        color: '#47AC39',
        fontSize: '14px',
        paddingTop: '2px',
        paddingBottom: '2px',
        '&:hover': {
            boxShadow: 'none'
        }
    },
    containerBottom: {
        marginBottom: '15px'
    },
    settings: {
        padding: '10px 0px'
    },
    row: {
        // alignItems: 'center'
    },
    label: {
        fontSize: '14px'
    },
    value: {
        fontSize: '14px',
        margin: '0px'
    },
    error: {
        marginTop: '10px',
        width: '100% !important',
        '& .MuiAlert-filledError': {
            minWidth: '100% !important',
            maxWidth: '100% !important',
            color: 'black',
            border: '2px solid #f44336',
            background: 'transparent'
        },
        '& .MuiAlert-filledSuccess': {
            width: '100% !important'
        }
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        backgroundColor: 'gray !important'
    },
    updateBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginRight: '15px',
        marginRight: '12px'
    }
}));


export default useStyles;

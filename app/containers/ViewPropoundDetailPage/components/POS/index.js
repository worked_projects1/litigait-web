/**
 * 
 *  Propound Proof Of Service Modal
 * 
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';
import EditRecordForm from 'components/EditRecordForm';
import validation from './validation';
import moment from 'moment';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function POS(props) {

    const { children, fields = [], metaData = {}, className, style, footerStyle, record = {}, actions, footerBtn, match } = props;
    const section = fields && typeof fields === 'object' && fields || false;

    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [enableForms, setEnableForms] = useState(fields.reduce((a, el) => Object.assign({}, a, { [el.key]: false }), {}));
    const [formRecord, setFormRecord] = useState(record);
    const [formLoader, setFormLoader] = useState(false);
    const [error, setError] = useState(validation && typeof validation === 'function' && validation(formRecord, section) || false);

    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    let errorMessage = [];

    const closeModal = () => {
        setModalOpen(false);
    }

    const handleInlineRecord = (data, dispatch, { form }) => {
        if (form === "propounder_opposing_counsel") {
            setFormLoader(true);
            dispatch(actions.updateRecord(Object.assign({}, data, { date_of_loss: new Date(moment(data.date_of_loss).format('MM/DD/YYYY')) }, { opposing_counsel_info: true }), form, setFormLoader));
        }
        setFormRecord(Object.assign({}, formRecord, data));
        setEnableForms(Object.assign({}, enableForms, { [form]: false }));
    }

    const validate = () => {
        const formValidation = validation && typeof validation === 'function' && validation(formRecord, section) || false;
        if (formValidation && Object.keys(formValidation).length === 0) {
            setError(false);
            return false;
        } else if (formValidation && Object.keys(formValidation).length > 0) {
            setError(formValidation);
            return true;
        }
    }

    const handleFormClose = (form) => {
        setEnableForms(Object.assign({}, enableForms, { [form]: false }));
    }

    const columValue = (column, row) => {
        switch (column.type) {
            case 'upload':
                return column.html && column.html(row, metaData) || (
                    <img
                        src={row[column.value] || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                        role="presentation"
                        className={classes.img}
                    />
                );
            case 'checkbox':
                return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value]}</p>;
            case 'select':
                return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value]}</p>;
            case 'textarea':
                return column.html && column.html(row, metaData) || <p className={classes.textarea}>{row[column.value] != null && row[column.value] != '' ? lineBreak(row[column.value]) : '-'}</p>;
            default:
                return column.html && column.html(row, metaData) || <p className={classes.value}>{row[column.value] != null && row[column.value] != '' ? row[column.value] : '-'}</p>;
        }
    };

    let errors = error && Object.keys(error) && Object.keys(error).length > 0;

    if (errors) {
        Object.keys(error).map(el => {
            if (['propounder_serving_attorney_street', 'propounder_serving_attorney_city', 'propounder_serving_attorney_state', 'propounder_serving_attorney_zip_code'].includes(el)) {
                errorMessage.push('Serving Attorney Address');
            } else if (['opposing_counsel'].includes(el)) {
                errorMessage.push('Opposing Counsel');
            } else if (['propounder_serving_date'].includes(el)) {
                errorMessage.push('PoS Date');
            }
        })
    }

    return <Grid container className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || false}>
                <Paper className={classes.paper} style={!sm ? { width: '90%' } : {}}>
                    <Grid container className={classes.header} justify="space-between">
                        <Grid item xs={10}>
                            <Typography component="span" className={classes.title}>Please review and confirm the following info to generate PoS and Propounding</Typography>
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                            <CloseIcon onClick={closeModal} className={classes.closeIcon} />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.body}>
                        <Grid item xs={12}>
                            <Grid container direction="column">
                                {fields.map((field, index) => {
                                    return (<Grid item xs={12} key={index} className={classes.containerBottom}>
                                        <Grid container>
                                            <Grid item xs={6}>
                                                <Typography variant="subtitle1" className={classes.subTitle}>{field.label}</Typography>
                                            </Grid>
                                            {field.editForm && <Grid item xs={6} container justify="flex-end">
                                                <Button
                                                    type="button"
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.editButton}
                                                    onClick={() => setEnableForms(Object.assign({}, enableForms, { [field.key]: true }))}>
                                                    Edit
                                                </Button>
                                            </Grid>}
                                            <Grid item xs={12}>
                                                {enableForms[field.key] && field.editForm ?
                                                    <EditRecordForm
                                                        initialValues={formRecord}
                                                        form={`${field.key}`}
                                                        fields={field.columns.filter(_ => _.editRecord)}
                                                        metaData={metaData}
                                                        onSubmit={handleInlineRecord}
                                                        spinner={formLoader}
                                                        updateBtn={classes.updateBtn}
                                                        cancelBtn={() => <Button
                                                            type="button"
                                                            variant="contained"
                                                            color="primary"
                                                            onClick={() => handleFormClose(field.key)}
                                                            className={classes.cancelBtn}>
                                                            Cancel
                                                        </Button>} /> :
                                                    <Grid container direction="row">
                                                        {field && field.key === 'propounder_opposing_counsel' && formRecord.opposing_counsel && formRecord.opposing_counsel.length > 0 ? formRecord.opposing_counsel.map((val, i) => {
                                                            return <Grid container key={i} className={classes.container} direction="row">
                                                                <Typography style={{ fontWeight: 600 }}>Counsel {i + 1}</Typography>
                                                                {field.columns.filter(_ => _.viewRecord).map((column, childIndex) => {
                                                                    return (<Grid key={childIndex} item xs={12}>
                                                                        <Grid className={classes.settings}>
                                                                            <Grid container className={classes.row}>
                                                                                <Grid item xs={6} md={5} className={classes.label}>
                                                                                    {column.label}:
                                                                                </Grid>
                                                                                <Grid item xs={6} md={7} className={classes.value}>
                                                                                    {columValue(column, val)}
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Grid>
                                                                    </Grid>)
                                                                })}
                                                            </Grid>
                                                        }) : field && field.key === 'propounder_opposing_counsel' && !formRecord.opposing_counsel ? field.columns.filter(_ => _.viewRecord).map((column, i) => {
                                                            return (<Grid key={i} item xs={12}>
                                                                <Grid className={classes.settings}>
                                                                    <Grid container className={classes.row}>
                                                                        <Grid item xs={6} md={5} className={classes.label}>
                                                                            {column.label}:
                                                                        </Grid>
                                                                        <Grid item xs={6} md={7} className={classes.value}>
                                                                            {columValue(column, formRecord)}
                                                                        </Grid>
                                                                    </Grid>
                                                                </Grid>
                                                            </Grid>)
                                                        }) : null}
                                                        {field && field.key != 'propounder_opposing_counsel' && field.columns.filter(_ => _.viewRecord).map((column, i) => {
                                                            return (<Grid key={i} item xs={12}>
                                                                <Grid className={classes.settings}>
                                                                    <Grid container className={classes.row}>
                                                                        <Grid item xs={6} md={5} className={classes.label}>
                                                                            {column.label}:
                                                                        </Grid>
                                                                        <Grid item xs={6} md={7} className={classes.value}>
                                                                            {columValue(column, formRecord)}
                                                                        </Grid>
                                                                    </Grid>
                                                                </Grid>
                                                            </Grid>)
                                                        })}
                                                    </Grid>
                                                }
                                            </Grid>
                                        </Grid>
                                    </Grid>)
                                })}
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        {errorMessage && Array.isArray(errorMessage) && errorMessage.length > 0 ?
                            <Error className={classes.error} errorMessage={`Required: ${[...new Set(errorMessage)]}`} /> : null}
                    </Grid>
                    <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                        <Grid>
                            {footerBtn && footerBtn(formRecord, setModalOpen, validate) || null}
                        </Grid>
                        <Button
                            type="button"
                            variant="contained"
                            onClick={closeModal}
                            className={classes.button}>
                            Cancel
                        </Button>
                    </Grid>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}


POS.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    fields: PropTypes.array,
    record: PropTypes.object,
    user: PropTypes.object
};

export default POS;

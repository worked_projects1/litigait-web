/**
 * 
 * POS Validation
 * 
 * 
 */


/**
 * 
 * @param {object} values 
 * @param {object} section 
 * @returns 
 */
export default function validation(values, section) {
    const errors = {};

    const requiredFields = section.filter(x => x.editForm).map(_ => { return _.columns }).reduce((a, el) => {
        el.filter(e => e.editRecord).map(_ => a.push({ label: _.label, value: _.value }))
        return a;
    }, []);

    requiredFields.forEach(field => {
        if ((!values[field.value] && field.value != 'dob' && typeof values[field.value] === 'boolean') || (!values[field.value] && field.value != 'dob' && values[field.value] != '0')) {
            errors[field.value] = 'Required'
        }
    })


    return errors;
}

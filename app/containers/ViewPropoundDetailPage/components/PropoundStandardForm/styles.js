import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  btn: {
    fontSize: '12px',
    textTransform: 'none',
    padding: '4px 8px',
    margin: '6px 6px',
    fontWeight: 'bold',
  },
  btnGrid: {
    marginTop: '6px',
    marginBottom: '6px',
  },
  chkBox: {
    display: 'inline',
    color: "#2ca01c",
    marginTop: '-10px',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4)
  },
  button: {
    fontWeight: 'bold',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular',
    marginLeft: '20px'
  },
  section: {
    margin: '0px'
  },
  form: {
    margin: '10px'
  },
  checkboxField: {
    display: 'inline',
    marginTop: '-10px',
    marginRight: '10px'
  },
  header: {
    position: 'fixed',
    height: 'auto',
    zIndex: '999',
    background: '#fff',
    top: '48px',
    left: '60px',
    padding: '25px',
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    boxShadow: '0px 3px 4px -1px lightgrey'
  }
}));

export default useStyles;
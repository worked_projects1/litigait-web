/***
 * 
 * Propound Standard Form
 * 
 */


import React, { useState, useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Grid, Typography, FormControlLabel, Checkbox } from '@material-ui/core';
import Styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

const FieldBox = ({ input, questions, locationState, classes, record, match, meta: { touched, error } }) => {
    let inputValue = (input && input.value.length > 0 && Array.isArray(input.value) && input.value) || [];

    const handleChange = (val, checked, key) => {
        if (val === "all" && checked) {
            inputValue = record && record.questions || [];
        } else if (val === "all" && !checked) {
            inputValue = [];
        } else if (checked) {
            inputValue = inputValue.filter(el => el.question_number_text != key);
            inputValue = inputValue.concat(val.map(c => Object.assign({}, { ...c }, { question_options: [] })));
        } else {
            inputValue = inputValue.filter(el => el.question_number_text != key);
        }
        input.onChange(inputValue);
    }

    return (
        <Grid container>
            <Grid container item xs spacing={3} className={classes.section} style={{ marginTop: '30px' }}>
                <Grid container direction="row" spacing={3} className={classes.form}>
                    <FormControlLabel className={classes.checkboxField}
                        control={<Checkbox
                            style={{ color: "#2ca01c" }}
                            checked={record && inputValue.length == record.questions.length ? true : false}
                            onChange={(e) => handleChange('all', e.target.checked)} />}
                    />
                    <Grid container xs style={{ marginTop: '2px' }}>
                        <Typography variant="subtitle2" >
                            <b>Select All</b>
                        </Typography>
                    </Grid>
                </Grid>
                {(Object.keys(questions) || []).map((key, index) => {
                    const form = questions[key] && questions[key].length > 0 && questions[key].
                        reduce((a, g, z) => {
                            return Object.assign({}, a, {
                                question_number: g.question_number,
                                question_number_text: g.question_number_text,
                                question_options: g.question_options,
                                question_text: z === 0 ? `${g.question_text} \n${g.question_section || ''} ${g.question_section_text || ''}` : `${a.question_text || ''} \n${g.question_section || ''} ${g.question_section_text || ''}`
                            })
                    }, {});

                    return (
                        <Grid container key={index} direction="row" spacing={3} className={classes.form}>
                            <FormControlLabel className={classes.checkboxField}
                                control={<Checkbox
                                    style={{ color: "#2ca01c" }}
                                    checked={inputValue.map(_ => _.question_number).includes(form.question_number) ? true : false}
                                    onChange={(e) => handleChange(questions[key], e.target.checked, key)} />}
                            />
                            <Grid container xs style={{ marginTop: '2px' }}>
                                {match && match.path && match.path.indexOf('propound') > -1 && match.params.type != 'frogs' ? <Typography variant="subtitle2" >
                                    {form && <b>{form.question_number_text || form.question_number}. {form.question_text}</b>}
                                </Typography> : 
                                <Typography variant="subtitle2" >
                                    {form && <b>{form.question_number_text || form.question_number}. {form.question_text.split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>)}</b>}
                                </Typography>}
                            </Grid>
                        </Grid>
                    )
                })}
            </Grid>
        </Grid>
    )
}


function PropoundStandardForm(props) {
    const { handleSubmit, questions, locationState, record, questionsSelected, setQuestionsSelected, match, pristine, submitting, dispatch, actions, navbarWidth, columns, change, history } = props;
    const classes = Styles();

    const [width, setWidth] = useState(window.innerWidth);
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));

    useEffect(() => {
        let mounted = true;
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);

    return <div>
        <form onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={12}>
                    <Field
                        name="questions"
                        className={classes.chkBox}
                        questions={questions}
                        record={record}
                        classes={classes}
                        component={FieldBox}
                        locationState={locationState}
                        type="text"
                        match={match}
                        onChange={(data) => setQuestionsSelected(data)} />
                </Grid>
            </Grid>
        </form>
    </div>
}

export default reduxForm({
    form: 'propoundStandardForm',
    touchOnChange: true,
    destroyOnUnmount: false,
})(PropoundStandardForm)
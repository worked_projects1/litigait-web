

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  paper: {
    flexGrow: 1,
    maxWidth: '315px',
    maxHeight: '58px',
    boxShadow: 'none',
    '@global': {
      '.MuiTab-textColorPrimary.Mui-selected': {
        color: '#2ca01c'
      },
      '.MuiButtonBase-root': {
        width: '120px'
      },
      '.MuiButtonBase-root > span': {
        fontFamily: 'Avenir-Bold'
      },
      '.MuiTabs-indicator': {
        backgroundColor: '#2ca01c'
      }
    }
  },
  spinner: {
    padding: '20px',
    marginTop: '50px'
  },
  filter: {
    height: '100%'
  },
  filterOPtions: {
    padding: '0px',
    border: 'none',
    outline: 'none',
    position: 'relative',
    background: 'transparent',
    fontSize: '15px',
    marginRight: '6px',
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px'
  }
  },
  card: {
    margin: 'auto',
    borderRadius: '4px', // 16px
    transition: '0.3s',
    boxShadow: '0px 2px 7px rgba(34, 35, 58, 0.2)',
    position: 'relative',
    overflow: 'initial',
    background: '#ffffff',
    alignItems: 'center'
  },
  media: {
    position: 'relative',
    width: '100%',
    margin: 0,
    height: 'auto',
    padding: '15px',
    '@global': {
      '.MuiCardHeader-content > span': {
        fontWeight: 'bold',
        fontSize: '19px'
      }
    }
  },
  content: {
    paddingTop: '0px'
  },
  PieChart: {
    paddingTop: '25px'
  },
  usageCard: {
    maxWidth: '20%',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '50%',
    },
    [theme.breakpoints.down('md')]: {
      maxWidth: '50%',
    },
    [theme.breakpoints.down('xs')]: {
      maxWidth: '100%',
    }
  },
  filterBtn: {
    borderRadius: '28px',
    fontFamily: 'Avenir-Regular',
    fontWeight: 'bold',
    textTransform: 'capitalize'
  },
  setResponseXdays: {
    marginLeft: '2px',
    fontSize: '14px !important',
    fontWeight: 'bold',
    fontFamily: 'Avenir-Regular',
    cursor: 'pointer',
    display: 'flex',
    marginRight: '5px',
    paddingBottom: '4px',
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px'
    }
  },
  calendarIcon: {
    width: '22px',
    height: '22px',
    color: 'rgba(0, 0, 0, 0.54)',
    cursor: 'pointer'
  },
  modalPaper1: {
    width: '85% !important',
    [theme.breakpoints.down('xs')]: {
      width: '92% !important'
    }
  },
  bodyScroll: {
    maxHeight: '500px',
    overflowY: 'scroll',
    overflowX: 'hidden',
    "&::-webkit-scrollbar": {
      width: 12,
    },
    "&::-webkit-scrollbar-track": {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
      borderRadius: '30px'
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#2ca01c",
      borderRadius: '30px'
    }
  },
  date: {
    color: 'blue',
    fontFamily: 'Avenir-Regular',
    marginRight: '5px',
    cursor: 'pointer',
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px'
    }
  }
}));


export default useStyles;
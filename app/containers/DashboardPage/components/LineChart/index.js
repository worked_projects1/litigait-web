import React, { useEffect } from 'react';
import cx from 'clsx';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';
import { Grid, Card, CardHeader, CardContent } from '@material-ui/core';
import useStyles from './styles';

export default function (props) {
    const { id, type, title, data = [], filter, filterOptions = [], onChangeFilter } = props;
    const classes = useStyles();
    const shadowStyles = useOverShadowStyles();

    useEffect(() => {
        const ctx = document.getElementById(`Line_${id}`).getContext('2d');
        if (ctx) {
            //adding custom chart type
            Chart.defaults.multicolorLine = Chart.defaults.line;
            Chart.controllers.multicolorLine = Chart.controllers.line.extend({
                draw: function (ease) {
                    var
                        startIndex = 0,
                        meta = this.getMeta(),
                        points = meta.data || [],
                        colors = this.getDataset().colors,
                        area = this.chart.chartArea,
                        originalDatasets = meta.dataset._children
                            .filter(function (data) {
                                return !isNaN(data._view.y);
                            });

                    function _setColor(newColor, meta) {
                        meta.dataset._view.borderColor = newColor;
                    }

                    if (!colors) {
                        Chart.controllers.line.prototype.draw.call(this, ease);
                        return;
                    }

                    for (var i = 2; i <= colors.length; i++) {
                        if (colors[i - 1] !== colors[i]) {
                            _setColor(colors[i - 1], meta);
                            meta.dataset._children = originalDatasets.slice(startIndex, i);
                            meta.dataset.draw();
                            startIndex = i - 1;
                        }
                    }

                    meta.dataset._children = originalDatasets.slice(startIndex);
                    meta.dataset.draw();
                    meta.dataset._children = originalDatasets;

                    points.forEach(function (point) {
                        point.draw(area);
                    });
                }
            });

            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'multicolorLine',

                // The data for our dataset
                data: {
                    labels: (data || []).map(_ => _.name),
                    datasets: [{
                        label: title,
                        fill: false,
                        borderColor: '#2ca01c',
                        pointBorderWidth: 3,
                        pointRadius: 5,
                        pointHoverBorderWidth: 3,
                        pointHoverRadius: 5,
                        pointBackgroundColor: (data || []).map(_ => '#ffff'),
                        pointBorderColor: (data || []).map(_ => '#2ca01c'), 
                        data: (data || []).map(_ => _.value),
                        //first color is not important
                        colors: [''].concat(data || []).map(_ => '#2ca01c')
                    }]
                },

                // Configuration options go here
                options: {
                    legend: {
                        display: false
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                let value = data.datasets[0].data[tooltipItem.index];
                                return id.indexOf('Revenue') > -1 ? `${new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(value || 0)} - ${type}` : `${value} - ${type}` || 0;
                            }
                        }
                    }
                }
            });
        }
    }, [data, title]);
    
    return (<Grid item xs={12} className={classes.LineChart}>
        <Card className={cx(classes.root, shadowStyles.root)}>
            <CardHeader
                title={title}
                className={classes.media}
            />
            <CardContent className={classes.content}>
                <Grid container>
                    <Grid item xs={12}>
                        <Grid container direction="row" justify="flex-end" alignItems="center" className={classes.filter}>
                            <select name={id} defaultValue={filter[id]} className={classes.filterOPtions} onChange={onChangeFilter}>
                                {(filterOptions || []).map((opt, index) => (<option key={index} value={opt.value}>{opt.label}</option>))}
                            </select>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <canvas id={`Line_${id}`} height="200" width="250" />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </Grid>)
}

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: 'auto',
        borderRadius: '4px', // 16px
        transition: '0.3s',
        boxShadow: '0px 2px 7px rgba(34, 35, 58, 0.2)',
        position: 'relative',
        overflow: 'initial',
        background: '#ffffff',
        alignItems: 'center'
    },
    blue: {
        '&:after': {
            background: 'linear-gradient(147deg, #2BBACF 0%, #2BBACF 74%)'
        }
    },
    green: {
        '&:after': {
            background: 'linear-gradient(147deg, #56AF5A 0%, #56AF5A 74%)'
        }
    },
    orange: {
        '&:after': {
            background: 'linear-gradient(147deg, #FC9A28 0%, #FC9A28 74%)'
        }
    },
    red: {
        '&:after': {
            background: 'linear-gradient(147deg, #EA4844 0%, #EA4844 74%)'
        }
    },
    lightGreen: {
        '&:after': {
            background: 'linear-gradient(147deg, #6dea44 0%, #6dea44 74%)'
        }
    },
    media: {
        position: 'relative', 
        width: '100%',
        margin: 0,
        height: 'auto', 
        padding: '15px',
        '@global': {
            '.MuiCardHeader-content > span': {
                fontWeight: 'bold',
                fontSize: '19px'
            }
        }
    },
    content: {
        paddingTop: '0px'
    },
    LineChart: {
        paddingTop: '25px',
    },
    filter: { 
      paddingTop: '25px',
      paddingBottom: '25px', 
      height: '100%' 
    },
    filterOPtions: {
      padding: '0px',
      border: 'none',
      outline: 'none',
      position: 'relative',
      background: 'transparent',
      fontSize: '15px',
      marginRight: '6px',
      paddingRight: '12px'
    }
}));


export default useStyles;

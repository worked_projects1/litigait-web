


/**
 * 
 * @param {number} size 
 */
export function chartColors(size) {
  const colors = ["#118380", "#19A6A4", "#1FC1BF", "#29EFEA", "#07ede7", '#276664', '#337d6d', '#0f5e4d'];

  for (let idx = 3; idx < size; idx += 1) {
    colors.push(`hsl(${Math.round(Math.random() * 360)}, 33%, 66%)`);
  }

  return colors.slice(0, size);
};




import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    legends: {
        display: 'flex',
        alignItems: 'center'
    }
}));


export default useStyles;
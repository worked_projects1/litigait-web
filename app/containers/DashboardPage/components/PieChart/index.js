/**
 * 
 * Pie Chart 
 * 
 */

import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
import Chart from 'chart.js';
import { chartColors } from './utils';
import useStyles from './styles';

export default function (props) {

    const { id, title, data, style } = props;
    const classes = useStyles();

    useEffect(() => {
        const ctx = document.getElementById(`Pie_${id}`);
        if (ctx) {
            const totalValue = (data || []).reduce((a, el) => a + parseInt(el.value), 0);
            const myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    responsive: true,
                    maintainAspectRatio: false,
                    datasets: [{
                        data: totalValue === 0 ? [1e-10] : (data || []).map(_ => _.value),
                        label: title,
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: chartColors(data.length),
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: '#817D7D',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: '#ef8b5d',
                        pointHoverBorderColor: '#ea6225',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        spanGaps: false,
                        backgroundColor: chartColors(data.length)
                    }],
                    labels: totalValue === 0 ? ['None'] : (data || []).map(_ => _.label)
                },
                options: {
                    legendCallback: function (chart) {
                        let text = [];
                        let bgColor = '';
                        text.push('<ul class="' + chart.id + '-legend">');
                        for (let i = 0; i < data.length; i++) {
                            bgColor = chart.data.datasets[0].backgroundColor[i];
                            text.push(`<li><div><span style="background-color: ${bgColor};"></span>`);
                            if (data[i].label) {
                                text.push(`${data[i].label} (${data[i].value})`);
                            }
                            text.push('</div></li>');
                        }
                        text.push('</ul>');
                        return text.join('');
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                const label = data.labels[tooltipItem.index];
                                let value = data.datasets[0].data[tooltipItem.index] || 'Other';
                                value = (value < 1e-3) ? "" : value;
                                return value ? `${label} : ${value}` : label;
                            }
                        }
                    },
                    legend: {
                        display: false,
                        position: 'bottom',
                        labels: {
                            boxWidth: 15,
                            fontSize: 14,
                            fontFamily: 'Avenir-Bold'
                        }
                    },
                    cutoutPercentage: 65
                }
            });

            const Legend = document.getElementById(`Pie_Legends_${id}`);
            if (Legend)
                Legend.innerHTML = myChart.generateLegend();
        }
    }, [data, title])

    return (<Grid container style={style}>
        <Grid item xs={12}>
            <canvas id={`Pie_${id}`} height="200" width="250" />
        </Grid>
        <Grid item xs={12} className={classes.legends}>
            <div className="Pie_Legends" id={`Pie_Legends_${id}`} />
        </Grid>
    </Grid>);

}

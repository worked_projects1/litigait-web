

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: 'auto',
        borderRadius: '4px', // 16px
        transition: '0.3s',
        boxShadow: '0px 2px 7px rgba(34, 35, 58, 0.2)',
        position: 'relative',
        overflow: 'initial',
        background: '#ffffff',
        alignItems: 'center',
        '@global': {
            '.MuiCardHeader-avatar': {
                marginRight: '3px'
            }
        }
    },
    media: {
        position: 'relative', 
        width: '100%',
        margin: 0,
        height: 'auto', 
        padding: '15px',
        '@global': {
            '.MuiCardHeader-content > span': {
                fontWeight: 'bold',
                fontSize: '19px'
            }
        }
    },
    content: {
        paddingTop: '0px'
    },
    CountCard: {
        paddingTop: '25px',
    },
    cardCount: {
        display: 'inline-block',
        textAlign: 'right',
        cursor: 'pointer',
        color: 'blue'
    }
}));


export default useStyles;
/**
 * 
 * Count Card
 * 
 */



import React from 'react';
import cx from 'clsx';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';
import { Grid, Card, CardHeader, CardContent, Typography } from '@material-ui/core';
import useStyles from './styles';
import Icons from 'components/Icons';

/**
 * 
 * @param {object} props 
 */
export default function CountCard(props) {
    const { id, title, data, icon, style, navigate, filter, customFilter, handleNavigateFunc } = props;
    const customFilterData = filter && Object.keys(filter).length > 0 && filter.statistics && filter.statistics != 'custom_filter' && filter || customFilter && Object.keys(customFilter).length > 0 && customFilter;

    const classes = useStyles();
    const shadowStyles = useOverShadowStyles();

    return (<Grid id={id} item xs={12} className={classes.CountCard}>
        <Card className={cx(classes.root, shadowStyles.root)} style={style}>
            <CardHeader
                avatar={icon && <Icons type={icon} style={{ marginBottom: '3px', marginRight: '3px' }} /> || false}
                title={title}
                className={classes.media}
            />
            <CardContent className={classes.content}>
                <Grid container direction="column" spacing={2}>
                    {(data || []).map((card, index) => (
                        <Grid key={index} item xs={12}>
                            <Grid container direction="row" justify="space-between">
                                <Typography variant="body2" component="p">
                                    {card.label}
                                </Typography>
                                {navigate ? <Typography variant="body2" className={classes.cardCount} component="p" onClick={() => card.value > 0 && handleNavigateFunc(card.linkTo, card.type, card.name, customFilterData)}>
                                    <span>{card.value}</span>
                                </Typography> : <Typography variant="body2" component="p" >
                                    <b>{card.value}</b>
                                </Typography>}
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
            </CardContent>
        </Card>
    </Grid>);

}
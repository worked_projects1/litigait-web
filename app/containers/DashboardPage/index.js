/**
 *
 * Dashboard Page
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';
import cx from 'clsx';
import moment from 'moment';

import {
    selectLoggedIn,
    selectUser
} from 'blocks/session/selectors';


import {
    selectBusinessMetrics,
    selectFilter,
    selectLoading,
    selectError,
    selectSuccess,
    selectCustomFilter,
    selectDashboard
} from 'blocks/analytics/selectors';

import { loadBusinessMetrics, loadHistogramData, updateFilterData, updateCustomFilterData, updateDashboardFilterData, updateActiveCustomFilter, updateDashboardTabValue } from 'blocks/analytics/actions';

import { Grid, Paper, Tabs, Tab, Card, CardHeader, CardContent } from '@material-ui/core';

import {
    mapPracticesBlock,
    mapUsersBlock,
    mapCasesBlock,
    mapClientsBlock,
    mapProductsSalesBlock,
    mapProductsRevenueBlock,
    mapQuestionsSentToClientPie,
    mapQuestionsRespondedByClientPie,
    mapGeneratedDocumentsPie,
    mapFinalDocumentsPie,
    mapPracticesLine,
    mapRevenueLine,
    mapMonthlyBreakdown,
    mapYearlyBreakdown,
    mapSubscriptionsBlock
} from './mappings';


import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Error from 'components/Error';
import CountCard from './components/CountCard';
import PieChart from './components/PieChart';
import useStyles from './styles';
import Spinner from 'components/Spinner2';
import LineChart from './components/LineChart';
import ModalForm from 'components/ModalRecordForm';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { Tooltip } from "@material-ui/core";
import schema from 'routes/schema';
import { customTextToDate } from 'utils/tools';
import { convertObjectToParams } from 'utils/tools';
import 'moment-timezone';

const customDateFilterColumns = schema().customDateFilter().columns;
/**
 * @param {object} props 
 */
function DashboardPage(props) {

    const classes = useStyles();
    const { analytics = {}, dispatch, history = {}, location = {}, user = {}, loading, pathData = {}, error, filter = {}, customFilter = {}, tabValue } = props;

    const { practices = {}, users = {}, clients = {}, cases = {}, salesByProduct = {}, cumulativeRevenue = {}, byMonthRevenue = {}, cumulativePractices = {}, byMonthPractices = {}, revenueByProduct = {}, monthlyBreakDown = {}, yearlyBreakDown = {}, subscription = {} } = analytics;
    const { filterColumns = {} } = pathData;
    const { statistics = [], histogram = [] } = filterColumns;
    const [pageLoader, setPageLoader] = useState(false);
    const [tabs, setTabs] = useState(tabValue);
    const [openDateModal, setOpenDateModal] = useState(false);
    const shadowStyles = useOverShadowStyles();
    const currentYear = moment().year();

    const filterRecord = filter && filter.statistics && Object.keys(filter.statistics).length > 0 && filter.statistics || false;
    const customFilterRecord = customFilter && Object.keys(customFilter).length > 0 && customFilter.from_date && customFilter.to_date && customFilter || false;

    useEffect(() => {
        if (filterRecord || customFilterRecord) {
            dispatch(loadBusinessMetrics(Object.assign({}, { filter: filterRecord }, customFilterRecord)));
        } else {
            dispatch(loadBusinessMetrics());
        }

        dispatch(loadHistogramData({ filter: currentYear }));
        setPageLoader(true);
        setTimeout(() => setPageLoader(false), 1000);
    }, []);

    const CountCardData = [
        { id: 'practices', title: "Practices", data: mapPracticesBlock(practices), icon: 'Business', navigate: true },
        { id: 'users', title: "Users", data: mapUsersBlock(users), icon: 'Group', navigate: true },
        { id: 'clients', title: "Clients", data: mapClientsBlock(clients), icon: 'DeviceHub' },
        { id: 'cases', title: "Cases", data: mapCasesBlock(cases), icon: 'School' },
        { id: 'subscribers', title: "Subscribers", data: mapSubscriptionsBlock(subscription), icon: 'Dollar', navigate: true }
    ];

    const PieChartData = [
        {
            id: "Questions", title: "Questions Sent & Responded By Client", PieData: [
                { id: 'QuestionsSentToClient', data: mapQuestionsSentToClientPie(analytics) },
                { id: 'QuestionsRespondedByClient', data: mapQuestionsRespondedByClientPie(analytics) }
            ]
        },
        {
            id: "Documents", title: "Created Shell & Discovery", PieData: [
                { id: 'GeneratedDocuments', title: "Created Shell", data: mapGeneratedDocumentsPie(analytics) },
                { id: 'FinalDocuments', title: "Completed Discovery", data: mapFinalDocumentsPie(analytics) }
            ]
        }
    ];

    const LineChartData = [
        { id: 'cumulativeRevenue', title: "Revenue (Cumulative)", type: 'Cumulative', data: mapRevenueLine(cumulativeRevenue, 'total', filter && filter.cumulativeRevenue || currentYear), filterOptions: histogram },
        { id: 'byMonthRevenue', title: "Revenue (By Month)", type: 'New', data: mapRevenueLine(byMonthRevenue, 'new', filter && filter.byMonthRevenue || currentYear), filterOptions: histogram },
        { id: 'cumulativePractices', title: "Practices (Cumulative)", type: 'Cumulative', data: mapPracticesLine(cumulativePractices, 'total', filter && filter.cumulativePractices || currentYear), filterOptions: histogram },
        { id: 'byMonthPractices', title: "New Practices (By Month)", type: 'New', data: mapPracticesLine(byMonthPractices, 'new', filter && filter.byMonthPractices || currentYear), filterOptions: histogram }
    ];

    const SalesCardData = [
        { id: 'products', title: 'Sales By Product', data: mapProductsSalesBlock(salesByProduct || {}), icon: 'ShoppingCart' },
        { id: 'productsRevenue', title: 'Revenue By Product', data: mapProductsRevenueBlock(revenueByProduct || {}), icon: 'Dollar' },
        { id: 'monthlyBreakdown', title: 'Monthly Breakdown', data: mapMonthlyBreakdown(monthlyBreakDown || {}), icon: 'Income' },
        { id: 'yearlyBreakdown', title: 'Yearly Breakdown', data: mapYearlyBreakdown(yearlyBreakDown || {}), icon: 'Income' }
    ];


    const handleChangeFilter = (e) => {
        if (e.target.value == 'custom_filter') {
            setOpenDateModal(true);
            dispatch(updateFilterData(Object.assign({}, filter, { [e.target.name]: e.target.value })));
        } else {
            dispatch(updateCustomFilterData({}));
            dispatch(updateFilterData(Object.assign({}, filter, { [e.target.name]: e.target.value })))
            if (e.target.name === 'statistics')
                dispatch(loadBusinessMetrics(Object.assign({}, { filter: e.target.value })));
            else
                dispatch(loadHistogramData(Object.assign({}, { id: e.target.name, filter: e.target.value })));
        }
    }

    const handleTabChange = (e, tabIndex) => {
        setTabs(tabIndex);
        if (tabIndex === 0 && Object.keys(cumulativeRevenue).length === 0)
            dispatch(loadHistogramData({ filter: currentYear }));
    };

    const handleCustomDateFilter = (date, dispatch, { form }) => {
        const custom_date = Object.assign({}, {
            from_date: moment(date.from_date).format('YYYY-MM-DD 00:00:00'),
            to_date: moment(date.to_date).format('YYYY-MM-DD 23:59:59')
        });
        // if (filterRecord && filterRecord != 'custom_filter') {
        //     dispatch(updateFilterData({ "statistics": false }))
        // }
        dispatch(updateCustomFilterData(custom_date));
        dispatch(loadBusinessMetrics(Object.assign({}, { filter: 'custom_filter' }, custom_date)));
    }

    const handleNavigateFunc = (link, type, name, data) => {
        let timeZone = moment.tz.guess();
        let custom_date;
        if (type && type != "total") {
            if (data && Object.keys(data).length > 0 && data.from_date && data.to_date) {
                const fromDate = moment(data && data.from_date).format('YYYY-MM-DD 00:00:00');
                const endDate = moment(data && data.to_date).format('YYYY-MM-DD 23:59:59');

                const from_date = timeZone && moment.tz(fromDate, timeZone).utc().format('YYYY-MM-DD HH:mm:ss') || moment.tz(fromDate, 'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm:ss');
                const to_date = timeZone && moment.tz(endDate, timeZone).utc().format('YYYY-MM-DD HH:mm:ss') || moment.tz(endDate, 'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm:ss');

                custom_date = convertObjectToParams({ from_date, to_date });
            } else {
                custom_date = filter.statistics && convertObjectToParams(Object.assign({}, { filter: filter.statistics }));
            }

            const filterValue = data ? (custom_date ? `filter_type=${type}&${custom_date}` : `filter_type=${type}&filter=last_30_days`) : `filter_type=${type}&filter=last_30_days`;

            const activeType = type && type == 'active' ? { "active": true } : { "active": false };

            let selectedData = filter && Object.keys(filter).length > 0 && filter.statistics && customTextToDate(filter) || customFilterRecord && Object.assign({}, { from_date: moment(customFilterRecord.from_date).format('MM/DD/YYYY 00:00:00'), to_date: moment(customFilterRecord.to_date).format('MM/DD/YYYY 23:59:59') }) || customTextToDate({ statistics: "last_30_days" });
            
            dispatch(updateActiveCustomFilter(Object.assign({}, { [name]: selectedData }), Object.assign({}, { [name]: activeType })));
            dispatch(updateDashboardFilterData(filterValue));
        }
        dispatch(updateDashboardTabValue(1));
        history.push({ pathname: `${link}`, state: location.state });
    }

    if (error) {
        return <Grid container>
            <Grid item xs={12} className={classes.error}>
                <Error errorMessage={<FormattedMessage {...messages.error} />} />
            </Grid>
        </Grid>
    }

    if (loading || pageLoader) {
        return <Spinner className={classes.spinner} />
    }

    return (
        <Grid container>
            <Grid item xs={12}>
                <Grid container direction="row" justify="space-between">
                    <Grid item xs={12}>
                        <Paper square className={classes.paper}>
                            <Tabs
                                value={tabs}
                                onChange={handleTabChange}
                                variant="fullWidth"
                                indicatorColor="primary"
                                textColor="primary">
                                <Tab label="Sales" />
                                <Tab label="Usage" />
                            </Tabs>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container direction="row" justify="flex-end" alignItems="center" className={classes.filter}>
                            {customFilterRecord && <Grid item className={classes.createdDate} onClick={() => setOpenDateModal(true)}>
                                <Grid item className={classes.date}>{`${moment(customFilterRecord.from_date).format('MM/DD/YY')} - ${moment(customFilterRecord.to_date).format('MM/DD/YY')} `}</Grid>
                            </Grid>}
                            <select name="statistics" defaultValue={filter.statistics || false} className={classes.filterOPtions} onChange={handleChangeFilter}>
                                {(statistics || []).map((opt, index) => (<option key={index} value={opt.value}>{opt.label}</option>))}
                            </select>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {tabs === 0 && <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={12}>
                        <Grid container direction="row" spacing={2}>
                            {(SalesCardData || []).map((salesProps, salesIndex) => (
                                <Grid key={salesIndex} item md={3} sm={12} xs={12}>
                                    <CountCard
                                        style={{ minHeight: '320px' }}
                                        user={user}
                                        history={history}
                                        locationState={location.state}
                                        filter={filter}
                                        customFilter={customFilter}
                                        handleNavigateFunc={handleNavigateFunc}
                                        {...salesProps} />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container direction="row" spacing={2}>
                            {(LineChartData || []).map((lineProps, lineIndex) => (
                                <Grid key={lineIndex} item md={6} sm={6} xs={12}>
                                    <LineChart {...lineProps} filter={filter} onChangeFilter={handleChangeFilter} />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid> || tabs === 1 &&
                <Grid item xs={12}>
                    <Grid container>
                        <Grid item xs={12}>
                            <Grid container direction="row" spacing={2}>
                                {(CountCardData || []).map((cardProps, cardIndex) => (
                                    <Grid key={cardIndex} item md={3} sm={6} xs={12} className={classes.usageCard}>
                                        <CountCard
                                            style={{ minHeight: '210px' }}
                                            user={user}
                                            history={history}
                                            locationState={location.state}
                                            filter={filter}
                                            customFilter={customFilter}
                                            handleNavigateFunc={handleNavigateFunc}
                                            {...cardProps} />
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container direction="row" spacing={2}>
                                {(PieChartData || []).map((pieProps, pieIndex) => (
                                    <Grid key={pieIndex} item md={6} sm={6} xs={12}>
                                        <Grid className={classes.PieChart} item xs={12}>
                                            <Card className={cx(classes.card, shadowStyles.root)}>
                                                <CardHeader
                                                    title={pieProps.title}
                                                    className={classes.media}
                                                />
                                                <CardContent className={classes.content}>
                                                    <Grid container direction="row">
                                                        {(pieProps.PieData || []).map((pieData, pieDataIndex) =>
                                                        (<Grid key={pieDataIndex} item md={6} sm={6} xs={12}>
                                                            <PieChart
                                                                user={user}
                                                                history={history}
                                                                locationState={location.state}
                                                                {...pieData} />
                                                        </Grid>))}
                                                    </Grid>
                                                </CardContent>
                                            </Card>
                                        </Grid>
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                || null}
            <ModalForm
                initialValues={customFilter || {}}
                title={`Custom Date Filter`}
                fields={customDateFilterColumns.filter(_ => _.editRecord && !_.disableRecord)}
                form={`custom_date_filter`}
                show={openDateModal}
                onClose={() => setOpenDateModal(false)}
                onSubmit={handleCustomDateFilter.bind(this)}
                onSubmitClose
                btnLabel="Submit"
                disableContainer />
        </Grid>
    );
}

DashboardPage.propTypes = {
    analytics: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    children: PropTypes.object,
    dispatch: PropTypes.func,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    filter: PropTypes.object,
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    loggedIn: PropTypes.bool,
    match: PropTypes.object,
    pathData: PropTypes.object,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    user: PropTypes.object,
    customFilter: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
    loggedIn: selectLoggedIn(),
    loading: selectLoading(),
    filter: selectFilter(),
    error: selectError(),
    success: selectSuccess(),
    user: selectUser(),
    analytics: selectBusinessMetrics(),
    customFilter: selectCustomFilter(),
    tabValue: selectDashboard()
});

const withConnect = connect(mapStateToProps);

export default compose(
    withConnect,
    memo,
)(DashboardPage);

/**
 * 
 * Mappings
 * 
 */
import moment from 'moment';

const currYear = moment().year();
const currMonth = moment().format('M');

export const mapPracticesBlock = ({ active, new : newly_created, total }) => ([
    {
        linkTo: '/practices',
        label: 'New Practices',
        type: 'new',
        name: 'practices',
        value: newly_created || 0
    },
    {
        linkTo: '/practices',
        label: 'Active Practices',
        type: 'active',
        name: 'practices',
        value: active || 0
    },
    {
        linkTo: '/practices',
        label: 'Total Practices',
        type: 'total',
        name: 'practices',
        value: total || 0
    }
]);


export const mapUsersBlock = ({ active, new : newly_created, total }) => ([
    {
        linkTo: '/userCategories/rest/practice/users',
        label: 'New Users',
        type: 'new',
        name: 'practiceUsers',
        value: newly_created || 0        
    },
    {
        linkTo: '/userCategories/rest/practice/users',
        label: 'Active Users',
        type: 'active',
        name: 'practiceUsers',
        value: active || 0
    },
    {
        linkTo: '/userCategories/rest/practice/users',
        label: 'Total users',
        type: 'total',
        name: 'practiceUsers',
        value: total || 0
    }
]);

export const mapCasesBlock = ({ active, new : newly_created, total }) => ([
    {
        linkTo: '/cases',
        label: 'New Cases',
        value: newly_created || 0
    },
    {
        linkTo: '/cases',
        label: 'Active Cases',
        value: active || 0
    },
    {
        linkTo: '/cases',
        label: 'Total Cases',
        value: total || 0
    }
]);

export const mapClientsBlock = ({ active, new : newly_created, total }) => ([
    {
        linkTo: '/clients',
        label: 'New Clients',
        value: newly_created || 0
    },
    {
        linkTo: '/clients',
        label: 'Active Clients',
        value: active || 0
    },
    {
        linkTo: '/clients',
        label: 'Total Clients',
        value: total || 0
    }
]);

export const mapSubscriptionsBlock = ({ monthly, yearly, activationFee, activationFeeWaived }) => ([
    {
        linkTo: '/orderCategories/rest/subscriptions/admin/history',
        label: 'Monthly',
        name: 'subscriptions',
        type: 'monthly',
        value: monthly || 0
    },
    {
        linkTo: '/orderCategories/rest/subscriptions/admin/history',
        label: 'Yearly',
        name: 'subscriptions',
        type: 'yearly',
        value: yearly || 0
    },
    {
        linkTo: '/orderCategories/rest/subscriptions/admin/history',
        label: 'Activation Fee',
        name: 'subscriptions',
        type: 'activationFee',
        value: activationFee || 0
    },
    {
        linkTo: '/orderCategories/rest/subscriptions/admin/history',
        label: 'Activation Fee Waived',
        name: 'subscriptions',
        type: 'activationFeeWaived',
        value: activationFeeWaived || 0
    },
]);

export const mapProductsSalesBlock = ({ shell, discovery, pos, teksign, medical_history, propounding, total }) => ([
    {
        linkTo: '/case',
        label: 'Shell',
        value: shell || 0
    },
    {
        linkTo: '/case',
        label: 'Discovery',
        value: discovery || 0
    },
    {
        linkTo: '/case',
        label: 'POS',
        value: pos || 0
    },
    {
        linkTo: '/case',
        label: 'Teksign',
        value: teksign || 0
    },
    {
        linkTo: '/cases',
        label: 'Propounding',
        value: propounding || 0
    },
    {
        linkTo: '/cases',
        label: 'Medical History',
        value: medical_history || 0
    },
    {
        linkTo: '/cases',
        label: 'Total',
        value: total || 0
    }
]);

export const mapProductsRevenueBlock = ({ monthly_grand_total, yearly_grand_total, activationFee, propounding_monthly_199, propounding_yearly_2199, medical, total }) => ([
    {
        linkTo: '/cases',
        label: 'Responding Monthly',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(monthly_grand_total || 0)
    },
    {
        linkTo: '/cases',
        label: 'Responding Yearly',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(yearly_grand_total || 0)
    },
    {
        linkTo: '/cases',
        label: 'Activation Fee',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(activationFee || 0)
    },
    {
        linkTo: '/cases',
        label: 'Propounding Monthly',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(propounding_monthly_199 || 0)
    },
    {
        linkTo: '/cases',
        label: 'Propounding Yearly',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(propounding_yearly_2199 || 0)
    },
    {
        linkTo: '/cases',
        label: 'Medical History',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(medical || 0)
    },
    {
        linkTo: '/cases',
        label: 'Total',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(total || 0)
    }
]);

export const mapMonthlyBreakdown = ({ new_monthly, renewal_monthly, monthly_canceled_count, discount_applied }) => ([
    {
        linkTo: '/cases',
        label: 'New Monthly',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(new_monthly || 0)
    },
    {
        linkTo: '/cases',
        label: 'Monthly Renewal',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(renewal_monthly || 0)
    },
    {
        linkTo: '/cases',
        label: 'Monthly Cancellations',
        value: monthly_canceled_count || 0
    },
    {
        linkTo: '/cases',
        label: 'Total Discount Applied',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(discount_applied || 0)
    }
]);

export const mapYearlyBreakdown = ({ new_yearly, renewal_yearly, yearly_canceled_count, discount_applied }) => ([
    {
        linkTo: '/cases',
        label: 'New Yearly',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(new_yearly || 0)
    },
    {
        linkTo: '/cases',
        label: 'Yearly Renewal',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(renewal_yearly || 0)
    },
    {
        linkTo: '/cases',
        label: 'Yearly Cancellations',
        value: yearly_canceled_count || 0
    },
    {
        linkTo: '/cases',
        label: 'Total Discount Applied',
        value: new Intl.NumberFormat('en-us', { style: 'currency', currency: 'USD' }).format(discount_applied || 0)
    }
]);


export const mapQuestionsSentToClientPie = ({ frongs = {}, rfa = {}, rfpd = {}, sprogs = {}, bop = {}, odd = {}, ndi = {}, nta = {} }) => ([
    {
        label: 'FROGS',
        value: frongs.QuestionsSentToClient || 0
    },
    {
        label: 'SPROGS',
        value: sprogs.QuestionsSentToClient || 0
    },
    {
        label: 'RFA',
        value: rfa.QuestionsSentToClient || 0
    },
    {
        label: 'RFPD',
        value: rfpd.QuestionsSentToClient || 0
    },
    {
        label: 'BOP',
        value: bop.QuestionsSentToClient || 0
    },
    {
        label: 'ODD',
        value: odd.QuestionsSentToClient || 0
    },
    {
        label: 'NDI',
        value: ndi.QuestionsSentToClient || 0
    },
    {
        label: 'NTA',
        value: nta.QuestionsSentToClient || 0
    },
]);


export const mapQuestionsRespondedByClientPie = ({ frongs = {}, rfa = {}, rfpd = {}, sprogs = {}, bop = {}, odd = {}, ndi = {}, nta = {} }) => ([
    {
        label: 'FROGS',
        value: frongs.QuestionsRespondedByClient || 0
    },
    {
        label: 'SPROGS',
        value: sprogs.QuestionsRespondedByClient || 0
    },
    {
        label: 'RFA',
        value: rfa.QuestionsRespondedByClient || 0
    },
    {
        label: 'RFPD',
        value: rfpd.QuestionsRespondedByClient || 0
    },
    {
        label: 'BOP',
        value: bop.QuestionsRespondedByClient || 0
    },
    {
        label: 'ODD',
        value: odd.QuestionsRespondedByClient || 0
    },
    {
        label: 'NDI',
        value: ndi.QuestionsRespondedByClient || 0
    },
    {
        label: 'NTA',
        value: nta.QuestionsRespondedByClient || 0
    },
]);


export const mapGeneratedDocumentsPie = ({ frongs = {}, rfa = {}, rfpd = {}, sprogs = {}, bop = {}, odd = {}, ndi = {}, nta = {} }) => ([
    {
        label: 'FROGS',
        value: frongs.GeneratedDocuments || 0
    },
    {
        label: 'SPROGS',
        value: sprogs.GeneratedDocuments || 0
    },
    {
        label: 'RFA',
        value: rfa.GeneratedDocuments || 0
    },
    {
        label: 'RFPD',
        value: rfpd.GeneratedDocuments || 0
    },
    {
        label: 'BOP',
        value: bop.GeneratedDocuments || 0
    },
    {
        label: 'ODD',
        value: odd.GeneratedDocuments || 0
    },
    {
        label: 'NDI',
        value: ndi.GeneratedDocuments || 0
    },
    {
        label: 'NTA',
        value: nta.GeneratedDocuments || 0
    },
]);


export const mapFinalDocumentsPie = ({ frongs = {}, rfa = {}, rfpd = {}, sprogs = {}, bop = {}, odd = {}, ndi = {}, nta = {} }) => ([
    {
        label: 'FROGS',
        value: frongs.FinalDocuments || 0
    },
    {
        label: 'SPROGS',
        value: sprogs.FinalDocuments || 0
    },
    {
        label: 'RFA',
        value: rfa.FinalDocuments || 0
    },
    {
        label: 'RFPD',
        value: rfpd.FinalDocuments || 0
    },
    {
        label: 'BOP',
        value: bop.FinalDocuments || 0
    },
    {
        label: 'ODD',
        value: odd.FinalDocuments || 0
    },
    {
        label: 'NDI',
        value: ndi.FinalDocuments || 0
    },
    {
        label: 'NTA',
        value: nta.FinalDocuments || 0
    },
]);



export const mapPracticesLine = (byMonth = {}, value, year) => byMonth && (Object.keys(byMonth).reduce((a, el, index) => {
    a.push(Object.assign({}, { 
        name: el,
        value: year.toString() === currYear.toString() && index >= parseInt(currMonth)-1 ? NaN : byMonth[el][value] || 0
    }));
    return a;
}, [])) || [];


export const mapRevenueLine = (byMonth = {}, value, year) => byMonth && (Object.keys(byMonth).reduce((a, el, index) => {
    a.push(Object.assign({}, { 
        name: el,
        value: year.toString() === currYear.toString() && index >= parseInt(currMonth)-1 ? NaN : byMonth[el][value] || 0
    }));
    return a;
}, [])) || [];

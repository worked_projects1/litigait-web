

/**
 *
 * Vew Questions Form Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import QuestionBox from './components/QuestionBox';
import ButtonSpinner from 'components/ButtonSpinner';
import ModalForm from 'components/ModalRecordForm';
import moment from 'moment';
import { isPropound, autoIncrementQuestions } from 'utils/tools';
import lodash from 'lodash';

const Default_Question = [Object.assign({}, { question_id: 1, question_number: '1', question_number_text: '1', question_text: '', question_options: '' })];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectSessionExpand, selectUser, selectBtnLoader, selectDefendantPracticeDetails } = selectors;

    const { questionFormColumns, opposingCounselFormColumns } = columns;

    /**
     * @param {object} data 
     * @param {function} dispatch 
     * @param {string} queryParams 
     */
    function handleForm(data, dispatch, queryParams) {
        let submitData;
        if(queryParams && queryParams === 'quickCreate') {
            submitData = Object.assign({}, {
                practice_id: data.practice_id,
                case_id: data.case_id,
                client_id: data.client_id,
                questions: data.questions,
                document_type: data.document_type,
                content_type: 'application/pdf',
                from: 'questions_form',
                form: 'questionsForm',
                legalforms_id: data.legalforms_id,
                state: data.state,
                extraction_id: data.id
            });
            dispatch(actions.quickCreateSaveQuestionsForm(submitData));
        } else {
            submitData = Object.assign({}, {
                practice_id: data.practice_id,
                case_id: data.id,
                client_id: data.client_id,
                file_name: data.type,
                questions: data.questions,
                document_type: data.document_type,
                content_type: 'application/pdf',
                from: 'questions_form',
                form: 'questionsForm',
                filename: data.filename,
                legalforms_id: data.legalform_id,
                state: data.state
            });
            dispatch(actions.saveQuestionsForm(submitData));
        }
    }

    /**
     * @param {object} props 
     */
    function ViewQuestionsFormPage(props) {
        
        const { location, record, dispatch, success, error, loading, progress, expand, history, match, user, queryParams, btnLoader, defendantPracticeDetails } = props;
        const classes = Styles();

        const [loadForm, setLoadForm] = useState(true);
        const [loadQuestions, setLoadQuestions] = useState(false);
        const [editable, setEditable] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [questions, setQuestions] = useState(location && location.state && ((location.state.questions && location.state.questions.length > 0 && location.state.questions) || Default_Question).map(er => Object.assign({}, { question_id: er.question_id, question_number: er.question_number, question_number_text: er.question_number_text, question_text: er.question_text, question_options: er.question_options, question_category: er.question_category, question_category_id: er.question_category_id })) || []);
        const [openForm, setOpenForm] = useState(location && location.state && location.state.opposing_counsel_form || false);

        const documentData = defendantPracticeDetails && location?.state?.legalform_id && defendantPracticeDetails?.document_type?.toLowerCase() === location.state.document_type?.toLowerCase() && defendantPracticeDetails?.legalform_id?.toString() === location.state.legalform_id?.toString() ? defendantPracticeDetails : {};
        const isPropoundPage = user && record && isPropound(user, record);
        const quickCreate = queryParams && queryParams === 'quickCreate' ? true : false;
        const opposing_counsel_info = documentData?.opposing_counsel_info || false;
        const initialValues = opposing_counsel_info ? Object.assign({}, { ...record }, { opposing_counsel: [ opposing_counsel_info ] }) : Object.assign({}, { ...record });

        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const navbarWidth = expand ? 240 : 60;

        const questionRecords = questions;

        const category = questions && questions.length > 0 && questions[0] && questions[0]['question_category'] || false;
        const questionCategory = category ? lodash.groupBy(questions, 'question_category_id') : false;

        useEffect(() => {
            let mounted = true;
            window.addEventListener('resize', () => setWidth(window.innerWidth));
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            setLoadForm(true);
            setTimeout(() => setLoadForm(false), 200);
            return () => mounted = false;
        }, [match.params.id]);

        const handleChange = (data, type) => {
            let index = questionRecords && questionRecords.length > 0 && questionRecords[0] && questionRecords[0].question_number && parseInt(questionRecords[0].question_number) &&  parseInt(questionRecords[0].question_number) > 1 ? parseInt(questionRecords[0].question_number) : 1 || 1;
            index = index - 1;
            const changedVal = (questionRecords || []).reduce((a, el, i) => {
                if (el.question_id.toString() === data.question_id.toString()) {
                    if (type === 'input') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: data.question_number.toString(), question_number_text: data.question_number.toString(), question_text: data.question_text }));
                    } else if (type == 'add') {
                        if(category) {
                            if(el && el?.question_category === data?.question_category) {
                                index++;
                                a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                                index++;
                                a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: "", question_number_text: "", question_options: "", question_text: "", question_category: data?.question_category, question_category_id: data?.question_category_id }));
                            } else {
                                a.push(Object.assign({}, { ...el }));
                            }
                        } else {
                            index++;
                            a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                            index++;
                            a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: "", question_number_text: "", question_options: "", question_text: "" }));
                        }
                    } else if (type != 'delete') {
                        if(category) {
                            if(el && el?.question_category === data?.question_category) {
                                index++;
                                a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: index.toString(), question_number_text: index.toString() }));
                            } else {
                                a.push(Object.assign({}, { ...el }));
                            }
                        } else {
                            index++;
                            a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: index.toString(), question_number_text: index.toString() }));
                        }
                    }
                } else {
                    if(category) {
                        if(el && el?.question_category === data?.question_category) {
                            index++;
                            a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                        } else {
                            a.push(Object.assign({}, { ...el }));
                        }
                    } else {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: i+1, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                    }
                }
                return a;
            }, []);

            if (type === 'delete') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal, category, data?.question_category);
                setLoadQuestions(true);
                setTimeout(() => {
                    setLoadQuestions(false);
                    setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
                }, 500);
            } else if(type == 'add') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal, category, data?.question_category);
                setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
            } else {
                setQuestions(changedVal && changedVal.length > 0 ? changedVal : Default_Question);
            }

        }

        const handleCancel = () => {
            if (editable) {
                setQuestions(location && location.state && ((location.state.questions && location.state.questions.length > 0 && location.state.questions) || Default_Question).map(er => Object.assign({}, { question_id: er.question_id, question_number: er.question_number, question_number_text: er.question_number_text, question_text: er.question_text, question_options: er.question_options })) || [])
            }
            setEditable(!editable);
        }

        const handleSaveForm = (data, dispatch, { form }) => {
            const submitRecord = Object.assign({}, { ...data }, { date_of_loss: new Date(moment(data.date_of_loss).format('MM/DD/YYYY')), opposing_counsel_info: true })
            dispatch(actions.updateRecord(submitRecord, form));
        }

        const handleClose = () => {
            if(quickCreate) {
                dispatch(actions.deleteQuickCreateDocument(record, isPropoundPage));
            } else {
                history.push({ pathname: isPropoundPage ? `/casesCategories/cases/${record.id}/forms` : `/casesCategories/cases/${record.id}/form`, state: Object.assign({}, { ...location.state }, { legalform_id: null }) })
            }
        }

        return (<Grid container className={classes.questionsFormPage}>
            {!loading && record && !loadForm && Object.keys(record).length > 0 ?
                <Grid container>
                    <Grid item xs={12} className={classes.header} style={!md ? { left: '0', width: `${width}px` } : { width: `${width - navbarWidth}px`, left: navbarWidth }}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container justify="center">
                                    {editable && <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handleForm(Object.assign({}, { ...record }, { document_type: (location && location.state && location.state.document_type) || record.document_type, questions: questionRecords, filename: location.state && location.state.filename, legalform_id: location.state && location.state.legalform_id }), dispatch, queryParams)}>
                                        {progress && <ButtonSpinner /> || 'Save'}
                                    </Button> || null}
                                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleCancel} disabled={progress}>
                                        {editable && 'Cancel' || 'Edit'}
                                    </Button>
                                    <Button type="button" onClick={() => handleClose()} color={quickCreate ? "primary": "default"}variant="contained" className={quickCreate ? classes.button : classes.btnClose} disabled={progress}>
                                        {quickCreate ? btnLoader && <ButtonSpinner /> || 'Next' : 'Close'}
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} className={classes.notes}>
                                <b>Please check if the questions listed below matches the document, use edit option to make modifications if any.</b>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} style={{ marginTop: '132px' }}>
                        <Grid container spacing={5}>
                            {!loadQuestions && category ? questionCategory && Object.keys(questionCategory).map(key => {
                                return <>
                                    <Grid>
                                        <Typography style={{ fontWeight: 'bold', paddingLeft: '12px', textDecoration: 'underline', fontSize: '16px', textTransform: 'capitalize' }}>Category - {questionCategory[key] && questionCategory[key].length > 0 && questionCategory[key][0] && questionCategory[key][0]['question_category'].toLowerCase()}</Typography>
                                    </Grid>
                                    {questionCategory && questionCategory[key] && questionCategory[key]?.length > 0 && questionCategory[key]?.map((r, i) => {
                                        return <Grid key={i} item xs={12}>
                                        {editable && <QuestionBox
                                            name={name}
                                            path={path}
                                            fields={questionFormColumns}
                                            record={r}
                                            handleChange={handleChange}
                                        /> || <Grid container direction="row" style={{ fontSize: '16px' }}>
                                                <Grid item style={{ width: '3%' }}><b>{`${r.question_number}.`}</b></Grid>
                                                <Grid item style={{ width: '97%' }}>{r.question_text || 'No Text'}</Grid>
                                            </Grid>}
                                    </Grid>
                                    })}
                                </>
                            }) : !loadQuestions && (questionRecords || []).map((r, i) => (
                                <Grid key={i} item xs={12}>
                                    {editable && <QuestionBox
                                        name={name}
                                        path={path}
                                        fields={questionFormColumns}
                                        record={r}
                                        handleChange={handleChange}
                                    /> || <Grid container direction="row" style={{ fontSize: '16px' }}>
                                            <Grid item style={{ width: '3%' }}><b>{`${r.question_number}.`}</b></Grid>
                                            <Grid item style={{ width: '97%' }}>{r.question_text || 'No Text'}</Grid>
                                        </Grid>}
                                </Grid>)) || <Spinner className={classes.spinner} style={{ marginTop: '62px' }} />}
                        </Grid>
                        <ModalForm
                            title={'Opposing counsel info'}
                            message={'Since this case is missing opposing counsel info, we found the following from the uploaded document. Please review, make any corrections if required and save.'}
                            initialValues={initialValues || {}}
                            fields={opposingCounselFormColumns && opposingCounselFormColumns.filter(_ => _.editRecord)}
                            form={`opposingCounselForms_${record.id}`}
                            btnLabel="Save"
                            show={documentData && documentData.defendant_practice_details && openForm}
                            className={classes.form}
                            onSubmitClose={true}
                            disableContainer
                            enableSubmitBtn
                            details={documentData && documentData.defendant_practice_details || false}
                            onClose={() => setOpenForm(false)}
                            enableScroll={classes.bodyScroll}
                            footerStyle={{ border: 'none' }}
                            style={{ paddingRight: '0px' }}
                            paperClassName={classes.modalPaper}
                            onSubmit={handleSaveForm} />
                    </Grid>
                </Grid> : <Spinner className={classes.spinner} />}
            <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid >);
    }

    ViewQuestionsFormPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        defendantPracticeDetails: PropTypes.object,
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress(),
        expand: selectSessionExpand(),
        user: selectUser(),
        btnLoader: selectBtnLoader(),
        defendantPracticeDetails: selectDefendantPracticeDetails()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewQuestionsFormPage);

}

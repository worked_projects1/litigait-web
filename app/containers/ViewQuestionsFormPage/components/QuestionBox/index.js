/**
 * 
 * Question Box
 * 
 */


 import React, { useState } from 'react';
 import { Grid, Typography } from '@material-ui/core';
 import FormProvider from '../FormProvider';
 import Tooltip from '@material-ui/core/Tooltip';
 import Styles from './styles';
 
 function QuestionBox(props) {
     const { record, fields, handleChange } = props;
     const classes = Styles();
 
     const [openForm, setOpenForm] = useState(false);
 
     const handleSubmit = (data) => {
         handleChange(data, 'input');
     }
 
     return (
         <Grid container spacing={3} direction="column" className={classes.section}>
             {(fields || []).map((field, index) => (
                 <Grid key={index} item xs={12} className={classes.field}>
                     <Grid container spacing={3} direction="column">
                         <Grid item xs={12} style={{paddingLeft: '0px', paddingBottom: '0px'}}>
                             <Typography variant="subtitle2" className={classes.responseType}>
                                 <b>{field.title}</b>
                             </Typography>
                         </Grid>
                         {openForm && field.editRecord ? <Grid item xs={12} style={{paddingTop: '0px'}}>
                             <FormProvider
                                 initialValues={Object.assign({}, record, {[field.value]: record[field.value].replace(/\n/g, " ")}) || {}}
                                 form={`questionForm_${field.value}_${record && record.question_id || ''}`}
                                 field={field}
                                 onInputChange={handleSubmit}
                                 onSubmit={handleSubmit}
                                 {...props}
                             />
                         </Grid> :
                             <Grid item xs={12} className={classes[field.type]} onClick={() => field.editRecord && setOpenForm(true) || null}>
                                 {record[field.value] || ''}
                             </Grid>}
                     </Grid>
                 </Grid>
             ))}
             <Grid item xs={12}>
                 <Tooltip title="Add"><img src={require('images/icons/plus.svg')} className={classes.image} onClick={() => handleChange(record, 'add')} /></Tooltip>
                 <Tooltip title="Delete"><img src={require('images/icons/trash.svg')} className={classes.image} onClick={() => handleChange(record, 'delete')} /></Tooltip>
             </Grid>
         </Grid>
     )
 }
 
 
 export default QuestionBox;
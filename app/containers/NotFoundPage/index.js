/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React, { memo, useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';


import { selectLoggedIn, selectUser } from 'blocks/session/selectors';

import messages from './messages';
import { Helmet } from 'react-helmet';
import { Grid, Button } from '@material-ui/core';
import styles from './styles';
import Spinner from 'components/Spinner';

/**
 * @param {object} props 
 */
function NotFound(props) {
  
  const { location = {}, loggedIn } = props;
  const title = <FormattedMessage {...messages.title} />;
  const description = <FormattedMessage {...messages.description} />;
  const classes = styles();
  const [showPage, setShowPage] = useState(false);

  useEffect(() => {
    setTimeout(() => setShowPage(true), 6000);
  }, []);

  return showPage && (
    <div className={classes.notFoundPage}>
      <Helmet
        title={title.props.defaultMessage}
        meta={[
          { name: 'description', content: description.props.defaultMessage },
        ]}
      />
      <Grid container justify="center" className={classes.section}>
        <Grid item xs={8}>
          <h1 className={classes.notFound}>{loggedIn ? <FormattedMessage {...messages.notFound} /> : <FormattedMessage {...messages.expired} />}</h1>
          <h2 className={classes.message}>{loggedIn ? <FormattedMessage {...messages.message} /> : <FormattedMessage {...messages.reLogin} />}</h2>
          <Link to={{ pathname: process.env.PUBLIC_PATH || '/', state: { ...location.state } }}>
            <Button
              type="button"
              variant="contained"
              color="primary">
              <FormattedMessage {...messages.backToHome} />
            </Button>
          </Link>
        </Grid>
      </Grid>
    </div>
  ) || <Spinner loading={true} showHeight />;
}


NotFound.propTypes = {
  children: PropTypes.object,
  dispatch: PropTypes.func,
  history: PropTypes.object,
  location: PropTypes.object,
  loggedIn: PropTypes.bool,
  match: PropTypes.object,
  pathData: PropTypes.object,
  user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  loggedIn: selectLoggedIn(),
  user: selectUser(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(NotFound);

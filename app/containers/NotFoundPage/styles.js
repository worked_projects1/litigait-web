

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    notFoundPage: {
        height: '100%',
        marginTop: '0 !important',
        paddingTop: '4%'
    },
    notFound: {
        fontSize: '2em',
        color: '#000',
        fontWeight: 'bold'
    },
    message: {
        fontSize: '1.5em',
        marginBottom: '1.5em',
        color: '#000'
    },
    section: {
        textAlign: 'center'
    }
}));


export default useStyles;
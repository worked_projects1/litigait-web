import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    create: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular'
    },
    settings: {
        paddingLeft: '25px',
        paddingRight: '25px'
    },
    row: {
        paddingTop: '15px'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none',
        margin: '0px'
    },
    table: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        marginTop: '40px',
        overflow: 'auto',
        marginTop: '30px',
    },
    edit: {
        padding: '20px',
        paddingTop: '0px'
    },
    section: {
        marginTop: '40px'
    },
    label: {
        fontWeight: 'bold',
        paddingRight: '25px',
    },
    value: {
        fontWeight: 'bold',
        fontSize: '14px',
    },
    dollar: {
        paddingBottom: '2px',
        paddingRight: '2px'
    },
    dollarDiv: {
        display: 'flex',
        alignItems: 'center'
    },
    spinner: {
        marginTop: '40px'
    },
    note: {
        fontSize: '16px',
        marginBottom: '20px'
    },
    icons: {
        height: '32px',
        color: '#2CA01C',
        cursor: 'pointer'
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        backgroundColor: 'gray !important'
    },
    back: {
        display: 'flex',
        alignContent: 'center',
        marginTop: '4px'
      },
    iconBack: {
        width: '30px',
        height: '30px'
    },
    link: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Bold',
        backgroundColor: '#fff !important',
        boxShadow: 'none',
        color: '#47AC39',
        fontSize: '18px'
    },
    switchBtn: {
        '@global': {
            '.MuiSwitch-colorPrimary.Mui-checked': {
                color: '#2ca01c'
            },
            '.MuiSwitch-colorPrimary.Mui-checked + .MuiSwitch-track': {
                backgroundColor: '#2ca01c'
            }
        }
    }
}));

export default useStyles;
/**
 *
 * View Practice Settings Page
 *
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import EditRecordForm from 'components/EditRecordForm';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Icons from 'components/Icons';
import { Link } from 'react-router-dom';
import messages from './messages';
import { FormattedMessage } from 'react-intl';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {

    const {
        selectPageLoader,
        selectRecord,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectSettings
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewPracticeSettingsPage(props) {
        
        const classes = Styles();
        const { dispatch, location = {}, loading, error, success, progress, match, record = {} } = props;
        const [showForm, setShowForm] = useState(false);
        const [showMedicalValidation, setShowMedicalValidation] = useState(false);
        const [pageLoader, setPageLoader] = useState(false);
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const settingsColumns = columns && typeof columns === 'function' ? columns({ actions, dispatch }) : columns;

        async function handleEdit(data, dispatch, { form }) {

            let error = false;
            if (showForm === 1) {
                dispatch(actions.updatePracticeSettings(Object.assign({}, { practice_id: match.params.id }, data, { billing_type: 'customized' }), form));
                setShowForm(false);
            } else if (showForm === 2) {
                setShowMedicalValidation(false);
                error = false;
                data.medical_history_pricing_tier.map((item, pos, rows) => {
                    if (rows.length - 1 != pos) {
                        if ((item.to >= item.from) && (pos == 0 || (item.from - rows[pos - 1].to === 1)) && item.price >= 0) {

                        } else {
                            error = true;
                            setShowMedicalValidation("Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.");
                        }
                    }
                })
                if (!error) {
                    dispatch(actions.updatePracticeSettings(Object.assign({}, { practice_id: match.params.id }, data, { billing_type: 'customized' }), form));
                    setShowForm(false);
                } else {
                    setTimeout(() => {
                        setShowMedicalValidation(false);
                    }, 2000);
                }
            }
        }

        useEffect(() => {
            let mounted = true;
            setPageLoader(true);
            setTimeout(() => setPageLoader(false), 2000);
            dispatch(actions.loadRecords());
            dispatch(actions.loadRecord(props.match.params.id));
            dispatch(actions.loadPracticeSettings(match.params.id));
            return () => mounted = false;
        }, [match.params.id]);

        const columValue = (column, value) => {
            switch (column.type) {
                case 'upload':
                    return (
                        <img
                            src={value || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                            role="presentation"
                            className={classes.img}
                        />
                    );
                case 'textarea':
                    return <p className={classes.terms}>{value != null && value != '' ? lineBreak(value) : '-'}</p>;
                case 'component':
                    return value && Array.isArray(value) && value.length > 0 ? <TableContainer>
                        <Table aria-label="spanning table" className={"medicalPrice"} style={{ maxWidth: '450px' }} >
                            <TableBody >
                                {value.map((r, i) => <TableRow key={i}>
                                    <TableCell style={{ paddingLeft: '0px' }}><b>{`Tier ${i + 1}: Pages`}</b><span style={{ marginLeft: '40px' }}>{i === (value.length - 1) ? `more than ${r.from} ` : r.from}</span>
                                        {i === (value.length - 1) ? null : <span> - </span>}
                                        {i === (value.length - 1) ? null : <span>{r.to}  </span>}
                                    </TableCell>

                                    <TableCell colSpan={i === (value.length - 1) ? 3 : 1}>
                                        {i === 0 ? 'Free' : i === (value.length - 1) ? 'Call for quote and order' : <div>${r.price}</div>}</TableCell>
                                </TableRow>)}
                            </TableBody>
                        </Table>
                    </TableContainer> : <p className={classes.value}>{value != null && value != '' ? value : '-'}</p>
                default:
                    return <p className={classes.value}>{value != null && value != '' ? value : '0'}</p>;
            }
        };

        const lineBreak = (val) => {
            return val && val.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos}>{line}</p>)
        }

        if (progress || pageLoader) {
            return <Spinner className={classes.spinner} />;
        }

        return <Grid>
            <Grid item xs={6} className={classes.back}>
                <Link to={{ pathname: `/practices/${match.params.id}/form`, state: { ...location.state } }} className={classes.link}>
                    {md ? <FormattedMessage {...messages.back} /> :  <Icons type="Back" className={classes.iconBack} />}
                </Link>
            </Grid>
            {((settingsColumns || []) || []).map((tab, index) =>
            (<Grid key={index} className={classes.section} style={index === 0 ? { marginTop: '20px' } : {}} container>
                {index === 0 && props[tab.value] && props[tab.value]['billing_type'] === 'global' &&  <Grid item xs={12}>
                    <Typography variant="body2" className={classes.note}>
                        This customer does not have a customized pricing and will be charged according to global price settings.
                    </Typography>
                </Grid> || null}
                <Grid item xs={12}>
                    <Grid container>
                        <Grid item sm={6} xs={8}>
                            <Grid container justify="space-between">
                                {tab.customTitle && typeof tab.customTitle === 'function' ? 
                                    <Typography component="h1" variant="h5" className={classes.name}>
                                        {tab.customTitle(props[tab.value], record)}
                                    </Typography> : <Typography component="h1" variant="h5" className={classes.name}>
                                        {tab.title && typeof tab.title === 'function' && props[tab.value] && tab.title(props[tab.value]) || tab.name} for {record.name}
                                    </Typography>}
                            </Grid>
                        </Grid>
                        <Grid item sm={6} xs={4}>
                            <Grid container justify={"flex-end"}>
                                {tab.edit ? sm ? <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setShowForm(tab.schemaId)}
                                    className={classes.create} >
                                    {tab.btnLabel && typeof tab.btnLabel === 'function' && props[tab.value] && tab.btnLabel(props[tab.value]) || `Change ${tab.name}`}
                                </Button> : <Icons type='Edit' className={classes.icons} onClick={() => setShowForm(tab.schemaId)} /> : null}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={showForm ? 6 : 12} className={classes.columns}>
                    {tab.columns.filter(_ => _.viewRecord).map((col, index) => {
                        const columnVal = (col.value.indexOf('.') > -1 && col.value.split('.')) || col.value;

                        const fieldVal = (Array.isArray(columnVal) && props[tab.value] && tab.filter(props[tab.value]) && tab.filter(props[tab.value])[columnVal[0]] && tab.filter(props[tab.value])[columnVal[0]][columnVal[1]]) || (props[tab.value] && tab.filter(props[tab.value]) && tab.filter(props[tab.value])[col.value]);
                        const Component = col?.render || false;

                        return (
                            <Grid key={index} >
                                <Grid className={classes.settings}>
                                    {col.label === 'Minimum Pricing' ? 
                                        <Grid container className={classes.row} alignItems={"center"}>
                                            <Grid item xs={col.label ? 3 : 12} md={!col.label ? 12 : showForm ? 2 : 7} className={classes.value} style={!col.label ? { paddingLeft: '0px' } : null}>
                                                <Table aria-label="spanning table" className={"medicalPrice"} style={{ width: '450px' }} >
                                                    <TableBody >
                                                        <TableRow>
                                                            <TableCell style={{ paddingLeft: '0px', width: '255px' }}>
                                                                <b>Minimum Pricing</b>
                                                            </TableCell>
                                                            <TableCell colSpan={1}>${fieldVal}</TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table> 
                                            </Grid>
                                        </Grid> : <Grid container className={col.label == '' ? null : classes.row} alignItems={"center"}>
                                            {col.label ? <Grid item xs={9} md={showForm ? 10 : col.size ? 3 : 5} className={classes.label}>
                                                {col.label}
                                            </Grid> : null}
                                            <Grid item xs={col.label ? 3 : 12} md={!col.label ? 12 : showForm ? 2 : 7} className={classes.value} style={!col.label ? { paddingLeft: '0px' } : null}>
                                                {col.docType || col.dollar ? <div className={classes.dollarDiv}><span className={classes.dollar}>$</span><span>{columValue(col, fieldVal)}</span></div> : col.viewSwitch ? Component && <Component record={record} classes={classes} /> : columValue(col, fieldVal)}
                                            </Grid>
                                        </Grid>}
                                </Grid>
                                {col.type !== 'component' && col.label !== 'Minimum Pricing' && <Grid item xs={12} md={showForm ? 11 : 6} ><hr className={classes.hr} /></Grid> || null}
                            </Grid>
                        )
                    })}
                </Grid>
                {!loading && showForm === tab.schemaId ?
                    <Grid item xs={12} md={showForm ? 6 : 12} className={classes.table}>
                        <Grid className={classes.edit}>
                            <EditRecordForm
                                initialValues={tab.filter(props[tab.value]) || tab.initialValues || {}}
                                form={`SettingsForm_${tab.schemaId}`}
                                name={name}
                                path={path}
                                fields={tab.columns.filter(_ => _.editRecord)}
                                confirmButton={true}
                                confirmMessage={tab.confirmMessage}
                                btnLabel="Update"
                                onSubmit={handleEdit.bind(this)}
                                locationState={location.state}
                                cancelBtn={() => <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setShowForm(false)}
                                    className={classes.cancelBtn}>
                                    Cancel
                                </Button>}
                            />
                        </Grid>
                    </Grid> : null}
                <Snackbar show={error || success || showMedicalValidation ? true : false} text={error || success || showMedicalValidation} severity={showMedicalValidation && 'error' || error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>
            ))}
        </Grid>
    }

    ViewPracticeSettingsPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        settings: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        loading: selectPageLoader(),
        settings: selectSettings(),
        error: selectUpdateError(),
        success: selectSuccess(),
        progress: selectProgress(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewPracticeSettingsPage);

}

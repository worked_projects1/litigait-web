/*
 * ViewPracticeSettingsPage Messages
 *
 * This contains all the text for the ViewPracticeSettingsPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewPracticeSettingsPage';

export default defineMessages({
    back: {
        id: `${scope}.back`,
        defaultMessage: 'Back',
      },
});
/**
 *
 * Cases Page
 *
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link, matchPath } from 'react-router-dom';
import { Grid, Typography, Button, FormControlLabel, Switch, Tooltip } from '@material-ui/core';
import Styles from './styles';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import AlertDialog from 'components/AlertDialog';
import ModalForm from 'components/ModalRecordForm';
import moment from 'moment';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { getOffset, isPropound, mycaseSessionValidation } from 'utils/tools';
import { AsYouType } from 'libphonenumber-js';
import { ImplementationTable, RecordsData, getDefaultHeaders, titleCase } from 'utils/tools';
import Icons from 'components/Icons';
import Skeleton from '@material-ui/lab/Skeleton';
import { caseTitle } from './utils';
import { selectForm, selectLitifyRecords } from 'blocks/session/selectors';
import ButtonSpinner from 'components/ButtonSpinner';
import ModalPopupRecordForm from './components/ModalPopupRecordForm';
import IntegrationModalDialog from './components/IntegrationModalDialog';
import Snackbar from 'components/Snackbar';
import { getStepper } from 'containers/CasesPage/utils';
import Stepper from 'components/Stepper';
import InfoIcon from '@material-ui/icons/Info';
import { useDidUpdate } from 'utils/tools';


/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {object} filterColumns 
 * @param {array} customizedForm 
 */
export default function (name, path, columns, actions, selectors, filterColumns, { customizedForm, attachCaseForm, clientsForm, filevineForm, filevineList, myCaseListView, userRecordFormColumns, litifyListView }, instructionNote) {

    const {
        selectLoading,
        selectRecords,
        selectRecord,
        selectError,
        selectSuccess,
        selectRecordsMetaData,
        selectProgress,
        selectSessionExpand,
        selectTotalPageCount,
        selectHeaders,
        selectPageLoader,
        selectUser,
        selectFilevineRecords,
        selectButtonLoader,
        selectFilevineHeaders,
        selectUpdateError,
        selectMyCaseRecords,
        sessionError,
        sessionLoader,
        sessionKey,
        selectStepper,
        selectManageArchiveCase,
        selectClioRecords
    } = selectors;

    /**
     * @param {object} props 
     */
    function CasesPage(props) {

        const { dispatch, records, record, children, location = {}, history, error, metaData = {}, loading, progress, expand, totalPageCount, headers = {}, pageLoader, forms, user, filevineRecords, buttonLoader, filevineHeaders, errorMsg, success, myCaseRecords, sessionError, sessionLoader, sessionKey, openStepper, litifyRecords, clioRecords } = props;
        const classes = Styles();
        const { client_name, case_title, date_of_loss, case_number, total_cost } = record;
        const { pathname } = location;
        const activeChildren = path !== pathname;
        const fullView = pathname.indexOf('form') > -1 || pathname.indexOf('signature') > -1 || pathname.indexOf('propound') > -1 ? true : false;
        const standardForm = (pathname.indexOf('standardForm') > -1) || (pathname.indexOf('questionsForm') > -1) ? true : false;
        const integrationModal = location && location.state && location.state.integrationModal || false;
        const [filter, setFilter] = useState(false);
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const lg = useMediaQuery(theme.breakpoints.up('lg'));
        const [width, setWidth] = useState(window.innerWidth);
        const [position, setPosition] = useState(getOffset('esquiretek-header'));
        const [openAttachCaseForm, setAttchCaseForm] = useState(false);
        const [openCreateClientForm, setCreateClientForm] = useState(false);
        const [openCaseEditForm, setCaseEditForm] = useState(false);
        const [archiveLoader, setArchiveLoader] = useState(false);
        const [filevinePopup, setFilevinePopup] = useState(false);
        const [myCasePopup, setMyCasePopup] = useState(false);
        const [showFileVineForm, setShowFileVineForm] = useState(false);
        const [stepper, setStepper] = useState(false);
        const [showUserForm, setShowUserForm] = useState(false);
        const [integrationType, setIntegrationType] = useState(false);
        const [viewFileVineForm, setViewFileVineForm] = useState(false);
        const [filevineLoader, setFilevineLoader] = useState(false);
        const [litifyPopup, setLitifyPopup] = useState(false);
        const [clioPopup, setClioPopup] = useState(false);
        const navbarWidth = expand ? 240 : 60;
        const TableWrapper = ImplementationTable['reactTable'];
        const editFormRecord = forms && forms[`editRecordForm_${record.id}`] && forms[`editRecordForm_${record.id}`].values || {};
        const case_titles = editFormRecord && record && caseTitle(record, editFormRecord) || '';
        const isPropoundPractice = isPropound(user, record);
        const { myCaseDetails, myCaseCredentials, mycaseSessionDiff } = user && mycaseSessionValidation(user);
        const case_from = record?.case_from === 'mycase';
        const filevineDetails = user && user.filevineDetails && user.filevineDetails || false;
        const errorMessage = sessionError && sessionError.mycase || sessionError.auth_code || sessionError.litify || sessionError.clio;

        let currentPage = filevineHeaders && filevineHeaders.page;

        const firstPageIndex = (currentPage - 1) * 15;
        const lastPageIndex = firstPageIndex + 15;
        let filterRecords = filevineRecords && filevineRecords.slice(firstPageIndex, lastPageIndex);

        let myCaseData = myCaseRecords && myCaseRecords.length > 0 && myCaseRecords.slice(firstPageIndex, lastPageIndex);

        let litifyData = litifyRecords && litifyRecords.length > 0 && litifyRecords.slice(firstPageIndex, lastPageIndex);

        const { title, steps, successMessage } = (stepper || openStepper) && getStepper(stepper || openStepper) || {};

        const userRecordFormFields = user && userRecordFormColumns && userRecordFormColumns(user).columns;

        const messageRegular = <span>Enter the following information as you would like it to appear on your printed document.</span>;
        const responseTracking = record?.attorney_response_tracking;
        const isEnableTracking = user?.practiceDetails?.global_attorney_response_tracking || false;

        let clioData = clioRecords && clioRecords.length > 0 && clioRecords.slice(firstPageIndex, lastPageIndex);
        const clioSecretDetails = user && user.clioDetails && Object.keys(user.clioDetails).length > 0 && user.clioDetails || false;

        useEffect(() => {
            let mounted = true;
            dispatch(actions.setHeadersData(getDefaultHeaders()));
            dispatch(actions.loadRecords(true));
            dispatch(actions.setFilevineHeadersData(getDefaultHeaders()));
            window.addEventListener('resize', () => {
                setPosition(getOffset('esquiretek-header'));
                setWidth(window.innerWidth);
            });
            return () => mounted = false;
        }, []);

        if (error) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        function handleEdit(record, dispatch, { form }) {
            const submitRecord = Object.assign({}, { ...record });
            if (submitRecord && submitRecord.opposing_counsel && Array.isArray(submitRecord.opposing_counsel) && submitRecord.opposing_counsel.length > 0) {
                submitRecord.opposing_counsel_info = true;
            } else {
                submitRecord.opposing_counsel_info = false;
            }
            submitRecord.date_of_loss = submitRecord.date_of_loss != "" ? new Date(moment(submitRecord.date_of_loss).format('MM/DD/YYYY')) : null;
            if (case_from && !mycaseSessionDiff) {
                dispatch(actions.mycaseRedirectAuthUrI(true, true));
            } else if (record && record.case_from && record.case_from == 'filevine' && !filevineDetails) {
                setViewFileVineForm(true);
            } else if (record?.case_from === 'litify') {
                dispatch(actions.updateLitifyRecord(submitRecord, form));
            } else {
                dispatch(actions.updateRecord(submitRecord, form));
            }
        }

        const deleteRecord = () => {
            dispatch(actions.deleteRecord(record.id));
        };

        const handleAttachCase = (submitRecord, dispatch, { form }) => {
            dispatch(actions.attachCase(Object.assign({}, { case_id: record.id, client_id: submitRecord.client_id }), form, setAttchCaseForm));
        }

        const handleCreateClient = (ClientData, dispatch, { form }) => {
            const clientRecord = Object.assign({}, { ...ClientData });
            if (clientRecord.phone) {
                const phoneType = new AsYouType('US');
                phoneType.input(clientRecord.phone);
                const phoneVal = phoneType.getNumber().nationalNumber;
                clientRecord.phone = phoneVal || clientRecord.phone;
            }
            dispatch(actions.createClientRecord(Object.assign({}, clientRecord, { case_id: record.id }), form, setCreateClientForm));
        }

        const handlePageData = (data) => {
            dispatch(actions.setHeadersData(data));
            dispatch(actions.loadRecords(true));
        }

        const handleFilter = (e) => {
            setFilter(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value }));
            handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: JSON.stringify(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value })), search: false, sort: false, page: 1 }));

        }

        const handleArchiveRecord = () => {
            setArchiveLoader(true);
            dispatch(actions.archiveCase(record.id, setArchiveLoader));
        }

        const handleFilevineSession = (data, dispatch, { form }) => {
            // dispatch(actions.filevineSession(Object.assign({}, { ...data }), form, setFilevinePopup, setStepper, setShowFileVineForm));
            const submitEditRecord = editFormRecord && Object.assign({}, { ...editFormRecord });

            if (pathname.includes('forms') || pathname.includes('form')) {
                if (submitEditRecord && submitEditRecord.opposing_counsel && Array.isArray(submitEditRecord.opposing_counsel) && submitEditRecord.opposing_counsel.length > 0) {
                    submitEditRecord.opposing_counsel_info = true;
                } else {
                    submitEditRecord.opposing_counsel_info = false;
                }
                submitEditRecord.date_of_loss = submitEditRecord.date_of_loss != "" ? new Date(moment(submitEditRecord.date_of_loss).format('MM/DD/YYYY')) : null;
                dispatch(actions.createNewFilevineSession(data, form, submitEditRecord, `editRecordForm_${record.id}`));
            } else {
                dispatch(actions.filevineSession(Object.assign({}, { ...data }), form, setFilevinePopup, setStepper, setShowFileVineForm));
            }
        }

        const handleFilevineRecords = (data, dispatch, { form }) => {
            let submitRecord = data && data.integrations && data.integrations.length > 0 ? data.integrations : [];
            setIntegrationType('filevine');
            dispatch(actions.filevineSyncData(submitRecord, form, setFilevinePopup, setShowUserForm));
        }

        const handleSyncFilevine = () => {
            if (filevineDetails) {
                setFilevineLoader(true);
            }
            dispatch(actions.filevineSync(true, setFilevinePopup, setShowFileVineForm, setStepper, setFilevineLoader))
        }

        const handleFilevinePageData = (data) => {
            dispatch(actions.setFilevineHeadersData(data));
        }

        const handleMyCaseRecords = (data, dispatch, { form }) => {
            let submitRecords = data && data.integrations && data.integrations.length > 0 ? data.integrations : [];
            setIntegrationType('mycase');
            dispatch(actions.myCaseSyncData(submitRecords, form, setMyCasePopup, setShowUserForm));
        }

        const handleMyCaseLoadRecords = () => {
            dispatch(actions.myCaseLoadRecords(true, setMyCasePopup, setStepper));
        }

        const handleRemoveFilevineSecret = () => {
            dispatch(actions.removeFilevineSecret(filevineDetails));
        }

        const handleRemoveMyCaseSecret = () => {
            dispatch(actions.deleteMyCaseSecret(myCaseDetails));
        }

        const handleClioLoadRecords = () => {
            dispatch(actions.clioLoadRecords(true, setClioPopup, setStepper));
        }

        const handleRemoveClioSecret = () => {
            dispatch(actions.deleteClioSecret(clioSecretDetails));
        }

        const handleClioRecords = (data, dispatch, { form }) => {
            let submitRecords = data && data.integrations && data.integrations.length > 0 ? data.integrations : [];
            setIntegrationType('clio');
            dispatch(actions.clioSyncData(submitRecords, form, setClioPopup, setShowUserForm));
        }

        // Add object in ascending order
        const integrationOptions = [
            {
                title: 'Clio',
                url: '',
                separator: true,
                view: true,
                imageSrc: require('images/integrations/clio.jpg'),
                description: ``,
                action: handleClioLoadRecords,
                actionCancel: handleRemoveClioSecret,
                importBtnAction: (submitProps) => {
                    return clioSecretDetails ? (
                        <Grid>
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.button2}
                                onClick={() => submitProps.handleSubmit(submitProps)}>
                                {sessionLoader ? <ButtonSpinner /> : 'Sync & Import'}
                            </Button>
                        </Grid>) : (<Grid>
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.button2}
                                onClick={() => dispatch(actions.clioRedirectAuthUrI(true, false))}>
                                Import
                            </Button>
                        </Grid>);
                },
                deleteAction: clioSecretDetails ? (cancelProps) => (
                    <AlertDialog
                        description={`Do you want to delete the Clio credentials?`}
                        onConfirm={() => cancelProps.handleCancelSubmit(cancelProps)}
                        onConfirmPopUpClose={true}
                        btnLabel1='Yes'
                        btnLabel2='No'>
                        {(open) => <Button type="button" variant="contained" color="gray" disabled={clioSecretDetails ? false : true} className={classes.btnClose} onClick={open}>Delete Credentials</Button>}
                    </AlertDialog>
                ) : null
            },
            {
                title: 'Filevine',
                url: '',
                separator: true,
                view: true,
                imageSrc: require('images/integrations/filevine_icon.png'),
                description: ``,
                action: handleSyncFilevine,
                actionCancel: handleRemoveFilevineSecret,
                importBtnAction: (submitProps) => {
                    return <Grid>
                        <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={() => submitProps.handleSubmit(submitProps)}>{filevineDetails ? 'Sync & Import' : "Import"}</Button>
                    </Grid>
                },
                deleteAction: filevineDetails ? (cancelProps) => (
                    <AlertDialog
                        description={`Do you want to delete the Filevine credentials?`}
                        onConfirm={() => cancelProps.handleCancelSubmit(cancelProps)}
                        onConfirmPopUpClose={true}
                        btnLabel1='Yes'
                        btnLabel2='No'>
                        {(open) => <Button type="button" variant="contained" color="gray" disabled={filevineDetails ? false : true} className={classes.btnClose} onClick={open}>Delete Credentials</Button>}
                    </AlertDialog>
                ) : null
            },
            {
                title: 'MyCase',
                url: '',
                separator: false,
                view: true,
                imageSrc: require('images/integrations/mycase_icon.png'),
                description: ``,
                action: handleMyCaseLoadRecords,
                actionCancel: handleRemoveMyCaseSecret,
                importBtnAction: (submitProps) => {
                    return mycaseSessionDiff ? (
                        <Grid>
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.button2}
                                onClick={() => submitProps.handleSubmit(submitProps)}>
                                {sessionLoader ? <ButtonSpinner /> : 'Sync & Import'}
                            </Button>
                        </Grid>) : (
                        <Grid>
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.button2}
                                onClick={() => dispatch(actions.mycaseRedirectAuthUrI(true, false))}>
                                {'Import'}
                            </Button>
                        </Grid>
                    );
                },
                deleteAction: myCaseDetails ? (cancelProps) => (
                    <AlertDialog
                        description={`Do you want to delete the MyCase credentials?`}
                        onConfirm={() => cancelProps.handleCancelSubmit(cancelProps)}
                        onConfirmPopUpClose={true}
                        btnLabel1='Yes'
                        btnLabel2='No'>
                        {(open) => <Button type="button" variant="contained" color="gray" disabled={myCaseDetails ? false : true} className={classes.btnClose} onClick={open}>Delete Credentials</Button>}
                    </AlertDialog>
                ) : null
            },
        ];

        const handleCloseModalRecord = (type) => {
            if (type == 'mycase') {
                setMyCasePopup(false);
            } else if (type == 'filevine') {
                setFilevinePopup(false);
            } else if (type == 'litify') {
                setLitifyPopup(false);
            } else if (type == 'clio') {
                setClioPopup(false);
            }
            dispatch(actions.updateVerifySession(true));
        }

        const handleUpdateUserDetails = (submitRecords, dispatch, { form }) => {
            if (integrationType == 'mycase') {
                dispatch(actions.mycaseUpdateUserDetails(submitRecords, form, Object.assign({}, { integrationPopup: setMyCasePopup, integration_form: 'mycase_integration', setShowUserForm })));
            } else if (integrationType == 'filevine') {
                dispatch(actions.filvineUpdateUserDetails(submitRecords, form, Object.assign({}, { integrationPopup: setFilevinePopup, integration_form: 'filevine_integration', setShowUserForm })));
            } else if (integrationType == 'litify') {
                dispatch(actions.litifyUpdateUserDetails(submitRecords, form, Object.assign({}, {
                    integrationPopup: setLitifyPopup, integration_form: 'litify_integration', setShowUserForm
                })));
            } else if (integrationType == 'clio') {
                dispatch(actions.clioUpdateUserDetails(submitRecords, form, Object.assign({}, {
                    integrationPopup: setClioPopup, integration_form: 'clio_integration', setShowUserForm
                })))
            }
        }

        const handleSwitchHistory = () => {
            dispatch(actions.attorneyResponseTracking(Object.assign({}, { ...record }, { attorney_response_tracking: responseTracking ? false : true }), actions))
        }

        const handleLitifyRecords = (data, dispatch, { form }) => {
            let submitRecords = data && data.integrations && data.integrations.length > 0 ? data.integrations : [];
            setIntegrationType('litify');
            dispatch(actions.createLitifyBulkRecords(submitRecords, form, setLitifyPopup, setShowUserForm));
        }

        const handleArchiveCases = (data) => {
            dispatch(actions.archiveCase(data.id));
        }

        return !fullView && !standardForm ? (<Grid container>
            <Grid item xs={12}>
                <Grid container alignItems="center">
                    {filterColumns && filterColumns[name] && filterColumns[name].length > 0 ?
                        <Grid item sm={6} xs={12}>
                            {filterColumns[name].map((filterColumn, index) =>
                                <Grid key={index} className={classes.dropdownGrid}>
                                    <select className={classes.filter} name={filterColumn.value} defaultValue={""} onChange={handleFilter}>
                                        {metaData[filterColumn.options] === undefined ? <option value=''>Assigned Attorney: All</option> : ((filterColumn && Array.isArray(filterColumn.options) && filterColumn.options.length > 0 && filterColumn.options) || (filterColumn && typeof filterColumn.options === 'string' && metaData[filterColumn.options] && metaData[filterColumn.options].length > 0 && metaData[filterColumn.options]) || []).map((option, i) => <option key={i} disabled={option.disabled} value={option.value}>{option.label}</option>)}
                                    </select>
                                </Grid>)}
                        </Grid> : null}
                    <Grid item sm={6} xs={12} className={classes.addBtn}>
                        <Grid container justify={sm ? "flex-end" : "flex-start"} direction="row" alignItems="center">
                            <Grid style={{ marginRight: '15px' }}>
                                <IntegrationModalDialog
                                    disableContainer
                                    onConfirmPopUpClose={true}
                                    description={`EsquireTek allows you to import case/client details from Clio, Filevine or MyCase. You can use EsquireTek features to gather client inputs and generate the final doc, and upload it to Clio, Filevine or MyCase.`}
                                    options={integrationOptions && integrationOptions.filter(_ => _.view)}>
                                    {(open) => <Grid>
                                        <Button type="button" variant="contained" color="primary" className={classes.integrations} onClick={open}>
                                            {(buttonLoader || filevineLoader) ? <ButtonSpinner /> : <FormattedMessage {...messages.integrations} />}
                                        </Button>
                                    </Grid>}
                                </IntegrationModalDialog>
                            </Grid>
                            <Link to={{ pathname: `${path}/create`, state: { ...location.state } }}>
                                <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    className={classes.create} >
                                    <FormattedMessage {...messages.create} />
                                </Button>
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container direction={!sm ? "column-reverse" : null} wrap={!sm ? "nowrap" : null}>
                <Grid item xs={12} md={activeChildren ? 6 : 12} className={classes.table}>
                    <TableWrapper
                        records={loading ? RecordsData : filter && records.filter(item => Object.keys(filter).filter(key => ((item[key] && item[key].indexOf(filter[key]) > -1) || filter[key] === '')).length === Object.keys(filter).length) || records}
                        columns={columns && typeof columns === 'function' && columns(records, metaData, false, Object.assign({}, props, { handleSubmit: handleArchiveCases, actions })).columns || columns || []}
                        children={activeChildren}
                        path={path}
                        name={name}
                        history={history}
                        locationState={location.state}
                        metaData={metaData}
                        view={true}
                        totalPageCount={totalPageCount}
                        onChangeData={handlePageData}
                        headersData={headers}
                        loading={loading}
                        user={user}
                        manageArchiveCase={props.manageArchiveCase}
                    />
                </Grid>
                {activeChildren ?
                    <Grid item xs={12} md={6}>
                        <div className="children">
                            {children}
                        </div>
                    </Grid> : null}
            </Grid>
            {filevineRecords && filevineRecords.length > 0 && <Grid item>
                <ModalPopupRecordForm
                    initialValues={Object.assign({}, { integrations: filevineRecords }) || {}}
                    title={'Filevine integration'}
                    columns={filevineList}
                    fields={filevineList.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`filevine_integration`}
                    show={filevinePopup}
                    paperClassName={classes.modalPaper1}
                    onClose={() => handleCloseModalRecord('filevine')}
                    onSubmit={handleFilevineRecords.bind(this)}
                    disableContainer
                    records={filterRecords}
                    btnLabel="Import Clients & Cases"
                    metaData={metaData}
                    path={path}
                    name={name}
                    location={location}
                    enableSubmitBtn
                    loading={loading}
                    enableScroll={filevineRecords && filevineRecords.length > 6 ? classes.bodyScroll : false}
                    headersData={filevineHeaders}
                    onChangeData={handleFilevinePageData}
                    totalRecords={filevineRecords}
                    totalPageCount={filevineRecords.length}
                />
            </Grid>}
            {myCaseRecords && myCaseRecords.length > 0 && <Grid item>
                <ModalPopupRecordForm
                    initialValues={Object.assign({}, { integrations: myCaseRecords }) || {}}
                    title={'MyCase Integration'}
                    columns={myCaseListView}
                    fields={myCaseListView.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`mycase_integration`}
                    show={(myCasePopup || integrationModal == 'mycase') && myCaseRecords && myCaseRecords.length > 0 ? true : false}
                    paperClassName={classes.modalPaper1}
                    onClose={() => handleCloseModalRecord('mycase')}
                    onSubmit={handleMyCaseRecords.bind(this)}
                    disableContainer
                    records={myCaseData}
                    btnLabel="Import Clients & Cases"
                    metaData={metaData}
                    path={path}
                    page="cases"
                    name={name}
                    location={location}
                    enableSubmitBtn
                    loading={loading}
                    enableScroll={myCaseRecords && myCaseRecords.length > 6 ? classes.bodyScroll : false}
                    headersData={filevineHeaders}
                    onChangeData={handleFilevinePageData}
                    totalRecords={myCaseRecords}
                    totalPageCount={myCaseRecords.length}
                />
            </Grid>}
            {litifyRecords && litifyRecords.length > 0 && <Grid item>
                <ModalPopupRecordForm
                    initialValues={Object.assign({}, { integrations: litifyRecords }) || {}}
                    title={'Litify Integration'}
                    columns={litifyListView}
                    fields={litifyListView.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`litify_integration`}
                    show={(litifyPopup || integrationModal == 'litify') && litifyRecords && litifyRecords.length > 0 ? true : false}
                    paperClassName={classes.modalPaper1}
                    onClose={() => handleCloseModalRecord('litify')}
                    onSubmit={handleLitifyRecords.bind(this)}
                    disableContainer
                    records={litifyData}
                    btnLabel="Import Clients & Cases"
                    metaData={metaData}
                    path={path}
                    page="cases"
                    name={name}
                    location={location}
                    enableSubmitBtn
                    loading={loading}
                    enableScroll={litifyRecords && litifyRecords.length > 6 ? classes.bodyScroll : false}
                    headersData={filevineHeaders}
                    onChangeData={handleFilevinePageData}
                    totalRecords={litifyRecords}
                    totalPageCount={litifyRecords.length}
                />
            </Grid>}
            {clioRecords && clioRecords.length > 0 && <Grid item>
                <ModalPopupRecordForm
                    initialValues={Object.assign({}, { integrations: clioRecords }) || {}}
                    title={'Clio Integration'}
                    columns={myCaseListView}
                    fields={myCaseListView.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`clio_integration`}
                    show={(clioPopup || integrationModal == 'clio') && clioRecords && clioRecords.length > 0 ? true : false}
                    paperClassName={classes.modalPaper1}
                    onClose={() => handleCloseModalRecord('clio')}
                    onSubmit={handleClioRecords.bind(this)}
                    disableContainer
                    records={clioData}
                    btnLabel="Import Clients & Cases"
                    metaData={metaData}
                    path={path}
                    page="cases"
                    name={name}
                    location={location}
                    enableSubmitBtn
                    loading={loading}
                    enableScroll={clioRecords && clioRecords.length > 6 ? classes.bodyScroll : false}
                    headersData={filevineHeaders}
                    onChangeData={handleFilevinePageData}
                    totalRecords={clioRecords}
                    totalPageCount={clioRecords.length}
                />
            </Grid>}
            <ModalForm
                messageRegular={messageRegular}
                fields={userRecordFormFields.filter(_ => _.editRecord && !_.disableRecord)}
                show={showUserForm}
                form={`userDetailsForm`}
                onSubmit={handleUpdateUserDetails}
                metaData={metaData}
                btnLabel={'Submit'}
                onClose={() => setShowUserForm(false)}
                paperClassName={classes.modalPaper} />
            <Grid item>
                {!loading && <ModalForm
                    initialValues={Object.assign({}, { is_store_filevine_secret: true }) || {}}
                    show={showFileVineForm}
                    title={'Filevine Integration'}
                    fields={filevineForm && filevineForm.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`filevineSecretForm`}
                    onSubmit={handleFilevineSession.bind(this)}
                    btnLabel="Submit"
                    instructionNote={instructionNote}
                    onClose={() => setShowFileVineForm(false)}
                    style={{ paddingRight: '0px' }} />}
            </Grid>
            <Grid item xs={12}>
                {!loading ? <Stepper
                    title={title}
                    active={sessionKey}
                    show={(stepper || openStepper) ? true : false}
                    close={() => setStepper(false)}
                    steps={steps}
                    error={errorMsg || errorMessage || error}
                    disableCancelBtn
                    errorBtnText="Close"
                    errorBtnAction={() => setStepper(false)}
                    successMessage={successMessage} /> : null}
            </Grid>
            <Snackbar show={errorMsg || errorMessage || success ? true : false} text={errorMsg || success || errorMessage} severity={(errorMsg || errorMessage) && 'error' || 'success'} handleClose={() => errorMessage ? dispatch(actions.clearCache()) : dispatch(actions.loadRecordsCacheHit())} />
        </Grid>) :
            <Grid container className="ViewCasesPage">
                {!standardForm && record && Object.keys(record).length > 0 ? <Grid item xs={12} id="esquiretek-cases-header" className={classes.header} style={!md ? { left: '0', width: `${width}px`, top: `${position}px` } : { width: `${width - navbarWidth}px`, top: `${position}px`, left: navbarWidth }}>
                    <Grid container>
                        <Grid item xs={12}>
                            <Grid container>
                                <Grid item xs={6} className={classes.back}>
                                    {pageLoader ? <Skeleton animation="wave" width={80} /> :
                                        <Link to={location.state && location.state.document_type && Object.assign({}, { pathname: (isPropoundPractice) && location.state.tabValue ? `/casesCategories/cases/${record.id}/forms/${location.state.tabValue}` : `/casesCategories/cases/${record.id}/form`, state: Object.assign({}, { title: location.state.title }, { id: location.state.id, caseRecord: record }) }) || Object.assign({}, { pathname: path, state: Object.assign({}, { ...location.state }) })} className={classes.link}>
                                            {md ? <FormattedMessage {...messages.back} /> : <Icons type="Back" className={classes.iconBack} />}
                                        </Link>}
                                </Grid>
                                <Grid item xs={6}>
                                    {pageLoader ? <Grid container justify="flex-end">
                                        <Skeleton animation="wave" width={180} />
                                    </Grid> :
                                        <Grid container justify="flex-end">
                                            <Grid>
                                                <AlertDialog
                                                    description={`Archiving closed cases improves usability by not showing closed/past cases and improving page load speed. Archived cases are not deleted and can be un-archived. Do you want to proceed?`}
                                                    onConfirm={handleArchiveRecord}
                                                    onConfirmPopUpClose
                                                    btnLabel1='Yes'
                                                    btnLabel2='No' >
                                                    {(open) => md ? <Button type="button" variant="contained" className={classes.buttonArchive} onClick={open} >
                                                        {archiveLoader && <ButtonSpinner color='#2ca01c' /> || <FormattedMessage {...messages.archive} />}
                                                    </Button> : archiveLoader && <ButtonSpinner color='#2ca01c' /> || <Icons type='Archive' style={{ width: '28px' }} className={classes.icons} onClick={open} />}
                                                </AlertDialog>
                                            </Grid>
                                            <Grid>
                                                <AlertDialog
                                                    description={`This feature creates a new case with a copy of data from this case (use this feature when representing multiple clients for the same incident). <br/> <br/> Copied data include Case Title, Date of Loss, Case Number, State, County, Assigned Attorney and all uploaded forms. Copied data does not include medical history, responses from client and attorney.`}
                                                    onConfirm={() => setAttchCaseForm(true)}
                                                    onConfirmPopUpClose
                                                    btnLabel1='CONTINUE'
                                                    btnLabel2='CANCEL' >
                                                    {(open) => md ? <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>
                                                        <FormattedMessage {...messages.attachCase} />
                                                    </Button> : <Icons type='Duplicate' className={classes.icons} onClick={open} />}
                                                </AlertDialog>
                                                {/** 
                                             *  Form to choose client and create Duplicate Case
                                             */}
                                                <ModalForm
                                                    title={'Choose Client'}
                                                    fields={attachCaseForm.filter(_ => _.editRecord && !_.disableRecord)}
                                                    form={`attachCaseForm_${record.id}`}
                                                    show={openAttachCaseForm}
                                                    onClose={() => setAttchCaseForm(false)}
                                                    onSubmit={handleAttachCase.bind(this)}
                                                    btnLabel="Create Duplicate Case"
                                                    footerBtn={() => (<Grid container justify={lg ? "flex-start" : "flex-end"} style={lg ? { position: 'relative', top: '44px' } : null}>
                                                        <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={() => {
                                                            setCreateClientForm(true);
                                                            setAttchCaseForm(false);
                                                        }}>
                                                            <FormattedMessage {...messages.createClient} />
                                                        </Button>
                                                    </Grid>)}
                                                    footerStyle={{ border: 'none' }}
                                                    confirmButton
                                                    confirmMessage={`This feature creates a new case with a copy of data from this case (use this feature when representing multiple clients for the same incident). <br/> <br/> Copied data include Case Title, Date of Loss, Case Number, State, County, Assigned Attorney and all uploaded forms. Copied data does not include medical history, responses from client and attorney. <br/> <br/> Do you want to create duplicate case?`}
                                                    confirmPopUpClose
                                                    paperClassName={classes.modalPaper}
                                                    metaData={metaData} />

                                                {/** 
                                             *  Form to create client and create Duplicate Case
                                             */}
                                                <ModalForm
                                                    title={'Create Client and Duplicate Case'}
                                                    fields={columns && typeof columns === 'function' && columns(record, metaData).columns && columns(record, metaData, forms).clientsForm.filter(_ => _.editRecord && !_.disableRecord)}
                                                    form={`clientsForm_${record.id}`}
                                                    show={openCreateClientForm}
                                                    onClose={() => setCreateClientForm(false)}
                                                    onSubmit={handleCreateClient}
                                                    footerStyle={{ border: 'none' }}
                                                    confirmButton
                                                    confirmMessage={`This feature creates a new client and new case with a copy of data from this case (use this feature when representing multiple clients for the same incident). <br/> <br/> Copied data include Case Title, Date of Loss, Case Number, State, County, Assigned Attorney and all uploaded forms. Copied data does not include medical history, responses from client and attorney. <br/> <br/> Do you want to create duplicate case?`}
                                                    confirmPopUpClose
                                                    enableScroll={classes.bodyScroll}
                                                    keepDirtyOnReinitialize
                                                    metaData={metaData} />
                                            </Grid>
                                            <Grid>
                                                {md ? <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => setCaseEditForm(true)}>
                                                    <FormattedMessage {...messages.edit} />
                                                </Button> : <Icons type='Edit' className={classes.icons} onClick={() => setCaseEditForm(true)} />}
                                                <ModalForm
                                                    initialValues={Object.assign({}, record, { case_title: case_titles }) || {}}
                                                    title={client_name}
                                                    show={openCaseEditForm}
                                                    confirmMessage={case_from ? (!mycaseSessionDiff ? `We couldn’t update this change to your MyCase account due to session expired. Please click Activate button that will redirect to MyCase site. Once you logged in, you can try to update the case again.` : false) : false}
                                                    confirmButton={case_from ? (mycaseSessionDiff ? false : true) : false}
                                                    AlertBtnLabel1={'Activate'}
                                                    AlertBtnLabel2={'Cancel'}
                                                    page={'casesEdit'}
                                                    fields={columns && typeof columns === 'function' && columns(editFormRecord, metaData, `editRecordForm_${record.id}`, Object.assign({}, props, { actions: actions })).columns && columns(editFormRecord, metaData, false, Object.assign({}, props, { actions: actions })).columns.filter(_ => _.editRecord && !_.disableRecord && !_.create)}
                                                    form={`editRecordForm_${record.id}`}
                                                    onClose={() => setCaseEditForm(false)}
                                                    onSubmitClose={true}
                                                    onSubmit={handleEdit.bind(this)}
                                                    btnLabel="Update"
                                                    metaData={metaData}
                                                    keepDirtyOnReinitialize={true}
                                                    enableScroll={classes.bodyScroll}
                                                    footerStyle={{ border: 'none' }}
                                                    style={{ paddingRight: '0px' }} />
                                            </Grid>
                                            <Grid item>
                                                {!loading && <ModalForm
                                                    initialValues={Object.assign({}, { is_store_filevine_secret: true }) || {}}
                                                    show={viewFileVineForm}
                                                    title={'Filevine Integration'}
                                                    fields={filevineForm && filevineForm.filter(_ => _.editRecord && !_.disableRecord)}
                                                    form={`filevineSecretForm`}
                                                    onSubmit={handleFilevineSession.bind(this)}
                                                    onSubmitClose={true}
                                                    btnLabel="Submit"
                                                    instructionNote={instructionNote}
                                                    onClose={() => setViewFileVineForm(false)}
                                                    style={{ paddingRight: '0px' }} />}
                                            </Grid>
                                            {/* <Grid>
                                            <AlertDialog
                                                description={`Deleting this case will purge all documents uploaded and generated for this case. These documents cannot be recovered later. Do you want to continue?`}
                                                onConfirm={deleteRecord}
                                                btnLabel1='Delete'
                                                btnLabel2='Cancel' >
                                                {(open) => <Button type="button" variant="contained" className={classes.buttonDel} onClick={open} >
                                                    <FormattedMessage {...messages.delete} />
                                                </Button>}
                                            </AlertDialog>
                                        </Grid> */}
                                        </Grid>}
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} style={{ paddingBottom: '2px' }}>
                            <Grid container direction="row" justify="space-between">
                                {pageLoader ? <Skeleton animation="wave" width={320} /> :
                                    <Typography variant="subtitle1" >
                                        {/* <b>{client_name || ''}</b> {case_number ? `(Case: ${case_number || ''},` : ''} {date_of_loss ? `DOL: ${date_of_loss || ''},` : ''} {case_title ? `Title: ${case_title || ''},` : ''} {record && record.state ? `State: ${record.state || ''})` : ''} */}
                                        <b>{client_name || ''}</b> {case_number ? `(Case: ${case_number || ''}` : ''}{date_of_loss ? `, DOL: ${date_of_loss || ''}` : ''}{case_title ? <span style={{ textTransform: 'none' }}>{`, Title: ${titleCase(case_title) || ''}`}</span> : ''}{record && !record.federal ? `${record.state == 'PL' ? `, State: Other - Pleading Paper)` : record.state == 'NPL' ? `, State: Other - Non Pleading Paper)` : record.state ? `, State: ${record.state || ''})` : ''}` : ')'} {record && record.case_from && record.case_from === 'filevine' ? <span className={classes.badge}>
                                            FILEVINE</span> : record && record.case_from && record.case_from === 'mycase' ? <span className={classes.badge}>MYCASE</span> : record && record.case_from && record.case_from === 'litify' ? <span className={classes.badge}>LITIFY</span> : record && record.case_from && record.case_from === 'clio' ? <span className={classes.badge}>CLIO</span> : null}
                                    </Typography>}
                                {pageLoader ? <Skeleton animation="wave" width={80} /> :
                                    <Typography variant="subtitle1" style={{ textTransform: 'none', fontSize: '15px' }}>
                                        {isEnableTracking && <span>
                                            <Tooltip
                                                title={<span style={{ fontSize: '12px' }}>When enabled, you can see previous versions of responses to refer to or restore from.</span>}
                                                placement="top">
                                                <InfoIcon style={{ cursor: "pointer", width: "18px", color: "#47AC39", marginTop: "-3px", marginRight: "2px" }} />
                                            </Tooltip>
                                            <b>Response Tracking:</b>
                                            <FormControlLabel
                                                name="UserTracking"
                                                style={{ marginLeft: '0px' }}
                                                control={<Switch
                                                    color="primary"
                                                    size="small"
                                                    className={classes.switchBtn}
                                                    checked={responseTracking || false}
                                                    onChange={(e) => handleSwitchHistory()}
                                                />}
                                            />
                                        </span>}
                                        <b>Case expenses: ${total_cost && parseFloat(total_cost).toFixed(2) || '0.00'}</b>
                                    </Typography>}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid> : null}
                <Grid item xs={12} className={classes.children}>
                    {children}
                </Grid>
            </Grid>
    }

    CasesPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        headers: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        totalPageCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
        sessionError: PropTypes.object,
        sessionLoader: PropTypes.bool,
        sessionKey: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        manageArchiveCase: PropTypes.bool,
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        records: selectRecords(),
        record: selectRecord(),
        forms: selectForm(),
        error: selectError(),
        success: selectSuccess(),
        metaData: selectRecordsMetaData(),
        progress: selectProgress(),
        expand: selectSessionExpand(),
        headers: selectHeaders(),
        totalPageCount: selectTotalPageCount(),
        pageLoader: selectPageLoader(),
        user: selectUser(),
        filevineRecords: selectFilevineRecords(),
        buttonLoader: selectButtonLoader(),
        filevineHeaders: selectFilevineHeaders(),
        errorMsg: selectUpdateError(),
        myCaseRecords: selectMyCaseRecords(),
        sessionError: sessionError(),
        sessionLoader: sessionLoader(),
        sessionKey: sessionKey(),
        openStepper: selectStepper(),
        litifyRecords: selectLitifyRecords(),
        manageArchiveCase: selectManageArchiveCase(),
        clioRecords: selectClioRecords(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(CasesPage);

}

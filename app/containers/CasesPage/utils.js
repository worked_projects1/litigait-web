/**
 * 
 * Utils 
 * 
 */


/**
 * 
 * @param {object} record 
 * @param {object} formRecord 
 * @returns 
 */
export function caseTitle(record, formRecord) {

    const case_plaintiff_name = record && record.case_plaintiff_name && record.case_plaintiff_name.replace(/\s+/g, '') || false;
    const case_defendant_name = record && record.case_defendant_name && record.case_defendant_name.replace(/\s+/g, '') || false;
    let title = record && record.case_title && record.case_title.replace(/\s+/g, '') || false;
    title = title && title.trim();
    let splitStr = title && title.toLowerCase().includes('vs.') ? title.split('vs.') : title && title.toLowerCase().includes('v.') ? title.split('v.') : false;

    const case_title_check = splitStr && Array.isArray(splitStr) && splitStr.length > 0 && splitStr.includes(case_plaintiff_name) && splitStr.includes(case_defendant_name) || false;

    const plaintiff_name_edit = formRecord && formRecord.case_plaintiff_name || false;
    const defendant_name_edit = formRecord && formRecord.case_defendant_name || false;
    const case_title = formRecord && case_title_check && case_title_check ? (plaintiff_name_edit && defendant_name_edit ? `${plaintiff_name_edit} v. ${defendant_name_edit}` : plaintiff_name_edit ? plaintiff_name_edit : defendant_name_edit ? defendant_name_edit : formRecord.case_title) : record.case_title;

    return case_title;
}

export function getStepper(type) {
    switch (type) {
        case 'mycase':
            return {
                stepperId: 1,
                title: 'Import MyCase Details',
                steps: ['Fetch Clients', 'Fetch Cases', 'Merging Records'],
                successMessage: 'Client and Cases Imported Successfully'
            }
        case 'filevine':
            return {
                stepperId: 2,
                title: 'Import Filevine Details',
                steps: ['Fetch Clients', 'Fetch Cases', 'Merging Records'],
                successMessage: 'Client and Cases Imported Successfully'
            }
        case 'litify':
            return {
                stepperId: 3,
                title: 'Import Litify Details',
                steps: ['Processing', 'Fetching Cases', 'Merging Records'],
                successMessage: 'Client and Cases Imported Successfully'
            }
        case 'clio':
            return {
                stepperId: 1,
                title: 'Import Clio Details',
                steps: ['Fetch Clients', 'Fetch Cases', 'Merging Records'],
                successMessage: 'Client and Cases Imported Successfully'
            }
    }
}
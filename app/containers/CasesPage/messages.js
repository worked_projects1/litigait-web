/*
 * CasesPage Messages
 *
 * This contains all the text for the CasesPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.CasesPage';

export default defineMessages({
  create: {
    id: `${scope}.create`,
    defaultMessage: 'Add New Case',
  },
  edit: {
    id: `${scope}.edit`,
    defaultMessage: 'Edit',
  },
  delete: {
    id: `${scope}.delete`,
    defaultMessage: 'Delete',
  },
  error: {
    id: `${scope}.error`,
    defaultMessage: 'There was an error loading the resource. Please try again.',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Back',
  },
  send: {
    id: `${scope}.send`,
    defaultMessage: 'Send',
  },
  resend: {
    id: `${scope}.resend`,
    defaultMessage: 'Resend',
  },
  sendNew: {
    id: `${scope}.sendNew`,
    defaultMessage: 'Send New',
  },
  attachCase: {
    id: `${scope}.attachCase`,
    defaultMessage: 'Create Duplicate'
  },
  createClient: {
    id: `${scope}.createClient`,
    defaultMessage: 'Create Client and Duplicate Case'
  },
  archive: {
    id: `${scope}.archive`,
    defaultMessage: 'Archive'
  },
  integrations: {
    id: `${scope}.integrations`,
    defaultMessage: 'Import Case',
  },
});

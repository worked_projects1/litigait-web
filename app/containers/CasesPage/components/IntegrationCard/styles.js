

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    card: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        margin: 20,
        '@global': {
            '.MuiCardContent-root:last-child': {
                paddingBottom: '16px'
            }
        }
    },
    image: {
        height: 50,
        width: 50,
        objectFit: 'contain',
        borderRadius: '5px',
        marginRight: '5px'
    },
    imgItem: {
        display: 'flex', 
        flexWrap: "wrap",
        justifyContent:'center'        ,
        alignContent: 'center'
    },
    btn: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular',
        marginRight: '10px'
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular',
    },
    footerSection: {
        display: 'flex', 
        flexWrap: "wrap",
        justifyContent:'end'        ,
        alignContent: 'center'
    },
    title: {
        fontSize: '24px',
        fontFamily: 'Avenir-Bold',
        fontWeight: 400,
        lineHeight: 1.334,
        textTransform: 'Capitalize'
    },
    contentBody: {
        display: 'flex',
        alignItems: 'center',
    },
    separator: {
        height: '1px',
        background: 'gray',
        margin: '10px 0px',
        opacity: '0.6'
    }
}));


export default useStyles;
import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import Styles from './styles';

export default function IntegrationCard(props) {
  const classes = Styles();
  const { title, imageSrc, description, url, importBtnAction, deleteAction, separator } = props;
  return (
    <>
      <Grid container style={{ padding: '15px 10px'}}>
        <Grid item xs={6} className={classes.contentBody}>
          <img className={classes.image} src={imageSrc} alt="integration_img" />
          <Typography variant="body2" className={classes.title}>{title}</Typography>
        </Grid>
        <Grid item xs={6} className={classes.footerSection} justify="center">
          {deleteAction && typeof deleteAction === 'function' && React.createElement(deleteAction, props)}
          {importBtnAction && typeof importBtnAction === 'function' && React.createElement(importBtnAction, props)}
        </Grid>
      </Grid>
      {separator ? <div className={classes.separator} /> : null}
    </>
  );
}

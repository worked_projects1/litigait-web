/**
 * 
 *  Modal Popup Record Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { reduxForm } from 'redux-form';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';
import history from 'utils/history';
import ReactTable from '../ReactTable';
import { connect } from 'react-redux';
import { compose } from 'redux';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ModalPopupRecordForm(props) {
    const { name, path, title, message, handleSubmit, submitting, children, metaData, className, style, btnLabel, onOpen, onClose, error, show, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, disableContainer, enableSubmitBtn, disableCancelBtn, progress, footerBtn, confirmPopUpClose, paperClassName, enableScroll, page, footerBtn1, disableCancelButton, records, columns, location, loading, change, forms, headersData, onChangeData, totalPageCount, totalRecords } = props;
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const formValues = forms && forms['values'] && forms['values']['integrations'] || false;

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
        if(page === 'cases') {
            history.push({ pathname: '/casesCategories/cases', state : Object.assign({}, { ...history.location.state }, { integrationModal: false })})
        }
    }

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                    <form onSubmit={handleSubmit.bind(this)} className={classes.form} noValidate>
                        <Grid container className={classes.header} justify="space-between">
                            <Grid item xs={10}>
                                <Typography component="span" className={classes.title}>{title || ''}</Typography>
                            </Grid>
                            <Grid item xs={2} style={{ textAlign: 'end' }}>
                                {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                            </Grid>
                        </Grid>
                        {message ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{message || ''}</Typography>
                            </Grid> : null}
                            <Grid item xs={12} className={enableScroll ? enableScroll : null}>
                                <Grid className={classes.table}>
                                    <ReactTable
                                        records={records}
                                        columns={columns}
                                        children={false}
                                        path={path}
                                        name={name}
                                        history={history}
                                        locationState={location.state}
                                        view={false}
                                        metaData={metaData}
                                        sm={sm}
                                        loading={loading}
                                        change={change}
                                        formValues={formValues}
                                        headersData={headersData}
                                        onChangeData={onChangeData}
                                        totalPageCount={totalPageCount}
                                        totalRecords={totalRecords}
                                    />
                                </Grid>
                            </Grid>
                        <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                            {footerBtn1 && React.createElement(footerBtn1) || null}
                            {footerBtn && React.createElement(footerBtn) || null}
                            {confirmButton ?
                                <AlertDialog
                                    description={confirmMessage}
                                    onConfirm={() => handleSubmit()}
                                    onConfirmPopUpClose={confirmPopUpClose}
                                    btnLabel1='Yes'
                                    btnLabel2='No' >
                                    {(open) => <Button
                                        type="button"
                                        variant="contained"
                                        onClick={open}
                                        disabled={formValues && formValues.length > 0 ? false : true}
                                        color="primary"
                                        className={classes.button}>
                                        {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                    </Button>}
                                </AlertDialog> :
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={formValues && formValues.length > 0 ? false : true}
                                    className={classes.button}
                                    onClick={!invalid && onSubmitClose ? closeModal : null}>
                                    {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                </Button>}
                            {(!disableCancelBtn && !disableCancelButton) ? <Button
                                type="button"
                                variant="contained"
                                onClick={closeModal}
                                className={classes.button}>
                                Cancel
                            </Button> : true}
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ModalPopupRecordForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};


function mapStateToProps({ form }, props) {
    return {
        forms: form && form[props.form]
    }
}

export default compose(
    connect(mapStateToProps),
    reduxForm({
        form: 'modalPopupRecord',
        validate,
        enableReinitialize: true,
        touchOnChange: true,
        destroyOnUnmount: false,
        forceUnregisterOnUnmount: true
    })
)(ModalPopupRecordForm);
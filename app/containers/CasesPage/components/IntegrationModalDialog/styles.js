

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        margin: "0px 15px",
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '12px',
        width: '58%',
        outline: 'none',
        paddingBottom: '35px',
        [theme.breakpoints.down('sm')]: {
            width: '90% !important',
        }
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize'
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitleOptions: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    gridTitle: {
        margin: '5px',
        marginTop: '10px',
        paddingLeft: '20px',
        paddingRight: '20px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    gridButton: {
        textAlign: 'center',
        display: 'flex', 
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'Avenir-Bold',
        fontSize: '22px',
    },
    header:{
        padding: '5px'
    },
    subHeader: {
        padding: '15px',
        margin: '5px 0px',
        fontSize: '16px'
    },
    separator: {
        height: '1px',
        background: 'gray',
        margin: '10px 0px',
        opacity: '0.6'
    }
}));


export default useStyles;
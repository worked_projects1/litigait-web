/**
 * 
 *  Integration Modal Dialog
 * 
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import Styles from './styles';
import IntegrationCard from '../IntegrationCard';

function IntegrationModalDialog({ title, children, className, style, headerStyle, options, show, onClose, onConfirmPopUpClose, heading, disableContainer, description }) {
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const handleClose = () => {
        setModalOpen(false);
        if (onClose) {
            onClose();
        }
    }

    const handleSubmit = (option) => {
        option.action(setModalOpen, option.value)
        if (onConfirmPopUpClose) {
            handleClose();
        }
    }
    
    const handleCancelSubmit = (option) => {
        option.actionCancel(setModalOpen, option.value)
        if (onConfirmPopUpClose) {
            handleClose();
        }
    }

    return <Grid container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show}>
                <Paper className={classes.paper}>
                    <Grid container className={classes.header} justify="space-between">
                        <Grid item xs={10}>
                            <Typography component="span" className={classes.title}>{title || ''}</Typography>
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                            <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.subHeader}>
                        <Grid item xs={12}>
                            {description}
                        </Grid>                        
                    </Grid>
                    <div className={classes.separator} />
                    <Grid container wrap="nowrap" style={{ display: 'block' }}>
                        {(options || []).map((option, index) => {
                            return <IntegrationCard key={index} handleSubmit={handleSubmit} handleCancelSubmit={handleCancelSubmit} {...option} />
                        })}
                    </Grid>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

IntegrationModalDialog.propTypes = {
    title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default IntegrationModalDialog;
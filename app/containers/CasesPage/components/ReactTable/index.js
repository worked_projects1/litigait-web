/**
 *
 * React Table
 *
 */

import React, { Component } from 'react';
import Table from './Table';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';


class ReactTable extends Component {

    static propTypes = {
        records: PropTypes.array,
        columns: PropTypes.array,
        children: PropTypes.bool,
        path: PropTypes.string,
        locationState: PropTypes.object,
        view: PropTypes.bool
    };


    constructor(props) {
        super(props);
    }

    render() {
        const { name, records, columns, children, path, view, sm, metaData = {}, totalPageCount, onChangeData, headersData, loading, change, formValues, totalRecords } = this.props;
        const tableColumns = children ? columns.filter((column) => column.visible && column.viewMode) : columns.filter((column) => column.visible);

        let rows = records.map((record) => Object.assign({}, record));

        tableColumns.forEach((column) => {
            switch (column.type) {
                case 'upload':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) :
                                    <img
                                        src={`${row[column.value] || ''}`}
                                        role="presentation" style={{ height: 64, padding: 4 }}
                                    />,
                        },
                    ));
                    break;
                case 'download':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: column.html ? column.html(row, metaData) : row[column.value]
                        },
                    ));
                    break;
                case 'checkbox':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) : row[column.value] && 'Yes' || 'No',
                        },
                    ));
                    break;
                case 'select':
                case 'multiSelect':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: column.html ? column.html(row, metaData) : row[column.value],
                        },
                    ));
                    break;
                default:
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) : column.limit && row[column.value] && row[column.value].length > 60 ? `${row[column.value].toString().substring(0, 60)}...` : row[column.value] != null ? row[column.value] : '' || '',
                        },
                    ));
                    break;
            }
        });

        

        return (
            <Grid container>
                <Grid item xs={12} className={sm ? name : null}>
                    <Table
                        columns={tableColumns.map((column) => Object.assign(
                            {},
                            {
                                Header: column.sort ? column.label + '⇅' : column.label,
                                accessor: column.value,
                                sort: column.sort,
                                sortType: 'basic',
                                sortColumn: column.sortColumn
                            }
                        ))}
                        data={rows}
                        totalPageCount={totalPageCount || rows.length}
                        onChangeData={onChangeData}
                        headersData={headersData}
                        loading={loading}
                        change={change}
                        formValues={formValues}
                        totalRecords={totalRecords}
                    />
                </Grid>
            </Grid>
        );
    }

}

export default React.memo(ReactTable);

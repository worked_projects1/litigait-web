/**
 * 
 * GlobalFilter
 * 
 */


import React, { useState } from 'react';
import { useAsyncDebounce } from 'react-table';

function GlobalFilter({
  globalFilter,
  setGlobalFilter
}) {

  const [value, setValue] = useState(globalFilter);

  const onChange = useAsyncDebounce(value => {
    setGlobalFilter(value || undefined)
  }, 1000);
  
  return (
    <div>
      <label className="react-tableoptions-search--label">
        <input
          className="react-tableoptions-search--input"
          type="text"
          placeholder="Search…"
          value={value}
          onChange={e => {
            onChange(e.target.value);
            setValue(e.target.value);
          }}
        />
      </label>
    </div>
  );
}

export default GlobalFilter;

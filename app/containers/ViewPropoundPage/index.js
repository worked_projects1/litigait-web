

/**
 *
 * View Propound Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Styles from './styles';
import CountBox from './components/CountBox';
import Spinner from 'components/Spinner';
import Snackbar from 'components/Snackbar';
import ModalDialog from 'components/ModalDialog';
import { getOffset, mycaseSessionValidation } from 'utils/tools';
import { selectForm } from 'blocks/session/selectors';
import validate from '../ViewCasesPage/validation';
import { AsYouType } from 'libphonenumber-js';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import ModalForm from 'components/ModalRecordForm';
import moment from 'moment';
import { setInitialValue, clientWithCasesFunction } from '../ViewCasesPage/utils';
import FilevineSessionForm from 'components/FilevineSessionForm';
import LitifyInfoDialog from 'components/LitifyInfoDialog';
import ModalAlertDialog from 'components/ModalAlertDialog';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} legalForms 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, legalForms, actions, selectors) {
    const { clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal } = columns;
    const { selectRecord, selectUpdateError, selectSuccess, selectKey, selectPageLoader, selectBilling, selectProgress, selectUser, selectRecordsMetaData, selectRecords, selectFilevineSessionForm, selectLitifyInfoDialog } = selectors;

    /**
     * @param {object} props 
     */
    function ViewPropoundPage(props) {

        const { record, dispatch, match, loading, error, success, history, location, user = {}, metaData = {}, formDetails = {} } = props;
        const classes = Styles();
        const [progress, setProgress] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const breakPointWidth = '1000';
        const updateWidthAndHeight = () => {
            setWidth(window.innerWidth);
        };
        const { uploadColumns, changeFormColumns, editFormColumns, calendarDaysColumns, propoundForms, selectPropoundingQuestions } = legalForms(record || {}, metaData || []);        
        const propoundFormsType = record && propoundForms || [];
        const clientCaseColumns = metaData && user && record && clientCaseForm && typeof clientCaseForm === 'function' && clientCaseForm() && clientCaseForm(record, user, metaData).columns;
        const quickCreateClientCaseColumns = metaData && user && record && quickCreateClientCaseForm && typeof quickCreateClientCaseForm === 'function' && quickCreateClientCaseForm() && quickCreateClientCaseForm(record).columns;
        const case_from = record && record.case_from && ['filevine', 'mycase', 'quick_create', 'litify'].includes(record.case_from) || false;
        const clientCaseField = case_from && record && record.case_from == 'quick_create' && quickCreateClientCaseColumns || clientCaseColumns;
        const validation = validate && record && typeof validate === 'function' && validate(record, clientCaseField);
        const clientCaseFormValue = formDetails && formDetails[`client_case_${record.id}`] && formDetails[`client_case_${record.id}`].values;
        const initialRecord = record && setInitialValue && setInitialValue(record, clientCaseFormValue);
        const mycase_case_from = case_from && record && record.case_from == 'mycase' || false;
        const { mycaseSessionDiff } = user && mycaseSessionValidation(user);
        const filevineDetails = user && user.filevineDetails || false;
        const clioDetails = user && user.clioDetails || false;
        const [mailHistoryData, setMailHistoryData] = useState(false);
        const [emailSentDate, setEmailSentDate] = useState(false);

        useEffect(() => {
            dispatch(actions.loadRecords());
            dispatch(actions.loadPropoundRecord(match.params.id));
            window.addEventListener("resize", updateWidthAndHeight);
            return () => window.removeEventListener("resize", updateWidthAndHeight);
        }, [match.params.id]);

        const handleSubmit = (data, dispatch, { form }) => {
            setProgress('uploadDoc');
            dispatch(actions.uploadForm(Object.assign({}, {
                practice_id: record.practice_id,
                case_id: record.id,
                client_id: record.client_id,
                file_name: data.document_file[0].name,
                document_file: data.document_file,
                document_type: form.split('_')[1],
                content_type: 'application/pdf',
                filename: data.filename,
                state: record.state,
                federal: record.federal || false,
                federal_district: record.federal_district || false
            }), form));
        }

        const handleDeleteForm = (data) => {
            dispatch(actions.deletePropoundForm(Object.assign({}, data, { case_id: record.id }), actions.loadPropoundRecord));
        }

        const handleDeleteShred = (data) => {
            dispatch(actions.shred(Object.assign({}, data, { case_id: record.id }), actions.loadRecord));
        }

        const handleEditForm = (data) => {
            dispatch(actions.editPropoundForm(data));
        }

        const handleClientWithCases = (data, dispatch, { form }) => {
            clientWithCasesFunction(Object.assign({}, { data, form, dispatch, record, filevineDetails, clioDetails, mycaseSessionDiff, actions, mycase_case_from, case_from }));
        }

        const handleShowEmailHistory = (emailLogsData, mailSentDate) => {
            setMailHistoryData(emailLogsData);
            setEmailSentDate(mailSentDate);
        }

        if (loading || (record && !record.propounding_form_details) || (propoundFormsType && !propoundFormsType.filter(_ => _.viewForm).length)) {
            return <Spinner className={classes.spinner} />
        }

        return (<Grid container className={classes.casesPage} style={{ marginTop: `30px` }}>
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    {propoundFormsType && propoundFormsType.filter(_ => _.viewForm).map((form, key) => {
                        return (<Grid key={key} item xs={12} md={width < breakPointWidth ? 6 : 4} style={form.style || null} className={classes.cardsPadding}>
                            <CountBox
                                name={name}
                                path={path}
                                record={record}
                                columns={uploadColumns}
                                selectPropoundingQuestions={selectPropoundingQuestions}
                                changeFormColumns={changeFormColumns}
                                editFormColumns={editFormColumns}
                                calendarDaysColumns={calendarDaysColumns}
                                handleEditForm={handleEditForm}
                                handleSubmit={handleSubmit}
                                deleteForm={handleDeleteForm}
                                deleteShred={handleDeleteShred}
                                progress={props.progress}
                                locationState={location.state}
                                metaData={metaData}
                                setResponseDate={(r, form, setDueDate) => dispatch(actions.setResponseDate(r, form, setDueDate))}
                                forms={formDetails}
                                match={match}
                                dispatch={dispatch}
                                history={history}
                                handleShowEmailHistory={handleShowEmailHistory}
                                {...form} />
                        </Grid>)
                    }) || null}
                </Grid>
            </Grid>
            <Grid>
                <ModalForm
                    initialValues={initialRecord}
                    title={'Client & Case Details'}
                    fields={clientCaseField.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`client_case_${record.id}`}
                    className={classes.form}
                    show={case_from && validation && Object.keys(validation) && Object.keys(validation).length > 0}
                    confirmMessage={mycase_case_from ?(!mycaseSessionDiff ? `We couldn’t update this change to your MyCase account due to session expired. Please click Activate button that will redirect to MyCase site. Once you logged in, you can try to update the case again.` : false) : false}
                    confirmButton={mycase_case_from ?(mycaseSessionDiff ? false : true) : false}
                    onSubmit={handleClientWithCases.bind(this)}
                    btnLabel="Update"
                    disableCancelBtn
                    enableSubmitBtn
                    keepDirtyOnReinitialize={true}
                    enableScroll={classes.bodyScroll}
                    footerStyle={{ border: 'none' }}
                    style={{ paddingRight: '0px' }}
                    footerCancelBtn={(StyleClasses) => {
                        return (<Link to={{ pathname: path, state: { ...location.state }}}>
                            <Button
                                type="button"
                                variant="contained"
                                className={StyleClasses.button}>
                                Cancel
                            </Button>
                        </Link>)
                    }}
                    metaData={metaData} />
            </Grid>
            <Grid>
                <ModalAlertDialog
                    description={emailSentHistoryModal}
                    show={mailHistoryData ? true : false}
                    disableSubmitBtn={true}
                    disableCancelButton={true}
                    enableScroll={classes.bodyScroll}
                    title={`Document was sent by email on ${emailSentDate && moment(emailSentDate).format('MM/DD/YY')} to the following addresses`}
                    paperClassName={classes.paperClassName}
                    mailHistoryData={mailHistoryData}
                    closeDialog={() => setMailHistoryData(false)}
                    onClose={() => setMailHistoryData(false)}
                />
            </Grid>
            <Grid>
                {!loading && <FilevineSessionForm
                    formRecord={clientCaseFormValue}
                    record={record}
                    actions={actions}
                    dispatch={dispatch}
                    createClientCase
                    {...props} />}
            </Grid>
            <Grid>
                {!loading && <LitifyInfoDialog
                    record={record}
                    actions={actions}
                    dispatch={dispatch}
                    {...props}
                />}
            </Grid>
            {!progress ? <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} /> : null}
        </Grid >);
    }

    ViewPropoundPage.propTypes = {
        active: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
        billing: PropTypes.object,
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object,
        filevineSessionForm: PropTypes.bool,
        litifyInfoDialog: PropTypes.bool,
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        active: selectKey(),
        loading: selectPageLoader(),
        billing: selectBilling(),
        progress: selectProgress(),
        user: selectUser(),
        metaData: selectRecordsMetaData(),
        records: selectRecords(),
        formDetails: selectForm(),
        filevineSessionForm: selectFilevineSessionForm(),
        litifyInfoDialog: selectLitifyInfoDialog(),
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewPropoundPage);

}

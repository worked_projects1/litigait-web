


export function getState(record, document_type, type) {
  const state = Object.assign({}, { ...record }, { document_type });
  switch (type) {
    case 'SentToClient':
      return Object.assign({}, state, { lawyer_response_status_filter: 'All', client_response_status_filter: 'SentToClient' })
    case 'ClientResponseAvailable':
      return Object.assign({}, state, { lawyer_response_status_filter: 'All', client_response_status_filter: 'ClientResponseAvailable' })
    case 'Final':
      return Object.assign({}, state, { lawyer_response_status_filter: 'Final', client_response_status_filter: 'All' })
    default:
      return Object.assign({}, state, { lawyer_response_status_filter: 'All', client_response_status_filter: 'All' })
  }
}

export const validation = (formData, propsData) => {

  const { record = {}, value, fields, form } = propsData || {};
  const requiredFields = fields.reduce((a, el) => {
    if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object' && ((el.mandatory.create && form.indexOf('create') > -1) || (el.mandatory.edit && form.indexOf('edit') > -1)))) {
      a.push(el.value)
    }
    return a;
  }, []);
  const errors = {};
  requiredFields.forEach(field => {
    if ((!formData[field] && field != 'dob' && typeof formData[field] === 'boolean') || (!formData[field] && field != 'dob' && formData[field] != '0')) {
      errors[field] = 'Required'
    }
  });

  const duplicateFileName = formData?.filename && record && record[value] && record[value].length > 0 && record[value].find(_ => _?.file_name.toString() === formData?.filename.toString()) || false;
  if (duplicateFileName && !formData.id) {
    errors.filename = 'File Name Already Exists.';
  }

  if (formData.legalFormId) {
    const errorFileName = formData?.filename && record && record[value] && record[value].length > 0 && record[value].find(_ => _?.file_name.toString() === formData?.filename.toString() && _.id.toString() !== formData.id.toString()) || false;
    if (errorFileName) {
      errors.filename = 'File Name Already Exists.';
    }
  }

  if (formData?.filename && formData?.filename.length > 40) {
    errors.filename = 'Only 40 characters allowed.';
  }

  return errors;
}

/**
 * 
 * Propound Count Box
 * 
 */


import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Button, Typography, Card, CardContent } from '@material-ui/core';
import moment from 'moment';
import Styles from './styles';
import { useTheme } from '@material-ui/core/styles';
import AlertDialog from 'components/AlertDialog';
import { getState, validation } from './utils';
import SVG from 'react-inlinesvg';
import { reset } from 'redux-form';
import ClipLoader from 'react-spinners/ClipLoader';
import { isPropoundEntityUrl } from 'utils/tools';
import ModalForm from 'components/ModalRecordForm';
import PopOverForm from 'components/PopOverForm';
import Icons from 'components/Icons';

function CountBox(props) {
    const { label, value, title, record, deleteForm, progress, type, locationState, btnLabel1, dispatch, history, selectPropoundingQuestions, changeFormColumns, editFormColumns, handleEditForm, handleShowEmailHistory } = props;
    const [document, setDocument] = useState();
    const [filter, setFilter] = useState(false);
    const [visiblePopOver, setVisiblePopOver] = useState(false);
    const [legalFormId, setLegalFormId] = useState(false);
    
    const classes = Styles();
    const propoundFormsRecord = record && record.propounding_form_details && record.propounding_form_details;
    const propoundTemplateRecord = record && record.propound_template_details && record.propound_template_details;

    const selectedForm = legalFormId && propoundFormsRecord && propoundFormsRecord[value] && propoundFormsRecord[value].length > 0 && propoundFormsRecord[value].find(_ => _.id.toString() === legalFormId.toString()) || propoundFormsRecord && propoundFormsRecord[value] && propoundFormsRecord[value][0] || {};

    const document_files = propoundTemplateRecord && value && propoundTemplateRecord[value] && propoundTemplateRecord[value].length > 0 && propoundTemplateRecord[value];
    const selectTemplateOptions =  document_files && document_files?.map(_ => Object.assign({}, { label: _.file_name, value: _.id }));


    const propoundingRecord = propoundFormsRecord && propoundFormsRecord[value] && propoundFormsRecord[value].length > 0 ? true : false;

    const filesOptions = Object.assign({}, { filesOptions: propoundFormsRecord && propoundFormsRecord[value] && propoundFormsRecord[value].length > 0 && propoundFormsRecord[value].map(r => Object.assign({}, { value: r && r.id || '', label: r && r.file_name || '' })) || [] });

    const {
        id,
        createdAt,
        file_name,
        number_of_questions,
        number_of_questions_sent,
        public_url,
        email_sent_date,
        email_viewed_date,
        pdf_file_url,
        email_sent_to
    } = selectedForm;

    const selectTemplateColumn =  selectPropoundingQuestions && typeof selectPropoundingQuestions === 'function' ? selectPropoundingQuestions(selectTemplateOptions) : selectPropoundingQuestions;

    const handleDelete = () => {
        setDocument(Object.assign({}, { value, type }));
        deleteForm(Object.assign({}, { id: selectedForm.id }));
    }

    const handleDestroy = () => {
        dispatch(reset(`propoundStandardForm_${record.id}`));
        dispatch(reset(`standardForm_${record.id}`));
    }

    const handleFilter = (e) => {
        const { name, value } = e.target;
        setFilter(Object.assign({}, { ...filter }, { [name]: value }));
        const selectedTemplate = document_files.filter((el) => el.id === value);
        history.push({
            pathname: isPropoundEntityUrl({ id: record.id, type: selectedTemplate[0].document_type.toLowerCase(), pid: selectedTemplate[0].id }, 'standardForm'),
            state: Object.assign({}, locationState, { questions: selectedTemplate[0].questions, tabValue: '0/propound', defaultQuestions: [], formLabel: 'Propounding Questions' }, getState(record.id, label))
        });
    }

    const handleSubmit = (data, dispatch, { form, reset }) => {
        const template = data && data?.template_id;
        const selectedTemplate = document_files.filter((el) => el.id === template);
        history.push({
            pathname: isPropoundEntityUrl({ id: record.id, type: selectedTemplate[0].document_type.toLowerCase(), pid: selectedTemplate[0].id }, 'standardForm'),
            state: Object.assign({}, locationState, { questions: selectedTemplate[0].questions, tabValue: '0/propound', defaultQuestions: [], formLabel: 'Propounding Questions' }, getState(record.id, label))
        });
        dispatch(reset(form));
    }

    const handleLegalForm = (formData) => {
        if (formData && formData.legalFormId && selectedForm && selectedForm.id && formData.legalFormId.toString() != selectedForm.id.toString()) {
            setLegalFormId(formData.legalFormId);
            setVisiblePopOver(true);
            setTimeout(() => setVisiblePopOver(false), 8);
        }
    }
    

    return <Card className={classes.cards}>
        <CardContent>
            <Grid container style={{ alignItems: 'center' }}>
                <Typography variant="subtitle1" className={classes.label}>
                    {title}
                </Typography>
            </Grid>
            {propoundingRecord ?
                <Grid container style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Grid>
                        <Typography variant="subtitle1">
                            {file_name}
                            <span>
                                <ModalForm
                                    initialValues={record && Object.assign({}, record, { case_id: record.id, document_type: value, filename: file_name, legalFormId: selectedForm?.id }) || {}}
                                    fields={editFormColumns.filter(_ => _.editRecord)}
                                    form={`propoundEditForms_${value}`}
                                    btnLabel="Update"
                                    className={classes.form}
                                    onSubmitClose={true}
                                    disableContainer
                                    validate={(f, p) => validation(f, Object.assign({}, p, { record: propoundFormsRecord, value }))}
                                    onSubmit={handleEditForm.bind(this)}
                                    footerStyle={{ border: 'none' }}>
                                    {(open) => <Icons type="Edit" className={classes.editFileName} onClick={open} />}
                                </ModalForm>
                            </span>
                        </Typography>
                    </Grid>
                    {!visiblePopOver ? <PopOverForm
                        initialValues={selectedForm && Object.assign({}, selectedForm, { case_id: record.id, document_type: value, legalFormId: selectedForm.id }) || {}}
                        title="Select Form"
                        fields={changeFormColumns.filter(_ => _.editRecord)}
                        form={`propound_files_forms_${value}`}
                        className={classes.form}
                        onSubmitClose={true}
                        disableCancelBtn
                        disableHeader
                        disableFooter
                        disableContainer
                        onChange={handleLegalForm}
                        paperStyle={{ padding: '0px', paddingRight: '10px', paddingLeft: '10px', minWidth: '150px', overflow: 'hidden' }}
                        metaData={filesOptions}>
                        {(open) =>
                            propoundFormsRecord[value].length > 1 && <Typography variant="subtitle1" className={classes.filesCount} onClick={open}>
                                {propoundFormsRecord[value].length} Forms
                            </Typography>}
                    </PopOverForm> : null}
                </Grid> : null}
            <Grid container spacing={2} className={classes.body}>
                {!btnLabel1 && propoundFormsRecord && propoundFormsRecord[value] && !propoundFormsRecord[value].length && <Grid item xs={12} className={classes.dropdownGrid}>
                    <Grid>
                        <select className={classes.filter} name={'choose'} defaultValue={""} onChange={handleFilter}>
                            <option value="">{'Choose Template'}</option>
                            {document_files && document_files.map((r, i) => (<option key={i} value={r.id}>{r.file_name}</option>))}
                        </select>
                    </Grid>
                </Grid>}
                <Grid item xs={12} className={classes.count}>
                    <Typography component="div" variant="subtitle1" className={classes.fontStyle}>
                        {number_of_questions && <Link
                            to={{
                                pathname: isPropoundEntityUrl({ id: record.id, type: value, pid: id }, 'details'),
                                state: Object.assign({}, locationState, { legalforms_id: selectedForm.id, tabValue: '0/propound' }, getState(record.id, label))
                            }}
                            className={classes.quesCount}>
                            {number_of_questions || 0}
                        </Link> || <Typography className={classes.quesCountTemp} component="span" variant="subtitle1">
                                {0}
                            </Typography>}
                        {file_name || 'Template name'}
                    </Typography>
                    <Typography component="div" variant="subtitle1" className={classes.fontStyle}>
                        Uploaded {createdAt && moment(createdAt).format('MM/DD') || '--/--'}
                    </Typography>
                </Grid>
                <Grid item xs={12} className={classes.count}>
                    <Typography component="div" variant="subtitle1" className={classes.fontStyle}>
                        {number_of_questions_sent && <Link
                            to={{
                                pathname: isPropoundEntityUrl({ id: record.id, type: value, pid: id }, 'details'),
                                state: Object.assign({}, locationState, { legalforms_id: selectedForm.id, tabValue: '0/propound' }, getState(record.id, label))
                            }}
                            className={classes.quesCount}>
                            {number_of_questions_sent || 0}
                        </Link> || <Typography className={classes.quesCountTemp} component="span" variant="subtitle1">
                                {0}
                            </Typography>}
                        {'Document Emailed'}
                        {email_sent_date && <img src={require('images/icons/pdf2.png')} id={`${value}_pdf`} className={classes.attachPdf} onClick={() => window.open(pdf_file_url, '_blank')} />}
                        {(email_sent_date && email_sent_to) && <img src={require('images/icons/maillog.svg')} onClick={() => handleShowEmailHistory(email_sent_to, email_sent_date)} className={classes.mailIcon} />}
                    </Typography>
                    <Typography component="div" variant="subtitle1" className={classes.fontStyle}>
                        Sent {email_sent_date && moment(email_sent_date).format('MM/DD') || '--/--'}, Read {email_viewed_date && moment(email_viewed_date).format('MM/DD') || '--/--'}
                    </Typography>
                </Grid>
            </Grid>
            <Grid item xs={12} style={propoundingRecord ? { marginTop: '104px' } : {}}>
                <Grid container className={classes.formsBtn}>
                    {(propoundFormsRecord && propoundFormsRecord[value].length > 0) &&
                        <Grid item xs={12} md={propoundFormsRecord && propoundFormsRecord[value].length > 0 <= 4 ? 6 : 12} className={classes.formDiv} >
                            <Link
                                to={{
                                    pathname: isPropoundEntityUrl({ id: record.id, type: value, pid: id}, 'details'),
                                    state: Object.assign({}, locationState, { legalforms_id: selectedForm.id, tabValue: '0/propound' }, getState(record.id, label))
                                }}
                                className={classes.openForm}>
                                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleDestroy}>
                                        View Form
                                    </Button>
                            </Link>
                        </Grid>
                    }
                    {propoundingRecord && propoundFormsRecord && propoundFormsRecord[value].length <= 4 ?
                        <Grid item xs={12} md={6} className={classes.formDiv}>
                            <Grid>
                                <ModalForm
                                    fields={selectTemplateColumn.filter(_ => _.editRecord)}
                                    form={`select_forms_${value}`}
                                    title={'Select Questions'}
                                    btnLabel="Submit"
                                    onSubmitClose={true}
                                    onSubmit={handleSubmit.bind(this)}>
                                    {(open) =>
                                        <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>
                                            Add New Form
                                        </Button>}
                                </ModalForm>
                            </Grid>
                        </Grid> : null}
                </Grid>
            </Grid>
            <Grid item xs={12} className={classes.icons} style={{ marginTop: '20px' }}>
                <Grid container direction="row">
                    <Grid item xs={12} lg={6} className={classes.btnDivIcons}>
                        <Typography component="span" variant="subtitle2" className={classes.template} onClick={() => public_url ? window.open(public_url, '_self') : null}>
                            Document: {public_url ? <img src={require('images/icons/download.png')} className={classes.attachPDF} /> : '-'}
                        </Typography>
                    </Grid>

                    <Grid item xs={12} lg={6} className={classes.btnDivIcons} >
                        <AlertDialog
                            description={`Do you want to delete this form?`}
                            onConfirm={handleDelete}
                            onConfirmPopUpClose={true}
                            btnLabel1='Delete'
                            btnLabel2='Cancel' >
                            {(open) => <Typography component="span" variant="subtitle2" className={`${classes.template} deleteForm`} onClick={open}>
                                Delete form:  {document && document.value === value && document.type === type && progress ? <ClipLoader color="#2ca01c" /> : selectedForm && Object.keys(selectedForm).length > 0 ? <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} /> : '-'}
                            </Typography>}
                        </AlertDialog>
                    </Grid>
                </Grid>
            </Grid>
        </CardContent>
    </Card >
}



export default CountBox;

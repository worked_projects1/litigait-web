/**
 *
 * View Cases Categories Page
 *
 */



import React, { useState, useEffect, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Tabs, Tab, Paper } from '@material-ui/core';
import Styles from './styles';
import { matchPath } from 'react-router-dom';
import { getOffset } from 'utils/tools';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {

    const {
        selectLoading,
        selectUpdateError,
        selectSuccess,
        selectRecord
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewCasesCategoriesPage(props) {
        const classes = Styles();
        const { dispatch, location = {}, children, history, match, loading } = props;
        const { pathname } = location;
        const selectedTab = location && location.pathname && matchPath(location.pathname, { path: `/casesCategories/cases/:id/forms/:tab` }) && matchPath(location.pathname, { path: `/casesCategories/cases/:id/forms/:tab` }).params.tab || false;
        const sections = columns && typeof columns === 'object' && columns.section || false;
        const fullView = pathname.indexOf('details') > -1 || pathname.indexOf('standardForm') > -1 || pathname.indexOf('signature') > -1 || pathname.indexOf('questionsForm') > -1 ? true : false;
        const [position, setPosition] = useState(0);

        useEffect(() => {
            let mounted = true;
            if (!selectedTab && match) {
                history.push({ pathname: `${path}/${match.params.id}/forms/1/respond`, state: location.state });
            }
            dispatch(actions.loadRecords());
            window.addEventListener('resize', () => {
                setPosition(getOffset('esquiretek-cases-header'));
            });
            return () => mounted = false;
        }, [selectedTab]);



        const handleTabs = (e, tabIndex) => {
            const currentSection = sections.find((s, i) => i === tabIndex);
            history.push({ pathname: currentSection ? `${path}/${match.params.id}/forms/${tabIndex}/${currentSection.path}` : `${path}/${tabIndex}`, state: location.state });
        }


        return !fullView ? (<Grid container className={classes.casesPage} style={{ marginTop: `${position || getOffset('esquiretek-cases-header') || 0}px` }}>
            <Grid container id={name}>
                <Grid item xs={12}>
                    <Paper square className={classes.paper}>
                        <Tabs
                            value={parseInt(selectedTab)}
                            onChange={handleTabs}
                            variant="fullWidth"
                            indicatorColor="primary"
                            textColor="primary">
                            {sections.map((s, i) => <Tab key={i} label={s.label} wrapped />)}
                        </Tabs>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
        </Grid>) :
        <Grid container className="ViewCasesCategoriesPage">
            <Grid item xs={12}>
                {children}
            </Grid>
        </Grid>
    }

    ViewCasesCategoriesPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        error: selectUpdateError(),
        success: selectSuccess(),
        record: selectRecord(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewCasesCategoriesPage);

}

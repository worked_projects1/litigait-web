import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    casesPage: {
        marginTop: '85px',
        '@global': {
            '.MuiTypography-subtitle1': {
                fontSize: '0.84rem'
            }
        }
    },
    spinner: {
        padding: '20px',
        marginTop: '50px'
    },
    form: {
        justifyContent: 'center',
        marginTop: '21px'
    },
    cardsPadding: {
        padding: '8px !important'
    },
    paper: {
        flexGrow: 1,
        maxWidth: '75%',
        maxHeight: '58px',
        boxShadow: 'none',
        '@global': {
            '.MuiTab-textColorPrimary.Mui-selected': {
                color: '#2ca01c'
            },
            '.MuiButtonBase-root': {
                width: '135px'
            },
            '.MuiButtonBase-root > span': {
                fontFamily: 'Avenir-Bold'
            },
            '.MuiTabs-indicator': {
                backgroundColor: '#2ca01c'
            }
        }
    },
    categoryChildren: {
        marginTop: '25px'
    }
}));

export default useStyles;


/**
 * 
 * Vew Subscription Renewals Page
 * 
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import AlertDialog from 'components/AlertDialog';
import Snackbar from 'components/Snackbar';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import ButtonSpinner from 'components/ButtonSpinner';
import Skeleton from '@material-ui/lab/Skeleton';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {boolean} decoratorView 
 */
export default function (name, path, columns, actions, selectors, decoratorView) {

    const {
        selectRecord,
        selectRecordsMetaData,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectPageLoader
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewSubscriptionRenewalsPage(props) {

        const classes = Styles();
        const { record, error, success, loading, progress, dispatch, location = {}, match = {}, metaData = {}, queryParams } = props;
        const [loader, setLoader] = useState(false);
        const schemaId = location && location.state && location.state.schemaId || false;
        const section = columns && typeof columns === 'object' && schemaId && columns.section || false;
        const sectionColumns = section && schemaId && section.find(_ => _.schemaId === schemaId) || false;        

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecord(match.params.id, queryParams));
            dispatch(actions.loadRecords(false, queryParams));
            return () => mounted = false;
        }, [match.params.id]);

        const columValue = (column, row) => {
            switch (column.type) {
                case 'upload':
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row, metaData) :
                            (<img
                                src={row[column.value] || 'https://esquiretek-assets.s3.us-west-2.amazonaws.com/placeholder.jpeg'}
                                role="presentation"
                                className={classes.img}
                            />);
                default:
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row, metaData) :
                            (<p
                                className={classes.value}
                                dangerouslySetInnerHTML={{ __html: !row[column.value] && row[column.value] != '0' ? '-' : row[column.value] || '-' }}
                            />);
            }
        };

        const handleRenewalSubscription = () => {
            setLoader(true)
            dispatch(actions.subscriptionRenewals(record, setLoader));
        }


        return (
            <Grid container>
                <Grid item xs={12}>
                    <Grid container className={classes.header}>
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Link to={{ pathname: path, state: { ...location.state } }} className={classes.link}>
                                <FormattedMessage {...messages.close} />
                            </Link>}
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Grid className={classes.section}>
                                <AlertDialog
                                    description={'Do you want to renewal the subscription?'}
                                    onConfirm={handleRenewalSubscription}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='YES'
                                    btnLabel2='NO' >
                                    {(open) => <Button type="button" variant="contained" className={classes.Button} onClick={open} >
                                        {loader && <ButtonSpinner color='#2ca01c' /> || 'Renewal Subscription'}
                                    </Button>}
                                </AlertDialog>
                            </Grid>}
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            {(section && sectionColumns && sectionColumns.columns || columns || []).map((column) =>
                            (column.viewRecord ?
                                <Grid item xs={12} key={column.id}>
                                    <div>
                                        <div className={classes.label}>{column.label}:</div>
                                        {columValue(column, record)}
                                        <hr className={classes.hr} />
                                    </div>
                                </Grid> : null)
                            )}
                        </Grid>
                    </Grid>
                </Grid>
                <Snackbar show={error || success ? true : false} text={error || success} severity={error ? 'error' : 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>);
    }

    ViewSubscriptionRenewalsPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        metaData: selectRecordsMetaData(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewSubscriptionRenewalsPage);

}
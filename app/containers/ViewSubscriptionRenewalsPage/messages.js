/*
 * ViewSubscriptionRenewalsPage Messages
 *
 * This contains all the text for the ViewSubscriptionRenewals container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewSubscriptionRenewals';

export default defineMessages({
    edit: {
        id: `${scope}.edit`,
        defaultMessage: 'Edit',
    },
    delete: {
        id: `${scope}.delete`,
        defaultMessage: 'Delete',
    },
    close: {
        id: `${scope}.close`,
        defaultMessage: 'Close',
    },
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    }
});

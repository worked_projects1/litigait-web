/**
 * 
 * Contact Page 
 * 
 */


import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import ContactForm from 'components/ContactForm';
import { requestSupport } from 'blocks/session/actions';
import useStyles from './styles';

/**
 * @param {object} props 
 */
function ContactPage(props) {
    const { locationState = {}, success, clearCache } = props;
    const classes = useStyles();

    const handleSubmit = (data, dispatch, { form }) => {
        dispatch(requestSupport(data, form));
    }

    return (<Grid container>
        <Grid item xs={12} className={classes.base}>
            <Grid container>
                <Grid item xs={12} md={6} className={classes.sectionImage} />
                <Grid item xs={12} md={6}>
                    <ContactForm
                        form={`contactForm`}
                        onSubmit={handleSubmit.bind(this)}
                        success={success}
                        clearCache={clearCache}
                        locationState={locationState} />
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12} className={classes.section4}>
            <h1>READY TO GET STARTED?</h1>
            <Link
                to={{
                    pathname: '/',
                    state: {
                        form: 'register',
                        page: 'contact'
                    }
                }}
                className={classes.link}>
                <Button
                    type="button"
                    variant="contained"
                    color="primary"
                    className={classes.signupBtn}>
                    Sign Up for Free
                    </Button>
            </Link>
        </Grid>
    </Grid>)
}

ContactPage.propTypes = {
    locationState: PropTypes.object,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
    clearCache: PropTypes.func,
    errorMessage: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

export default ContactPage;
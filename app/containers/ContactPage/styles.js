import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    base: {
        marginTop: '110px', padding: '0px', paddingBottom: '0px', overflow: 'hidden'
    },
    section4: {
        padding: '25px', textAlign: 'center', backgroundColor: '#EA5D0D', backgroundImage: `url(${require('images/home/white-design.png')})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'right', backgroundSize: '48%',
        '& > h1': {
            fontFamily: 'ClearSans-Regular', fontWeight: 'normal', color: '#ffff', fontSize: '28px', letterSpacing: '4px'
        }
    },
    signupBtn: {
        backgroundColor: '#ffff !important',
        border: '2px solid #e95d0c',
        boxShadow: 'none',
        borderRadius: 'initial',
        textTransform: 'none',
        color: '#e95d0c',
        minWidth: '180px',
        fontFamily: 'ClearSans-Regular',
        '&:hover': {
            boxShadow: 'none',
        }
    },
    sectionImage: {
        backgroundImage: `url(${require('images/home/contact_banner.png')})`,
        backgroundSize: 'cover',
        minHeight: '460px',
        backgroundRepeat: 'no-repeat',
        backgroundColor: 'transparent',
        width: '100%'
    }
}));


export default useStyles;
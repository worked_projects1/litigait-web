/**
 * 
 * Archive Cases Page
 * 
 */



import React, { useEffect, useState, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@material-ui/core';
import Styles from './styles';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Snackbar from 'components/Snackbar';
import { selectForm } from 'blocks/session/selectors';
import { getDefaultHeaders } from 'utils/tools';
import { ImplementationTable, RecordsData } from 'utils/tools';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} create 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, filterColumns, create, view) {

    const {
        selectLoading,
        selectRecords,
        selectRecord,
        selectError,
        selectSuccess,
        selectRecordsMetaData,
        selectProgress,
        selectSessionExpand,
        selectTotalPageCount,
        selectHeaders,
        selectPageLoader,
        selectUpdateError,
    } = selectors;

    /**
   * @param {object} props 
   */
    
    function ArchiveCasesPage(props) {
        const [loader, setLoader] = useState(false);
        const classes = Styles();
        const { dispatch, children, records, record, location = {}, history, error, loading, metaData = {}, user = {}, success, updateError, totalPageCount, headers } = props;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const TableWrapper = ImplementationTable['reactTable'];

        useEffect(() => {
            let mounted = true;
            dispatch(actions.setHeadersData(getDefaultHeaders()));
            dispatch(actions.loadRecords(true));
            setLoader(true);
            setTimeout(() => setLoader(false), 1000);
            return () => mounted = false;
        }, []);

        const handlePageData = (data) => {
            dispatch(actions.setHeadersData(data));
            dispatch(actions.loadRecords(true));
        }

        if (error) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        if (loader) {
            return <Spinner className={classes.spinner} />
        }

        const handleUnArchiveCases = (data) => {
            setLoader(true);
            const submitRecord = Object.assign({}, { ...data });
            dispatch(actions.unArchiveCase(Object.assign({}, { practice_id: submitRecord.practice_id, case_id: submitRecord.case_id, client_id: submitRecord.client_id, id: submitRecord.id, state: submitRecord.state }), setLoader));
        }

        
        return <Grid container className={classes.ArchiveCasesPage}>
            <Grid item xs={12} md={12} className={classes.archiveTable}>
                <TableWrapper
                    records={loading ? RecordsData : records}
                    columns={columns && typeof columns === 'function' ? columns(Object.assign({}, props, { handleSubmit: handleUnArchiveCases, actions })).columns : columns}
                    path={path}
                    name={name}
                    history={history}
                    view={false}
                    locationState={location.state}
                    metaData={metaData}
                    totalPageCount={totalPageCount}
                    onChangeData={handlePageData}
                    headersData={headers}
                    loading={loading}
                />
            </Grid>
            <Snackbar show={updateError || success ? true : false} text={updateError || success} severity={updateError && 'error' || success && 'success'} handleClose={() => dispatch(actions.clearCache())} />
        </Grid>
    }

    ArchiveCasesPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        headers: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        records: PropTypes.array,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        updateError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        totalPageCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        records: selectRecords(),
        record: selectRecord(),
        forms: selectForm(),
        error: selectError(),
        success: selectSuccess(),
        metaData: selectRecordsMetaData(),
        progress: selectProgress(),
        expand: selectSessionExpand(),
        headers: selectHeaders(),
        totalPageCount: selectTotalPageCount(),
        pageLoader: selectPageLoader(),
        updateError: selectUpdateError(),
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ArchiveCasesPage);

}
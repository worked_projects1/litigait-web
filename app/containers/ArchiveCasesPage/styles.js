import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    ArchiveCasesPage: {
        '@global': {
            '.MuiFormControlLabel-label': {
                marginTop: '4px'
            }
        }
    },
    error: {
        marginTop: '12px'
    },
    spinner: {
        padding: '20px',
        marginTop: '50px'
    },
    archiveTable: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        marginTop: '40px',
        overflow: 'auto'
    }
}));

export default useStyles;
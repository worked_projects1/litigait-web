/**
 *
 * Categories Page
 *
 */



import React, { useEffect, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Grid, Tabs, Tab, Paper } from '@material-ui/core';
import Styles from './styles';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const routesUrl = ['/casesCategories/cases', '/casesCategories/cases/create', '/casesCategories/rest/archive/cases', '/supportCategories/rest/help-request', '/supportCategories/rest/propound-support', '/supportCategories/rest/document-extraction-support'];
const Routes = ['casesCategories', 'supportCategories', 'propoundSupportRequest', 'extractionSupportRequest'];

/**
 * 
 * @param {String} name 
 * @param {String} path 
 * @param {Array} categories 
 * @returns 
 */
export default function (name, path, categories) {
    
    /**
     * @param {object} props 
     */
    function CategoriesPage(props) { 

        const classes = Styles();
        const { location = {}, children, history } = props;
        const selectedTab = location && location.pathname && (categories || []).findIndex(_ => location.pathname.indexOf(`${path}/${_.path}`) > -1) || 0;
        const pathname = location && location.pathname || false;
        const showTabs = ((Routes.includes(name) && routesUrl.includes(pathname)) || (!Routes.includes(name)));

        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
    
        useEffect(() => {
            let mounted = true;
            if (selectedTab === -1 && categories && categories.length > 0 && categories[0].path) {
                history.push({ pathname: `${path}/${categories[0].path}`, state: location.state });
            }
            return () => mounted = false;
        }, [selectedTab]);

        const handleTabs = (e, tabIndex) => {
            const category = categories.find((s, i) => i === tabIndex);
            if(category)
                history.push({ pathname: `${path}/${category.path}`, state: location.state });
        }

        if (!showTabs) {
            return children;
        }

        return <Grid container id={name}>
            <Grid item xs={12}>
                <Paper square className={classes.paper}>
                    <Tabs
                        value={selectedTab}
                        onChange={handleTabs}
                        variant={md ? "fullWidth" : "standard"}
                        indicatorColor="primary"
                        textColor="primary">
                        {(categories || []).map((category, index) => <Tab key={index} label={category && category.data && category.data.title} />)}
                    </Tabs>
                </Paper>
            </Grid>
            <Grid item xs={12} className={classes.categoryChildren}>
                {children}
            </Grid>
        </Grid>
    }

    CategoriesPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object
    };

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(CategoriesPage);

}

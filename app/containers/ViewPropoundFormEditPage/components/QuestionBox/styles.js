

import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  textarea: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '150px',
    overflowY: 'auto',
    padding: '20px',
    fontSize: '14px',
    borderBottom: '1px solid #2ca01c',
    cursor: 'pointer',
    minHeight: '150px',
    maxWidth: '85%'
  },
  number: {
    color: 'black !important',
    fontSize: '14px',
    borderBottom: '2px solid #2ca01c',
    cursor: 'pointer',
    width: '100px',
    padding: '10px 5px 10px 5px'
  },
  responseType: {
    fontSize: '12px',
    marginBottom: '4px',
  },
  section: {
    margin: '0px'
  },
  field: {
    paddingTop: '0px'
  },
  lineResponse: {
    marginTop: '10px'
  },
  image: {
    width: '20px',
    height: '20px',
    marginLeft: '10px',
    cursor: 'pointer'
  }
}));


export default useStyles;
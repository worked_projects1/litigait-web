import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    header: {
        position: 'fixed',
        height: 'auto',
        zIndex: '999',
        background: '#fff',
        top: '48px',
        left: '60px',
        padding: '25px',
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
        boxShadow: '0px 3px 4px -1px lightgrey'
    },
    button: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginLeft: '20px'
    },
    btnClose: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginLeft: '20px',
        '&.MuiButton-contained.Mui-disabled': {
            backgroundColor: '#e0e0e0 !important',
            color: 'grey !important'
        }
    },
    notes: {
        fontSize: '13px',
        paddingTop: '25px'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(4)
    },
    modalPaper: {
        width: '50% !important',
        [theme.breakpoints.down('xs')]: {
            width: '92% !important'
        }
    },
    bodyScroll: {
        maxHeight: '400px',
        overflowY: 'scroll',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 12,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    }
}));

export default useStyles;
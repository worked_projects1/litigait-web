

/**
 *
 * Vew Propound Questions Edit Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import Styles from './styles';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import QuestionBox from './components/QuestionBox';
import ButtonSpinner from 'components/ButtonSpinner';
import { useDidUpdate, isPropoundEntityUrl, autoIncrementQuestions } from 'utils/tools';

const Default_Question = [Object.assign({}, { question_id: 1, question_number: '1', question_number_text: '1', question_text: '', question_options: '' })];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectSessionExpand } = selectors;

    const { questionFormColumns } = columns;

    /**
     * @param {object} data 
     * @param {function} dispatch 
     */
    function handleForm(data, dispatch, editRecord) {
        const submitData = Object.assign({}, {
            practice_id: data.practice_id,
            case_id: data.id,
            questions: data.questions,
            document_type: editRecord && editRecord[0] && editRecord[0].document_type,
            file_name: editRecord && editRecord[0] && editRecord[0].file_name,
            id: editRecord && editRecord[0] && editRecord[0].id,
            from: 'questions_form',
            form: 'questionsForm'
        });
        dispatch(actions.updatePropoundForm(submitData));
    }

    /**
     * @param {object} props 
     */
    function ViewPropoundFormEditPage(props) {

        const { location, record, dispatch, success, error, loading, progress, expand, history, match } = props;
        const classes = Styles();

        const [loadForm, setLoadForm] = useState(true);
        const [loadQuestions, setLoadQuestions] = useState(false);
        const [width, setWidth] = useState(window.innerWidth);
        const [questions, setQuestions] = useState(((record && record.questions && record.questions.length > 0 && record.questions) || Default_Question).map(er => Object.assign({}, { question_id: er.question_id, question_number: er.question_number, question_number_text: er.question_number_text, question_text: er.question_text, question_options: er.question_options })) || []);

        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const navbarWidth = expand ? 240 : 60;
        const propoundFormdata = record && record.propounding_form_details && record.propounding_form_details[match.params.type] && record.propounding_form_details[match.params.type];
        const editRecord = propoundFormdata && propoundFormdata.length > 0 && propoundFormdata.filter(elm => elm.id == match.params.pid);

        useEffect(() => {
            let mounted = true;
            window.addEventListener('resize', () => setWidth(window.innerWidth));
            dispatch(actions.loadPropoundRecord(match.params.id));
            dispatch(actions.loadRecords());
            setLoadForm(true);
            setTimeout(() => setLoadForm(false), 200);
            return () => mounted = false;
        }, [match.params.id]);

        useDidUpdate(() => {
            if (record && !record.questions) {
                dispatch(actions.loadPropoundRecord(match.params.id));                
                dispatch(actions.loadPropoundForm(Object.assign({}, {
                    case_id: match.params.id,
                    document_type: match.params.type,
                    id: match.params.pid
                }), setQuestions));
            }
        }, [record, actions, dispatch, match])

        const handleChange = (data, type) => {
            let index = questions && questions.length > 0 && questions[0] && questions[0].question_number && parseInt(questions[0].question_number) && parseInt(questions[0].question_number) > 1 ? parseInt(questions[0].question_number) : 1 || 1;
            index = index - 1;
            const changedVal = (questions || []).reduce((a, el) => {
                if (el.question_id.toString() === data.question_id.toString()) {
                    if (type === 'input') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: data.question_number.toString(), question_number_text: data.question_number.toString(), question_text: data.question_text }));
                    } else if (type == 'add') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: "", question_number_text: "", question_options: "", question_text: "" }));
                    } else if (type != 'delete') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: index.toString(), question_number_text: index.toString() }));
                    }
                } else {
                    index++;
                    a.push(Object.assign({}, { ...el }, { question_id: index, question_number: el.question_number.toString(), question_number_text: el.question_number.toString() }));
                }

                return a;
            }, []);

            if (type === 'delete') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal);
                setLoadQuestions(true);
                setTimeout(() => {
                    setLoadQuestions(false);
                    setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
                }, 500);
            } else if(type == 'add') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal);
                setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
            } else {
                setQuestions(changedVal && changedVal.length > 0 ? changedVal : Default_Question);
            }

        }

        const handleCancel = () => {
            history.push({
                pathname: isPropoundEntityUrl({ id: record.id, type: match.params.type, pid: match.params.pid}, 'details') ,
                state: Object.assign({}, location.state, { tabValue: '0/propound' })
            });
        }

        return (<Grid container className={classes.questionsFormPage}>
            {!loading && record && record.questions && record.questions.length > 0 && !loadForm && Object.keys(record).length > 0 ?
                <Grid container>
                    <Grid item xs={12} className={classes.header} style={!md ? { left: '0', width: `${width}px` } : { width: `${width - navbarWidth}px`, left: navbarWidth }}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container justify="center">
                                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handleForm(Object.assign({}, { ...record }, { document_type: (location && location.state && location.state.document_type) || record.document_type, questions, filename: location.state && location.state.filename, totalQuestions: record.questions }), dispatch, editRecord)}>
                                        {progress && <ButtonSpinner /> || 'Save'}
                                    </Button>
                                    <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleCancel} disabled={progress}>
                                        {'Cancel'}
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} style={{ marginTop: '90px' }}>
                        <Grid container spacing={5}>
                            {!loadQuestions && (questions || []).map((r, i) => (
                                <Grid key={i} item xs={12}>
                                    <QuestionBox
                                        name={name}
                                        path={path}
                                        fields={questionFormColumns}
                                        record={r}
                                        handleChange={handleChange}
                                        formName={'questionForm'}
                                    />
                                </Grid>)) || <Spinner className={classes.spinner} style={{ marginTop: '62px' }} />}
                        </Grid>
                    </Grid>
                </Grid> : <Spinner className={classes.spinner} />}
            <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid >);
    }

    ViewPropoundFormEditPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress(),
        expand: selectSessionExpand()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewPropoundFormEditPage);

}

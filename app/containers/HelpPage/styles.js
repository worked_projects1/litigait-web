import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '10px'
    },
    titleGrid: {
        color: '#404040',
        fontSize: '16px',
        padding: '6px',
        textAlign: 'center',
        fontFamily: `"Martel Sans", Sans-serif`,
        fontWeight: '400',
        letterSpacing: '1.2px'
    },
    videoResponsive: {
        overflow: 'hidden',
        paddingBottom: '56.25%',
        position: 'relative',
        height: 0
    },
    iframe: {
        left: 0,
        top: 0,
        height: '100%',
        width: '100 %',
        position: 'absolute'
    },
    title: {
        color: '#2ca01c',
        fontSize: '15px',
        fontWeight: 'bold',
        textAlign: 'center',
        padding: '10px',
        letterSpacing: '2px',
        fontFamily: `"Martel Sans", Sans-serif`,
    },
    heading: {
        color: '#848485',
        fontWeight: 450,
        textAlign: 'center',
        letterSpacing: '0.7px',
        fontSize: '45px',
        lineHeight: '50px',
        padding: '10px',
        fontFamily: `"Martel Sans", Sans-serif`,
        [theme.breakpoints.down('md')]: {
            fontSize: '33px',
        }
    },
    faqsPara: {
        color: 'rgba(69,69,69,0.8)',
        fontSize: '17px',
        lineHight: '1.8em',
        textAlign: 'center',
        fontFamily: `"Martel Sans", Sans-serif !important`,
    },
    faqTitle: {
        padding: 0,
        margin: 0,
        lineHeight: 1,
        fontSize: '27px',
        color: '#404040',
        fontFamily: `"Martel Sans", Sans-serif !important`,
    },

    listItem: {
        border: 'solid 2px grey'
    },
    paper: {
        boxShadow: '1px 2px 4px #00000045',
    }
}));

export default useStyles;
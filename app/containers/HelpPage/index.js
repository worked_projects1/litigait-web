/**
 * 
 * Settings Page
 * 
 */



import React from 'react';
import Styles from './styles';
import { Grid, Paper, Typography, CardMedia } from '@material-ui/core';
import FaqsList from './Components/FAQS';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns
 */
export default function (name, path, columns) {

    /**
     * 
     * @param {object} props 
     * @returns 
     */
    function HelpFAQsPage(props) {

        const classes = Styles();
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const helpVideos = columns?.helpVideos;
        const helpFAQs = columns?.helpFAQs;
        return (
            <Grid className={classes.root}>
                <Grid container spacing={4}>
                    {helpVideos.map((video, index) => (
                        <Grid item xs={12} sm={6} md={4} key={index}>
                            <Paper className={classes.paper}>
                                <Grid className={classes.videoResponsive}>
                                    <CardMedia component="iframe"
                                        className={classes.iframe}
                                        width="853"
                                        height="480"
                                        src={video.url}
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share  "
                                        allowFullScreen
                                        title={video.title}
                                    />
                                </Grid>
                                <Grid>
                                    <Typography variant='h6' className={classes.titleGrid}>{video.title}</Typography>
                                </Grid>
                            </Paper>
                        </Grid>
                    ))}
                    <Grid item xs={12}>
                        <Typography variant='h6' className={classes.title}>
                            HAVE QUESTIONS?
                        </Typography>
                        <Typography className={classes.heading}>
                            CHECK OUT OUR FAQS
                        </Typography>
                        <Typography component='p' className={classes.faqsPara}>
                            We have answered several common questions about EsquireTek below. If there is anything unanswered here, please contact us.
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <FaqsList columns={helpFAQs} />
                    </Grid>
                </Grid>
            </Grid>
        )
    }

    return HelpFAQsPage;

}

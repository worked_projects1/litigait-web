import React, { useState } from 'react';
import Styles from './styles';
import { Grid, Typography, List, ListItem, ListItemText, ListItemIcon, Collapse, Link, Button } from '@material-ui/core';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import RemoveOutlinedIcon from '@material-ui/icons/RemoveOutlined';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

function HelpFAQs({ columns }) {

    const [open, setOpen] = useState([]);
    const classes = Styles();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));

    const handleClick = (id, index) => {
        const isOpen = open.find((item) => item.schemaId === id && item.position === index);
        const isOpenShema = open.find((item) => item.schemaId !== id);
        const isOpenIndex = open.find((item) => item.schemaId === id);
        if (isOpen) {
            const updatedOpen = open.filter((item) => !(item.position === index && item.schemaId === id));
            setOpen(updatedOpen);
        } else if (isOpenShema) {
            if (isOpenIndex) {
                const updatedOpen = open.filter((item) => item.schemaId !== id);
                setOpen([...updatedOpen, {
                    schemaId: id,
                    position: index
                },]);
            } else {
                setOpen((prevState) => {
                    return [
                        ...prevState,
                        {
                            schemaId: id,
                            position: index
                        },
                    ]
                })
            }
        } else {
            setOpen([
                {
                    schemaId: id,
                    position: index
                },
            ]);
        }
    };

    return (
        <Grid>
            {columns?.map((_, key) => {
                return <Grid key={key}>
                    <Typography variant='h5' className={classes.faqTitle}>
                        {_.heading}
                    </Typography>
                    {(_?.list).map((data, index) => {
                        const show = open?.find((item) => {
                            return (item.position === index) && (item.schemaId === _.schemaId) || false;
                        })
                        return <Grid key={index} className={index !== 0 ? `${classes.gridBorder} ${classes.grid}` : `${classes.grid}`}>
                            <List style={{ padding: 0 }}>
                                <ListItem onClick={() => handleClick(_.schemaId, index)} className={show && classes.listHeading} style={{ cursor: 'pointer' }}>
                                    <ListItemIcon>
                                        {!show ? <AddOutlinedIcon className={classes.plusIcon} /> : <RemoveOutlinedIcon className={classes.minusIcon} />}
                                    </ListItemIcon>
                                    <Link className={!show ? classes.linkPlus : classes.linkMinus}>
                                        <ListItemText primary={data.title}
                                            className={show && classes.subTitle}
                                            classes={{ primary: classes.listItemText }}
                                        />
                                    </Link>
                                </ListItem>
                                <Collapse in={show} timeout={{ enter: 600, exit: 600 }} unmountOnExit>
                                    <List>
                                        {data.img && <ListItem>
                                            <img src={data.img} width="100%" height="100%" />
                                        </ListItem>}
                                        <ListItem>
                                            <Typography component='p' className={classes.
                                            paragraph}>
                                                <span dangerouslySetInnerHTML={{ __html: data.paragraph }} />
                                            </Typography>
                                        </ListItem >
                                        {data?.listItems?.length > 0 ? <ListItem>
                                            <ol>
                                                {data?.listItems?.map(el => {
                                                    return (
                                                        <li className={classes.listItems}>
                                                            <span dangerouslySetInnerHTML={{ __html: el }} />
                                                        </li>
                                                    )
                                                })}
                                            </ol>
                                        </ListItem> : null}
                                        {data.paragraph1 && <ListItem>
                                            <Typography component='p' className={classes.
                                            paragraph}>
                                                <span dangerouslySetInnerHTML={{ __html: data.paragraph1 }} />
                                            </Typography>
                                        </ListItem>}
                                    </List>
                                </Collapse>
                            </List>
                        </Grid>
                    })}
                </Grid>
            })}
        </Grid>
    )
}

export default HelpFAQs;
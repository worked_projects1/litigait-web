import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    faqTitle: {
        marginTop: '25px',
        marginBottom: '20px',
        lineHeight: 1,
        fontFamily: `"Martel Sans", Sans-serif`,
        fontWeight: 'bold',
        fontSize: '25px',
        color: '#404040',        
        [theme.breakpoints.down('md')]: {
            fontSize: '23px',
        }
    },
    faqTitleMd: {
        fontSize: '23px',
        fontFamily: `"Martel Sans", Sans-serif`,
    },
    grid: {
        border: '1px solid #d4d4d4',
    },
    gridBorder: {
        borderTop: 'none'
    },
    plusIcon: {
        color: '#5C5C5C',
        fontSize: '25px'
    },
    minusIcon: {
        color: '#2ca01c',
        fontSize: '25px'
    },
    subTitle: {
        fontFamily: `"Martel Sans"`,
        fontSize: '16px',
        fontWeight: 400,
        textTransform: 'uppercase',
        fontStyle: 'normal',
        letterSpacing: '0px'
    },
    linkPlus: {
        color: '#5C5C5C',
    },
    listItems: {
        fontSize: '16px',
        fontFamily: `Martel Sans`,
        color: '#4a4a4a'
    },
    paragraph: {
        fontSize: '16px',
        fontFamily: `Martel Sans`,
        fontWeight: 200,
        lineHeight: '1.65',
        color: '#4a4a4a'
    },
    listHeading: {
        borderBottom: '1px solid #d4d4d4',
        cursor: 'pointer',
        fontFamily: `"Martel Sans", Sans-serif !important`,
    },
    listItemText: {
        fontSize: '15px',
        marginLeft: '-20px',
        fontFamily: `"Martel Sans", Sans-serif !important`,
    },
    linkMinus: {
        color: '#2ca01c'
    }
}));

export default useStyles;
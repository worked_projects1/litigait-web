

/**
 *
 * Vew Support Request Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import QuestionBox from './components/QuestionBox';
import ButtonSpinner from 'components/ButtonSpinner';
import messages from './messages';
import { FormattedMessage } from 'react-intl';
import AlertDialog from 'components/AlertDialog';
import ModalForm from 'components/ModalRecordForm';
import { getOffset, useDidUpdate, autoIncrementQuestions } from 'utils/tools';
import { AsYouType } from 'libphonenumber-js';
import { caseTitle } from './utils';
import { selectForm } from 'blocks/session/selectors';

const Default_Question = [Object.assign({}, { question_id: 1, question_number: '1', question_number_text: '1', question_text: '', question_options: '' })];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors
 * @param {string} pageType
 */
export default function (name, path, columns, actions, selectors, pageType) {

    const { questionColumns, bulkQuestionsColumns, clientCaseInfoFields, deleteSupportRequestColumn } = columns;
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectUser, selectSessionExpand, selectRecordsMetaData } = selectors;

    /**
     * @param {object} data 
     * @param {function} dispatch 
     */
    function handleForm(data, dispatch) {
        if (pageType == 'propound') {
            dispatch(actions.savePropoundQuestions(data));
        } else if (pageType == 'extraction') {
            dispatch(actions.saveExtractionQuestions(data));
        } else {
            const submitData = Object.assign({}, {
                practice_id: data.practice_id,
                case_id: data.case_id,
                client_id: data.client_id,
                file_name: data.type,
                questions: data.questions,
                document_type: data.document_type,
                content_type: 'application/pdf',
                from: 'questions_form',
                form: 'questionsForm',
                action_by: data.role,
                legalforms_id: data.legalforms_id,
                filename: data.filename
            });
            dispatch(actions.saveQuestionsForm(submitData));
        }
    }

    /**
     * @param {object} props 
     */
    function ViewSupportRequestPage(props) {

        const { location, record = {}, dispatch, match, success, error, loading, progress, user = {}, expand, metaData, history, forms } = props;
        const { role } = user;
        const { practice_name, client_name, case_number, case_title, document_type, state } = record;
        const clientCaseInfo = (pageType && pageType == 'extraction' && record && record.legalform_filename) && !record.is_case_available && !record.is_client_available ? true : false;
        const classes = Styles();

        const [loadForm, setLoadForm] = useState(true);
        const [loadQuestions, setLoadQuestions] = useState(false);
        const [editable, setEditable] = useState(false);
        const [showForm, setShowForm] = useState(false);
        const [showNote, setShowNote] = useState(true);
        const [width, setWidth] = useState(window.innerWidth);
        const [questions, setQuestions] = useState(location && location.state && ((location.state.questions && location.state.questions.length > 0 && location.state.questions) || Default_Question).map(er => Object.assign({}, { question_id: er.question_id, question_number: er.question_number, question_number_text: er.question_number_text, question_text: er.question_text, question_options: er.question_options })) || []);
        const [margin, setMargin] = useState(0);
        const [position, setPosition] = useState(false);
        const [clientCaseModal, setClientCaseModal] = useState(clientCaseInfo);

        const clientCaseInfoForm = forms && forms[`clientCaseInfoColumns_${match.params.id}`] && forms[`clientCaseInfoColumns_${match.params.id}`].values || {};
        const case_titles = clientCaseInfoForm && record && caseTitle(clientCaseInfoForm) || '';
        const clientCaseInfoColumns = clientCaseInfoFields && typeof clientCaseInfoFields === 'function' && clientCaseInfoFields(clientCaseInfoForm).columns;

        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const navbarWidth = expand ? 240 : 60;

        useEffect(() => {
            let mounted = true;
            window.addEventListener('resize', () => {
                setMargin(getOffset('esquiretek-detail-header'));
                setPosition(getOffset('esquiretek-header') - 2);
                setWidth(window.innerWidth);
            });
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            setLoadForm(true);
            setTimeout(() => setLoadForm(false), 200);
            return () => mounted = false;
        }, [match.params.id]);

        useDidUpdate(() => {
            if (clientCaseInfo) {
                setClientCaseModal(clientCaseInfo);
            }
        }, [record, actions, dispatch, match])

        const handleChange = (data, type) => {
            let index = 0;
            const changedVal = (questions || []).reduce((a, el) => {
                if (el.question_id.toString() === data.question_id.toString()) {
                    if (type === 'input') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: data.question_number.toString(), question_number_text: data.question_number.toString(), question_text: data.question_text }));
                    } else if (type == 'add') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index }));
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: index.toString(), question_number_text: index.toString(), question_options: "", question_text: "" }));
                    } else if (type != 'delete') {
                        index++;
                        a.push(Object.assign({}, { ...el }, { question_id: index, question_number: index.toString(), question_number_text: index.toString() }));
                    }
                } else {
                    index++;
                    a.push(Object.assign({}, { ...el }, { question_id: index }));
                }

                return a;
            }, []);

            if (type === 'delete') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal);
                setLoadQuestions(true);
                setTimeout(() => {
                    setLoadQuestions(false);
                    setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
                    setShowNote(finalVal && finalVal.length > 0 ? false : true);
                }, 500);
            } else if(type == 'add') {
                let finalVal = changedVal && autoIncrementQuestions(changedVal);

                setQuestions(finalVal && finalVal.length > 0 ? finalVal : Default_Question);
                setShowNote(finalVal && finalVal.length > 0 ? false : true);
            } else {
                setQuestions(changedVal && changedVal.length > 0 ? changedVal : Default_Question);
                setShowNote(changedVal && changedVal.length > 0 ? false : true);
            }

        }

        const handleClientCaseInfo = (data, dispatch, {form}) => {
            let submitRecord = Object.assign({}, { ...data });

            if (submitRecord.phone) {
                const phoneType = new AsYouType('US');
                phoneType.input(submitRecord.phone);
                const phoneVal = phoneType.getNumber().nationalNumber;
                submitRecord.phone = phoneVal || submitRecord.phone;
            }

            submitRecord = Object.assign({}, {
                client_info: {
                    first_name: submitRecord.first_name,
                    middle_name: submitRecord.middle_name,
                    last_name: submitRecord.last_name,
                    phone: submitRecord.phone
                },
                case_info: {
                    case_plaintiff_name: submitRecord.case_plaintiff_name,
                    case_defendant_name: submitRecord.case_defendant_name,
                    case_title: submitRecord.case_title,
                    case_number: submitRecord.case_number,
                    county: submitRecord.county,
                    state: submitRecord.state
                },
                practice_id: record.practice_id,
                document_type: submitRecord.document_type,
                set_number: submitRecord.set_number,
                legalform_filename: record.legalform_filename,
                extraction_id: record.id
            })
            dispatch(actions.saveClientCaseInfo(submitRecord, setClientCaseModal, actions.loadRecord, form));
        }

        const handleClientCaseClose = () => {
            setClientCaseModal(false);
            history.push({ pathname: `/supportCategories/rest/document-extraction-support`, state: location.state });
        }        

        const confirmMessage = pageType == "respond" ? "This case will be closed.. Do you want to continue?" : "This request will be closed. Do you want to continue?"

        const handleDeleteForm = (formData) => {
            dispatch(actions.deleteSupportRequest(Object.assign({}, record, { reason: formData?.deleteReason }), pageType));
        }

        if (loading || loadForm) {
            return <Spinner className={classes.spinner} />
        }


        return (<Grid container className={classes.supportRequestPage}>
            <Grid container>
                <Grid item xs={12} className={classes.header} id="esquiretek-detail-header" style={!md ? { left: '0', width: `${width}px`, top: `${position || getOffset('esquiretek-header')}px` } : { width: `${width - navbarWidth}px`, top: `${position || getOffset('esquiretek-header')}px`, left: navbarWidth }}>
                    <Grid container>
                        <Grid item xs={12} style={{ paddingBottom: '6px' }}>
                            <Grid container alignItems="center">
                                <Grid item xs={6}>
                                    <Link to={Object.assign({}, { pathname: path, state: Object.assign({}, { ...location.state }) })} className={classes.link}>
                                        <FormattedMessage {...messages.back} />
                                    </Link>
                                </Grid>
                                {showForm ? <Grid item xs={6}>
                                    <Grid container direction="row" justify="flex-end">
                                        {editable && <AlertDialog
                                            description={confirmMessage}
                                            onConfirm={() => handleForm(Object.assign({}, { ...record }, { document_type: (location && location.state && location.state.document_type) || record.document_type, questions, role }), dispatch)}
                                            onConfirmPopUpClose={true}
                                            btnLabel1='Yes'
                                            btnLabel2='No' >
                                            {(open) => <Typography variant="subtitle2" className={classes.link} onClick={open} style={{ marginRight: '42px' }}>
                                                {progress && <ButtonSpinner color="#47AC39" /> || <FormattedMessage {...messages.save} />}
                                            </Typography>}
                                        </AlertDialog> || null}
                                        <Grid>
                                            <Typography variant="subtitle2" className={classes.link} onClick={() => setEditable(!editable)}>
                                                {editable && <FormattedMessage {...messages.cancel} /> || <FormattedMessage {...messages.edit} />}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                    :
                                    <Grid item xs={6}>
                                        <Grid container direction="row" justify="flex-end">
                                            <Grid>
                                                <ModalForm
                                                    fields={deleteSupportRequestColumn}
                                                    form={`deleteSupportRequestForm`}
                                                    btnLabel="Delete"
                                                    onSubmitClose
                                                    onSubmit={handleDeleteForm}>
                                                    {(open) => <Typography variant="subtitle2" className={classes.link} onClick={open}>
                                                        {progress && <ButtonSpinner color="#47AC39" /> || <FormattedMessage {...messages.delete} />}
                                                    </Typography>}
                                                </ModalForm>
                                            </Grid>
                                        </Grid>
                                    </Grid>}
                            </Grid>
                        </Grid>
                        <Grid item xs={12} style={{ paddingBottom: '2px' }}>
                            {pageType == 'respond' ? <Typography variant="subtitle1">
                                <b>{client_name || ''}</b> {practice_name ? `(Practice: ${practice_name || ''},` : ''} {case_number ? `Case: ${case_number || ''},` : ''}  {case_title ? `Title: ${case_title || ''},` : ''} {document_type ? `Document Type: ${document_type || ''})` : ''}
                            </Typography> : pageType == 'propound' ? <Typography variant="subtitle1">
                                <b>{practice_name || ''}</b> {state ? `(State: ${state || ''},` : ''}  {document_type ? `Document Type: ${document_type || ''})` : ''}
                            </Typography> : <Typography variant="subtitle1"><b>{practice_name || ''}</b> {(state && document_type) && `(State: ${state || ''}, Document Type: ${document_type || ''})`}</Typography>}
                        </Grid>
                    </Grid>
                </Grid>
                {record && showForm && Object.keys(record).length > 0 ?
                    <Grid item xs={12} style={{ marginTop: !sm ? `${margin || (getOffset('esquiretek-detail-header') + 40) || 0}px` : '132px' }}>
                        <Grid container spacing={5}>
                            {loadQuestions ?
                                <Spinner className={classes.spinner} style={{ marginTop: '62px' }} /> : showNote && !editable ?
                                    <Grid container direction="column" alignItems="center">
                                        <Typography variant="subtitle1">
                                            <FormattedMessage {...messages.noQuestions} />
                                        </Typography>
                                        <Typography variant="subtitle2" className={classes.note}>
                                            <FormattedMessage {...messages.note} />
                                        </Typography>
                                    </Grid> : (questions || []).map((r, i) => (
                                        <Grid key={i} item xs={12}>
                                            {editable && <QuestionBox
                                                name={name}
                                                path={path}
                                                fields={questionColumns}
                                                record={r}
                                                handleChange={handleChange}
                                            /> || <Grid container direction="row" style={{ fontSize: '16px' }}>
                                                    <Grid item xs={12}><b>{`${r.question_number}.`}</b> {r.question_text || 'No Text'}</Grid>
                                                </Grid>}
                                        </Grid>))}
                        </Grid>
                    </Grid> : <Grid item xs={12} className={classes.manual}>
                        <Grid container alignItems="center" direction="column">
                            <Grid item xs={12}>
                                <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={() => setShowForm(true)}>
                                    <FormattedMessage {...messages.manual} />
                                </Button>
                            </Grid>
                            {(user.email === 'm.shahidulkareem@rifluxyss.com' || user.email === 'siva@ospitek.com' || user.email === 'siva@miotiv.com') ? <React.Fragment>
                                <Grid item xs={12} className={classes.btwn}>
                                    <Typography variant="subtitle2" className={classes.note}>
                                        <FormattedMessage {...messages.or} />
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <ModalForm
                                        title="Update Questions"
                                        initialValues={Object.assign({}, record, { document_type: (location && location.state && location.state.document_type) || record.document_type, role })}
                                        fields={bulkQuestionsColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                        form={`Bulk_Questiion_Form_${name}_${match.params.id}`}
                                        onSubmit={handleForm.bind(this)}
                                        disableContainer
                                        progress={progress}
                                        btnLabel="Update">
                                        {(open) => <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={open}>
                                            <FormattedMessage {...messages.manualJSON} />
                                        </Button>}
                                    </ModalForm>
                                </Grid>
                            </React.Fragment> : null}
                        </Grid>

                    </Grid>}
            </Grid>
            {clientCaseInfo && <Grid>
                <ModalForm
                    initialValues={Object.assign({}, record, { case_title: case_titles }) || {}}
                    title="Client & Case Info"
                    fields={clientCaseInfoColumns.filter(_ => _.editRecord && !_.disableRecord)}
                    form={`clientCaseInfoColumns_${match.params.id}`}
                    show={clientCaseModal}
                    metaData={metaData}
                    keepDirtyOnReinitialize={true}
                    onClose={handleClientCaseClose}
                    onSubmit={handleClientCaseInfo.bind(this)}
                    disableContainer
                    enableScroll={classes.bodyScroll}
                    progress={progress}
                    btnLabel="Save" />
            </Grid>}
            <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid >);
    }

    ViewSupportRequestPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object,
        metaData: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress(),
        user: selectUser(),
        expand: selectSessionExpand(),
        metaData: selectRecordsMetaData(),
        forms: selectForm(),
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewSupportRequestPage);

}

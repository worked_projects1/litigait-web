/*
 * ViewSupportRequestPage Messages
 *
 * This contains all the text for the View Support Request Page container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewSupportRequestPage';

export default defineMessages({
    edit: {
        id: `${scope}.edit`,
        defaultMessage: 'Edit',
    },
    back: {
        id: `${scope}.back`,
        defaultMessage: 'Back',
    },
    save: {
        id: `${scope}.save`,
        defaultMessage: 'Save',
    },
    cancel: {
        id: `${scope}.cancel`,
        defaultMessage: 'Cancel',
    },
    noQuestions: {
        id: `${scope}.noQuestions`,
        defaultMessage: 'No Questions Found.',
    },
    note: {
        id: `${scope}.note`,
        defaultMessage: 'Please click edit button to add questions here.',
    },
    manual: {
        id: `${scope}.manual`,
        defaultMessage: 'Manually Update Questions',
    },
    or: {
        id: `${scope}.or`,
        defaultMessage: 'OR',
    },
    manualJSON: {
        id: `${scope}.manualJSON`,
        defaultMessage: 'Manually Update Bulk JSON Questions',
    },
    delete: {
        id: `${scope}.manualJSON`,
        defaultMessage: 'Delete'
    }
});

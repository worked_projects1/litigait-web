import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    header: {
        position: 'fixed',
        height: 'auto',
        zIndex: '999',
        background: '#fff',
        top: '48px',
        left: '60px',
        padding: '25px',
        paddingTop:'0px',
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
        boxShadow: '0px 3px 4px -1px lightgrey'
    },
    button: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginLeft: '20px'
    },
    notes: {
        fontSize: '13px',
        paddingTop: '25px'
    },
    link: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Bold',
        backgroundColor: '#fff !important',
        boxShadow: 'none',
        color: '#47AC39',
        fontSize: '18px',
        paddingTop: '4px',
        paddingBottom: '10px',
        cursor: 'pointer'
    },
    note: {
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    button2: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular'
    },
    manual: { 
        marginTop: '132px', 
        textAlign: 'center' 
    },
    btwn: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    bodyScroll: {
        maxHeight: '430px',
        overflowY: 'scroll',
        overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 12,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#2ca01c",
            borderRadius: '30px'
        }
    }
}));

export default useStyles;
/**
 * 
 * Utils 
 * 
 */


/**
 * @param {object} formRecord 
 * @returns 
 */
export function caseTitle(formRecord) {

    const plaintiff_name = formRecord && formRecord.case_plaintiff_name || false;
    const defendant_name = formRecord && formRecord.case_defendant_name || false;

    const case_title = plaintiff_name && defendant_name ? `${plaintiff_name} v. ${defendant_name}` : plaintiff_name ? plaintiff_name : defendant_name ? defendant_name : '';

    return case_title;
}
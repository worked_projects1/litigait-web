import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  table: {
    boxShadow: '0px 0px 8px 1px lightgrey',
    borderRadius: '8px',
    paddingTop: '8px',
    paddingBottom: '8px',
    marginTop: '150px',
    overflow: 'auto',
    [theme.breakpoints.down('xs')]: {
      marginTop: '320px'
    },
    [theme.breakpoints.between('sm', 'md')]: {
      marginTop: '200px'
    }
  },
  button: {
    fontWeight: 'bold',
    borderRadius: '28px',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular',
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px'
    }
  },
  back: {
    display: 'flex',
    alignContent: 'center',
    marginTop: '4px'
  },
  link: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    fontFamily: 'Avenir-Bold',
    backgroundColor: '#fff !important',
    boxShadow: 'none',
    color: '#47AC39',
    fontSize: '18px',
    paddingTop: '4px',
    paddingBottom: '10px'
  },
  header: {
    width: '82%',
    position: 'fixed',
    height: 'auto',
    zIndex: '999',
    background: '#fff',
    top: '50px',
    left: '60px',
    paddingRight: '25px',
    paddingLeft: '25px',
    paddingBottom: '25px',
    boxShadow: '0px 3px 4px -1px lightgrey'
  },
  btnView: {
    textAlign: 'center'
  },
  downloadBtn: {
    fontWeight: 'bold',
    borderRadius: '28px',
    textTransform: 'none',
    minWidth: '130px',
    fontFamily: 'Avenir-Regular',
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px',
      marginBottom: 0
    },
    [theme.breakpoints.between('md', 'xl')]: {
      marginRight: '10px'
    },
    [theme.breakpoints.between('sm','md')]: {
      marginBottom: '10px'
    },
    [theme.breakpoints.down('sm')]: {
      marginRight: '10px'
    }
  },
  iconBack: {
    width: '30px',
    height: '30px'
  },
  btnContainerDiv: {
    justifyContent: 'end',
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center',
    },
  },
}));

export default useStyles;
/**
 * 
 *  View Summary Documents
 * 
 */

import React, { useState } from 'react';
import Styles from './styles';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import SVG from 'react-inlinesvg';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import AlertDialog from 'components/AlertDialog';
import { viewPDF } from 'utils/tools';
import ModalForm from 'components/ModalRecordForm';
import ButtonSpinner from 'components/ButtonSpinner';

function ViewSummaryDocument(props) {
    const { children, show, onClose, heading, fields, record, showDelete, showUpload, handleSubmit, uploadSummaryColumns, handleDeleteDocuments, progress } = props
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [spinnerType, setSpinnerType] = useState(false);
    const [openChangeDocs, setOpenChangeDocs] = useState(false);
    const [editState, setEditState] = useState(false);
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));

    const handleClose = () => {
        setModalOpen(false);
        if (onClose) {
            onClose();
        }
        setEditState(false);
    }
    const handleDelete = (field) => {
        setSpinnerType(field.public_url);
        let value = field.public_url;
        let filteredValue = fields.filter((field, index) => {
            if (field.public_url != value) {
                return field
            }
        })
        handleDeleteDocuments(filteredValue, '')
    }

    return <Grid>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show}>
                <Paper className={classes.paper} style={!md ? { maxWidth: '80%' } : { maxWidth: '50%' }}>
                    <Grid container className={classes.header}>
                        {heading ? <Grid className={classes.gridTitle}>
                            <Typography variant="h6" gutterBottom component="span" className={classes.title}><b>{heading || ''}</b></Typography>
                        </Grid> : null}
                        <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                    </Grid>
                    <Grid className={classes.gridList}>
                        {fields && fields.length > 0 && fields.map((field, index) => {
                            return <Grid key={index} container className={classes.gridContainer}>
                                <Grid item xs={10} style={{ paddingRight: '4px', marginTop: '10px', marginBottom: '10px' }}>
                                    <Typography variant="subtitle1" >{field.file_name}</Typography>
                                </Grid>
                                <Grid item xs={2} className={classes.gridDelete}>
                                    <img src={require('images/icons/pdf2.png')} className={classes.attachPdf} onClick={() => viewPDF(field.public_url)} />
                                    {showDelete && editState ? <AlertDialog
                                        description={`Do you want to delete this document?`}
                                        onConfirm={() => handleDelete(field)}
                                        onConfirmPopUpClose={true}
                                        btnLabel1='Delete'
                                        btnLabel2='Cancel' >
                                        {(open) =>
                                            progress && spinnerType === field.public_url ? <ButtonSpinner color="#47AC39" className="terms" style={{ width: 'auto', minWidth: '0px', marginLeft: '8px' }} /> :
                                                <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} disabled onClick={open} />
                                        }
                                    </AlertDialog> : null}
                                </Grid>
                                <Grid item xs={12} ><hr className={classes.hr} /></Grid>
                            </Grid>
                        }) || <Grid>No Medical Document Found.</Grid>}
                    </Grid>
                    <Grid container justify="flex-end" direction="row">
                        {showUpload && editState ?
                            <Button type="button" disabled={fields.length >= 4 ? true : false} variant="contained" color="primary" className={classes.button} onClick={() => {
                                setOpenChangeDocs(true);
                                setModalOpen(false);
                            }}>
                                {'Upload New'}
                            </Button> :
                            <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => {
                                setEditState(true);
                            }}>
                                {'Edit'}
                            </Button>
                        }
                    </Grid>
                </Paper>
            </Fade>
        </Modal>
        {showUpload && <ModalForm
            fields={uploadSummaryColumns.filter(_ => _.editRecord).map(item => {
                item.max = record && record.summery_documents && Object.values(record.summery_documents).length > 0 ? (4 - Object.keys(record.summery_documents).length) : 4;
                return item
            })}
            form={`forms_${record.id}`}
            btnLabel="Next"
            show={openChangeDocs}
            onClose={() => setOpenChangeDocs(false)}
            className={classes.form}
            onSubmitClose={true}
            disableContainer={true}
            confirmPopUpClose
            onSubmit={handleSubmit} />}
    </Grid>
}
ViewSummaryDocument.propTypes = {
    children: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default ViewSummaryDocument;
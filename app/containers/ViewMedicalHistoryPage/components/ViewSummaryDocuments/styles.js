

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '25px',
        minWidth: '40%',
        maxWidth: '50%',
        outline: 'none'
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    gridTitle: {
        maxWidth: '600px',
        maxHeight: '400px',
        flex: 'auto',        
    },
    closeIcon: {
        cursor: 'pointer'
    },
    attachDelete: {
        width: '20px',
        height: '20px',
        cursor: 'pointer',
        marginLeft: '14px'
    },
    attachPdf: {
        width: '22px',
        height: 'auto',
        cursor: 'pointer',
        marginLeft: '10px'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none'
    },
    gridContainer: {
        width: 'auto',
        alignItems: 'center',
    },
    gridDelete: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    gridList: {
        padding: '14px 0px',
        height: 'auto',
        maxHeight: '450px'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize'
    },
    title: {
        fontFamily: 'Avenir-Bold',
        fontSize: '22px',
    },
}));


export default useStyles;
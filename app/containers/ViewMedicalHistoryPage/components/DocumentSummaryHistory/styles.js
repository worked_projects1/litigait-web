import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '25px',
        minWidth: '40%',
        maxWidth: '50%',
        outline: 'none'
    },
    gridTitle: {
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
    renderMedicalHistory: {
        maxHeight: '400px',
        overflow: 'auto'
    },
    attachDelete: {
        width: '20px',
        height: '20px',
        cursor: 'pointer',
        marginLeft: '14px'
    },
    deleteDiv: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'center'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    footer: {
        paddingTop: '15px',
        borderTop: '1px solid lightgray'
    },
    medicalRow: {
        marginTop: '20px',
        marginBottom: '20px'
    }
}));

export default useStyles;
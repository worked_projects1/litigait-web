/**
 * 
 * 
 * Document Summary History
 * 
 */

 import React from 'react';
 import { reduxForm, FieldArray } from 'redux-form';
 import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
 import useMediaQuery from '@material-ui/core/useMediaQuery';
 import { useTheme } from '@material-ui/core/styles';
 import SVG from 'react-inlinesvg';
 import ButtonSpinner from 'components/ButtonSpinner';
 import Styles from './styles';
 import AlertDialog from 'components/AlertDialog';
 
 const renderMedicalHistory = ({ fields, meta: { error, submitFailed } }) => {
     const classes = Styles();
     return <Grid container className={classes.renderMedicalHistory}>
         {fields ? fields.map((field, index) => {
             const record = fields.get(index);
             return (<Grid key={index} item xs={12} className={classes.medicalRow}>
                 <Grid container direction="row">
                     <Grid item xs={10}>
                         <Grid container direction="column">
                             <Grid item xs={12}>
                                 <Typography variant="subtitle1" >{record.file_name}</Typography>
                             </Grid>
                         </Grid>
                     </Grid>
                     <Grid item xs={2} className={classes.deleteDiv}>
                         <SVG src={require('images/icons/trash.svg')} className={classes.attachDelete} onClick={() => fields.remove(index)} />
                     </Grid>
                 </Grid>
             </Grid>)
         }) : <Typography variant="h6" gutterBottom component="span" style={{textAlign: 'center'}}><b>No Files Found.</b></Typography>}
     </Grid>
 }
 
 function DocumentSummaryHistory(props) {
     const { show, handleClose, heading, className, style, handleSubmit, pristine, submitting, initialUpload } = props;
     const classes = Styles();
     const theme = useTheme();
     const md = useMediaQuery(theme.breakpoints.up('md'));
 
     return (<Grid container className={className} style={style}>
         <Modal
             aria-labelledby="transition-modal-title"
             aria-describedby="transition-modal-description"
             open={show || false}
             className={classes.modal}                     
             closeAfterTransition             
             BackdropComponent={Backdrop}
             BackdropProps={{
                 timeout: 500,
             }}>
             <Fade in={show}>
                 <Paper className={classes.paper} style={!md ? { maxWidth: '80%' } : { maxWidth: '50%' }}>
                     <form onSubmit={handleSubmit} className={classes.form} noValidate >
                         {heading ? <Grid className={classes.gridTitle}>
                             <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
                         </Grid> : null}
                         <Grid className={classes.gridList}>
                             <FieldArray name="summaryDoc" component={renderMedicalHistory} />
                         </Grid>
                         <Grid container justify="flex-end" className={classes.footer}>                             
                            {initialUpload ? <AlertDialog
                                description={`Shall this request be marked as Complete?`}
                                onConfirm={() => handleSubmit()}
                                onConfirmPopUpClose={true}
                                btnLabel1='Yes'
                                btnLabel2='No' >
                                {(open) => <Button
                                    type="button"
                                    onClick={open}
                                    variant="contained"
                                    color="primary"
                                    className={classes.button}>
                                        {submitting && <ButtonSpinner /> || 'Upload'}
                                </Button>}
                            </AlertDialog> 
                            :
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                className={classes.button}>
                                {submitting && <ButtonSpinner /> || 'Upload'}
                            </Button>}
                             <Button
                                 type="button"
                                 variant="contained"
                                 onClick={handleClose}
                                 className={classes.button}>
                                 Cancel
                             </Button>
                         </Grid>
                     </form>
                 </Paper>
             </Fade>
         </Modal>
     </Grid>)
 }
 
 
 export default reduxForm({
     form: 'documentSummaryHistory',
     enableReinitialize: true
 })(DocumentSummaryHistory);
 
 
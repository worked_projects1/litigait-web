

/**
 * 
 * Vew Medical History Page
 * 
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ModalForm from 'components/ModalRecordForm';
import { createStructuredSelector } from 'reselect';
import Styles from './styles';
import PropTypes from 'prop-types';
import TableWrapper from 'components/TableWrapper';
import Snackbar from 'components/Snackbar';
import { Grid, Button, Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { Link } from 'react-router-dom';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import InlineRecordForm from 'components/InlineRecordForm';
import Spinner from 'components/Spinner';
import { getFormValues } from 'redux-form';
import ButtonSpinner from 'components/ButtonSpinner';
import ViewSummaryDocument from './components/ViewSummaryDocuments';
import { reset } from 'redux-form';
import DocumentSummaryHistory from './components/DocumentSummaryHistory';
import { getOffset, generateZIP } from 'utils/tools';
import Icons from 'components/Icons';
import AlertDialog from 'components/AlertDialog';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 */
export default function (name, path, columns, actions, selectors, filterColumns) {
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectProgress, selectRecordsMetaData, selectSessionExpand } = selectors;
    const { medicalDocumentsColumns = [], uploadSummaryColumns = [], statusFormColumns } = columns;

    /**
     * @param {function} col 
     * @param {object} record 
     */
    const resolveColumns = (col, record) => col && typeof col === 'function' ? col(record) : col;

    /**
     * @param {object} record 
     * @param {function} dispatch 
     * @param {object} param2 
     */
    function handleEdit(record, dispatch, { form }) {
        const submitRecord = Object.assign({}, { ...record });
        if (submitRecord.amount_charged) {
            submitRecord.amount_charged = submitRecord.amount_charged.replace("$", '');
        }

        if (form === `forms_${record.id}`) {
            submitRecord.status = 'Complete';
        }

        if (form === `inlineRecord_${record.id}` && (submitRecord.status === 'In Progress' || submitRecord.status === 'Assigned')) {
            submitRecord.summery_url = '';
        }

        if (form === `inlineRecord_${record.id}` && (submitRecord.status === 'In Progress' || submitRecord.status === 'Assigned')) {
            submitRecord.summery_documents = '';
        }

        delete submitRecord.medicalDoc_public_url;
        delete submitRecord.medicalHistory;

        dispatch(actions.updateRecord(submitRecord, form));
    }

    /**
     * @param {object} props 
     */
    function ViewMedicalHistoryPage(props) {

        const { record = {}, history, match, dispatch, location, error, success, metaData, loading, progress, formRecord = {}, expand } = props;

        const classes = Styles();
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const [width, setWidth] = useState(window.innerWidth);
        const navbarWidth = expand ? 240 : 60;
        const [summaryDocs, setSummaryDocs] = useState(false);
        const [position, setPosition] = useState(false);
        const [downloadSpinner, setDownloadSpinner] = useState(false);

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            window.addEventListener('resize', () => 
            {
                setPosition(getOffset('esquiretek-header') - 2);
                setWidth(window.innerWidth);
            });
            return () => mounted = false;
        }, [match.params.id]);

        const handleSubmit = (record, dispatch, { form }) => {
            let submitRecord = record.summery_url && record.summery_url.length > 0 && record.summery_url.map(file => Object.assign({}, {
                file_name: file.name,
                uploadFile: file,
            }));
            setSummaryDocs(submitRecord);
            dispatch(reset(form));
        }

        const handleMedicalSummary = (data, dispatch, { form }) => {
            const submitRecord = Object.assign({}, { ...record });
            if (submitRecord.amount_charged) {
                submitRecord.amount_charged = submitRecord.amount_charged.replace("$", '');
            }
            if (form === `documentSummary_${submitRecord.id}`) {
                submitRecord.status = 'Complete';
            }

            delete submitRecord.medicalDoc_public_url;
            delete submitRecord.medicalHistory;

            dispatch(actions.uploadSummary(Object.assign({}, submitRecord, { summery_url: data.summaryDoc }), form, actions.loadRecord, setSummaryDocs));
        }

        const handleDeleteDocuments = (data, form) => {
            if (record.amount_charged) {
                record.amount_charged = record.amount_charged.replace("$", '');
            }
            if (Object.keys(data).length === 0) {
                record.status = 'In Progress';
            }
            dispatch(actions.deleteSummary(Object.assign({}, record, { summery_documents: data }), form, actions.loadRecord));
        }

        const handleZip = () => {
            setDownloadSpinner('files');
            const filename = `${record.case_title}`;
            const files = record && record.medicalDoc_public_url || [];
            generateZIP(filename, files, setDownloadSpinner);
        }

        const handleSummaryDocument = () => {
            setDownloadSpinner('summary');
            const zipName = `${record.case_number}`;
            const summaryDocUrl = record?.summary_doc_url_ai || '';
            const billingDocUrl = record?.billing_summary_url_ai || '';
        
            const summary = summaryDocUrl && { file_name: "Medical Records Summary.docx", public_url: summaryDocUrl };
            const billing = billingDocUrl && { file_name: "Medical Billing Summary.xls", public_url: billingDocUrl };
        
            const summaryFiles = billing ? [billing, summary] : [summary];        
            generateZIP(zipName, summaryFiles, setDownloadSpinner);
        };
        

        const handleRetrySummary = () => {
            dispatch(actions.retryMedicalSummary(record, actions.loadRecord));
        }

        if (loading) {
            return <Spinner className={classes.spinner} />
        }


        return (
            <Grid container className={classes.body}>

                <Grid item xs={12} className={classes.header} style={!md ? { left: '0', width: `${width}px`, top: `${position || getOffset('esquiretek-header')}px` } : { width: `${width - navbarWidth}px`, top: `${position || getOffset('esquiretek-header')}px`, left: navbarWidth }}>
                    <Grid item xs={6} className={classes.back}>
                        <Link to={{ pathname: `/rest/medical-history-summery`, state: { ...location.state } }} className={classes.link}>
                        {md ? <FormattedMessage {...messages.back} /> :  <Icons type="Back" className={classes.iconBack} />}
                        </Link>
                    </Grid>
                    <Grid>
                        <Typography variant="subtitle1" >
                            <b>{record.practice_name || ''}</b>{record.case_number ? ` (Case: ${record.case_number || ''},` : ''} {record.case_title ? `Title: ${record.case_title || ''})` : ''}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container direction="row" alignItems="center" style={{ marginTop: '20px'}}>
                            <Grid item xs={12} sm={6} lg={5}>
                                    <InlineRecordForm
                                        initialValues={record || {}}
                                        form={`inlineRecord_${record.id}`}
                                        name={name}
                                        path={path}
                                        metaData={metaData}
                                        fields={resolveColumns(statusFormColumns, formRecord).filter(_ => _.editRecord)}
                                        onSubmit={handleEdit.bind(this)}
                                        btnStyle={!sm ? (classes.btnView) : null}
                                        locationState={location.state}
                                    />
                            </Grid>
                            <Grid item xs={12} sm={6} lg={7}>
                                <Grid container className={classes.btnContainerDiv} direction="row">
                                    {(record && record.ai_status && record.ai_status == "Failed") ?
                                        <AlertDialog
                                            description={'Do you want to retry summary document?'}
                                            onConfirm={handleRetrySummary}
                                            onConfirmPopUpClose={true}
                                            btnLabel1={'Yes'}
                                            btnLabel2={'No'}>
                                            {(open) => <Grid>
                                                <Button type="button" variant='contained' color="primary" className={classes.downloadBtn} onClick={open}>
                                                    {progress ? <ButtonSpinner /> : <FormattedMessage {...messages.retry} />}
                                                </Button>
                                            </Grid>}
                                        </AlertDialog> : (record && record.medicalDoc_public_url && record.medicalDoc_public_url.length > 0) && <Grid>
                                            {(!record.ai_scheduler && record.ai_status == 'Completed' && record.summary_doc_url_ai) ? <Button type="button" variant="contained" color="primary" className={classes.downloadBtn} onClick={handleSummaryDocument}>
                                                {(downloadSpinner == 'summary' && <ButtonSpinner /> || <FormattedMessage {...messages.downloadSummary} />)}
                                            </Button> : (record.ai_scheduler) ? <Button type="button" variant="contained" color="primary" className={classes.downloadBtn} disabled>
                                                <FormattedMessage {...messages.processingSummary} />
                                            </Button> : null}
                                        </Grid>}
                                    {record && record.medicalDoc_public_url && record.medicalDoc_public_url.length > 0 ? <Grid>
                                        <Button type="button" variant="contained" color="primary" className={classes.downloadBtn} onClick={() => handleZip()}>
                                            {downloadSpinner == 'files' ? <ButtonSpinner /> : <FormattedMessage {...messages.downloadFiles} />}
                                        </Button>
                                    </Grid> : null}
                                    {record.summery_documents && record.summery_documents != '' ?
                                        <ViewSummaryDocument
                                            fields={record && record.summery_documents}
                                            record={record}
                                            showDelete={true}
                                            showUpload={true}
                                            summaryDocs={summaryDocs}
                                            uploadSummaryColumns={uploadSummaryColumns}
                                            heading={'Summary Documents'}
                                            handleSubmit={handleSubmit}
                                            progress={progress}
                                            handleDeleteDocuments={handleDeleteDocuments}>
                                            {(open) => <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>
                                                {Object.keys(record.summery_documents).length > 1 ? <FormattedMessage {...messages.ChangeUploads} /> : <FormattedMessage {...messages.ChangeUpload} />}
                                            </Button>}
                                        </ViewSummaryDocument> :
                                        <Grid>
                                            <ModalForm
                                                initialValues={record || {}}
                                                fields={uploadSummaryColumns.filter(_ => _.editRecord).map(item => {
                                                    item.max = record && record.summery_documents && Object.values(record.summery_documents).length > 0 ? (4 - Object.keys(record.summery_documents).length) : 4;
                                                    return item
                                                })}
                                                form={`forms_${record.id}`}
                                                btnLabel="Next"
                                                confirmPopUpClose
                                                onSubmitClose={true}
                                                disableContainer={true}
                                                onSubmit={handleSubmit.bind(this)}>
                                                {(open) =>
                                                    <Button type="button" variant="contained" color="primary" className={classes.button} disabled={record && record.status === 'New Request'} onClick={open}>
                                                        <FormattedMessage {...messages.InitialUpload} />
                                                    </Button>}
                                            </ModalForm>
                                        </Grid>}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                {record && Object.keys(record).length > 0 && record.medicalHistory ?
                    <Grid item xs={12} className={classes.table}>
                        <TableWrapper
                            records={record && record.medicalHistory || []}
                            columns={medicalDocumentsColumns}
                            path={path}
                            name={name}
                            history={history}
                            locationState={location.state}
                        />
                    </Grid> : <Spinner style={{ marginTop: '180px' }} className={classes.spinner} />}
                <Grid>
                    <DocumentSummaryHistory
                        initialValues={summaryDocs && summaryDocs.length > 0 ? { summaryDoc: summaryDocs } : {}}
                        heading={'Summary Documents'}
                        show={summaryDocs ? true : false}
                        form={`documentSummary_${record.id}`}
                        onSubmit={handleMedicalSummary}
                        onChange={(e) => setSummaryDocs(e && e.summaryDoc)}
                        handleClose={() => setSummaryDocs(false)}
                        initialUpload={!record.summery_documents ? true : false}
                    />
                </Grid>
                {!progress ? <Snackbar show={error || success ? true : false} text={error || success} severity={error ? 'error' : 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} /> : null}
            </Grid >
        )


    }

    ViewMedicalHistoryPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        metaData: selectRecordsMetaData(),
        loading: selectPageLoader(),
        success: selectSuccess(),
        progress: selectProgress(),
        expand: selectSessionExpand(),
        formRecord: (state, props) => {
            const record = selectRecord()(state);
            return getFormValues(`inlineRecord_${record.id}`)(state)
        },
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewMedicalHistoryPage);

}
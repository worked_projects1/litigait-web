/*
 * ViewMedicalHistoryPage Messages
 *
 * This contains all the text for the ViewMedicalHistoryPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewMedicalHistoryPage';

export default defineMessages({
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    },
    InitialUpload: {
        id: `${scope}.InitialUpload`,
        defaultMessage: 'Upload Summary Document',
    },
    ChangeUpload: {
        id: `${scope}.ChangeUpload`,
        defaultMessage: 'Summary Document',
    },
    ChangeUploads: {
        id: `${scope}.ChangeUploads`,
        defaultMessage: 'Summary Documents',
    },
    back: {
        id: `${scope}.back`,
        defaultMessage: 'Back',
    },
    downloadFiles: {
        id: `${scope}.downloadFiles`,
        defaultMessage: 'Download Files',
    },
    retry: {
        id: `${scope}.retry`,
        defaultMessage: 'Retry Summary Document'
    },
    downloadSummary: {
        id: `${scope}.downloadSummary`,
        defaultMessage: 'Download Summary'
    },
    processingSummary: {
        id: `${scope}.processingSummary`,
        defaultMessage: 'Processing Summary'
    }
});

/*
 * ObjectionsPage Messages
 *
 * This contains all the text for the ObjectionsPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ObjectionsPage';

export default defineMessages({
  error: {
    id: `${scope}.error`,
    defaultMessage: 'There was an error loading the resource. Please try again.',
  }
});

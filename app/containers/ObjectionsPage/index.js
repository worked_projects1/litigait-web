/**
 * 
 * Objections Page
 * 
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Typography, FormControlLabel, Checkbox, Button } from '@material-ui/core';
import Styles from './styles';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Spinner from 'components/Spinner';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Snackbar from 'components/Snackbar';
import { Link } from 'react-router-dom';
import { ImplementationTable, RecordsData } from '../../utils/tools';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} create 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, filterColumns, create, view) {

    const {
        selectLoading,
        selectRecords,
        selectUser,
        selectRecordsMetaData,
        selectError,
        selectSuccess,
        selectUpdateError,
        sessionError,
        sessionSuccess,
        selectFilter,
    } = selectors;


    /**
     * @param {object} props 
     */
    function ObjectionsPage(props) {

        const classes = Styles();
        const { dispatch, records, children, location = {}, history, error, loading, metaData = {}, user = {}, success, updateError, sessionError, sessionSuccess, filter = {} } = props;
        const { pathname, state } = location;
        const activeChildren = path !== pathname;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const { practiceDetails = {} } = user;
        const TableWrapper = ImplementationTable.default;
        const [loader, setLoader] = useState(false);

        useEffect(() => {
            let mounted = true;
            dispatch(actions.loadRecords());
            return () => mounted = false;
        }, []);

        const handleChangeFilter = (e) => {
            setLoader(true);
            setTimeout(()=>setLoader(false), 1000);
            history.push({ pathname: path, state: state });
            dispatch(actions.UpdateStateFilter(Object.assign({}, filter, { [e.target.name]: e.target.value })))
        }

        let displayName = "objection";

        if (error) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        return <Grid container className={classes.ObjectionsPage}>
            <Grid item xs={12}>
                <Typography component="h1" style={{ marginBottom: '20px' }}>
                    {path.indexOf('customer-objections') > -1 ?
                        'Manage "Objection  List" for your practice using this page for one click entry of objections to client forms / interrogatories. An initial list of objections is provided to conveniently start  using this feature without any setup steps - however, you can add new objections or edit existing objections for your practice.'
                        : 'Manage default objection list of new practices using this page. New practices will get the objections in this list to conveniently start using this feature without any setup steps - however, practices can add new objections or change their list.'}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Grid container direction="row" alignItems="center">
                    {path.indexOf('customer-objections') > -1 && <Grid item sm={8} xs={12}>
                        <FormControlLabel
                            name="ObjectionList"
                            control={<Checkbox
                                style={{ color: "#2ca01c" }}
                                defaultChecked={practiceDetails && practiceDetails.objections || false}
                                onChange={(e) => dispatch(actions.updatePracticeObjections(Object.assign({}, user, { objections: e.target.checked, practiceDetails: Object.assign({}, practiceDetails, { objections: e.target.checked }) })))}
                            />}
                            label={<span><b>Include title with objection text</b></span>} />
                    </Grid>}
                </Grid>
            </Grid>
            <Grid container direction="row" alignItems="center">
                {filterColumns && filterColumns[name] && filterColumns[name].length > 0 ?
                    <Grid item sm={8} xs={12}>
                        {filterColumns[name].map((filterColumn, index) =>
                            <Grid key={index} className={classes.dropdownGrid}>
                                <select name={filterColumn.value} className={classes.filter} defaultValue={filter.state} onChange={handleChangeFilter}>
                                    {((filterColumn && Array.isArray(filterColumn.options) && filterColumn.options.length > 0 && filterColumn.options) || (filterColumn && typeof filterColumn.options === 'string' && metaData[filterColumn.options] && metaData[filterColumn.options].length > 0 && metaData[filterColumn.options]) || []).map((option, i) => <option key={i} disabled={option.disabled} value={option.value}>{option.label}</option>)}
                                </select>
                            </Grid>)}
                    </Grid> : null}
                <Grid item sm={4} xs={12}>
                    <Grid container justify={sm ? "flex-end" : "flex-start"} direction="row">
                        {create ? <Link to={{ pathname: `${path}/create`, state: { ...location.state } }}>
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.create}>
                                Add New {displayName}
                            </Button>
                        </Link> : null}
                    </Grid>
                </Grid>
            </Grid>
            <Grid container direction={!sm ? "column-reverse" : null} wrap={!sm ? "nowrap" : null}>
                <Grid item xs={12} md={activeChildren ? 6 : 12}>
                    <Grid item xs={12}>
                        <div className={`objections-table ${classes.table}`}>
                            <TableWrapper
                                records={loading || loader ? RecordsData : filter && records.filter(item => Object.keys(filter).filter(key => ((item[key] && item[key].indexOf(filter[key]) > -1) || filter[key] === '')).length === Object.keys(filter).length) || records}
                                columns={columns}
                                children={activeChildren}
                                path={path}
                                name={name}
                                history={history}
                                locationState={location.state}
                                view={true}
                                metaData={metaData}
                                sm={sm}
                                loading={loading || loader}
                            />
                        </div>
                    </Grid>
                </Grid>
                {activeChildren ?
                    <Grid item xs={12} md={6}>
                        <div className="children" style={{ marginTop: '40px' }}>
                            {children}
                        </div>
                    </Grid> : null}
            </Grid>
            <Snackbar show={updateError || success || (sessionError && sessionError.objections) || (sessionSuccess && sessionSuccess.objections) ? true : false} text={updateError || success || (sessionError && sessionError.objections) || (sessionSuccess && sessionSuccess.objections)} severity={updateError || (sessionError && sessionError.objections) ? 'error' : 'success'} handleClose={() => (sessionError && sessionError.objections) || (sessionSuccess && sessionSuccess.objections) ? dispatch(actions.clearCache()) : dispatch(actions.loadRecordsCacheHit())} />
        </Grid>
    }

    ObjectionsPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        filter: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        records: PropTypes.array,
        sessionError: PropTypes.object,
        sessionSuccess: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        updateError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        user: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        loading: selectLoading(),
        records: selectRecords(),
        user: selectUser(),
        metaData: selectRecordsMetaData(),
        error: selectError(),
        updateError: selectUpdateError(),
        success: selectSuccess(),
        sessionError: sessionError(),
        sessionSuccess: sessionSuccess(),
        filter: selectFilter()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ObjectionsPage);

}

/**
 * 
 * Overview Page 
 * 
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from './styles';

/**
 * @param {object} props 
 */
function OverviewPage(props) {
    
    const classes = useStyles();

    return (<Grid container>
        <Grid item xs={12} className={classes.base}>
            <Grid container>
                <Grid item md={7}>
                    <img src={require('images/home/banner.png')} style={{ width: '100%' }} />
                </Grid>
                <Grid item md={5} className={classes.section}>
                    <h2>THERE IS NOTHING MORE VALUABLE THAN TIME</h2>
                    <h1>SAVE HOURS WITH ESQUIRE<b>TEK</b></h1>
                    <p>Our team of professional litigators and software developers powers law firms. With EsquireTek, hours spent conducting interviews, preparing discovery and completing response shells and objections are reduced to seconds.</p>
                    <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        className={classes.button}>
                        Learn More
                        </Button>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12} className={classes.section2}>
            <h2>OVERVIEW</h2>
            <h1>MORE POWER AT THE CLICK OF A BUTTON</h1>
            <p>In a few simple steps, EsquireTek allows lawyers, paralegals and clients to easily build formatted PDF files essential for discovery in seconds. Just import a Word document and our
        groundbreaking software will format it and convert it to a PDF. You can customize a questionnaire and text a completion request link to your client to fill in at their leisure. EsquireTek gives firms the ability to create effective draft litigation documents and have them ready for review in a fraction of the time.</p>
            <Link
                to={{
                    pathname: '/',
                    state: {
                        form: 'requestDemo'
                    }
                }}
                className={classes.link}>
                <Button
                    type="button"
                    variant="contained"
                    color="primary"
                    className={classes.button}>
                    Request a Demo
                </Button>
            </Link>
        </Grid>
        <Grid item xs={12} className={classes.paddTop}>
            <Grid container>
                <Grid item md={6} className={classes.sectionImage} />
                <Grid item md={6} className={classes.section3}>
                    <Grid style={{ display: 'flex', marginTop: '25px' }}>
                        <img src={require('images/home/convenient.png')} />
                        <div>
                            <h2>CONVENIENT</h2>
                            <p>EsquireTek is the only tool that allows clients to answer discovery questions with simple text messaging.</p>
                        </div>
                    </Grid>
                    <Grid style={{ display: 'flex', marginTop: '25px' }}>
                        <img src={require('images/home/effcient.png')} />
                        <div>
                            <h2>EFFICIENT</h2>
                            <p>Paralegals and lawyers can spend valuable time focusing on their clients and the case while our advanced AI enters accurate data and design forms.</p>
                        </div>
                    </Grid>
                    <Grid style={{ display: 'flex', marginTop: '25px', marginBottom: '25px' }}>
                        <img src={require('images/home/affordable.png')} />
                        <div>
                            <h2>AFFORDABLE</h2>
                            <p>We have a pricing plan for any budget, including unlimited monthly subscriptions and pay-as-you go options starting at just $5!</p>
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12} className={classes.section4}>
            <h1>READY TO GET STARTED?</h1>
            <Link
                to={{
                    pathname: '/',
                    state: {
                        form: 'register'
                    }
                }}
                className={classes.link}>
                <Button
                    type="button"
                    variant="contained"
                    color="primary"
                    className={classes.signupBtn}>
                    Sign Up for Free
                    </Button>
            </Link>
        </Grid>
    </Grid>)
}

OverviewPage.propTypes = {
    locationState: PropTypes.object,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
    clearCache: PropTypes.func,
    errorMessage: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

export default OverviewPage;
/**
 *
 *   Setting Form
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';

import {
    Backdrop,
    Modal,
    Paper,
    Fade,
    Grid,
    Typography,
    Button,
} from '@material-ui/core';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function SettingsForm(props) {
    const { handleSubmit, submitting, fields, metaData, className, style, onOpen, onClose, error, pristine, invalid, footerStyle, destroy, notes, disableContainer, record, tabValue, click, show, menuName, SetShowSettingsForm, handleChangeForm, selectedFormData } = props;

    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const closeModal = () => {
        SetShowSettingsForm(false);
        setModalOpen(false);
        if (onClose) onClose();
    };

    useEffect(() => {
        return () => destroy();
    }, []);

    const handleConfirmClose = () => {
        handleSubmit();
        if (!invalid && tab.onSubmitClose) closeModal();
    };

    const tab = (menuName === 'changePassword') ? fields[0] : (menuName === 'enabledTwoFactorAuthentication') ? fields[1] : (menuName === 'switch_practice') ? fields[2] : fields[0];

    const description = record.twofactor_status === true ? tab.disableMessage : tab.confirmMessage && typeof tab.confirmMessage === 'function' && tab.confirmMessage(record, selectedFormData, metaData) || tab.confirmMessage;

    return (
        <Grid container={disableContainer ? false : true} className={className} style={style}>
            {/* {children && children(() => setModalOpen(!showModal))} */}
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={showModal || show || false}
                className={classes.modal}
                onClose={closeModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                onRendered={onOpen}
                BackdropProps={{
                    timeout: 500,
                }}>
                <Fade in={showModal || show || false}>
                    <Paper className={classes.paper} style={!sm ? { width: '90%' } : {}}>
                        <Grid container justify="flex-end">
                            <CloseIcon onClick={closeModal} className={classes.closeIcon} />
                        </Grid>

                        <form onSubmit={handleSubmit} className={classes.form} noValidate>
                            <Grid container className={classes.header} direction="column">
                                {tab.message ? (
                                    <Grid className={classes.messageGrid}>
                                        <Typography component="span" className={classes.message}>
                                            {tab.message || ''}
                                        </Typography>
                                    </Grid>
                                ) : null}
                            </Grid>
                            <Grid container className={classes.body}>
                                <Grid item xs={12}>
                                    <Grid container spacing={3}>
                                        {(tab.columns || []).map((field, index) => {
                                            const InputComponent = ImplementationFor[field.type];
                                            return (
                                                <Grid key={index} item xs={12}>
                                                    <Field
                                                        name={field.value}
                                                        label={field.label}
                                                        onChange={(value) => handleChangeForm(value)}
                                                        type="text"
                                                        metaData={metaData}
                                                        component={InputComponent}
                                                        required={field.required}
                                                        disabled={
                                                            field.disableOptons &&
                                                            field.disableOptons.edit
                                                        }
                                                        {...field}
                                                    />
                                                </Grid>
                                            );
                                        })}
                                    </Grid>
                                </Grid>
                                {error ? (<Grid item xs={12} className={classes.error}>
                                    {' '}
                                    <Error errorMessage={error} />
                                </Grid>) : null}
                            </Grid>
                            {notes ? (<Grid>
                                <Typography component="span" className={classes.note}>
                                    {notes || ''}
                                </Typography>
                            </Grid>) : null}
                            <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                                {tab.confirmButton ? (
                                    <AlertDialog
                                        description={description}
                                        onConfirm={handleConfirmClose}
                                        onConfirmPopUpClose={true}
                                        btnLabel1="Yes"
                                        btnLabel2="No">
                                        {open => (<Button
                                            type="button"
                                            variant="contained"
                                            onClick={open}
                                            disabled={tab.enableSubmitBtn && (pristine || invalid)}
                                            color="primary"
                                            className={classes.button}>
                                            {(submitting && <ButtonSpinner />) || tab.btnLabel || 'submit'}
                                        </Button>)}
                                    </AlertDialog>
                                ) : (<Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={tab.enableSubmitBtn && (pristine || invalid)}
                                    className={classes.button}
                                    onClick={!invalid && tab.onSubmitClose ? closeModal : null}>
                                    {(submitting && <ButtonSpinner />) || tab.btnLabel || 'submit'}
                                </Button>)}
                                <Button
                                    type="button"
                                    variant="contained"
                                    onClick={closeModal}
                                    className={classes.button}>
                                    Cancel
                                </Button>
                            </Grid>
                        </form>
                    </Paper>
                </Fade>
            </Modal>
        </Grid>
    );
}

SettingsForm.propTypes = {
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'settingsForm',
    validate,
    enableReinitialize: true,
    touchOnChange: true,
})(SettingsForm);

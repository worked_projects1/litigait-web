import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
   header: {
      marginTop: "10px"
   },
   modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
   },
   paper: {
      backgroundColor: theme.palette.background.paper,
      border: 'none',
      boxShadow: theme.shadows[5],
      paddingLeft: '25px',
      paddingRight: '25px',
      paddingBottom: '25px',
      minWidth: '36%',
      outline: 'none',
      width: '25%',
      maxHeight: "345px",
      '@global': {
         '.MuiTab-textColorPrimary.Mui-selected': {
            color: '#2ca01c'
         },
         '.MuiTabs-indicator': {
            backgroundColor: '#2ca01c'
         },
         '.MuiSwitch-colorPrimary.Mui-checked': {
            color: '#2ca01c'
         },
         '.MuiSwitch-colorPrimary.Mui-checked + .MuiSwitch-track': {
            backgroundColor: '#2ca01c'
         }
      }
   },
   body: {
      marginTop: '10px',
   },
   footer: {
      paddingTop: '15px',
      borderTop: '1px solid lightgray'
   },
   button: {
      fontWeight: 'bold',
      borderRadius: '28px',
      textTransform: 'none',
      fontFamily: 'Avenir-Regular',
      marginTop: '10px',
      outline: 'none',
      textTransform: 'capitalize',
      marginRight: '12px'
   },
   title: {
      fontFamily: 'Avenir-Bold',
      fontSize: '22px',
      marginTop: '10px'
   },
   message: {
      fontFamily: 'Avenir-Bold',
      fontSize: '18px',
      paddingTop: '18px',
   },
   messageRegular: {
      fontSize: '16px',
      paddingTop: '18px',
   },
   closeIcon: {
      cursor: 'pointer',
      marginTop: '12px'
   },
   messageGrid: {
      paddingTop: '10px',
      paddingLeft: '10px'
   },
   error: {
      marginTop: '10px'
   },
   note: {
      fontSize: '16px',
   },
   ListItem: {
      height: "55px",
      '&:hover': {
         backgroundColor: "#0066001f"
      },
      "& .MuiButtonBase-root": {
         opacity: "none"
      }
   },
   cursor: {
      cursor: 'default'
   },
   form: {
      maxHeight: "295px",
   },
   enableScrollbar: {
      maxHeight: "295px",
      overflowY: "scroll",
      overflowX: 'hidden',
      "&::-webkit-scrollbar": {
        width: 5,
      },
      "&::-webkit-scrollbar-track": {
        boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
        borderRadius: '30px'
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#2ca01c",
        borderRadius: '30px'
      }
   },
   ListItemText: {
      color: "#000000fa",
      "& .MuiListItemText-primary": {
         fontSize: '16px',
      },
      "& .MuiListItemText-secondary": {
         fontSize: '13px !important'
      },
      
   }
}));


export default useStyles;

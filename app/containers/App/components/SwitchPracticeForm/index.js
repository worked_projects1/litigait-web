/**
 *
 *   Switch Practice Form
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { Field, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import {
   Backdrop,
   Modal,
   Paper,
   Fade,
   Grid,
   Typography,
} from '@material-ui/core';
import validate from 'utils/validation';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Styles from './styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';


/**
 * 
 * @param {object} props 
 * @returns 
 */
function SwitchPracticeForm(props) {
   const { handleSubmit, metaData, className, style, onOpen, onClose, error, destroy, notes, disableContainer, show, setOpenSwitchPracticeForm, initialValues, handleConfirm } = props;


   const { practicesData = [] } = metaData;

   const classes = Styles();
   const [showModal, setModalOpen] = useState(false);
   const [selectedPractice, setSelectedPractice] = useState(false);

   const theme = useTheme();
   const md = useMediaQuery(theme.breakpoints.up('md'));
   const sm = useMediaQuery(theme.breakpoints.up('sm'));

   const closeModal = () => {
      setOpenSwitchPracticeForm(false);
      setModalOpen(false);
      if (onClose) onClose();
   };

   useEffect(() => {
      return () => destroy();
   }, []);

   const openAlertDialog = (value) => {
      const selectedPracticeData = practicesData && Array.isArray(practicesData) && practicesData.length && practicesData.filter((practice) => practice.value === value) || "";
      const getSeledtedPractice = selectedPracticeData && Array.isArray(selectedPracticeData) && selectedPracticeData.length && selectedPracticeData[0] || {};
      setSelectedPractice(getSeledtedPractice);
   }

   const handleSwitchPractice = () => {
      const userId = selectedPractice[`user_id`] || "";
      const practiceId = selectedPractice[`value`] || "";
      const email = selectedPractice[`email`] || "";
      handleConfirm(userId, practiceId, email);
   }


   const confirmationMessage = selectedPractice && Object.keys(selectedPractice).length && `Do you want to switch ${selectedPractice.name}?` || "";

   const switchPracticeList = (<List dense className={classes.root}>
      {practicesData && Array.isArray(practicesData) && practicesData.length && practicesData.map((value) => {
         const currentPractice = initialValues && Object.keys(initialValues).length && initialValues.currentPractice || "";
         const initialRecord = (value && value.value) === currentPractice ? true : false;
         const labelId = `checkbox-list-secondary-label-${value}`;
         return (
            <ListItem
               key={value}
               button
               divider
               onClick={!initialRecord ? () => openAlertDialog(value.value) : false}
               selected={initialRecord}
               className={initialRecord ? `${classes.cursor} ${classes.ListItem}` : `${classes.ListItem}`}
            >
               <ListItemAvatar>
                  <Avatar
                     alt={`Avatar n°${value}`}
                     src={value.logo}
                  />
               </ListItemAvatar>
               <ListItemText className={classes.ListItemText} id={labelId} primary={value.name} secondary={initialRecord ? "(Current Practice)" : ""} />
               {initialRecord && <ListItemSecondaryAction>
                  <CheckCircleIcon style={{ color: "green" }} />
               </ListItemSecondaryAction> || null}
            </ListItem>
         );
      })}
   </List>);

   return (
      <Grid container={disableContainer ? false : true} className={className} style={style}>
         <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
               timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
               <Paper className={classes.paper} style={!sm ? { width: '90%' } : {}}>
                  <Grid container className={classes.header}>
                     <Grid item xs={10}>
                        <Grid className={classes.messageGrid}>
                           <Typography component="span" className={classes.message}>
                              {`Switch Practice`}
                           </Typography>
                        </Grid>
                     </Grid>
                     <Grid item xs={2}>
                        <Grid container justify="flex-end">
                           <CloseIcon onClick={closeModal} className={classes.closeIcon} />
                        </Grid>
                     </Grid>
                  </Grid>
                  <div className={practicesData && practicesData.length > 5 ? classes.enableScrollbar : classes.form} >
                     <Grid container className={classes.body}>
                        <Grid item xs={12}>
                           {switchPracticeList}
                        </Grid>
                        {error ? (<Grid item xs={12} className={classes.error}>
                           {' '}
                           <Error errorMessage={error} />
                        </Grid>) : null}
                     </Grid>
                     {notes ? (<Grid>
                        <Typography component="span" className={classes.note}>
                           {notes || ''}
                        </Typography>
                     </Grid>) : null}
                     <AlertDialog
                        description={confirmationMessage}
                        openDialog={selectedPractice}
                        closeDialog={() => setSelectedPractice(false)}
                        onConfirm={() => handleSwitchPractice()}
                        onConfirmPopUpClose={true}
                        btnLabel1='YES'
                        btnLabel2='NO' />
                  </div>
               </Paper>
            </Fade>
         </Modal>
      </Grid >
   );
}

export default reduxForm({
   form: 'settingsForm',
   validate,
   enableReinitialize: true,
   touchOnChange: true,
})(SwitchPracticeForm);

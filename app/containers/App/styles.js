
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';

export const StyledListItem = withStyles({
  root: {
    "&$selected": {
      backgroundColor: "black"
    },
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      backgroundColor: '#2C2C2F',
    },
    "&.MuiListItem-root.Mui-selected": {
      backgroundColor: "black"
    },
    "&.MuiListItem-button": {
      minHeight: '48px'
    },
    '@global': {
      ".MuiListItemText-root": {
        marginLeft: '20px',
        marginTop: '8px'
      }
    }
  },
  selected: {}
})(ListItem);

export const useStyles = (drawerWidth) => makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
    background: '#393a3d',

  },
  appBar: {
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    boxShadow: '0px 1px 6px -1px lightgrey',
    background: '#fff',
    zIndex: '999',
    position: 'fixed',
    overflow: 'hidden'
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
    color: '#2DA01D',
  },
  menuIcon: {
    position: "fixed",
    transform: "translate(45px, 18px)",
    backgroundColor: "#e8e8e8",
    width: "25px",
    height: "25px",
    zIndex: '100',
    '&:hover': {
      backgroundColor: '#e8e8e8',
    }
  },
  mobileMenuIcon: {
    position: "fixed",
    transform: "translate(225px, 18px)",
    backgroundColor: "#e8e8e8",
    width: "25px",
    height: "25px",
    zIndex: '100',
    '&:hover': {
      backgroundColor: '#e8e8e8',
    }
  },
  // necessary for content to be below app bar
  toolbar: {
    ...theme.mixins.toolbar,
    textAlign: 'center'
  },
  drawerPaper: {
    width: drawerWidth,
    background: '#393A3D',
    color: '#F1F1F1',
    overflow: 'auto',
    "&::-webkit-scrollbar": {
      width: 0
    }
  },
  drawerPaperFixed: {
    width: 240,
    background: '#393A3D',
    color: '#F1F1F1',
    overflow: 'auto',
    "&::-webkit-scrollbar": {
      width: 0
    }
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  login: {
    flexGrow: 1,
  },
  title: {
    color: '#2DA01D',
    textAlign: 'center',
    marginTop: '40px',
  },
  link: {
    textDecoration: 'none',
    color: '#F1F1F1',
  },
  settings: {
    position: 'relative',
    color: '#A6A6A9',
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  logout: {
    marginLeft: '12px',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    marginRight: '4px'
  },
  separator: {
    height: '1px',
    background: 'gray',
    margin: '10px 5%',
    opacity: '0.6'
  },
  logo: {
    width: '80px',
    height: '80px',
    borderRadius: '100%',
    margin: '30px 0px',
    marginTop: '10px'
  },
  mobileLogo: {
    width: '40px',
    height: '40px',
    borderRadius: '100%',
    margin: '80px 0px 20px',
  },
  svg: {
    width: '25px',
    height: '25px'
  },
  changePasswordForm: {
    display: 'contents'
  },
  esquiretekLogo: {
    width: '80px',
    height: '78px',
    borderRadius: '100%',
    margin: '30px 0px',
    marginTop: '10px'
  },
  settingForm: {
    display: 'contents'
  },
  beta: {
    background: '#2ca01c',
    borderRadius: '16px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: '12px',
    paddingRight: '12px',
    paddingTop: '3px',
    marginLeft: '7px',
    height: '20px',
    '& > h6': {
      fontSize: '12px',
      color: 'white',
      fontFamily: 'Avenir-Heavy'
    }
  },
  signatureForm: {
    display: 'contents'
  },
  signture: {
    marginRight: '12px',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },
  toolTip: {
    marginLeft: '4px'
  },
  menuItem: {
    "&:hover": {
      backgroundColor: '#2C2C2F',
    },
    "&.MuiMenuItem-root": {
      minHeight: '48px'
    },
  },
  menuList: {
    backgroundColor: '#2C2C2F',
  },
  button: {
    color: '#2DA01D',
    fontWeight: 'bold',
    fontFamily: 'Avenir-Bold',
  },
}));


/**
 *
 * App Page
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { getFormValues } from 'redux-form';

import {
  selectLoggedIn,
  selectUser,
  selectError,
  selectSuccess,
  selectLocation,
  selectActiveSession,
  selectSessionExpand,
  selectMetaData,
  selectNotificationLoader
} from 'blocks/session/selectors';

import { sessionLogin, logOut, clearCache, changePassword, sessionTimeout, sessionExpand, twoFactorAuthentication, sessionClearTimeout, updateSignature, switchPractice, chatNotification } from 'blocks/session/actions';

import {
  Grid,
  AppBar,
  CssBaseline,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItemText,
  Toolbar,
  Typography,
} from '@material-ui/core';


import MenuIcon from '@material-ui/icons/Menu';
import { useTheme } from '@material-ui/core/styles';
import SVG from 'react-inlinesvg';
import GlobalStyle from '../../global-styles';
import { StyledListItem, useStyles } from './styles';
import Snackbar from 'components/Snackbar';
import '../../../src/main.css';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ModalForm from 'components/ModalRecordForm';
import SettingsForm from './components/SettingsForm';
import SwitchPracticeForm from './components/SwitchPracticeForm';
import { Visibility } from 'utils/tools';
import Icons from 'components/Icons';
import schema from 'routes/schema';
import moment from 'moment';
import store2 from 'store2';
import Tooltip from '@material-ui/core/Tooltip';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import ButtonSpinner from 'components/ButtonSpinner';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import { withStyles } from '@material-ui/core/styles';
import AlertDialog from 'components/AlertDialog';


const changePasswordColumns = schema().changePassword().columns;
const loginFormColumns = schema().login().columns;
const userSettingsColumns = schema().userMenuSettings().section;
const authLoginFormColumns = schema().authLogin().columns;

/**
 * @param {object} props 
 */
function App(props) {

  const { loggedIn, user = {}, container, pages, dispatch, error, success, location = {}, activeSession, expand, metaData, change_password_form, two_factor_authentication_form, notificationLoader } = props;

  const { practicesData = [] } = metaData;

  const signatureColumns = schema().signature(user).columns;

  const pathData = location && location.pathname && pages && pages.length > 0 && pages.find(_ => _.data && _.data.path && location.pathname.includes(_.data.path)) || false;
  const { practiceDetails = {}, email, lastPasswordChangeDate = moment(), temporary_password } = user;
  const { name, logoFile } = practiceDetails;
  const { state = {} } = location;
  const activePath = location.pathname;
  const classes = useStyles(expand ? 240 : 60)();
  const theme = useTheme();
  const md = useMediaQuery(theme.breakpoints.up('md'));
  const [mobileOpen, setMobileOpen] = useState(false);
  const role = user && user.role || false;
  const [tabValue, setTabValue] = useState(0);
  const errorMessage = error && (error.practice || error.changePassword || error.twoFactorAuthentication || error.sessionTimeout || error.signature || error.litify || error.notification);
  const successMessage = success && (success.practice || success.changePassword || success.twoFactorAuthentication || success.signature || success.switchPractice);
  const isPropoundingPlan = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['propounding'];
  const isPropounding = isPropoundingPlan || false;
  const login_platform = store2.get('login_platform')

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const [formData, setFormData] = useState(false);
  const [openSwitchPracticeForm, setOpenSwitchPracticeForm] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [menuList, setMenuList] = React.useState(null);
  const [selectedMenu, setSelectedMenu] = React.useState(false);

  const [formSection, setFormSection] = useState(0);
  const field = userSettingsColumns[formSection];

  const { title, message, confirmMessage, confirmButton, columns, schemaId } = field;

  const initialValues = (field.value === "enabledTwoFactorAuthentication") ? Object.assign({}, { twofactor: user.twofactor_status }) : false;

  const formValues = (field.value === "changePassword") ? change_password_form : (field.value === "enabledTwoFactorAuthentication") ? two_factor_authentication_form : false;

  const confirmationMessage = confirmMessage && typeof confirmMessage == "function" ? confirmMessage(user, formValues, metaData) : confirmationMessage;
  const passwordChanges = ((moment().diff(lastPasswordChangeDate, 'days') >= 90 || temporary_password)) ? true : false;

  const StyledMenu = withStyles({
    paper: {
      border: '1px solid #d3d4d5',
      boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
      width: '260px'
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      {...props}
    />
  ));

  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '& .MuiListItemIcon-root': {
        minWidth: "35px",
      },
      '&:hover': {
        '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
          color: "#008000c9",
          fontSize: "17px",
        },
      },
    },
  }))(MenuItem);


  const handleExpandToggle = () => {
    dispatch(sessionExpand(!expand));
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleChangePassword = (data, dispatch, { form }) => {
    dispatch(changePassword(data, form));
  };

  const handleSessionLogin = (loginData, dispatch, { form }) => {
    if (loginData) {
      const email = loginData.identifier || false;
      const secret = loginData.secret || false;
      dispatch(sessionLogin(email, secret, form));
    }
  }

  const handleSignature = (data, dispatch, { form }) => {
    dispatch(updateSignature(data, form))
  }

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    let mounted = true;
    Visibility({ callback: () => dispatch(sessionTimeout(dispatch)), clearSessionTimeout: () => dispatch(sessionClearTimeout()) });
    return () => mounted = false;
  }, []);

  const settingsMenu = [
    {
      id: 0,
      label: "Change Password",
      value: ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'].includes(user.role) ? "admin_change_password" : "changePassword",
      show: true,
      openModal: true,
      logo: require('images/icons/change01.svg'),
      style: {}
    },
    {
      id: 1,
      label: "Two Factor Authentication",
      value: "enabledTwoFactorAuthentication",
      show: ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'].includes(user.role) ? false : true,
      openModal: true,
      logo: require('images/icons/TFA01.svg'),
      style: { width: "23px", height: "23px" }
    },
    // {
    //   id: 2,
    //   label: "Switch Practice",
    //   value: "switch_practice",
    //   show: !['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'].includes(user.role) && practicesData && Array.isArray(practicesData) && practicesData.length > 1 ? true : false,
    //   openModal: true,
    //   logo: require('images/icons/switch01.svg'),
    //   style: { width: "22px", height: "22px" }
    // },
    {
      id: 3,
      label: "Logout",
      value: "logout",
      show: true,
      openModal: true,
      logo: require('images/icons/logout01.svg'),
      style: { width: "22px", height: "20px" }
    }
  ]

  const handleClick = (e) => {
    setMenuList(e?.currentTarget);
  };

  const handleCloseMenuList = () => {
    setMenuList(null);
    setSelectedMenu(false);
  };

  const drawer = (
    <div>
      {!mobileOpen ? <IconButton
        onClick={handleExpandToggle}
        className={expand ? classes.mobileMenuIcon : classes.menuIcon}>
        <Icons type={expand ? "ChevronLeftIcon" : "ChevronRightIcon"} />
      </IconButton> : null}
      {(expand || mobileOpen) ? <Grid>
        <Typography variant="h5" className={classes.title}>
          {name || ''}
        </Typography>
      </Grid> : null}
      <div className={classes.toolbar}>
        {logoFile ? <img src={logoFile || ''} alt="Logo" className={(expand || mobileOpen) ? classes.logo : classes.mobileLogo} /> : role && role == 'QualityTechnician' ? <img className={expand ? classes.esquiretekLogo : classes.mobileLogo} src={require('images/home/xs_logo.png')} /> : null}
      </div>
      <List>
        {pages.filter(p => (p.data && p.data.users && p.data.users.includes(user.email)) || (p.data && isPropounding && p.data.isPropoundAccess && p.data.isPropoundAccess.includes(isPropounding)) || (p.data && !p.data.users && !p.data.isPropoundAccess)).map((page, index) => {
          const menuItems = <StyledListItem
            button
            key={(page.data && page.data.title) || ''}
            selected={(activePath === page.data.path) || (page.data.path != '/' && activePath.indexOf(page.data.path) > -1)}
            className={selectedMenu === page.data.path ? classes.menuList : false}
            onClick={(e) => {
              if (page?.data?.nestedMenu) {
                handleClick(e);
                setSelectedMenu(page.data.path)
              } else {
                setMobileOpen(false);
              }
            }}>
            <Icons type={(page.data && page.data.icon) || ''} />
            {(expand || mobileOpen) && <ListItemText primary={(page.data && page.data.title) || ''} /> || null}
          </StyledListItem>;
          return <Link
            key={index}
            data-intercom-target={`tab-${page?.id}`}
            to={page && page?.data && !page?.data?.nestedMenu && { pathname: page.data && page.data.path || '/', state: { title: page.data.title } }}
            className={classes.link}>
            {page.data.separator ? <div className={classes.separator} /> : null}
            {(!expand && !mobileOpen) ? <Tooltip
              classes={{ tooltip: classes.toolTip }}
              placement='right'
              title={<span style={{ fontSize: '12px' }}>
                {page.data && page.data.title}</span>}>
              {menuItems}
            </Tooltip> : menuItems}
            {page?.data?.nestedMenu ? <Menu
              id="simple-menu"
              anchorEl={menuList}
              keepMounted
              open={Boolean(menuList)}
              classes={{
                paper: (mobileOpen || md) ? classes.drawerPaperFixed : classes.drawerPaper,
              }}
              anchorOrigin={{
                vertical: 'center',
                horizontal: 'right',
              }}
              onClose={handleCloseMenuList}
            >
              {page?.data?.nestedMenuOption?.map((menuList, i) => {
                return <MenuItem 
                className={classes.menuItem}
                onClick={() => {
                  window.open(menuList?.url, '_blank')
                  handleCloseMenuList();
                  setMobileOpen(false);
                }}>
                  <Icons type={(menuList && menuList.icon) || ''} />
                  <span style={{ marginLeft: '20px', marginTop: '8px' }}>{menuList?.label} </span>
                </MenuItem>
              })}
            </Menu> : null}
          </Link>
        })}
      </List>
    </div>
  );

  const hanleClickMenu = (id, value) => {
    setAnchorEl(null);
    if (value == "logout") {
      dispatch(logOut(true));
    } else {
      setOpenModal(true);
      setFormSection(id);
    }
  }

  const handleMenuChange = (data, dispatch, { form }) => {
    if (form === "settingsMenu_1") {
      dispatch(changePassword(data, form));
    } else if (form === "settingsMenu_2") {
      dispatch(twoFactorAuthentication(data, form));
    }
  };

  const handleChangeForm = (value) => {
    setFormData(value)
  }

  const handleSwitchPractice = (userId, practiceId, email) => {
    dispatch(switchPractice(Object.assign({
      id: userId,
      practice_id: practiceId,
      email: email
    })));
    setOpenSwitchPracticeForm(false);
  }

  return (
    <div className={classes.root}>
      <Helmet
        title={"EsquireTek"}
        meta={[
          { name: 'description', content: "APP Page" },
        ]}
      />
      {loggedIn && (!['/signin', '/signup', '/forgot'].includes(activePath) || process.env.ENV === 'production') ? (
        <div>
          <CssBaseline />
          <AppBar id="esquiretek-header" position="fixed" className={classes.appBar}>
            <Toolbar>
              <IconButton
                aria-label="open drawer"
                edge="start"
                onClick={handleDrawerToggle}
                className={classes.menuButton}>
                <MenuIcon />
              </IconButton>
              <Grid container justify="flex-start">
                <Typography component="h1" variant="h5" style={{ color: 'black' }}>
                  {pathData && pathData.data && pathData.data.title || ''}
                </Typography>
                {/* {location && state.id && state.caseRecord && state.title && state.caseRecord.state && !['CA', 'TN', 'FL'].includes(state.caseRecord.state) && state.title === 'Cases'  && (location.pathname.indexOf('form') > -1 || location.pathname.indexOf('signature') > -1) ? <Grid className={classes.beta}>
                  <Typography variant="subtitle1">
                    BETA
                  </Typography>
                </Grid> : null} */}
              </Grid>
              <Grid container justify="flex-end">
                <Grid className={classes.settings}>
                  {((!['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'].includes(user.role)) && (practicesData && Array.isArray(practicesData) && practicesData.length > 1)) ? <><Typography noWrap style={{ marginRight: '10px' }} onClick={() => setOpenSwitchPracticeForm(true)}>
                    <img src={require('images/icons/switch01.svg')} className={classes.svg} style={{ width: '24px', marginTop: '-3px', height: 'auto' }} />
                    <span style={{ paddingLeft: "8px" }}>{md && <FormattedMessage {...messages.switchPractice} />}</span>
                  </Typography>
                    <SwitchPracticeForm
                      initialValues={Object.assign({}, { currentPractice: user && user?.practice_id })}
                      fields={userSettingsColumns}
                      show={openSwitchPracticeForm}
                      form={`switch_practice_form`}
                      record={user}
                      metaData={metaData}
                      className={classes.settingForm}
                      setOpenSwitchPracticeForm={setOpenSwitchPracticeForm}
                      handleChangeForm={handleChangeForm}
                      selectedFormData={formData}
                      dispatch={dispatch}
                      handleConfirm={handleSwitchPractice}
                    /></> : null}
                  {['lawyer', 'paralegal'].includes(user.role) ? <ModalForm
                    initialValues={Object.assign({}, { signature: user.signature })}
                    title="Signature"
                    fields={signatureColumns}
                    form={`userSignatureForm`}
                    btnLabel="Update"
                    onSubmitClose
                    className={classes.signatureForm}
                    onSubmit={handleSignature}>
                    {(open) =>
                      <Typography noWrap style={{ marginRight: '10px' }} onClick={open}>
                        <SVG src={require('images/icons/Signature.svg')} className={classes.svg} style={{ width: '34px', marginTop: '-3px', height: 'auto' }} /> {md && <FormattedMessage {...messages.signature} />}
                      </Typography>}
                  </ModalForm> : null}
                  <Typography noWrap onClick={handleMenu}>
                    <SVG
                      src={require('images/icons/user-settings.svg')}
                      className={classes.svg}
                      style={{
                        width: '20px',
                        height: 'auto',
                      }}
                    />
                    <span style={{ paddingLeft: "2px" }}>{md && email}</span>
                  </Typography>
                  <StyledMenu
                    id="customized-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    {settingsMenu.map((menu) => 
                        (menu.show ? <StyledMenuItem style={{ height: "35px" }} onClick={() => hanleClickMenu(menu.id, menu.value)}>
                          <ListItemIcon>
                            <img src={menu.logo} className={classes.svg} style={menu.style} />
                          </ListItemIcon>
                          <ListItemText style={{ color: "#0000009e" }} primary={menu.label} />
                        </StyledMenuItem> : null)
                    )}
                  </StyledMenu>
                  {/* <SettingsForm
                    initialValues={Object.assign({}, { twofactor: user.twofactor_status, currentPractice: user && user?.practice_id })}
                    fields={userSettingsColumns}
                    show={showSettingsForm}
                    form={`settingForm`}
                    record={user}
                    metaData={metaData}
                    className={classes.settingForm}
                    onSubmit={handleChangeSettings.bind(this)}
                    tabValue={tabValue}
                    menuName={settingMenuName}
                    click={(value) => handleChangeTab(value)}
                    SetShowSettingsForm={SetShowSettingsForm}
                    handleChangeForm={handleChangeForm}
                    selectedFormData={formData}
                  /> */}
                  {openModal && <ModalForm
                    initialValues={initialValues}
                    show={openModal}
                    title={title || ""}
                    fields={columns}
                    message={message}
                    form={`settingsMenu_${schemaId}`}
                    btnLabel="Update"
                    metaData={metaData}
                    onSubmitClose
                    onSubmitbtn={!confirmButton ? true : false}
                    className={classes.changePasswordForm}
                    confirmButton={confirmButton || false}
                    alertOnSubmitClose
                    confirmMessage={confirmationMessage}
                    onClose={() => setOpenModal(false)}
                    onSubmit={handleMenuChange.bind(this)} />}
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <nav className={classes.drawer} aria-label="mailbox folders">
            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
            <Hidden mdUp implementation="css">
              <Drawer
                container={container}
                variant="temporary"
                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                open={mobileOpen}
                onClose={handleDrawerToggle}
                classes={{
                  paper: classes.drawerPaperFixed,
                }}
                ModalProps={{
                  keepMounted: true, // Better open performance on mobile.
                }}>
                {drawer}
              </Drawer>
            </Hidden>
            <Hidden smDown implementation="css">
              <Drawer
                classes={{
                  paper: classes.drawerPaper,
                }}
                variant="permanent"
                open>
                {drawer}
              </Drawer>
            </Hidden>
          </nav>
        </div>
      ) : null}
      <main className={loggedIn ? classes.content : classes.login}>
        {loggedIn ? <div className={classes.toolbar} /> : null}
        {props.children}
      </main>
      <GlobalStyle />
      <Snackbar show={errorMessage || successMessage ? true : false} text={errorMessage || successMessage} severity={errorMessage && 'error' || successMessage && 'success'} handleClose={() => dispatch(clearCache())} />
      {!activeSession && loggedIn && role && role !== "superAdmin" && <ModalForm
        initialValues={{ identifier: user.email }}
        title="Login"
        fields={login_platform && ['google', 'microsoft'].includes(login_platform) ? authLoginFormColumns : loginFormColumns}
        message={'Your session has expired. Please login to continue.'}
        form={`SessionLoginForm`}
        btnLabel="Login"
        onSubmitClose={true}
        enableSubmitBtn
        show
        disableCancelBtn
        className={classes.changePasswordForm}
        onSubmit={handleSessionLogin} /> || null}
      {activeSession && loggedIn && role && login_platform && !['google', 'microsoft'].includes(login_platform) && <ModalForm
        title="Change Password"
        fields={changePasswordColumns}
        message={'Password must be changed every 90 days'}
        form={`changePasswordForm90`}
        btnLabel="Update"
        onSubmitClose
        enableSubmitBtn
        disableCancelBtn={passwordChanges}
        show={passwordChanges}
        className={classes.changePasswordForm}
        onSubmit={handleChangePassword.bind(this)} /> || null}
        {!['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'].includes(role) && activeSession && loggedIn && user?.notification && !passwordChanges ? <AlertDialog
        openDialog={loggedIn && user?.notification && !passwordChanges}
        description={() => {
          return <>
            We've enhanced our customer support ticketing system to provide you with even better assistance. For any issues or inquiries, please reach out to us at <a href='support@esquiretek.com' style={{ textDecoration: 'underline' }}>support@esquiretek.com</a>. We sincerely appreciate your support. Thank you!
          </>
        }}
        onConfirm={() => dispatch(chatNotification(Object.assign({}, { notification: false })))}
        onConfirmPopUpClose={true}
        btnLabel1='OK'
        footerBtn={() => {
          return <Button onClick={() => dispatch(chatNotification(Object.assign({}, { notification: false })))} color="primary" autoFocus className={classes.button}>
            {notificationLoader && <ButtonSpinner color={`#2DA01D`}/> || 'OK'}
            </Button>
        }}
        />: null}
    </div>
  );
}

App.propTypes = {
  activeSession: PropTypes.number,
  children: PropTypes.object,
  dispatch: PropTypes.func,
  error: PropTypes.object,
  expand: PropTypes.bool,
  location: PropTypes.object,
  loggedIn: PropTypes.bool,
  pages: PropTypes.array,
  success: PropTypes.object,
  user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  loggedIn: selectLoggedIn(),
  error: selectError(),
  success: selectSuccess(),
  user: selectUser(),
  location: selectLocation(),
  activeSession: selectActiveSession(),
  expand: selectSessionExpand(),
  metaData: selectMetaData(),
  change_password_form: getFormValues(`settingsMenu_1`),
  two_factor_authentication_form: getFormValues(`settingsMenu_2`),
  notificationLoader: selectNotificationLoader()
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(App);

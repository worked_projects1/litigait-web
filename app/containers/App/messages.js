/*
 * AppPage Messages
 *
 * This contains all the text for the AppPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AppPage';

export default defineMessages({
    help: {
        id: `${scope}.help`,
        defaultMessage: 'Help',
    },
    logout: {
        id: `${scope}.logout`,
        defaultMessage: 'Logout',
    },
    hotReload: {
        id: 'app.components.App.error',
        defaultMessage: 'You are using older version of app. <br/> Please do hard refresh to get latest changes. <br/> <br/> How to do hard refresh? <br/><b> For Windows/Linux</b> : Ctrl-F5. <br/><br/><b> For Mac</b>: <br/><b> For Safari</b>: Command (⌘)-Option-R. <br/><b> For Chrome or Firefox</b>: Command(⌘)-Shift-R.',
    },
    signature: {
        id: `${scope}.signature`,
        defaultMessage: 'Signature',
    },
    switchPractice: {
        id: `${scope}.switchPractice`,
        defaultMessage: 'Switch Practice',
    },
});

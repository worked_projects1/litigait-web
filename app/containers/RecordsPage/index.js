/**
 * 
 * Records Page
 * 
 */



import React, { useEffect, memo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button, Typography } from '@material-ui/core';
import Styles from './styles';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import schema from 'routes/schema';
import ModalForm from 'components/ModalRecordForm';
import { ImplementationTable, RecordsData, getDefaultHeaders, customTextToDate } from 'utils/tools';
import { selectUser, selectRespondDetails, selectActiveSession, selectForm } from 'blocks/session/selectors';
import moment from 'moment';
import AlertDialog from 'components/AlertDialog';
import Snackbar from 'components/Snackbar';
import 'moment-timezone';
import store2 from 'store2';
import CustomFilter from 'components/CustomFilter';
import { selectCreatedDateFilter, selectActiveFilter } from 'blocks/analytics/selectors';
import { updateActiveCustomFilter, updateDashboardFilterData } from 'blocks/analytics/actions';
import { verifySession } from 'blocks/session/actions';
import Spinner from 'components/Spinner';


const bulkTranslationsColumn = schema().questionsTranslations().bulkTranslations;
const excelExportColumns = schema().excelExport().columns;
const clientsForm = schema().cases().clientsForm;
const attachCaseForm = schema().cases().attachCase;
const activeFilter = schema().activeFilter;
const customDateFilterColumns = schema().customDateFilter().columns;

const ReactTablePages = ['practices', 'adminOrders', 'orders', 'formOtps', 'otherPartiesFormOtp', 'esquireTekUsers', 'practiceUsers', 'respondingTemplate', 'respondingCustomTemplate', 'subscriptions', 'propoundingTemplate', 'propoundingCustomTemplate'];
const routesUrl = ['/practices', '/userCategories/rest/esquiretek/users', '/userCategories/rest/practice/users', '/orderCategories/rest/subscriptions/admin/history', '/orderCategories/rest/subscriptions/history'];
const Routes = ['practices', 'esquireTekUsers', 'practiceUsers', 'subscriptions'];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} create 
 * @param {boolean} view 
 * @param {string} note 
 * @param {boolean} excel 
 * @param {string} customConfirmMessage
 * @param {boolean} customFilter
 * @param {function} userBasedLimitationConfirmMessage
 */
export default function (name, path, columns, actions, selectors, filterColumns, create, view, note, excel, customConfirmMessage, customFilter, userBasedLimitationConfirmMessage) {

    const {
        selectLoading,
        selectRecords,
        selectRecordsMetaData,
        selectError,
        selectTotalPageCount,
        selectHeaders,
        selectFilter,
        selectSuccess,
        selectUpdateError,
        selectExistedPasswordData
    } = selectors;

    /**
     * @param {object} submitData 
     * @param {function} dispatch 
     * @param {object} param2 
     */
    function handleBulkTranslations(submitData, dispatch, { form }) {
        dispatch(actions.bulkTranslation(submitData, form));
    }

    /**
     * @param {object} props 
     */
    function RecordsPage(props) {

        const classes = Styles();
        const { dispatch, records, children, location = {}, history, error, loading, metaData = {}, totalPageCount, headers = {}, filterObjection = {}, queryParams, user, pathData, respondDetails, activeSession, success, updateError, forms, createdDate, activeRecord, existedPasswordData } = props;


        const { pathname } = location;
        let respondCase = location && location.state && location.state.respondCase || false;

        const [filter, setFilter] = useState(name === 'objections' ? { state: 'AZ' } : false);
        const [excelExport, setExcelExport] = useState(false);
        const [openCreateClientForm, setCreateClientForm] = useState(respondCase);
        const [showCreateClientForm, setShowCreateClientForm] = useState(false);
        const [openAttachCaseForm, setAttchCaseForm] = useState(respondCase);
        const [openDialog, setOpenDialog] = useState(respondCase && respondDetails && respondDetails.responder_case_id && !respondDetails.is_template_used_by_responder);
        const [progress, setProgress] = useState(false);
        const [pageLoading, setPageLoading] = useState(false);

        const activeChildren = path !== pathname;
        const fullView = activeChildren && pathname.indexOf('settings') > -1 ? true : false;
        const section = columns && typeof columns === 'object' && columns.section || false;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const lg = useMediaQuery(theme.breakpoints.up('lg'));
        const TableWrapper = ReactTablePages.includes(name) ? ImplementationTable['reactTable'] : ImplementationTable.default;
        const customNote = ['esquireTekUsers', 'practiceUsers', 'practices'].includes(name) ? filter?.inactive_accounts ? `This will download the list of practices inactive for 1 ${filter.duration} (the list you are currently viewing)` : `Filter by account created date` : ['adminOrders', 'subscriptions'].includes(name) ? `Filter by order date` : false;
        let metaRecord = metaData && metaData.clientFilterOptions && metaData.clientFilterOptions.length > 0 ? true : false;
        const lastPasswordChangedDate = user && moment().diff(user.lastPasswordChangeDate, 'days') <= 90;
        let showClientModel = openCreateClientForm && user && Object.keys(user).length > 0 && user.clients_available === false && lastPasswordChangedDate;

        let showCaseModel = metaRecord && openAttachCaseForm && user && Object.keys(user).length > 0 && user.clients_available && lastPasswordChangedDate;

        let confirmMessage = customConfirmMessage && typeof customConfirmMessage === 'function' && customConfirmMessage(user, records) || false;
        let userLimitationConfirmMessage = userBasedLimitationConfirmMessage && typeof userBasedLimitationConfirmMessage === 'function' && userBasedLimitationConfirmMessage(user, records) || false;

        const activeFilterColumns = activeFilter && activeFilter(name).columns;

        const activeFilterValues = forms && forms[`activeFilter_${name}`] && forms[`activeFilter_${name}`].values && Object.keys(forms[`activeFilter_${name}`].values).length > 0 && forms[`activeFilter_${name}`].values || {};

        const createdDateValues = forms && forms[`createdDate_${name}`] && forms[`createdDate_${name}`].values && Object.keys(forms[`createdDate_${name}`].values).length > 0 && forms[`createdDate_${name}`].values || {};

        const initialActiveRecord = activeRecord && Object.keys(activeRecord).length > 0 && activeRecord[name] && Object.keys(activeRecord[name]).length > 0 && activeRecord[name];

        const initialCreatedDate = createdDate && Object.keys(createdDate).length > 0 && createdDate[name] && Object.keys(createdDate[name]).length > 0 && createdDate[name];

        const {practicesOptions = []} = metaData;
        const practiceName = (name === 'practiceUsers') && existedPasswordData && Object.keys(existedPasswordData).length && practicesOptions &&
        practicesOptions.length && practicesOptions.filter((val) => val.value == existedPasswordData.practiceId)[0].label || "";

        useEffect(() => {
            let mounted = true;
            if((Routes.includes(name) && pathname && routesUrl.includes(pathname)) || (!Routes.includes(name))){
                dispatch(actions.setHeadersData(getDefaultHeaders()));
                dispatch(actions.loadRecords(ReactTablePages.includes(name) ? true : false, queryParams));
            }
            return () => mounted = false;
        }, []);

        const closePasswordMessageModal = () => {
            dispatch(actions.clearExistingPasswordData());
            if (name === "users" && existedPasswordData && Object.keys(existedPasswordData).length) {
                const secret = store2.get('secret');
                dispatch(verifySession(secret));
            }
        }
        
        let displayName;
        if (name == 'clients') {
            displayName = "client";
        } else if (name == 'cases') {
            displayName = "case";
        } else if (name == 'orders') {
            displayName = "order";
        } else if (name == 'adminOrders') {
            displayName = "order";
        } else if (name == 'users') {
            displayName = "user";
        } else if (name == 'practices') {
            displayName = "practice";
        } else if (name == 'practice users') {
            displayName = "practice user";
        } else if (name == 'system admins') {
            displayName = "system admin";
        } else if (name == 'objections') {
            displayName = "objection";
        } else if (name == 'federalObjections') {
            displayName = "objection";
        } else if (name == 'questionsTranslations') {
            displayName = "Questions Translations";
        } else if (name == 'plans') {
            displayName = "Plan";
        } else if (name == 'other-parties') {
            displayName = "Non-Party";
        } else if (name == 'esquireTekUsers') {
            displayName = "EsquireTek User";
        } else if (name == 'practiceUsers') {
            displayName = "Practice User";
        } else if (name == 'respondingTemplate' || name == 'propoundingTemplate') {
            displayName = "Custom Template";
        } else if (name == 'discountCode') {
            displayName = 'Promo Code'
        } else if (name == 'feeWaiver'){
            displayName = "Fee Waiver"
        } else {
            displayName = name;
        }


        if (error) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        if (fullView) {
            return children;
        }

        const handlePageData = (data) => {
            dispatch(actions.setHeadersData(data));
            dispatch(actions.loadRecords(true, queryParams));
        }

        const handleFilter = (e) => {
            if (name === 'objections') {
                history.push({ pathname: path, state: location.state });
                dispatch(actions.UpdateStateFilter(Object.assign({}, filterObjection, { [e.target.name]: e.target.value })));
            }
            else {
                setFilter(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value }));
                if (ReactTablePages.includes(name)) {
                    handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: JSON.stringify(Object.assign({}, { ...filter }, { [e.target.name]: e.target.value })), search: false, sort: false, page: 1 }));
                }
            }
        }

        const handleExcelExport = (data, dispatch, { form }) => {
            let fileName = pathData && pathData.title;
            if(name === 'practices' && data && data?.inactive_accounts) {
                dispatch(actions.excelExport(data, form, queryParams, columns, fileName, setExcelExport, name));
            } else {
                let timeZone = moment.tz.guess();

                const fromDate = moment(data && data.from_date).format('YYYY-MM-DD 00:00:00');
                const endDate = moment(data && data.to_date).format('YYYY-MM-DD 23:59:59');            
    
                const utcFromDate = timeZone && moment.tz(fromDate, timeZone).utc().format('YYYY-MM-DD HH:mm:ss') || moment.tz(fromDate, 'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm:ss');
                const utcEndDate = timeZone && moment.tz(endDate, timeZone).utc().format('YYYY-MM-DD HH:mm:ss') || moment.tz(endDate, 'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm:ss');
                
                const submitData = Object.assign({}, { from_date: utcFromDate }, { to_date: utcEndDate });                        
                dispatch(actions.excelExport(submitData, form, queryParams, columns, fileName, setExcelExport, name));
            }
        }

        const handleCreateClient = (respondDetailRecord, data, dispatch, { form }) => {
            setProgress(true);
            let submitData = Object.assign({}, {...data, ...respondDetailRecord });
            dispatch(actions.createClientWithCase(submitData, form, setCreateClientForm, setShowCreateClientForm, actions.loadRecords, setProgress));
        }

        const handleAttachCase = (respondDetailRecord, data, dispatch, { form }) => {
            setProgress(true);
            let submitData = Object.assign({}, {...data, ...respondDetailRecord });
            dispatch(actions.createClientWithCase(submitData, form, setAttchCaseForm, false, actions.loadRecords, setProgress));
        }

        const handleRespondForm = (data) => {
            let submitData = Object.assign({}, { ...data }); 
            dispatch(actions.createResponderForm(submitData, actions.loadRecords));
        }

        const handleActiveRecord = (data, type) => {            
            const activeType = data && Object.keys(data).length > 0 && data.active && type || false;
            setFilter(Object.assign({}, { ...filter }, { inactive_accounts : false, duration: 'month'  }));
            dispatch(actions.setHeadersData(Object.assign({}, { ...headers }, { filter: JSON.stringify(Object.assign({}, { ...filter }, { inactive_accounts: false })) })));
            let createdDateRecord = createdDateValues && Object.keys(createdDateValues).length > 0 && createdDateValues.from_date && createdDateValues.to_date && Object.assign({}, {
                from_date: moment(createdDateValues.from_date).format('YYYY-MM-DD 00:00:00'),
                to_date: moment(createdDateValues.to_date).format('YYYY-MM-DD 23:59:59')
            }) || customTextToDate({ statistics: "last_30_days" });

            dispatch(updateDashboardFilterData(false));
            dispatch(updateActiveCustomFilter({ [name]: createdDateRecord }, { [name]: data }));
            dispatch(actions.loadActiveFilterRecord(createdDateRecord, activeType, queryParams));
            if(name === 'practices' && filter && filter?.inactive_accounts) {
                setPageLoading(true);
                setFilter(false);
                setTimeout(() => {
                    setPageLoading(false);
                }, 1000);
            }
        }

        const handleInActiveRecord = (value, type) => { 
            if(value) {
                setFilter(Object.assign({}, { ...filter }, { [type]: value, duration: 'month' }));
            }
            dispatch(updateDashboardFilterData(false));
            dispatch(updateActiveCustomFilter({ [name]: false }));
            if (ReactTablePages.includes(name)) {
                handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: JSON.stringify(Object.assign({}, { ...filter }, { [type]: value, duration: 'month' })), search: false, sort: false, page: 1 }));
            }

            if(name === 'practices' && !value) {
                setPageLoading(true);
                setFilter(false);
                setTimeout(() => {
                    setPageLoading(false);
                }, 1000);
            }
        }

        const handleCreatedDate = (data) => {
            const from_date = data && moment(data.from_date).format('YYYY-MM-DD 00:00:00');
            const to_date = data && moment(data.to_date).format('YYYY-MM-DD 23:59:59');            

            const activeType = activeFilterValues && Object.keys(activeFilterValues).length > 0 && activeFilterValues.active && 'active' || false;
            dispatch(updateDashboardFilterData(false));
            dispatch(updateActiveCustomFilter({ [name]: { from_date, to_date } }, { [name]: activeFilterValues })); 
            dispatch(actions.loadActiveFilterRecord({ from_date, to_date }, activeType, queryParams));
        }

        const handleClearFilter = () => {
            dispatch(updateDashboardFilterData(false));
            dispatch(updateActiveCustomFilter({}, {}));
            if(name === 'practices') {
                setPageLoading(true);
                setFilter(false);
                setTimeout(() => {
                    setPageLoading(false);
                }, 1000);
            }
            dispatch(actions.setHeadersData(getDefaultHeaders()));
            dispatch(actions.loadRecords(true, queryParams));
        }

        if(pageLoading) {
            return <Spinner loading={true} className={classes.spinner} />;
        }

        return <Grid container>
            <Grid item xs={12}>
                <Grid container>
                    <Grid item sm={8} xs={12}>
                        {filterColumns && filterColumns[name] && filterColumns[name].length > 0 ?
                            <Grid container>
                                {filterColumns[name].map((filterColumn, index) =>
                                    <Grid key={index} className={classes.dropdownGrid}>
                                        <select name={filterColumn.value} className={classes.filter} defaultValue={filterObjection ? filterObjection.state : ''} onChange={handleFilter}>
                                            {((filterColumn && Array.isArray(filterColumn.options) && filterColumn.options.length > 0 && filterColumn.options) || (filterColumn && typeof filterColumn.options === 'string' && metaData[filterColumn.options] && metaData[filterColumn.options].length > 0 && metaData[filterColumn.options]) || []).map((option, i) => <option key={i} disabled={option.disabled} value={option.value}>{option.label}</option>)}
                                        </select>
                                    </Grid>)}
                                    {customFilter && !loading && <Grid item xs={sm ? 8 : 12} className={classes.customFilter}>
                                        <CustomFilter
                                            initialValues={initialActiveRecord || {}}
                                            fields={activeFilterColumns.filter(_ => _.editRecord && !_.disableRecord && name != 'subscriptions')}
                                            name={name}
                                            title={name == 'subscriptions' ? 'Order Date' : filter?.inactive_accounts ? 'Last Login Date' : 'Created Date'}
                                            form={`activeFilter_${name}`}
                                            handleActiveRecord={handleActiveRecord}
                                            handleCreatedDate={handleCreatedDate}
                                            handleClearFilter={handleClearFilter}
                                            columns={customDateFilterColumns}
                                            initialCreatedDate={initialCreatedDate}
                                            initialActiveRecord={initialActiveRecord}
                                            createdDateValues={createdDateValues}
                                            filter={filter}
                                            handleFilter={handleFilter}
                                            practicesCustomFilter={filterColumns && filterColumns?.practicesCustomFilter}
                                            handleInActiveRecord={handleInActiveRecord} />
                                        
                                    </Grid>}
                            </Grid> : null}
                    </Grid>
                    <Grid item xs={12} sm={filterColumns && filterColumns[name] && filterColumns[name].length > 0 ? 4 : 12} className={classes.addBtn}>
                        <Grid container direction="row">
                            {note ? <Grid item xs={12}>
                                <Typography component="h1" style={{ marginBottom: '20px' }}>
                                    {note}
                                </Typography>
                            </Grid> : null}
                            <Grid item xs={12}>
                                <Grid container>
                                    {filterColumns && !filterColumns[name] && customFilter && !loading && <Grid item xs={sm ? 6 : 12} className={classes.customFilter}>
                                        <CustomFilter
                                            initialValues={initialActiveRecord || {}}
                                            fields={activeFilterColumns.filter(_ => _.editRecord && !_.disableRecord && name != 'subscriptions')}
                                            name={name}
                                            title={name == 'subscriptions' ? 'Order Date' : filter?.inactive_accounts ? 'Last Login Date' : 'Created Date'}
                                            form={`activeFilter_${name}`}
                                            handleActiveRecord={handleActiveRecord}
                                            handleCreatedDate={handleCreatedDate}
                                            handleClearFilter={handleClearFilter}
                                            columns={customDateFilterColumns}
                                            initialCreatedDate={initialCreatedDate}
                                            initialActiveRecord={initialActiveRecord}
                                            filter={filter}
                                            handleFilter={handleFilter}
                                            practicesCustomFilter={filterColumns && filterColumns?.practicesCustomFilter}
                                            createdDateValues={createdDateValues} />
                                    </Grid>}
                                    <Grid item xs={filterColumns && filterColumns[name] ? 12 : (sm && customFilter && !loading) ? 6 : 12}>
                                        <Grid container justify={sm ? "flex-end" : "flex-start"} direction="row">
                                            {excel ? <Button type="button" className={classes.exportXlsxBtn} variant="contained" color="primary" onClick={() => setExcelExport(true)}>Download Excel</Button> : null}
                                            <ModalForm
                                                initialValues={name === 'practices' ? filter?.inactive_accounts ? Object.assign({}, { duration: filter?.duration || '', inactive_accounts: filter?.inactive_accounts || ''}) : createdDateValues : createdDateValues}
                                                title={name === 'practices' && filter?.inactive_accounts ? '' : pathData && pathData.title}
                                                customNote={customNote}
                                                fields={excelExportColumns.filter(_ => _.editRecord && !_.disableRecord)}
                                                form={`excelExport_${name}`}
                                                show={excelExport}
                                                enableSubmitBtn
                                                onClose={() => setExcelExport(false)}
                                                onSubmit={handleExcelExport.bind(this)}
                                                btnLabel="Submit"
                                                disableContainer
                                                hideFields={filter && filter?.inactive_accounts || false}
                                                style={{ marginRight: '10px' }} />
                                            {name === 'questionsTranslations' &&
                                                <ModalForm
                                                    title={displayName}
                                                    fields={bulkTranslationsColumn.filter(_ => _.editRecord && !_.disableRecord)}
                                                    form={`Bulk_${name}`}
                                                    onSubmit={handleBulkTranslations.bind(this)}
                                                    disableContainer
                                                    style={{ marginRight: '20px' }}
                                                    btnLabel="Update">
                                                    {(open) => <Button type="button" variant="contained" color="primary" className={classes.button} onClick={open}>Bulk Upload</Button>}
                                                </ModalForm> || null}
                                            {create && section ?
                                                <Grid className={classes.createGrid}>
                                                    {section.map((tab, index) =>
                                                        <div key={index} className={classes.createDiv}>
                                                            <Link to={{ pathname: `${path}/create`, state: Object.assign({}, { ...location.state }, { schemaId: tab.schemaId }) }}>
                                                                <Button
                                                                    type="button"
                                                                    variant="contained"
                                                                    color="primary"
                                                                    className={classes.create} >
                                                                    Add New {tab.addNewLabel}
                                                                </Button>
                                                            </Link>
                                                        </div>)}
                                                </Grid> :
                                                create ? confirmMessage ? <AlertDialog
                                                    description={confirmMessage}
                                                    onConfirmPopUpClose={true}
                                                    page={name}
                                                    btnLabel1='OK'>
                                                    {(open) =>
                                                        <Button
                                                            type="button"
                                                            variant="contained"
                                                            color="primary"
                                                            onClick={open}
                                                            className={classes.create} >
                                                            Add New {displayName}
                                                        </Button>}
                                                </AlertDialog> : userLimitationConfirmMessage ? <AlertDialog
                                                    description={userLimitationConfirmMessage}
                                                    onConfirmPopUpClose={true}
                                                    page={name}
                                                    btnLabel1='Settings'
                                                    btnLabel2='Cancel'>
                                                    {(open) =>
                                                        <Button
                                                            type="button"
                                                            variant="contained"
                                                            color="primary"
                                                            onClick={open}
                                                            className={classes.create} >
                                                            Add New {displayName}
                                                        </Button>}
                                                </AlertDialog> : <Link to={{ pathname: `${path}/create`, state: { ...location.state } }}>
                                                    <Button
                                                        type="button"
                                                        variant="contained"
                                                        color="primary"
                                                        className={classes.create} >
                                                        Add New {displayName}
                                                    </Button>
                                                </Link> : null}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container direction={!sm ? "column-reverse" : null} wrap={!sm ? "nowrap" : null}>
                <Grid item xs={12} md={activeChildren ? 6 : 12}>
                    {section ? section.map((tab, index) =>
                        <div key={index} className={classes.section}>
                            <Typography component="h1" variant="h5">{tab.name}</Typography>
                            <Grid className={classes.tableSection}>
                                <TableWrapper
                                    schemaId={tab.schemaId}
                                    records={loading ? RecordsData : tab.filter && tab.filter(records)}
                                    columns={tab.columns}
                                    children={activeChildren}
                                    path={path}
                                    name={name}
                                    history={history}
                                    locationState={location.state}
                                    view={view}
                                    sm={sm}
                                    metaData={metaData}
                                    pageLimit={[10, 25, 100]}
                                    totalPageCount={totalPageCount}
                                    onChangeData={handlePageData}
                                    headersData={headers}
                                    loading={loading}
                                />
                            </Grid>
                        </div>) :
                        <div className={classes.table}>
                            <TableWrapper
                                records={loading ? RecordsData : !ReactTablePages.includes(name) && name !== 'objections' ? filter && records.filter(item => Object.keys(filter).filter(key => ((item[key] && item[key] === filter[key]) || filter[key] === '')).length === Object.keys(filter).length) || records : !ReactTablePages.includes(name) && name == 'objections' ? filterObjection && records.filter(item => Object.keys(filterObjection).filter(key => ((item[key] && item[key] === filterObjection[key]) || filterObjection[key] === '')).length === Object.keys(filterObjection).length) || records : records}
                                columns={typeof columns == 'function' ? columns(user).columns : columns}
                                children={activeChildren}
                                path={path}
                                name={name}
                                history={history}
                                locationState={location.state}
                                view={view}
                                metaData={metaData}
                                sm={sm}
                                totalPageCount={totalPageCount}
                                onChangeData={handlePageData}
                                headersData={headers}
                                loading={loading}
                            />
                        </div>}
                </Grid>
                {activeChildren ?
                    <Grid item xs={12} md={6} style={section && { marginTop: '32px' } || {}}>
                        <div className="children">
                            {children}
                        </div>
                    </Grid> : null}
            </Grid>
            <ModalForm
                title={'Client Information'}
                fields={clientsForm.filter(_ => _.editRecord && !_.disableRecord)}
                form={`clientsForm`}
                show={!loading && activeSession && (showClientModel || showCreateClientForm) && respondDetails && !respondDetails.responder_case_id}
                onClose={() => {
                    setCreateClientForm(false);
                    setShowCreateClientForm(false);
                }}
                page={name}
                progress={progress}
                btnLabel="Create Client and Case"
                onSubmit={handleCreateClient.bind(this, respondDetails)}
                footerStyle={{ border: 'none' }}
                disableContainer
                message={'Please provide client info for this propounding document.'}
                metaData={metaData} />
            <ModalForm
                title={'Choose Client'}
                fields={attachCaseForm.filter(_ => _.editRecord && !_.disableRecord)}
                form={`attachCaseForm`}
                show={!loading && activeSession && showCaseModel && respondDetails && !respondDetails.responder_case_id}
                onClose={() => setAttchCaseForm(false)}
                onSubmit={handleAttachCase.bind(this, respondDetails)}
                progress={progress}
                disableContainer
                message={'Please provide client info for this propounding document.'}
                btnLabel="Create Case"
                page={name}
                footerBtn={() => (<Grid container justify={lg ? "flex-start" : "flex-end"} style={lg ? { position: 'relative', top: '44px' } : null}>
                    <Button type="button" variant="contained" color="primary" className={classes.button2} onClick={() => {
                        setShowCreateClientForm(true);
                        setAttchCaseForm(false);
                    }}>
                        Create Client and Case
                    </Button>
                </Grid>)}
                footerStyle={{ border: 'none' }}
                paperClassName={classes.modalPaper}
                metaData={metaData} />
            <AlertDialog
                description={`Do you want to upload the propounding doc sent to you by "${respondDetails.propounder_practice_name}" for the case - ${respondDetails.case_title}?`}
                openDialog={!loading && activeSession && openDialog}
                closeDialog={() => setOpenDialog(false)}
                onConfirm={() => {
                    setOpenDialog(true);
                    handleRespondForm(respondDetails);
                }}
                page={name}
                onConfirmPopUpClose
                btnLabel1='Yes'
                btnLabel2='No' />
            <AlertDialog
                description={ (name === 'practiceUsers') ? `User has been linked to ${practiceName}. They can use their existing credentials to access the practice from settings. An email notification will be sent to the user.` : `User has been linked to your practice. They can use their existing credentials to access your practice from settings. An email notification will be sent to the user.`}
                openDialog={existedPasswordData}
                closeDialog={() => closePasswordMessageModal()}
                onConfirmPopUpClose
                btnLabel1='OK' />
            <Snackbar show={error || updateError || success ? true : false} text={updateError || error || success} severity={(updateError || error) && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid>
    }

    RecordsPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        headers: PropTypes.object,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        records: PropTypes.array,
        totalPageCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
        filterObjection: PropTypes.object,
        user: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        updateError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        forms: PropTypes.object,
        createdDate: PropTypes.object,
        activeRecord: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        headers: selectHeaders(),
        loading: selectLoading(),
        records: selectRecords(),
        metaData: selectRecordsMetaData(),
        error: selectError(),
        success: selectSuccess(),
        totalPageCount: selectTotalPageCount(),
        filterObjection: selectFilter(),
        user: selectUser(),
        respondDetails: selectRespondDetails(),
        activeSession: selectActiveSession(),
        updateError: selectUpdateError(),
        forms: selectForm(),
        createdDate: selectCreatedDateFilter(),
        activeRecord: selectActiveFilter(),
        existedPasswordData : selectExistedPasswordData()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(RecordsPage);

}
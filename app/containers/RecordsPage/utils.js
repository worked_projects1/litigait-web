
/**
 * 
 * Utils 
 * 
 */


import TableWrapper from 'components/TableWrapper';
import ReactTableWrapper from 'components/ReactTableWrapper';


export const ImplementationTable = {
  default: TableWrapper,
  reactTable: ReactTableWrapper
};

export const RecordsData = Array.from(Array(25), (x, index) => Object.assign({}, { id: index+1 }));
/***
 * 
 * Standard Form
 * 
 */


import React, { useState, useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Grid, Button, Typography, FormControlLabel, Checkbox } from '@material-ui/core';
import ButtonSpinner from 'components/ButtonSpinner';
import Styles from './styles';
import AlertDialog from 'components/AlertDialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import ModalForm from 'components/ModalRecordForm';
import lodash from 'lodash';
import { validation } from './utils';
import { reset } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

const FieldBox = ({ input, questions, locationState, classes, match, meta: { touched, error } }) => {
    let inputValue = (input && input.value.length > 0 && Array.isArray(input.value) && input.value) || [];

    const handleChange = (val, checked, key) => {
        if (val === "all" && checked) {
            inputValue = locationState && locationState.questions || [];
        } else if (val === "all" && !checked) {
            inputValue = [];
        } else if (checked) {
            inputValue = inputValue.filter(el => el.question_number_text != key);
            inputValue = inputValue.concat(val.map(c => Object.assign({}, { ...c }, { question_options: [] })));
        } else {
            inputValue = inputValue.filter(el => el.question_number_text != key);
        }

        input.onChange(inputValue);

    }

    return (
        <Grid container>
            <Grid container item xs spacing={3} className={classes.section} style={(locationState && locationState.filename) && match && match.path && !match.path.includes('propound') ? { marginTop: '115px' } : { marginTop: '65px' }}>
                <Grid container direction="row" spacing={3} className={classes.form}>
                    <FormControlLabel className={classes.checkboxField}
                        control={<Checkbox
                            style={{ color: "#2ca01c" }}
                            checked={locationState && inputValue.length == locationState.questions.length ? true : false}
                            onChange={(e) => handleChange('all', e.target.checked)} />}
                    />
                    <Grid container xs style={{ marginTop: '2px' }}>
                        <Typography variant="subtitle2" >
                            <b>Select All</b>
                        </Typography>
                    </Grid>
                </Grid>
                {(Object.keys(questions) || []).map((key, index) => {
                    const form = questions[key] && questions[key].length > 0 && questions[key].reduce((a, g, z) => Object.assign({}, a, {
                        question_number: g.question_number,
                        question_number_text: g.question_number_text,
                        question_options: g.question_options,
                        question_text: z === 0 ? `${g.question_text} \n${g.question_section || ''} ${g.question_section_text || ''}` : `${a.question_text || ''} \n${g.question_section || ''} ${g.question_section_text || ''}`
                    }), {});
                    return (
                        <Grid container key={index} direction="row" spacing={3} className={classes.form}>
                            <FormControlLabel className={classes.checkboxField}
                                control={<Checkbox
                                    style={{ color: "#2ca01c" }}
                                    checked={inputValue.map(_ => _.question_number).includes(form.question_number) ? true : false}
                                    onChange={(e) => handleChange(questions[key], e.target.checked, key)} />}
                            />
                            <Grid container xs style={{ marginTop: '2px' }}>
                                {match && match.path && match.path.indexOf('propound') > -1 && match.params.type != 'frogs' ? <Typography variant="subtitle2" >
                                    <b>{form.question_number_text || form.question_number}. {form.question_text}</b>
                                </Typography> : 
                                <Typography variant="subtitle2" >
                                    <b>{form.question_number_text || form.question_number} {form.question_text.split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>)}</b>
                                </Typography>}
                            </Grid>
                        </Grid>
                    )
                })}
            </Grid>
        </Grid>
    )
}



function StandardForm(props) {
    const { handleSubmit, questions, locationState, record, questionsSelected, setQuestionsSelected, pristine, submitting, dispatch, actions, navbarWidth, columns, change, history, match, isPropoundPage, forms, defaultQuestionsLength, quickCreate, btnLoader } = props;

    const classes = Styles();

    const [width, setWidth] = useState(window.innerWidth);
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const [openForm, setOpenForm] = useState(false);
    const [buttonLoader, setButtonLoader] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const standardFormRecord = record && forms && forms[`standardEditForms_${record.id}`] && forms[`standardEditForms_${record.id}`].values || false;
    const propound = match && match.path && match.path.indexOf('propound') > -1 || false;
    const useSavedTemplate = !propound && locationState && locationState.useSavedTemplate && defaultQuestionsLength && questionsSelected && defaultQuestionsLength === questionsSelected;


    useEffect(() => {
        let mounted = true;
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);

    const handleEdit = (submitData) => {
        setOpenForm(false);
        setButtonLoader(true);
        setShowAlert(false);
        change('filename', submitData.filename);
        change('questionTemplate', false);
        setTimeout(() => handleSubmit(), 2000);
    }

    const handleOnCloseEdit = (submitData) => {
        setOpenForm(false);
        setButtonLoader(true);
        setShowAlert(false);
        if(submitData && submitData.filename) {
            change('filename', submitData.filename);
            change('questionTemplate', false);
        }
        setTimeout(() => handleSubmit(), 2000);
    }

    const handleClose = () => {
        dispatch(actions.destroyStandardForm(record, quickCreate, `standardForm_${record.id}`));
    }

    const handleUpdate = (submitData) => {
        setOpenForm(false);
        setButtonLoader(true);
        setShowAlert(false);
        change('filename', submitData.filename);
        change('questionTemplate', true);
        setTimeout(() => handleSubmit(), 2000);
    }

    return <div>
        <form onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={12} className={classes.header} style={!md ? { left: '0', width: `${width}px` } : { width: `${width - navbarWidth}px`, left: navbarWidth }}>
                    <Grid container>
                        <Grid item xs={12}>
                            <Grid container justify="center">
                                <AlertDialog
                                    description={`You have selected ` + questionsSelected + ` questions. Do you want to continue?`}
                                    onConfirm={() => locationState && locationState.filename ? (propound || quickCreate) ? handleSubmit() : setShowAlert(true) : setOpenForm(true)}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='Yes'
                                    btnLabel2='No' >
                                    {(open) =>
                                        <Button type="button" variant="contained" color="primary" disabled={pristine && questionsSelected === 0} className={classes.button} onClick={open} >
                                            {(submitting || buttonLoader) && <ButtonSpinner /> || 'Save Form'}
                                        </Button>}
                                </AlertDialog>
                                <Button type="button" color={quickCreate ? "primary" : "default"} onClick={() => handleClose()} variant="contained" className={classes.button}>
                                    {quickCreate ? btnLoader && <ButtonSpinner /> || 'Next' : 'Close'}
                                </Button>
                            </Grid>
                        </Grid>
                        {locationState && locationState.filename && match && match.path && !match.path.includes('propound') ? <Grid item xs={12} style={{ fontSize: '13px', paddingTop: '25px' }}>
                            <b>Please review and confirm that the questions checked below. Match the checkboxes on the document scanned. Save form after review/necessary modifications if any.</b>
                        </Grid> : null}
                        
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Field
                        name="questions"
                        className={classes.chkBox}
                        questions={questions}
                        classes={classes}
                        component={FieldBox}
                        locationState={locationState}
                        type="text"
                        match={match}
                        onChange={(e) => {
                            const updatedQuestions = lodash.groupBy(e, 'question_number_text');
                            setQuestionsSelected(Object.keys(updatedQuestions).length);
                        }} />
                </Grid>
            </Grid>
        </form>
        <ModalForm
            title={locationState.formLabel}
            initialValues={record || {}}
            fields={columns && columns.filter(_ => _.editRecord).map(c => c.value === 'filename' ? Object.assign({}, c, { label: locationState.formLabel ? `FILENAME (FOR ${locationState.formLabel.toUpperCase()})` : 'FILENAME' }) : c)}
            form={`standardEditForms_${record.id}`}
            btnLabel="Update"
            show={openForm}
            className={classes.form}
            onSubmitClose={true}
            disableContainer
            enableSubmitBtn
            onClose={() => setOpenForm(false)}
            footerStyle={{ borderTop: 'none' }}
            validate={(f, p) => validation(f, Object.assign({}, p, { record, value: 'frogs' }), propound)}
            onSubmit={(propound || useSavedTemplate) ? handleEdit : () => setShowAlert(true)} />
            <AlertDialog
                description={`Do you want to save this as a template for future use?`}
                onConfirm={() => handleUpdate(standardFormRecord)}
                openDialog={showAlert}
                onConfirmPopUpClose={true}
                alertOnClose={() => handleOnCloseEdit(standardFormRecord)}
                btnLabel1='Yes'
                btnLabel2='No' />
    </div>
}


function mapStateToProps({ form }, props) {
    return {
        forms: form
    }
}

export default compose(
    connect(mapStateToProps),
    reduxForm({
        form: 'standardForm',
        touchOnChange: true,
        destroyOnUnmount: false,
    })
)(StandardForm);
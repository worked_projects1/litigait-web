

/**
 *
 * View Standard Form Page
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import StandardForm from './components/standardForm';
import Styles from './styles';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import { isPropound, autoIncrementQuestions } from 'utils/tools';
import { reset } from 'redux-form';
import lodash from 'lodash';
import RenderPagination from  'components/RenderPagination';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors) {
    const { selectRecord, selectUpdateError, selectSuccess, selectPageLoader, selectSessionExpand, selectUser, selectBtnLoader } = selectors;

    /**
     * @param {string} legalform_id 
     * @param {object} data 
     * @param {function} dispatch 
     * @param {object} param2 
     */
    function handleForm(legalform_id, match, editRecord, location, queryParams, data, dispatch, { form }) {
        const { path } = match;
        let submitData;
        const saveFormType = path && path.indexOf('propound') > -1 || false;
        if(queryParams && queryParams === 'quickCreate') {
            submitData = Object.assign({}, {
                practice_id: data.practice_id,
                case_id: data.case_id,
                client_id: data.client_id,
                questions: data.questions,
                document_type: data.document_type,
                content_type: 'application/pdf',
                from: 'standard_form',
                filename: data.filename,
                legalforms_id: legalform_id,
                state: data.state,
                questionTemplate: data.questionTemplate || false,
                extraction_id: data.id
            })

            dispatch(actions.quickCreateSaveStandardForm(submitData, form))
        } else if (saveFormType) {
            const questions = data && data.questions;            
            const templateQuestions = location && location.state && location.state.questions || [];
            let questionsRecord = (questions && templateQuestions && questions.length != templateQuestions.length) ? autoIncrementQuestions(questions) : questions;

            submitData = Object.assign({}, {
                practice_id: data.practice_id,
                case_id: data.id,
                questions: questionsRecord,
                document_type: editRecord && editRecord[0] && editRecord[0].document_type || match.params.type,
                file_name: data.filename
            });

            dispatch(actions.savePropoundForm(submitData, form));
        } else {
            submitData = Object.assign({}, {
                practice_id: data.practice_id,
                case_id: data.id,
                client_id: data.client_id,
                file_name: data.type,
                questions: data.questions,
                document_type: data.document_type,
                content_type: 'application/pdf',
                from: 'standard_form',
                filename: data.filename,
                legalforms_id: legalform_id,
                state: data.state,
                questionTemplate: data.questionTemplate || false,
                question_type: data && data.questions && data.questions[0] && data.questions[0].question_type || false
            })

            dispatch(actions.saveStandardForm(submitData, form))
        }
    }

    /**
     * @param {object} props 
     */
    function ViewStandardFormPage(props) {

        const { location, record, dispatch, match, history, success, error, loading, expand, user, queryParams, btnLoader } = props;        
        const classes = Styles();
        const questions = location && location.state && location.state.questions && lodash.groupBy(location.state.questions, 'question_number_text') || [];
        const documentType = location && location.state && location.state.document_type || false;
        const defaultQuestions = location && location.state && location.state.defaultQuestions || [];
        const defaultQuestionsLength = defaultQuestions && defaultQuestions.length > 0 && lodash.groupBy(defaultQuestions, 'question_number_text') || [];
        const [loadForm, setLoadForm] = useState(true);
        const [questionsSelected, setQuestionsSelected] = useState(Object.keys(defaultQuestionsLength).length);
        const navbarWidth = expand ? 240 : 60;
        const [currentPage, setCurrentPage] = useState(1);
        const pageSize = 25;
        const [pageLoader, setPageLoader] = useState(false);
        const legalform_id = location && location.state && location.state.legalform_id || null;
        const propoundTemplateData = record && record.propound_template_details && record.propound_template_details[match.params.type] && record.propound_template_details[match.params.type];
        const editRecord =  propoundTemplateData && propoundTemplateData.length > 0 && propoundTemplateData.filter(elm => elm.id == match.params.pid);
        const isPropoundPage = user && record && isPropound(user, record);
        const quickCreate = queryParams && queryParams === 'quickCreate' ? true : false;

        useEffect(() => {
            let mounted = true;
            match && match.path && match.path.indexOf('propound') > -1 ? dispatch(actions.loadPropoundRecord(match.params.id)) : dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords());
            setLoadForm(true);
            setTimeout(() => setLoadForm(false), 200);
            return () => mounted = false;
        }, [match.params.id]);

        const handlePagination = (page) =>{
            setCurrentPage(page);
            setPageLoader(true);
            setTimeout(() => setPageLoader(false), 2000);
        }


        return (<Grid container className={classes.standardFormPage}>
            {!loading && record && !loadForm && Object.keys(record).length > 0 ? 
                <RenderPagination
                    className="pagination-bar"
                    records={questions}
                    currentPage={currentPage}
                    totalCount={Object.keys(questions).length}
                    pageLoader={pageLoader}
                    pageSize={pageSize}
                    standardForm={true}
                    onPageChange={page => handlePagination(page)}>
                    {(questions) => {
                        return  <StandardForm
                                    initialValues={Object.assign({}, { ...record }, { document_type: documentType, type: location.state && location.state.type, questions: defaultQuestions, filename: location.state && location.state.filename })}
                                    name={name}
                                    path={path}
                                    columns={columns}
                                    form={`standardForm_${record.id}`}
                                    questions={questions}
                                    onSubmit={handleForm.bind(this, legalform_id, match, editRecord, location, queryParams)}
                                    match={match}
                                    locationState={location.state}
                                    history={history}    
                                    record={record}
                                    actions={actions}
                                    dispatch={dispatch}
                                    navbarWidth={navbarWidth}
                                    questionsSelected={questionsSelected}
                                    setQuestionsSelected={setQuestionsSelected}
                                    isPropoundPage={isPropoundPage}
                                    defaultQuestionsLength={Object.keys(defaultQuestionsLength).length}
                                    quickCreate={quickCreate}
                                    btnLoader={btnLoader}
                                /> }}
                </RenderPagination> : <Spinner className={classes.spinner} />}
            <Snackbar show={error || success ? true : false} text={error || success} severity={error && 'error' || 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
        </Grid >);
    }

    ViewStandardFormPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        expand: PropTypes.bool,
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        expand: selectSessionExpand(),
        user: selectUser(),
        btnLoader: selectBtnLoader(),
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewStandardFormPage);

}

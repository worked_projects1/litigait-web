import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    font-family: Avenir-Regular;
    background-color: #fff;
  }

  #app {
    background-color: #fff;
    min-height: 100%;
    min-width: 100%;
    font-family: Avenir-Regular;
  }

  p,
  label {
    font-family: Avenir-Regular;;
    line-height: 1.5em;
  }

  .MuiButton-containedPrimary{
      background-color: #2ca01c !important;
    }

  a {
    text-decoration: none;
  }  

  .MuiFilledInput-multiline {
    padding: 12px 12px 10px;
  }



  .chq-charts--wrap {
  position: relative;
}

.chq-charts--export {
  opacity: 0;
  pointer-events: none;
  position: absolute;
  right: 0;
  top: 0;
  transition: opacity 300ms ease-in-out;
}

.chq-charts--export svg {
  border-radius: 26px;
  box-sizing: content-box;
  height: 26px;
  margin: 0;
  padding: 4px;
  transition: background-color 150ms;
  width: 26px;
}

.chq-charts--export svg:hover {
  background-color: white;
}

.chq-charts--export button {
  background: inherit;
  border: 0;
  cursor: pointer;
  display: inline;
  font-family: inherit;
  font-size: inherit;
  line-height: inherit;
  margin: 0;
  outline: none;
  padding: 0;
}

.chq-charts--export-dropdown {
  border: 1px solid rgba(0, 0, 0, .15);
  border-radius: 4px;
  box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
  overflow: hidden;
  position: absolute;
  right: -5px;
  top: calc(100% + 3px);
  z-index: 1;
}

.chq-charts--export-dropdown a {
  background-color: white;
  color: #6a89af;
  display: block;
  padding: 5px 15px;
  text-decoration: none;
  white-space: nowrap;
}

.chq-charts--export-dropdown a:hover {
  background-color: #f7f7f7;
}

.chq-charts--export-trigger line {
  stroke: black;
  stroke-width: 1.5;
}

.chq-charts--wrap canvas {
  width: 400px;
  height: 400px;
}

.chq-charts--wrap:hover .chq-charts--export {
  opacity: 1;
  pointer-events: all;
}

.chq-charts--info {
  background-color: rgba(247, 247, 247, 0);
  box-sizing: border-box;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding: 27% 8px 8px;
  pointer-events: none;
  position: absolute;
  text-align: center;
  transition: background-color 300ms ease-in-out, padding-top 400ms;
}

.chq-charts--info:focus {
  outline: none;
}

.chq-charts--info-show {
  background-color: rgba(247, 247, 247, .8);
  padding-top: 25%;
  pointer-events: all;
}

.chq-charts--mono {
  font-family: monospace;
}

.chq-charts--info button {
  background: none;
  border: 0;
  border-radius: 5px;
  color: #6a89af;
  cursor: pointer;
  display: inline;
  font-family: inherit;
  font-size: inherit;
  line-height: inherit;
  margin: 0;
  outline: none;
  padding: 4px 10px;
  transition: background-color 300ms;
}

.chq-charts--info button:focus,
.chq-charts--info button:hover {
  background-color: #f7f7f7;
  box-shadow: 0 0 1px rgba(0, 0, 0, .2);
}

.chq-charts--wrap .chq-charts--chart {
  overflow: visible;
}

.chq-charts--chart text,
.chq-charts--chart line {
  animation: chqChartsFadeIn 1s ease-in-out forwards 1.25s;
  opacity: 0;
}

.chq-charts--pie-slice {
  animation: chqChartsFadeIn 1.5s ease-in-out forwards;
  cursor: pointer;
  opacity: 0;
  outline: none;
  transition: transform 300ms;
}

.chq-charts--pie-slice:hover,
.chq-charts--pie-slice:focus {
  transform: scale(1.05);
}

.chq-charts--pie-slice path:first-child {
  opacity: .3;
  transition: opacity 300ms;
}

.chq-charts--pie-slice:hover path:first-child,
.chq-charts--pie-slice:focus path:first-child {
  opacity: .6;
}

.chq-charts--hori-bar-group {
  animation: chqChartsScaleX 1.5s ease-in-out forwards;
  opacity: 0;
  outline: none;
  cursor: pointer;
  transform: scale3d(0, 1, 1);
  transform-box: fill-box;
}

.chq-charts--vert-bar-group {
  animation: chqChartsScaleY 1.5s ease-in-out forwards;
  cursor: pointer;
  opacity: 0;
  outline: none;
  transform: scale3d(1, 0, 1);
  transform-box: fill-box;
  transform-origin: 50% 100%;
}

.chq-charts--bar-shadow {
  opacity: 0;
  transition: opacity 300ms ease-in-out;
}

.chq-charts--hori-bar-group:hover .chq-charts--bar-shadow,
.chq-charts--hori-bar-group:focus .chq-charts--bar-shadow,
.chq-charts--vert-bar-group:hover .chq-charts--bar-shadow,
.chq-charts--vert-bar-group:focus .chq-charts--bar-shadow {
  opacity: .5;
}

.chq-charts--noselect {
  pointer-events: none;
}

@keyframes chqChartsFadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

@keyframes chqChartsScaleX {
  from {
    opacity: 0;
    transform: scale3d(0, 1, 1);
  }

  to {
    opacity: 1;
    transform: scale3d(1, 1, 1);
  }
}

@keyframes chqChartsScaleY {
  from {
    opacity: 0;
    transform: scale3d(1, 0, 1);
  }

  to {
    opacity: 1;
    transform: scale3d(1, 1, 1);
  }
}




.skCubeGrid {
  width: 40px;
  height: 40px;
  margin: 100px auto;
}

.skCubeGrid .skCube {
  width: 33%;
  height: 33%;
  background-color: #008580;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
  animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
}

.skCubeGrid .skCube1 {
  -webkit-animation-delay: 0.2s;
  animation-delay: 0.2s;
}

.skCubeGrid .skCube2 {
  -webkit-animation-delay: 0.3s;
  animation-delay: 0.3s;
}

.skCubeGrid .skCube3 {
  -webkit-animation-delay: 0.4s;
  animation-delay: 0.4s;
}

.skCubeGrid .skCube4 {
  -webkit-animation-delay: 0.1s;
  animation-delay: 0.1s;
}

.skCubeGrid .skCube5 {
  -webkit-animation-delay: 0.2s;
  animation-delay: 0.2s;
}

.skCubeGrid .skCube6 {
  -webkit-animation-delay: 0.3s;
  animation-delay: 0.3s;
}

.skCubeGrid .skCube7 {
  -webkit-animation-delay: 0s;
  animation-delay: 0s;
}

.skCubeGrid .skCube8 {
  -webkit-animation-delay: 0.1s;
  animation-delay: 0.1s;
}

.skCubeGrid .skCube9 {
  -webkit-animation-delay: 0.2s;
  animation-delay: 0.2s;
}

.skCubeGrid1 {
  width: 40px;
  height: 40px;
  margin: 0px auto;
}

.skCubeGrid1 .skCube {
  width: 33%;
  height: 33%;
  background-color: #008580;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
  animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
}

.skCubeGrid1 .skCube1 {
  -webkit-animation-delay: 0.2s;
  animation-delay: 0.2s;
}

.skCubeGrid1 .skCube2 {
  -webkit-animation-delay: 0.3s;
  animation-delay: 0.3s;
}

.skCubeGrid1 .skCube3 {
  -webkit-animation-delay: 0.4s;
  animation-delay: 0.4s;
}

.skCubeGrid1 .skCube4 {
  -webkit-animation-delay: 0.1s;
  animation-delay: 0.1s;
}

.skCubeGrid1 .skCube5 {
  -webkit-animation-delay: 0.2s;
  animation-delay: 0.2s;
}

.skCubeGrid1 .skCube6 {
  -webkit-animation-delay: 0.3s;
  animation-delay: 0.3s;
}

.skCubeGrid1 .skCube7 {
  -webkit-animation-delay: 0s;
  animation-delay: 0s;
}

.skCubeGrid1 .skCube8 {
  -webkit-animation-delay: 0.1s;
  animation-delay: 0.1s;
}

.skCubeGrid1 .skCube9 {
  -webkit-animation-delay: 0.2s;
  animation-delay: 0.2s;
}

@-webkit-keyframes sk-cubeGridScaleDelay {
  0%,
  70%,
  100% {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  35% {
    -webkit-transform: scale3d(0, 0, 1);
    transform: scale3d(0, 0, 1);
  }
}

@keyframes sk-cubeGridScaleDelay {
  0%,
  70%,
  100% {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  35% {
    -webkit-transform: scale3d(0, 0, 1);
    transform: scale3d(0, 0, 1);
  }
}


.Pie_Legends li {
  list-style-type: none;
  margin-bottom:5px
}

.Pie_Legends li div {
  display: flex
}


.Pie_Legends li div span {
  width:15px;
  height:12px;
  margin-right:8px;
  margin-top:4px
}

.Pie_Legends {
  display: flex;
  justify-content: center;
  width: 86%;
}

.practice-alert {
  font-weight: bold;
  font-size: 18px;
}

.viewPlanDetails {
  display: flex;
  justify-content: flex-end;
  position: relative;
  top: 15px;
  z-index: 999;
}

.viewPlanDetails > span {
  cursor: pointer;
  font-size: 10px;
  color: #2ca01c
}

.searchBtn {
  margin: 15px;
  padding: 2px;
  padding-left: 10px;
  padding-right: 10px;
  border-radius: 7px;
  cursor: pointer;
  margin-left: 0px;
  border: 1px solid gray;
}

`;

export default GlobalStyle;

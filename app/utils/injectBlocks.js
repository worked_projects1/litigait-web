/**
 * 
 * Inject Blocks
 * 
 */

import { merge } from 'lodash';
import { all } from 'redux-saga/effects';

export default function useInjectBlocks(name, blocks) {
  const blocksPromises = blocks.map((block) => import(`blocks/${block}/index`));

  //It will return all the promise with synthesized data
  return Promise.all(blocksPromises).then(([...reflectors]) => { // eslint-disable-line no-shadow
    /**
     * reflectors have all the blocks data
     */
    const synthesize = reflectors.reduce((variant, sync) => {
      if (sync.default) {
        const blockData = sync.default(name);
        // Pushing the each saga generator in sagas array to concat the sagas at the end
        variant.sagas.push(blockData && blockData.saga);
        //Merging the sync and returning the merged object
        return merge({}, variant, blockData);
      }
    }, { sagas: [] });

    return Object.assign({}, synthesize, {
      saga: function* rootSaga() {
        yield all(synthesize.sagas.map(cb => cb()))
      }
    });
  });
}


// Old user plans

const oldPlans = ['tekasyougo','monthly', 'yearly'];

// monthly, yearly subscriptions
// 2023
const freePlan = ['free_trial'];
const respondingNewPlans = ['responding_monthly_349', 'responding_yearly_3490', 'responding_monthly_495', 'responding_yearly_5100'];
const propoundingNewPlans = ['propounding_monthly_199', 'propounding_yearly_2199'];

// VIP plans
const vipPlans = ['responding_vip', 'propounding_vip'];

// user based billing plans
export const userBasedRespondingPlans = ['users_responding_monthly_license_495', 'users_responding_yearly_license_5100'];
const userBasesdPropoundingPlans = ['users_propounding_monthly_license_199', 'users_propounding_yearly_license_2149_20'];
export const bothUserBasedPlans = [...userBasedRespondingPlans, ...userBasesdPropoundingPlans];


export const respondingPlansArray = ['monthly', 'yearly', 'responding_monthly_349', 'responding_yearly_3490', 'responding_monthly_495', 'responding_yearly_5100', 'responding_vip', 'users_responding_monthly_license_495', 'users_responding_yearly_license_5100'];

export const propoundingPlansArray = ['propounding_monthly_199', 'propounding_yearly_2199', 'propounding_vip', 'users_propounding_monthly_license_199', 'users_propounding_yearly_license_2149_20'];

export const planCategoryArray = ['activation_fee', 'monthly', 'yearly', 'responding_monthly_349', 'responding_yearly_3490', 'responding_monthly_495', 'responding_yearly_5100', 'users_responding_monthly_license_495', 'users_responding_yearly_license_5100'];
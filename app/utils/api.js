import axios from 'axios';

const api = axios.create({
  baseURL: process.env.API_URL,
  timeout: 40000,
  headers: { Accept: 'application/json' },
});

export function setAuthToken(authToken) {
  api.defaults.headers.common['Authorization'] = authToken;
}

export default api;

export function uploadFile(uploadUrl, data, contentType) {
  return axios.put(uploadUrl, data, {
    headers: {
      Accept: 'application/json',
      'Content-Type': contentType,
    },
  }).then((response) => response.data);
}

export function uploadDocument(uploadUrl, data, contentType) {
  return axios.put(uploadUrl, data, {
    headers: {
      Accept: 'application/json',
      'Content-Type': contentType,
    },
  }).then((response) => response.status);
}

export const filevineApi = axios.create({
  timeout: 40000,
  headers: { Accept: 'application/json' },
});

export function setFilevineAuthToken(secret) {
  filevineApi.defaults.headers.common['Authorization'] = `Bearer ${secret.accessToken}`;
  filevineApi.defaults.headers.common['x-fv-sessionid'] = secret.refreshToken;
  filevineApi.defaults.headers.common['x-fv-userid'] = secret.userId;
  filevineApi.defaults.headers.common['x-fv-orgid'] = secret.orgId;
}

export function setFilevineBaseUrl(baseUrlName) {
  filevineApi.defaults.baseURL = `${baseUrlName}`;
}
const validate = (values, props) => {
  
  const requiredFields = props.fields.reduce((a, el) => {
    if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object' && ((el.mandatory.create && props.form.indexOf('create') > -1) || (el.mandatory.edit && props.form.indexOf('edit') > -1)))) {
      a.push(el.value)
    }
    return a;
  }, []);
  
  const errors = {}

  const sequence = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678)+/ig

  const identical = /^(?!.*(.)\1\1.*).*$/igm

  const commonNames = ["123456", "password", "123456789", "12345678", "12345",
      "111111", "1234567", "sunshine", "qwerty", "iloveyou", "princess", "admin", "welcome",
      "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&amp;*", "charlie",
      "aa123456", "donald", "password1", "qwerty123"
  ]


  requiredFields.forEach(field => {
    if ((!values[field] && field != 'dob' && typeof values[field] === 'boolean') || (!values[field] && field != 'dob' && values[field] != '0')) {
      errors[field] = 'Required'
    } else if (field === 'date_of_loss') {
      errors[field] = dateValidation(values[field]);
    }
  })

  if (values.name && values.name.length < 4) {
    errors.name = 'The name must be at least 4 characters.'
  }

  if ((values?.case_defendant_name && values?.case_defendant_name?.length || 0) + (values.case_plaintiff_name && values.case_plaintiff_name.length || 0)  > 240 ) {
    errors.case_defendant_name = 'The combined length of plaintiff and defendant names should not be more than 240 characters.';
    errors.case_plaintiff_name = 'The combined length of plaintiff and defendant names should not be more than 240 characters.'
  }

  if (values.case_title && values.case_title.length < 4) {
    errors.case_title = 'The case title must be at least 4 characters.'
  }
  
  if (values.objection_title && values.objection_title.trim().length < 2) {
    errors.objection_title = 'The objection title must be at least 2 characters.'
  }

  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
  ) {
    errors.email = 'Invalid Email'
  }

  if (values.password && values.password.length < 8) {
    errors.password = 'Password must be at least 8 characters'
  }

  if (values.password && values.password.length >= 8 && !/^(?=.*[\d#?!@$%^&*-]).{8,}$/i.test(values.password)) {
    errors.password = 'Must contain at least one numeric or special character '
  }

  if (values.password && sequence.test(values.password) || !identical.test(values.password)) {
    errors.password = 'Avoid consecutive sequential and identical characters'
  }

  if(values.state_bar_number && values.state_bar_number.length < 5){
    errors.state_bar_number = 'State bar number should be at least 5 characters.' 
  }

  commonNames.forEach(field => {
    if (values.password == field) {
      errors.password = "Password is easily guessable"
    }
  })

  if (values.password && values.new_password && values.new_password != values.password) {
    errors.new_password = 'Password Mismatch'
  }

  if(values.address && values.address.length < 4){
    errors.address = 'The address must be at least 4 characters.';
  }

  if (values.medical_history_pricing_tier) {
    values.medical_history_pricing_tier.map((item, pos, rows) => {

      if (rows.length - 1 != pos) {
        if ((item.to >= item.from) && (pos == 0 || (item.from - rows[pos - 1].to === 1)) && item.price >= 0) {

        } else {
          // errors.medical_history_pricing_tier = 'Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.';
        }
      }
    })

  }

  if (values.dob) {
    errors.dob = dateValidation(values.dob);
  }

  if(values.matter_id && values.matter_id.length > 65) {
    errors.matter_id = 'Only 64 characters allowed.';
  }
  
  if(values.claim_number && values.claim_number.length > 65) {
    errors.claim_number = 'Only 64 characters allowed.';
  }

  if(values.numberOfDays && values.numberOfDays < 1) {
    errors.numberOfDays = 'Negative values and zero are not allowed.';
  }

  if(values.default_objections) {
    errors.default_objections = jsonValidation(values.default_objections)
  }

  if(values.opposing_counsel_attorney_name && values.opposing_counsel_attorney_name.split(',') && values.opposing_counsel_attorney_name.split(',').length > 5) {
    errors.opposing_counsel_attorney_name = "Only 5 Opposing counsel attorney name allowed."
  }

  if(values.selected_forms && !values.selected_forms.length) {
    errors.selected_forms = 'Please select atleast one file to send an email.'
  }

  if (values.cc_propounder_email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.cc_propounder_email)
  ) {
    errors.cc_propounder_email = 'Invalid Email'
  }

  if (values.cc_responder_email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.cc_responder_email)
  ) {
    errors.cc_responder_email = 'Invalid Email'
  }

  const opposing_counsels = props.fields.reduce((a, el) => {
    if (el.type === 'fieldArray') {
      a.push(...el.fieldArray)
    }
    return a;
  }, []);

  const requiredFields1 = opposing_counsels && opposing_counsels.length > 0 && opposing_counsels.reduce((a, el) => {
    if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object' && ((el.mandatory.create && props.form.indexOf('create') > -1) || (el.mandatory.edit && props.form.indexOf('edit') > -1)))) {
      a.push(el.value)
    }
    return a;
  }, []);


  if (requiredFields.includes('opposing_counsel')) {
    if ((!values.opposing_counsel || values.opposing_counsel.length <= 0) && props.form != `editRecordForm_${values.id}`) {
      errors.opposing_counsel = { _error: 'At least one PoS info must be entered' }; 
    } else if (values.opposing_counsel && typeof values.opposing_counsel !== 'string') {
      const posArrayErrors = [];
      if(values.opposing_counsel && values.opposing_counsel.length > 0) {
        values.opposing_counsel.forEach((fields, fieldsIndex) => {
          const posErrors = {};
          requiredFields1 && requiredFields1.map((field, i) => {
            if ((!fields[field] && field != 'dob' && typeof fields[field] === 'boolean') || (!fields[field] && field != 'dob' && fields[field] != '0')) {
              posErrors[field] = "Required";
              posArrayErrors[fieldsIndex] = posErrors;
            } else {
              if(field === 'opposing_counsel_attorney_name') {
                if(fields[field] && fields[field].split(',').length > 5) {
                  posErrors[field] = "Only 5 Opposing counsel attorney name allowed.";
                  posArrayErrors[fieldsIndex] = posErrors;
                }
              }
            }
          });
        });
      }
  
      if (posArrayErrors.length > 0) {
        errors.opposing_counsel = posArrayErrors
      }
    } 
  }

  if (requiredFields.includes('propounder_opposing_counsel')) {
    if (!values.propounder_opposing_counsel || values.propounder_opposing_counsel.length <= 0) {
      errors.propounder_opposing_counsel = { _error: 'At least one PoS info must be entered' }; 
    } else if (values.propounder_opposing_counsel && typeof values.propounder_opposing_counsel !== 'string') {
      const posArrayErrors = [];
      if(values.propounder_opposing_counsel && values.propounder_opposing_counsel.length > 0) {
        values.propounder_opposing_counsel.forEach((fields, fieldsIndex) => {
          const posErrors = {};
          requiredFields1 && requiredFields1.map((field, i) => {
            if ((!fields[field] && field != 'dob' && typeof fields[field] === 'boolean') || (!fields[field] && field != 'dob' && fields[field] != '0')) {
              posErrors[field] = "Required";
              posArrayErrors[fieldsIndex] = posErrors;
            } else {
              if(field === 'opposing_counsel_attorney_name') {
                if(fields[field] && fields[field].split(',').length > 5) {
                  posErrors[field] = "Only 5 Opposing counsel attorney name allowed.";
                  posArrayErrors[fieldsIndex] = posErrors;
                }
              }
            }
          });
        });
      }
  
      if (posArrayErrors.length > 0) {
        errors.propounder_opposing_counsel = posArrayErrors
      }
    } 
  }

  if (values.client_name && values.client_name.length > 200) {
    errors.client_name = 'The client name must be 200 characters.'
  }

  if (values.license_count && parseInt(values.license_count) > 200) {
    errors.license_count = 'The maximum license limit is 200'
  }

  const duplicateFileName = values && values.file_name && props.records && props.records.length > 0 && props.records.find(_ => _.id != values.id && _.file_name && _.file_name.toString() === values.file_name.toString()) || false;

  if (duplicateFileName) {
    errors.file_name = 'File Name Already Exists.';
  }

  if (values?.file_name?.length > 40) {
    errors.file_name = 'Only 40 characters allowed.';
  }

  if(values?.discount_percentage > 100) {
    errors.discount_percentage = 'Discount percentage should not be more than 100'
  }

  if(values?.discount_code?.length > 12){
    errors.discount_code = 'Only 12 characters allowed.';
  }

  if (values?.headings_to_add) {
    const headings = checkJSON(values?.headings_to_add);
    if (!headings) {
      errors.headings_to_add = 'Invalid array format';
    }
  }

  if (values?.phone && phoneNumberValidation(values?.phone)) {
    errors.phone = 'The phone number must be 10 digits';
  } 
  
  if (values?.client_phone && phoneNumberValidation(values?.client_phone)) {
    errors.client_phone = 'The phone number must be 10 digits';
  }

  return errors
}

const dateValidation = (val) => {
  var date = val;
  var result = '';
  if (!isNaN(Date.parse(date))) {

  } else {
    result = 'Invalid date'
  }
  return result
}

const dateValidation1 = (val) => {
  var date = val;
  var result = '';
  var splittedDate = date && date.includes('/') && date.split('/');
  if (!isNaN(Date.parse(date)) && splittedDate.length == 3) {
    if (splittedDate[2].length > 4 || splittedDate[2].length < 4) {
      result = 'Invalid date'
    }
  } else {
    result = 'Invalid date'
  }
  return result
}

const jsonValidation = (val) => {
  var result = '';
  try {
    JSON.parse(val);
  } catch (error) {
    result = 'Invalid json format';
  }
  return result;
}

const checkJSON = (text) => {
  if (typeof text !== "string") {
    return false;
  }
  try {
    var json = JSON.parse(text);
    return json;
  }
  catch (error) {
    return false;
  }
}

const phoneNumberValidation = (phoneNumber) => {
  let phone = phoneNumber.toString(); 
  phone = phone.replace(/[\(\)\s-]/g, '');
  if(phone && phone.length < 10) return true;
  return false;
}

export default validate;

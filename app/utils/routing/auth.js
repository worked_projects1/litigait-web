
import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import * as Loads from 'react-loads';
import Spinner from 'components/Spinner';
import store2 from 'store2';
import { useClearCacheCtx } from 'react-clear-cache';
import { getUTCTimestamp } from 'utils/tools';
import history from 'utils/history';
import ZendeskService from './ZendeskService';

export default function ({ Component, path, exact = false, children, data, loggedIn, location, user = {}, queryParams, appVersion }) {

    const { role, version } = user;
    
    const {
        response: Page,
        error,
        isPending,
        isResolved,
        isRejected
    } = Loads.useLoads(path, Component);

    
    // useEffect(() => {
    //     window.Intercom("boot", {
    //         app_id: "qhv3mw7n",
    //         name: user.name, // Full name
    //         email: user.email, // Email address
    //         created_at: getUTCTimestamp(user.createdAt), // Signup date as a Unix timestamp
    //         hide_default_launcher: !loggedIn
    //     });
    //     window.Intercom("update");
    // }, [user, loggedIn]);

    const { emptyCacheStorage } = useClearCacheCtx();
    const storeVersion = store2.get('version');

    /* Reset Cache */
    if ((!storeVersion && version) || (version && storeVersion && version !== storeVersion)) {
        store2.set('version', version);
        emptyCacheStorage();
    }

    if((!version && appVersion && storeVersion && appVersion !== storeVersion) || (appVersion && !version && !storeVersion)) {
        store2.set('version', appVersion);
        emptyCacheStorage();
    }

    return (
        <>
            {loggedIn ? <ZendeskService /> : null}
            <Route
                exact={exact}
                path={path}
                render={props => loggedIn && data && data.route ?
                    <Redirect
                        to={{
                            pathname: `/${role && ['manager', 'superAdmin'].includes(role) ? 'dashboard' : role && ['operator'].includes(role) ? 'practices' : role == 'medicalExpert' ? 'rest/medical-history-summery' : role == 'QualityTechnician' ? 'supportCategories' : 'quickCreate'}`,
                            state: Object.assign({}, { ...history.location.state }, { title: role && ['manager', 'superAdmin'].includes(role) ? 'Dashboard' : role && ['operator'].includes(role) ? 'Practices' : role == 'medicalExpert' ? 'Medical History' : role == 'QualityTechnician' ? 'Support Request' : 'Quick Create' })
                        }} /> :
                    (<div>
                        {isPending && !isResolved && !loggedIn && <Spinner loading={true} showHeight /> || isPending && !isResolved && 'Loading...'}
                        {(isRejected || error) && 'Something Went Wrong. Please try again.'}
                        {Page && <Page {...props} pathData={data} queryParams={queryParams || false}>{children}</Page>}
                    </div>)
                }
            />
        </>
        
    )

}

/**
 * 
 * Route Switcher
 * 
 */

import anonymousRoutesProvider from 'routes/anonymous';
import administratorRoutesProvider from 'routes/administrator';
import attorneyRoutesProvider from 'routes/attorney';
import customerRoutesProvider from 'routes/customer';
import managerRoutesProvider from 'routes/manager';
import operatorRoutesProvider from 'routes/operator';
import medicalRoutesProvider from 'routes/medical';
import qualityTechnicianRoutesProvider from 'routes/qualityTechnician';

const routeSwitcher = (user, loggedIn, callback) => {
  if (!loggedIn) {
    return anonymousRoutesProvider(callback);
  }
  switch (user.routes) {
    case 'superAdmin':
      return administratorRoutesProvider(callback);
    case 'lawyer':
    case 'paralegal':
      return attorneyRoutesProvider(callback, user);
    case 'customer':
      return customerRoutesProvider(callback, user);
    case 'manager':
      return managerRoutesProvider(callback);
    case 'operator':
      return operatorRoutesProvider(callback);
    case 'medicalExpert':
      return medicalRoutesProvider(callback);
    case 'QualityTechnician':
      return qualityTechnicianRoutesProvider(callback);
    default:
      return anonymousRoutesProvider(callback);
  }
};

export default routeSwitcher;
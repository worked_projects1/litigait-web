
export default (injectReducer, injectSaga, injectBlocks, applyPromise) => ({ id, path, pageName, queryParams, exact, require, container, childRoutes, data, chunk }) => ({
  id,
  path,
  pageName,
  queryParams,
  exact,
  Component() {
    if (typeof container === 'function') {
      if (chunk) {
        return injectBlocks(pageName, chunk).then(synthesizeData => applyPromise({ injectReducer, injectSaga, container, require, synthesizeData }));
      } else {
        return applyPromise({ injectReducer, injectSaga, container, require });
      }
    } else {
      return Promise.resolve(import(`containers/${container}/index`)
        .then((defaultContainer) => defaultContainer.default)
        .catch(error => error));
    }
  },
  childRoutes,
  data,
});

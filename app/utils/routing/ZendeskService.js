import React, { useEffect } from 'react';

const ZendeskService = () => {
  useEffect(() => {
    const script = document.createElement('script');
    script.id = 'ze-snippet';
    script.src = 'https://static.zdassets.com/ekr/snippet.js?key=a961c7cf-7da6-4b05-8910-e4f4279e394d';
    document.head.appendChild(script);

    return () => {
      document.head.removeChild(script);
    };
  }, []);

  return <div id="ze-snippet" />;
};

export default ZendeskService;

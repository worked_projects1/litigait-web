/*
 *
 *  global settings remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    function loadGlobalSettings() {
        return api.get(`settings`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateGlobalSettings(record) {
        return api.put(`settings`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        loadGlobalSettings,
        updateGlobalSettings
    }
}



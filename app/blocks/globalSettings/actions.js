/*
 *
 * global settings actions
 *
 */


/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {
    const {
        LOAD_GLOBAL_SETTINGS,
        LOAD_GLOBAL_SETTINGS_ERROR,
        LOAD_GLOBAL_SETTINGS_SUCCESS,
        UPDATE_GLOBAL_SETTINGS,
        UPDATE_GLOBAL_SETTINGS_ERROR,
        UPDATE_GLOBAL_SETTINGS_SUCCESS,
        LOAD_GLOBAL_SETTINGS_META_DATA,
        LOAD_GLOBAL_SETTINGS_META_DATA_SUCCESS,
        LOAD_GLOBAL_SETTINGS_META_DATA_ERROR,
    } = constants;


    /**
     * @param {boolean} load  
     */
    function loadGlobalSettings(load) {
        return {
            type: LOAD_GLOBAL_SETTINGS,
            load
        };
    }

    /**
     * @param {string} error 
     */
    function loadGlobalSettingsError(error) {
        return {
            type: LOAD_GLOBAL_SETTINGS_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     */
    function loadGlobalSettingsSuccess(record) {
        return {
            type: LOAD_GLOBAL_SETTINGS_SUCCESS,
            record
        };
    }

    /**
     * @param {object} record
     * @param {object} user
     * @param {string} form 
     */
    function updateGlobalSettings(record, user, form) {
        return {
            type: UPDATE_GLOBAL_SETTINGS,
            record,
            user,
            form
        };
    }

    /**
     * @param {string} error 
     */
    function updateGlobalSettingsError(error) {
        return {
            type: UPDATE_GLOBAL_SETTINGS_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updateGlobalSettingsSuccess(record, success) {
        return {
            type: UPDATE_GLOBAL_SETTINGS_SUCCESS,
            record,
            success
        };
    }


    function loadGlobalSettingsMetaData(load) {
        return {
            type: LOAD_GLOBAL_SETTINGS_META_DATA,
            load
        }
    }
    
    /**
     * 
     * @param {object} recordsMetaData 
     * @returns 
     */
    function loadGlobalSettingsMetaDataSuccess(recordsMetaData){
        return {
            type: LOAD_GLOBAL_SETTINGS_META_DATA_SUCCESS,
            recordsMetaData
        }
    }
    
    /**
     * 
     * @param {string} error 
     * @returns 
     */
    function loadGlobalSettingsMetaDataError(error){
        return {
            type: LOAD_GLOBAL_SETTINGS_META_DATA_ERROR,
            error
        }
    }


    return {
        loadGlobalSettings,
        loadGlobalSettingsError,
        loadGlobalSettingsSuccess,
        updateGlobalSettings,
        updateGlobalSettingsError,
        updateGlobalSettingsSuccess,
        loadGlobalSettingsMetaData,
        loadGlobalSettingsMetaDataError,
        loadGlobalSettingsMetaDataSuccess
    }

}



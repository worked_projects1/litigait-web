/**
 * 
 * global settings selectors
 * 
 */

import { createSelector } from 'reselect';
import mapRecords from './utils';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {
    
    const selectDomain = () => (state) => state[name] || false;

    const selectGlobalSettings = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.record && mapRecords(domain.record, name) || {},
    );
    
    
    return {
        selectDomain,
        selectGlobalSettings
    }
    
}


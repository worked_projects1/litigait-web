
/*
 *
 *  utils
 *
 */

import moment from 'moment';
import 'moment-timezone';



/**
 * @param {object} record 
 */
export function mapGlobalSettings(record) {
    if (record && Object.keys(record).length > 0) {
        let Record = Object.assign({}, record);

        Record.old_pricing_till = record.old_pricing_till && moment(record.old_pricing_till).format('MM/DD/YYYY HH:mm:ss') || false;
        Record.new_pricings_from = record.new_pricings_from && moment(record.new_pricings_from).format('MM/DD/YYYY HH:mm:ss') || false;

        return Record;
    }
}


/**
 * @param {object} record 
 * @param {string} name 
 */
export default function mapRecords(record, name) {
    switch (name) {
        case 'globalSettings':
            return mapGlobalSettings(record);   
        default:
            return record;
    }
}

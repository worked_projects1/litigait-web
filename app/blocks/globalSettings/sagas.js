/*
 *
 *  global settings sagas
 *
 */


import { call, put, all, takeLatest, delay } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import appRemotes from '../records/remotes';
import { verifySession } from 'blocks/session/remotes';
import { verifySessionSuccess } from 'blocks/session/actions'

import {
    DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR,
    DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {object} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {
    const {
        LOAD_GLOBAL_SETTINGS,
        UPDATE_GLOBAL_SETTINGS,
        LOAD_GLOBAL_SETTINGS_META_DATA
    } = constants;


    const {
        loadGlobalSettings: loadGlobalSettingsAction,
        loadGlobalSettingsError,
        loadGlobalSettingsSuccess,
        updateGlobalSettingsError,
        updateGlobalSettingsSuccess,
        loadGlobalSettingsMetaData,
        loadGlobalSettingsMetaDataError,
        loadGlobalSettingsMetaDataSuccess
    } = actions;


    const {
        loadGlobalSettings,
        updateGlobalSettings
    } = remotes;




    function* loadGlobalSettingsSaga() {
        yield takeLatest(LOAD_GLOBAL_SETTINGS, function* updater({ load }) {
            if (load) {
                yield put(loadGlobalSettingsMetaData(true));
                try {
                    const result = yield call(loadGlobalSettings);
                    if (result) {
                        yield put(loadGlobalSettingsSuccess(result));
                    } else {
                        yield put(loadGlobalSettingsError(DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR));
                    }
                } catch (error) {
                    yield put(loadGlobalSettingsError(DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR));
                }
            }
        })
    }

    function* updateGlobalSettingsSaga() {
        yield takeLatest(UPDATE_GLOBAL_SETTINGS, function* updater({ record, user, form }) {
            if (record) {
                yield put(startSubmit(form));
                try {
                    const result = yield call(updateGlobalSettings, record);
                    if (result) {
                        let successMessage = form && form === 'SettingsForm_3' ? 'Medical History Pricing Updated' : form && form === 'SettingsForm_2' ? 'Price Change Updated' : form && form === 'OtherSettingsForm_2' ? 'Account Quotas Updated' : form && form === 'OtherSettingsForm_3' ? 'Custom Template Limitations Updated' : form && form === 'OtherSettingsForm_4' ? 'Propound Settings Updated' : form && form === 'OtherSettingsForm_5' ? 'Tek-as-you-go Settings Updated' : form && form === 'OtherSettingsForm_6' ? 'New Pricing Feature Date Upadated' : form && form === 'OtherSettingsForm_7' ? 'Version History Limit Updated' : form && form === 'OtherSettingsForm_8' ? 'Document Editor Access Updated' : form && form === 'OtherSettingsForm_9' ? 'Prompt Updated' : (user && user?.role == 'medicalExpert' && form === 'OtherSettingsForm_1') ? 'Prompt Updated' : 'HIPAA Compliance Updated';
                        yield put(updateGlobalSettingsSuccess(result, successMessage));
                        yield put(stopSubmit(form));
                        if(form && form === 'OtherSettingsForm_6') {
                            yield delay(500);
                            const user = yield call(verifySession);            
                            if(user) 
                                yield put(verifySessionSuccess(user));
                        }
                    } else {
                        yield put(updateGlobalSettingsError(DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR));
                        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR }));
                    }
                } catch (error) {
                    yield put(updateGlobalSettingsError(DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR));
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR }));
                }
            }
        })
    }


    function* loadGlobalSettingsMetaDataSaga() {
        yield takeLatest(LOAD_GLOBAL_SETTINGS_META_DATA, function* updater({ load }) {
            if(load) {
                try {
                    let recordsMetaData = {};
                    if (entityUrl === 'globalSettings') {
                      const practicesRemotes = yield call(appRemotes, 'rest/practices');
                      const practices = yield call(practicesRemotes.loadRecords);
                      recordsMetaData = { practices };
                    }
                    yield put(loadGlobalSettingsMetaDataSuccess(recordsMetaData));
                } catch (error) {
                    yield put(loadGlobalSettingsMetaDataError(error));
                }
            }
        })
    }



    return function* rootSaga() {
        yield all([
            loadGlobalSettingsSaga(),
            updateGlobalSettingsSaga(),
            loadGlobalSettingsMetaDataSaga()
        ]);
    }
}


/*
 *
 *  Translations remotes
 *
 */

import api from 'utils/api';



export default function (name) {
    /**
     * @param {array} records 
     */
    function bulkTranslation(records) {
        return api.post(`/rest/questions-translations/save-multiple`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        bulkTranslation
    }
}

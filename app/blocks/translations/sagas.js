/*
 *
 *  Translations sagas
 *
 */

import { push } from 'react-router-redux';
import { call, take, put, race, all } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import history from 'utils/history';

import {
  DEFAULT_BULK_TRANSLATION_ERROR
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    BULK_TRANSLATION
  } = constants;

  const {
    bulkTranslationError,
    bulkTranslationSuccess
  } = actions;

  const {
    bulkTranslation
  } = remotes;


  function* bulkTranslationSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { bulk } = yield race({
        bulk: take(BULK_TRANSLATION),
      });
      const { record, form } = bulk || {};
      if (record) {
        yield put(startSubmit(form));
        try {

          const result = yield call(bulkTranslation, JSON.parse(record.json_data));

          if (result) {
            yield put(stopSubmit(form));
            yield put(bulkTranslationSuccess(JSON.parse(record.json_data)));
            yield put(push({ pathname: `/rest/questions-translations`, state: history.location.state }));
          } else {
            yield put(stopSubmit(form, { _error: DEFAULT_BULK_TRANSLATION_ERROR }));
            yield put(bulkTranslationError(DEFAULT_BULK_TRANSLATION_ERROR));
          }
        } catch (error) {
          yield put(stopSubmit(form, { _error: DEFAULT_BULK_TRANSLATION_ERROR }));
          yield put(bulkTranslationError(DEFAULT_BULK_TRANSLATION_ERROR));
        }
      }
    }
  }


  return function* rootSaga() {
    yield all([
      bulkTranslationSaga()
    ]);
  }


}
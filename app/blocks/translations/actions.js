/*
 *
 * Translations actions
 *
 */

/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {
  const {
    BULK_TRANSLATION,
    BULK_TRANSLATION_SUCCESS,
    BULK_TRANSLATION_ERROR
  } = constants;


  /**
   * @param {object} record 
   * @param {string} form 
   */
  function bulkTranslation(record, form) {
    return {
      type: BULK_TRANSLATION,
      record,
      form
    };
  }

  /**
   * @param {array} records 
   */
  function bulkTranslationSuccess(records) {
    return {
      type: BULK_TRANSLATION_SUCCESS,
      records
    };
  }

  /**
   * @param {string} error 
   */
  function bulkTranslationError(error) {
    return {
      type: BULK_TRANSLATION_ERROR,
      error,
    };
  }



  return {
    bulkTranslation,
    bulkTranslationSuccess,
    bulkTranslationError
  }
}

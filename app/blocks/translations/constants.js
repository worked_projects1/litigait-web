/*
 *
 * Translations ants
 *
 */


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {
    const url = `lg/${name}`;

    return {
        BULK_TRANSLATION: `${url}/BULK_TRANSLATION`,
        BULK_TRANSLATION_SUCCESS: `${url}/BULK_TRANSLATION_SUCCESS`,
        BULK_TRANSLATION_ERROR: `${url}/BULK_TRANSLATION_ERROR`
    }
}

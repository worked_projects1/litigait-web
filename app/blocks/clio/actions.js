/*
*
* Clio actions
*
*/


/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {

  const {
    CLIO_REDIRECT_AUTH_URI,
    DELETE_CLIO_SECRET,
    DELETE_CLIO_SECRET_SUCCESS,
    DELETE_CLIO_SECRET_ERROR,
    CLIO_SYNC_DATA,
    CLIO_SYNC_DATA_ERROR,
    CLIO_SYNC_DATA_SUCCESS,
    CLIO_UPLOAD_DOCUMENT,
    CLIO_UPLOAD_DOCUMENT_SUCCESS,
    CLIO_UPLOAD_DOCUMENT_ERROR,
    CLIO_UPDATE_USER_DETAILS,
    CLIO_UPDATE_USER_DETAILS_SUCCESS,
    CLIO_UPDATE_USER_DETAILS_ERROR
  } = constants;

/**
 * @param {boolean} load
 */
function clioRedirectAuthUrI(load, clioLoadRecord) {
  return {
    type: CLIO_REDIRECT_AUTH_URI,
    load,
    clioLoadRecord
  }
}

/**
 * @param {object} record
 */
function deleteClioSecret(record) {
  return {
    type: DELETE_CLIO_SECRET,
    record
  }
}

/**
 * 
 * @param {string} success 
 * @returns 
 */
function deleteClioSecretSuccess(success) {
  return {
    type: DELETE_CLIO_SECRET_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
function deleteClioSecretError(error) {
  return {
    type: DELETE_CLIO_SECRET_ERROR,
    error
  }
}


/**
 * 
 * @param {array} records 
 * @param {string} form 
 * @param {function} setClioPopup 
 * @param {function} setShowUserForm 
 * @returns 
 */
function clioSyncData(records, form, setClioPopup, setShowUserForm) {
  return {
    type: CLIO_SYNC_DATA,
    records,
    form,
    setClioPopup,
    setShowUserForm
  }
}

/**
 * 
 * @param {string} success 
 * @returns 
 */
function clioSyncDataSuccess(success) {
  return {
    type: CLIO_SYNC_DATA_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
function clioSyncDataError(error) {
  return {
    type: CLIO_SYNC_DATA_ERROR,
    error
  }
}

/**
 * 
 * @param {object} record 
 */
function clioUploadDocument(record) {
  return {
    type: CLIO_UPLOAD_DOCUMENT,
    record
  }
}

/**
 * 
 * @param {string} success 
 * @returns 
 */
function clioUploadDocumentSuccess(success) {
  return {
    type: CLIO_UPLOAD_DOCUMENT_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
function clioUploadDocumentError(error) {
  return {
    type: CLIO_UPLOAD_DOCUMENT_ERROR,
    error
  }
}

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {object} integration 
   * @returns 
   */
  function clioUpdateUserDetails(record, form, integration) {
    return {
      type: CLIO_UPDATE_USER_DETAILS,
      record,
      form,
      integration
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function clioUpdateUserDetailsSuccess(success) {
    return {
      type: CLIO_UPDATE_USER_DETAILS_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function clioUpdateUserDetailsError(error) {
    return {
      type: CLIO_UPDATE_USER_DETAILS_ERROR,
      error
    }
  }

  return {
    clioRedirectAuthUrI,
    deleteClioSecret,
    deleteClioSecretError,
    deleteClioSecretSuccess,
    clioSyncData,
    clioSyncDataError,
    clioSyncDataSuccess,
    clioUploadDocument,
    clioUploadDocumentSuccess,
    clioUploadDocumentError,
    clioUpdateUserDetails,
    clioUpdateUserDetailsSuccess,
    clioUpdateUserDetailsError
  }
}

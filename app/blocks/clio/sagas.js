/*
 *
 *  Clio sagas
 *
 */


import { push } from 'react-router-redux';
import { call, put, all, delay, takeLatest, select } from 'redux-saga/effects';
import store2 from 'store2';
import { destroy, startSubmit, stopSubmit } from 'redux-form/immutable';

import { verifySession as verifySessionAction } from 'blocks/session/actions';
import { verifySession as verifySessionApi } from 'blocks/session/remotes';

import {
} from 'utils/errors';

import records from 'blocks/records';
import { clioLoadRecordsSuccess } from 'blocks/session/actions';
import history from 'utils/history';
import { selectForm } from 'blocks/session/selectors';

const casesSaga = records('cases');
const { actions: casesActions, selectors: casesSelectors } = casesSaga;

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    CLIO_REDIRECT_AUTH_URI,
    DELETE_CLIO_SECRET,
    CLIO_SYNC_DATA,
    CLIO_UPLOAD_DOCUMENT,
    CLIO_UPDATE_USER_DETAILS
  } = constants;


  const {
    deleteClioSecretError,
    deleteClioSecretSuccess,
    clioSyncData: clioSyncDataAction,
    clioSyncDataError,
    clioSyncDataSuccess,
    clioUploadDocumentSuccess,
    clioUploadDocumentError,
    clioUpdateUserDetailsSuccess,
    clioUpdateUserDetailsError
  } = actions;


  const {
    deleteClioSecret,
    clioSyncData,
    clioUploadDocument,
    updateUserRecord
  } = remotes;


  function* clioRedirectAuthUriSaga() {
    yield takeLatest(CLIO_REDIRECT_AUTH_URI, function* updater({ load, clioLoadRecord=false }) {
      if (load) {
        store2.set('disable_clio_load_record', clioLoadRecord);
        window.location.href = `${process.env.CLIO_URL}/oauth/authorize?response_type=code&client_id=${process.env.CLIO_CLIENT_ID}&redirect_uri=${process.env.REDIRECT_URI}/clio`;
      }
    })
  }

  function* deleteClioSecretSaga() {
    yield takeLatest(DELETE_CLIO_SECRET, function* updater({ record }) {
      if(record) {
        try {
          const res = yield call(deleteClioSecret, record);
          if(res) {
            yield put(deleteClioSecretSuccess('Clio Secret Successfully Removed.'));
            yield delay(1000);
            const secret = store2.get('secret');
            yield put(verifySessionAction(secret));
          } 
        } catch (error) {
          yield put(deleteClioSecretError('Failed to delete secrets'));
        }
      }
    })
  }

  function* clioSyncDataSaga() {
    yield takeLatest(CLIO_SYNC_DATA, function* updater({ records, form, setClioPopup, setShowUserForm }) {
      if (records) {
        yield put(startSubmit(form));
        try {
          const user_info = yield call(verifySessionApi);
          if (user_info && (user_info?.role == 'lawyer' && !user_info.state_bar_number || !user_info.user_name) && setShowUserForm) {
            yield call(setShowUserForm, true);
            yield put(stopSubmit(form));
          } else {
            yield put(startSubmit(form));
            try {
              if (records && Array.isArray(records) && records.length > 0) {
                let loop = 0;
                let currentIndex = 1;
                let totalSize = 100;
                const totalSlice = records.length % totalSize == 0 ? Math.floor(records.length / totalSize) : Math.floor((records.length / totalSize) + 1);

                while (loop < totalSlice) {
                  const firstIndex = (currentIndex - 1) * totalSize;
                  const lastIndex = firstIndex + totalSize;
                  const saveRecords = records.slice(firstIndex, lastIndex);
                  const result = yield call(clioSyncData, saveRecords);

                  if (result) {
                    loop++;
                    currentIndex++;
                  } else {
                    yield put(clioSyncDataError('Failed to save records'));
                  }
                }
                yield put(stopSubmit(form));
                yield put(clioSyncDataSuccess('Clio data sync successfully'));
                yield delay(1000);
                yield put(push({ pathname: '/casesCategories/cases', state: Object.assign({}, { ...history.location.state }) }));
                yield put(casesActions.loadRecords(true));
                yield put(clioLoadRecordsSuccess([]));
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              }
            } catch (err) {
              yield put(clioSyncDataError('Failed to save records'));
              yield put(stopSubmit(form));
            } finally {
              if (setClioPopup) {
                yield call(setClioPopup, false);
              }
              yield put(destroy(form));
            }
          }
        } catch (error) {
          yield put(clioSyncDataError('Failed to save records'));
          yield put(stopSubmit(form));
        }
      }
    })
  }

  function* clioUploadDocumentSaga() {
    yield takeLatest(CLIO_UPLOAD_DOCUMENT, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(clioUploadDocument, record);
          if (result && result.status && result.status.toLowerCase() === 'ok') {
            yield put(clioUploadDocumentSuccess('Document Uploaded Successfully'));
          } else {
            yield put(clioUploadDocumentError('Failed to Upload Document'));
          }
        } catch (error) {
          yield put(clioUploadDocumentError('Failed to Upload Document'));
        }
      }
    })
  }

  function* clioUpdateUserDetailsSaga() {
    yield takeLatest(CLIO_UPDATE_USER_DETAILS, function* updater({ record, form, integration }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateUserRecord, record);
          if (result) {
            const { integrationPopup, integration_form, setShowUserForm } = integration;
            yield put(stopSubmit(form));
            yield call(setShowUserForm, false);
            const storeForms = yield select(selectForm());            
            const selectFormRecord = storeForms && storeForms[`${integration_form}`] && storeForms[`${integration_form}`].values && storeForms[`${integration_form}`]['values']['integrations'];
            if(selectFormRecord?.length > 0) {
                yield put(clioSyncDataAction(selectFormRecord, integration_form, integrationPopup));
            }
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to Update User Record';
          yield put(stopSubmit(form));
          yield put(clioUpdateUserDetailsError(Err));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      clioRedirectAuthUriSaga(),
      deleteClioSecretSaga(),
      clioSyncDataSaga(),
      clioUploadDocumentSaga(),
      clioUpdateUserDetailsSaga()
    ]);
  }

}
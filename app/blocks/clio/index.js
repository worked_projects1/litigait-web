/*
 *
 *  Clio History
 *
 */

import actions from './actions';
import constants from './constants';
import sagas from './sagas';
import remotes from './remotes';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function(name) {

  const actualConstants = constants(name);
  const actualActions = actions(actualConstants);
  const actualRemotes = remotes(name);
  let actualSagas = sagas(actualConstants, actualActions, actualRemotes, false, name);

  return {
    actions: actualActions,
    constants: actualConstants,
    saga: actualSagas,
    remotes: actualRemotes
  };

}
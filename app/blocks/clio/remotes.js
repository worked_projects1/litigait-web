/*
*
*  Clio remotes
*
*/

import api from 'utils/api';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function deleteClioSecret(record) {
        return api.delete(`/rest/integrations/clio/${record.id}`).then(res => res.data).catch(err => Promise.reject(err))
    }

    /**
     * @param {array} records
     */
    function clioSyncData(records) {
        return api.post('/rest/integrations/clio/create-client-and-case', records).then(res => res.data).catch(err => Promise.reject(err))
    }

    /**
     * @param {object} record
     */
    function clioUploadDocument(record) {
        return api.post('/rest/integrations/clio-file-upload', record).then(res => res.data).catch(err => Promise.reject(err))
    }

    /**
     * @param {object} record
     */
    function updateUserRecord(record) {
        return api.put(`/rest/user/update-details`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        deleteClioSecret,
        clioSyncData,
        clioUploadDocument,
        updateUserRecord
    }
}


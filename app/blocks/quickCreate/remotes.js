/*
 *
 *  QuickCreate remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * @param {object} record 
     */
    function uploadDocumentForm(record) {
        return api.post(`rest/document-extraction-progress/upload-private-file`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function generateDocumentForm(record) {
        return api.get(`${process.env.SCANNED_URL}/extract?key=${record.s3_file_key}&practice_id=${record.practice_id}&id=${record.id}&legalform_filename=${record.legalform_filename}&user_id=${record.user_id}&pdf_filename=${record.pdf_filename}&quickCreate=${record.quickCreate}&hash_id=${record.hash_id}&doc_textract_region=${record.doc_textract_region}&doc_textract_region_count=${record.doc_textract_region_count}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function saveDocumentForm(record) {
        return api.post(`legalforms/save-form-data/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function loadDocumentForm(record) {
        let params = `lawyer_response_status_filter=${record.lawyer_response_status_filter}&client_response_status_filter=${record.client_response_status_filter}&legalforms_id=${record.legalforms_id || ''}`;
        return api.get(`forms/data/${record.id}/${record.document_type}?${params}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateDocumentForm(record) {
        return api.put(`forms/data/${record.case_id}/${record.document_type}`, Object.assign({}, record, { lawyer_response_text: record.lawyer_response_text ? record.lawyer_response_text.replace(/[^\x00-\x7F]/g, "") : record.lawyer_response_text })).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function fetchDocumentForm(record) {
        return api.get(`forms/data/${record.id}/${record.document_type}?lawyer_response_status_filter=${record.lawyer_response_status_filter}&client_response_status_filter=${record.client_response_status_filter}&legalforms_id=${record.legalforms_id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function getDocumentStatus(record) {
        return api.get(`rest/document-extraction-progress/get-status?id=${record.id}&user_id=${record.user_id}&practice_id=${record.practice_id}`).then((response) => response.data && response.data.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function setDocumentStatus(record) {
        return api.put(`rest/document-extraction-progress/set-status`, record).then((response) => response.data && response.data.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function saveDocumentDetails(record) {
        return api.post('rest/document-extraction-progress/save-extraction-form', record).then((response) => response.data).catch((error) => Promise.reject(error))
    }

    /**
     * @param {object} record 
     */
    function extractionRequestHelp(record) {
        return api.put(`rest/document-extraction-progress/help-request/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function cancelDocumentForm(record) {
        return api.put(`rest/document-extraction-progress/set-status`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function saveClientCaseInfo(record){
        return api.post(`rest/document-extraction-support`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function saveExtractionQuestions(record) {
        return api.put(`/rest/document-extraction-progress/${record.legalforms_id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function saveCaseForm(record) {
        return api.post(`/rest/legalforms/quick-create-forms-create`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {string} id 
     */
    function destoryQuickCreateDocument(id) {
        return api.delete(`/rest/document-extraction-progress/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function hashedFileQuestions(record) {
        return api.post(`/rest/hash/file-hashing`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }


    /**
     * @param {string} id
     */
    function updateQCNotificationStatus(id) {
        return api.put(`/rest/users/qc-notification/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        uploadDocumentForm,
        generateDocumentForm,
        cancelDocumentForm,
        saveDocumentForm,
        loadDocumentForm,
        updateDocumentForm,
        fetchDocumentForm,
        getDocumentStatus,
        setDocumentStatus,
        saveDocumentDetails,
        extractionRequestHelp,
        saveClientCaseInfo,
        saveExtractionQuestions,
        saveCaseForm,
        destoryQuickCreateDocument,
        hashedFileQuestions,
        updateQCNotificationStatus
    }

}

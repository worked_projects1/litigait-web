/*
 *
 *  QuickCreate sagas
 *
 */

import { push, replace } from 'react-router-redux';
import { call, put, all, select, delay, takeLatest } from 'redux-saga/effects';
import { startSubmit, stopSubmit, destroy } from 'redux-form/immutable';
import history from 'utils/history';
import { getDuplicates, TimeOutArr } from 'utils/tools';
import { AsYouType } from 'libphonenumber-js';
import schema from 'routes/schema';
import store2 from 'store2';
import { verifySession } from '../session/actions';

import {
	DEFAULT_CANCEL_FORM_ERROR,
	DEFAULT_LOAD_FORM_ERROR,
	DEFAULT_DOCUMENT_ERROR,
	DEFAULT_DOCUMENT_CONTENT_NOT_FOUND_ERROR,
	DEFAULT_REQUEST_HELP_ERROR,
	DEFAULT_SAVE_QUESTIONS_FORM_ERROR,
	DEFAULT_SAVE_QUESTIONS_FORM_VALIDATE_ERROR
} from 'utils/errors';

import { uploadFile } from 'utils/api';

import { selectForm } from 'blocks/session/selectors';

import records from 'blocks/records';

const quickCreateSaga = records('rest/document-extraction-progress');
const { actions: quickCreateActions, selectors: quickCreateSelectors } = quickCreateSaga;
const { selectRecord, selectRecords } = quickCreateSelectors;
const { updateRecordSuccess, loadRecords } = quickCreateActions;
const standardFormColumns = schema().cases().standardForm;
const supportRequestSaga = records('rest/help-request');

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

	const {
		UPLOAD_DOCUMENT_FORM,
		GENERATE_DOCUMENT_FORM,
		SAVE_DOCUMENT_DETAILS_SUCCESS,
    	LOAD_DOCUMENT_FORM,
		CANCEL_DOCUMENT_FORM,
		EXTRACTION_REQUEST_HELP,
		SAVE_CLIENT_CASE_INFO,
		SAVE_EXTRACTION_QUESTIONS,
		SAVE_DOCUMENT_QUESTIONS,
		SAVE_DOCUMENT_INFO_QUESTIONS,
		DESTORY_QUICK_CREATE_DOCUMENT,
		UPDATE_QUICK_CREATE_NOTIFICATION
	} = constants;

	const {
		uploadDocumentFormError,
		generateDocumentForm: generateDocumentFormAction,
		generateDocumentFormError,
		cancelDocumentFormError,
		saveDocumentDetailsError,
		loadDocumentForm: loadDocumentFormAction,
		loadDocumentFormError,
		loadDocumentFormSuccess,
		updateKey,
		documentContentNotFound,
		clearRecord,
		extractionRequestHelpError,
		extractionRequestHelpSuccess,
		saveClientCaseInfoError,
		saveClientCaseInfoSuccess,
		saveExtractionQuestionsError,
		saveExtractionQuestionsSuccess,
		saveDocumentQuestionsSuccess,
		saveDocumentQuestionsError,
		saveDocumentInfoQuestionsError,
		destoryQuickCreateDocumentError,
		updateQuickCreateNotificationSuccess,
		updateQuickCreateNotificationError
	} = actions;


	const {
		uploadDocumentForm,
		getDocumentStatus,
		saveDocumentDetails,
		generateDocumentForm,
		cancelDocumentForm,
		loadDocumentForm,
		extractionRequestHelp,
		saveClientCaseInfo,
		saveExtractionQuestions,
		saveCaseForm,
		destoryQuickCreateDocument,
		hashedFileQuestions,
		updateQCNotificationStatus
	} = remotes;



	function* uploadDocumentFormSaga() {
		yield takeLatest(UPLOAD_DOCUMENT_FORM, function* updater({ record, setProgress, form }) {
			if (record) {
				yield put(startSubmit(form));
				try {
					yield put(updateKey(0));
					yield put(clearRecord({}));
					yield put(updateRecordSuccess({}));
					yield delay(1000);
					
					const result = yield call(uploadDocumentForm, Object.assign({}, { ...record }));
					if (result) {
						yield call(uploadFile, result.uploadURL, record.document_file[0], record.content_type);
						yield put(updateKey(1));
						yield put(updateRecordSuccess(Object.assign({}, { id: result?.extraction_data?.id, document_s3_key: result.s3_file_key, practice_id: record.practice_id, user_id: record.user_id })));
						yield put(generateDocumentFormAction(Object.assign({}, { id: result?.extraction_data?.id,  s3_file_key: result.s3_file_key, legalform_filename: record.legalform_filename, practice_id: record.practice_id, user_id: record.user_id, quickCreate: true, pdf_filename: record.pdf_filename, publicUrl: result.public_url, doc_textract_region : result?.doc_textract_region, doc_textract_region_count : result?.doc_textract_region_count }), setProgress));
						yield put(stopSubmit(form));
					} else {
						yield put(uploadDocumentFormError(DEFAULT_DOCUMENT_ERROR));
						yield put(stopSubmit(form, { _error: DEFAULT_DOCUMENT_ERROR }));
					}
				} catch (error) {
					yield put(uploadDocumentFormError(DEFAULT_DOCUMENT_ERROR));
					yield put(stopSubmit(form, { _error: DEFAULT_DOCUMENT_ERROR }));
				} finally {
					yield put(destroy(form));
				}
			}
		})
	}

	function* generateDocumentFormSaga() {
		yield takeLatest(GENERATE_DOCUMENT_FORM, function* updater({ record, setProgress }) {
			if (record) {
				try {
					const hashedFileQuestionsResult = yield call(hashedFileQuestions, Object.assign({}, record, { s3_file_key: record?.s3_file_key, uploaded_type: "QuickCreate", filename: record?.pdf_filename }));
					const hashId = hashedFileQuestionsResult?.['hashResponse']?.id || false;
					
					if (hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'true') {
						const quickCreateId = hashedFileQuestionsResult?.['hashResponse']?.quick_create_id || false;
						if (quickCreateId) {
							yield put(push({ pathname: `/quickCreate/${quickCreateId}/reviewDocument`, state: Object.assign({}, { ...history.location.state }) }));
							yield put(updateKey(3));
							yield put(setProgress(false));
						}else {
							yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
						}
					}
					else if (hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'false') {
						const result = yield call(generateDocumentForm, Object.assign({}, record, { hash_id: hashId }));
						if (result) {
							let timeIntervalArr = TimeOutArr(result);

							yield put(updateKey(2));
							if (result?.document_modal === 'scanned') {
								var i = 0;

								// Delays the dispatch of select record untill the store is populated with an initial list of Questions.
								while (i < timeIntervalArr.length) {
									if (i == 0) {
										yield delay(10000);
									}
									const DocumentStatus = yield call(getDocumentStatus, record);
									yield delay(timeIntervalArr[i]);
									yield put(documentContentNotFound(false));

									if (DocumentStatus) {
										const uploading_status = DocumentStatus && DocumentStatus.status || false;
										if (uploading_status && uploading_status === 'Content_not_found') {
											yield put(documentContentNotFound(true));
											yield put(stopSubmit(`upload_document_form`));
											yield put(generateDocumentFormError(DEFAULT_DOCUMENT_CONTENT_NOT_FOUND_ERROR));
											return;
										}

										if (uploading_status && uploading_status === 'Completed') {

											yield put(push({ pathname: `/quickCreate/${DocumentStatus.id}/reviewDocument`, state: Object.assign({}, { ...history.location.state }) }));
											yield put(updateKey(3));
											yield put(setProgress(false));
											return;
										} else if (uploading_status && uploading_status === 'cancelled') {
											return;
										} else if (uploading_status && uploading_status === 'Failed') {
											yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
											return;
										} else if ((i == (timeIntervalArr.length - 1)) || (uploading_status && uploading_status === 'Failed')) {
											yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
											return;
										}

									} else {
										yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
										return;
									}

									i++;
								}

							} else {
								yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
							}
						} else {
							yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
						}
					} 
					else {
						yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
					}
				} catch (error) {
					yield put(generateDocumentFormError(DEFAULT_DOCUMENT_ERROR));
				}
			}
		})
	}

	function* cancelDocumentFormSaga() {
		yield takeLatest(CANCEL_DOCUMENT_FORM, function* updater({ record, loadRecords }) {
			if (record) {
				try {
					const result = yield call(cancelDocumentForm, record);

					if (result) {
						yield put(loadRecords(true));
					} else {
						yield put(cancelDocumentFormError(DEFAULT_CANCEL_FORM_ERROR));
						yield put(loadRecords(true));
					}
				} catch (error) {
					yield put(cancelDocumentFormError(DEFAULT_CANCEL_FORM_ERROR));
					yield put(loadRecords(true));
				}
			}
		})
	}
	
	function* loadDocumentFormSaga() {
		yield takeLatest(LOAD_DOCUMENT_FORM, function* updater({ record, setFormLoader }) {
			if (record) {
				try {
				const result = yield call(loadDocumentForm, record);
				if (result?.questions) {
					// Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
					while (true) { // eslint-disable-line no-constant-condition
						const recordsInStore = yield select(selectRecords());
						if (recordsInStore && recordsInStore.length > 0) {
							break;
						}
						yield delay(500);
					}
					yield put(loadDocumentFormSuccess(Object.assign({}, { id: record.document_id, questions: result?.questions })));
					if (setFormLoader) {
						yield delay(500);
						yield call(setFormLoader, false);
					}
				} else {
					yield put(loadDocumentFormError(DEFAULT_LOAD_FORM_ERROR));
				}
				} catch (error) {
				yield put(loadDocumentFormError(DEFAULT_LOAD_FORM_ERROR));
				}
			}
		})
  	}
	
	function* saveDocumentDetailsSaga() {
		yield takeLatest(SAVE_DOCUMENT_DETAILS_SUCCESS, function* updater({ record, form }) {
			if (record) {
				yield put(startSubmit(form));
				try {
					const storeForms = yield select(selectForm());
					const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
						if (key.indexOf('documentDetailForm_') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']) {
						arr.push(storeForms[key]['values'])
						}
						return arr;
					}, []) || false;
				
					let formRecords = selectedForm.reduce((acc, el) => {
						if (el) {
						  acc = Object.assign({}, acc, el);
						}
						return acc;
					}, {});
					
					if(formRecords.phone) {
						const phoneType = new AsYouType('US');
						phoneType.input(formRecords.phone);
						const phoneVal = phoneType.getNumber().nationalNumber;
						formRecords.phone = phoneVal || formRecords.phone;
					}
					const submitRecord = Object.assign({}, { ...record }, 
						{ 
							extraction_id: record.id, 
							client_info: {
								first_name: formRecords.first_name,
								last_name: formRecords.last_name,
								middle_name: formRecords.middle_name,
								phone: formRecords.phone, 
								email: formRecords?.email || null, 
								dob: formRecords?.dob
							}, 
							case_info: { 
								case_title: formRecords.case_title, 
								case_plaintiff_name: formRecords.case_plaintiff_name, 
								county: formRecords.county, 
								case_number: formRecords.case_number, 
								case_defendant_name: formRecords.case_defendant_name, 
								state: formRecords.state, 
								attorneys: record.attorneys || formRecords.attorneys
							},
							document_type: formRecords?.document_type,
							set_number: formRecords?.set_number,
							opposing_counsel: formRecords.opposing_counsel || false,
						}
					)


					const result = yield call(saveDocumentDetails, submitRecord);
					if(result) {
						yield put(loadDocumentFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: result?.document_type.toUpperCase(), id: result.case_id, document_id: record.id, lawyer_response_status_filter: "All", legalforms_id: result.legalforms_id })));
						yield delay(4000);
						while (true) { // eslint-disable-line no-constant-condition
							const storeRecord = yield select(selectRecord(record.id));
							if (storeRecord && storeRecord.questions) {
								const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
									if (!q && storeRecord.questions && storeRecord.questions.length > 0) {
										const arr = storeRecord.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
										if (arr)
										q = nc;
									}
									return q;
								}, false);
								const FormColumns = DiscType ? standardFormColumns[DiscType] : [];
								if (result?.document_type?.toLowerCase() === 'frogs') {
									yield put(replace({ pathname: `/quickCreate/${record.id}/standardForm`, state: Object.assign({}, { ...history.location.state }, { document_type: result?.document_type, legalform_id: result?.legalforms_id, questions: FormColumns, filename: record.legalform_filename, defaultQuestions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [] }) }));
								} else {
									yield put(replace({ 
										pathname: `/quickCreate/${record.id}/questionsForm`, 
										state: Object.assign({}, { ...history.location.state }, { document_type: result?.document_type, legalform_id: result?.legalforms_id, questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [], 
										filename: record.legalform_filename }) 
									}));
								}
								let i = 0;
								while (i < selectedForm.length) {
									yield put(destroy(`documentDetailForm_${record.id}_${i}`));
									i++;
								}
								
								break;
							}
							yield delay(500);
						}
					}
					yield put(stopSubmit(form));
				} catch (error) {
					yield put(stopSubmit(form));
					yield put(saveDocumentDetailsError('Failed to save records'));
				}
			}
		})
	}

	function* extractionRequestHelpSaga() {
		yield takeLatest(EXTRACTION_REQUEST_HELP, function* updater({ record, stepperLoader }) {
			if (record) {
				try {
					const result = yield call(extractionRequestHelp, record);
					if (result) {
						yield put(extractionRequestHelpSuccess('Support request sent to EsquireTek. Our support team will extract questions from this document and update the system. You will receive an email once questions are updated.'));
						yield call(cancelDocumentForm, Object.assign({}, { status: 'Failed', id: record.id, practice_id: record.practice_id, user_id: record.user_id }));
					} else {
						yield put(extractionRequestHelpError(DEFAULT_REQUEST_HELP_ERROR));
					}
				} catch (error) {
					yield put(extractionRequestHelpError(DEFAULT_REQUEST_HELP_ERROR));
				} finally {
					if (stepperLoader) {
						yield call(stepperLoader, false);
					}
				}
			}
		})
	}

	function* saveClientCaseInfoSaga() {
		yield takeLatest(SAVE_CLIENT_CASE_INFO, function* updater({ record, clientCaseModal, loadRecord, form }) {
			if (record) {
				try {
					const result = yield call(saveClientCaseInfo, record);
					if (result) {
						yield call(clientCaseModal, false);
						yield put(saveClientCaseInfoSuccess('Client and Case details saved successfully'));
						yield delay(1000);
						yield put(loadRecord(record.extraction_id));
						yield put(destroy(form));
					} else {
						yield put(saveClientCaseInfoError('Failed to save records'));
					}
				} catch (error) {
					yield put(saveClientCaseInfoError('Failed to save records'));
				}
			}
		})
	}

	function* saveExtractionQuestionsSaga() {
		yield takeLatest(SAVE_EXTRACTION_QUESTIONS, function* updater({ record }) {
		  if (record) {
            const submitData = Object.assign({}, { practice_id: record.practice_id, case_id: record.case_id, client_id: record.client_id, file_name: record.pdf_filename, questions: record.questions, document_type: record.document_type, content_type: 'application/pdf', from: 'questions_form', form: 'questionsForm', action_by: record.role, legalforms_id: record.legalforms_id, extraction_id: record.id, filename: record.legalform_filename, set_number: record.set_number });
			let { questions = [], form } = submitData;
			let duplicate;
			try {
			  questions = typeof questions === 'string' && JSON.parse(questions) || questions;
			  if (form && form === 'questionsForm') {
				duplicate = getDuplicates(questions.map(_ => { return _.question_number }));
			  }
			  const validate = questions.filter(_ => _.question_text === '' || _.question_number === '' || _.question_number_text === '');
	
			  if (validate && validate.length === 0) {
				if (duplicate && duplicate != '' && form && form === 'questionsForm') {
				  yield put(saveExtractionQuestionsError(`Question Number ${duplicate} duplicated`));
				} else {
				  const result = submitData;
				  if (result) {
					const upload = yield call(saveExtractionQuestions, Object.assign({}, submitData, { questions }));
					if (upload) {
					  yield put(saveExtractionQuestionsSuccess("Questions Saved"));
					  yield put(supportRequestSaga.actions.loadRecords(true));
					  yield put(push({ pathname: `/supportCategories/rest/document-extraction-support`, state: history.location.state }));
					} else {
					  yield put(saveExtractionQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
					}
				  } else {
					yield put(saveExtractionQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
				  }
				}
			  } else {
				yield put(saveExtractionQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_VALIDATE_ERROR));
			  }
			} catch (error) {
			  yield put(saveExtractionQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
			}
		  }
		})
	}


	function* saveDocumentQuestionsSaga() {
		yield takeLatest(SAVE_DOCUMENT_QUESTIONS, function* updater({ record}) {
			if (record) {
				try {
					const result = yield call(saveCaseForm, record);
					if(result) {
						yield put(loadDocumentFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: result?.document_type.toUpperCase(), id: result.case_id, document_id: record.id, lawyer_response_status_filter: "All", legalforms_id: result.legalforms_id })));
						yield delay(4000);
						while (true) { // eslint-disable-line no-constant-condition
							const storeRecord = yield select(selectRecord(record.id));
							if (storeRecord && storeRecord.questions) {
								const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
									if (!q && storeRecord.questions && storeRecord.questions.length > 0) {
										const arr = storeRecord.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
										if (arr)
										q = nc;
									}
									return q;
								}, false);
								
								const FormColumns = DiscType ? standardFormColumns[DiscType] : [];
								yield put(saveDocumentQuestionsSuccess('success'));
								if (result?.document_type?.toLowerCase() === 'frogs') {
									yield put(replace({ pathname: `/quickCreate/${record.id}/standardForm`, state: Object.assign({}, { ...history.location.state }, { document_type: result?.document_type, legalform_id: result?.legalforms_id, questions: FormColumns, filename: record.legalform_filename, defaultQuestions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [] }) }));
								} else {
									yield put(replace({ 
										pathname: `/quickCreate/${record.id}/questionsForm`, 
										state: Object.assign({}, { ...history.location.state }, { document_type: result?.document_type, legalform_id: result?.legalforms_id, questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [], 
										filename: record.legalform_filename }) 
									}));
								}
								yield delay(500);
								break;
							}
						}
					}

				} catch (error) {
					yield put(saveDocumentQuestionsError('Failed to save records'));
				}
			}
		})
	}


	function* saveDocumentInfoQuestionsSaga() {
		yield takeLatest(SAVE_DOCUMENT_INFO_QUESTIONS, function* updater({ record, form }) {
			if (record) {
				yield put(startSubmit(form));
				try {
					const result = yield call(saveCaseForm, record);
					if(result) {
						yield put(loadDocumentFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: result?.document_type.toUpperCase(), id: result.case_id, document_id: record.id, lawyer_response_status_filter: "All", legalforms_id: result.legalforms_id })));
						yield delay(4000);
						while (true) { // eslint-disable-line no-constant-condition
							const storeRecord = yield select(selectRecord(record.id));
							if (storeRecord && storeRecord.questions) {
								const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
									if (!q && storeRecord.questions && storeRecord.questions.length > 0) {
										const arr = storeRecord.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
										if (arr)
										q = nc;
									}
									return q;
								}, false);
								const FormColumns = DiscType ? standardFormColumns[DiscType] : [];
								if (result?.document_type?.toLowerCase() === 'frogs') {
									yield put(replace({ pathname: `/quickCreate/${record.id}/standardForm`, state: Object.assign({}, { ...history.location.state }, { document_type: result?.document_type, legalform_id: result?.legalforms_id, questions: FormColumns, filename: record.legalform_filename, defaultQuestions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [] }) }));
								} else {
									yield put(replace({ 
										pathname: `/quickCreate/${record.id}/questionsForm`, 
										state: Object.assign({}, { ...history.location.state }, { document_type: result?.document_type, legalform_id: result?.legalforms_id, questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [], 
										filename: record.legalform_filename }) 
									}));
								}
								yield put(stopSubmit(form));
								yield put(destroy(form));
								break;
							}
						}
					}
					
				} catch (error) {
					yield put(stopSubmit(form));
					yield put(saveDocumentInfoQuestionsError('Failed to save records'));
				}
			}
		})
	}


	function* destoryQuickCreateDocumentSaga() {
		yield takeLatest(DESTORY_QUICK_CREATE_DOCUMENT, function* updater({ record, form }) {
			if (record) {
				try {
					yield call(destoryQuickCreateDocument, record.id);
					if(form) {
						yield put(destroy(form));
					}
					yield put(replace({ pathname: '/quickCreate', state: Object.assign({}, { ...history.location.state }) }));
					yield put(loadRecords(true));
				} catch (error) {
					yield put(destoryQuickCreateDocumentError('Failed to delete the Document details'));
					yield put(delay(500));
					yield put(replace({ pathname: '/quickCreate', state: Object.assign({}, { ...history.location.state }) }));
					yield put(loadRecords(true));
				}
			}
		})
	}

	function* updateQCNotificationStatusSagas() {
		yield takeLatest(UPDATE_QUICK_CREATE_NOTIFICATION, function* updater({ userId }) {
			try {
				const result = yield call(updateQCNotificationStatus, userId);
				if(result){
					yield put(updateQuickCreateNotificationSuccess("Updated Successfully"));
					// Calling Verifysession for get updated qc notification status
					const secret = store2.get('secret');
					yield put(verifySession(secret));
				}
				else{
					yield put(updateQuickCreateNotificationError("Failed to save"));
				}
			
			} catch (error) {
				yield put(updateQuickCreateNotificationError("Failed to save"));
			}
		})
	}


	return function* rootSaga() {
		yield all([
			uploadDocumentFormSaga(),
			generateDocumentFormSaga(),
			cancelDocumentFormSaga(),
			loadDocumentFormSaga(),
			saveDocumentDetailsSaga(),
			extractionRequestHelpSaga(),
			saveClientCaseInfoSaga(),
			saveExtractionQuestionsSaga(),
			saveDocumentQuestionsSaga(),
			saveDocumentInfoQuestionsSaga(),
			destoryQuickCreateDocumentSaga(),
			updateQCNotificationStatusSagas()
		]);
	}
}



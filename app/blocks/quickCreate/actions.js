/*
 *
 * QuickCreate  actions
 *
 */

/**
 *
 * @param {object} constants
 * @returns
 */
export default function actions(constants) {
	const {
		UPLOAD_DOCUMENT_FORM,
		UPLOAD_DOCUMENT_FORM_ERROR,
		GENERATE_DOCUMENT_FORM,
		GENERATE_DOCUMENT_FORM_ERROR,
		CANCEL_DOCUMENT_FORM,
		CANCEL_DOCUMENT_FORM_ERROR,
		SAVE_DOCUMENT_DETAILS_SUCCESS,
		SAVE_DOCUMENT_DETAILS_SUCCESS_SUCCESS,
		SAVE_DOCUMENT_DETAILS_SUCCESS_ERROR,
		LOAD_DOCUMENT_FORM,
		LOAD_DOCUMENT_FORM_ERROR,
		LOAD_DOCUMENT_FORM_SUCCESS,
		UPDATE_DOCUMENT_FORM,
		UPDATE_DOCUMENT_FORM_ERROR,
		UPDATE_DOCUMENT_FORM_SUCCESS,
		UPDATE_KEY,
		DOCUMENT_CONTENT_NOT_FOUND,
		CLEAR_RECORD,
		EXTRACTION_REQUEST_HELP,
		EXTRACTION_REQUEST_HELP_ERROR,
		EXTRACTION_REQUEST_HELP_SUCCESS,
		SAVE_CLIENT_CASE_INFO,
		SAVE_CLIENT_CASE_INFO_ERROR,
		SAVE_CLIENT_CASE_INFO_SUCCESS,
		SAVE_EXTRACTION_QUESTIONS,
		SAVE_EXTRACTION_QUESTIONS_SUCCESS,
		SAVE_EXTRACTION_QUESTIONS_ERROR,
		SAVE_DOCUMENT_QUESTIONS,
		SAVE_DOCUMENT_QUESTIONS_SUCCESS,
		SAVE_DOCUMENT_QUESTIONS_ERROR,
		SAVE_DOCUMENT_INFO_QUESTIONS,
		SAVE_DOCUMENT_INFO_QUESTIONS_SUCCESS,
		SAVE_DOCUMENT_INFO_QUESTIONS_ERROR,
		DESTORY_QUICK_CREATE_DOCUMENT,
		DESTORY_QUICK_CREATE_DOCUMENT_SUCCESS,
		DESTORY_QUICK_CREATE_DOCUMENT_ERROR,
		SHOW_QUICK_CREATE_DESCRIPTION,
		UPDATE_QUICK_CREATE_NOTIFICATION,
		UPDATE_QUICK_CREATE_NOTIFICATION_SUCCESS,
		UPDATE_QUICK_CREATE_NOTIFICATION_ERROR
	} = constants;

	/**
	 * @param {object} record
	 * @param {function} setProgress
	 * @param {string} form
	 */
	function uploadDocumentForm(record, setProgress, form) {
		return {
			type: UPLOAD_DOCUMENT_FORM,
			record,
			setProgress,
			form
		};
	}

	/**
	 * @param {string} error
	 */
	function uploadDocumentFormError(error) {
		return {
			type: UPLOAD_DOCUMENT_FORM_ERROR,
			error,
		};
	}

	/**
	 * @param {object} record
	 * @param {function} setProgress
	 */
	function generateDocumentForm(record, setProgress) {
		return {
			type: GENERATE_DOCUMENT_FORM,
			record,
			setProgress
		};
	}

	/**
	 * @param {string} error
	 */
	function generateDocumentFormError(error) {
		return {
			type: GENERATE_DOCUMENT_FORM_ERROR,
			error,
		};
	}


	/**
	 * 
	 * @param {boolean} content 
	 * @returns 
	 */
	function documentContentNotFound(content) {
		return {
		  type: DOCUMENT_CONTENT_NOT_FOUND,
		  content
		}
	  }

	/**
	 * @param {object} record
	 * @param {function} loadRecords
	 */
	function cancelDocumentForm(record, loadRecords) {
		return {
			type: CANCEL_DOCUMENT_FORM,
			record,
			loadRecords,
		};
	}

	/**
	 * @param {string} error
	 */
	function cancelDocumentFormError(error) {
		return {
			type: CANCEL_DOCUMENT_FORM_ERROR,
			error,
		};
	}

	/**
	 * @param {object} record 
	 * @param {string} form 
	 */
	function saveDocumentDetails(record, form) {
		return {
			type: SAVE_DOCUMENT_DETAILS_SUCCESS,
			record,
			form
		}
  	}
  
	/**
	 * @param {string} success 
	 */
	function saveDocumentDetailsSuccess(success) {
		return {
			type: SAVE_DOCUMENT_DETAILS_SUCCESS_SUCCESS,
			success
		}
  	}
	
	/**
	 * 
	 * @param {string} error 
	 * @returns 
	 */
	function saveDocumentDetailsError(error) {
		return {
			type: SAVE_DOCUMENT_DETAILS_SUCCESS_ERROR,
			error
		}
  	}

	/**
	 * @param {object} record
	 * @param {function} setFormLoader
	 * @returns
	 */
	function loadDocumentForm(record, setFormLoader) {
		return {
			type: LOAD_DOCUMENT_FORM,
			record,
			setFormLoader,
		};
	}

	/**
	 * @param {string} error
	 */
	function loadDocumentFormError(error) {
		return {
			type: LOAD_DOCUMENT_FORM_ERROR,
			error,
		};
	}

	/**
	 * @param {object} record
	 */
	function loadDocumentFormSuccess(record) {
		return {
			type: LOAD_DOCUMENT_FORM_SUCCESS,
			record,
		};
	}

	/**
	 *
	 * @param {object} record
	 * @param {string} form
	 * @param {function} closeForm
	 * @param {boolean} subGroupForm
	 * @returns
	 */
	function updateDocumentForm(record, form, closeForm, subGroupForm) {
		return {
			type: UPDATE_DOCUMENT_FORM,
			record,
			form,
			closeForm,
			subGroupForm,
		};
	}

	/**
	 * @param {string} error
	 */
	function updateDocumentFormError(error) {
		return {
			type: UPDATE_DOCUMENT_FORM_ERROR,
			error,
		};
	}

	/**
	 * @param {object} record
	 */
	function updateDocumentFormSuccess(record) {
		return {
			type: UPDATE_DOCUMENT_FORM_SUCCESS,
			record,
		};
	}

	/**
	 * @param {number} key
	 */
	function updateKey(key) {
		return {
			type: UPDATE_KEY,
			key,
		};
	}


	function clearRecord(record) {
		return {
			type: CLEAR_RECORD,
			record
		}
	}


	/**
	 * @param {object} record
   	 * @param {function} stepperLoader
	 * @returns
	 */
	function extractionRequestHelp(record, stepperLoader) {
		return {
			type: EXTRACTION_REQUEST_HELP,
			record,
			stepperLoader
		}
	}

	/**
	 * @param {string} error
	 */
	function extractionRequestHelpError(error) {
		return {
			type: EXTRACTION_REQUEST_HELP_ERROR,
			error
		}
	}

	/**
	 * @param {string} success 
	 */
	function extractionRequestHelpSuccess(success) {
		return {
			type: EXTRACTION_REQUEST_HELP_SUCCESS,
			success
		};
	}

	/**
	 * @param {object} record
	 * @param {function} clientCaseModal
	 * @param {function} loadRecord
	 * @param {string} form
	 * @returns
	 */
	function saveClientCaseInfo(record, clientCaseModal, loadRecord, form) {
		return {
			type: SAVE_CLIENT_CASE_INFO,
			record,
			clientCaseModal,
			loadRecord,
			form
		}
	}

	/**
	 * @param {string} error
	 */
	function saveClientCaseInfoError(error) {
		return {
			type: SAVE_CLIENT_CASE_INFO_ERROR,
			error
		}
	}

	/**
	 * @param {string} success 
	 */
	function saveClientCaseInfoSuccess(success) {
		return {
			type: SAVE_CLIENT_CASE_INFO_SUCCESS,
			success
		};
	}

	/**
	 * @param {object} record
	 */
	function saveExtractionQuestions(record) {
		return {
			type: SAVE_EXTRACTION_QUESTIONS,
			record
		}
	}

	/**
	 * @param {string} error
	 */
	function saveExtractionQuestionsError(error) {
		return {
			type: SAVE_EXTRACTION_QUESTIONS_ERROR,
			error
		}
	}

	/**
	 * @param {string} success 
	 */
	function saveExtractionQuestionsSuccess(success) {
		return {
			type: SAVE_EXTRACTION_QUESTIONS_SUCCESS,
			success
		};
	}
	

	/**
	 * 
	 * @param {object} record 
	 * @returns 
	 */
	function saveDocumentQuestions(record) {
		return {
			type: SAVE_DOCUMENT_QUESTIONS,
			record
		}
	}
	
	/**
	 * 
	 * @param {string} success 
	 * @returns 
	 */
	function saveDocumentQuestionsSuccess(success) {
		return {
			type: SAVE_DOCUMENT_QUESTIONS_SUCCESS,
			success
		}
	}
	
	/**
	 * 
	 * @param {string} error 
	 * @returns 
	 */
	function saveDocumentQuestionsError(error) {
		return {
			type: SAVE_DOCUMENT_QUESTIONS_ERROR,
			error
		}
	}
	
	
	/**
	 * 
	 * @param {object} record 
	 * @param {string} form 
	 * @returns 
	 */
	function saveDocumentInfoQuestions(record, form) {
		return {
			type: SAVE_DOCUMENT_INFO_QUESTIONS,
			record,
			form
		}
	}
	
	/**
	 * 
	 * @param {string} success 
	 * @returns 
	 */
	function saveDocumentInfoQuestionsSuccess(success) {
		return {
			type: SAVE_DOCUMENT_INFO_QUESTIONS_SUCCESS,
			success
		}
	}
	
	/**
	 * 
	 * @param {string} error 
	 * @returns 
	 */
	function saveDocumentInfoQuestionsError(error) {
		return {
			type: SAVE_DOCUMENT_INFO_QUESTIONS_ERROR,
			error
		}
	}

	/**
	 * 
	 * @param {object} record 
	 * @param {string} form 
	 * @returns 
	 */
	function destoryQuickCreateDocument(record, form) {
		return {
			type: DESTORY_QUICK_CREATE_DOCUMENT,
			record,
			form
		}
	}
	
	/**
	 * 
	 * @param {string} success 
	 * @returns 
	 */
	function destoryQuickCreateDocumentSuccess(success) {
		return {
			type: DESTORY_QUICK_CREATE_DOCUMENT_SUCCESS,
			success
		}
	}
	
	/**
	 * 
	 * @param {string} error 
	 * @returns 
	 */
	function destoryQuickCreateDocumentError(error) {
		return {
			type: DESTORY_QUICK_CREATE_DOCUMENT_ERROR,
			error
		}
	}

	/**
	 * 
	 * @param {boolean} showQuickCreateDescription 
	 * @returns 
	 */
	function showQuickCreateDescription(showQuickCreateDescription) {
		return {
			type: SHOW_QUICK_CREATE_DESCRIPTION,
			showQuickCreateDescription
		}
	}

	/**
	 * 
	 * @param {boolean} showQuickCreateDescription 
	 * @returns 
	 */
	function updateQuickCreateNotification(userId) {
		return {
			type: UPDATE_QUICK_CREATE_NOTIFICATION,
			userId
		}
	}

	/**
	 * 
	 * @param {string} success 
	 * @returns 
	 */
	function updateQuickCreateNotificationSuccess(success) {
		return {
			type: UPDATE_QUICK_CREATE_NOTIFICATION_SUCCESS,
			success
		}
	}

	/**
	 * 
	 * @param {string} error 
	 * @returns 
	 */
	function updateQuickCreateNotificationError(error) {
		return {
			type: UPDATE_QUICK_CREATE_NOTIFICATION_ERROR,
			error
		}
	}

	return {
		uploadDocumentForm,
		uploadDocumentFormError,
		generateDocumentForm,
		generateDocumentFormError,
		documentContentNotFound,
		cancelDocumentForm,
		cancelDocumentFormError,
		saveDocumentDetails,
		saveDocumentDetailsSuccess,
		saveDocumentDetailsError,
		loadDocumentForm,
		loadDocumentFormError,
		loadDocumentFormSuccess,
		updateDocumentForm,
		updateDocumentFormError,
		updateDocumentFormSuccess,
		updateKey,
		clearRecord,
		extractionRequestHelp,
		extractionRequestHelpError,
		extractionRequestHelpSuccess,
		saveClientCaseInfo,
		saveClientCaseInfoError,
		saveClientCaseInfoSuccess,
		saveExtractionQuestions,
		saveExtractionQuestionsError,
		saveExtractionQuestionsSuccess,
		saveDocumentQuestions,
		saveDocumentQuestionsSuccess,
		saveDocumentQuestionsError,
		saveDocumentInfoQuestions,
		saveDocumentInfoQuestionsSuccess,
		saveDocumentInfoQuestionsError,
		destoryQuickCreateDocument,
		destoryQuickCreateDocumentSuccess,
		destoryQuickCreateDocumentError,
		showQuickCreateDescription,
		updateQuickCreateNotification,
		updateQuickCreateNotificationSuccess,
		updateQuickCreateNotificationError
	};
}

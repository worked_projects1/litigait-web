/**
 * 
 * QuickCreate selectors
 * 
 */

import { createSelector } from 'reselect';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function selectors(name) {
    const selectDomain = () => (state) => state[name] || state[`rest/document-extraction-progress`] || false;

    const selectSubscriptionBilling = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.billing || {},
    );

    const selectQuickCreateDescription = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.showQuickCreateDescription || false,
    );
    
    
    return {
        selectDomain,
        selectSubscriptionBilling,
        selectQuickCreateDescription
    }
}




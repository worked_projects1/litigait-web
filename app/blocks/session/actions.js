/*
 *
 * Session actions
 *
 */

import {
  VERIFY_SESSION,
  VERIFY_SESSION_SUCCESS,
  VERIFY_SESSION_ERROR,
  LOG_IN,
  LOG_IN_SUCCESS,
  LOG_IN_ERROR,
  LOG_OUT,
  LOG_OUT_SUCCESS,
  LOG_OUT_ERROR,
  SIGN_UP,
  SIGN_UP_SUCCESS,
  SIGN_UP_ERROR,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  REQUEST_SUPPORT,
  REQUEST_SUPPORT_SUCCESS,
  REQUEST_SUPPORT_ERROR,
  REQUEST_DEMO,
  REQUEST_DEMO_SUCCESS,
  REQUEST_DEMO_ERROR,
  UPDATE_VERSION,
  UPDATE_VERSION_SUCCESS,
  UPDATE_VERSION_ERROR,
  SESSION_TIMEOUT,
  SESSION_TIMEOUT_SUCCESS,
  SESSION_TIMEOUT_ERROR,
  SESSION_LOGIN,
  SESSION_LOGIN_SUCCESS,
  SESSION_LOGIN_ERROR,
  GET_STRIPE_INDENT,
  GET_STRIPE_INDENT_SUCCESS,
  GET_STRIPE_INDENT_ERROR,
  SETUP_STRIPE_PAYMENT,
  SETUP_STRIPE_PAYMENT_SUCCESS,
  SETUP_STRIPE_PAYMENT_ERROR,
  CREATE_SUBSCRIPTION_PLAN,
  CREATE_SUBSCRIPTION_PLAN_SUCCESS,
  CREATE_SUBSCRIPTION_PLAN_ERROR,
  LOAD_SETTINGS_PLAN,
  LOAD_SETTINGS_PLAN_SUCCESS,
  LOAD_SETTINGS_PLAN_ERROR,
  UPDATE_PRACTICE_OBJECTIONS,
  UPDATE_PRACTICE_OBJECTIONS_SUCCESS,
  UPDATE_PRACTICE_OBJECTIONS_ERROR,
  VERIFY_OTP,
  VERIFY_OTP_SUCCESS,
  VERIFY_OTP_ERROR,
  RESEND_OTP,
  RESEND_OTP_SUCCESS,
  RESEND_OTP_ERROR,
  CLEAR_CACHE,
  SESSION_EXPAND,
  TWO_FACTOR_AUTHENTICATION,
  TWO_FACTOR_AUTHENTICATION_SUCCESS,
  TWO_FACTOR_AUTHENTICATION_ERROR,
  SESSION_CLEAR_TIMEOUT,
  SESSION_CLEAR_TIMEOUT_SUCCESS,
  SESSION_CLEAR_TIMEOUT_ERROR,
  SESSION_TOKEN,
  SESSION_TOKEN_SUCCESS,
  SESSION_TOKEN_ERROR,
  LOAD_APP_VERSION,
  LOAD_APP_VERSION_SUCCESS,
  LOAD_APP_VERSION_ERROR,
  UPDATE_SIGNATURE,
  UPDATE_SIGNATURE_SUCCESS,
  UPDATE_SIGNATURE_ERROR,
  LOAD_RESPOND_DETAIL,
  LOAD_RESPOND_DETAIL_SUCCESS,
  LOAD_RESPOND_DETAIL_ERROR,
  SIGN_UP_DISCOUNT_CODE,
  SIGN_UP_DISCOUNT_CODE_SUCCESS,
  SIGN_UP_DISCOUNT_CODE_ERROR,
  MYCASE_ACCESS_TOKEN,
  MYCASE_ACCESS_TOKEN_SUCCESS,
  MYCASE_ACCESS_TOKEN_ERROR,
  MYCASE_LOAD_RECORDS,
  MYCASE_LOAD_RECORDS_SUCCESS,
  MYCASE_LOAD_RECORDS_ERROR,
  UPDATE_KEY,
  AUTHENTICATION,
  AUTHENTICATION_ERROR,
  AUTHENTICATION_SUCCESS,
  GOOGLE_AUTH,
  GOOGLE_AUTH_SUCCESS,
  GOOGLE_AUTH_ERROR,
  MICROSOFT_AUTH,
  MICROSOFT_AUTH_ERROR,
  MICROSOFT_AUTH_SUCCESS,
  AUTH_SIGN_UP,
  AUTH_SIGN_UP_ERROR,
  AUTH_SIGN_UP_SUCCESS,
  AUTH_SESSION_LOGIN,
  AUTH_SESSION_LOGIN_ERROR,
  AUTH_SESSION_LOGIN_SUCCESS,
  EMAIL_VALIDATION,
  EMAIL_VALIDATION_SUCCESS,
  EMAIL_VALIDATION_ERROR,
  DISABLE_AUTH_PAGE_LOADER,
  DASHBOARD_CLEAR_FILTER,
  DASHBOARD_CLEAR_FILTER_ERROR,
  GET_LITIFY_RECORD,
  GET_LITIFY_RECORD_SUCCESS,
  GET_LITIFY_RECORD_ERROR,
  LITIFY_CASES_CREATE,
  LITIFY_CASES_CREATE_SUCCESS,
  LITIFY_CASES_CREATE_ERROR,
  GET_LITIFY_BULK_RECORDS,
  GET_LITIFY_BULK_RECORDS_ERROR,
  GET_LITIFY_BULK_RECORDS_SUCCESS,
  WEB_AUTHENTICATION,
  WEB_AUTHENTICATION_SUCCESS,
  WEB_AUTHENTICATION_ERROR,
  SWITCH_PRACTICE,
  SWITCH_PRACTICE_SUCCESS,
  SWITCH_PRACTICE_ERROR,
  CLIO_ACCESS_TOKEN,
  CLIO_ACCESS_TOKEN_ERROR,
  CLIO_ACCESS_TOKEN_SUCCESS,
  CLIO_LOAD_RECORDS,
  CLIO_LOAD_RECORDS_ERROR,
  CLIO_LOAD_RECORDS_SUCCESS,
  GET_ALL_MY_PRACTICES,
  CLEAR_PRACTICE_SWITCH_RENDERING,
  SET_PRACTICE_SWITCH_RENDERING,
  CHAT_NOTIFICATION,
  CHAT_NOTIFICATION_SUCCESS,
  CHAT_NOTIFICATION_ERROR
} from './constants';

/**
 * @param {string} secret 
 */
export function verifySession(secret) {
  return {
    type: VERIFY_SESSION,
    secret
  };
}

/**
 * @param {object} user 
 */
export function verifySessionSuccess(user) {
  return {
    type: VERIFY_SESSION_SUCCESS,
    user
  };
}

/**
 * @param {string} error 
 */
export function verifySessionError(error) {
  return {
    type: VERIFY_SESSION_ERROR,
    error,
  };
}

/**
 * @param {string} identifier 
 * @param {string} secret 
 * @param {object} queryParams 
 * @param {string} form 
 * @param {boolean} respondCase 
 * @param {string} integrationParams
 */
export function logIn(identifier, secret, queryParams, form, respondCase, integrationParams) {
  return {
    type: LOG_IN,
    identifier,
    secret,
    queryParams,
    form,
    respondCase,
    integrationParams
  };
}

/**
 * @param {object} user 
 * @param {string} token 
 */
export function logInSuccess(user, token) {
  return {
    type: LOG_IN_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function logInError(error) {
  return {
    type: LOG_IN_ERROR,
    error,
  };
}


/**
 * @param {boolean} load 
 */
export function logOut(load) {
  return {
    type: LOG_OUT,
    load
  };
}

export function logOutSuccess() {
  return {
    type: LOG_OUT_SUCCESS,
  };
}

/**
 * @param {string} error 
 */
export function logOutError(error) {
  return {
    type: LOG_OUT_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 * @param {string} integrationParams
 */
export function signUp(record, form, integrationParams) {
  return {
    type: SIGN_UP,
    record,
    form,
    integrationParams
  };
}

/**
 * @param {object} user 
 * @param {string} token 
 */
export function signUpSuccess(user, token) {
  return {
    type: SIGN_UP_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function signUpError(error) {
  return {
    type: SIGN_UP_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function forgotPassword(record, form) {
  return {
    type: FORGOT_PASSWORD,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function forgotPasswordSuccess(success) {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function forgotPasswordError(error) {
  return {
    type: FORGOT_PASSWORD_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function resetPassword(record, form) {
  return {
    type: RESET_PASSWORD,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function resetPasswordSuccess(success) {
  return {
    type: RESET_PASSWORD_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function resetPasswordError(error) {
  return {
    type: RESET_PASSWORD_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function changePassword(record, form) {
  return {
    type: CHANGE_PASSWORD,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function changePasswordSuccess(success) {
  return {
    type: CHANGE_PASSWORD_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function changePasswordError(error) {
  return {
    type: CHANGE_PASSWORD_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function requestSupport(record, form) {
  return {
    type: REQUEST_SUPPORT,
    record,
    form
  };
}

/**
 * @param {string} form 
 * @param {string} success 
 */
export function requestSupportSuccess(form, success) {
  return {
    type: REQUEST_SUPPORT_SUCCESS,
    form,
    success
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function requestSupportError(form, error) {
  return {
    type: REQUEST_SUPPORT_ERROR,
    form,
    error
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function requestDemo(record, form) {
  return {
    type: REQUEST_DEMO,
    record,
    form
  };
}

/**
 * @param {string} form 
 * @param {string} success 
 */
export function requestDemoSuccess(form, success) {
  return {
    type: REQUEST_DEMO_SUCCESS,
    form,
    success
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function requestDemoError(form, error) {
  return {
    type: REQUEST_DEMO_ERROR,
    form,
    error
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function updateVersion(record, form) {
  return {
    type: UPDATE_VERSION,
    record,
    form
  };
}

/**
 * @param {integer} version 
 */
export function updateVersionSuccess(version) {
  return {
    type: UPDATE_VERSION_SUCCESS,
    version
  };
}

/**
 * @param {string} error 
 */
export function updateVersionError(error) {
  return {
    type: UPDATE_VERSION_ERROR,
    error
  };
}

/**
 * @param {function} dispatch 
 */
export function sessionTimeout(dispatch) {
  return {
    type: SESSION_TIMEOUT,
    dispatch
  };
}

/**
 * @param {function} timeout 
 */
export function sessionTimeoutSuccess(timeout) {
  return {
    type: SESSION_TIMEOUT_SUCCESS,
    timeout
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function sessionTimeoutError(form, error) {
  return {
    type: SESSION_TIMEOUT_ERROR,
    form,
    error
  };
}

/**
 * @param {string} identifier 
 * @param {string} secret 
 * @param {string} form  
 */
export function sessionLogin(identifier, secret, form) {
  return {
    type: SESSION_LOGIN,
    identifier,
    secret,
    form
  };
}

/**
 * @param {integer} duration 
 */
export function sessionLoginSuccess(duration) {
  return {
    type: SESSION_LOGIN_SUCCESS,
    duration
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function sessionLoginError(form, error) {
  return {
    type: SESSION_LOGIN_ERROR,
    form,
    error
  };
}

/**
 * 
 * @returns 
 */
export function getStripeIndent() {
  return {
    type: GET_STRIPE_INDENT
  };
}

/**
* 
* @param {array} record 
*  
*/
export function getStripeIndentSuccess(record) {
  return {
    type: GET_STRIPE_INDENT_SUCCESS,
    record
  };
}

/**
* @param {string} error 
*/
export function getStripeIndentError(error) {
  return {
    type: GET_STRIPE_INDENT_ERROR,
    error
  };
}

/**
 * 
 * @param {object} record 
 * @param {string} form 
 * @param {object} locationState 
 * @returns 
 */
export function setupStripePayment(record, form, locationState) {
  return {
    type: SETUP_STRIPE_PAYMENT,
    record,
    form,
    locationState
  };
}

/**
* 
* @param {array} record 
*  
*/
export function setupStripePaymentSuccess(record) {
  return {
    type: SETUP_STRIPE_PAYMENT_SUCCESS,
    record
  };
}

/**
* @param {string} error 
*/
export function setupStripePaymentError(error) {
  return {
    type: SETUP_STRIPE_PAYMENT_ERROR,
    error
  };
}

/**
 * 
 * @param {object} record 
 * @param {object} user 
 * @param {string} secret 
 * @returns 
 */
export function createSubscriptionPlan(record, user, secret) {
  return {
    type: CREATE_SUBSCRIPTION_PLAN,
    record,
    user,
    secret
  };
}

/**
* 
* @param {array} record 
*  
*/
export function createSubscriptionPlanSuccess(success) {
  return {
    type: CREATE_SUBSCRIPTION_PLAN_SUCCESS,
    success
  };
}

/**
* @param {string} error 
*/
export function createSubscriptionPlanError(error) {
  return {
    type: CREATE_SUBSCRIPTION_PLAN_ERROR,
    error
  };
}


export function loadSettingsPlan() {
  return {
    type: LOAD_SETTINGS_PLAN
  };
}

/**
* 
* @param {object} metaData 
*  
*/
export function loadSettingsPlanSuccess(metaData) {
  return {
    type: LOAD_SETTINGS_PLAN_SUCCESS,
    metaData
  };
}

/**
* @param {string} error 
*/
export function loadSettingsPlanError(error) {
  return {
    type: LOAD_SETTINGS_PLAN_ERROR,
    error
  };
}

/**
 * @param {object} record 
 */
export function updatePracticeObjections(record) {
  return {
    type: UPDATE_PRACTICE_OBJECTIONS,
    record
  };
}

/**
 * @param {object} record 
 * @param {string} success 
 */
export function updatePracticeObjectionsSuccess(record, success) {
  return {
    type: UPDATE_PRACTICE_OBJECTIONS_SUCCESS,
    record,
    success
  };
}


/**
 * @param {string} error 
 */
export function updatePracticeObjectionsError(error) {
  return {
    type: UPDATE_PRACTICE_OBJECTIONS_ERROR,
    error
  };
}


/**
 * 
 * @param {string} email 
 * @param {string} otp 
 * @param {string} form 
 * @param {string} user_from 
 * @param {string} integrationParams
 * @returns 
 */
export function verifyOtp(email, otp, form, user_from, integrationParams) {
  return {
    type: VERIFY_OTP,
    email,
    otp,
    form,
    user_from,
    integrationParams
  };
}


/**
 * @param {object} user 
 * @param {string} token 
 */
export function verifyOtpSuccess(user, token) {
  return {
    type: VERIFY_OTP_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function verifyOtpError(error) {
  return {
    type: VERIFY_OTP_ERROR,
    error
  };
}


/**
 * 
 * @param {string} identifier 
 * @param {string} secret 
 * @param {function} setSpinner 
 * @returns 
 */
export function resendOtp(identifier, secret, setSpinner) {
  return {
    type: RESEND_OTP,
    identifier,
    secret,
    setSpinner
  };
}

/**
 * @param {string} success 
 */
export function resendOtpSuccess(success) {
  return {
    type: RESEND_OTP_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function resendOtpError(error) {
  return {
    type: RESEND_OTP_ERROR,
    error,
  };
}


/**
 * 
 * @param {object} record 
 * @param {string} form 
 * @returns 
 */
export function twoFactorAuthentication(record, form) {
  return {
    type: TWO_FACTOR_AUTHENTICATION,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function twoFactorAuthenticationSuccess(user, success) {
  return {
    type: TWO_FACTOR_AUTHENTICATION_SUCCESS,
    user,
    success
  };
}

/**
 * @param {string} error 
 */
export function twoFactorAuthenticationError(error) {
  return {
    type: TWO_FACTOR_AUTHENTICATION_ERROR,
    error,
  };
}


export function clearCache() {
  return {
    type: CLEAR_CACHE
  };
}

/**
 * 
 * @param {string} expand 
 * @returns 
 */
export function sessionExpand(expand) {
  return {
    type: SESSION_EXPAND,
    expand
  };
}


export function sessionClearTimeout() {
  return {
    type: SESSION_CLEAR_TIMEOUT
  };
}

export function sessionClearTimeoutSuccess() {
  return {
    type: SESSION_CLEAR_TIMEOUT_SUCCESS
  };
}

/**
 * @param {string} form
 * @param {string} error 
 */
export function sessionClearTimeoutError(form, error) {
  return {
    type: SESSION_CLEAR_TIMEOUT_ERROR,
    form,
    error
  };
}

/**
 * 
 * @param {object} record 
 * @param {boolean} respondCase
 * @param {boolean} integration
 * @param {boolean} model
 * @returns 
 */
export function sessionToken(record, respondCase, integration, model) {
  return {
    type: SESSION_TOKEN,
    record,
    respondCase,
    integration,
    model
  };
}

/**
 * 
 * @param {string} token 
 * @returns 
 */
export function sessionTokenSuccess(token) {
  return {
    type: SESSION_TOKEN_SUCCESS,
    token
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 * @returns 
 */
export function sessionTokenError(form, error) {
  return {
    type: SESSION_TOKEN_ERROR,
    form,
    error
  };
}

export function loadAppVersion() {
  return {
    type: LOAD_APP_VERSION,
  }
}

/**
 * 
 * @param {integer} appVersion 
 * @returns 
 */
export function loadAppVersionSuccess(appVersion) {
  return {
    type: LOAD_APP_VERSION_SUCCESS,
    appVersion
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function loadAppVersionError(error) {
  return {
    type: LOAD_APP_VERSION_ERROR,
    error
  }
}

/**
 * 
 * @param {object} record 
 * @param {string} form 
 * @returns 
 */
export function updateSignature(record, form) {
  return {
    type: UPDATE_SIGNATURE,
    record,
    form
  }
}

/**
 * 
 * @param {string} success 
 * @returns 
 */
export function updateSignatureSuccess(success) {
  return {
    type: UPDATE_SIGNATURE_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function updateSignatureError(error) {
  return {
    type: UPDATE_SIGNATURE_ERROR,
    error
  }
}

/**
 * 
 * @param {string} secret 
 * @param {boolean} activeSession 
 * @param {object} user 
 * @returns 
 */
export function loadRespondDetail(secret, activeSession, user) {
  return {
    type: LOAD_RESPOND_DETAIL,
    secret,
    activeSession,
    user
  }
}

/**
 * 
 * @param {object} records 
 * @returns 
 */
export function loadRespondDetailSuccess(record) {
  return {
    type: LOAD_RESPOND_DETAIL_SUCCESS,
    record
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function loadRespondDetailError(error) {
  return {
    type: LOAD_RESPOND_DETAIL_ERROR,
    error
  }
}

/**
 * 
 * @param {object} record 
 * @param {string} form 
 * @param {function} dialog  
 * @param {boolean} cardDetails 
 * @param {boolean} oneTimeActivationFee 
 * @param {function} handleSubmit 
 * @returns 
 */
export function signUpDiscountCode(record, form, dialog, cardDetails, handleSubmit) {
  return {
    type: SIGN_UP_DISCOUNT_CODE,
    record,
    form,
    dialog,
    cardDetails,
    handleSubmit
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function signUpDiscountCodeError(error) {
  return {
    type: SIGN_UP_DISCOUNT_CODE_ERROR,
    error
  }
}

/**
 * 
 * @param {array} discountDetail
 * @returns 
 */
export function signUpDiscountCodeSuccess(discountDetail) {
  return {
    type: SIGN_UP_DISCOUNT_CODE_SUCCESS,
    discountDetail
  }
}


/**
 *  
 * @param {string} searchParams 
 * @param {boolean} activeSession 
 * @param {boolean} loadRecord
 * @returns 
 */
export function myCaseAccessToken(searchParams, loadRecord) {
  return {
    type: MYCASE_ACCESS_TOKEN,
    searchParams,
    loadRecord
  }
}

/**
 * 
 * @param {object} token
 * @returns 
 */
export function myCaseAccessTokenSuccess(token) {
  return {
    type: MYCASE_ACCESS_TOKEN_SUCCESS,
    token
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function myCaseAccessTokenError(error) {
  return {
    type: MYCASE_ACCESS_TOKEN_ERROR,
    error
  }
}

/**
 * 
 * @param {boolean} load
 * @param {function} setMyCasePopup
 * @param {function} setStepper
 */
export function myCaseLoadRecords(load, setMyCasePopup, setStepper) {
  return {
    type: MYCASE_LOAD_RECORDS,
    load,
    setMyCasePopup,
    setStepper
  }
}

/**
 * @param {object} records
 */
export function myCaseLoadRecordsSuccess(records) {
  return {
    type: MYCASE_LOAD_RECORDS_SUCCESS,
    records,
  };
}

/**
 * @param {string} error 
 */
export function myCaseLoadRecordsError(error) {
  return {
    type: MYCASE_LOAD_RECORDS_ERROR,
    error,
  };
}

/**
 * @param {number} key 
 */
export function updateKey(key) {
  return {
    type: UPDATE_KEY,
    key
  };
}

/**
 * @param {object} record 
 */
export function authentication(record) {
  return {
    type: AUTHENTICATION,
    record
  };
}

/**
 * @param {object} record 
 */
export function authenticationSuccess(record) {
  return {
    type: AUTHENTICATION_SUCCESS,
    record
  };
}

/**
 * @param {string} error 
 */
export function authenticationError(error) {
  return {
    type: AUTHENTICATION_ERROR,
    error,
  };
}


/**
 * @param {string} searchParams 
 */
export function googleAuth(searchParams) {
  return {
    type: GOOGLE_AUTH,
    searchParams
  };
}

/**
 * @param {object} record 
 */
export function googleAuthSuccess(record) {
  return {
    type: GOOGLE_AUTH_SUCCESS,
    record
  };
}

/**
 * @param {string} error 
 */
export function googleAuthError(error) {
  return {
    type: GOOGLE_AUTH_ERROR,
    error,
  };
}


/**
 * @param {string} searchParams 
 */
export function microsoftAuth(searchParams) {
  return {
    type: MICROSOFT_AUTH,
    searchParams
  };
}

/**
 * @param {object} record 
 */
export function microsoftAuthSuccess(record) {
  return {
    type: MICROSOFT_AUTH_SUCCESS,
    record
  };
}

/**
 * @param {string} error 
 */
export function microsoftAuthError(error) {
  return {
    type: MICROSOFT_AUTH_ERROR,
    error,
  };
}


/**
 * @param {object} record 
 * @param {string} form 
 */
export function authSignUp(record, form) {
  return {
    type: AUTH_SIGN_UP,
    record,
    form
  };
}

/**
 * @param {object} user 
 * @param {string} token 
 */
export function authSignUpSuccess(user, token) {
  return {
    type: AUTH_SIGN_UP_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function authSignUpError(error) {
  return {
    type: AUTH_SIGN_UP_ERROR,
    error,
  };
}


/**
 * @param {string} identifier 
 * @param {string} form 
 */
export function authSessionLogin(identifier, form) {
  return {
    type: AUTH_SESSION_LOGIN,
    identifier,
    form
  };
}

/**
 * @param {integer} duration 
 */
export function authSessionLoginSuccess(duration) {
  return {
    type: AUTH_SESSION_LOGIN_SUCCESS,
    duration
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function authSessionLoginError(form, error) {
  return {
    type: AUTH_SESSION_LOGIN_ERROR,
    form,
    error
  };
}

/**
 * @param {object} record
 * @param {string} routePath
 * @param {string} form
 * @param {string} integrationParams
 */
export function emailValidation(record, routePath, form, integrationParams){
  return {
    type: EMAIL_VALIDATION,
    record,
    routePath,
    form,
    integrationParams
  }
}

/**
 * @param {object} success
 */
export function emailValidationSuccess(success){
  return {
    type: EMAIL_VALIDATION_SUCCESS,
    success
  }
}

/**
 * @param {object} success
 */
export function emailValidationError(error){
  return {
    type: EMAIL_VALIDATION_ERROR,
    error
  }
}


export function disableAuthPageLoader() {
  return {
    type: DISABLE_AUTH_PAGE_LOADER
  };
}

/**
 * @param {boolean} load
 */
export function dashboardClearFilter(load){
  return {
    type: DASHBOARD_CLEAR_FILTER,
    load
  }
}

/**
 * @param { string } error
 */
export function dashboardClearFilterError(error) {
  return {
    type: DASHBOARD_CLEAR_FILTER_ERROR,
    error
  }
}

/**
 * @param {string} secret
 * @param {string} activeSession
 * @param {string} user
 * @param {string} searchParams
 */
export function getLitifyRecord(secret, activeSession, user, searchParams) {
  return {
    type: GET_LITIFY_RECORD,
    secret,
    activeSession,
    user,
    searchParams
  }
}

/**
 * @param {string} success
 */
export function getLitifyRecordSuccess(success) {
  return {
    type: GET_LITIFY_RECORD_SUCCESS,
    success
  }
}

/**
 * @param {string} error
 */
export function getLitifyRecordError(error) {
  return {
    type: GET_LITIFY_RECORD_ERROR,
    error
  }
}

/**
 * 
 * @param {string} litifyParams
 */
export function litifyCaseCreate(litifyParams) {
  return {
    type: LITIFY_CASES_CREATE,
    litifyParams
  }
}

/**
 * @param {string} success
 */
export function litifyCaseCreateSuccess(success) {
  return {
    type: LITIFY_CASES_CREATE_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function litifyCaseCreateError(error) {
  return {
    type: LITIFY_CASES_CREATE_ERROR,
    error
  }
}

/**
 * 
 * @param {string} secret
 * @param {object} user 
 * @param {boolean} load
 * @param {string} path
 * @returns 
 */
export function getLitifyBulkRecords(secret, user, load, path) {
  return {
    type: GET_LITIFY_BULK_RECORDS,
    secret,
    user,
    load,
    path
  }
}

/**
 * 
 * @param {string} records 
 * @returns 
 */
export function getLitifyBulkRecordsSuccess(records) {
  return {
    type: GET_LITIFY_BULK_RECORDS_SUCCESS,
    records
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function getLitifyBulkRecordsError(error) {
  return {
    type: GET_LITIFY_BULK_RECORDS_ERROR,
    error
  }
}

/**
 * 
 * @param {object} record 
 * @param {string} form
 * @param {function} setAuthenticate
 * @returns 
 */
export function webAuthentication(record, form, setAuthenticate) {
  return {
    type: WEB_AUTHENTICATION,
    record,
    form,
    setAuthenticate
  }
}

/**
 * 
 * @param {string} status 
 * @returns 
 */
export function webAuthenticationSuccess(status) {
  return {
    type: WEB_AUTHENTICATION_SUCCESS,
    status
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function webAuthenticationError(error) {
  return {
    type: WEB_AUTHENTICATION_ERROR,
    error
  }
}

/**
 * 
 * @param {object} practice_data 
 * @param {string} form
 * @returns 
 */
export function switchPractice(practice_data, form) {
  return {
      type: SWITCH_PRACTICE,
      practice_data,
      form
  }
}

/**
 * @param {object} user 
 * @param {string} token 
 * @param {string} success
 */
export function switchPracticeSuccess(user, token, success) {
  return {
      type: SWITCH_PRACTICE_SUCCESS,
      user,
      token,
      success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function switchPracticeError(error) {
  return {
      type: SWITCH_PRACTICE_ERROR,
      error
  }
}

/**
 *  
 * @param {string} searchParams
 * @param {boolean} loadRecord
 * @returns 
 */
export function clioAccessToken(searchParams, loadRecord) {
  return {
    type: CLIO_ACCESS_TOKEN,
    searchParams,
    loadRecord
  }
}

/**
 * 
 * @param {object} token
 * @returns 
 */
export function clioAccessTokenSuccess(token) {
  return {
    type: CLIO_ACCESS_TOKEN_SUCCESS,
    token
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function clioAccessTokenError(error) {
  return {
    type: CLIO_ACCESS_TOKEN_ERROR,
    error
  }
}

/**
 * 
 * @param {boolean} load
 * @param {function} setClioPopup
 * @param {function} setStepper
 */
export function clioLoadRecords(load, setClioPopup, setStepper) {
  return {
    type: CLIO_LOAD_RECORDS,
    load,
    setClioPopup,
    setStepper
  }
}

/**
 * @param {object} records
 */
export function clioLoadRecordsSuccess(records) {
  return {
    type: CLIO_LOAD_RECORDS_SUCCESS,
    records,
  };
}

/**
 * @param {string} error 
 */
export function clioLoadRecordsError(error) {
  return {
    type: CLIO_LOAD_RECORDS_ERROR,
    error,
  };
}

export function getAllMyPractices(metaData) {
  return {
      type: GET_ALL_MY_PRACTICES,
      metaData
  }
}

export function clearSwitchPracticeRendering(renderObject) {
  return {
      type: CLEAR_PRACTICE_SWITCH_RENDERING,
      renderObject
  }
}

export function setSwitchPracticeRendering(renderObject) {
  return {
      type: SET_PRACTICE_SWITCH_RENDERING,
      renderObject
  }
}

/**
 * 
 * @param {object} record 
 * @returns 
 */
export function chatNotification(record) {
  return {
    type: CHAT_NOTIFICATION,
    record
  }
}

/**
 * 
 * @param {string} success 
 * @returns 
 */
export function chatNotificationSuccess(success) {
  return {
    type: CHAT_NOTIFICATION_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function chatNotificationError(error) {
  return {
    type: CHAT_NOTIFICATION_ERROR,
    error
  }
}

export default {
  verifySession,
  verifySessionSuccess,
  verifySessionError,
  logIn,
  logInSuccess,
  logInError,
  logOut,
  logOutSuccess,
  logOutError,
  forgotPassword,
  forgotPasswordSuccess,
  forgotPasswordError,
  resetPassword,
  resetPasswordSuccess,
  resetPasswordError,
  changePassword,
  changePasswordSuccess,
  changePasswordError,
  requestSupport,
  requestSupportSuccess,
  requestSupportError,
  requestDemo,
  requestDemoSuccess,
  requestDemoError,
  updateVersion,
  updateVersionSuccess,
  updateVersionError,
  sessionTimeout,
  sessionTimeoutSuccess,
  sessionTimeoutError,
  sessionLogin,
  sessionLoginSuccess,
  sessionLoginError,
  getStripeIndent,
  getStripeIndentSuccess,
  getStripeIndentError,
  setupStripePayment,
  setupStripePaymentSuccess,
  setupStripePaymentError,
  createSubscriptionPlan,
  createSubscriptionPlanSuccess,
  createSubscriptionPlanError,
  loadSettingsPlan,
  loadSettingsPlanSuccess,
  loadSettingsPlanError,
  updatePracticeObjections,
  updatePracticeObjectionsSuccess,
  updatePracticeObjectionsError,
  verifyOtp,
  verifyOtpSuccess,
  verifySessionError,
  resendOtp,
  resendOtpSuccess,
  resendOtpError,
  twoFactorAuthentication,
  twoFactorAuthenticationSuccess,
  twoFactorAuthenticationError,
  clearCache,
  sessionExpand,
  sessionClearTimeout,
  sessionClearTimeoutSuccess,
  sessionClearTimeoutError,
  sessionToken,
  sessionTokenSuccess,
  sessionTokenError,
  loadAppVersion,
  loadAppVersionSuccess,
  loadAppVersionError,
  updateSignature,
  updateSignatureSuccess,
  updateSignatureError,
  loadRespondDetail,
  loadRespondDetailSuccess,
  loadRespondDetailError,
  signUpDiscountCode,
  signUpDiscountCodeError,
  signUpDiscountCodeSuccess,
  myCaseAccessToken,
  myCaseAccessTokenError,
  myCaseAccessTokenSuccess,
  myCaseLoadRecords,
  myCaseLoadRecordsSuccess,
  myCaseLoadRecordsError,
  updateKey,
  authentication,
  authenticationSuccess,
  authenticationError,
  microsoftAuth,
  microsoftAuthSuccess,
  microsoftAuthError,
  authSignUp,
  authSignUpSuccess,
  authSignUpError,
  emailValidation,
  emailValidationSuccess,
  emailValidationError,
  disableAuthPageLoader,
  dashboardClearFilter,
  dashboardClearFilterError,
  getLitifyRecord,
  getLitifyRecordSuccess,
  getLitifyRecordError,
  litifyCaseCreate,
  litifyCaseCreateSuccess,
  litifyCaseCreateError,
  getLitifyBulkRecords,
  getLitifyBulkRecordsError,
  getLitifyBulkRecordsSuccess,
  webAuthentication,
  webAuthenticationSuccess,
  webAuthenticationError,
  switchPractice,
  switchPracticeSuccess,
  switchPracticeError,
  clioAccessToken,
  clioAccessTokenSuccess,
  clioAccessTokenError,
  clioLoadRecords,
  clioLoadRecordsSuccess,
  clioLoadRecordsError,
  getAllMyPractices,
  clearSwitchPracticeRendering,
  setSwitchPracticeRendering,
  chatNotification,
  chatNotificationSuccess,
  chatNotificationError
}
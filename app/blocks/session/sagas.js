/*
 *
 *  session sagas
 *
 */

import { call, take, put, select, all, takeLatest, delay } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { reset } from 'redux-form';
import history from 'utils/history';
const CryptoJS = require("crypto-js");
const base64 = require('base-64');


import { DEFAULT_SESSION_TOKEN_ERROR, DEFAULT_RESET_PASSWORD_ERROR, DEFAULT_CHANGE_PASSWORD_ERROR, DEFAULT_FORGOT_PASSWORD_ERROR, DEFAULT_REQUEST_SUPPORT_ERROR, DEFAULT_REQUEST_DEMO_ERROR, DEFAULT_UPDATE_VERSION_ERROR, DEFAULT_SESSION_TIMEOUT_ERROR, DEFAULT_GET_STRIPE_INDENT_ERROR, DEFAULT_SETUP_STRIPE_PAYMENT_ERROR, DEFAULT_CREATE_SUBSCRIPTION_PLAN_ERROR, DEFAULT_LOAD_SETTINGS_PLAN_ERROR, DEFAULT_UPDATE_PRACTICE_OBJECTIONS_ERROR, DEFAULT_RESEND_OTP_ERROR, DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR, DEFAULT_SESSION_CLEAR_TIMEOUT_ERROR, DEFAULT_MYCASE_RECORDS_ERROR, DEFAULT_AUTH_ERROR, DEFAULT_GOOGLE_AUTH_ERROR, DEFAULT_MICROSOFT_AUTH_ERROR, DEFAULT_CLIO_RECORDS_ERROR } from 'utils/errors.js';
import { setAuthToken } from 'utils/api';
import { Timer, Localize, getAuthCode } from 'utils/tools';

import store2 from 'store2';
import { LOG_IN, LOG_OUT, VERIFY_SESSION, SIGN_UP, ROUTER, FORGOT_PASSWORD, RESET_PASSWORD, CHANGE_PASSWORD, REQUEST_SUPPORT, REQUEST_DEMO, UPDATE_VERSION, SESSION_TIMEOUT, SESSION_LOGIN, SETUP_STRIPE_PAYMENT, CREATE_SUBSCRIPTION_PLAN, LOAD_SETTINGS_PLAN, UPDATE_PRACTICE_OBJECTIONS, VERIFY_OTP, RESEND_OTP, TWO_FACTOR_AUTHENTICATION, SESSION_CLEAR_TIMEOUT, SESSION_TOKEN, LOAD_APP_VERSION, UPDATE_SIGNATURE, LOAD_RESPOND_DETAIL, SIGN_UP_DISCOUNT_CODE, MYCASE_ACCESS_TOKEN, MYCASE_LOAD_RECORDS, AUTHENTICATION, GOOGLE_AUTH, MICROSOFT_AUTH, AUTH_SIGN_UP, EMAIL_VALIDATION, DASHBOARD_CLEAR_FILTER, GET_LITIFY_RECORD, LITIFY_CASES_CREATE, GET_LITIFY_BULK_RECORDS, WEB_AUTHENTICATION, SWITCH_PRACTICE, CLIO_ACCESS_TOKEN, CLIO_LOAD_RECORDS, CHAT_NOTIFICATION } from './constants';

import {
  verifySessionSuccess,
  verifySessionError,
  logInSuccess,
  logInError,
  logOut as logOutAction,
  logOutSuccess,
  logOutError,
  signUpSuccess,
  signUpError,
  forgotPasswordError,
  forgotPasswordSuccess,
  resetPasswordError,
  resetPasswordSuccess,
  changePasswordError,
  changePasswordSuccess,
  requestSupportError,
  requestSupportSuccess,
  requestDemoError,
  requestDemoSuccess,
  updateVersionError,
  updateVersionSuccess,
  sessionTimeoutError,
  sessionTimeoutSuccess,
  sessionLoginError,
  sessionLoginSuccess,
  getStripeIndentError,
  getStripeIndentSuccess,
  setupStripePayment as setupStripePaymentAction,
  setupStripePaymentError,
  setupStripePaymentSuccess,
  createSubscriptionPlan as createSubscriptionPlanAction,
  createSubscriptionPlanError,
  createSubscriptionPlanSuccess,
  loadSettingsPlanError,
  loadSettingsPlanSuccess,
  updatePracticeObjectionsError,
  updatePracticeObjectionsSuccess,
  verifySession as verifySessionAction,
  verifyOtpError,
  verifyOtpSuccess,
  resendOtpError,
  resendOtpSuccess,
  twoFactorAuthenticationSuccess,
  twoFactorAuthenticationError,
  sessionClearTimeoutSuccess,
  sessionClearTimeoutError,
  sessionToken as sessionTokenAction,
  sessionTokenSuccess,
  sessionTokenError,
  loadAppVersionSuccess,
  loadAppVersionError,
  updateSignatureError,
  updateSignatureSuccess,
  loadRespondDetail as loadRespondDetailAction,
  loadRespondDetailSuccess,
  loadRespondDetailError,
  signUpDiscountCodeSuccess,
  signUpDiscountCodeError,
  myCaseAccessToken as myCaseAccessTokenAction,
  myCaseAccessTokenSuccess,
  myCaseAccessTokenError,
  myCaseLoadRecords as myCaseLoadRecordsAction,
  myCaseLoadRecordsSuccess,
  myCaseLoadRecordsError,
  updateKey,
  authentication as authenticationAction,
  authenticationSuccess,
  authenticationError,
  googleAuth as googleAuthAction,
  googleAuthError,
  microsoftAuth as microsoftAuthAction,
  microsoftAuthError,
  authSignUpSuccess,
  authSignUpError,
  emailValidationError,
  disableAuthPageLoader as disableAuthPageLoaderAction,
  dashboardClearFilter as dashboardClearFilterAction,
  dashboardClearFilterError,
  getLitifyRecord as getLitifyRecordAction,
  getLitifyRecordSuccess,
  getLitifyRecordError,
  litifyCaseCreate as litifyCaseCreateAction,
  litifyCaseCreateSuccess,
  litifyCaseCreateError,
  getLitifyBulkRecords as getLitifyBulkRecordsAction,
  getLitifyBulkRecordsSuccess,
  getLitifyBulkRecordsError,
  webAuthenticationSuccess,
  webAuthenticationError,
  switchPracticeSuccess,
  switchPracticeError,
  clioAccessToken as clioAccessTokenAction,
  clioAccessTokenSuccess,
  clioAccessTokenError,
  clioLoadRecords as clioLoadRecordsAction,
  clioLoadRecordsSuccess,
  clioLoadRecordsError,
  getAllMyPractices as getAllMyPracticesAction,
  setSwitchPracticeRendering,
  chatNotificationError,
  chatNotificationSuccess
} from './actions';

import { logIn, verifySession, signUp, forgotPassword, resetPassword, changePassword, requestSupport, requestDemo, updateVersion, getStripeIndent, setupStripePayment, createSubscriptionPlan, loadSignUpPlan, updatePracticeObjections, verifyOtp, twoFactorAuthentication, sessionToken, loadAppVersion, updateSignature, respondDetail, validateDiscountCode, myCaseAccessToken, myCaseClients, googleAuth, microsoftAuth, authSignUp, authLogin, emailValidation, syncMycaseRecords, getLitifyRecord, getLitifyBulkRecords, updateLitifyBulkRecords, websiteAuthentication, switchPractice, clioAccessToken, clioCaseClients, syncClioRecords, getAllPracticesList, chatNotification } from './remotes';

import { selectToken, selectLoggedIn, selectUser, selectActiveSession, selectTimeout } from './selectors';

import lodash from 'lodash';

import appRemotes from '../records/remotes';

import { selectCreatedDateFilter, selectActiveFilter, selectFilterData } from 'blocks/analytics/selectors';
import { updateActiveCustomFilter, updateDashboardFilterData, updateDashboardTabValue, updateFilterData, updateCustomFilterData } from 'blocks/analytics/actions';

export function* verifyInitialSessionSaga() {
  const secret = store2.get('secret');
  const disableLoadRecord = store2.get('disable_mycase_load_record');
  const clioLoadRecord = store2.get('disable_clio_load_record');
  const activeSession = store2.get('activeSession');
  const location = window.location && window.location.pathname || '/';
  const searchParams = history && history.location && history.location.search;

  if (secret && !['/reset-password/'].includes(location)) {
    try {
      setAuthToken(secret);
      store2.set('secret', secret);
      const user = yield call(verifySession);
      if(['/respond/'].includes(location)) {
        yield put(loadRespondDetailAction(secret, activeSession, user));
      } else if(location.includes('/litify/')) {
        if (searchParams) {
          yield put(getLitifyRecordAction(secret, activeSession, user, searchParams));
        } else {
          yield put(getLitifyBulkRecordsAction(secret, user, true));
        }
      } else if (['/clio'].includes(location)) {
        if (clioLoadRecord) {
          yield put(clioAccessTokenAction(searchParams, false));
        } else {
          yield put(clioAccessTokenAction(searchParams, true));
        }
      } else if (location && searchParams) {
        if(disableLoadRecord) {
          yield put(myCaseAccessTokenAction(searchParams, false));
        } else {
          yield put(myCaseAccessTokenAction(searchParams, true));
        }
      } else {
        yield put(verifySessionSuccess(user));
        yield put(sessionTokenAction(user));
        if (activeSession != null) {
          yield put(sessionLoginSuccess(activeSession));
        }
      }
    } catch (error) {
      store2.remove('secret');
      store2.remove('activeSession');
      yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
      yield put(push(process.env.PUBLIC_PATH || '/'));
    } finally {
      store2.remove('disable_mycase_load_record');
    }
  } else {
    try {
      const loggedIn = yield select(selectLoggedIn());
      if(!activeSession && !loggedIn && !['/login', '/signin', '/signup', '/forgot', '/reset-password/', '/respond/', '/google-oauth2', '/microsoft-oauth2', '/litify/'].includes(location)) {
        yield put(push(process.env.PUBLIC_PATH || '/'));
      }

      if(!activeSession && !loggedIn && location && ['/respond/' ].includes(location)) {
        yield put(loadRespondDetailAction(false, activeSession, false)); 
      }

      if (!activeSession && !loggedIn && location && location.includes('/litify/')) {
        if(searchParams){
          yield put(getLitifyRecordAction(false, activeSession, false, searchParams));
        } else {          
          yield put(getLitifyBulkRecordsAction(false, false, true, location));
        }
      }

      if(!activeSession && !loggedIn && location && ['/google-oauth2'].includes(location)) {
        yield put(googleAuthAction(searchParams)); 
      }

      if(!activeSession && !loggedIn && location && ['/microsoft-oauth2'].includes(location)) {
        yield put(microsoftAuthAction(searchParams)); 
      }
      yield put(logOutSuccess());
    } catch (error) {
      yield put(logOutError(error));
    } finally {
      store2.remove('activeSession');
      store2.remove('secret');
      store2.remove('disable_mycase_load_record');
      store2.remove('login_platform');
    }
  }
}

export function* verifySessionSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { secret } = yield take(VERIFY_SESSION);
    const loggedIn = yield select(selectLoggedIn());
    if (secret) {
      try {
        setAuthToken(secret);
        const user = yield call(verifySession);
        yield put(verifySessionSuccess(user));
      } catch (error) {
        const session_error = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_SESSION_TOKEN_ERROR;
        if (loggedIn && session_error === "invalid auth token") {
          const record = yield select(selectUser());
          yield put(sessionTokenAction(record));
          // yield put(sessionLoginSuccess(0));
          // store2.set('activeSession', 0);
        }
      }
    }
  }
}

export function* loginSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { identifier, secret, queryParams, form, respondCase, integrationParams } = yield take(LOG_IN);
    yield put(startSubmit(form));

    try {
      const result = yield call(logIn, identifier, secret, queryParams);
      store2.set('login_platform', 'default');
      if (result && result.user && result.user['two_factor']) {
        let pathUrl  = integrationParams && integrationParams || process.env.PUBLIC_PATH || '/';
        yield put(push({ pathname: pathUrl, state: Object.assign({}, { ...history.location.state }, { form: 'verifyOtp', identifier, secret, respondCase: respondCase || false }) }));
      } else {
        store2.set('secret', result.authToken);
        setAuthToken(result.authToken);
        yield put(logInSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken));
        yield put(verifySessionAction(result.authToken));
        if (integrationParams) {
          yield put(litifyCaseCreateAction(integrationParams));
        } else {
          yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }, { respondCase: respondCase || false }) }));
        }
      }
    } catch (error) {
      const login_attempts = error.response && error.response.data && error.response.data.login_attempts || false;
      if (login_attempts && login_attempts >= 3 && form != 'loginFailureForm') {
        yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: { form: 'loginFailure', identifier, secret } }));
      } else {
        store2.remove('secret');
        store2.remove('activeSession');
        yield put(logInError(error));
      }
    } finally {
      yield put(stopSubmit(form));
    }
  }
}

export function* logOutSaga() {
  yield takeLatest(LOG_OUT, function* updater({ load }) {
    if(load) {
      try {
        const logout_url = (process.env.ENV === 'production') ? `https://www.esquiretek.com` : (process.env.ENV === 'staging') ? `https://staging.esquiretek.com` : 'http://localhost:3001';
        const login_platform = store2.get('login_platform') || false;
        if(login_platform && login_platform === 'microsoft') {
          yield put(logOutSuccess());
          window.location.href = `https://login.microsoftonline.com/common/oauth2/v2.0/logout?post_logout_redirect_uri=${logout_url}`;
        } else {
          if (['production', 'staging'].includes(process.env.ENV)) {
            yield put(logOutSuccess());
            window.location.href = logout_url;
          } else {
            yield put(logOutSuccess());
            yield put(push(process.env.PUBLIC_PATH || '/'));
          }
        }
      } catch (error) {
        yield put(logOutError(error));
      } finally {
        store2.remove('secret');
        store2.remove('filevine');
        store2.remove('activeSession');
        store2.remove('login_platform');
        store2.remove('disable_mycase_load_record');
        store2.remove('web_authenticate');
      }
    }
  })
}

export function* signUpSaga() {
  yield takeLatest(SIGN_UP, function* updater({ record, form, integrationParams }) {
    if(record) {
      yield put(startSubmit(form));
      try {
        const result = yield call(signUp, record);
        if (result) {
          setAuthToken(result.authToken);
          const user = yield call(verifySession);
          if(user) {
            yield put(verifySessionSuccess(user));
            store2.set('secret', result.authToken);
            store2.set('getting_started', true);  
            store2.set('login_platform', 'default');
            yield put(stopSubmit(form));
            if (integrationParams) {
              yield put(litifyCaseCreateAction(integrationParams));
            } else {
              yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: { form: false, secret: result.authToken, user: Object.assign({}, result, { one_time_activation_fee: result.onetime_activation || false }), respondCase: record.respondCase || false } }));
            }
          }
        } else {
          yield put(stopSubmit(form));
          yield put(getStripeIndentError(DEFAULT_GET_STRIPE_INDENT_ERROR));
        }
      } catch (error) {
        yield put(stopSubmit(form));
        yield put(signUpError(error));
      }
    }
  })
}


export function* forgotPasswordSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record, form } = yield take(FORGOT_PASSWORD);
    yield put(startSubmit(form));

    try {
      const result = yield call(forgotPassword, record);
      if (result) {
        yield put(forgotPasswordSuccess(result.message));
        yield put(stopSubmit(form));
      } else {
        yield put(forgotPasswordError(DEFAULT_FORGOT_PASSWORD_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_FORGOT_PASSWORD_ERROR }));
      }
    } catch (error) {
      const forgot_password_error = error.response && error.response.data && error.response.data.error || DEFAULT_FORGOT_PASSWORD_ERROR;
      yield put(forgotPasswordError(forgot_password_error));
      yield put(stopSubmit(form, { _error: forgot_password_error }));
    }
  }
}


export function* resetPasswordSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record, form } = yield take(RESET_PASSWORD);
    yield put(startSubmit(form));

    try {
      const result = yield call(resetPassword, record);
      if (result) {
        yield put(resetPasswordSuccess(result.message))
        yield put(stopSubmit(form));
      } else {
        yield put(resetPasswordError(DEFAULT_RESET_PASSWORD_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_RESET_PASSWORD_ERROR }));
      }
    } catch (error) {
      const Error = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_RESET_PASSWORD_ERROR;
      yield put(resetPasswordError(Error && Error === 'invalid auth token' && 'Reset password link is expired. Please try resetting the password again.' || Error));
      yield put(stopSubmit(form, { _error: Error && Error === 'invalid auth token' && 'Reset password link is expired. Please try resetting the password again.' || Error }));
    }
  }
}


export function* changePasswordSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, form } = yield take(CHANGE_PASSWORD);
    yield put(startSubmit(form));

    try {
      const { password, new_password } = record;
      const secret = store2.get('secret');
      if (password && password.length < 8) {
        yield put(changePasswordError('Password must be at least 8 characters'));
        yield put(stopSubmit(form, { _error: 'Password must be at least 8 characters' }));
      } else if (password && new_password && password != new_password) {
        yield put(changePasswordError('Password Mismatch'));
        yield put(stopSubmit(form, { _error: 'Password Mismatch' }));
      } else {
        const result = yield call(changePassword, record);
        if (result) {
          yield put(changePasswordSuccess(result.message));
          yield put(verifySessionAction(secret));
          yield put(stopSubmit(form));
        } else {
          yield put(changePasswordError(DEFAULT_CHANGE_PASSWORD_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_CHANGE_PASSWORD_ERROR }));
        }
      }
    } catch (error) {
      const Error = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CHANGE_PASSWORD_ERROR;
      yield put(changePasswordError(Error));
      yield put(stopSubmit(form, { _error: Error }));
    }
  }
}



export function* requestSupportSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, form } = yield take(REQUEST_SUPPORT);
    yield put(startSubmit(form));

    try {
      const result = yield call(requestSupport, record);
      if (result) {
        yield put(requestSupportSuccess(form, result.message))
        yield put(reset(form));
        yield put(stopSubmit(form));
      } else {
        yield put(requestSupportError(form, DEFAULT_REQUEST_SUPPORT_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_REQUEST_SUPPORT_ERROR }));
      }
    } catch (error) {
      yield put(requestSupportError(form, DEFAULT_REQUEST_SUPPORT_ERROR));
      yield put(stopSubmit(form, { _error: DEFAULT_REQUEST_SUPPORT_ERROR }));
    }
  }
}


export function* requestDemoSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, form } = yield take(REQUEST_DEMO);
    yield put(startSubmit(form));

    try {
      const result = yield call(requestDemo, record);
      if (result) {
        yield put(requestDemoSuccess(form, result.message))
        yield put(reset(form));
        yield put(stopSubmit(form));
      } else {
        yield put(requestSupportError(form, DEFAULT_REQUEST_DEMO_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_REQUEST_DEMO_ERROR }));
      }
    } catch (error) {
      yield put(requestSupportError(form, DEFAULT_REQUEST_DEMO_ERROR));
      yield put(stopSubmit(form, { _error: DEFAULT_REQUEST_DEMO_ERROR }));
    }
  }
}

export function* updateVersionSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, form } = yield take(UPDATE_VERSION);
    yield put(startSubmit(form));

    try {
      const result = yield call(updateVersion, record);
      if (result) {
        yield put(updateVersionSuccess(record && record.version))
        yield put(stopSubmit(form));
      } else {
        yield put(updateVersionError(DEFAULT_UPDATE_VERSION_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_VERSION_ERROR }));
      }
    } catch (error) {
      yield put(updateVersionError(DEFAULT_UPDATE_VERSION_ERROR));
      yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_VERSION_ERROR }));
    }
  }
}


export function* sessionTimeoutSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { dispatch } = yield take(SESSION_TIMEOUT);

    try {
      const user = yield select(selectUser());
      const activeSession = yield select(selectActiveSession());
      if (user && user.session_timeout && activeSession && user.role && user.role !== "superAdmin") {
        const timeout = yield call(Timer, () => Localize(() => dispatch(sessionLoginSuccess()), 'activeSession', 0), (parseInt(user.session_timeout) * 1000));
        yield put(sessionTimeoutSuccess(timeout));
      }
    } catch (error) {
      yield put(sessionTimeoutError('sessionTimeout', DEFAULT_SESSION_TIMEOUT_ERROR));
    }
  }
}

export function* sessionLoginSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { identifier, secret, form } = yield take(SESSION_LOGIN);
    yield put(startSubmit(form));

    try {
      let result;
      const login_platform = store2.get('login_platform') || false;
      if(login_platform && ['google', 'microsoft'].includes(login_platform)) {
        const submitRecord = Object.assign({}, { email: identifier, secret: false, queryParams: false, sessionLogin: true });
        result = yield call(authLogin, submitRecord);
      } else {
        result = yield call(logIn, identifier, secret, false, true);
      }
      
      if (result) {
        setAuthToken(result.authToken);
        yield put(verifySessionAction(result.authToken));
        store2.set('secret', result.authToken);
        store2.set('activeSession', 1);
        yield put(sessionLoginSuccess(1));
        if(['google', 'microsoft'].includes(login_platform)) {
          store2.set('login_platform', login_platform);
        } else {
          store2.set('login_platform', 'default');
        }

      }
    } catch (error) {
      const login_attempts = error.response && error.response.data && error.response.data.login_attempts || false;
      if (login_attempts && login_attempts >= 3 && form != 'loginFailureForm') {
        yield put(logOutSuccess());
        yield put(push(process.env.PUBLIC_PATH || '/'));
        store2.remove('secret');
        store2.remove('activeSession');
      } else {
        const Err = login_attempts === 1 ? `Wrong Password. You have 2 left attempts` : login_attempts === 2 ? `Wrong Password. You have 1 left attempt` : 'Something Went Wrong!';
        yield put(sessionLoginError(form, Err));
        yield put(stopSubmit(form, { _error: Err }));
      }
    }
  }
}

export function* setupStripePaymentSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { record, form, locationState } = yield take(SETUP_STRIPE_PAYMENT);
    const { user = {}, secret } = locationState || {};
    yield put(startSubmit(form));

    if (record) {
      try {
        if (record && record.plan_id === 'free_trial') {
          if (user && secret) {
            yield put(createSubscriptionPlanAction(Object.assign({}, { plan_id: record.plan_id, page: record.page, selected_plan: record.selected_plan, plan_category: record.plan_category, discount_code: record.discount_code }), user, secret));
          }
          // store2.set('secret', secret);
          // yield put(signUpSuccess(user, secret));
          // yield put(verifySessionAction(secret));
          // yield put(push({ pathname: process.env.PUBLIC_PATH || '/' }));
        } else if (record && !record.error) {
          const { setupIntent = {} } = record;
          const result = yield call(setupStripePayment, Object.assign({}, { payment_method: setupIntent.payment_method }));
          if (result && user && secret) {
            yield put(createSubscriptionPlanAction(Object.assign({}, { plan_id: record.plan_id, page: record.page, selected_plan: record.selected_plan, plan_category: record.plan_category, discount_code: record.discount_code }), user, secret));
          } else {
            yield put(setupStripePaymentError(DEFAULT_SETUP_STRIPE_PAYMENT_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_SETUP_STRIPE_PAYMENT_ERROR }));
          }
        } else {
          yield put(setupStripePaymentError(DEFAULT_SETUP_STRIPE_PAYMENT_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_SETUP_STRIPE_PAYMENT_ERROR }));
        }

      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SETUP_STRIPE_PAYMENT_ERROR;
        yield put(setupStripePaymentError(Err));
        yield put(stopSubmit(form, { _error: Err }));
      } finally {
        yield put(stopSubmit(form));
      }
    }
  }
}

export function* createSubscriptionPlanSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { record, user, secret } = yield take(CREATE_SUBSCRIPTION_PLAN);
    if (record) {
      try {
        const result = yield call(createSubscriptionPlan, Object.assign({}, record, { subscription_id: user.subscription_id }));

        if (result && user && secret) {
          yield put(createSubscriptionPlanSuccess('Successfully Created Subscription Plan.'));
          store2.set('secret', secret);
          yield put(signUpSuccess(user, secret));
          yield put(verifySessionAction(secret));
          yield put(signUpDiscountCodeSuccess([]));
          yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }) }));
        } else {
          yield put(createSubscriptionPlanError(DEFAULT_CREATE_SUBSCRIPTION_PLAN_ERROR));
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_SUBSCRIPTION_PLAN_ERROR;
        yield put(createSubscriptionPlanError(Err));
      }
    }
  }
}


export function* loadSettingsPlanSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const load = yield take(LOAD_SETTINGS_PLAN);
    if (load) {
      try {
        const page = history && history.location && history.location.pathname && (['/', '/login', '/signin', '/signup', '/respond/', '/google-oauth2', '/microsoft-oauth2'].includes(history.location.pathname) ? 'homePage' : (history.location.pathname.indexOf('/litify') > -1) ? 'homePage' :  history.location.pathname);
        const result = yield call(loadSignUpPlan, page);

        if (result) {
          let plans = result.filter(_ => !['responding_vip', 'propounding_vip'].includes(_.plan_type));
          plans = lodash.orderBy(result.filter(_ => _.active && _.plan_type && !['pay_as_you_go', 'tek_as_you_go', 'propounding_monthly_199', 'propounding_yearly_2199'].includes(_.plan_type.toLowerCase())), ['order'], ['asc']).map(r =>  Object.assign({}, r, { label: r.name, value: r.plan_id }));
          yield put(loadSettingsPlanSuccess(Object.assign({}, { plans: plans })));
        } else {
          yield put(loadSettingsPlanError(DEFAULT_LOAD_SETTINGS_PLAN_ERROR));
        }
      } catch (error) {
        yield put(loadSettingsPlanError(DEFAULT_LOAD_SETTINGS_PLAN_ERROR));
      }
    }
  }
}


export function* updatePracticeObjectionsSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const update = yield take(UPDATE_PRACTICE_OBJECTIONS);
    const { record } = update || {};
    if (record) {
      try {
        const result = yield call(updatePracticeObjections, record);
        if (result) {
          yield put(updatePracticeObjectionsSuccess(record, 'Practice level objections updated.'));
        } else {
          yield put(updatePracticeObjectionsError(DEFAULT_UPDATE_PRACTICE_OBJECTIONS_ERROR));
        }
      } catch (error) {
        yield put(updatePracticeObjectionsError(DEFAULT_UPDATE_PRACTICE_OBJECTIONS_ERROR));
      }
    }
  }
}


export function* verifyOtpSaga() {
  while (true) {
    const { email, otp, form, user_from, integrationParams } = yield take(VERIFY_OTP);
    yield put(startSubmit(form));

    try {
      const result = yield call(verifyOtp, email, otp, user_from);
      store2.set('secret', result.authToken);
      setAuthToken(result.authToken);

      yield put(verifyOtpSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken));
      yield put(verifySessionAction(result.authToken));
      if(integrationParams){
        yield put(litifyCaseCreateAction(integrationParams));
      } else {
        yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }) }));
      }
    } catch (error) {
      const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_VERIFY_OTP_ERROR;
      yield put(verifyOtpError(Err));
    } finally {
      yield put(stopSubmit(form));
    }
  }
}


export function* resendOtpSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { identifier, secret, setSpinner } = yield take(RESEND_OTP);
    try {
      const result = yield call(logIn, identifier, secret);
      if (result) {
        yield put(resendOtpSuccess("Email Sent."));
        yield call(setSpinner, false);
      } else {
        yield put(resendOtpError(DEFAULT_RESEND_OTP_ERROR));
      }
    } catch (error) {
      const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_RESEND_OTP_ERROR;
      yield put(resendOtpError(Err));
    }
  }
}



export function* twoFactorAuthenticationSaga() {

  while (true) {
    const { record = {}, form } = yield take(TWO_FACTOR_AUTHENTICATION);
    yield put(startSubmit(form));

    try {
      const result = yield call(twoFactorAuthentication, record);
      if (result) {
        const user = yield select(selectUser());
        yield put(twoFactorAuthenticationSuccess(Object.assign({}, user, { twofactor_status: record.twofactor }), result.message));
        yield put(stopSubmit(form));
      } else {
        yield put(twoFactorAuthenticationError(DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR }));
      }
    } catch (error) {
      const Error = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR;
      yield put(twoFactorAuthenticationError(Error));
      yield put(stopSubmit(form, { _error: Error }));
    }
  }
}



export function* routerSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    yield take(ROUTER);
    const secret = store2.get('secret');
    const token = yield select(selectToken());

    const loggedIn = yield select(selectLoggedIn());
    if (secret || token) {
      try {
        const user = yield select(selectUser());
        
        if(user && user.role && ['superAdmin', 'manager'].includes(user.role)) {
          yield put(dashboardClearFilterAction(true));
        }
        setAuthToken(secret || token);
        yield call(verifySession);
        if(loggedIn && user && user.role && ['lawyer', 'paralegal'].includes(user.role)) {
          const result = yield call(getAllPracticesList);
          if(result) {
            const practicesData = result && result?.length > 0 && result.map((practice) => Object.assign({}, {
              name: practice.practice_name,
              label: practice.practice_name,
              value: practice.practice_id,
              logo: practice.logo,
              user_id : practice.id,
              email: practice.email
            })).sort((a, b) => a.name.localeCompare(b.name)) || [];
            yield put(getAllMyPracticesAction(Object.assign({}, { practicesData })));
          }
        }
      } catch (error) {
        const session_error = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_SESSION_TOKEN_ERROR;
        if (loggedIn && session_error === "invalid auth token") {
          const record = yield select(selectUser());
          yield put(sessionTokenAction(record));
        }
      }
    }
  }
}


export function* sessionClearTimeoutSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    yield take(SESSION_CLEAR_TIMEOUT);

    try {
      const sessionTimeout = yield select(selectTimeout());
      if (sessionTimeout) {
        clearTimeout(sessionTimeout);
        yield put(sessionClearTimeoutSuccess());
      }
    } catch (error) {
      yield put(sessionClearTimeoutError('sessionTimeout', DEFAULT_SESSION_CLEAR_TIMEOUT_ERROR));
    }
  }
}


export function* sessionTokenSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, respondCase, integration, model = false } = yield take(SESSION_TOKEN);
    const path = history && history.location.pathname;

    if (record) {
      try {
        const result = yield call(sessionToken, record);
        if (result) {
          // Decrypt
          var bytes = CryptoJS.AES.decrypt(result.authToken, record.id);
          var secret = bytes.toString(CryptoJS.enc.Utf8);
          
          setAuthToken(secret);
          store2.set('secret', secret);
          yield put(sessionTokenSuccess(secret));
          if (integration) {
            yield put(push({ pathname: '/casesCategories/cases', state: Object.assign({}, { ...history.location.state }, { integrationModal: model }) }));
          } else {
            let pathname = ['/respond/'].includes(path) ? '/' : path;
            yield put(push({ pathname: pathname || process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }, { respondCase: respondCase || false }) }));
          }
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error || DEFAULT_SESSION_TOKEN_ERROR;
        yield put(sessionTokenError('sessionTimeout', Err));
      }
    }
  }
}



export function* loadAppVersionSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const version = yield take(LOAD_APP_VERSION);

    if(version) {
      try {
        let result = yield call(loadAppVersion);
        if(result) {
          yield put(loadAppVersionSuccess(result.version));
        }
      } catch(e) {
        yield put(loadAppVersionError('Failed to load app version'));
      }
    }
  }
}

export function* updateSignatureSaga() {
  yield takeLatest(UPDATE_SIGNATURE, function* updater({ record, form }) {
    if(record) {
      yield put(startSubmit(form));
      try {
        const userInfo = yield select(selectUser());
        let signature;
        if(record.signature) {
          const userId = userInfo && userInfo.id;
          const encodeBase64 = base64.encode(record.signature);
          signature = CryptoJS.AES.encrypt(encodeBase64, userId).toString();;
        } else {
          signature = null;
        }
        let res = yield call(updateSignature, Object.assign({}, { signature }));
        if(res) yield put(updateSignatureSuccess('Signature updated successfully'));
        yield put(stopSubmit(form));
        const user = yield call(verifySession);
        if(user) {
          yield put(verifySessionSuccess(user));
        }
        
      } catch (err) {
        yield put(stopSubmit(form, { _error: 'Failed to update Signature' }));
        yield put(updateSignatureError('Failed to update Signature'));
      }
    }
  })
}


export function* loadRespondDetailSaga() {
  yield takeLatest(LOAD_RESPOND_DETAIL, function* updater({ secret, activeSession, user }) {
    let pathname = history && history.location && history.location.pathname || '/';
    let searchParams = history && history.location && history.location.search;
    let token_id = searchParams && searchParams.includes('=') && searchParams.split('=');
    token_id = token_id && Array.isArray(token_id) && token_id.length > 0 && token_id[1];
    if(token_id) {
      try { 
        let opposing_user = yield call(respondDetail, token_id);
        let userAccount = opposing_user && opposing_user.is_new_user || false;
        if(opposing_user) {
          yield put(loadRespondDetailSuccess(opposing_user));
          if(secret && user) {
            if(token_id) {
              if(user && opposing_user && user.email && opposing_user.responder_email && user.email === opposing_user.responder_email) {
                yield put(verifySessionSuccess(user));
                yield put(sessionTokenAction(user, true));
                if (activeSession != null) {
                  yield put(sessionLoginSuccess(activeSession));
                }
              } else {
                store2.remove('secret');
                store2.remove('activeSession');
                yield put(logOutSuccess());
                yield put(push({ pathname: `${pathname}${searchParams}`, state: Object.assign({}, { ...history.location.state},  { form: userAccount ? 'register' : 'login', respondCase: true })}));
              }
            }
          } else {
            if(opposing_user) {
              yield put(push({ pathname: `${pathname}${searchParams}`, state: Object.assign({}, { ...history.location.state },  { form: userAccount ? 'register' : 'login', respondCase: true })}))
            }
          }
        } else {
          store2.remove('secret');
          store2.remove('activeSession');
          yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
          yield put(push(process.env.PUBLIC_PATH || '/'));
        }
      } catch (err) {
        store2.remove('secret');
        store2.remove('activeSession');
        yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
        yield put(push(process.env.PUBLIC_PATH || '/'));
      }
    }
  })
}

export function* singUpDiscountCodeSaga() {
  yield takeLatest(SIGN_UP_DISCOUNT_CODE, function* updater({ record, form, dialog, cardDetails, handleSubmit }) {
    if(record) {
      yield put(startSubmit(form));
      try {
        if(record && record.discount_code && record.discount_code !== '') {
          const res = yield call(validateDiscountCode, Object.assign({}, { discount_code: record.discount_code, plan_id: record.plan_id }));
          if(res && res.length > 0 && Array.isArray(res)) {
            yield put(signUpDiscountCodeSuccess(res));
          }
        }

        if(dialog && cardDetails) {
          yield call(dialog, true);
        } else {
          yield call(handleSubmit)
        }
      } catch (error) {
        yield put(signUpDiscountCodeError('This promo code is invalid.'));
        yield put(stopSubmit(form, { _error: 'This promo code is invalid.' }));
      } finally {
        yield put(stopSubmit(form));
      }
    }
  })
}

export function* myCaseAccessTokenSaga() {
  yield takeLatest(MYCASE_ACCESS_TOKEN, function* updater({ searchParams, loadRecord }) {
    let auth_code = searchParams && searchParams.includes('=') && searchParams.split('=');
    auth_code = auth_code && Array.isArray(auth_code) && auth_code.length > 0 && auth_code[1];

    if (auth_code) {
      const activeSession = store2.get('activeSession');
      try {
        const user = yield call(verifySession);
        if(user) {
          yield put(verifySessionSuccess(user));
          yield put(sessionTokenAction(user, true, true, 'mycase'));
          yield put(disableAuthPageLoaderAction(false));
          if (activeSession != null) {
            yield put(sessionLoginSuccess(activeSession));
          }
          let responseData = yield call(myCaseAccessToken, { code: auth_code });
          if (responseData && responseData.status) {
            if(loadRecord) {
              yield put(myCaseLoadRecordsAction(true, false, false));
            }
          }
        }
      } catch (err) {
        yield put(myCaseAccessTokenError('Something went wrong!'));
      } finally {
        store2.remove('disable_mycase_load_record');
      }
    }
  })
}

export function* myCaseLoadRecordsSaga() {
  yield takeLatest(MYCASE_LOAD_RECORDS, function* updater({ load, setMyCasePopup, setStepper }) {
    if (load) {
      try {
        yield put(updateKey(0));
        if(setStepper){
          yield call(setStepper, 'mycase');
        }
        const casesRemotes = yield call(appRemotes, 'rest/cases');
        const cases = yield call(casesRemotes.loadRecords);
        const filter_case_records = cases && cases.filter(_ => _.integration_case_id) || [];
        const filter_case = filter_case_records && filter_case_records.length > 0 && filter_case_records.map(el => el.integration_case_id);

        let clients = [];
        let clients_cases = [];
        let finalRecord = [];
        let token = false

        yield put(updateKey(1));
        while (true) {
          try {
            const contacts = yield call(myCaseClients, { type: 'Client', token: token });
            if (contacts && contacts.clientDetails && contacts.clientDetails.length > 0) {
              const activeClients = contacts.clientDetails.reduce((acc, el) => {
                if(el && !el.archived) {
                  acc.push(el)
                }
                return acc;
              }, [])

              if(activeClients && activeClients.length > 0) {
                clients.push(...activeClients)
              }

              if (!contacts.hasMore) {
                token = false;
                break;
              } else {
                token = contacts.token;
              }
            }
            yield delay(500);
          } catch (error) {
            yield put(myCaseLoadRecordsError('Failed to fetching client and cases'));
            break;
          }
        }

        yield put(updateKey(2));
        while (true) {
          try {
            const cases = yield call(myCaseClients, { type: 'Cases', token: token });
            if (cases && cases.caseDetails && cases.caseDetails.length > 0) {
              const activeCases = cases.caseDetails.reduce((acc, el) => {
                if(el && el.status === "open") {
                  acc.push(el)
                }
                return acc;
              }, [])

              if(activeCases && activeCases.length > 0) {
                clients_cases.push(...activeCases)
              }

              if (!cases.hasMore) {
                token = false;
                break;
              } else {
                token = cases.token;
              }
            }
            yield delay(500);
          } catch (error) {
            yield put(myCaseLoadRecordsError('Failed to fetching client and cases'));
            break;
          }
        }

        if (clients && clients.length > 0 && clients_cases && clients_cases.length > 0) {
          for (let i = 0; i < clients_cases.length; ++i) {
            let updatedItem = clients_cases[i];
            let newArray = [];
            for (let j = 0; j < clients.length; ++j) {
              var origItem = clients[j];
              const personId = origItem && origItem.id;
              const clientId = updatedItem && updatedItem.clients && updatedItem.clients[0] && updatedItem.clients[0].id;
              if (personId == clientId) {
                newArray.push(Object.assign({}, updatedItem));
                break;
              }
            }
            let result = Object.assign({}, origItem, { cases: newArray });
            finalRecord.push(result);
          }
        }

        if (finalRecord && finalRecord.length > 0) {
          yield put(updateKey(2));
          let submitRecord = finalRecord.map((el, i) => {
            let case_title = el && el.cases && el.cases[0] && el.cases[0].name;
            let case_id = el && el.cases && el.cases[0] && el.cases[0].id && el.cases[0].id;
            // let address_details = el && el.address && Object.keys(el.address).length > 0 && el.address || '';
            let client_name = el.middle_name && `${el.first_name} ${el.middle_name} ${el.last_name}` || `${el.first_name} ${el.last_name}`;

            return Object.assign({}, el, { name: client_name, email: el.email && el.email.length > 0 ? el.email : '', phone: el.cell_phone_number && el.cell_phone_number.length > 0 ? el.cell_phone_number : '', dob: el.birthdate || '', case_title: case_title, client_id: el.id, id: i + 1, project_id: case_id, case_id: case_id })
          });
          submitRecord = submitRecord.filter(_ => _.cases && _.cases.length > 0);
          submitRecord = submitRecord && submitRecord.length > 0 && lodash.orderBy(submitRecord,  ['created_at'], ['desc']);

          const finalSubmitRecord = filter_case && filter_case.length > 0 ? submitRecord.reduce((acc, el) => {
            let res = filter_case.find(_ => _ == el.case_id);
            if (!res) {
              acc.push(el);
            }
            return acc;
          }, []) : submitRecord;


          const syncRecord = filter_case && filter_case.length > 0 && submitRecord.reduce((acc, el) => {
            let res = filter_case.find(_ => _ == el.case_id);
            if (res) {
              acc.push(el);
            }
            return acc;
          }, []);

          if (syncRecord && Array.isArray(syncRecord) && syncRecord.length > 0) {
            let loop = 0;
            let currentIndex = 1;
            let totalSize = 25;
            const totalSlice = syncRecord.length % totalSize == 0 ? Math.floor(syncRecord.length / totalSize) : Math.floor((syncRecord.length / totalSize) + 1);
      
            while (loop < totalSlice) {
              const firstIndex = (currentIndex - 1) * totalSize;
              const lastIndex = firstIndex + totalSize;
              const saveRecords = syncRecord.slice(firstIndex, lastIndex);
              const result = yield call(syncMycaseRecords, saveRecords);

              if (result) {
                loop++;
                currentIndex++;
              } else {
                yield put(myCaseLoadRecordsError('Failed to save records'));
              }
            }
          }

          if (finalSubmitRecord && finalSubmitRecord.length > 0) {
            yield put(myCaseLoadRecordsSuccess(finalSubmitRecord));
            yield put(updateKey(3));
            if (setMyCasePopup) {
              yield call(setMyCasePopup, true);
            }
          } else {
            yield put(myCaseLoadRecordsSuccess([]));
            yield put(myCaseLoadRecordsError(DEFAULT_MYCASE_RECORDS_ERROR));
            yield delay(500);
            const secret = store2.get('secret');
            yield put(verifySessionAction(secret));
          }
        }
      } catch (error) {
        yield put(myCaseLoadRecordsError('Failed to fetching client and cases'));
      } finally {
        if (setStepper) {
          yield call(setStepper, false);
        }
      }
    }
  })
}

export function* authenticationSaga() {
  yield takeLatest(AUTHENTICATION, function* updater({ record }) {
    if(record) {
      try {
        const result = record;
        const user_from = result?.user?.user_from || false;
        const identifier = result?.user?.email;
        if (result?.user?.two_factor) {
          yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }, { form: 'verifyOtp', identifier, respondCase: false }) }));
        } else if(result?.user?.newUserLogin) {
          yield put(authenticationSuccess(result.user));
          yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }, { respondCase: false }, { form: 'authConfirm', identifier, respondCase: false, user_from }) }));
        } else {
          store2.set('secret', result.authToken);
          setAuthToken(result.authToken);
          yield put(logInSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken));
          yield put(verifySessionAction(result.authToken));
          yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }, { respondCase: false }) }));
        }
      } catch (error) {
        const session_error = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_AUTH_ERROR;
        store2.remove('secret');
        store2.remove('activeSession');
        yield put(push(process.env.PUBLIC_PATH || '/'));
        yield delay(500);
        yield put(googleAuthError(session_error));
      } finally {
        yield put(disableAuthPageLoaderAction(false));
      }
    }
  })
}

export function* googleAuthSaga() {
  yield takeLatest(GOOGLE_AUTH, function* updater({ searchParams }) {
    if(searchParams) {
      const code = getAuthCode(searchParams);
      if(code) {
        try { 
          let result = yield call(googleAuth, Object.assign({}, { code }));
          if(result) {
            store2.set('login_platform', 'google');
            yield put(authenticationAction(result));
          }
        } catch (error) {
          const session_error = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_GOOGLE_AUTH_ERROR;
          store2.remove('secret');
          store2.remove('activeSession');
          yield put(push(process.env.PUBLIC_PATH || '/'));
          yield delay(500);
          yield put(googleAuthError(session_error));
          yield put(disableAuthPageLoaderAction(false));
        }
      }
    }
  })
}

export function* microsoftAuthSaga() {
  yield takeLatest(MICROSOFT_AUTH, function* updater({ searchParams }) {
    if(searchParams) {
      const code = searchParams && getAuthCode(searchParams);
      if(code) {
        try { 
          let result = yield call(microsoftAuth, Object.assign({}, { code }));
          if(result) {
            store2.set('login_platform', 'microsoft');
            yield put(authenticationAction(result));
          }
        } catch (error) {
          const err = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_MICROSOFT_AUTH_ERROR;
          store2.remove('secret');
          store2.remove('activeSession');
          yield put(push(process.env.PUBLIC_PATH || '/'));
          yield delay(500);
          yield put(microsoftAuthError(err));
          yield put(disableAuthPageLoaderAction(false));
        }
      }
    }
  })
}

export function* authSignUpSaga() {
  yield takeLatest(AUTH_SIGN_UP, function* updater({ record, form }) {
    if(record) {
      yield put(startSubmit(form));
      try {
        const result = yield call(authSignUp, Object.assign({}, { ...record }));
        if(result) {
          store2.set('secret', result.authToken);
          store2.set('getting_started', true);
          setAuthToken(result.authToken);
          yield put(logInSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken));
          yield put(verifySessionAction(result.authToken));
          yield put(authenticationSuccess(false));
          yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state, form: false }, { respondCase: false }) }));
        }
      } catch (error) {
        yield put(authSignUpError(error));
        store2.remove('login_platform');
      } finally {
        yield put(stopSubmit(form));
      }
    }
  })
}

export function* emailValidationSaga() {
  yield takeLatest(EMAIL_VALIDATION, function* updater({ record, routePath, form, integrationParams }) {
    if (record) {
      yield put(startSubmit(form));
      try {
        const result = yield call(emailValidation, record);
        if (result) {
          let pathUrl = routePath && integrationParams ? `${routePath}${integrationParams}` : routePath || (integrationParams && integrationParams || process.env.PUBLIC_PATH || '/');
          yield put(push({ pathname: pathUrl, state: Object.assign({}, { ...history.location.state, form: 'register',  email: record.email }) }));
        }
      } catch (error) {
        yield put(emailValidationError(error));
      } finally {
        yield put(stopSubmit(form));
      }
    }
  })
}

export function* dashboardClearFilterSaga() {
  yield takeLatest(DASHBOARD_CLEAR_FILTER, function* updater({ load }) {
    if (load) {
      try {
        const page = history && history.location && history.location.pathname;
        const createdDateFilter = yield select(selectCreatedDateFilter());
        const activeFilter = yield select(selectActiveFilter());
        const restrictedPath = ['/practices', '/practice/users', '/admin/history'].filter(substring => page.includes(substring));

        if (history && history.action != 'POP' && restrictedPath && restrictedPath.length == 0) {
          yield put(updateDashboardTabValue(0));
          yield put(updateFilterData({ "statistics": false }));
          yield put(updateCustomFilterData({}));
        }

        if (!lodash.isEmpty(createdDateFilter) && !lodash.isEmpty(activeFilter) && restrictedPath && restrictedPath.length == 0) {
          yield put(updateDashboardFilterData(false));
          yield put(updateActiveCustomFilter({}, {}));
        }
      } catch (error) {
        yield put(dashboardClearFilterError(error));
      }
    }
  })
}

export function* getLitifyRecordSaga() {
  yield takeLatest(GET_LITIFY_RECORD, function* updater({ secret, activeSession, user, searchParams }) {
    let integrationCaseId = searchParams && searchParams.includes('=') && searchParams.split('=');
    integrationCaseId = integrationCaseId && Array.isArray(integrationCaseId) && integrationCaseId.length > 0 && integrationCaseId[1];
    let pathname = history && history.location && history.location.pathname || '/';

    if (integrationCaseId) {
      const activeSession = store2.get('activeSession');
      try {
        if (secret && user) {
          if (user) {
            yield put(verifySessionSuccess(user));
            if (!activeSession) {
              store2.set('activeSession', 1);
              yield put(sessionLoginSuccess(1));
            }
            yield put(disableAuthPageLoaderAction(false));
            let result = yield call(getLitifyRecord, { integration_case_id: integrationCaseId });
            if (result) {
              const { ResponseData } = result;
              const caseDetails = ResponseData && ResponseData.case && Object.keys(ResponseData.case).length > 0 && ResponseData.case;
              yield put(push({ pathname: `/casesCategories/cases/${caseDetails.id}/form`, state: Object.assign({}, { ...history.location.state }) }));
            }
          }
        } else {
          yield put(disableAuthPageLoaderAction(false));
          yield put(push({ pathname: `${pathname}${searchParams}`, state: Object.assign({}, { ...history.location.state }, { form: 'login' }) }));
        }
      } catch (error) {
        const errorMessage = error.response && error.response.data && error.response.data && error.response.data.error || 'Something went wrong!';
        yield put(push({ pathname: `/`, state: Object.assign({}, { ...history.location.state }) }));
        yield put(getLitifyRecordError(errorMessage));
      }
    }
  })
}

export function* litifyCaseCreateSaga() {
  yield takeLatest(LITIFY_CASES_CREATE, function* updater({ litifyParams }) {
    if (litifyParams) {
      try {
        let integrationCaseId = litifyParams && litifyParams.includes('=') && litifyParams.split('=');
        integrationCaseId = integrationCaseId && Array.isArray(integrationCaseId) && integrationCaseId.length > 0 && integrationCaseId[1];
        if (integrationCaseId) {
          const caseResponse = yield call(getLitifyRecord, { integration_case_id: integrationCaseId });
          yield delay(500);
          const { ResponseData } = caseResponse;
          const caseDetails = ResponseData && ResponseData.case && Object.keys(ResponseData.case).length > 0 && ResponseData.case;
          if (caseResponse && caseDetails) {
            let pathUrl = caseDetails && Object.keys(caseDetails).length > 0 && `/casesCategories/cases/${caseDetails.id}/form`;
            yield put(push({ pathname: pathUrl || '/', state: Object.assign({}, { ...history.location.state }) }));
          } else {
            yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }) }));
            yield put(litifyCaseCreateError('Something went wrong!'));
          }
        } else {
          const secret = store2.get('secret');
          const user = yield call(verifySession);
          yield put(getLitifyBulkRecordsAction(secret, user, true));
        }
      } catch (error) {
        const errorMessage = error.response && error.response.data && error.response.data && error.response.data.error || 'Something went wrong!';
        yield put(push({ pathname: `/`, state: Object.assign({}, { ...history.location.state }) }));
        yield put(litifyCaseCreateError(errorMessage));
      }
    }
  })
}

export function* getLitifyBulkRecordsSaga() {
  yield takeLatest(GET_LITIFY_BULK_RECORDS, function* updater({ secret, user, load, path }) {
    if (load) {
      const activeSession = store2.get('activeSession');
      try {
        if (secret && user) {
          yield put(push({ pathname: `/casesCategories/cases`, state: Object.assign({}, { ...history.location.state }) }));

          const casesRemotes = yield call(appRemotes, 'rest/cases');
          const cases = yield call(casesRemotes.loadRecords);
          const filter_case_records = cases && cases.filter(_ => _.integration_case_id && _.case_from == 'litify') || [];
          const filter_case = filter_case_records && filter_case_records.length > 0 && filter_case_records.map(el => el.integration_case_id);

          if (user) {
            yield put(verifySessionSuccess(user));
            yield put(sessionTokenAction(user, true, true, 'litify'));
            if (!activeSession) {
              store2.set('activeSession', 1);
              yield put(sessionLoginSuccess(1));
            }
            yield put(disableAuthPageLoaderAction(false));

            let clients_cases = [];
            let currentIndex = 0;
            let totalSize = 100;

            while (true) {
              try {
                let firstIndex = currentIndex * totalSize;
                let lastIndex = totalSize;
                const getRecords = yield call(getLitifyBulkRecords, firstIndex, lastIndex);
                if (getRecords && Object.keys(getRecords).length > 0) {
                  const { caseDetails, hasMore } = getRecords;
                  const { records } = caseDetails;
                  records && records.length > 0 && clients_cases.push(...records);
                  if (!hasMore) {
                    currentIndex = 0;
                    totalSize = 100;
                    break;
                  } else {
                    currentIndex++;
                  }
                }
                yield put(updateKey(1));
                yield delay(500);
              } catch (error) {
                yield put(getLitifyBulkRecordsError('Failed to fetching client and cases'));
                return;
              }
            }

            if (clients_cases && clients_cases.length > 0) {
              yield put(updateKey(2));

              let submitRecord = clients_cases.map((el, i) => {
                delete el.attributes;
                let case_title = el && el.litify_pm__Case_Title__c;
                let case_id = el && el.Id;
                let clientRecord = el && el.litify_pm__Client__r;
                delete clientRecord.attributes;

                let client_name = el && clientRecord && clientRecord.first_name && clientRecord.last_name && `${clientRecord.first_name} ${clientRecord.last_name}` || clientRecord.Name;
                let clientEmail = clientRecord && clientRecord.litify_pm__Email__c || '';
                let clientPhone = clientRecord && clientRecord.litify_pm__Phone_Mobile__c || '';
                let clientDob = clientRecord && clientRecord.litify_pm__Date_of_birth__c || '';

                return Object.assign({}, el, { ...clientRecord }, { name: client_name, email: clientEmail, phone: clientPhone, dob: clientDob, case_title: case_title, client_id: clientRecord.Id, id: i + 1, project_id: case_id, case_id: case_id })
              });

              submitRecord = submitRecord && submitRecord.length > 0 && lodash.orderBy(submitRecord, ['CreatedDate'], ['desc']);

              const finalSubmitRecord = filter_case && filter_case.length > 0 ? submitRecord.reduce((acc, el) => {
                let res = filter_case.find(_ => _ == el.case_id);
                if (!res) {
                  acc.push(el);
                }
                return acc;
              }, []) : submitRecord;

              const syncRecord = filter_case && filter_case.length > 0 && submitRecord.reduce((acc, el) => {
                let res = filter_case.find(_ => _ == el.project_id);
                if (res) {
                  acc.push(el);
                }
                return acc;
              }, []);

              if (syncRecord && Array.isArray(syncRecord) && syncRecord.length > 0) {
                let loop = 0;
                let currentIndex = 1;
                let totalSize = 10;
                const totalSlice = syncRecord.length % totalSize == 0 ? Math.floor(syncRecord.length / totalSize) : Math.floor((syncRecord.length / totalSize) + 1);

                while (loop < totalSlice) {
                  const firstIndex = (currentIndex - 1) * totalSize;
                  const lastIndex = firstIndex + totalSize;
                  const saveRecords = syncRecord.slice(firstIndex, lastIndex);
                  const result = yield call(updateLitifyBulkRecords, saveRecords);

                  if (result) {
                    loop++;
                    currentIndex++;
                  } else {
                    yield put(getLitifyBulkRecordsError('Failed to save records'));
                  }
                }
              }

              if (finalSubmitRecord && finalSubmitRecord.length > 0) {
                yield put(updateKey(3));
                yield put(getLitifyBulkRecordsSuccess(finalSubmitRecord));
              } else {
                yield put(getLitifyBulkRecordsSuccess([]));
                yield put(getLitifyBulkRecordsError('No new records found in Litify'));
                yield delay(500);
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              }
            }
          }
        } else {
          yield put(disableAuthPageLoaderAction(false));
          yield put(push({ pathname: `${path}`, state: Object.assign({}, { ...history.location.state }, { form: 'login' }) }));
        }
      } catch (error) {
        const errorMessage = error.response && error.response.data && error.response.data && error.response.data.error || 'Something went wrong!';
        yield put(push({ pathname: `/`, state: Object.assign({}, { ...history.location.state }) }));
        yield put(getLitifyRecordError(errorMessage));
      }
    }
  })
}

export function* webAuthenticationSaga() {
  yield takeLatest(WEB_AUTHENTICATION, function* updater({ record, form, setAuthenticate }) {
    yield put(startSubmit(form));
    if (record) {
      try {
        let result = yield call(websiteAuthentication, record);
        if (result) {
          yield put(stopSubmit(form));
          store2.set('web_authenticate', true);
          yield call(setAuthenticate, false);
        } else {
          yield put(webAuthenticationError("Invalid Password"));
          yield put(stopSubmit(form, { _error: 'Invalid Password' }));
        }
      }
      catch (error) {
        yield put(webAuthenticationError("Invalid Password"));
        yield put(stopSubmit(form, { _error: 'Invalid Password' }));
      }
    }
  })
}

function* switchPracticeSaga() {
  yield takeLatest(SWITCH_PRACTICE, function* updater({ practice_data, form }) {
    if (practice_data) {
      try {
        yield put(startSubmit(form));
        const result = yield call(switchPractice, practice_data);
        if (result) {
          store2.set('secret', result.authToken);
          setAuthToken(result.authToken);
          yield put(switchPracticeSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken, "Practice Switched Successfully"));
          yield put(setSwitchPracticeRendering(Object.assign({}, {"clients" : true, "rest/users" : true, "rest/customer-objections" : true})));
          yield put(verifySessionAction(result.authToken));
          yield put(stopSubmit(form));
          yield put(push({ pathname: `/`, state: Object.assign({}, { ...history.location.state }) }));
          
          
        } else {
          yield put(switchPracticeError("Failed to switch practice"))
          yield put(stopSubmit(form, { _error: 'Failed to switch practice' }));
        }
      } catch (error) {
        const errorMessage = error.response && error.response.data && error.response.data && error.response.data.error || 'Failed to switch practice';
        yield put(switchPracticeError(errorMessage));
        yield put(stopSubmit(form, { _error: 'Failed to switch practice' }));
      }
    }
  })
}

export function* clioAccessTokenSaga() {
  yield takeLatest(CLIO_ACCESS_TOKEN, function* updater({ searchParams, loadRecord }) {
    let auth_code = searchParams && searchParams.includes('=') && searchParams.split('=');
    auth_code = auth_code && Array.isArray(auth_code) && auth_code.length > 0 && auth_code[1];

    if (auth_code) {
      const activeSession = store2.get('activeSession');
      try {
        const user = yield call(verifySession);
        if (user) {
          yield put(verifySessionSuccess(user));
          yield put(sessionTokenAction(user, true, true, 'clio'));
          yield put(disableAuthPageLoaderAction(false));
          if (activeSession != null) {
            yield put(sessionLoginSuccess(activeSession));
          }
          let responseData = yield call(clioAccessToken, { code: auth_code });
          if (responseData && responseData.status) {
            if (loadRecord) {
              yield put(clioLoadRecordsAction(true, false, false))
            } else {
              const secret = store2.get('secret');
              yield put(verifySessionAction(secret));
            }
          }
        }
      } catch (err) {
        yield put(clioAccessTokenError('Something went wrong!'));
      } finally {
        store2.remove('disable_mycase_load_record');
      }
    }
  })
}

export function* clioLoadRecordsSaga() {
  yield takeLatest(CLIO_LOAD_RECORDS, function* updater({ load, setClioPopup, setStepper }) {
    if (load) {
      try {
        if (setStepper) {
          yield call(setStepper, 'clio');
        }
        yield put(updateKey(0));
        const casesRemotes = yield call(appRemotes, 'rest/cases');
        const cases = yield call(casesRemotes.loadRecords);
        const filter_case_records = cases && cases.filter(_ => _.integration_case_id) || [];
        const filter_case = filter_case_records && filter_case_records.length > 0 && filter_case_records.map(el => el.integration_case_id);

        let clients_cases = [];
        let finalRecord = [];
        let token = false;

        yield put(updateKey(1));
        yield delay(500);
        while (true) {
          try {
            const cases = yield call(clioCaseClients, { type: 'Cases', token: token });
            if (cases && cases.caseDetails && cases.caseDetails.data && cases.caseDetails.data.length > 0) {
              const activeCases = cases.caseDetails.data.reduce((acc, el) => {
                if (el && el.status == "Open" && el?.client?.is_client) {
                  acc.push(el)
                }
                return acc;
              }, [])

              if (activeCases && activeCases.length > 0) {
                clients_cases.push(...activeCases)
              }

              if (!cases.hasMore) {
                token = false;
                break;
              } else {
                token = cases.token;
              }
            }
            else {
              token = false;
              yield put(clioLoadRecordsError('Failed to fetching client and cases'));
              break;
            }
            yield delay(500);
          } catch (error) {
            token = false;
            yield put(clioLoadRecordsError('Failed to fetching client and cases'));
            break;
          }
        }

        if (clients_cases && clients_cases.length > 0) {

          const clients = clients_cases.map(e => Object.assign({}, e.client));

          for (let i = 0; i < clients_cases.length; ++i) {
            let updatedItem = clients_cases[i];
            let newArray = [];
            for (let j = 0; j < clients.length; ++j) {
              var origItem = clients[j];
              const personId = origItem && origItem.id;
              const clientId = updatedItem && updatedItem.client && updatedItem.client.id;
              if (personId == clientId) {
                newArray.push(Object.assign({}, updatedItem));
                break;
              }
            }
            let result = Object.assign({}, origItem, { cases: newArray });
            finalRecord.push(result);
          }
        }

        if (finalRecord && finalRecord.length > 0) {
          yield put(updateKey(2));
          let submitRecord = finalRecord.map((el, i) => {
            let case_title = el && el.cases && el.cases[0] && el.cases[0].display_number && el.cases[0].description && `${el.cases[0].description}`;
            let case_id = el && el.cases && el.cases[0] && el.cases[0].id && el.cases[0].id;
            let client_name = el && el.type == 'Company' ? `${el.name}` : `${el.first_name} ${el.last_name}`;

            return Object.assign({}, el, { name: client_name, email: el?.primary_email_address || '', phone: el.primary_phone_number || '', dob: el.date_of_birth || '', address: null, case_title: case_title, client_id: el.id, id: i + 1, project_id: case_id, case_id: case_id, clio_email_reference_id: null, clio_phone_reference_id: null, clio_address_reference_id: null })
          });

          submitRecord = submitRecord.filter(_ => _.cases && _.cases.length > 0);
          submitRecord = submitRecord && submitRecord.length > 0 && lodash.orderBy(submitRecord, ['created_at'], ['desc']);

          const finalSubmitRecord = filter_case && filter_case.length > 0 ? submitRecord.reduce((acc, el) => {
            let res = filter_case.find(_ => _ == el.case_id);
            if (!res) {
              acc.push(el);
            }
            return acc;
          }, []) : submitRecord;

          yield put(updateKey(2));
          const syncRecord = filter_case && filter_case.length > 0 && submitRecord.reduce((acc, el) => {
            let res = filter_case.find(_ => _ == el.case_id);
            if (res) {
              acc.push(el);
            }
            return acc;
          }, []);

          if (syncRecord && Array.isArray(syncRecord) && syncRecord.length > 0) {
            let loop = 0;
            let currentIndex = 1;
            let totalSize = 25;
            const totalSlice = syncRecord.length % totalSize == 0 ? Math.floor(syncRecord.length / totalSize) : Math.floor((syncRecord.length / totalSize) + 1);

            while (loop < totalSlice) {
              const firstIndex = (currentIndex - 1) * totalSize;
              const lastIndex = firstIndex + totalSize;
              const saveRecords = syncRecord.slice(firstIndex, lastIndex);
              const result = yield call(syncClioRecords, saveRecords);
              if (result) {
                loop++;
                currentIndex++;
              } else {
                yield put(clioLoadRecordsError('Failed to save records'));
              }
            }
          }
 
          if (finalSubmitRecord && finalSubmitRecord.length > 0) {
            yield put(clioLoadRecordsSuccess(finalSubmitRecord));
            yield put(updateKey(3));
            if (setClioPopup) {
              yield call(setClioPopup, true);
            }
          } else {
            yield put(clioLoadRecordsSuccess([]));
            yield put(clioLoadRecordsError(DEFAULT_CLIO_RECORDS_ERROR));
            yield delay(500);
            const secret = store2.get('secret');
            yield put(verifySessionAction(secret));
          }
        } else {
          yield put(clioLoadRecordsError('No Records Found'));
        }
      } catch (error) {
        yield put(clioLoadRecordsError('Failed to fetching client and cases'));
      } finally {
        if (setStepper) {
          yield call(setStepper, false);
        }
      }
    }
  })
}

function* chatNotificationSaga() {
  yield takeLatest(CHAT_NOTIFICATION, function* updater({ record }) {
    if(record) {
      try {
        const result = yield call(chatNotification, record);
        if(result) {
          const user = yield call(verifySession);
          yield put(verifySessionSuccess(user));
          yield put(chatNotificationSuccess(true));
        }
      } catch (error) {
        yield put(chatNotificationError('Failed to update the notification status.'));
      }
    }
  })
}


export default function* rootSaga() {
  yield all([
    verifyInitialSessionSaga(),
    verifySessionSaga(),
    loginSaga(),
    logOutSaga(),
    signUpSaga(),
    forgotPasswordSaga(),
    resetPasswordSaga(),
    changePasswordSaga(),
    requestSupportSaga(),
    requestDemoSaga(),
    updateVersionSaga(),
    sessionTimeoutSaga(),
    sessionLoginSaga(),
    setupStripePaymentSaga(),
    createSubscriptionPlanSaga(),
    loadSettingsPlanSaga(),
    updatePracticeObjectionsSaga(),
    verifyOtpSaga(),
    resendOtpSaga(),
    twoFactorAuthenticationSaga(),
    routerSaga(),
    sessionClearTimeoutSaga(),
    sessionTokenSaga(),
    loadAppVersionSaga(),
    updateSignatureSaga(),
    loadRespondDetailSaga(),
    singUpDiscountCodeSaga(),
    myCaseAccessTokenSaga(),
    myCaseLoadRecordsSaga(),
    authenticationSaga(),
    googleAuthSaga(),
    microsoftAuthSaga(),
    authSignUpSaga(),
    emailValidationSaga(),
    dashboardClearFilterSaga(),
    getLitifyRecordSaga(),
    litifyCaseCreateSaga(),
    getLitifyBulkRecordsSaga(),
    webAuthenticationSaga(),
    switchPracticeSaga(),
    clioAccessTokenSaga(),
    clioLoadRecordsSaga(),
    chatNotificationSaga()
  ]);
}
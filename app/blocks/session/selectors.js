/*
 *
 *  session selectors
 *
 */

import { createSelector } from 'reselect';
import { Map } from 'immutable';

export const selectSession = () => (state) => state ? state.session : Map();

export const selectRouter = () => (state) => state ? state.router : Map();

export const selectForm = () => (state) => state ? state.form : Map();

export const selectLoggedIn = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.loggedIn || false,
);

export const selectToken = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.secret || false,
);

export const selectUser = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.user || {},
);

export const selectVersion = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.user && { version: sessionState.user.version || false } || {},
);

export const selectActiveSession = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.activeSession,
);

export const selectError = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.error || {},
);

export const selectSuccess = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.success || {},
);

export const selectSessionExpand = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.expand || false,
);

export const selectLocation = () => createSelector(
  selectRouter(),
  (routerState) => routerState && routerState.location || {},
);

export const selectTimeout = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.timeout || false,
);

export const selectPayment = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.payment || {},
);

export const selectLoading = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.loading || false,
);

export const selectMetaData = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.metaData || {},
);

export const selectAppVersion = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.appVersion || false,
);

export const selectRespondDetails = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.respondDetail || false,
);

export const selectDiscountDetail = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.discountDetail || false,
);

export const selectMyCaseRecords = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.myCaseRecords && sessionState.myCaseRecords || []
);

export const selectButtonLoader = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.buttonLoader || false
);

export const selectSessionkey = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.key || false
);

export const selectStepper = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.stepper || false
);

export const selectAuthUserDetails = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.authUser || false,
);

export const selectAuthPageLoader = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.authPageLoader || false,
);

export const selectLitifyRecords = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.litifyRecords || false,
);

export const selectClioRecords = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.clioRecords && sessionState.clioRecords || []
);

export const isSwitchPracticeRender = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.switchPracticeRender && sessionState.switchPracticeRender || false
);

export const selectNotificationLoader = () => createSelector(
  selectSession(),
  (sessionState) => sessionState && sessionState.notificationLoader || false,
);


export default {
  selectSession,
  selectToken,
  selectLoggedIn,
  selectUser,
  selectVersion,
  selectActiveSession,
  selectError,
  selectSuccess,
  selectSessionExpand,
  selectLocation,
  selectTimeout,
  selectForm,
  selectPayment,
  selectLoading,
  selectMetaData,
  selectAppVersion,
  selectRespondDetails,
  selectDiscountDetail,
  selectMyCaseRecords,
  selectButtonLoader,
  selectSessionkey,
  selectStepper,
  selectAuthUserDetails,
  selectAuthPageLoader,
  selectLitifyRecords,
  selectClioRecords,
  isSwitchPracticeRender,
  selectNotificationLoader
};

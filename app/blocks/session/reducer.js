/*
 *
 * Session reducer
 *
 */

import produce from 'immer';

import merge from 'lodash/merge';

import {
  VERIFY_SESSION_SUCCESS,
  VERIFY_SESSION_ERROR,
  LOG_IN_SUCCESS,
  LOG_OUT_SUCCESS,
  LOG_IN_ERROR,
  LOG_OUT_ERROR,
  SIGN_UP_SUCCESS,
  SIGN_UP_ERROR,
  FORGOT_PASSWORD_ERROR,
  FORGOT_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  CHANGE_PASSWORD_SUCCESS,
  REQUEST_SUPPORT_ERROR,
  REQUEST_SUPPORT_SUCCESS,
  REQUEST_DEMO_ERROR,
  REQUEST_DEMO_SUCCESS,
  UPDATE_VERSION_SUCCESS,
  SESSION_TIMEOUT_ERROR,
  SESSION_TIMEOUT_SUCCESS,
  SESSION_LOGIN_ERROR,
  SESSION_LOGIN_SUCCESS,
  GET_STRIPE_INDENT_ERROR,
  GET_STRIPE_INDENT_SUCCESS,
  SETUP_STRIPE_PAYMENT,
  SETUP_STRIPE_PAYMENT_ERROR,
  SETUP_STRIPE_PAYMENT_SUCCESS,
  CREATE_SUBSCRIPTION_PLAN_ERROR,
  CREATE_SUBSCRIPTION_PLAN_SUCCESS,
  LOAD_SETTINGS_PLAN_ERROR,
  LOAD_SETTINGS_PLAN_SUCCESS,
  UPDATE_PRACTICE_OBJECTIONS_ERROR,
  UPDATE_PRACTICE_OBJECTIONS_SUCCESS,
  VERIFY_OTP_ERROR,
  VERIFY_OTP_SUCCESS,
  RESEND_OTP_ERROR,
  RESEND_OTP_SUCCESS,
  TWO_FACTOR_AUTHENTICATION_SUCCESS,
  TWO_FACTOR_AUTHENTICATION_ERROR,
  CLEAR_CACHE,
  SESSION_EXPAND,
  SESSION_CLEAR_TIMEOUT_SUCCESS,
  SESSION_CLEAR_TIMEOUT_ERROR,
  SESSION_TOKEN_SUCCESS,
  SESSION_TOKEN_ERROR,
  LOAD_APP_VERSION_SUCCESS,
  LOAD_APP_VERSION_ERROR,
  UPDATE_SIGNATURE_SUCCESS,
  UPDATE_SIGNATURE_ERROR,
  LOAD_RESPOND_DETAIL_SUCCESS,
  LOAD_RESPOND_DETAIL_ERROR,
  SIGN_UP_DISCOUNT_CODE_SUCCESS,
  SIGN_UP_DISCOUNT_CODE_ERROR,
  MYCASE_LOAD_RECORDS,
  MYCASE_LOAD_RECORDS_SUCCESS,
  MYCASE_LOAD_RECORDS_ERROR,
  MYCASE_ACCESS_TOKEN_ERROR,
  UPDATE_KEY,
  AUTHENTICATION_SUCCESS,
  GOOGLE_AUTH_ERROR,
  MICROSOFT_AUTH_ERROR,
  EMAIL_VALIDATION_ERROR,
  GOOGLE_AUTH,
  MICROSOFT_AUTH,
  DISABLE_AUTH_PAGE_LOADER,
  MYCASE_ACCESS_TOKEN,
  GET_LITIFY_RECORD,
  GET_LITIFY_RECORD_ERROR,
  LITIFY_CASES_CREATE,
  LITIFY_CASES_CREATE_ERROR,
  GET_LITIFY_BULK_RECORDS,
  GET_LITIFY_BULK_RECORDS_SUCCESS,
  GET_LITIFY_BULK_RECORDS_ERROR,
  AUTH_SIGN_UP_ERROR,
  WEB_AUTHENTICATION_ERROR,
  SWITCH_PRACTICE_SUCCESS,
  SWITCH_PRACTICE_ERROR,
  CLIO_LOAD_RECORDS,
  CLIO_LOAD_RECORDS_SUCCESS,
  CLIO_LOAD_RECORDS_ERROR,
  CLIO_ACCESS_TOKEN,
  CLIO_ACCESS_TOKEN_ERROR,
  GET_ALL_MY_PRACTICES,
  SUBSCRIPTION_LICENSE_VALIDATION_SUCCESS,
  CLEAR_PRACTICE_SWITCH_RENDERING,
  SET_PRACTICE_SWITCH_RENDERING,
  CHAT_NOTIFICATION,
  CHAT_NOTIFICATION_SUCCESS,
  CHAT_NOTIFICATION_ERROR
} from './constants';


const initialState = { error: {}, success: {}, activeSession: 1, secret: false, version: '1.0', expand: false, timeout: false, loading: false, metaData: {}, appVersion: false, myCaseRecords: [], stepper: false, key: 0, authUser: false, litifyRecords: [], switchPracticeRender : false };

/**
 * @param {object} state 
 * @param {object} action 
 */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case VERIFY_SESSION_SUCCESS:
        draft.loggedIn = true;
        draft.user = action.user;
        draft.loading = false;
        break;
      case LOG_IN_SUCCESS:
      case SIGN_UP_SUCCESS:
      case VERIFY_OTP_SUCCESS:
        draft.loggedIn = true;
        draft.user = action.user;
        draft.secret = action.token;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case SESSION_TOKEN_SUCCESS:
        draft.secret = action.token;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case VERIFY_SESSION_ERROR:
      case LOG_IN_ERROR:
      case SIGN_UP_ERROR:
      case GET_STRIPE_INDENT_ERROR:
      case GOOGLE_AUTH_ERROR:
      case MICROSOFT_AUTH_ERROR:
      case EMAIL_VALIDATION_ERROR:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = { login: action.error };
        draft.success = {};
        draft.loading = false;
        break;
      case VERIFY_OTP_ERROR:
      case RESEND_OTP_ERROR:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = { verifyOtp: action.error };
        draft.success = {};
        draft.loading = false;
        break;
      case LOG_OUT_ERROR:
      case LOG_OUT_SUCCESS:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case FORGOT_PASSWORD_ERROR:
        draft.error = { forgot: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case FORGOT_PASSWORD_SUCCESS:
        draft.error = {}
        draft.success = { forgot: action.success };
        draft.loading = false;
        break;
      case RESET_PASSWORD_ERROR:
        draft.error = { reset: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case RESET_PASSWORD_SUCCESS:
        draft.error = {}
        draft.success = { reset: action.success };
        draft.loading = false;
        break;
      case RESEND_OTP_SUCCESS:
        draft.error = {}
        draft.success = { verifyOtp: action.success };
        draft.loading = false;
        break;
      case CHANGE_PASSWORD_ERROR:
        draft.error = { changePassword: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case CHANGE_PASSWORD_SUCCESS:
        draft.error = {}
        draft.success = { changePassword: action.success };
        draft.loading = false;
        break;
      case SETUP_STRIPE_PAYMENT_ERROR:
      case CREATE_SUBSCRIPTION_PLAN_ERROR:
      case SIGN_UP_DISCOUNT_CODE_ERROR:
        draft.error = { stripe: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case UPDATE_PRACTICE_OBJECTIONS_ERROR:
        draft.error = { objections: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case UPDATE_PRACTICE_OBJECTIONS_SUCCESS:
        draft.user = merge({}, draft.user, action.record);
        draft.error = {};
        draft.success = { objections: action.success };
        draft.loading = false;
        break;
      case SETUP_STRIPE_PAYMENT_SUCCESS:
        draft.error = {}
        draft.success = { stripe: action.success };
        break;
      case CREATE_SUBSCRIPTION_PLAN_SUCCESS:
        draft.error = {}
        draft.success = { stripe: action.success };
        draft.loading = false;
        break;
      case REQUEST_SUPPORT_ERROR:
      case REQUEST_DEMO_ERROR:
      case SESSION_TIMEOUT_ERROR:
      case SESSION_LOGIN_ERROR:
      case SESSION_CLEAR_TIMEOUT_ERROR:
      case SESSION_TOKEN_ERROR:
        draft.error = { [action.form]: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case REQUEST_SUPPORT_SUCCESS:
      case REQUEST_DEMO_SUCCESS:
        draft.error = {}
        draft.success = { [action.form]: action.success };
        draft.loading = false;
        break;
      case UPDATE_VERSION_SUCCESS:
        draft.error = {}
        draft.user.version = action.version;
        draft.loading = false;
      case CLEAR_CACHE:
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case SESSION_TIMEOUT_SUCCESS:
        draft.error = {};
        draft.success = {};
        draft.timeout = action.timeout;
        draft.loading = false;
        break;
      case SESSION_CLEAR_TIMEOUT_SUCCESS:
        draft.error = {};
        draft.success = {};
        draft.timeout = false;
        draft.loading = false;
        break;
      case SESSION_LOGIN_SUCCESS:
        draft.error = {};
        draft.success = {};
        draft.activeSession = action.duration;
        draft.loading = false;
        break;
      case SESSION_EXPAND:
        draft.expand = action.expand;
        draft.loading = false;
        break;
      case GET_STRIPE_INDENT_SUCCESS:
        draft.payment = action.record;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case SETUP_STRIPE_PAYMENT:
        draft.error = {};
        draft.success = {};
        draft.loading = true;
        break;
      case LOAD_SETTINGS_PLAN_SUCCESS:
        draft.metaData = action.metaData;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
      case TWO_FACTOR_AUTHENTICATION_ERROR:
        draft.error = { twoFactorAuthentication: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case TWO_FACTOR_AUTHENTICATION_SUCCESS:
        draft.user = action.user;
        draft.error = {}
        draft.success = { twoFactorAuthentication: action.success };
        draft.loading = false;
        break;
      case LOAD_APP_VERSION_SUCCESS:
        draft.loading = false;
        draft.success = {};
        draft.error = {};
        draft.appVersion = action.appVersion;
        break;
      case UPDATE_SIGNATURE_SUCCESS:
        draft.error = {}
        draft.success = { signature: action.success };
        draft.loading = false;
        break;
      case UPDATE_SIGNATURE_ERROR:
        draft.error = { signature: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case LOAD_RESPOND_DETAIL_SUCCESS:
        draft.loading = false;
        draft.success = {};
        draft.error = {};
        draft.respondDetail = action.record;
        break;
      case SIGN_UP_DISCOUNT_CODE_SUCCESS:
        draft.loading = false;
        draft.success = {};
        draft.error = {};
        draft.discountDetail = action.discountDetail;
        break;
      case MYCASE_LOAD_RECORDS:
        draft.buttonLoader = true;
        draft.loading = false;
        draft.error = {};
        draft.success = false;
        draft.progress = false;
        draft.stepper = 'mycase';
        draft.key = 0;
        break;
      case MYCASE_LOAD_RECORDS_SUCCESS:
        draft.myCaseRecords = action.records;
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = {};
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case MYCASE_LOAD_RECORDS_ERROR:
        draft.myCaseRecords = action.records;
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = { mycase: action.error };
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case GET_LITIFY_RECORD_ERROR:
      case LITIFY_CASES_CREATE_ERROR:
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = { litify: action.error };
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case MYCASE_ACCESS_TOKEN_ERROR:
      case CLIO_ACCESS_TOKEN_ERROR:
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = { auth_code: action.error };
        draft.success = false;
        draft.progress = false;
        break;
      case UPDATE_KEY:
        draft.key = action.key;
        draft.loading = false;
        draft.error = false;
        draft.updateError = false;
        draft.success = false;
        draft.progress = false;
        break;
      case AUTHENTICATION_SUCCESS:
        draft.loading = false;
        draft.success = {};
        draft.error = {};
        draft.authUser = action.record;
        break;
      case AUTH_SIGN_UP_ERROR:
        draft.loading = false;
        draft.success = {};
        draft.error = { authConfirm: action.error };
        break;
      case GOOGLE_AUTH:
      case MICROSOFT_AUTH:
      case GET_LITIFY_RECORD:
      case MYCASE_ACCESS_TOKEN:
      case LITIFY_CASES_CREATE:
      case CLIO_ACCESS_TOKEN:
        draft.authPageLoader = true;
        draft.success = {};
        draft.error = {};
        break;
      case DISABLE_AUTH_PAGE_LOADER:
        draft.authPageLoader = false;
        draft.success = {};
        draft.error = {};
        break;
      case GET_LITIFY_BULK_RECORDS:
        draft.buttonLoader = true;
        draft.loading = false;
        draft.error = {};
        draft.success = false;
        draft.progress = false;
        draft.stepper = 'litify';
        draft.key = 0;
        break;
      case GET_LITIFY_BULK_RECORDS_SUCCESS:
        draft.litifyRecords = action.records;
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = {};
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case GET_LITIFY_BULK_RECORDS_ERROR:
        draft.litifyRecords = action.records;
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = { litify: action.error };
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case WEB_AUTHENTICATION_ERROR:
        draft.loading = false;
        draft.success = {};
        draft.error = { protection: action.error };
        break;
      case SWITCH_PRACTICE_SUCCESS:
        draft.loggedIn = true;
        draft.user = action.user;
        draft.secret = action.token;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        draft.success = { switchPractice: action.success };
        break;
      case SET_PRACTICE_SWITCH_RENDERING:
        draft.switchPracticeRender = action.renderObject;
        break;
      case SWITCH_PRACTICE_ERROR:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = { switchPractice: action.error };
        draft.success = {};
        draft.loading = false;
        break;
      case CLIO_LOAD_RECORDS:
        draft.buttonLoader = true;
        draft.loading = false;
        draft.error = {};
        draft.success = false;
        draft.progress = false;
        draft.stepper = 'clio';
        draft.key = 0;
        break;
      case CLIO_LOAD_RECORDS_SUCCESS:
        draft.clioRecords = action.records;
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = {};
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case CLIO_LOAD_RECORDS_ERROR:
        draft.clioRecords = action.records;
        draft.buttonLoader = false;
        draft.loading = false;
        draft.error = { clio: action.error };
        draft.success = false;
        draft.progress = false;
        draft.stepper = false;
        break;
      case GET_ALL_MY_PRACTICES:
        draft.metaData = action.metaData;
        break;
      case CLEAR_PRACTICE_SWITCH_RENDERING:
        draft.switchPracticeRender = Object.assign({}, draft.switchPracticeRender, action.renderObject);
        break;
      case CHAT_NOTIFICATION_ERROR:
        draft.error = { notification: action.error };
        break;
      case CHAT_NOTIFICATION:
        draft.notificationLoader = true;
        break;
      case CHAT_NOTIFICATION_SUCCESS:
        draft.notificationLoader = false;
        draft.error = {};
        break;
      case CHAT_NOTIFICATION_ERROR:
        draft.error = { notification: action.error };
        draft.notificationLoader = false;
        break;
    }
  });


export default appReducer;

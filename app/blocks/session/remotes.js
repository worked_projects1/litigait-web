/*
 *
 *  session remotes
 *
 */

import api from 'utils/api';
import { signatureDecoding } from 'utils/tools';

export function verifySession() {
  return api
    .get(`/users/me`)
    .then(response => {
      if (!response.data || (response.data && response.data.error)) {
        return Promise.reject('Session Timout');
      }
      const { data = {} } = response;
      const { is_admin, role } = data;

      let Record = Object.assign({}, { ...data });

      if(data && data.signature && data.signature != '') {
        Record.signature = signatureDecoding(data.signature, data.id)
      }

      if (role != 'superAdmin' && is_admin) {
        return Object.assign({}, { ...Record }, { routes: 'customer' })
      } else {
        return Object.assign({}, { ...Record }, { routes: role })
      }
    })
    .catch(error => Promise.reject(error));
}

/**
 * @param {string} email 
 * @param {string} password 
 * @param {object} queryParams 
 * @param {boolean} sessionLogin 
 */
export function logIn(email, password, queryParams, sessionLogin) {
  return api
    .post(`/login`, { email, password, reactivation_token: queryParams && queryParams.reactivation || false, session: sessionLogin || false })
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

export function logOut() {
  return api
    .delete(`/session`)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function signUp(record) {
  return api
    .post(`/users/signup`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function forgotPassword(record) {
  return api
    .post(`/users/forget-paasword`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function resetPassword(record) {
  return api
    .post(`/users/reset-password`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function changePassword(record) {
  return api
    .put(`/rest/users/change-password`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function requestSupport(record) {
  return api
    .post(`/rest/support/support-request`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function requestDemo(record) {
  return api
    .post(`/rest/support/demo-request`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function updateVersion(record) {
  return api
    .put(`/rest/version-history`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

export function getStripeIndent() {
  return api
    .get(`/billing/set-payment-intent`)
    .then((response) => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record
 */
export function setupStripePayment(record) {
  return api
    .post(`billing/save-card-data`, record)
    .then((response) => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record
 */
export function createSubscriptionPlan(record) {
  return api
    .post(`/rest/subscriptions`, record)
    .then((response) => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {string} page 
 * @returns 
 */
export function loadSignUpPlan(page) {
  return api
    .get(`/rest/signup/plans?page=${page}`)
    .then((response) => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function updatePracticeObjections(record) {
  return api.put(`rest/practices/update-objection-status`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {string} email 
 * @param {string} otp 
 * @param {string} user_from 
 * @returns 
 */
export function verifyOtp(email, otp, user_from){
  return api.post('/login/otpverify',{email, otp, user_from }).then(response => {return response.data}).catch(error => Promise.reject(error));
}


/**
 * @param {object} record 
 */
export function twoFactorAuthentication(record) {
  return api.put(`/users/twofactor`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function subscriptionValidate() {
  return api
  .put(`/rest/users/subscription-validate`)
  .then((response) => response.data)
  .catch((error) => Promise.reject(error));
}


/**
 * @param {object} record 
 */
export function sessionToken(record) {
  return api
    .post(`/rest/users/session`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function loadAppVersion(record) {
  return api
    .get(`/rest/version-history`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function updateSignature(record) {
  return api
    .put('/rest/user/update-signature', record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {string} id 
 */
export function respondDetail(id) {
  return api
    .get(`/rest/propound-responder/details/${id}`)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function validateDiscountCode(record) {
  return api
    .put(`/rest/validate/discount-code?code=${record.discount_code}&type=responding`, record)
    .then(response => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record
 */
export function myCaseAccessToken(record) {
  return api
    .post(`rest/integrations/mycase/get-auth-token`, record)
    .then(response => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} pageSize
 */
export function myCaseClients(record) {
  const url = record && record.token ? `rest/integrations/mycase/get-data?type=${record.type}&page_token=${record.token}` : `rest/integrations/mycase/get-data?type=${record.type}`
  return api
  .get(url)
  .then(response => response.data)
  .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function googleAuth(record) {
  return api
    .post(`rest/google/authentication`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function microsoftAuth(record) {
  return api.post(`rest/microsoft/authentication`, record)
  .then(response => response.data)
  .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function authSignUp(record) {
  return api.post(`rest/user/update-other-signup-details`, record)
  .then(response => response.data)
  .catch(error => Promise.reject(error));
}


/**
 * @param {object} record 
 */
export function authLogin(record) {
  return api.post(`rest/login/session-login`, record)
  .then(response => response.data)
  .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function emailValidation(record) {
  return api.post(`rest/login/email-validation`, record)
  .then(response => response.data)
  .catch(error => Promise.reject(error));
}


/**
 * @param {object} record 
 */
export function syncMycaseRecords(record) {
  return api.put(`/rest/integrations/mycase-update-case-client`, record)
  .then((response) => response.data)
  .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record
 */
export function getLitifyRecord(record) {
  return api.post(`/rest/integrations/get-litify-data`, record)
    .then((response) => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {number} offset 
 * @param {number} limit 
 * @returns 
 */
export function getLitifyBulkRecords(offset, limit) {
  return api.get(`/rest/integrations/get-multiple-records-litify?offset=${offset}&limit=${limit}`)
    .then((response) => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} record
 * @returns
 */
export function updateLitifyBulkRecords(record) {
  return api.put(`/rest/integrations/bulk-update-litify `, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {object} record 
 * @returns 
 */
export function websiteAuthentication(record) {
  return api.post(`/rest/website-authentication`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {object} record 
 * @returns 
 */
export function switchPractice(record) {
  return api.post(`/rest/practices/switch`, record).then((response) => response.data).catch((error) => Promise.reject(error));
} 

/**
 * @param {object} record
 */
export function clioAccessToken(record) {
  return api
    .post(`/rest/integrations/get-clio-authtoken`, record)
    .then(response => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} pageSize
 */
export function clioCaseClients(record) {
  const url = record && record.token ? `rest/integrations/get-clio-data?type=${record.type}&page_token=${record.token}` : `rest/integrations/get-clio-data?type=${record.type}`
  return api
    .get(url)
    .then(response => response.data)
    .catch((error) => Promise.reject(error));
}

/**
 * @param {object} records
 */
export function syncClioRecords(records) {
  return api
    .put(`/rest/integrations/clio-update-client-and-case`, records)
    .then(response => response.data)
    .catch((error) => Promise.reject(error));
}

export function getAllPracticesList() {
  return api.get(`rest/practices/details-fetch`).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {object} record 
 * @returns 
 */
export function chatNotification(record) {
  return api.put('/rest/user/update-notification-status', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  verifySession,
  logIn,
  logOut,
  signUp,
  forgotPassword,
  resetPassword,
  changePassword,
  requestSupport,
  requestDemo,
  updateVersion,
  getStripeIndent,
  setupStripePayment,
  createSubscriptionPlan,
  loadSignUpPlan,
  updatePracticeObjections,
  verifyOtp,
  twoFactorAuthentication,
  subscriptionValidate,
  sessionToken,
  loadAppVersion,
  updateSignature,
  respondDetail,
  validateDiscountCode,
  myCaseAccessToken,
  myCaseClients,
  googleAuth,
  microsoftAuth,
  authSignUp,
  authLogin,
  emailValidation,
  syncMycaseRecords,
  getLitifyRecord,
  getLitifyBulkRecords,
  updateLitifyBulkRecords,
  websiteAuthentication,
  switchPractice,
  clioAccessToken,
  clioCaseClients,
  syncClioRecords,
  getAllPracticesList,
  chatNotification
};

/*
 *
 * MyCase actions
 *
 */



/**
 * 
 * @param {object} constants 
 * @returns 
 */
 export default function (constants) {

  const {
    UPLOAD_MY_CASE_DOCUMENT,
    UPLOAD_MY_CASE_DOCUMENT_SUCCESS,
    UPLOAD_MY_CASE_DOCUMENT_ERROR,
    DELETE_MYCASE_SECRET,
    DELETE_MYCASE_SECRET_SUCCESS,
    DELETE_MYCASE_SECRET_ERROR,
    MYCASE_SYNC_DATA,
    MYCASE_SYNC_DATA_SUCCESS,
    MYCASE_SYNC_DATA_ERROR,
    MYCAE_REDIRECT_AUTH_URI,
    MYCASE_UPDATE_USER_DETAILS,
    MYCASE_UPDATE_USER_DETAILS_ERROR,
    MYCASE_UPDATE_USER_DETAILS_SUCCESS
  } = constants;

  /**
   * @param {object} record 
   */
  function uploadMyCaseDocument(record) {
    return {
      type: UPLOAD_MY_CASE_DOCUMENT,
      record
    };
  }

  /**
   * @param {string} success 
   */
  function uploadMyCaseDocumentSuccess(success) {
    return {
      type: UPLOAD_MY_CASE_DOCUMENT_SUCCESS,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function uploadMyCaseDocumentError(error) {
    return {
      type: UPLOAD_MY_CASE_DOCUMENT_ERROR,
      error,
    };
  }


  function deleteMyCaseSecret(record) {
    return {
      type: DELETE_MYCASE_SECRET,
      record
    }
  }
  
  
  function deleteMyCaseSecretSuccess(success) {
    return {
      type: DELETE_MYCASE_SECRET_SUCCESS,
      success
    }
  }
  
  
  function deleteMyCaseSecretError(error) {
    return {
      type: DELETE_MYCASE_SECRET_ERROR,
      error
    }
  }
  
  /**
  * 
  * @param {array} records 
  * @param {string} form 
  * @param {function} setMyCasePopup 
  * @param {function} setShowUserForm 
  * @returns 
  */
  function myCaseSyncData(records, form, setMyCasePopup, setShowUserForm) {
    return {
      type: MYCASE_SYNC_DATA,
      records,
      form,
      setMyCasePopup,
      setShowUserForm
    }
  }

  /**
  * 
  * @param {string} success 
  * @returns 
  */
  function myCaseSyncDataSuccess(success) {
  return {
      type: MYCASE_SYNC_DATA_SUCCESS,
      success
    }
  }

  /**
  * 
  * @param {string} error 
  * @returns 
  */
  function myCaseSyncDataError(error) {
    return {
      type: MYCASE_SYNC_DATA_ERROR,
      error
    }
  }

  /**
   * 
   * @param {boolean} load 
   * @param {boolean} mycaseLoadRecord 
   * @returns 
   */
  function mycaseRedirectAuthUrI(load, mycaseLoadRecord) {
    return {
      type: MYCAE_REDIRECT_AUTH_URI,
      load,
      mycaseLoadRecord
    }
  }
  

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {object} integration 
   * @returns 
   */
  function mycaseUpdateUserDetails(record, form, integration) {
    return {
      type:  MYCASE_UPDATE_USER_DETAILS,
      record,
      form,
      integration
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function mycaseUpdateUserDetailsSuccess(success) {
    return {
      type: MYCASE_UPDATE_USER_DETAILS_SUCCESS,
      success
    }
  }
  
  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function mycaseUpdateUserDetailsError(error) {
    return {
      type: MYCASE_UPDATE_USER_DETAILS_ERROR,
      error
    }
  }

  return { 
    uploadMyCaseDocument,
    uploadMyCaseDocumentSuccess,
    uploadMyCaseDocumentError,
    deleteMyCaseSecret,
    deleteMyCaseSecretSuccess,
    deleteMyCaseSecretError,
    myCaseSyncData,
    myCaseSyncDataSuccess,
    myCaseSyncDataError,
    mycaseRedirectAuthUrI,
    mycaseUpdateUserDetails,
    mycaseUpdateUserDetailsSuccess,
    mycaseUpdateUserDetailsError
  }
}

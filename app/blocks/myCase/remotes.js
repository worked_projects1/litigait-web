/*
 *
 *  MyCase remotes
 *
 */

import api from 'utils/api';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function uploadMyCaseDocument(record) {
        return api.post('/rest/integrations/mycase-file-upload', record).then(res => res.data).catch(err => Promise.reject(err))
    }
    
    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function deleteMyCaseSecret(record) {
        return api.delete(`/rest/integrations/mycase/${record.id}`).then(res => res.data).catch(err => Promise.reject(err))
    }

    /**
     * 
     * @param {array} records 
     * @returns 
     */
    function myCaseSyncData(records) {
        return api.post('/rest/integrations/mycase/create-client-and-case', records).then(res => res.data).catch(err => Promise.reject(err))
    }

    /**
     * @param {object} record
     */
    function updateUserRecord(record){
        return api.put(`/rest/user/update-details`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        uploadMyCaseDocument,
        deleteMyCaseSecret,
        myCaseSyncData,
        updateUserRecord
    }
}


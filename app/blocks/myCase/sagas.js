/*
 *
 *  MyCase sagas
 *
 */


import { push } from 'react-router-redux';
import { call, put, all, delay, takeLatest, select } from 'redux-saga/effects';
import store2 from 'store2';
import { destroy, startSubmit, stopSubmit } from 'redux-form/immutable';
import lodash from 'lodash';
import { verifySession as verifySessionAction, verifySessionSuccess } from 'blocks/session/actions';
import { verifySession as verifySessionApi } from 'blocks/session/remotes';
import history from 'utils/history';
import { myCaseLoadRecordsSuccess } from 'blocks/session/actions';
import records from 'blocks/records';
import { selectForm } from 'blocks/session/selectors';

const casesSaga = records('cases');
const { actions: casesActions, selectors: casesSelectors } = casesSaga;

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    UPLOAD_MY_CASE_DOCUMENT,
    DELETE_MYCASE_SECRET,
    MYCASE_SYNC_DATA,
    MYCAE_REDIRECT_AUTH_URI,
    MYCASE_UPDATE_USER_DETAILS
  } = constants;


  const {
    uploadMyCaseDocumentSuccess,
    uploadMyCaseDocumentError,
    deleteMyCaseSecretSuccess,
    deleteMyCaseSecretError,
    myCaseSyncDataSuccess,
    myCaseSyncDataError,
    mycaseRedirectAuthUrI: mycaseRedirectAuthUrIAction,
    myCaseSyncData: myCaseSyncDataAction,
    mycaseUpdateUserDetailsError
  } = actions;


  const {
    uploadMyCaseDocument,
    deleteMyCaseSecret,
    myCaseSyncData,
    updateUserRecord
  } = remotes;


  function* uploadMyCaseDocumentSaga() {
    yield takeLatest(UPLOAD_MY_CASE_DOCUMENT, function* updater({ record }) {
      if(record) {
        try {
          if(record.myCaseDetails) {
            const result = yield call(uploadMyCaseDocument, Object.assign({}, { s3_file_key: record.discovery_s3_file_key, integration_case_id: record.integration_case_id, document_type: record.document_type && record.document_type.toUpperCase() }));
            if(result) {
              
              yield put(uploadMyCaseDocumentSuccess('Document Uploaded Successfully'));
              yield delay(500);
              const pageLoader = store2.get('pageLoader');
              const userLoader = store2.get('userLoader');
              if(pageLoader || userLoader) {
                const user = yield call(verifySessionApi);            
                if(user) 
                  yield put(verifySessionSuccess(user));
                  store2.remove('pageLoader');
                  store2.remove('userLoader');
              }
            }
          } else {
            yield put(mycaseRedirectAuthUrIAction(true, true));
          }
        } catch (error) {
          yield put(uploadMyCaseDocumentError('Failed to Upload Document'));
        }
      }
    })
  }


  function* deleteMyCaseSecretSaga() {
    yield takeLatest(DELETE_MYCASE_SECRET, function* updater({ record }) {
      if(record) {
        try {
          const res = yield call(deleteMyCaseSecret, record);
          if(res) {
            yield put(deleteMyCaseSecretSuccess('MyCase Secret Successfully Removed.'));
            yield delay(1000);
            const secret = store2.get('secret');
            yield put(verifySessionAction(secret));
          } 
        } catch (error) {
          yield put(deleteMyCaseSecretError('Failed to delete secrets'));
        }
      }
    })
  }

  function* myCaseSyncDataSaga() {
    yield takeLatest(MYCASE_SYNC_DATA, function* updater({ records, form, setMyCasePopup, setShowUserForm }) {
      if(records) {
        yield put(startSubmit(form));
        try {
          const user_info = yield call(verifySessionApi);
          if(user_info && (user_info?.role == 'lawyer' && !user_info.state_bar_number || !user_info.user_name) && setShowUserForm) {
            yield call(setShowUserForm, true);
            yield put(stopSubmit(form));
          } else {
            yield put(startSubmit(form));
            try {
              if (records && Array.isArray(records) && records.length > 0) {
                let loop = 0;
                let currentIndex = 1;
                let totalSize = 100;
                const totalSlice = records.length % totalSize == 0 ? Math.floor(records.length / totalSize) : Math.floor((records.length / totalSize) + 1);
          
                while (loop < totalSlice) {
                  const firstIndex = (currentIndex - 1) * totalSize;
                  const lastIndex = firstIndex + totalSize;
                  const saveRecords = records.slice(firstIndex, lastIndex);
                  const result = yield call(myCaseSyncData, saveRecords);
    
                  if (result) {
                    loop++;
                    currentIndex++;
                  } else {
                    yield put(myCaseSyncDataError('Failed to save records'));
                  }
                }
                yield put(stopSubmit(form));
                yield put(myCaseSyncDataSuccess('MyCase data sync successfully'));
                yield delay(1000);
                yield put(push({ pathname: '/casesCategories/cases', state: Object.assign({}, { ...history.location.state }, { integrationModal: false }) }));         
                yield put(casesActions.loadRecords(true));
                yield put(myCaseLoadRecordsSuccess([]));
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              }
            } catch (err) {
              yield put(myCaseSyncDataError('Failed to save records'));
              yield put(stopSubmit(form));
            } finally {
              if(setMyCasePopup){
                yield call(setMyCasePopup, false);
              }          
              yield put(destroy(form));
            }
          }
        } catch(error) {          
          yield put(myCaseSyncDataError('Failed to save records'));
          yield put(stopSubmit(form));
        }
      }
    })
  }


  function* mycaseRedirectAuthUriSaga() { 
    yield takeLatest(MYCAE_REDIRECT_AUTH_URI, function* updater({ load, mycaseLoadRecord }) {
      if(load) {
        store2.set('disable_mycase_load_record', mycaseLoadRecord);
        window.location.href = `${process.env.MYCASE_URL}/login_sessions/new?client_id=${process.env.MYCASE_CLIENT_ID}&include_granted_scopes=true&redirect_uri=${process.env.REDIRECT_URI}/integrations/my_case/oauth2_callback&response_type=code`;
      }   
    })
  }

  function* mycaseUpdateUserDetailsSaga() {
    yield takeLatest(MYCASE_UPDATE_USER_DETAILS, function* updater({ record, form, integration }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateUserRecord, record);
          if (result) {
            const { integrationPopup, integration_form, setShowUserForm } = integration;
            yield put(stopSubmit(form));
            yield call(setShowUserForm, false);
            const storeForms = yield select(selectForm());
            const selectFormRecord = storeForms && storeForms[`${integration_form}`] && storeForms[`${integration_form}`].values && storeForms[`${integration_form}`]['values']['integrations'];
            if(selectFormRecord?.length > 0) {
                yield put(myCaseSyncDataAction(selectFormRecord, integration_form, integrationPopup));
            }
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to Update User Record';
          yield put(stopSubmit(form));
          yield put(mycaseUpdateUserDetailsError(Err));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      uploadMyCaseDocumentSaga(),
      deleteMyCaseSecretSaga(),
      myCaseSyncDataSaga(),
      mycaseRedirectAuthUriSaga(),
      mycaseUpdateUserDetailsSaga()
    ]);
  }

}
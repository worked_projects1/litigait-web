/*
 *
 * practice constants
 *
 */


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {
    const url = `lg/${name}`;
    return {
        LOAD_PRACTICE: `${url}/LOAD_PRACTICE`,
        LOAD_PRACTICE_ERROR: `${url}/LOAD_PRACTICE_ERROR`,
        LOAD_PRACTICE_SUCCESS: `${url}/LOAD_PRACTICE_SUCCESS`,
        UPDATE_PRACTICE: `${url}/UPDATE_PRACTICE`,
        UPDATE_PRACTICE_ERROR: `${url}/UPDATE_PRACTICE_ERROR`,
        UPDATE_PRACTICE_SUCCESS: `${url}/UPDATE_PRACTICE_SUCCESS`,
        LOAD_PRACTICE_SETTINGS: `${url}/LOAD_PRACTICE_SETTINGS`,
        LOAD_PRACTICE_SETTINGS_ERROR: `${url}/LOAD_PRACTICE_SETTINGS_ERROR`,
        LOAD_PRACTICE_SETTINGS_SUCCESS: `${url}/LOAD_PRACTICE_SETTINGS_SUCCESS`,
        UPDATE_PRACTICE_SETTINGS: `${url}/UPDATE_PRACTICE_SETTINGS`,
        UPDATE_PRACTICE_SETTINGS_ERROR: `${url}/UPDATE_PRACTICE_SETTINGS_ERROR`,
        UPDATE_PRACTICE_SETTINGS_SUCCESS: `${url}/UPDATE_PRACTICE_SETTINGS_SUCCESS`,
        PRACTICE_CANCEL_SUBSCRIPTION: `${url}/PRACTICE_CANCEL_SUBSCRIPTION`,
        PRACTICE_CANCEL_SUBSCRIPTION_ERROR: `${url}/PRACTICE_CANCEL_SUBSCRIPTION_ERROR`,
        PRACTICE_CANCEL_SUBSCRIPTION_SUCCESS: `${url}/PRACTICE_CANCEL_SUBSCRIPTION_SUCCESS`,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION: `${url}/PRACTICE_ADMIN_CANCEL_SUBSCRIPTION`,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_ERROR: `${url}/PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_ERROR`,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_SUCCESS: `${url}/PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_SUCCESS`,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING:`${url}/GLOBAL_ATTORNEY_RESPONSE_TRACKING`,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING_SUCCESS:`${url}/GLOBAL_ATTORNEY_RESPONSE_TRACKING_SUCCESS`,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING_ERROR:`${url}/GLOBAL_ATTORNEY_RESPONSE_TRACKING_ERROR`,
        VIP_SUBSCRIPTION:`${url}/VIP_SUBSCRIPTION`,
        VIP_SUBSCRIPTION_SUCCESS:`${url}/VIP_SUBSCRIPTION_SUCCESS`,
        VIP_SUBSCRIPTION_ERROR:`${url}/VIP_SUBSCRIPTION_ERROR`,
        VIP_CANCEL_SUBSCRIPTION:`${url}/VIP_CANCEL_SUBSCRIPTION`,
        VIP_CANCEL_SUBSCRIPTION_SUCCESS:`${url}/VIP_CANCEL_SUBSCRIPTION_SUCCESS`,
        VIP_CANCEL_SUBSCRIPTION_ERROR:`${url}/VIP_CANCEL_SUBSCRIPTION_ERROR`,
    }
}


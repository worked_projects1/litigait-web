/*
 *
 *  practice remotes
 *
 */

import api from 'utils/api';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * @param {integer} id 
     */
    function loadPractice(id) {
        return api.get(`practices/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updatePractice(record) {
        return api.put(`practices/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {integer} id 
     */
    function loadPracticeSettings(id) {
        return api.get(`rest/practice-settings?practice_id=${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updatePracticeSettings(record) {
        return api.put(`rest/practice-settings`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function practiceCancelSubscription(record) {
        return api.put(`/rest/admin/subscription-cancel?practice_id=${record.practice_id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function cancelPropoundSubscription(record) {
        return api.post(`/rest/subscriptions/cancel-propounding`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    } 
    
    /**
     * @param {object} record 
     */
    function vipSubscription(record) {
        return api.post(`/rest/subscriptions/vip/enable`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function vipCancelSubscription(record) {
        return api.post(`/rest/subscriptions/vip/disable`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }   

    /**
     * @param {object} record 
     */
    function cancelUserPropoundSubscription(record) {
        return api.put(`/rest/users-license/subscription/cancel-propounding/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    } 

    return {
        loadPractice,
        updatePractice,
        loadPracticeSettings,
        updatePracticeSettings,
        practiceCancelSubscription,
        cancelPropoundSubscription,
        vipSubscription,
        vipCancelSubscription,
        cancelUserPropoundSubscription
    }
}


/*
 *
 *  practice sagas
 *
 */


import { call, take, put, race, all, takeLatest, delay, select } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';

import {
    DEFAULT_LOAD_PRACTICE_ERROR,
    DEFAULT_UPDATE_PRACTICE_ERROR,
    DEFAULT_LOAD_PRACTICE_SETTINGS_ERROR,
    DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR,
    DEFAULT_CANCEL_PRACTICE_SUBSCRIPTION_ERROR,
    DEFAULT_GLOBAL_ATTORNEY_TRACKING_STATUS_ERROR,
    DEFAULT_VIP_SUBSCRIPTION_ERROR
} from 'utils/errors';
import { verifySession as verifySessionAction } from 'blocks/session/actions';
import subscriptionRemotes from 'blocks/subscriptions/remotes';
import store2 from 'store2';
import moment from 'moment';
import { reset } from 'redux-form';

import records from 'blocks/records';
import { selectUser } from 'blocks/session/selectors';

const practiceSaga = records('practices');

const { selectors: practiceSelector, } = practiceSaga;
const { selectRecord } = practiceSelector;


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

    const {
        LOAD_PRACTICE,
        UPDATE_PRACTICE,
        LOAD_PRACTICE_SETTINGS,
        UPDATE_PRACTICE_SETTINGS,
        PRACTICE_CANCEL_SUBSCRIPTION,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING,
        VIP_SUBSCRIPTION,
        VIP_CANCEL_SUBSCRIPTION
    } = constants;


    const {
        loadPracticeError,
        loadPracticeSuccess,
        updatePracticeError,
        updatePracticeSuccess,
        loadPracticeSettingsError,
        loadPracticeSettingsSuccess,
        updatePracticeSettingsError,
        updatePracticeSettingsSuccess,
        practiceCancelSubscriptionError,
        practiceCancelSubscriptionSuccess,
        practiceAdminCancelSubscriptionError,
        practiceAdminCancelSubscriptionSuccess,
        globalAttorneyResponseTrackingSuccess,
        globalAttorneyResponseTrackingError,
        vipSubscriptionSuccess,
        vipSubscriptionError,
        vipCancelSubscriptionSuccess,
        vipCancelSubscriptionError,
    } = actions;


    const {
        loadPractice,
        updatePractice,
        loadPracticeSettings,
        updatePracticeSettings,
        practiceCancelSubscription,
        cancelPropoundSubscription,
        vipSubscription,
        vipCancelSubscription,
        cancelUserPropoundSubscription
    } = remotes;



    function* loadPracticeSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { load } = yield race({
                load: take(LOAD_PRACTICE),
            });
            const { id } = load || {};
            if (id) {
                try {
                    const result = yield call(loadPractice, id);
                    if (result) {
                        yield put(loadPracticeSuccess(result));
                    } else {
                        yield put(loadPracticeError(DEFAULT_LOAD_PRACTICE_ERROR));
                    }
                } catch (error) {
                    yield put(loadPracticeError(DEFAULT_LOAD_PRACTICE_ERROR));
                }
            }
        }
    }



    function* updatePracticeSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { update } = yield race({
                update: take(UPDATE_PRACTICE),
            });
            const { record, form, setShowForm, setFormLoader } = update || {};
            if (record) {
                yield put(startSubmit(form));
                const secret = store2.get('secret');
                try {
                    const result = yield call(updatePractice, record);
                    if (result) {
                        yield call(setFormLoader, false);
                        yield call(setShowForm, false);
                        yield put(updatePracticeSuccess(result, 'Profile Updated'));
                        yield put(stopSubmit(form));
                        if (secret) {
                            yield put(verifySessionAction(secret));
                        }
                    } else {
                        yield put(updatePracticeError(DEFAULT_UPDATE_PRACTICE_ERROR));
                        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_ERROR }));
                    }
                } catch (error) {
                    yield put(updatePracticeError(DEFAULT_UPDATE_PRACTICE_ERROR));
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_ERROR }));
                }
            }
        }
    }



    function* loadPracticeSettingsSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { load } = yield race({
                load: take(LOAD_PRACTICE_SETTINGS),
            });
            const { id } = load || {};
            if (id) {
                try {
                    const result = yield call(loadPracticeSettings, id);
                    if (result) {
                        yield put(loadPracticeSettingsSuccess(result));
                    } else {
                        yield put(loadPracticeSettingsError(DEFAULT_LOAD_PRACTICE_SETTINGS_ERROR));
                    }
                } catch (error) {
                    yield put(loadPracticeSettingsError(DEFAULT_LOAD_PRACTICE_SETTINGS_ERROR));
                }
            }
        }
    }



    function* updatePracticeSettingsSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { update } = yield race({
                update: take(UPDATE_PRACTICE_SETTINGS),
            });
            const { record, form } = update || {};
            if (record) {
                yield put(startSubmit(form));
                try {
                    const result = yield call(updatePracticeSettings, record);
                    if (result) {
                        yield put(updatePracticeSettingsSuccess(result, 'Pricing Updated'));
                        yield put(stopSubmit(form));
                    } else {
                        yield put(updatePracticeSettingsError(DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR));
                        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR }));
                    }
                } catch (error) {
                    yield put(updatePracticeSettingsError(DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR));
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR }));
                }
            }
        }
    }

    function* practiceAdminCancelSubscriptionSaga() {
        yield takeLatest(PRACTICE_ADMIN_CANCEL_SUBSCRIPTION, function* updater({ record, subscriptionRecord, setLoader, loadRecord }) {
            if (record) {
                try {
                    if(record.subscriptionData) {
                        const result = yield call(practiceCancelSubscription, Object.assign({}, { ids: record.subscriptionData.map(el => el.subscription_id), practice_id: record.id }));
                        if (result) {
                            const successMessages = subscriptionRecord ? `Auto-renewal is canceled. Practice will receive an email to indicate that the account will not auto-renew and that service is available for using the service for ${subscriptionRecord.plan_category} till ${moment.unix(subscriptionRecord.current_period_end).format('MM/DD/YYYY')}` : `Auto-renewal is canceled. Practice will receive an email to indicate that the account will not auto-renew.`;
                            yield call(setLoader, false);                    
                            yield put(practiceAdminCancelSubscriptionSuccess(successMessages));
                            yield delay(3000);
                            yield put(loadRecord(record.id));
                        } else {
                            yield put(practiceAdminCancelSubscriptionError(DEFAULT_CANCEL_PRACTICE_SUBSCRIPTION_ERROR));
                        }
                    }
                } catch (error) {
                    yield put(practiceAdminCancelSubscriptionError(DEFAULT_CANCEL_PRACTICE_SUBSCRIPTION_ERROR));
                }
                finally {
                    yield call(setLoader, false);
                    yield put(reset(`cancelSubcription_${record.id}`));
                }
            }
        })
    }

    function* practiceCancelSubscriptionSaga() {
        yield takeLatest(PRACTICE_CANCEL_SUBSCRIPTION, function* updater({ record, setLoader, propound }) {
            if (record) {
                try {
                    const subscriptionRecord = record && Object.keys(record).length > 0 && Object.assign({}, { current_period_end: record['stripe_subscription_data'] && record['stripe_subscription_data']['current_period_end'], cancel_at_period_end: record['stripe_subscription_data'] && record['stripe_subscription_data']['cancel_at_period_end'], plan_category: record.plan_category });
                    let result;

                    if (propound) {
                        const user_info = yield select(selectUser());
                        const userBasedBilling = user_info?.practiceDetails?.billing_type === 'limited_users_billing' ? true : false;
                        const submitRecord = Object.assign({}, { id: record.id, practice_id: record.practice_id })
                        if(userBasedBilling) {
                            result = yield call(cancelUserPropoundSubscription, submitRecord);
                        } else {
                            result = yield call(cancelPropoundSubscription, submitRecord);
                        }
                    } else {
                        const subscription = yield call(subscriptionRemotes, 'subscriptions');
                        const loadSubscriptions = yield call(subscription.loadSubscriptions);
                        if(loadSubscriptions) {
                            result = yield call(practiceCancelSubscription, Object.assign({}, { ids: loadSubscriptions.map(el => el.id), practice_id: record.practice_id }));
                        }
                    }

                    if (result) {
                        yield call(setLoader, false);                        
                        const successMessages = subscriptionRecord ? `Auto-renewal is canceled. You will receive an email to indicate that the account will not auto-renew and that service is available for using the service for ${subscriptionRecord.plan_category} till ${moment.unix(subscriptionRecord.current_period_end).format('MM/DD/YYYY')}` : `Auto-renewal is canceled. You will receive an email to indicate that the account will not auto-renew.`;
                        yield put(practiceCancelSubscriptionSuccess(successMessages));
                        yield delay(3000);                        
                        const secret = store2.get('secret');
                        if(secret) {
                            yield put(verifySessionAction(secret));
                        }
                    } else {
                        yield put(practiceCancelSubscriptionError(DEFAULT_CANCEL_PRACTICE_SUBSCRIPTION_ERROR));
                    }
                } catch (error) {
                    yield put(practiceCancelSubscriptionError(DEFAULT_CANCEL_PRACTICE_SUBSCRIPTION_ERROR));
                }
                finally {
                    yield call(setLoader, false);
                }
            }
        })
    }

    function* globalAttorneyResponseTrackingSaga() {
        yield takeLatest(GLOBAL_ATTORNEY_RESPONSE_TRACKING, function* updater({ record }) {
            if (record) {
                try {
                    const result = yield call(updatePractice, record);
                    if (result) {
                        yield put(globalAttorneyResponseTrackingSuccess(result, 'Attorney Response Tracking Status Updated'));                       
                        const secret = store2.get('secret');
                        if(secret) {
                            yield put(verifySessionAction(secret));
                        }
                    } else {
                        yield put(globalAttorneyResponseTrackingError(DEFAULT_GLOBAL_ATTORNEY_TRACKING_STATUS_ERROR));
                    }
                } catch (error) {
                    yield put(globalAttorneyResponseTrackingError(DEFAULT_GLOBAL_ATTORNEY_TRACKING_STATUS_ERROR));
                }
            }
        })
    }


    function* vipSubscriptionSaga() {
        yield takeLatest(VIP_SUBSCRIPTION, function* updater({ record }) {
            if (record) {
                try {
                    const result = yield call(vipSubscription, record);
                    if (result) {
                        const storeRecord = yield select(selectRecord());
                        yield put(vipSubscriptionSuccess(Object.assign({}, { ...storeRecord }, { is_vip_account: record.is_vip_account }), 'VIP Subscription status updated successfully.'));                       
                        
                    } else {
                        yield put(vipSubscriptionError(DEFAULT_VIP_SUBSCRIPTION_ERROR));
                    }
                } catch (error) {
                    yield put(vipSubscriptionError(DEFAULT_VIP_SUBSCRIPTION_ERROR));
                }
            }
        })
    }
    
    function* vipCancelSubscriptionSaga() {
        yield takeLatest(VIP_CANCEL_SUBSCRIPTION, function* updater({ record }) {
            if (record) {
                try {
                    const result = yield call(vipCancelSubscription, record);
                    if (result) {
                        const storeRecord = yield select(selectRecord(record.practice_id));
                        yield put(vipCancelSubscriptionSuccess(Object.assign({}, { ...storeRecord }, { is_vip_account: record.is_vip_account }), 'VIP Subscription status updated successfully.'));                       
                        
                    } else {
                        yield put(vipCancelSubscriptionError(DEFAULT_VIP_SUBSCRIPTION_ERROR));
                    }
                } catch (error) {
                    yield put(vipCancelSubscriptionError(DEFAULT_VIP_SUBSCRIPTION_ERROR));
                }
            }
        })
    }

    return function* rootSaga() {
        yield all([
            loadPracticeSaga(),
            updatePracticeSaga(),
            loadPracticeSettingsSaga(),
            updatePracticeSettingsSaga(),
            practiceAdminCancelSubscriptionSaga(),
            practiceCancelSubscriptionSaga(),
            globalAttorneyResponseTrackingSaga(),
            vipSubscriptionSaga(),
            vipCancelSubscriptionSaga()
        ]);
    }
}


/**
 * 
 * practices selectors
 * 
 */

 import { createSelector } from 'reselect';

 /**
  * 
  * @param {string} name 
  * @returns 
  */
 export default function(name){
    const selectDomain = () => (state) => state[name] || false;
 
    const selectPractice = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.practice || {},
    );
     
     
    return {
        selectDomain,
        selectPractice
    }
     
}
 
 
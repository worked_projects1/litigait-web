/*
 *
 * practice actions
 *
 */

/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {

    const {
        LOAD_PRACTICE,
        LOAD_PRACTICE_ERROR,
        LOAD_PRACTICE_SUCCESS,
        UPDATE_PRACTICE,
        UPDATE_PRACTICE_ERROR,
        UPDATE_PRACTICE_SUCCESS,
        LOAD_PRACTICE_SETTINGS,
        LOAD_PRACTICE_SETTINGS_ERROR,
        LOAD_PRACTICE_SETTINGS_SUCCESS,
        UPDATE_PRACTICE_SETTINGS,
        UPDATE_PRACTICE_SETTINGS_ERROR,
        UPDATE_PRACTICE_SETTINGS_SUCCESS,
        PRACTICE_CANCEL_SUBSCRIPTION,
        PRACTICE_CANCEL_SUBSCRIPTION_ERROR,
        PRACTICE_CANCEL_SUBSCRIPTION_SUCCESS,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_ERROR,
        PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_SUCCESS,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING_SUCCESS,
        GLOBAL_ATTORNEY_RESPONSE_TRACKING_ERROR,
        VIP_SUBSCRIPTION,
        VIP_SUBSCRIPTION_SUCCESS,
        VIP_SUBSCRIPTION_ERROR,
        VIP_CANCEL_SUBSCRIPTION,
        VIP_CANCEL_SUBSCRIPTION_SUCCESS,
        VIP_CANCEL_SUBSCRIPTION_ERROR
    } = constants;


    /**
     * @param {integer} id 
     */
    function loadPractice(id) {
        return {
            type: LOAD_PRACTICE,
            id
        };
    }

    /**
     * @param {string} error 
     */
    function loadPracticeError(error) {
        return {
            type: LOAD_PRACTICE_ERROR,
            error,
        };
    }

    /**
     * @param {object} record 
     */
    function loadPracticeSuccess(record) {
        return {
            type: LOAD_PRACTICE_SUCCESS,
            record
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     */
    function updatePractice(record, form, setShowForm, setFormLoader) {
        return {
            type: UPDATE_PRACTICE,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * @param {string} error 
     */
    function updatePracticeError(error) {
        return {
            type: UPDATE_PRACTICE_ERROR,
            error,
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updatePracticeSuccess(record, success) {
        return {
            type: UPDATE_PRACTICE_SUCCESS,
            record,
            success
        };
    }



    /**
     * 
     * @param {integer} id 
     * @returns 
     */
    function loadPracticeSettings(id) {
        return {
            type: LOAD_PRACTICE_SETTINGS,
            id
        };
    }

    /**
     * @param {string} error 
     */
    function loadPracticeSettingsError(error) {
        return {
            type: LOAD_PRACTICE_SETTINGS_ERROR,
            error,
        };
    }

    /**
     * @param {object} record
     */
    function loadPracticeSettingsSuccess(record) {
        return {
            type: LOAD_PRACTICE_SETTINGS_SUCCESS,
            record
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     */
    function updatePracticeSettings(record, form) {
        return {
            type: UPDATE_PRACTICE_SETTINGS,
            record,
            form
        };
    }

    /**
     * @param {string} error 
     */
    function updatePracticeSettingsError(error) {
        return {
            type: UPDATE_PRACTICE_SETTINGS_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updatePracticeSettingsSuccess(record, success) {
        return {
            type: UPDATE_PRACTICE_SETTINGS_SUCCESS,
            record,
            success
        };
    }

    /**
     * @param {object} record
     * @param {function} setLoader
     * @param {boolean} propound
     */
    function practiceCancelSubscription(record, setLoader, propound) {
        return {
            type: PRACTICE_CANCEL_SUBSCRIPTION,
            record,
            setLoader,
            propound
        };
    }

    /**
     * @param {string} error 
     */
    function practiceCancelSubscriptionError(error) {
        return {
            type: PRACTICE_CANCEL_SUBSCRIPTION_ERROR,
            error,
        };
    }

    /**
     * @param {string} success 
     */
    function practiceCancelSubscriptionSuccess(success) {
        return {
            type: PRACTICE_CANCEL_SUBSCRIPTION_SUCCESS,
            success
        };
    }

    /**
     * @param {object} record
     * @param {object} subscriptionRecord
     * @param {function} setLoader
     * @param {function} loadRecord
     */
    function practiceAdminCancelSubscription(record, subscriptionRecord, setLoader, loadRecord) {
        return {
            type: PRACTICE_ADMIN_CANCEL_SUBSCRIPTION,
            record,
            subscriptionRecord,
            setLoader,
            loadRecord
        };
    }

    /**
     * @param {string} error 
     */
    function practiceAdminCancelSubscriptionError(error) {
        return {
            type: PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_ERROR,
            error,
        };
    }

    /**
     * @param {string} success 
     */
    function practiceAdminCancelSubscriptionSuccess(success) {
        return {
            type: PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_SUCCESS,
            success
        };
    }

    /**
     * @param {object} record  
     */
    function globalAttorneyResponseTracking(record) {
        return {
            type: GLOBAL_ATTORNEY_RESPONSE_TRACKING,
            record,
        }
    }

    /**
    * @param {string} success 
    */
    function globalAttorneyResponseTrackingSuccess(record, success) {
        return {
            type: GLOBAL_ATTORNEY_RESPONSE_TRACKING_SUCCESS,
            record,
            success
        }
    }

    /**
    * @param {string} error 
    */
    function globalAttorneyResponseTrackingError(error) {
        return {
            type: GLOBAL_ATTORNEY_RESPONSE_TRACKING_ERROR,
            error
        }
    }
    
    /**
    * @param {object} record 
    */
    function vipSubscription(record) {
        return {
            type: VIP_SUBSCRIPTION,
            record
        }
    }
    
    /**
    * @param {object} record
    * @param {string} success
    */
    function vipSubscriptionSuccess(record, success) {
        return {
            type: VIP_SUBSCRIPTION_SUCCESS,
            record,
            success
        }
    }
    
    /**
    * @param {string} error 
    */
    function vipSubscriptionError(error) {
        return {
            type: VIP_SUBSCRIPTION_ERROR,
            error
        }
    }
    
    /**
    * @param {object} record 
    */
    function vipCancelSubscription(record) {
        return {
            type: VIP_CANCEL_SUBSCRIPTION,
            record
        }
    }
    
    /**
    * @param {object} record
    * @param {string} success
    */
    function vipCancelSubscriptionSuccess(record, success) {
        return {
            type: VIP_CANCEL_SUBSCRIPTION_SUCCESS,
            record,
            success
        }
    }
    
    /**
    * @param {string} error 
    */
    function vipCancelSubscriptionError(error) {
        return {
            type: VIP_CANCEL_SUBSCRIPTION_ERROR,
            error
        }
    }


    return {
        loadPractice,
        loadPracticeError,
        loadPracticeSuccess,
        updatePractice,
        updatePracticeError,
        updatePracticeSuccess,
        loadPracticeSettings,
        loadPracticeSettingsError,
        loadPracticeSettingsSuccess,
        updatePracticeSettings,
        updatePracticeSettingsError,
        updatePracticeSettingsSuccess,
        practiceCancelSubscription,
        practiceCancelSubscriptionError,
        practiceCancelSubscriptionSuccess,
        practiceAdminCancelSubscription,
        practiceAdminCancelSubscriptionError,
        practiceAdminCancelSubscriptionSuccess,
        globalAttorneyResponseTracking,
        globalAttorneyResponseTrackingSuccess,
        globalAttorneyResponseTrackingError,
        vipSubscription,
        vipSubscriptionSuccess,
        vipSubscriptionError,
        vipCancelSubscription,
        vipCancelSubscriptionSuccess,
        vipCancelSubscriptionError
    }


}

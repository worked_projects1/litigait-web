/*
 *
 * Medical History actions
 *
 */


/**
 * 
 * @param {object} constants 
 * @returns 
 */
 export default function (constants) {

  const {
    UPLOAD_SUMMARY,
    UPLOAD_SUMMARY_SUCCESS,
    UPLOAD_SUMMARY_ERROR,
    DELETE_SUMMARY,
    DELETE_SUMMARY_SUCCESS,
    DELETE_SUMMARY_ERROR,
    RETRY_MEDICAL_SUMMARY,
    RETRY_MEDICAL_SUMMARY_ERROR,
    RETRY_MEDICAL_SUMMARY_SUCCESS,
  } = constants;
  
  
  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} loadRecord 
   * @param {function} setSummaryDocs 
   * @returns 
   */
  function uploadSummary(record, form, loadRecord, setSummaryDocs) {
    return {
      type: UPLOAD_SUMMARY,
      record,
      form,
      loadRecord,
      setSummaryDocs
    };
  }
  
  /**
   * @param {object} record 
   */
  function uploadSummarySuccess(record) {
    return {
      type: UPLOAD_SUMMARY_SUCCESS,
      record,
    };
  }

  /**
   * @param {string} error 
   */
  function uploadSummaryError(error) {
    return {
      type: UPLOAD_SUMMARY_ERROR,
      error,
    };
  }
  
  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} loadRecord 
   * @returns 
   */
  function deleteSummary(record, form, loadRecord) {
    return {
      type: DELETE_SUMMARY,
      record,
      form,
      loadRecord
    };
  }

  /**
   * @param {object} record 
   * @returns 
   */
  function deleteSummarySuccess(record) {
    return {
      type: DELETE_SUMMARY_SUCCESS,
      record,
    };
  }
  
  /**
   * @param {string} error 
   * @returns 
   */
  function deleteSummaryError(error) {
    return {
      type: DELETE_SUMMARY_ERROR,
      error,
    };
  }

   /**
   * 
   * @param {object} record
   * @param {function} loadRecord
   * @returns 
   */
  function retryMedicalSummary(record, loadRecord) {
    return {
      type: RETRY_MEDICAL_SUMMARY,
      record,
      loadRecord
    }
  }

   /**
   * 
   * @param {string} success 
   * @returns 
   */
  function retryMedicalSummarySuccess(success) {
    return {
      type: RETRY_MEDICAL_SUMMARY_SUCCESS,
      success
    }
  }

   /**
   * 
   * @param {string} error 
   * @returns 
   */
  function retryMedicalSummaryError(error) {
    return {
      type: RETRY_MEDICAL_SUMMARY_ERROR,
      error
    }
  }
  
  return {
    uploadSummary,
    uploadSummarySuccess,
    uploadSummaryError,
    deleteSummary,
    deleteSummarySuccess,
    deleteSummaryError,
    retryMedicalSummary,
    retryMedicalSummarySuccess,
    retryMedicalSummaryError
  }
}

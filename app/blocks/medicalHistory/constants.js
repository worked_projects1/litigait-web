/*
 *
 * Medical History constants
 *
 */

/**
 * 
 * @param {string} name 
 * @returns 
 */
 export default function (name) {
    const url = `lg/${name}`;
    
    return {
        UPLOAD_SUMMARY: `${url}/UPLOAD_SUMMARY`,
        UPLOAD_SUMMARY_SUCCESS: `${url}/UPLOAD_SUMMARY_SUCCESS`,
        UPLOAD_SUMMARY_ERROR: `${url}/UPLOAD_SUMMARY_ERROR`,
        DELETE_SUMMARY: `${url}/DELETE_SUMMARY`,
        DELETE_SUMMARY_SUCCESS: `${url}/DELETE_SUMMARY_SUCCESS`,
        DELETE_SUMMARY_ERROR: `${url}/DELETE_SUMMARY_ERROR`,
        RETRY_MEDICAL_SUMMARY: `${url}/RETRY_MEDICAL_SUMMARY`,
        RETRY_MEDICAL_SUMMARY_SUCCESS: `${url}/RETRY_MEDICAL_SUMMARY_SUCCESS`,
        RETRY_MEDICAL_SUMMARY_ERROR: `${url}/RETRY_MEDICAL_SUMMARY_ERROR`
    }
}


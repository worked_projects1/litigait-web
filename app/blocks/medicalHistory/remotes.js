/*
 *
 *  Medical History remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {
    /**
     * @param {array} record
     */
    function uploadSummary(record) {        
        return api.put(`/rest/medical-history-summery/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function retryMedicalSummary(record) {
        return api.get(`/rest/medical-history-summery-ai/retryGenerateMedicalSummary/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        uploadSummary,
        retryMedicalSummary
    }

}


/*
 *
 *  Medical History sagas
 *
 */
import { push } from 'react-router-redux';
import { call, take, put, race, all, takeLatest, delay } from 'redux-saga/effects';
import { s3BucketPublic } from 'blocks/upload/remotes';
import { getContentType } from 'components/FileUpload/utils';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import history from 'utils/history';
import {
  DEFAULT_UPDATE_RECORD_ERROR,
  DEFAULT_DELETE_RECORD_ERROR
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */


export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    UPLOAD_SUMMARY,
    DELETE_SUMMARY,
    RETRY_MEDICAL_SUMMARY
  } = constants;

  const {
    uploadSummaryError,
    uploadSummarySuccess,
    deleteSummaryError,
    deleteSummarySuccess,
    retryMedicalSummarySuccess,
    retryMedicalSummaryError
  } = actions;

  const {
    uploadSummary,
    retryMedicalSummary
  } = remotes;


  function* uploadSummarySaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { create } = yield race({
        create: take(UPLOAD_SUMMARY),
      });
      const { record, form, loadRecord, setSummaryDocs } = create || {};      
      if (record) {
        yield put(startSubmit(form));
        try {
          const uploadArr=[];
          let i = 0; 
          while(i < record.summery_url.length) {
            let data = record.summery_url[i]
              let uploadResult = yield call(s3BucketPublic, data.file_name , getContentType(data.file_name), data.uploadFile, record);
              uploadArr.push(uploadResult);
          i++;
          }
          const result = yield call(uploadSummary, Object.assign({} ,record, { summery_documents : record.summery_documents ? record.summery_documents.concat(uploadArr) : uploadArr, summery_url: ''}));
          if (result) {
            yield put(stopSubmit(form));
            yield call(setSummaryDocs, false);
            yield put(loadRecord(record.id));
          } else {
            yield put(uploadSummaryError(DEFAULT_UPDATE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_RECORD_ERROR }));
          }
        } catch (error) {
          yield put(uploadSummaryError(DEFAULT_UPDATE_RECORD_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_RECORD_ERROR }));
        }
      }
    }
  }

  function* deleteSummarySaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { edit } = yield race({
        edit: take(DELETE_SUMMARY),
      });
      const { record, form, loadRecord } = edit || {};
      if (edit) {
        yield put(startSubmit(form));
        try {
          const result = yield call(uploadSummary, record);

          if (result) {
            yield put(deleteSummarySuccess(record));
            yield put(loadRecord(record.id));
          } else {
            yield put(deleteSummaryError(DEFAULT_DELETE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_DELETE_RECORD_ERROR }));
          }

        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_RECORD_ERROR;
          yield put(deleteSummaryError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }

  function* retryMedicalSummarySaga() {
    yield takeLatest(RETRY_MEDICAL_SUMMARY, function* updater({ record, loadRecord }) {
      if (record) {
        try {
          const result = yield call(retryMedicalSummary, record);
          if (result && result.status && result.status.toLowerCase() === 'ok') {            
            yield put(retryMedicalSummarySuccess('Summary document successfully initiated'));            
            yield delay(2000);
            yield put(loadRecord(record.id));
          } else {
            yield put(retryMedicalSummaryError('Something Went Wrong!'));
          }
        } catch (error) {
          yield put(retryMedicalSummaryError('Something Went Wrong!'));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      uploadSummarySaga(),
      deleteSummarySaga(),
      retryMedicalSummarySaga()
    ]);
  }


}

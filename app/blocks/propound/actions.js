/*
 *
 * propound actions
 *
 */


/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function actions(constants) {

  const {
    UPLOAD_PROPOUND_TEMPLATE,
    UPLOAD_PROPOUND_TEMPLATE_ERROR,
    UPLOAD_PROPOUND_TEMPLATE_SUCCESS,
    GENERATE_PROPOUND_TEMPLATE,
    GENERATE_PROPOUND_TEMPLATE_ERROR,
    CANCEL_PROPOUND_TEMPLATE,
    CANCEL_PROPOUND_TEMPLATE_ERROR,
    SAVE_PROPOUND_TEMPLATE,
    SAVE_PROPOUND_TEMPLATE_ERROR,
    LOAD_PROPOUND_TEMPLATE,
    LOAD_PROPOUND_TEMPLATE_ERROR,
    LOAD_PROPOUND_TEMPLATE_SUCCESS,
    UPDATE_KEY,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE_ERROR,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE_SUCCESS,
    PROPOUND_REQUEST_HELP,
    PROPOUND_REQUEST_HELP_ERROR,
    PROPOUND_REQUEST_HELP_SUCCESS,
    SAVE_PROPOUND_QUESTIONS,
    SAVE_PROPOUND_QUESTIONS_ERROR,
    SAVE_PROPOUND_QUESTIONS_SUCCESS,
    PROPOUND_DOCUMENT_CONTENT_NOT_FOUND,
    PROPOUND_QUESTIONS_NOT_FOUND
  } = constants;

  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} setProgress 
   */
  function uploadPropoundTemplate(record, form, setProgress) {
    return {
      type: UPLOAD_PROPOUND_TEMPLATE,
      record,
      form,
      setProgress
    };
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function uploadPropoundTemplateSuccess(record) {
    return {
      type: UPLOAD_PROPOUND_TEMPLATE_SUCCESS,
      record
    }
  }

  /**
   * @param {string} error 
   */
  function uploadPropoundTemplateError(error) {
    return {
      type: UPLOAD_PROPOUND_TEMPLATE_ERROR,
      error
    }
  }


  /**
   * @param {object} record 
   * @param {function} setProgress 
   */
   function generatePropoundTemplate(record, setProgress) {
    return {
      type: GENERATE_PROPOUND_TEMPLATE,
      record,
      setProgress
    };
  }

  /**
   * @param {string} error 
   */
  function generatePropoundTemplateError(error) {
    return {
      type: GENERATE_PROPOUND_TEMPLATE_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   * @param {function} loadRecords 
   */
  function cancelPropoundTemplate(record, loadRecords) {
    return {
      type: CANCEL_PROPOUND_TEMPLATE,
      record,
      loadRecords
    };
  }

  /**
   * @param {string} error 
   */
  function cancelPropoundTemplateError(error) {
    return {
      type: CANCEL_PROPOUND_TEMPLATE_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   * @param {function} setProgress 
   */
  function savePropoundTemplate(record, setProgress) {
    return {
      type: SAVE_PROPOUND_TEMPLATE,
      record,
      setProgress
    };
  }

  /**
   * @param {string} error 
   */
  function savePropoundTemplateError(error) {
    return {
      type: SAVE_PROPOUND_TEMPLATE_ERROR,
      error
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {function} setTemplateLoader 
   * @returns 
   */
  function loadPropoundTemplate(record, setTemplateLoader, setQuestions) {
    return {
      type: LOAD_PROPOUND_TEMPLATE,
      record,
      setTemplateLoader,
      setQuestions
    };
  }

  /**
   * @param {string} error 
   */
  function loadPropoundTemplateError(error) {
    return {
      type: LOAD_PROPOUND_TEMPLATE_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function loadPropoundTemplateSuccess(record) {
    return {
      type: LOAD_PROPOUND_TEMPLATE_SUCCESS,
      record
    };
  }
  

  /**
   * @param {number} key 
   */
  function updateKey(key) {
    return {
      type: UPDATE_KEY,
      key
    };
  }


  /**
   * @param {object} record 
   */
  function savePropoundQuestionsTemplate(record) {
    return {
      type: SAVE_QUESTIONS_PROPOUND_TEMPLATE,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function savePropoundQuestionsTemplateError(error) {
    return {
      type: SAVE_QUESTIONS_PROPOUND_TEMPLATE_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function savePropoundQuestionsTemplateSuccess(success) {
    return {
      type: SAVE_QUESTIONS_PROPOUND_TEMPLATE_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   * @param {function} stepperLoader 
   * @returns 
   */
  function propoundRequestHelp(record, stepperLoader) {
    return {
      type: PROPOUND_REQUEST_HELP,
      record,
      stepperLoader
    };
  }

  /**
   * @param {string} error 
   */
  function propoundRequestHelpError(error) {
    return {
      type: PROPOUND_REQUEST_HELP_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function propoundRequestHelpSuccess(success) {
    return {
      type: PROPOUND_REQUEST_HELP_SUCCESS,
      success
    };
  }


  /**
   * @param {object} record 
   */
  function savePropoundQuestions(record) {
    return {
      type: SAVE_PROPOUND_QUESTIONS,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function savePropoundQuestionsError(error) {
    return {
      type: SAVE_PROPOUND_QUESTIONS_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function savePropoundQuestionsSuccess(success) {
    return {
      type: SAVE_PROPOUND_QUESTIONS_SUCCESS,
      success
    };
  }

  /**
   * @param {boolean} content 
   */
  function propoundDocumentContentNotFound(content) {
    return {
      type: PROPOUND_DOCUMENT_CONTENT_NOT_FOUND,
      content
    }
  }

  /**
   * 
   * @param {boolean} content 
   * @returns 
   */
  function PropoundQuestionsNotFound(content) {
    return {
      type: PROPOUND_QUESTIONS_NOT_FOUND,
      content
    }
  }

  
  return {
    uploadPropoundTemplate,
    uploadPropoundTemplateSuccess,
    uploadPropoundTemplateError,
    generatePropoundTemplate,
    generatePropoundTemplateError,
    cancelPropoundTemplate,
    cancelPropoundTemplateError,
    savePropoundTemplate,
    savePropoundTemplateError,
    loadPropoundTemplate,
    loadPropoundTemplateError,
    loadPropoundTemplateSuccess,
    updateKey,
    savePropoundQuestionsTemplate,
    savePropoundQuestionsTemplateError,
    savePropoundQuestionsTemplateSuccess,
    propoundRequestHelp,
    propoundRequestHelpError,
    propoundRequestHelpSuccess,
    savePropoundQuestions,
    savePropoundQuestionsError,
    savePropoundQuestionsSuccess,
    propoundDocumentContentNotFound,
    PropoundQuestionsNotFound
  }
}
/*
 *
 *  propound remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {str} name 
 * @returns 
 */
export default function (name) {

    /**
     * @param {object} record 
     */
    function uploadPropoundTemplate(record) {
        return api.post(`rest/propound/upload-template`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function generatePropoundTemplate(record) {
        return api.get(`${process.env.SCANNED_URL}/extract?key=${record.s3_file_key}&document_type=${record.document_type.toUpperCase()}&id=${record.id}&state=${record.state}&practice_id=${record.practice_id}&propounding=${record.propounding}&hash_id=${record.hash_id}&doc_textract_region=${record.doc_textract_region}&doc_textract_region_count=${record.doc_textract_region_count}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function cancelPropoundTemplate(record) {
        return api.put(`propound/document-upload-progreess/set-status`, Object.assign({}, record)).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function savePropoundTemplate(record) {
        return api.post(`propound/save-template-questions`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function getPropoundDocumentStatus(record) {
        return api.get(`propound/document-upload-progreess/get-status?document_type=${record.document_type.toUpperCase()}&propound_template_id=${record.id}&practice_id=${record.practice_id}`).then((response) => response.data && response.data.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function loadPropoundTemplate(record) {
        return api.get(`rest/propound/template/${record.id}/${record.document_type}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function propoundRequestHelp(record) {
        return api.post(`rest/propound-support`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function savePropoundQuestions(record) {
        return api.post(`rest/propound`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }


    /**
     * @param {object} record
     */
    function hashedFileQuestions(record) {
        return api.post(`/rest/hash/file-hashing`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        uploadPropoundTemplate,
        generatePropoundTemplate,
        cancelPropoundTemplate,
        savePropoundTemplate,
        getPropoundDocumentStatus,
        loadPropoundTemplate,
        propoundRequestHelp,
        savePropoundQuestions,
        hashedFileQuestions
    }

}

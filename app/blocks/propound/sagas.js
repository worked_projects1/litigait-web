/*
 *
 *  propound sagas
 *
 */

import { push } from 'react-router-redux';
import { call, put, all, select, delay, takeLatest } from 'redux-saga/effects';
import { startSubmit, stopSubmit, destroy } from 'redux-form/immutable';
import history from 'utils/history';
import { getDuplicates, autoIncrementQuestions, TimeOutArr } from 'utils/tools';

import {
  DEFAULT_LOAD_FORM_ERROR,
  DEFAULT_SAVE_QUESTIONS_FORM_ERROR,
  DEFAULT_CANCEL_FORM_ERROR,
  DEFAULT_PROPOUND_DOCUMENT_ERROR,
  DEFAULT_REQUEST_HELP_ERROR,
  DEFAULT_PROPOUND_DOCUMENT_CONTENT_NOT_FOUND_ERROR
} from 'utils/errors';

import { uploadFile } from 'utils/api';

import records from 'blocks/records';

const propoundSaga = records('rest/propound');
const supportRequestSaga = records('rest/propound-support');
const { actions: propoundActions, selectors: propoundSelectors } = propoundSaga;
const { selectRecord, selectRecords } = propoundSelectors;
const { loadRecords } = propoundActions;

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    UPLOAD_PROPOUND_TEMPLATE,
    GENERATE_PROPOUND_TEMPLATE,
    CANCEL_PROPOUND_TEMPLATE,
    SAVE_PROPOUND_TEMPLATE,
    LOAD_PROPOUND_TEMPLATE,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE,
    PROPOUND_REQUEST_HELP,
    SAVE_PROPOUND_QUESTIONS
  } = constants;

  const {
    uploadPropoundTemplateSuccess,
    uploadPropoundTemplateError,
    generatePropoundTemplate: generatePropoundTemplateAction,
    generatePropoundTemplateError,
    cancelPropoundTemplateError,
    savePropoundTemplate: savePropoundTemplateAction,
    savePropoundTemplateError,
    loadPropoundTemplate: loadPropoundTemplateAction,
    loadPropoundTemplateError,
    loadPropoundTemplateSuccess,
    updateKey,
    savePropoundQuestionsTemplateError,
    savePropoundQuestionsTemplateSuccess,
    propoundRequestHelpError,
    propoundRequestHelpSuccess,
    savePropoundQuestionsError,
    savePropoundQuestionsSuccess,
    propoundDocumentContentNotFound,
    PropoundQuestionsNotFound
  } = actions;


  const {
    uploadPropoundTemplate,
    generatePropoundTemplate,
    cancelPropoundTemplate,
    savePropoundTemplate,
    getPropoundDocumentStatus,
    loadPropoundTemplate,
    propoundRequestHelp,
    savePropoundQuestions,
    hashedFileQuestions
  } = remotes;




  function* uploadPropoundTemplateSaga() {
    yield takeLatest(UPLOAD_PROPOUND_TEMPLATE, function* updater({ record, form, setProgress }) {
      if (record) {

        yield put(startSubmit(form));
        try {
          yield put(updateKey(0));
          yield put(uploadPropoundTemplateSuccess({}));
          yield delay(1000);
          const result = yield call(uploadPropoundTemplate, record);
          if (result) {
            yield call(uploadFile, result.uploadURL, record.uploadFile, record.content_type);
            yield put(updateKey(1));
            yield put(uploadPropoundTemplateSuccess(Object.assign({}, { id: result.PropoundTemplatesObj.id, document_s3_key: result.s3_file_key, propound_template_id: result.PropoundTemplatesObj.id, practice_id: record.practice_id, document_type: result.PropoundTemplatesObj.document_type, file_name: record.file_name, state: record.state, upload_document_name: record.upload_document_name })));
            yield put(generatePropoundTemplateAction(Object.assign({}, { id: result.PropoundTemplatesObj.id, propound_template_id: result.PropoundTemplatesObj.id, document_type: record.document_type, s3_file_key: result.s3_file_key, filename: record.file_name, state: record.state, practice_id: record.practice_id, propounding: 'true', upload_document_name: record?.upload_document_name, doc_textract_region : result.doc_textract_region, doc_textract_region_count : result.doc_textract_region_count }), setProgress));
            yield put(stopSubmit(form));
          } else {
            yield put(uploadPropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_PROPOUND_DOCUMENT_ERROR }));
          }
        } catch (error) {
          yield put(uploadPropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_PROPOUND_DOCUMENT_ERROR }));
        } finally {
          yield put(destroy(form));
        }
      }
    })
  }



  function* generatePropoundTemplateSaga() {
    yield takeLatest(GENERATE_PROPOUND_TEMPLATE, function* updater({ record, setProgress }) {
      if (record) {
        try {

          // let result = {};
          // if (record.document_type && record.document_type.toLowerCase() === 'frogs') {
          //   result = {
          //     "questions": [
          //       {
          //         "id": "0e372128-5ca4-440d-ac02-ffc1b10f55bf",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 1,
          //         "question_number_text": "1.1",
          //         "question_number": "1.1",
          //         "question_text": "State the name, ADDRESS, telephone number, and relationship to you of each PERSON who prepared or assisted in the preparation of the responses to these interrogatories. (Do not identify anyone who simply typed or reproduced the responses.)",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d6086fbd-9a85-41bd-a809-ac1e9f5c54c5",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 2,
          //         "question_number_text": "2.1",
          //         "question_number": "2.1",
          //         "question_text": "State:\n(a) your name;\n(b) every name you have used in the past; \n(c) the dates you used each name.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "9bac560e-358f-4aa6-b38e-dadd9a7e37c7",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 3,
          //         "question_number_text": "2.2",
          //         "question_number": "2.2",
          //         "question_text": "State the date and place of your birth.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d474ae68-535e-4863-bacc-d79f7e697bdb",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 4,
          //         "question_number_text": "2.5",
          //         "question_number": "2.5",
          //         "question_text": "State: \n(a) your present residence ADDRESS; \n(b) your residence ADDRESSES for the past five years; and \n(c) the dates you lived at each ADDRESS.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d353e3f7-3257-4d60-b921-20edc610664e",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 5,
          //         "question_number_text": "2.7",
          //         "question_number": "2.7",
          //         "question_text": "State: \n(a) the name and ADDRESS of each school or other academic or vocational institution you have attended, beginning with high school; \n(b) the dates you attended; \n(c) the highest grade level you have completed; and \n(d) the degrees received.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //     ]
          //   };
          // } else if (record.document_type) {
          //   result = {
          //     "questions": [
          //       {
          //         "question_number": "1",
          //         "question_number_text": "1",
          //         "question_id": 1,
          //         "question_text": "State  your  name,  any  other  names  by  which you have been known, and your ADDRESS."
          //       },
          //       {
          //         "question_number": "2",
          //         "question_number_text": "2",
          //         "question_id": 2,
          //         "question_text": "State the date and place of your birth."
          //       },
          //       {
          //         "question_number": "3",
          //         "question_number_text": "3",
          //         "question_id": 3,
          //         "question_text": "State, as of the time of the  INCIDENT,  your driver's license number, the state of issuance, the expiration date, and any restrictions."
          //       }
          //     ]
          //   };
          // } else {
          //   result = yield call(generatePropoundTemplate, record);
          // }

          const hashedFileQuestionsResult = yield call(hashedFileQuestions, Object.assign({}, { s3_file_key: record?.s3_file_key, template_id: record.propound_template_id, filename: record.upload_document_name, uploaded_type : "Propounding", state: record.state, document_type: record.document_type })); 
          const hashId = hashedFileQuestionsResult?.['hashResponse']?.id || false;

          yield put(uploadPropoundTemplateSuccess(Object.assign({}, { id: record.propound_template_id, document_s3_key: record?.s3_file_key, propound_template_id: record.propound_template_id, practice_id: record.practice_id, document_type: record.document_type, file_name: record.filename, state: record.state, upload_document_name: record.upload_document_name, hash_id : hashId })));

          if(hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'NoQuestions'){
            yield put(PropoundQuestionsNotFound(true));
            yield put(generatePropoundTemplateError("Questions not found in this file"));
            yield put(stopSubmit(`forms_${record.document_type.toLowerCase()}`));        
            return;
          }

          if (hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'true') {
            yield put(PropoundQuestionsNotFound(false));
            yield put(savePropoundTemplateAction(Object.assign({ questions: hashedFileQuestionsResult['hashResponse'].questions, propound_template_id: record.id, practice_id: record.practice_id, document_type: record.document_type, hash_id: hashId, id: record.id, state : record.state }), setProgress));
            yield delay(4000);
            // yield put(loadPropoundTemplateAction(Object.assign({}, { id: record.id, document_type: record.document_type.toUpperCase(), propound_template_id: record.id })));

            while (true) { // eslint-disable-line no-constant-condition
              const storeRecord = yield select(selectRecord(record.id));
              if (storeRecord && storeRecord.questions) {
                if (record.document_type && record.document_type.toLowerCase() !== 'frogs') {
                  yield put(push({
                    pathname: `/rest/propound/${record.id}/propoundQuestions/${record.document_type}`,
                    state: Object.assign({}, { ...history.location.state }, {
                      document_type: record.document_type,
                      propound_template_id: record && record.id,
                      questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
                      filename: record.filename,
                      disableNote: false
                    })
                  }));
                  yield call(setProgress, false);
                }
                break;
              }
              yield delay(500);
            }
            yield put(updateKey(3));
            // return;
          } else if(hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'false') {
            yield put(PropoundQuestionsNotFound(false));
            const result = yield call(generatePropoundTemplate, Object.assign({}, record, { hash_id: hashId }));

            if (result) {
              let timeIntervalArr = TimeOutArr(result);

              yield put(updateKey(2));
              if (result.document_modal === 'scanned') {
                var i = 0;

                // Delays the dispatch of select record untill the store is populated with an initial list of Questions.
                while (i < timeIntervalArr.length) {
                  if (i == 0) {
                    yield delay(10000);
                  }
                  const DocumentStatus = yield call(getPropoundDocumentStatus, record);

                  yield delay(timeIntervalArr[i]);
                  yield put(propoundDocumentContentNotFound(false));

                  if (DocumentStatus) {
                    const uploading_status = DocumentStatus && DocumentStatus.status || false;
                    if (uploading_status && uploading_status === 'Content_not_found') {
                      yield put(propoundDocumentContentNotFound(true));
                      yield put(stopSubmit(`forms_${record.document_type.toLowerCase()}`));
                      yield put(generatePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_CONTENT_NOT_FOUND_ERROR));
                      return;
                    }

                    if (uploading_status && uploading_status === 'Completed') {
                      yield put(loadPropoundTemplateAction(Object.assign({}, { id: record.id, document_type: record.document_type.toUpperCase(), propound_template_id: record.id })));
                      yield delay(4000);

                      while (true) { // eslint-disable-line no-constant-condition
                        const storeRecord = yield select(selectRecord(record.id));
                        if (storeRecord && storeRecord.questions) {
                          if (record.document_type && record.document_type.toLowerCase() !== 'frogs') {
                            yield put(push({
                              pathname: `/rest/propound/${record.id}/propoundQuestions/${record.document_type}`,
                              state: Object.assign({}, { ...history.location.state }, {
                                document_type: record.document_type,
                                propound_template_id: record && record.id,
                                questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
                                filename: record.filename,
                                disableNote: false
                              })
                            }));
                            yield call(setProgress, false);
                          }
                          break;
                        }
                        yield delay(500);
                      }

                      yield put(updateKey(3));
                      return;
                    } else if (uploading_status && uploading_status === 'cancelled') {
                      yield put(stopSubmit(`forms_${record.document_type.toLowerCase()}`));
                      return;
                    } else if ((i == (timeIntervalArr.length - 1)) || (uploading_status && uploading_status === 'Failed')) {
                      yield put(generatePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
                      return;
                    }
                  } else {
                    yield put(generatePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
                    return;
                  }


                  i++;
                }

              } else {
                yield put(savePropoundTemplateAction(Object.assign({}, result, record), setProgress));
              }
            } else {
              yield put(generatePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
            }
          } else {
            yield put(generatePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
          }
        } catch (error) {
          yield put(generatePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
        }
      }
    })
  }


  function* cancelPropoundTemplateSaga() {
    yield takeLatest(CANCEL_PROPOUND_TEMPLATE, function* updater({ record, loadRecords }) {
      if (record) {
        try {
          const result = yield call(cancelPropoundTemplate, record);

          if (result) {
            yield put(loadRecords(true));
            yield put(destroy('propoundForm'));
          } else {
            yield put(cancelPropoundTemplateError(DEFAULT_CANCEL_FORM_ERROR));
            yield put(loadRecords(true));
          }
        } catch (error) {
          yield put(cancelPropoundTemplateError(DEFAULT_CANCEL_FORM_ERROR));
          yield put(loadRecords(true));
        }
      }
    })
  }


  function* savePropoundTemplateSaga() {
    yield takeLatest(SAVE_PROPOUND_TEMPLATE, function* updater({ record, setProgress }) {
      if (record) {

        try {
          const result = yield call(savePropoundTemplate, record);
          if (result) {

            yield put(loadPropoundTemplateAction(Object.assign({}, { id: record.id, document_type: record.document_type.toUpperCase() })));
            yield delay(2000);

            // while (true) { // eslint-disable-line no-constant-condition
            //   const storeRecord = yield select(selectRecord(record.id));
            //   if (storeRecord && storeRecord.questions) {
            //     if (record.document_type && record.document_type.toLowerCase() !== 'frogs') {
            //       yield put(push({
            //         pathname: `/rest/propound/${record.id}/propoundQuestions/${record.document_type}`,
            //         state: Object.assign({}, { ...history.location.state }, {
            //           document_type: record.document_type,
            //           questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
            //           filename: record.filename,
            //           disableNote: false
            //         })
            //       }));
            //       yield call(setProgress, false);
            //     }
            //     break;
            //   }
            //   yield delay(500);
            // }

            yield put(updateKey(3));
          } else {
            yield put(savePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
          }
        } catch (error) {
          yield put(savePropoundTemplateError(DEFAULT_PROPOUND_DOCUMENT_ERROR));
        }
      }
    })
  }


  function* loadPropoundTemplateSaga() {
    yield takeLatest(LOAD_PROPOUND_TEMPLATE, function* updater({ record, setTemplateLoader, setQuestions }) {
      if (record) {
        try {
          const result = yield call(loadPropoundTemplate, record);
          if (result) {
            // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
            while (true) { // eslint-disable-line no-constant-condition
              const recordsInStore = yield select(selectRecords());
              if (recordsInStore && recordsInStore.length > 0) {
                break;
              }
              yield delay(500);
            }

            yield put(loadPropoundTemplateSuccess(Object.assign({}, { id: record.id, questions: result })));
            if(result && setQuestions) {
              yield call(setQuestions, result);
            }
            if (setTemplateLoader) {
              yield delay(200);
              yield call(setTemplateLoader, false);
            }
          } else {
            yield put(loadPropoundTemplateError(DEFAULT_LOAD_FORM_ERROR));
          }
        } catch (error) {
          yield put(loadPropoundTemplateError(DEFAULT_LOAD_FORM_ERROR));
        }
      }
    })
  }



  function* savePropoundQuestionsTemplateSaga() {
    yield takeLatest(SAVE_QUESTIONS_PROPOUND_TEMPLATE, function* updater({ record }) {
      if (record) {

        let { questions = [] } = record;
        let duplicate;
        try {
          questions = typeof questions === 'string' && JSON.parse(questions) || questions;
          if (record.form && record.form === 'questionsForm') {
            duplicate = getDuplicates(questions.map(_ => { return _.question_number }));
          }
          const validate = questions.filter(_ => _.question_text === '' || _.question_number === '' || _.question_number_text === '');

          if (validate && validate.length === 0) {
            if (duplicate && duplicate != '' && record.form && record.form === 'questionsForm') {
              yield put(savePropoundQuestionsTemplateError(`Question Number ${duplicate} duplicated`));
            } else {
              const result = record;
              if (result) {
                let questionsData = questions;
                questionsData = questionsData && questionsData.length > 0 && questionsData || questions;
                const finalRecord = Object.assign({}, record, { questions: questionsData });

                const upload = yield call(uploadPropoundTemplate, finalRecord);
                if (upload) {
                  const save = yield call(savePropoundTemplate, Object.assign({}, { ...finalRecord }, { id: upload.PropoundTemplatesObj.id, document_type: finalRecord.document_type.toUpperCase(), propound_template_id:  upload.PropoundTemplatesObj.id, questions: questionsData }));
                  if (save) {
                    yield put(savePropoundQuestionsTemplateSuccess("Questions Saved"));
                    yield put(push({ pathname: `/rest/propound`, state: history.location.state }));
                    yield put(loadRecords(true));
                  } else {
                    yield put(savePropoundQuestionsTemplateError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
                  }
                } else {
                  yield put(savePropoundQuestionsTemplateError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
                }
              } else {
                yield put(savePropoundQuestionsTemplateError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
              }
            }
          } else {
            yield put(savePropoundQuestionsTemplateError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
          }

        } catch (error) {
          yield put(savePropoundQuestionsTemplateError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
        }
      }
    })
  }

  function* propoundRequestHelpSaga() {
    yield takeLatest(PROPOUND_REQUEST_HELP, function* updater({ record, stepperLoader }) {
      if (record) {

        try {
          const result = yield call(propoundRequestHelp, record);

          if (result) {
            yield put(propoundRequestHelpSuccess('Support request sent to EsquireTek. Our support team will extract questions from this document and update the system. You will receive an email once questions are updated for this template.'));
            yield call(cancelPropoundTemplate, Object.assign({}, { practice_id: record.practice_id, status: 'cancelled', document_type: record.document_type, s3_file_key: record.document_s3_key, propound_template_id: record.propound_template_id }));
          } else {
            yield put(propoundRequestHelpError(DEFAULT_REQUEST_HELP_ERROR));
          }
        } catch (error) {
          yield put(propoundRequestHelpError(DEFAULT_REQUEST_HELP_ERROR));
        } finally {
          if (stepperLoader) {
            yield call(stepperLoader, false);
          }
        }
      }
    })
  }


  function* savePropoundQuestionsSaga() {
    yield takeLatest(SAVE_PROPOUND_QUESTIONS, function* updater({ record }) {
      if (record) {
        const submitData = Object.assign({}, { practice_id: record.practice_id, questions: record.questions, document_type: record.document_type, content_type: 'application/pdf', from: 'questions_form', form: 'questionsForm', file_name: record.file_name, upload_document_name: record.upload_document_name, propound_help_request_id: record.id, s3_file_key: record.document_s3_key, state: record.state, user_email: record.user_email });

        let { questions = [], form } = submitData;
        let duplicate;
        try {
          questions = typeof questions === 'string' && JSON.parse(questions) || questions;
          if (form && form === 'questionsForm') {
            duplicate = getDuplicates(questions.map(_ => { return _.question_number }));
          }
          const validate = questions.filter(_ => _.question_text === '' || _.question_number === '' || _.question_number_text === '');

          if (validate && validate.length === 0) {
            if (duplicate && duplicate != '' && form && form === 'questionsForm') {
              yield put(savePropoundQuestionsError(`Question Number ${duplicate} duplicated`));
            } else {
              const result = submitData;
              if (result) {
                const upload = yield call(savePropoundQuestions, Object.assign({}, submitData, { questions }));
                if (upload) {
                  yield put(savePropoundQuestionsSuccess("Questions Saved"));
                  yield put(supportRequestSaga.actions.loadRecords(true));
                  yield put(push({ pathname: `/supportCategories/rest/propound-support`, state: history.location.state }));
                } else {
                  yield put(savePropoundQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
                }
              } else {
                yield put(savePropoundQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
              }
            }
          } else {
            yield put(savePropoundQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
          }
        } catch (error) {
          yield put(savePropoundQuestionsError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      uploadPropoundTemplateSaga(),
      generatePropoundTemplateSaga(),
      cancelPropoundTemplateSaga(),
      savePropoundTemplateSaga(),
      loadPropoundTemplateSaga(),
      savePropoundQuestionsTemplateSaga(),
      propoundRequestHelpSaga(),
      savePropoundQuestionsSaga()
    ]);
  }
}



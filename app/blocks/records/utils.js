
/*
 *
 *  utils
 *
 */

import moment from 'moment';
import { AsYouType } from 'libphonenumber-js';
import 'moment-timezone';

/**
 * @param {object} records 
 */
export function mapPractices(records) {
    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.phone = Record.phone && new AsYouType('US').input(Record.phone) || Record.phone;
            Record.fax = Record.fax && new AsYouType('US').input(Record.fax) || Record.fax;
            Record.createdAt = record.createdAt && moment(record.createdAt).tz(moment.tz.guess()).format('MM/DD/YYYY') || false;
            return Record;
        });
    }
    return records;
}

/**
 * @param {object} records 
 */
export function mapClients(records) {
    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.createdAt = record.createdAt && moment(record.createdAt).tz(moment.tz.guess()).format('MM/DD/YYYY') || false;
            Record.dob = record.dob && moment(record.dob).tz(moment.tz.guess()).format('MM/DD/YYYY') || false;
            Record.phone = Record.phone && new AsYouType('US').input(Record.phone) || Record.phone;
            return Record;
        });
    }
    return records;
}


/**
 * @param {object} records 
 */
 export function mapOtherParties(records) {
    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.createdAt = record.createdAt && moment(record.createdAt).tz(moment.tz.guess()).format('MM/DD/YYYY') || false;
            Record.dob = record.dob && moment(record.dob).tz(moment.tz.guess()).format('MM/DD/YYYY') || false;
            Record.phone = Record.phone && new AsYouType('US').input(Record.phone) || Record.phone;
            return Record;
        });
    }
    return records;
}


/**
 * @param {object} records 
 */
export function mapCases(records) {

    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.date_of_loss = record.date_of_loss && moment(record.date_of_loss).tz(moment.tz.guess()).format('MM/DD/YYYY') || '';
            Record.createdAt = record.createdAt && moment(record.createdAt).tz(moment.tz.guess()).format('MM/DD/YYYY') || '';
            return Record;
        });
    }
    return records;

}

/**
 * @param {object} records 
 */
export function mapOrders(records) {
    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.order_date = record.order_date && moment(record.order_date).tz(moment.tz.guess()).format('MM/DD/YYYY HH:mm:ss') || false;
            Record.document_generation_type = record.document_generation_type != null ? record.document_generation_type === 'medical' ? 'Medical' : record.document_generation_type == 'template' ? 'Shell' : record.document_generation_type == 'teksign' ? 'TekSign': record.document_generation_type == 'pos' ? 'PoS' : record.document_generation_type == 'propound_doc' ? 'Propound Doc' : 'Discovery' : false;
            Record.amount_charged = record.amount_charged != null ? `$` + record.amount_charged : $0;
            Record.document_type = record.document_type != null && record.document_type === 'MEDICAL_HISTORY' ? 'MEDICAL HISTORY' : record.document_type === 'pos' ? 'PoS' : record.document_type;
            return Record;
        });
    }
    return records;

}

/**
 * @param {object} records 
 */
export function mapMedicalHistory(records) {

    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.request_date = record.request_date && moment(record.request_date).tz(moment.tz.guess()).format('MM/DD/YYYY HH:mm:ss') || false;
            Record.assigned_to = record.assigned_to ? record.assigned_to : 'N/A';
            Record.amount_charged = record.amount_charged != null ? `$` + record.amount_charged : '$0';
            return Record;
        });
    }
    return records;

}

/**
 * @param {object} records 
 */
export function mapSupportRequest(records) {

    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.createdAt = record.createdAt && moment(record.createdAt).tz(moment.tz.guess()).format('MM/DD/YYYY HH:mm:ss') || false;
            return Record;
        });
    }
    return records;

}

/**
 * @param {object} records 
 */
 export function mapSubscriptions(records) {

    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.plan_type = ['responding_vip', 'propounding_vip'].includes(Record.plan_type) ? 'VIP ACCOUNT' : Record.plan_type === 'activationFee' ? 'ACTIVATION FEE' : Record.plan_type && Record.plan_type.toUpperCase() || null;
            Record.price = record.price != null ? `$` + record.price : record.price && $0;
            Record.subscription_valid_till = Record.subscription_valid_till && moment(Record.subscription_valid_till).tz(moment.tz.guess()).format('MM/DD/YY') || '-';
            Record.order_date = Record.order_date && moment(Record.order_date).tz(moment.tz.guess()).format('MM/DD/YY');
            return Record;
        });
    }
    return records;

}

/**
 * @param {object} records 
 * @param {string} name 
 */
export default function mapRecords(records, name) {
    switch (name) {
        case 'clients':
            return mapClients(records);
        case 'cases':
            return mapCases(records);
        case 'orders':
        case 'rest/admin/orders':
            return mapOrders(records);
        case 'practices':
            return mapPractices(records);
        case 'rest/medical-history-summery':
            return mapMedicalHistory(records);
        case 'rest/help-request':
            return mapSupportRequest(records);
        case 'rest/other-parties':
            return mapOtherParties(records);  
        case 'rest/subscriptions/history':
        case 'rest/subscriptions/admin/history':
            return mapSubscriptions(records);    
        default:
            return records;
    }
}

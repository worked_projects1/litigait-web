
/*
 *
 *  sagas
 *
 */

import { push } from 'react-router-redux';
import { call, take, put, race, select, all, delay, takeLatest } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { selectUser, isSwitchPracticeRender } from 'blocks/session/selectors';
import appRemotes from './remotes';
import history from 'utils/history';
import lodash from 'lodash';
import {
  DEFAULT_CREATE_RECORD_ERROR,
  DEFAULT_UPDATE_RECORD_ERROR,
  DEFAULT_DELETE_RECORD_ERROR,
  DEFAULT_EXCEL_EXPORT_ERROR
} from 'utils/errors';
import validation from './validation';
import { createDownLoadData, isPropound, convertUTC, convertParamsToObject, excelModifiedData, decryptApiKeyAndSecret } from 'utils/tools';
import { verifySession as verifySessionAction, loadRespondDetailSuccess, verifySessionSuccess, clearSwitchPracticeRendering } from 'blocks/session/actions';
import store2 from 'store2';
import { selectFilterData, selectCreatedDateFilter, selectActiveFilter } from 'blocks/analytics/selectors';
import { verifySession as verifySessionApi } from 'blocks/session/remotes';
import { setFilevineAuthToken, setFilevineBaseUrl } from 'utils/api';

const CryptoJS = require("crypto-js");
const base64 = require('base-64');

const clientCategoriesPage = ['clients', 'rest/other-parties'];
const casesCategories =  ['cases'];
const userCategoriesPage = ['rest/esquiretek/users', 'rest/practice/users'];
const loadRecordsWithHeaderPage = ['practices', 'rest/admin/orders', 'rest/medical-history-summery', 'rest/forms/otps', 'rest/OtherParties/otps', 'orders', 'cases', 'rest/esquiretek/users', 'rest/practice/users', 'rest/archive/cases', 'rest/practice-template','rest/subscriptions/admin/history', 'rest/subscriptions/history', 'rest/practice-propound-template'];

export default function sagas(constants, actions, remotes, selectors, entityUrl, additionalSaga) {

  const {
    LOAD_RECORD,
    LOAD_RECORDS,
    CREATE_RECORD,
    UPDATE_RECORD,
    DELETE_RECORD,
    LOAD_RECORDS_META_DATA,
    EXCEL_EXPORT,
    CREATE_CLIENT_WITH_CASE,
    CREATE_RESPONDER_FORM,
    NOTIFICATION_STATUS,
    LOAD_ACTIVE_FILTER_RECORD,
    DELETE_SUPPORT_REQUEST,
    UPDATE_LITIFY_RECORD,
    CREATE_USER
  } = constants;

  const {
    loadRecord: loadRecordAction,
    loadRecordSuccess,
    loadRecordError,
    loadRecords: loadRecordsAction,
    loadRecordsSuccess,
    loadRecordsError,
    createRecordSuccess,
    createRecordError,
    updateRecordSuccess,
    updateRecordError,
    deleteRecordSuccess,
    deleteRecordError,
    loadRecordsCacheHit,
    loadRecordsMetaData,
    loadRecordsMetaDataSuccess,
    loadRecordsMetaDataError,
    excelExportError,
    createClientWithCaseError,
    createClientWithCaseSuccess,
    createResponderFormError,
    createResponderFormSuccess,
    notificationStatusError,
    deleteSupportRequestSuccess,
    deleteSupportRequestError,
    litifyInfoDialog: litifyInfoDialogAction,
    updateLitifyRecordSuccess,
    updateLitifyRecordError,
    updateExistingPasswordData,
    createRecord: createRecordAction,
    syncRecords: syncRecordsActions
  } = actions;

  const {
    loadRecord,
    loadRecords,
    loadRecordsWithHeader,
    createRecord,
    updateRecord,
    deleteRecord,
    excelExport,
    createClientWithCases,
    loadDashboardRecords,
    excelExportNoLimits,
    myCaseUpdateData,
    myClientUpdateData,
    UpdateCasesToFilevine,
    updateClientToFilevine,
    filevineApiSession,
    storeFilevineSecret,
    updateCasesToLitify,
    updateClientToLitify,
    deleteSupportRequest,
    checkUserExist,
    clioUpdateClientData,
    clioUpdateCaseData,
    clioSyncRecord,
    getAllOtherPartiesCasesDetails
  } = remotes;

  const {
    selectRecord,
    selectRecords,
    selectUpdateTimestamp,
    selectHeaders,
    selectTotalPageCount
  } = selectors;




  function* loadRecordsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const load = yield race({
        explicitLoad: take(LOAD_RECORDS),
      });

      const { explicitLoad } = load;
      const { invalidateCache, queryParams } = explicitLoad || {};
      const lastLoad = yield select(selectUpdateTimestamp());
      const currentTimestamp = Math.floor(Date.now() / 1000);
      const VALID_CACHE_DIFF = entityUrl === 'cases' ? -240 : entityUrl === 'rest/archive/cases' ? -10 : -30;

      yield put(loadRecordsMetaData());// Calling Dropdown API's

      const isSwitchPracticeRendering = yield select(isSwitchPracticeRender());

      if (isSwitchPracticeRendering && isSwitchPracticeRendering?.[entityUrl]) {
        const data = yield call(loadRecords);
        yield put(loadRecordsSuccess(data));
        yield put(clearSwitchPracticeRendering(Object.assign({ [entityUrl]: false })));
      }
      else if (explicitLoad) {
        if (!invalidateCache && (lastLoad && (lastLoad - currentTimestamp) > VALID_CACHE_DIFF)) {
          yield put(loadRecordsCacheHit());
        } else {
          try {
            const headers = yield select(selectHeaders());
            const filter = yield select(selectFilterData());
            const createdDate = yield select(selectCreatedDateFilter());
            const activeFilter = yield select(selectActiveFilter());

            const filterDate = createdDate && Object.keys(createdDate).length > 0 && entityUrl === 'practices' && (createdDate.practices && Object.keys(createdDate.practices).length > 0 ? createdDate.practices : {}) || entityUrl === 'rest/practice/users' && (createdDate.practiceUsers && Object.keys(createdDate.practiceUsers).length > 0 ? createdDate.practiceUsers : {}) || entityUrl === 'rest/subscriptions/admin/history' && (createdDate.subscriptions && Object.keys(createdDate.subscriptions).length > 0 ? createdDate.subscriptions : {}) || {};
            const filterActive = activeFilter && Object.keys(activeFilter).length > 0 && entityUrl === 'practices' && (activeFilter.practices && Object.keys(activeFilter.practices).length > 0 ? activeFilter.practices : {}) || entityUrl === 'rest/practice/users' && (activeFilter.practiceUsers && Object.keys(activeFilter.practiceUsers).length > 0 ? activeFilter.practiceUsers : {}) || entityUrl === 'rest/subscriptions/admin/history' && (activeFilter.subscriptions && Object.keys(activeFilter.subscriptions).length > 0 ? activeFilter.subscriptions : {}) || {};

            let result;
            if (filter && filter != 'total') {
              result = yield call(loadDashboardRecords, headers, queryParams, filter);
            } else if (!lodash.isEmpty(filterDate) || !lodash.isEmpty(filterActive)) {
              const utcConvertedDate = filterDate && Object.keys(filterDate).length > 0 && convertUTC(filterDate) || false;
              const active = filterActive && Object.keys(filterActive).length > 0 && filterActive && filterActive.active ? 'active' : false;
              let filter = (utcConvertedDate && active) && `filter_type=${active}&from_date=${utcConvertedDate.from_date}&to_date=${utcConvertedDate.to_date}` || (utcConvertedDate) && `from_date=${utcConvertedDate.from_date}&to_date=${utcConvertedDate.to_date}` || active && `filter_type=${active}`;

              result = yield call(loadDashboardRecords, headers, queryParams, filter);
            } else {
              result = loadRecordsWithHeaderPage.includes(entityUrl) ? yield call(loadRecordsWithHeader, headers, queryParams) : yield call(loadRecords);
            }

            if (result && (loadRecordsWithHeaderPage.includes(entityUrl))) {
              yield put(loadRecordsSuccess(result.data, result.headers && result.headers.totalpagecount || false));
            } else if (result) {
              yield put(loadRecordsSuccess(result));
            } else {
              yield put(loadRecordsError());
            }
          } catch (error) {
            yield put(loadRecordsError(error));
          }
        }
      }
    }
  }

  function* loadRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const loadRequest = yield race({
        request: take(LOAD_RECORD),
      });
      const { request } = loadRequest;

      if (request) {
        const { id, queryParams } = request;
        try {
          const record = yield call(loadRecord, id, queryParams);
          const user_info = yield select(selectUser());

          let recordsMetaData = {};
          if (entityUrl === 'cases') {
            const partiesRemotes = yield call(appRemotes, 'rest/other-parties');
            const partiesRecords = yield call(partiesRemotes.loadRecords);
            const customerObjectionsRemotes = yield call(appRemotes);
            const customerObjections = yield call(customerObjectionsRemotes.customerObjections, Object.assign({}, { state: record?.state}));
            const parties = partiesRecords.filter(_ => _.case_id === id);
            recordsMetaData = { parties, partiesWithClient: record && record.client_name && record.client_id && [Object.assign({}, { name: `${record.client_name} (Client)`, id: record.client_id })].concat(parties), client_id: record && record.client_id || false, customerObjections };
          }

          const clio_record = (record?.client_from === 'clio' || record?.case_from === 'clio') ? true : false;

          function* saveRecordIntoStore (record, recordsMetaData, sync=false) {
            if (record) {
              // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
              while (true) { // eslint-disable-line no-constant-condition
                const recordsInStore = yield select(selectRecords());
                if (recordsInStore && recordsInStore.length > 0) {
                  break;
                }
                yield delay(500);
              }
              yield put(loadRecordSuccess(record, recordsMetaData));
              if(entityUrl === 'clients' && clio_record && sync) {
                yield put(syncRecordsActions(record));
              }
            } else {
              yield put(loadRecordError());
            }
          }

          if(record) {
            if (user_info?.clioDetails?.id && user_info?.clioDetails?.access_token && clio_record) {
              let clioRecord = false;
              let submitRecord;
              if (entityUrl === 'clients') {
                submitRecord = Object.assign({}, { integration_case_id: record?.integration_case_id, integration_client_id: record?.integration_client_id, case_id: record?.case_id, client_id: record?.id });
                clioRecord = record?.is_client_updated;
              } else if(entityUrl === 'cases') {
                submitRecord = Object.assign({}, { integration_case_id: [record?.integration_case_id], integration_client_id: record?.integration_client_id, case_id: [record?.id], client_id: record?.client_id })
                let integration_case_data = record?.integration_case_data;
                clioRecord = integration_case_data ? (integration_case_data?.primary_address && !record?.integration_case_data?.primary_email_address) || (integration_case_data?.primary_phone_number && !integration_case_data?.phone_numbers) : false;
              }
  
              if (clioRecord) {
                const syncRecord = yield call(clioSyncRecord, submitRecord);
                if (syncRecord) {
                  const result = yield call(loadRecord, id, queryParams);
                  if (result) {
                    yield call(saveRecordIntoStore, result, recordsMetaData, true)
                  }
                }
              } else {
                yield call(saveRecordIntoStore, record, recordsMetaData)
              }
            } else {
              yield call(saveRecordIntoStore, record, recordsMetaData)
            }
          }
 
        } catch (error) {
          yield put(loadRecordError(error));
        }
      }
    }
  }

  function* createRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { create } = yield race({
        create: take(CREATE_RECORD),
      });
      const { record, form } = create || {};
      if (create) {
        yield put(startSubmit(form));

        const redirectURL = entityUrl === 'rest/plans' ? process.env.PUBLIC_PATH || `/globalSettings/0/${entityUrl}` : clientCategoriesPage.includes(entityUrl) ? process.env.PUBLIC_PATH || `/clientCategories/${entityUrl}` : entityUrl === 'rest/customer-objections' || entityUrl === 'federal/customer-objections' || entityUrl === 'rest/admin-objections' || entityUrl === 'federal/admin-objections' ? process.env.PUBLIC_PATH || `/objectionsCategories/${entityUrl}` : userCategoriesPage.includes(entityUrl) ? process.env.PUBLIC_PATH || `/userCategories/${entityUrl}` : casesCategories.includes(entityUrl) ?  process.env.PUBLIC_PATH || `/casesCategories/${entityUrl}` : entityUrl === 'rest/fee-waiver' ? process.env.PUBLIC_PATH || `/globalSettings/3/${entityUrl}` : entityUrl === 'rest/discount-code' ? process.env.PUBLIC_PATH || `/globalSettings/4/${entityUrl}` : process.env.PUBLIC_PATH || `/${entityUrl}`;
        const sessionUser = yield select(selectUser());

        try {
          yield call(validation, record, Object.assign({}, { user: sessionUser }));
          const result = yield call(createRecord, record);

          if (result) {
            yield put(createRecordSuccess(result));
            yield put(stopSubmit(form));

            if (entityUrl === 'rest/users') {
              if (result.user_password_exist) {
                yield put(updateExistingPasswordData(Object.assign({ user_password_exist: "true", type: "create", practiceId : record.practice_id })));
              }
              else {
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              }
            }
            else if (entityUrl === 'rest/practice/users') {          
              if (result.user_password_exist) {
                yield put(updateExistingPasswordData(Object.assign({ user_password_exist: "true", type: "create", practiceId : record.practice_id })));
              }
            }

            if (entityUrl === 'clients' && record.isRedirectURL) {
              yield put(push({ pathname: record.isRedirectURL || redirectURL, state: Object.assign({}, history.location.state, { formRecord: result, metaData: Object.assign({}, { clientsOptions: [Object.assign({}, { value: result.id, label: result.name })] }) }) }));
            } else if (entityUrl === 'cases') {
              const isPropoundPractice = isPropound(sessionUser, result);
              const pathname = isPropoundPractice && `${redirectURL}/${result.id}/forms` || `${redirectURL}/${result.id}/form`;
              yield put(push({ pathname: pathname, state: Object.assign({}, history.location.state, { id: result.id, caseRecord: result }) }));
            } else {
              if(entityUrl === 'rest/esquiretek/users' || entityUrl === 'rest/practice/users') {
                const queryParams = entityUrl === 'rest/esquiretek/users' ? 'admin' : entityUrl === 'rest/practice/users' ? 'practice' : false;
                yield put(loadRecordsAction(true, queryParams));
              }
              yield put(push({ pathname: redirectURL, state: history.location.state }));
            }

          } else {
            yield put(createRecordError(DEFAULT_CREATE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CREATE_RECORD_ERROR }));
          }

        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_RECORD_ERROR;
          yield put(createRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }

  function* editRecordSaga() {
    yield takeLatest(UPDATE_RECORD, function* updater({ record, form, posLoader, integrationSecret }) {
      if (record) {
        yield put(startSubmit(form));
        const path = history && history.location.pathname;
        const redirectURL = (form === `forms_${record.id}` || form === `inlineRecord_${record.id}`) ? process.env.PUBLIC_PATH || `/${entityUrl}/${record.id}/form` : form === `editRecordForm_${record.id}` ? path : entityUrl === 'rest/plans' ? process.env.PUBLIC_PATH || `/globalSettings/0/${entityUrl}` : clientCategoriesPage.includes(entityUrl) ? process.env.PUBLIC_PATH || `/clientCategories/${entityUrl}` : entityUrl === 'rest/customer-objections' || entityUrl === 'federal/customer-objections' || entityUrl === 'rest/admin-objections' || entityUrl === 'federal/admin-objections' ? process.env.PUBLIC_PATH || `/objectionsCategories/${entityUrl}` : userCategoriesPage.includes(entityUrl) ? process.env.PUBLIC_PATH || `/userCategories/${entityUrl}` : form === `opposingCounselForms_${record.id}` ? path : entityUrl === 'rest/fee-waiver' ? process.env.PUBLIC_PATH || `/globalSettings/3/${entityUrl}` : entityUrl === 'rest/discount-code' ? process.env.PUBLIC_PATH || `/globalSettings/4/${entityUrl}` : process.env.PUBLIC_PATH || `/${entityUrl}`;
        const disableRedirection = ['opposing_counsel', 'propounder_opposing_counsel'];
        const sessionUser = yield call(verifySessionApi);
        const disableSuccessMessage = ['opposing_counsel', `opposingCounselForms_${record.id}`, 'propounder_opposing_counsel'];

        try {
          yield call(validation, record, Object.assign({}, { user: sessionUser }));
          const result = yield call(updateRecord, record);

          if (result) {
            const myCaseCredentials = sessionUser?.mycaseDetails && Object.keys(sessionUser?.mycaseDetails).length > 0 ? true : false;
            const filevineCredentials = sessionUser?.filevineDetails && Object.keys(sessionUser?.filevineDetails).length > 0 ? true : false;
            const filevineFrom = (record?.case_from === 'filevine' || record?.client_from === 'filevine') && !disableRedirection.includes(form);
            const clioSecretCredentials = sessionUser?.clioDetails && Object.keys(sessionUser?.clioDetails).length > 0 ? true : false;

            if (filevineFrom) {
              const practiceId = sessionUser && sessionUser.practice_id || false;
              const filevineDetails = sessionUser && sessionUser.filevineDetails || false;
              const apiKey = filevineDetails && filevineDetails.filevine_key || record?.filevine_key || false;
              const apiSecret = filevineDetails && filevineDetails.filevine_secret || record?.filevine_secret || false;
              const apiUrl = filevineDetails && filevineDetails.filevine_baseurl || record?.filevine_api || false;

              let integrationResult;
              if (integrationSecret) {
                integrationResult = integrationSecret;
              } else if (apiKey && apiSecret) {
                setFilevineBaseUrl(apiUrl);
                // Decryption
                const { decryptApiKey, decryptApiSecret } = decryptApiKeyAndSecret(apiKey, apiSecret, sessionUser);
                // TimeStamp
                const currentTimestamp = new Date().toISOString();
                // Ordering is important!
                const data = [decryptApiKey, currentTimestamp, decryptApiSecret].join('/');
                // Hash
                let currentHash = CryptoJS.MD5(data).toString('');
                integrationResult = yield call(filevineApiSession, Object.assign({}, { mode: "key", apiKey: `${decryptApiKey}`, apiHash: `${currentHash}`, apiTimestamp: `${currentTimestamp}`, filevine_baseurl : apiUrl}));

                store2.set('filevine', integrationResult);
                setFilevineAuthToken(integrationResult);

                const encodedRefreshToken = base64.encode(integrationResult.refreshToken);
                const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

                yield call(storeFilevineSecret, Object.assign({}, { filevine_key: apiKey, filevine_secret: apiSecret, filevine_user_id: integrationResult.userId, filevine_org_id: integrationResult.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: currentHash, filevine_timeStamp: currentTimestamp, filevine_baseurl : apiUrl }));
              }

              if (integrationResult) {
                const encodedAccessToken = base64.encode(integrationResult.accessToken);
                const encryptedAccessToken = CryptoJS.AES.encrypt(encodedAccessToken, practiceId).toString();
                const encodedRefreshToken = base64.encode(integrationResult.refreshToken);
                const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

                const submitRecords = Object.assign({}, record, { access_token: encryptedAccessToken, refresh_token: encryptedRefreshToken, filevine_org_id: integrationResult.orgId, filevine_user_id: integrationResult.userId, filevine_baseurl : apiUrl });

                if (record?.case_from === 'filevine') {
                  yield call(UpdateCasesToFilevine, submitRecords);
                }
                if (record?.client_from === 'filevine') {
                  yield call(updateClientToFilevine, submitRecords);
                }
              }
            }

            if (myCaseCredentials) {
              if (record?.case_from === 'mycase') {
                yield call(myCaseUpdateData, record);
              }
              if (record?.client_from === 'mycase') {
                yield call(myClientUpdateData, record);
              }
            }

            if(clioSecretCredentials) {
              if(record?.case_from === 'clio') {
                yield call(clioUpdateCaseData, record);
              }
              if(record?.client_from === 'clio') {
                yield call(clioUpdateClientData, record);
              }
            }

            let successMessage = false;
            if (entityUrl === 'cases' && !disableSuccessMessage.includes(form)) {
              successMessage = 'Your case has been updated';
            }
            yield put(updateRecordSuccess(result, successMessage));
            if (integrationSecret && sessionUser) {
              yield put(verifySessionSuccess(sessionUser));
            }
            if (posLoader && disableRedirection.includes(form)) {
              yield call(posLoader, false);
            } else if (!disableRedirection.includes(form)) {
              yield put(push({ pathname: redirectURL, state: history.location.state }));
            }
          } else {
            yield put(updateRecordError(DEFAULT_UPDATE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_RECORD_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_RECORD_ERROR;
          yield put(updateRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    })
  }


  function* deleteRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { del } = yield race({
        del: take(DELETE_RECORD),
      });
      const { id, form, queryParams } = del || {};
      if (del) {
        yield put(startSubmit(form));
        const redirectURL = entityUrl === 'rest/plans' ? process.env.PUBLIC_PATH || `/globalSettings/0/${entityUrl}` : clientCategoriesPage.includes(entityUrl) ? process.env.PUBLIC_PATH || `/clientCategories/${entityUrl}` : entityUrl === 'rest/customer-objections' || entityUrl === 'federal/customer-objections' || entityUrl === 'rest/admin-objections' || entityUrl === 'federal/admin-objections' ? process.env.PUBLIC_PATH || `/objectionsCategories/${entityUrl}` : userCategoriesPage.includes(entityUrl) ? process.env.PUBLIC_PATH || `/userCategories/${entityUrl}` : entityUrl === 'rest/fee-waiver' ? process.env.PUBLIC_PATH || `/globalSettings/3/${entityUrl}` : entityUrl === 'rest/discount-code' ? process.env.PUBLIC_PATH || `/globalSettings/4/${entityUrl}` : (queryParams && (entityUrl === 'rest/practice-template' || entityUrl === 'rest/practice-propound-template')) ? `/customCategories/${entityUrl}` ||  `/${entityUrl}` : process.env.PUBLIC_PATH || `/${entityUrl}`;

        try {
          yield call(deleteRecord, id);
          yield put(deleteRecordSuccess(id));
          yield put(stopSubmit(form));
          if(entityUrl === 'rest/users') {
            const secret = store2.get('secret');
            yield put(verifySessionAction(secret));
          }
          yield put(push({ pathname: redirectURL, state: history.location.state }));
          if(entityUrl === 'rest/practice-template') {
            yield put(loadRecordsAction(true, queryParams));
          } else if(entityUrl === 'rest/esquiretek/users' || entityUrl === 'rest/practice/users') {
            const queryParams = entityUrl === 'rest/esquiretek/users' ? 'admin' : entityUrl === 'rest/practice/users' ? 'practice' : false;
            yield put(loadRecordsAction(true, queryParams));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_DELETE_RECORD_ERROR;
          yield put(deleteRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }


  function* loadRecordsMetaDataSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const loadMetaData = yield race({
        metaData: take(LOAD_RECORDS_META_DATA),
      });

      try {

        let recordsMetaData = {};
        const { location } = history;
        const path = location && location.pathname && location.pathname && location.pathname;
        const queryParams = path.includes('respond') ? 'responding' : path.includes('propound') ? 'propounding' : 'responding';
        if (entityUrl === 'cases') {
          const sessionUser = yield select(selectUser());
          const clientsRemotes = yield call(appRemotes, 'clients');
          const usersRemotes = yield call(appRemotes, 'rest/users');
          const statesRemote = yield call(appRemotes, 'rest/state');
          const templateRemote = yield call(appRemotes, 'rest/frogs-template');
          const customTemplateRemotes = yield call(appRemotes, 'rest/custom-template');
          const plansRemotes = yield call(appRemotes, 'rest/plans');
          const clients = yield call(clientsRemotes.loadRecords);
          const users = yield call(usersRemotes.loadRecords);
          const states = yield call(statesRemote.loadRecords);
          const templateName = yield call(templateRemote.loadRecords);
          const modifiedTemplateOptions = yield call(customTemplateRemotes.loadRecords, queryParams);
          const plans = yield call(plansRemotes.loadRecords);
          recordsMetaData = { clients, users: sessionUser && ([sessionUser]).concat(users) || users, states, templateName, modifiedTemplateOptions, plans, currentUserInfo: sessionUser };
        } else if (entityUrl === 'rest/users' || entityUrl === 'rest/practice/users') {
          const practicesRemotes = yield call(appRemotes, 'rest/practices');
          const practices = yield call(practicesRemotes.loadRecords);
          recordsMetaData = { practices };
        } else if (entityUrl === 'rest/medical-history-summery') {
          const medicalRemotes = yield call(appRemotes, 'rest/medical-experts');
          const users = yield call(medicalRemotes.loadRecords);
          recordsMetaData = { users };
        } else if (entityUrl === 'rest/plans') {
          const sessionUser = yield select(selectUser());
          const usersRemotes = yield call(appRemotes, 'rest/users');
          const users = yield call(usersRemotes.loadRecords);
          recordsMetaData = { users: sessionUser && ([sessionUser]).concat(users) || users };
        } else if (entityUrl === 'rest/other-parties') {
          const cases = yield call(getAllOtherPartiesCasesDetails);
          recordsMetaData = { cases };
        } else if (entityUrl === 'rest/customer-objections' || entityUrl === 'rest/admin-objections' || entityUrl === 'rest/propound' || entityUrl === 'rest/practice-template' || entityUrl === 'rest/practice-propound-template' || entityUrl === 'rest/document-extraction-support' || entityUrl === 'federal/customer-objections') {
          const statesRemote = yield call(appRemotes, 'rest/state');
          const states = yield call(statesRemote.loadRecords);
          let metadata;
          if (entityUrl === 'rest/customer-objections' || entityUrl === 'federal/customer-objections') {
            const sessionUser = yield select(selectUser());
            const usersRemotes = yield call(appRemotes, 'rest/users');
            const users = yield call(usersRemotes.loadRecords);
            metadata = { states, users: sessionUser && ([sessionUser]).concat(users) || users };
          } else {
            metadata = { states };
          }
          recordsMetaData = metadata;
        } else if(entityUrl === 'clients') {
          const clientsRemotes = yield call(appRemotes, 'clients');
          const clients = yield call(clientsRemotes.loadRecords);
          recordsMetaData = { clients };
        } else if (entityUrl === 'rest/document-extraction-progress') {
          const usersRemotes = yield call(appRemotes, 'rest/users');
          const statesRemote = yield call(appRemotes, 'rest/state');
          const plansRemotes = yield call(appRemotes, 'rest/plans');
          const sessionUser = yield select(selectUser());
          const users = yield call(usersRemotes.loadRecords);
          const states = yield call(statesRemote.loadRecords);
          const plans = yield call(plansRemotes.loadRecords);
          recordsMetaData = { users: sessionUser && ([sessionUser]).concat(users) || users, states, plans, currentUserInfo: sessionUser };
        }

        yield put(loadRecordsMetaDataSuccess(recordsMetaData));
      } catch (error) {
        yield put(loadRecordsMetaDataError(error));
      }
    }
  }

  function* excelExportSaga() {
    yield takeLatest(EXCEL_EXPORT, function* updater({ record, form, queryParams, columns, fileName, excelModal, name, usersOptions }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          let headers = yield select(selectHeaders());
          const filter = yield select(selectFilterData());
          const createdDate = yield select(selectCreatedDateFilter());
          const activeFilter = yield select(selectActiveFilter());

          const filterDate = createdDate && Object.keys(createdDate).length > 0 && entityUrl === 'practices' && (createdDate.practices && Object.keys(createdDate.practices).length > 0 ? createdDate.practices : {}) || entityUrl === 'rest/practice/users' && (createdDate.practiceUsers && Object.keys(createdDate.practiceUsers).length > 0 ? createdDate.practiceUsers : {}) || entityUrl === 'rest/subscriptions/admin/history' && (createdDate.subscriptions && Object.keys(createdDate.subscriptions).length > 0 ? createdDate.subscriptions : {}) || {};
          const filterActive = activeFilter && Object.keys(activeFilter).length > 0 && entityUrl === 'practices' && (activeFilter.practices && Object.keys(activeFilter.practices).length > 0 ? activeFilter.practices : {}) || entityUrl === 'rest/practice/users' && (activeFilter.practiceUsers && Object.keys(activeFilter.practiceUsers).length > 0 ? activeFilter.practiceUsers : {}) || entityUrl === 'rest/subscriptions/admin/history' && (activeFilter.subscriptions && Object.keys(activeFilter.subscriptions).length > 0 ? activeFilter.subscriptions : {}) || {};

          let result;
          if (filter && filter != 'total') {
            const paramsObj = filter && convertParamsToObject(filter);
            const type = paramsObj && Object.keys(paramsObj).length > 0 && paramsObj.filter_type;
            const filterRecord = (record && type) && `filter_type=${type}&from_date=${record.from_date}&to_date=${record.to_date}` || (record) && `from_date=${record.from_date}&to_date=${record.to_date}` || type && `filter_type=${type}`;

            result = yield call(excelExportNoLimits, headers, queryParams, filterRecord);
            result = result && result.data && result.data.length > 0 && result.data;
          } else if (!lodash.isEmpty(filterDate) || !lodash.isEmpty(filterActive)) {
            const utcConvertedDate = filterDate && Object.keys(filterDate).length > 0 && convertUTC(filterDate) || false;
            const active = filterActive && Object.keys(filterActive).length > 0 && filterActive && filterActive.active ? 'active' : false;
            const selectedDateRecord = record && record || utcConvertedDate;
            const filterRecord = (selectedDateRecord && active) && `filter_type=${active}&from_date=${selectedDateRecord.from_date}&to_date=${selectedDateRecord.to_date}` || (selectedDateRecord) && `from_date=${selectedDateRecord.from_date}&to_date=${selectedDateRecord.to_date}` || active && `filter_type=${active}`;

            result = yield call(excelExportNoLimits, headers, queryParams, filterRecord);
            result = result && result.data && result.data.length > 0 && result.data;
          } else if (name === 'practices' && record?.inactive_accounts && record?.duration) {
            result = yield call(excelExportNoLimits, headers, queryParams, false);
            result = result && result.data && result.data.length > 0 && result.data;
          } else {
            result = yield call(excelExport, record, queryParams, headers);
          }
          if (result) {
            if (result && result.length > 0) {
              let submitRecords = excelModifiedData(result, name, usersOptions);
              yield call(createDownLoadData, submitRecords, columns, fileName);
              yield put(stopSubmit(form));
              yield call(excelModal, false);
            } else {
              yield put(excelExportError('No record found'));
              yield put(stopSubmit(form, { _error: 'No record found' }));
            }
          } else {
            yield put(excelExportError(DEFAULT_EXCEL_EXPORT_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_EXCEL_EXPORT_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_EXCEL_EXPORT_ERROR;
          yield put(excelExportError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    })
  }


  function* createClientWithCaseSaga() {
    while (true) {
      // eslint-disable-line no-constant-condition
      const { record, form, createForm, setClientForm, loadRecords, setProgress  } = yield take(CREATE_CLIENT_WITH_CASE);
      yield put(startSubmit(form));
      try {
        if (record) {
          let result = yield call(createClientWithCases, record);
          if(result) {
            const user_info = yield select(selectUser());
            let isPropoundPage = user_info && result && isPropound(user_info, result);
            yield put(createClientWithCaseSuccess('Client & Case created successfully'));
            yield put(loadRecords(true));
            yield delay(500);
            yield put(push({ pathname: isPropoundPage ? `/casesCategories/cases/${result.id}/forms` : `/casesCategories/cases/${result.id}/form`, state: Object.assign({}, { ...history.location.state }, { respondCase: false })}));
          } else {
            yield put(createClientError('Failed to create client & client'));
            yield put(stopSubmit(form));
          }
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to create client & client';
        yield put(createClientWithCaseError(Err));
        yield put(stopSubmit(form, { _error: Err }));
      } finally {
        if(createForm)
          yield call(createForm, false);
        if(setClientForm)
          yield call(setClientForm, false);
        if(setProgress)
          yield call(setProgress,false);
        yield put(loadRespondDetailSuccess({}));
      }
    }
  }

  function* createResponderFormSaga() {
    while (true) {
      // eslint-disable-line no-constant-condition
      const { record, loadRecords } = yield take(CREATE_RESPONDER_FORM);
      try {
        if (record) {
          let result = yield call(createClientWithCases, record);
          if(result) {
            if(!result.status) {
              const user_info = yield select(selectUser());
              let isPropoundPage = user_info && result && isPropound(user_info, result);
              yield put(createResponderFormSuccess('Form Saved Successfully'));
              yield delay(500);
              yield put(push({ pathname: isPropoundPage ? `/casesCategories/cases/${result.id}/forms` : `/casesCategories/cases/${result.id}/form`, state: Object.assign({}, { ...history.location.state }, { respondCase: false })}));
            } else {
              yield put(loadRecords(true));
            }
          } else {
            yield put(createResponderFormError('Failed to create form'))
          }
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to create form';
        yield put(createResponderFormError(Err));
      } finally {
        yield put(loadRespondDetailSuccess({}));
      }
    }
  }

  function* loadActiveFilterRecordSaga() {
    yield takeLatest(LOAD_ACTIVE_FILTER_RECORD, function* updater({ createdDate, active, queryParams }) {
      try {
        const headers = yield select(selectHeaders());
        const utcConvertedDate = createdDate && Object.keys(createdDate).length > 0 && convertUTC(createdDate) || false;

        let filter = (utcConvertedDate && active) ? `filter_type=${active}&from_date=${utcConvertedDate.from_date}&to_date=${utcConvertedDate.to_date}` : (utcConvertedDate) ? `from_date=${utcConvertedDate.from_date}&to_date=${utcConvertedDate.to_date}` : active ? `filter_type=${active}` : false;

        let result = yield call(loadDashboardRecords, headers, queryParams, filter);
        
        if (result) {
          yield put(loadRecordsSuccess(result.data, result.headers && result.headers.totalpagecount || false));
        } else {
          yield put(loadRecordsError());
        }
      } catch (error) {
        yield put(loadRecordsError(error));
      }
    })
  }

  function* deleteSupportRequestSaga() {
    yield takeLatest(DELETE_SUPPORT_REQUEST, function* updater({ record, pageType }) {
      try {
        if (record) {
          const redirectURL = pageType == 'propound' ? '/supportCategories/rest/propound-support' : pageType == 'extraction' ? '/supportCategories/rest/document-extraction-support' : '/supportCategories/rest/help-request';
          let result = yield call(deleteSupportRequest, record, pageType);
          if (result) {
            yield put(deleteSupportRequestSuccess('Request deleted successfully'));
            yield delay(2000);
            yield put(push({ pathname: redirectURL, state: history.location.state }));
          } else {
            yield put(deleteSupportRequestError('Failed to delete the request'));
          }
        }
      } catch (error) {
        yield put(deleteSupportRequestError('Failed to delete the request'));
      }
    })
  }

  function* updateLitifyRecordSaga() {
    yield takeLatest(UPDATE_LITIFY_RECORD, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));

        const redirectURL = clientCategoriesPage.includes(entityUrl) && process.env.PUBLIC_PATH || `/clientCategories/${entityUrl}`;
        const sessionUser = yield call(verifySessionApi);
        try {
          let litifyResult;
          yield call(validation, record, Object.assign({}, { user: sessionUser }));

          if (record?.case_from == 'litify') {
            litifyResult = yield call(updateCasesToLitify, record);
          }
          if (record?.client_from == 'litify') {
            litifyResult = yield call(updateClientToLitify, record);
          }

          if (litifyResult) {
            const result = yield call(updateRecord, record);
            yield delay(1000);
            let successMessage = false;
            if (result) {
              yield put(updateLitifyRecordSuccess(result, successMessage));
              yield put(push({ pathname: redirectURL, state: history.location.state }));
            } else {
              yield put(updateLitifyRecordError(DEFAULT_UPDATE_RECORD_ERROR));
              yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_RECORD_ERROR }));
            }
          }
        } catch (error) {
          const errorMsg = error.response && error.response.data && error.response.data.error;
          if (errorMsg && typeof errorMsg == 'object') {
            yield put(stopSubmit(form));
            yield put(litifyInfoDialogAction(errorMsg));
          } else {
            const Err = errorMsg && typeof errorMsg == 'string' ? errorMsg : DEFAULT_UPDATE_RECORD_ERROR;
            yield put(updateLitifyRecordError(Err));
            yield put(stopSubmit(form, { _error: Err }));
          }
        }
      }
    })
  }

  function* createUserSaga() {
    yield takeLatest(CREATE_USER, function* updater({ record, form, setExistingPasswordModal }) {
      try {
        if (record) {
          yield put(startSubmit(form));
          let practice_id = record?.practice_id || false;
          if (record.userCreatedIn == "customer_admin") {
            const user = yield select(selectUser());
            practice_id = user.practice_id;
          }
          const result = yield call(checkUserExist, Object.assign({ email: record.email, practice_id }));
          if (result) {
            const isExistingEmail = result?.user_password_exist;
            if (isExistingEmail) {
              setExistingPasswordModal(Object.assign({}, record, { "user_password_exist": isExistingEmail }), form);
              yield put(stopSubmit(form));
            } else {
              yield put(createRecordAction(Object.assign({}, record, { "user_password_exist": isExistingEmail }), form));
            }
          }
        }
      } catch (error) {
        yield put(createRecordError(DEFAULT_CREATE_RECORD_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_CREATE_RECORD_ERROR }));
      }
    })
  }

  function* recordsSaga() {
    yield all([
      loadRecordSaga(),
      loadRecordsSaga(),
      createRecordSaga(),
      editRecordSaga(),
      deleteRecordSaga(),
      loadRecordsMetaDataSaga(),
      excelExportSaga(),
      createClientWithCaseSaga(),
      createResponderFormSaga(),
      loadActiveFilterRecordSaga(),
      deleteSupportRequestSaga(),
      updateLitifyRecordSaga(),
      createUserSaga()
    ])
  }

  return function* rootSaga() {
    yield all([
      recordsSaga(),
      additionalSaga()
    ])
  }
}

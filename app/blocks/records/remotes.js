/*
 *
 *  remotes
 *
 */

import api, {filevineApi} from 'utils/api.js';

/**
 * @param {string} name 
 * @param {object} remotesBlock 
 */
export default function (name, remotesBlock = {}) {

  function urlConstantName() {
    return {
      'federal/customer-objections': 'rest/customer-objections',
      'federal/admin-objections': 'rest/admin-objections',
      'rest/esquiretek/users': 'rest/users',
      'rest/practice/users': 'rest/users',
      'rest/practice-template': 'rest/practice-template',
      'rest/practice-propound-template': 'rest/practice-template',
    }
  }

  function recordsWithHeader(queryParams) {
    return {
      'rest/admin/orders': 'rest/admin/orders/search',
      'orders': 'rest/orders/practice-search',
      'practices': 'rest/practices',
      'cases': 'rest/cases',
      'rest/users': `rest/users?type=${queryParams}`,
      'rest/practice-template': queryParams ? `rest/practice-template?type=${queryParams}` : `rest/practice-template?type=responding`,
    }
  }

  function adminUrlObject(filterType) {
    return {
      'rest/subscriptions/admin/history' : filterType && (filterType.includes('activation') ? `rest/subscriptions/history/dashboard/admin?${filterType}` : `rest/subscriptions/active/dashboard/admin?${filterType}`) || `rest/subscriptions/active/dashboard/admin`,
      'practices': filterType ? `rest/dashboard/practices?${filterType}` : `rest/dashboard/practices?`,
      'rest/users': filterType ? `rest/users/dashboard/admin?${filterType}` : `rest/users/dashboard/admin`,
    }
  }

  let constantName = urlConstantName()[name] || name;

  function loadRecords(queryParams) {
    return api.get(`/${constantName === 'rest/plans' ? `rest/plans?page=adminSettings` : constantName === 'rest/custom-template' ? `rest/practice-template?type=${queryParams}&status=Applied` : `${constantName}`}`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} headers 
   * @param {string} queryParams 
   * @returns 
   */
  function loadRecordsWithHeader(headers, queryParams) {
    const url = recordsWithHeader(queryParams)[constantName] || constantName;
    return api.get(`/${url}`, { headers }).then((response) => response).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {integer} id 
   * @param {string} queryParams
   * @returns 
   */
  function loadRecord(id, queryParams) {
    const url = (constantName === 'cases') ? `rest/cases/case-details/${id}` : (queryParams && constantName === 'rest/practice-template') ? `/rest/practice-template/${id}?type=${queryParams}` : (constantName === 'rest/practice-template') ? `/rest/practice-template/${id}?type=responding` : `/${constantName}/${id}`;
    return api.get(url).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function createRecord(record) {
    return api.post(`/${constantName}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function updateRecord(record) {
    return api.put(`/${constantName}/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {integer} id 
   * @returns 
   */
  function deleteRecord(id) {
    return api.delete(`/${constantName}/${id}`).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {integer} record 
   * @param {string} role 
   * @returns 
   */
  function excelExport(record, role, headers) {
    const url = constantName === 'rest/admin/orders' ? 'rest/admin/orders/search' : constantName === 'practices' ? 'rest/practices' : constantName;
    const apiUrl = role && constantName === 'rest/users' ? `rest/users?type=${role}&from_date=${record.from_date}&to_date=${record.to_date}&download=excel` : `${url}?from_date=${record.from_date}&to_date=${record.to_date}&download=excel`;
    return api.get(`${apiUrl}`, { headers }, record).then((response) => response.data).catch((error) => Promise.reject(error));
  }


  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function createClientWithCases(record) {
    return api.post(`/rest/propound-responder/case`, record).then((response) => response.data).catch((error) => Promise.reject(error))
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function notificationStatus(record) {
    return api.put(`/rest/users/update-notification/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} headers 
   * @param {string} role 
   * @param {string} filterType
   * @returns 
   */
  function loadDashboardRecords(headers, role, filterType) {
    let url = adminUrlObject(filterType)[constantName] || constantName;
    return api.get(`/${url}`, { headers }).then((response) => response).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} headers 
   * @param {string} role 
   * @param {string} filterType
   * @returns 
   */
  function excelExportNoLimits(headers, role, filterType) {
    let url = adminUrlObject(filterType)[constantName] || constantName;
    url = `${url}&download=excel`;
    return api.get(`/${url}`, { headers }).then((response) => response).catch((error) => Promise.reject(error));
  }

  /**
   * @param {object} record 
   */
  function myCaseUpdateData(record) {
    return api.put('/rest/integrations/update-case-to-mycase', record).then(res => res.data).catch(err => Promise.reject(err))
  }
  
  /**
   * @param {object} record 
   */
  function myClientUpdateData(record) {
    return api.put('/rest/integrations/update-client-to-mycase', record).then(res => res.data).catch(err => Promise.reject(err))
  }

  /**
   * @param {object} record
   */
  function UpdateCasesToFilevine(record) {
    return api.put('/rest/integrations/filevine/update-case-to-filevine', record).then(res => res.data).catch(err => Promise.reject(err))
  }

  /**
   * @param {object} record 
   */
  function updateClientToFilevine(record) {
    return api.put('/rest/integrations/filevine/update-client-to-filevine', record).then(res => res.data).catch(err => Promise.reject(err))
  }

  /**
   * @param {object} secret 
   */
  function filevineApiSession(secret) {
    return filevineApi.post(`/session`, secret).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function storeFilevineSecret(record) {
    return api.post('/rest/integrations/filevine-key-details', record).then(response => response.data).catch(error => Promise.reject(error));
  }

  /**
   * 
   * @param {object} record 
   * @returns
   */
  function updateCasesToLitify(record) {
    return api.put(`/rest/integrations/update-case-to-litify`, record).then(res => res.data).catch(err => Promise.reject(err));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function updateClientToLitify(record) {
    return api.put(`/rest/integrations/update-client-to-litify`, record).then(res => res.data).catch(err => Promise.reject(err));
  }

  /**
   * 
   * @param {object} record 
   * @param {string} pageType 
   * @returns 
   */
  function deleteSupportRequest(record, pageType) {
    return api.delete(`/rest/admin-help-request/${record.id}/${pageType}/${record?.reason}`).then(res => res.data).catch(err => Promise.reject(err));
  }

  function checkUserExist(record) {
    return api.post('/rest/user/user-exists', record).then(response => response.data).catch(error => Promise.reject(error));
  }

  /**
   * @param {object} record
   */
  function clioUpdateCaseData(record) {
    return api.put('/rest/integrations/update-case-to-clio', record).then(res => res.data).catch(err => Promise.reject(err));
  }

  /**
   * @param {object} record
   */
  function clioUpdateClientData(record) {
    return api.put('/rest/integrations/update-client-to-clio', record).then(res => res.data).catch((err) => Promise.reject(err));
  }
  
  /**
   * @param {object} record
   */
  function customerObjections(record) {
    return api.get(`/rest/get-all-customer-objection-by-state?state=${record.state}`).then(res => res.data).catch((err) => Promise.reject(err));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function clioSyncRecord(record) {
    return api.put('/rest/integrations/update-getone-clio-data', record).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function getAllOtherPartiesCasesDetails() {
    return api.get('/rest/cases/other-party-get-all').then((response) => response.data).catch((error) => Promise.reject(error));
  }
  
  return {
    loadRecords,
    loadRecordsWithHeader,
    loadRecord,
    createRecord,
    updateRecord,
    deleteRecord,
    excelExport,
    createClientWithCases,
    notificationStatus,
    loadDashboardRecords,
    excelExportNoLimits,
    myCaseUpdateData,
    myClientUpdateData,
    UpdateCasesToFilevine,
    updateClientToFilevine,
    filevineApiSession,
    storeFilevineSecret,
    updateCasesToLitify,
    updateClientToLitify,
    deleteSupportRequest,
    checkUserExist,
    clioUpdateClientData,
    clioUpdateCaseData,
    customerObjections,
    clioSyncRecord,
    getAllOtherPartiesCasesDetails,
    ...remotesBlock
  }


}
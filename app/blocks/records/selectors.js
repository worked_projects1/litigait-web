/*
 *
 *  selectors
 *
 */

import { createSelector } from 'reselect';
import { List, Map } from 'immutable';
import mapRecords from './utils';
import mapRecordsMetaData from './metaData';

/**
 * @param {string} name 
 */
export default function selectors(name) {
  const selectDomain = () => (state) => state[name] || false;

  const selectLoading = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.loading || false,
  );

  const selectProgress = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.progress || false,
  );

  const selectRecordsMetaData = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.recordsMetaData && mapRecordsMetaData(domain.recordsMetaData, name, domain) || {},
  );

  const selectRecords = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.records && mapRecords(domain.records, name) || [],
  );

  const selectRecord = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.record && mapRecords([domain.record], name)[0] || {}
  );

  const selectError = () => createSelector(
    selectDomain(),
    (domain) => domain && (domain.error || domain.pageError || false),
  );

  const selectSuccess = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.success || false,
  );

  const selectUpdateError = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.updateError || false,
  );

  const selectUpdateTimestamp = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.lastUpdate || false,
  );

  const selectKey = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.key || 0,
  );

  const selectSettings = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.settings || {},
  );

  const selectTotalPageCount = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.totalPageCount || false,
  );

  const selectHeaders = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.headers || {},
  );

  const selectFilter = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filter || {}
  );

  const selectPageLoader = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.pageLoader || false
  );

  const selectContentNotFound = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.contentNotFount || false,
  );

  const selectQuestionsNotFound = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.questionsNotFound || false,
);

  const selectFilevineRecords = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filevineRecords && mapRecords(domain.filevineRecords, name) || [],
  );

  const selectButtonLoader = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.buttonLoader || false
  );

  const selectFilevineHeaders = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filevineHeaders || {},
  );
  
  const selectBtnLoader = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.btnLoader || false,
  );
  
  const selectPageCustomLoader = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.pageCustomLoader || false,
  );

  const selectFilevineSessionForm = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filevineSessionForm || false,
  );

  const selectDefendantPracticeDetails = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.defendantPracticeDetails || false,
  );

  const selectLitifyInfoDialog = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.litifyInfoDialog || false,
  );

  const selectSubscriptionSuccess = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.subscriptionSuccess || false,
  );

  const selectManageArchiveCase = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.manageArchiveCase || false,
  );

  const selectExistedPasswordData = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.passwordData || false,
  );

  const selectDocumentStatus = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.documentStatus || false,
  );

  const selectEServePopup = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.eServePopup || false,
  );

  const selectFrogsQuestions = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.frogsQuestions || false,
  );

  const selectDynamicFrogsQuestions = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.frogsDynamicQuestions || false,
  );

  const selectDiscoveryHistory = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.discoveryHistory || false,
  );

  return {
    selectDomain,
    selectProgress,
    selectLoading,
    selectRecords,
    selectRecordsMetaData,
    selectRecord,
    selectError,
    selectSuccess,
    selectUpdateError,
    selectUpdateTimestamp,
    selectKey,
    selectSettings,
    selectTotalPageCount,
    selectHeaders,
    selectFilter,
    selectPageLoader,
    selectContentNotFound,
    selectFilevineRecords,
    selectButtonLoader,
    selectFilevineHeaders,
    selectBtnLoader,
    selectPageCustomLoader,
    selectFilevineSessionForm,
    selectDefendantPracticeDetails,
    selectLitifyInfoDialog,
    selectSubscriptionSuccess,
    selectManageArchiveCase,
    selectExistedPasswordData,
    selectDocumentStatus,
    selectEServePopup,
    selectQuestionsNotFound,
    selectFrogsQuestions,
    selectDynamicFrogsQuestions,
    selectDiscoveryHistory
  };
}

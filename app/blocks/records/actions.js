/*
 *
 *  actions
 *
 */

export default function actions(constants) {
  const {
    LOAD_RECORD,
    LOAD_RECORD_SUCCESS,
    LOAD_RECORD_ERROR,
    LOAD_RECORDS,
    LOAD_RECORDS_SUCCESS,
    LOAD_RECORDS_ERROR,
    CREATE_RECORD,
    CREATE_RECORD_SUCCESS,
    CREATE_RECORD_ERROR,
    UPDATE_RECORD,
    UPDATE_RECORD_SUCCESS,
    UPDATE_RECORD_ERROR,
    DELETE_RECORD,
    DELETE_RECORD_SUCCESS,
    DELETE_RECORD_ERROR,
    LOAD_RECORDS_VALID_CACHE,
    SET_HEADERS_DATA,
    LOAD_RECORDS_META_DATA,
    LOAD_RECORDS_META_DATA_SUCCESS,
    LOAD_RECORDS_META_DATA_ERROR,
    UPDATE_STATE_FILTER,
    EXCEL_EXPORT,
    EXCEL_EXPORT_ERROR,
    EXCEL_EXPORT_SUCCESS,
    CREATE_CLIENT_WITH_CASE,
    CREATE_CLIENT_WITH_CASE_SUCCESS,
    CREATE_CLIENT_WITH_CASE_ERROR,
    CREATE_RESPONDER_FORM,
    CREATE_RESPONDER_FORM_ERROR,
    CREATE_RESPONDER_FORM_SUCCESS,
    LOAD_ACTIVE_FILTER_RECORD,
    LOAD_ACTIVE_FILTER_RECORD_ERROR,
    LOAD_ACTIVE_FILTER_RECORD_SUCCESS,
    CLEAR_INDIVIDUAL_STORE_RECORD,
    SET_FILEVINE_SESSION_FORM,
    DELETE_SUPPORT_REQUEST,
    DELETE_SUPPORT_REQUEST_SUCCESS,
    DELETE_SUPPORT_REQUEST_ERROR,
    LITIFY_INFO_DIALOG,
    UPDATE_LITIFY_RECORD,
    UPDATE_LITIFY_RECORD_SUCCESS,
    UPDATE_LITIFY_RECORD_ERROR,
    UPDATE_EXISTED_PASSWORD_DATA,
    CLEAR_EXISTED_PASSWORD_DATA,
    CREATE_USER,
    SYNC_RECORDS
  } = constants;


  function loadRecordsCacheHit() {
    return {
      type: LOAD_RECORDS_VALID_CACHE,
    };
  }

  /**
   * @param {integer} id
   * @param {string} queryParams 
   */
  function loadRecord(id, queryParams) {
    return {
      type: LOAD_RECORD,
      id,
      queryParams
    };
  }

  /**
   * @param {object} record 
   * @param {object} recordsMetaData
   */
  function loadRecordSuccess(record, recordsMetaData) {
    return {
      type: LOAD_RECORD_SUCCESS,
      record,
      recordsMetaData
    };
  }

  /**
   * @param {string} error 
   */
  function loadRecordError(error) {
    return {
      type: LOAD_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {boolean} invalidateCache 
   * @param {string} queryParams 
   */
  function loadRecords(invalidateCache, queryParams) {
    return {
      type: LOAD_RECORDS,
      invalidateCache,
      queryParams
    };
  }

  /**
   * @param {array} records 
   * @param {number} totalPageCount 
   */
  function loadRecordsSuccess(records, totalPageCount) {
    return {
      type: LOAD_RECORDS_SUCCESS,
      records,
      totalPageCount
    };
  }

  /**
   * @param {string} error 
   */
  function loadRecordsError(error) {
    return {
      type: LOAD_RECORDS_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   * @param {string} form 
   */
  function createRecord(record, form) {
    return {
      type: CREATE_RECORD,
      record,
      form,
    };
  }

  /**
   * @param {object} record 
   */
  function createRecordSuccess(record) {
    return {
      type: CREATE_RECORD_SUCCESS,
      record,
    };
  }

  /**
   * @param {string} error 
   */
  function createRecordError(error) {
    return {
      type: CREATE_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} posLoader
   * @param {boolean} integrationSecret
   */
  function updateRecord(record, form, posLoader, integrationSecret) {
    return {
      type: UPDATE_RECORD,
      record,
      form,
      posLoader,
      integrationSecret
    };
  }

  /**
   * @param {object} record 
   */
  function updateRecordSuccess(record, success) {
    return {
      type: UPDATE_RECORD_SUCCESS,
      record,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function updateRecordError(error) {
    return {
      type: UPDATE_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {integer} id 
   * @param {string} form 
   * @param {string} queryParams
   */
  function deleteRecord(id, form, queryParams) {
    return {
      type: DELETE_RECORD,
      id,
      form,
      queryParams
    };
  }

  /**
   * @param {integer} id 
   */
  function deleteRecordSuccess(id) {
    return {
      type: DELETE_RECORD_SUCCESS,
      id,
    };
  }

  /**
   * @param {string} error 
   */
  function deleteRecordError(error) {
    return {
      type: DELETE_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   */
   function setHeadersData(record) {
    return {
      type: SET_HEADERS_DATA,
      record
    };
  }


  
  function loadRecordsMetaData(){
    return {
      type: LOAD_RECORDS_META_DATA
    }
  }

  /**
   * 
   * @param {object} recordsMetaData 
   * @returns 
   */
  function loadRecordsMetaDataSuccess(recordsMetaData){
    return {
      type: LOAD_RECORDS_META_DATA_SUCCESS,
      recordsMetaData
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function loadRecordsMetaDataError(error){
    return {
      type: LOAD_RECORDS_META_DATA_ERROR,
      error
    }
  }

  /**
   * @param {object} filter 
   */
  function UpdateStateFilter(filter) {
    return {
        type: UPDATE_STATE_FILTER,
        filter
    };
  }

  function updateExistingPasswordData(passwordData){
    return {
      type: UPDATE_EXISTED_PASSWORD_DATA,
      passwordData,
  };
  }

  function clearExistingPasswordData(){
    return {
      type: CLEAR_EXISTED_PASSWORD_DATA,
  };
  }

  /**
   * 
   * @param {object} record
   * @param {string} form
   * @param {string} queryParams
   * @param {object} columns
   * @param {string} fileName
   * @param {function} excelModal
   * @param {string} name
   * @param {object} usersOptions
   */
  function excelExport(record, form, queryParams, columns, fileName, excelModal, name, usersOptions) {
    return {
      type: EXCEL_EXPORT,
      record,
      form,
      queryParams,
      columns,
      fileName,
      excelModal,
      name,
      usersOptions
    }
  }

  /**
   * 
   * @param {object} error
   */
  function excelExportError(error) {
    return {
      type: EXCEL_EXPORT_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} success
   */
  function excelExportSuccess(success) {
    return {
      type: EXCEL_EXPORT_SUCCESS,
      success
    }
  }


  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} createForm 
   * @param {function} setClientForm 
   * @param {function} loadRecords 
   * @param {function} setProgress 
   * @returns 
   */
   function createClientWithCase(record, form, createForm, setClientForm, loadRecords, setProgress) {
    return {
      type: CREATE_CLIENT_WITH_CASE,
      record,
      form,
      createForm, 
      setClientForm,
      loadRecords,
      setProgress
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function createClientWithCaseError(error) {
    return {
      type: CREATE_CLIENT_WITH_CASE_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function createClientWithCaseSuccess(success) {
    return {
      type: CREATE_CLIENT_WITH_CASE_SUCCESS,
      success
    }
  }


  /**
   * 
   * @param {object} record 
   * @param {function} loadRecords 
   * @returns 
   */
   function createResponderForm(record, loadRecords) {
    return {
      type: CREATE_RESPONDER_FORM,
      record,
      loadRecords
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function createResponderFormError(error) {
    return {
      type: CREATE_RESPONDER_FORM_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function createResponderFormSuccess(success) {
    return {
      type: CREATE_RESPONDER_FORM_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {object} createdDate   
   * @param {object} active
   * @param {string} queryParams
   * @returns 
   */
  function loadActiveFilterRecord(createdDate, active, queryParams) {
    return {
      type: LOAD_ACTIVE_FILTER_RECORD,
      createdDate,
      active,
      queryParams
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function loadActiveFilterRecordError(error) {
    return {
      type: LOAD_ACTIVE_FILTER_RECORD_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function loadActiveFilterRecordSuccess(success) {
    return {
      type: LOAD_ACTIVE_FILTER_RECORD_SUCCESS,
      success
    }
  }

  function clearIndividualStoreRecord(record) {
		return {
			type: CLEAR_INDIVIDUAL_STORE_RECORD,
			record
		}
	}

  /**
   * 
   * @param {boolean} status 
   * @returns 
   */

  function setFilevineSessionForm(status) {
    return {
      type: SET_FILEVINE_SESSION_FORM,
      status
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {string} pageType
   */
  function deleteSupportRequest(record, pageType) {
    return {
      type: DELETE_SUPPORT_REQUEST,
      record,
      pageType
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function deleteSupportRequestSuccess(success) {
    return {
      type: DELETE_SUPPORT_REQUEST_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function deleteSupportRequestError(error) {
    return {
      type: DELETE_SUPPORT_REQUEST_ERROR,
      error
    }
  }

  /**
   * 
   * @param {array} litifyAlert 
   * @returns 
   */
  function litifyInfoDialog(litifyAlert) {
    return {
      type: LITIFY_INFO_DIALOG,
      litifyAlert
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {} form 
   * @returns 
   */
  function updateLitifyRecord(record, form) {
    return {
      type: UPDATE_LITIFY_RECORD,
      record,
      form
    }
  }

  /**
   * @param {object} record 
   */
  function updateLitifyRecordSuccess(record, success) {
    return {
      type: UPDATE_LITIFY_RECORD_SUCCESS,
      record,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function updateLitifyRecordError(error) {
    return {
      type: UPDATE_LITIFY_RECORD_ERROR,
      error,
    };
  }
 
  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} setExistingPasswordModal 
   */
  function createUser(record, form, setExistingPasswordModal) {
    return {
      type: CREATE_USER,
      record,
      form,
      setExistingPasswordModal
    };
  }
  
  /**
   * @param {object} record 
   */
  function syncRecords(record) {
    return {
      type: SYNC_RECORDS,
      record,
    };
  }

  return {
    loadRecord,
    loadRecordSuccess,
    loadRecordError,
    loadRecords,
    loadRecordsSuccess,
    loadRecordsError,
    createRecord,
    createRecordSuccess,
    createRecordError,
    updateRecord,
    updateRecordSuccess,
    updateRecordError,
    deleteRecord,
    deleteRecordSuccess,
    deleteRecordError,
    loadRecordsCacheHit,
    setHeadersData,
    loadRecordsMetaData,
    loadRecordsMetaDataSuccess,
    loadRecordsMetaDataError,
    UpdateStateFilter,
    excelExport,
    excelExportSuccess,
    excelExportError,
    createClientWithCase,
    createClientWithCaseError,
    createClientWithCaseSuccess,
    createResponderForm,
    createResponderFormError,
    createResponderFormSuccess,
    loadActiveFilterRecord,
    loadActiveFilterRecordError,
    loadActiveFilterRecordSuccess,
    clearIndividualStoreRecord,
    setFilevineSessionForm,
    deleteSupportRequest,
    deleteSupportRequestError,
    deleteSupportRequestSuccess,
    litifyInfoDialog,
    updateLitifyRecord,
    updateLitifyRecordSuccess,
    updateLitifyRecordError,
    updateExistingPasswordData,
    clearExistingPasswordData,
    createUser,
    syncRecords
  };
}

/*
 *
 *   reducer
 *
 */
import produce from 'immer';

import merge from 'lodash/merge';

import {
  UPDATE_VERSION_ERROR
} from 'blocks/session/constants';


const initialState = {
  records: [],
  record: {},
  recordsMetaData: {},
  settings: {},
  loading: false,
  pageLoader: false,
  pageError: false,
  error: false,
  progress: false,
  lastUpdate: null,
  totalPageCount: false,
  headers: {
    offset: 0,
    limit: 25,
    search: false,
    filter: false,
    sort: false,
    page: 1
  },
  filter: { state: 'AZ' },
  filevineRecords: [],
  buttonLoader: false,
  contentNotFount: false,
  questionsNotFound: false,
  autoSaveForms: [],
  defendantPracticeDetails: {},
  frogsDynamicQuestions: []
};

/**
 * @param {object} constants 
 * @param {string} name 
 */
export default function reducer(constants, name, additionalConstants = {}) {

  const {
    LOAD_RECORD,
    LOAD_RECORD_SUCCESS,
    LOAD_RECORD_ERROR,
    LOAD_RECORDS,
    LOAD_RECORDS_SUCCESS,
    LOAD_RECORDS_ERROR,
    CREATE_RECORD_SUCCESS,
    CREATE_RECORD_ERROR,
    UPDATE_RECORD_SUCCESS,
    UPDATE_RECORD_ERROR,
    DELETE_RECORD,
    DELETE_RECORD_SUCCESS,
    DELETE_RECORD_ERROR,
    LOAD_RECORDS_VALID_CACHE,
    SET_HEADERS_DATA,
    LOAD_RECORDS_META_DATA_SUCCESS,
    LOAD_RECORDS_META_DATA_ERROR,
    UPDATE_STATE_FILTER,
    CREATE_CLIENT_WITH_CASE_ERROR,
    CREATE_CLIENT_WITH_CASE_SUCCESS,
    CREATE_RESPONDER_FORM_ERROR,
    CREATE_RESPONDER_FORM_SUCCESS,
    LOAD_ACTIVE_FILTER_RECORD,
    CLEAR_INDIVIDUAL_STORE_RECORD,
    SET_FILEVINE_SESSION_FORM,
    DELETE_SUPPORT_REQUEST,
    DELETE_SUPPORT_REQUEST_SUCCESS,
    DELETE_SUPPORT_REQUEST_ERROR,
    LITIFY_INFO_DIALOG,
    UPDATE_LITIFY_RECORD_SUCCESS,
    UPDATE_LITIFY_RECORD_ERROR,
    UPDATE_EXISTED_PASSWORD_DATA,
    CLEAR_EXISTED_PASSWORD_DATA,
    SYNC_RECORDS
  } = constants;

  const {
    BULK_TRANSLATION_ERROR,
    BULK_TRANSLATION_SUCCESS,
    CUSTOMER_OBJECTIONS_ERROR,
    CUSTOMER_OBJECTIONS_SUCCESS,
    LOAD_PRACTICE_SETTINGS,
    LOAD_PRACTICE_SETTINGS_SUCCESS,
    UPDATE_PRACTICE_SETTINGS_SUCCESS,
    LOAD_PRACTICE_SETTINGS_ERROR,
    UPDATE_PRACTICE_SETTINGS_ERROR,
    RESET_LOGIN_ATTEMPT_ERROR,
    RESET_LOGIN_ATTEMPT_SUCCESS,
    LOAD_GLOBAL_SETTINGS,
    LOAD_GLOBAL_SETTINGS_ERROR,
    LOAD_GLOBAL_SETTINGS_SUCCESS,
    UPDATE_GLOBAL_SETTINGS_ERROR,
    UPDATE_GLOBAL_SETTINGS_SUCCESS,
    UPLOAD_FORM_ERROR,
    GENERATE_FORM_ERROR,
    CANCEL_FORM_ERROR,
    SAVE_FORM_ERROR,
    SAVE_FORM_SUCCESS,
    LOAD_FORM_ERROR,
    LOAD_FORM_SUCCESS,
    UPDATE_FORM_ERROR,
    UPDATE_FORM_SUCCESS,
    FETCH_FORM_ERROR,
    FETCH_FORM_SUCCESS,
    GENERATE_DOCUMENT_ERROR,
    SAVE_DOCUMENT,
    SAVE_DOCUMENT_ERROR,
    SAVE_DOCUMENT_SUCCESS,
    SEND_QUESTIONS,
    SEND_QUESTIONS_ERROR,
    SEND_QUESTIONS_SUCCESS,
    LOAD_BILLING_DETAILS,
    LOAD_BILLING_DETAILS_ERROR,
    LOAD_BILLING_DETAILS_SUCCESS,
    UPDATE_BILLING_DETAILS_ERROR,
    UPDATE_BILLING_DETAILS_SUCCESS,
    UPDATE_KEY,
    DELETE_FORM,
    DELETE_FORM_ERROR,
    DELETE_FORM_SUCCESS,
    SAVE_STANDARD_FORM_ERROR,
    SAVE_STANDARD_FORM_SUCCESS,
    SAVE_QUESTIONS_FORM,
    SAVE_QUESTIONS_FORM_ERROR,
    SAVE_QUESTIONS_FORM_SUCCESS,
    CREATE_MEDICAL_HISTORY,
    CREATE_MEDICAL_HISTORY_ERROR,
    CREATE_MEDICAL_HISTORY_SUCCESS,
    UPDATE_MEDICAL_HISTORY_ERROR,
    UPDATE_MEDICAL_HISTORY_SUCCESS,
    DELETE_MEDICAL_HISTORY,
    DELETE_MEDICAL_HISTORY_ERROR,
    DELETE_MEDICAL_HISTORY_SUCCESS,
    VIEW_MEDICAL_HISTORY,
    VIEW_MEDICAL_HISTORY_ERROR,
    VIEW_MEDICAL_HISTORY_SUCCESS,
    GENERATE_MEDICAL_SUMMARY,
    GENERATE_MEDICAL_SUMMARY_ERROR,
    GENERATE_MEDICAL_SUMMARY_SUCCESS,
    REQUEST_HELP_ERROR,
    REQUEST_HELP_SUCCESS,
    REQUEST_TRANSLATION_ERROR,
    MERGE_TRANSLATION_ERROR,
    MERGE_TRANSLATION_SUCCESS,
    SEND_VERIFICATION,
    SEND_VERIFICATION_ERROR,
    SEND_VERIFICATION_SUCCESS,
    COPY_TO_LAWYER_RESPONSE_ERROR,
    COPY_TO_LAWYER_RESPONSE_SUCCESS,
    LOAD_SUBSCRIPTIONS_ERROR,
    LOAD_SUBSCRIPTIONS_SUCCESS,
    CREATE_SUBSCRIPTIONS_ERROR,
    CREATE_SUBSCRIPTIONS_SUCCESS,
    UPDATE_SUBSCRIPTIONS_ERROR,
    UPDATE_SUBSCRIPTIONS_SUCCESS,
    CANCEL_SUBSCRIPTIONS_ERROR,
    CANCEL_SUBSCRIPTIONS_SUCCESS,
    LOAD_PRACTICE_SUCCESS,
    UPDATE_PRACTICE_SUCCESS,
    LOAD_PRACTICE_ERROR,
    UPDATE_PRACTICE_ERROR,
    SHRED,
    SHRED_ERROR,
    SHRED_SUCCESS,
    EDIT_FORM_ERROR,
    EDIT_FORM_SUCCESS,
    ATTACH_CASE,
    ATTACH_CASE_SUCCESS,
    ATTACH_CASE_ERROR,
    CREATE_CLIENT_RECORD_SUCCESS,
    CREATE_CLIENT_RECORD_ERROR,
    SET_RESPONSE_DATE_SUCCESS,
    SET_RESPONSE_DATE_ERROR,
    RESEND_QUESTIONS,
    RESEND_QUESTIONS_SUCCESS,
    RESEND_QUESTIONS_ERROR,
    SAVE_ALL_FORM_ERROR,
    UPLOAD_SUMMARY,
    UPLOAD_SUMMARY_SUCCESS,
    UPLOAD_SUMMARY_ERROR,
    DELETE_SUMMARY,
    DELETE_SUMMARY_ERROR,
    DELETE_SUMMARY_SUCCESS,
    UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR,
    UPDATE_PRACTICE_MODIFIED_TEMPLATE_SUCCESS,
    SAVE_SUB_GROUP_FORM_ERROR,
    SAVE_SUB_GROUP_FORM_SUCCESS,
    GENERATE_POS_DOCUMENT,
    GENERATE_POS_DOCUMENT_ERROR,
    GENERATE_POS_DOCUMENT_SUCCESS,
    COPY_ALL_TO_LAWYER_RESPONSE_SUCCESS,
    COPY_ALL_TO_LAWYER_RESPONSE_ERROR,
    SAVE_COMMENT_ERROR,
    SAVE_COMMENT_SUCCESS,
    ARCHIVE_CASE,
    ARCHIVE_CASE_SUCCESS,
    ARCHIVE_CASE_ERROR,
    UNARCHIVE_CASE_SUCCESS,
    UNARCHIVE_CASE_ERROR,
    ADD_CONSULTATION_ERROR,
    ADD_CONSULTATION_SUCCESS,
    DELETE_CONSULTATION_ERROR,
    DELETE_CONSULTATION_SUCCESS,
    UPDATE_EDITED_FLAG_ERROR,
    UPDATE_EDITED_FLAG_SUCCESS,
    UPLOAD_PROPOUND_TEMPLATE_SUCCESS,
    GENERATE_PROPOUND_TEMPLATE_ERROR,
    LOAD_PROPOUND_TEMPLATE_SUCCESS,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE_SUCCESS,
    SAVE_QUESTIONS_PROPOUND_TEMPLATE_ERROR,
    CANCEL_PROPOUND_TEMPLATE_ERROR,
    SAVE_PROPOUND_TEMPLATE_ERROR,
    SAVE_PROPOUND_FORM_ERROR,
    SAVE_PROPOUND_FORM_SUCCESS,
    LOAD_PROPOUND_RECORD,
    LOAD_PROPOUND_RECORD_ERROR,
    LOAD_PROPOUND_RECORD_SUCCESS,
    LOAD_PROPOUND_FORM_ERROR,
    LOAD_PROPOUND_FORM_SUCCESS,
    DELETE_PROPOUND_FORM,
    DELETE_PROPOUND_FORM_ERROR,
    DELETE_PROPOUND_FORM_SUCCESS,
    UPDATE_PROPOUND_FORM,
    UPDATE_PROPOUND_FORM_ERROR,
    UPDATE_PROPOUND_FORM_SUCCESS,
    SEND_PROPOUND_QUESTIONS,
    SEND_PROPOUND_QUESTIONS_ERROR,
    SEND_PROPOUND_QUESTIONS_SUCCESS,
    GENERATE_PROPOUND_DOCUMENT_ERROR,
    SAVE_PROPOUND_FORM_S3KEY_ERROR,
    SAVE_PROPOUND_FORM_S3KEY_SUCCESS,
    UPLOAD_PROPOUND_TEMPLATE_ERROR,
    CREATE_CASES_ERROR,
    CREATE_CASES_SUCCESS,
    SAVE_PROPOUNDER_SERVING_ATTORNEY,
    SAVE_PROPOUNDER_SERVING_ATTORNEY_ERROR,
    SAVE_PROPOUNDER_SERVING_ATTORNEY_SUCCESS,
    LOAD_GLOBAL_SETTINGS_META_DATA_SUCCESS,
    PROPOUND_REQUEST_HELP_SUCCESS,
    PROPOUND_REQUEST_HELP_ERROR,
    SAVE_PROPOUND_QUESTIONS,
    SAVE_PROPOUND_QUESTIONS_ERROR,
    SAVE_PROPOUND_QUESTIONS_SUCCESS,
    CREATE_CUSTOM_TEMPLATE_SUCCESS,
    CREATE_CUSTOM_TEMPLATE_ERROR,
    UPDATE_CUSTOM_TEMPLATE_ERROR,
    UPDATE_CUSTOM_TEMPLATE_SUCCESS,
    FILEVINE_LOAD_RECORDS,
    FILEVINE_LOAD_RECORDS_SUCCESS,
    FILEVINE_LOAD_RECORDS_ERROR,
    SET_FILEVINE_HEADERS_DATA,
    FILEVINE_SESSION_ERROR,
    FILEVINE_SYNC_DATA_SUCCESS,
    FILEVINE_SYNC_DATA_ERROR,
    AUTO_REFRESH_SESSION_ERROR,
    FILEVINE_SYNC_ERROR,
    MYCASE_SYNC_DATA_SUCCESS,
    MYCASE_SYNC_DATA_ERROR,
    UPLOAD_MY_CASE_DOCUMENT,
    UPLOAD_MY_CASE_DOCUMENT_SUCCESS,
    REMOVE_FILEVINE_SECRET_SUCCESS,
    REMOVE_FILEVINE_SECRET_ERROR,
    DELETE_MYCASE_SECRET_SUCCESS,
    DELETE_MYCASE_SECRET_ERROR,
    CONTENT_NOT_FOUND,
    PROPOUND_DOCUMENT_CONTENT_NOT_FOUND,
    UPLOAD_FILEVINE_DOCUMENT,
    UPLOAD_FILEVINE_DOCUMENT_SUCCESS,
    UPLOAD_FILEVINE_DOCUMENT_ERROR,
    VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION,
    VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_ERROR,
    UPDATE_PLAN_DETAILS,
    UPDATE_PLAN_DETAILS_SUCCESS,
    UPDATE_PLAN_DETAILS_ERROR,
    SEND_RESPOND_QUESTIONS,
    SEND_RESPOND_QUESTIONS_SUCCESS,
    SEND_RESPOND_QUESTIONS_ERROR,
    SETTINGS_DISCOUNT_CODE_SUCCESS,
    SETTINGS_DISCOUNT_CODE_ERROR,
    PRACTICE_CANCEL_SUBSCRIPTION_ERROR,
    PRACTICE_CANCEL_SUBSCRIPTION_SUCCESS,
    PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_ERROR,
    PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_SUCCESS,
    SUBSCRIPTION_RENEWALS_SUCCESS,
    SUBSCRIPTION_RENEWALS_ERROR,
    UPLOAD_DOCUMENT_FORM_ERROR,
    LOAD_DOCUMENT_FORM_SUCCESS,
    LOAD_DOCUMENT_FORM_ERROR,
    GENERATE_DOCUMENT_FORM_ERROR,
    DOCUMENT_CONTENT_NOT_FOUND,
    QUICK_CREATE_SAVE_QUESTIONS_FORM_SUCCESS,
    QUICK_CREATE_SAVE_QUESTIONS_FORM_ERROR,
    SAVE_DOCUMENT_DETAILS_SUCCESS_ERROR,
    CANCEL_DOCUMENT_FORM_ERROR,
    QUICK_CREATE_SAVE_STANDARD_FORM_ERROR,
    QUICK_CREATE_SAVE_STANDARD_FORM_SUCCESS,
    CLEAR_RECORD,
    EXTRACTION_REQUEST_HELP_SUCCESS,
    EXTRACTION_REQUEST_HELP_ERROR,
    SAVE_CLIENT_CASE_INFO,
    SAVE_CLIENT_CASE_INFO_SUCCESS,
    SAVE_CLIENT_CASE_INFO_ERROR,
    SAVE_EXTRACTION_QUESTIONS,
    SAVE_EXTRACTION_QUESTIONS_SUCCESS,
    SAVE_EXTRACTION_QUESTIONS_ERROR,
    QUICK_CREATE_SAVE_QUESTIONS_FORM,
    DELETE_QUICK_CREATE_DOCUMENT,
    DELETE_QUICK_CREATE_DOCUMENT_ERROR,
    DELETE_QUICK_CREATE_DOCUMENT_SUCCESS,
    SAVE_DOCUMENT_QUESTIONS,
    SAVE_DOCUMENT_QUESTIONS_ERROR,
    SAVE_DOCUMENT_QUESTIONS_SUCCESS,
    SAVE_DOCUMENT_INFO_QUESTIONS_ERROR,
    RESTORE_RESPONSE_HISTORY_SUCCESS,
    RESTORE_RESPONSE_HISTORY_ERROR,
    GLOBAL_ATTORNEY_RESPONSE_TRACKING_SUCCESS,
    GLOBAL_ATTORNEY_RESPONSE_TRACKING_ERROR,
    ATTORNEY_RESPONSE_TRACKING_SUCCESS,
    ATTORNEY_RESPONSE_TRACKING_ERROR,
    AUTO_SAVE_FORM_DETAILS_SUCCESS,
    AUTO_SAVE_FORM_DETAILS_ERROR,
    CLEAR_AUTO_SAVE_FORM_DETAILS,
    CREATE_CLIENT_CASE_ERROR,
    FILEVINE_SESSION_COMMON_UPDATE_ERROR,
    VIP_SUBSCRIPTION,
    VIP_SUBSCRIPTION_SUCCESS,
    VIP_SUBSCRIPTION_ERROR,
    VIP_CANCEL_SUBSCRIPTION,
    VIP_CANCEL_SUBSCRIPTION_SUCCESS,
    VIP_CANCEL_SUBSCRIPTION_ERROR,
    LITIFY_UPLOAD_DOCUMENT,
    LITIFY_UPLOAD_DOCUMENT_SUCCESS,
    LITIFY_UPLOAD_DOCUMENT_ERROR,
    CREATE_LITIFY_BULK_RECORDS_SUCCESS,
    CREATE_LITIFY_BULK_RECORDS_ERROR,
    RETRY_MEDICAL_SUMMARY,
    RETRY_MEDICAL_SUMMARY_ERROR,
    RETRY_MEDICAL_SUMMARY_SUCCESS,
    INITIAL_SUBSCRIPTION,
    INITIAL_SUBSCRIPTION_SUCCESS,
    INITIAL_SUBSCRIPTION_ERROR,
    STORE_DEFENDANT_DETAILS,
    CREATE_LITIFY_CLIENT_CASE_ERROR,
    MANAGE_ARCHIVE_CASE,
    DELETE_CLIO_SECRET_SUCCESS,
    DELETE_CLIO_SECRET_ERROR,
    CLIO_UPLOAD_DOCUMENT,
    CLIO_UPLOAD_DOCUMENT_ERROR,
    CLIO_UPLOAD_DOCUMENT_SUCCESS,
    CLIO_UPDATE_USER_DETAILS_ERROR,
    CLIO_SYNC_DATA_SUCCESS,
    CLIO_SYNC_DATA_ERROR,
    ADD_MORE_LICENSE_SUCCESS,
    ADD_MORE_LICENSE_ERROR,
    ADD_MORE_LICENSE_VALIDATION_SUCCESS,
    ADD_MORE_LICENSE_VALIDATION_ERROR,
    SUBSCRIPTION_LICENSE_VALIDATION_SUCCESS,
    SUBSCRIPTION_LICENSE_VALIDATION_ERROR,
    CREATE_PROPOUND_SUBSCRIPTIONS_ERROR,
    LOAD_DOCUMENT_EDITOR_SUCCESS,
    SAVE_DOCUMENT_EDITOR_SUCCESS,
    SAVE_DOCUMENT_EDITOR_ERROR,
    DOCUMENT_EDITOR_STATUS,
    LOAD_PROPOUND_DOCUMENT_EDITOR_SUCCESS,
    SAVE_PROPOUND_DOCUMENT_EDITOR_SUCCESS,
    SAVE_PROPOUND_DOCUMENT_EDITOR_ERROR,
    UPDATE_OBJECTION_FORM_SUCCESS,
    UPDATE_OBJECTION_FORM_ERROR,
    DELETE_ATTORNEY_OBJECTION_SUCCESS,
    DELETE_ATTORNEY_OBJECTION_ERROR,
    ESERVE_POPUP_STATUS,
    SAVE_DISCOVERY_FILENAME_SUCCESS,
    SAVE_PROPOUND_FILENAME_SUCCESS,
    CREATE_NEW_FILEVINE_SESSION_ERROR,
    QUESTIONS_NOT_FOUND,
    PROPOUND_QUESTIONS_NOT_FOUND,
    UPDATE_QUESTIONS,
    UPDATE_QUESTIONS_SUCCESS,
    UPDATE_QUESTIONS_ERROR,
    DELETE_HASH_WITH_FILE_SUCCESS,
    DELETE_HASH_WITH_FILE_ERROR,
    LOAD_FROGS_QUESTIONS_SUCCESS,
    UPDATE_FROGS_FORM_SUCCESS,
    UPDATE_FROGS_FORM_ERROR,
    UPDATE_FROGS_OBJECTION_FORM_SUCCESS,
    UPDATE_FROGS_OBJECTION_FORM_ERROR,
    DELETE_ATTORNEY_FROGS_OBJECTION_SUCCESS,
    DELETE_ATTORNEY_FROGS_OBJECTION_ERROR,
    SAVE_SUB_GROUP_FROGS_FORM_SUCCESS,
    SAVE_SUB_GROUP_FROGS_FORM_ERROR,
    SHOW_QUICK_CREATE_DESCRIPTION,
    EDIT_PROPOUND_FORM_SUCCESS,
    EDIT_PROPOUND_FORM_ERROR,
    ADD_FROGS_QUESTIONS_SUCCESS,
    ADD_FROGS_QUESTIONS_ERROR,
    DELETE_DUPLICATE_SET_SUCCESS,
    DELETE_DUPLICATE_SET_ERROR,
    FETCH_DISCOVERY_HISTORY,
    FETCH_DISCOVERY_HISTORY_SUCCESS,
  } = additionalConstants;

  return function recordsReducer(state = initialState, { type, id, record, records, recordsMetaData = {}, error, success, key, totalPageCount, filter, content, status, details, litifyAlert, passwordData, showQuickCreateDescription, discoveryHistory }) {

    return produce(state, draft => {
      switch (type) {
        case LOAD_RECORDS_VALID_CACHE:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          draft.subscriptionSuccess = false;
          break;
        case LOAD_RECORDS:
        case LOAD_ACTIVE_FILTER_RECORD:
        case ARCHIVE_CASE:
        case UPDATE_QUESTIONS:
          draft.loading = true;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORD:
        case LOAD_PROPOUND_RECORD:
          draft.pageLoader = true;
          draft.pageError = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS_SUCCESS:
          draft.records = records;
          draft.lastUpdate = Math.floor(Date.now() / 1000);
          draft.totalPageCount = parseInt(totalPageCount);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS_META_DATA_SUCCESS:
        case LOAD_GLOBAL_SETTINGS_META_DATA_SUCCESS:
          draft.recordsMetaData = Object.assign({}, draft.recordsMetaData, recordsMetaData);
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case CREATE_RECORD_ERROR:
        case UPDATE_RECORD_ERROR:
        case DELETE_RECORD_ERROR:
        case UPLOAD_FORM_ERROR:
        case GENERATE_FORM_ERROR:
        case CANCEL_FORM_ERROR:
        case SAVE_FORM_ERROR:
        case LOAD_FORM_ERROR:
        case UPDATE_FORM_ERROR:
        case FETCH_FORM_ERROR:
        case GENERATE_DOCUMENT_ERROR:
        case SAVE_DOCUMENT_ERROR:
        case SEND_QUESTIONS_ERROR:
        case UPDATE_GLOBAL_SETTINGS_ERROR:
        case LOAD_GLOBAL_SETTINGS_ERROR:
        case LOAD_BILLING_DETAILS_ERROR:
        case UPDATE_BILLING_DETAILS_ERROR:
        case DELETE_FORM_ERROR:
        case SAVE_STANDARD_FORM_ERROR:
        case SAVE_QUESTIONS_FORM_ERROR:
        case CREATE_MEDICAL_HISTORY_ERROR:
        case UPDATE_MEDICAL_HISTORY_ERROR:
        case DELETE_MEDICAL_HISTORY_ERROR:
        case VIEW_MEDICAL_HISTORY_ERROR:
        case GENERATE_MEDICAL_SUMMARY_ERROR:
        case REQUEST_HELP_ERROR:
        case REQUEST_TRANSLATION_ERROR:
        case MERGE_TRANSLATION_ERROR:
        case SEND_VERIFICATION_ERROR:
        case BULK_TRANSLATION_ERROR:
        case UPDATE_VERSION_ERROR:
        case LOAD_PRACTICE_SETTINGS_ERROR:
        case UPDATE_PRACTICE_SETTINGS_ERROR:
        case RESET_LOGIN_ATTEMPT_ERROR:
        case CUSTOMER_OBJECTIONS_ERROR:
        case COPY_TO_LAWYER_RESPONSE_ERROR:
        case LOAD_SUBSCRIPTIONS_ERROR:
        case CREATE_SUBSCRIPTIONS_ERROR:
        case UPDATE_SUBSCRIPTIONS_ERROR:
        case CANCEL_SUBSCRIPTIONS_ERROR:
        case LOAD_PRACTICE_ERROR:
        case UPDATE_PRACTICE_ERROR:
        case SHRED_ERROR:
        case EDIT_FORM_ERROR:
        case ATTACH_CASE_ERROR:
        case CREATE_CLIENT_RECORD_ERROR:
        case SET_RESPONSE_DATE_ERROR:
        case RESEND_QUESTIONS_ERROR:
        case LOAD_RECORDS_META_DATA_ERROR:
        case SAVE_ALL_FORM_ERROR:
        case UPLOAD_SUMMARY_ERROR:
        case DELETE_SUMMARY_ERROR:
        case UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR:
        case SAVE_SUB_GROUP_FORM_ERROR:
        case GENERATE_POS_DOCUMENT_ERROR:
        case COPY_ALL_TO_LAWYER_RESPONSE_ERROR:
        case SAVE_COMMENT_ERROR:
        case ARCHIVE_CASE_ERROR:
        case UNARCHIVE_CASE_ERROR:
        case ADD_CONSULTATION_ERROR:
        case DELETE_CONSULTATION_ERROR:
        case UPDATE_EDITED_FLAG_ERROR:
        case GENERATE_PROPOUND_TEMPLATE_ERROR:
        case SAVE_QUESTIONS_PROPOUND_TEMPLATE_ERROR:
        case CANCEL_PROPOUND_TEMPLATE_ERROR:
        case SAVE_PROPOUND_TEMPLATE_ERROR:
        case SAVE_PROPOUND_FORM_ERROR:
        case LOAD_PROPOUND_FORM_ERROR:
        case DELETE_PROPOUND_FORM_ERROR:
        case UPDATE_PROPOUND_FORM_ERROR:
        case SEND_PROPOUND_QUESTIONS_ERROR:
        case GENERATE_PROPOUND_DOCUMENT_ERROR:
        case SAVE_PROPOUND_FORM_S3KEY_ERROR:
        case UPLOAD_PROPOUND_TEMPLATE_ERROR:
        case CREATE_CLIENT_WITH_CASE_ERROR:
        case SAVE_PROPOUNDER_SERVING_ATTORNEY_ERROR:
        case CREATE_RESPONDER_FORM_ERROR:
        case PROPOUND_REQUEST_HELP_ERROR:
        case SAVE_PROPOUND_QUESTIONS_ERROR:
        case CREATE_CUSTOM_TEMPLATE_ERROR:
        case UPDATE_CUSTOM_TEMPLATE_ERROR:
        case FILEVINE_SESSION_ERROR:
        case FILEVINE_SYNC_DATA_ERROR:
        case AUTO_REFRESH_SESSION_ERROR:
        case FILEVINE_SYNC_ERROR:
        case MYCASE_SYNC_DATA_ERROR:
        case DELETE_MYCASE_SECRET_ERROR:
        case REMOVE_FILEVINE_SECRET_ERROR:
        case UPLOAD_FILEVINE_DOCUMENT_ERROR:
        case VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_ERROR:
        case UPLOAD_FILEVINE_DOCUMENT_ERROR:
        case UPDATE_PLAN_DETAILS_ERROR:
        case SEND_RESPOND_QUESTIONS_ERROR:
        case SETTINGS_DISCOUNT_CODE_ERROR:
        case PRACTICE_CANCEL_SUBSCRIPTION_ERROR:
        case PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_ERROR:
        case SUBSCRIPTION_RENEWALS_ERROR:
        case UPLOAD_DOCUMENT_FORM_ERROR:
        case LOAD_DOCUMENT_FORM_ERROR:
        case GENERATE_DOCUMENT_FORM_ERROR:
        case QUICK_CREATE_SAVE_QUESTIONS_FORM_ERROR:
        case SAVE_DOCUMENT_DETAILS_SUCCESS_ERROR:
        case CANCEL_DOCUMENT_FORM_ERROR:
        case QUICK_CREATE_SAVE_STANDARD_FORM_ERROR:
        case EXTRACTION_REQUEST_HELP_ERROR:
        case SAVE_CLIENT_CASE_INFO_ERROR:
        case SAVE_EXTRACTION_QUESTIONS_ERROR:
        case SAVE_DOCUMENT_INFO_QUESTIONS_ERROR:
        case RESTORE_RESPONSE_HISTORY_ERROR:
        case GLOBAL_ATTORNEY_RESPONSE_TRACKING_ERROR:
        case ATTORNEY_RESPONSE_TRACKING_ERROR:
        case CREATE_CLIENT_CASE_ERROR:
        case FILEVINE_SESSION_COMMON_UPDATE_ERROR:
        case VIP_SUBSCRIPTION_ERROR:
        case VIP_CANCEL_SUBSCRIPTION_ERROR:
        case LITIFY_UPLOAD_DOCUMENT_ERROR:
        case CREATE_LITIFY_BULK_RECORDS_ERROR:
        case RETRY_MEDICAL_SUMMARY_ERROR:
        case DELETE_SUPPORT_REQUEST_ERROR:
        case UPDATE_LITIFY_RECORD_ERROR:
        case CREATE_LITIFY_CLIENT_CASE_ERROR:
        case DELETE_CLIO_SECRET_ERROR:
        case CLIO_UPLOAD_DOCUMENT_ERROR:
        case CLIO_UPDATE_USER_DETAILS_ERROR:
        case CLIO_SYNC_DATA_ERROR:
        case ADD_MORE_LICENSE_ERROR:
        case ADD_MORE_LICENSE_VALIDATION_ERROR:
        case SUBSCRIPTION_LICENSE_VALIDATION_ERROR:
        case CREATE_PROPOUND_SUBSCRIPTIONS_ERROR:
        case SAVE_DOCUMENT_EDITOR_ERROR:
        case SAVE_PROPOUND_DOCUMENT_EDITOR_ERROR:
        case UPDATE_OBJECTION_FORM_ERROR:
        case DELETE_ATTORNEY_OBJECTION_ERROR:
        case CREATE_NEW_FILEVINE_SESSION_ERROR:
        case UPDATE_FROGS_FORM_ERROR:
        case UPDATE_FROGS_OBJECTION_FORM_ERROR:
        case DELETE_ATTORNEY_FROGS_OBJECTION_ERROR:
        case SAVE_SUB_GROUP_FROGS_FORM_ERROR:
        case EDIT_PROPOUND_FORM_ERROR:
        case DELETE_DUPLICATE_SET_ERROR:
          draft.loading = false;
          draft.error = false;
          draft.updateError = error;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS_ERROR:
          draft.loading = false;
          draft.error = true;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORD_ERROR:
        case LOAD_PROPOUND_RECORD_ERROR:
          draft.loading = false;
          draft.pageLoader = false;
          draft.pageError = true;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case CREATE_RECORD_SUCCESS:
          draft.records = [record].concat(draft.records);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case CREATE_CUSTOM_TEMPLATE_SUCCESS:
          draft.records = [record].concat(draft.records);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case LOAD_RECORD_SUCCESS:
        case LOAD_PROPOUND_RECORD_SUCCESS:
          draft.record = draft?.record?.id === record?.id ? Object.assign({}, draft.record, record) : Object.assign({}, record);
          draft.recordsMetaData = Object.assign({}, draft.recordsMetaData, recordsMetaData);
          draft.pageError = false;
          draft.pageLoader = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case EDIT_FORM_SUCCESS:
        case SEND_QUESTIONS_SUCCESS:
        case LOAD_PROPOUND_TEMPLATE_SUCCESS:
        case SAVE_PROPOUNDER_SERVING_ATTORNEY_SUCCESS:
        case SAVE_PROPOUND_FORM_S3KEY_SUCCESS:
        case EDIT_PROPOUND_FORM_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case UPLOAD_PROPOUND_TEMPLATE_SUCCESS:
          draft.records = state.records.find(r => record.id === r.id) ? state.records.map((r) => record.id === r.id ? Object.assign({}, r, record) : Object.assign({}, r)) : state.records.concat([Object.assign({}, record)]);
          draft.record = record;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case UPDATE_RECORD_SUCCESS:
        case UPDATE_CUSTOM_TEMPLATE_SUCCESS:
        case SET_RESPONSE_DATE_SUCCESS:
        case ATTORNEY_RESPONSE_TRACKING_SUCCESS:
        case UPDATE_LITIFY_RECORD_SUCCESS:
          draft.records = state.records.find(r => record.id === r.id) ? state.records.map((r) => record.id === r.id ? Object.assign({}, r, record) : Object.assign({}, r)) : state.records.concat([Object.assign({}, record)]);
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case LOAD_FORM_SUCCESS:
        case UPDATE_FORM_SUCCESS:
        case FETCH_FORM_SUCCESS:
        case UPLOAD_SUMMARY_SUCCESS:
        case DELETE_SUMMARY_SUCCESS:
        case UPDATE_PRACTICE_MODIFIED_TEMPLATE_SUCCESS:
        case ADD_CONSULTATION_SUCCESS:
        case LOAD_PROPOUND_FORM_SUCCESS:
        case LOAD_DOCUMENT_FORM_SUCCESS:
        case UPDATE_OBJECTION_FORM_SUCCESS:
        case DELETE_ATTORNEY_OBJECTION_SUCCESS:
          draft.records = state.records.find(r => record.id === r.id) ? state.records.map((r) => record.id === r.id ? Object.assign({}, r, record) : Object.assign({}, r)) : state.records.concat([Object.assign({}, record)]);
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case DELETE_RECORD_SUCCESS:
          draft.records = draft.records.filter((r, i) => r.id !== id);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case DELETE_RECORD:
        case LOAD_BILLING_DETAILS:
        case SEND_QUESTIONS:
        case SHRED:
        case DELETE_FORM:
        case DELETE_MEDICAL_HISTORY:
        case VIEW_MEDICAL_HISTORY:
        case GENERATE_MEDICAL_SUMMARY:
        case SAVE_QUESTIONS_FORM:
        case SEND_VERIFICATION:
        case LOAD_PRACTICE_SETTINGS:
        case CREATE_MEDICAL_HISTORY:
        case ATTACH_CASE:
        case RESEND_QUESTIONS:
        case UPLOAD_SUMMARY:
        case DELETE_SUMMARY:
        case GENERATE_POS_DOCUMENT:
        case SAVE_DOCUMENT:
        case SAVE_QUESTIONS_PROPOUND_TEMPLATE:
        case DELETE_PROPOUND_FORM:
        case UPDATE_PROPOUND_FORM:
        case SEND_PROPOUND_QUESTIONS:
        case SAVE_PROPOUNDER_SERVING_ATTORNEY:
        case SAVE_PROPOUND_QUESTIONS:
        case UPLOAD_MY_CASE_DOCUMENT:
        case UPLOAD_FILEVINE_DOCUMENT:
        case VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION:
        case UPDATE_PLAN_DETAILS:
        case SEND_RESPOND_QUESTIONS:
        case SAVE_CLIENT_CASE_INFO:
        case SAVE_EXTRACTION_QUESTIONS:
        case QUICK_CREATE_SAVE_QUESTIONS_FORM:
        case VIP_SUBSCRIPTION:
        case VIP_CANCEL_SUBSCRIPTION:
        case LITIFY_UPLOAD_DOCUMENT:
        case RETRY_MEDICAL_SUMMARY:
        case INITIAL_SUBSCRIPTION:
        case DELETE_SUPPORT_REQUEST:
        case CLIO_UPLOAD_DOCUMENT:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = true;
          break;
        case UPDATE_BILLING_DETAILS_SUCCESS:
        case DELETE_FORM_SUCCESS:
        case SAVE_FORM_SUCCESS:
        case SAVE_STANDARD_FORM_SUCCESS:
        case SAVE_QUESTIONS_FORM_SUCCESS:
        case CREATE_MEDICAL_HISTORY_SUCCESS:
        case UPDATE_MEDICAL_HISTORY_SUCCESS:
        case DELETE_MEDICAL_HISTORY_SUCCESS:
        case GENERATE_MEDICAL_SUMMARY_SUCCESS:
        case REQUEST_HELP_SUCCESS:
        case MERGE_TRANSLATION_SUCCESS:
        case SEND_VERIFICATION_SUCCESS:
        case RESET_LOGIN_ATTEMPT_SUCCESS:
        case COPY_TO_LAWYER_RESPONSE_SUCCESS:
        case CREATE_SUBSCRIPTIONS_SUCCESS:
        case UPDATE_SUBSCRIPTIONS_SUCCESS:
        case CANCEL_SUBSCRIPTIONS_SUCCESS:
        case SHRED_SUCCESS:
        case ATTACH_CASE_SUCCESS:
        case CREATE_CLIENT_RECORD_SUCCESS:
        case RESEND_QUESTIONS_SUCCESS:
        case SAVE_SUB_GROUP_FORM_SUCCESS:
        case GENERATE_POS_DOCUMENT_SUCCESS:
        case COPY_ALL_TO_LAWYER_RESPONSE_SUCCESS:
        case SAVE_COMMENT_SUCCESS:
        case ARCHIVE_CASE_SUCCESS:
        case UNARCHIVE_CASE_SUCCESS:
        case DELETE_CONSULTATION_SUCCESS:
        case UPDATE_EDITED_FLAG_SUCCESS:
        case SAVE_QUESTIONS_PROPOUND_TEMPLATE_SUCCESS:
        case SAVE_PROPOUND_FORM_SUCCESS:
        case DELETE_PROPOUND_FORM_SUCCESS:
        case UPDATE_PROPOUND_FORM_SUCCESS:
        case SEND_PROPOUND_QUESTIONS_SUCCESS:
        case CREATE_CLIENT_WITH_CASE_SUCCESS:
        case CREATE_RESPONDER_FORM_SUCCESS:
        case PROPOUND_REQUEST_HELP_SUCCESS:
        case SAVE_PROPOUND_QUESTIONS_SUCCESS:
        case MYCASE_SYNC_DATA_SUCCESS:
        case UPLOAD_MY_CASE_DOCUMENT_SUCCESS:
        case REMOVE_FILEVINE_SECRET_SUCCESS:
        case DELETE_MYCASE_SECRET_SUCCESS:
        case FILEVINE_SYNC_DATA_SUCCESS:
        case UPLOAD_FILEVINE_DOCUMENT_SUCCESS:
        case SEND_RESPOND_QUESTIONS_SUCCESS:
        case PRACTICE_CANCEL_SUBSCRIPTION_SUCCESS:
        case PRACTICE_ADMIN_CANCEL_SUBSCRIPTION_SUCCESS:
        case SUBSCRIPTION_RENEWALS_SUCCESS:
        case QUICK_CREATE_SAVE_QUESTIONS_FORM_SUCCESS:
        case QUICK_CREATE_SAVE_STANDARD_FORM_SUCCESS:
        case EXTRACTION_REQUEST_HELP_SUCCESS:
        case SAVE_CLIENT_CASE_INFO_SUCCESS:
        case SAVE_EXTRACTION_QUESTIONS_SUCCESS:
        case RESTORE_RESPONSE_HISTORY_SUCCESS:
        case LITIFY_UPLOAD_DOCUMENT_SUCCESS:
        case CREATE_LITIFY_BULK_RECORDS_SUCCESS:
        case RETRY_MEDICAL_SUMMARY_SUCCESS:
        case DELETE_SUPPORT_REQUEST_SUCCESS:
        case DELETE_CLIO_SECRET_SUCCESS:
        case CLIO_UPLOAD_DOCUMENT_SUCCESS:
        case CLIO_SYNC_DATA_SUCCESS:
        case ADD_MORE_LICENSE_SUCCESS:
        case UPDATE_QUESTIONS_SUCCESS:
        case DELETE_HASH_WITH_FILE_SUCCESS:
        case SAVE_SUB_GROUP_FROGS_FORM_SUCCESS:
        case DELETE_DUPLICATE_SET_SUCCESS:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case CUSTOMER_OBJECTIONS_SUCCESS:
        case BULK_TRANSLATION_SUCCESS:
          draft.records = records;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case UPDATE_KEY:
          draft.key = key;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_GLOBAL_SETTINGS_SUCCESS:
        case UPDATE_GLOBAL_SETTINGS_SUCCESS:
        case VIEW_MEDICAL_HISTORY_SUCCESS:
        case SAVE_DOCUMENT_SUCCESS:
        case SAVE_DOCUMENT_EDITOR_SUCCESS:
        case SAVE_PROPOUND_DOCUMENT_EDITOR_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case LOAD_BILLING_DETAILS_SUCCESS:
          draft.billing = record;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_PRACTICE_SETTINGS_SUCCESS:
        case UPDATE_PRACTICE_SETTINGS_SUCCESS:
          draft.settings = record;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success || false;
          draft.progress = false;
          break;
        case LOAD_SUBSCRIPTIONS_SUCCESS:
          draft.respondingSubscriptions = record && record.respondingSubscriptions;
          draft.propoundingSubscriptions = record && record.propoundingSubscriptions;
          draft.recordsMetaData = Object.assign({}, draft.recordsMetaData, recordsMetaData);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_PRACTICE_SUCCESS:
          draft.practice = record;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case UPDATE_PRACTICE_SUCCESS:
        case GLOBAL_ATTORNEY_RESPONSE_TRACKING_SUCCESS:
          draft.practice = record;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case SET_HEADERS_DATA:
          draft.headers = merge({}, draft.headers, record);
          break;
        case UPDATE_STATE_FILTER:
          draft.filter = Object.assign({}, draft.filter, filter);
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          break;
        case FILEVINE_LOAD_RECORDS:
          draft.buttonLoader = true;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case FILEVINE_LOAD_RECORDS_SUCCESS:
          draft.filevineRecords = records;
          draft.loading = false;
          draft.buttonLoader = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case FILEVINE_LOAD_RECORDS_ERROR:
          draft.buttonLoader = false;
          draft.loading = false;
          draft.error = false;
          draft.updateError = error;
          draft.success = false;
          draft.progress = false;
          break;
        case SET_FILEVINE_HEADERS_DATA:
          draft.filevineHeaders = merge({}, draft.filevineHeaders, record);
          break;
        case CONTENT_NOT_FOUND:
        case PROPOUND_DOCUMENT_CONTENT_NOT_FOUND:
        case DOCUMENT_CONTENT_NOT_FOUND:
          draft.contentNotFount = content;
          draft.loading = false;
          draft.success = false;
          draft.progress = false;
          break;
        case SETTINGS_DISCOUNT_CODE_SUCCESS:
        case SUBSCRIPTION_LICENSE_VALIDATION_SUCCESS:
          draft.discountDetail = records;
          draft.loading = false;
          draft.success = false;
          draft.progress = false;
          draft.error = false;
          break;
        case CLEAR_RECORD:
        case CLEAR_INDIVIDUAL_STORE_RECORD:
          draft.record = record;
          draft.loading = false;
          draft.success = false;
          draft.progress = false;
          draft.error = false;
          break;
        case DELETE_QUICK_CREATE_DOCUMENT:
          draft.btnLoader = true;
          draft.success = false;
          draft.progress = false;
          draft.error = false;
          break;
        case DELETE_QUICK_CREATE_DOCUMENT_SUCCESS:
          draft.btnLoader = false;
          draft.success = false;
          draft.progress = false;
          draft.error = false;
          break;
        case DELETE_QUICK_CREATE_DOCUMENT_ERROR:
          draft.btnLoader = false;
          draft.success = false;
          draft.progress = false;
          draft.error = error;
          break;
        case SAVE_DOCUMENT_QUESTIONS:
          draft.success = false;
          draft.progress = false;
          draft.pageCustomLoader = true;
          draft.error = false;
          break;
        case SAVE_DOCUMENT_QUESTIONS_SUCCESS:
          draft.success = false;
          draft.progress = false;
          draft.pageCustomLoader = false;
          draft.error = error;
          break;
        case SAVE_DOCUMENT_QUESTIONS_ERROR:
          draft.success = false;
          draft.progress = false;
          draft.pageCustomLoader = false;
          draft.error = error;
          break;
        case AUTO_SAVE_FORM_DETAILS_SUCCESS:
        case SAVE_DISCOVERY_FILENAME_SUCCESS:
        case SAVE_PROPOUND_FILENAME_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          break;
        case CLEAR_AUTO_SAVE_FORM_DETAILS:
          draft.record = Object.assign({}, draft.record, record);
          break;
        case SET_FILEVINE_SESSION_FORM:
          draft.success = false;
          draft.progress = false;
          draft.filevineSessionForm = status;
          draft.error = error;
          break;
        case VIP_SUBSCRIPTION_SUCCESS:
        case VIP_CANCEL_SUBSCRIPTION_SUCCESS:
          draft.practice = record;
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case STORE_DEFENDANT_DETAILS:
          draft.defendantPracticeDetails = details;
          break;
        case LITIFY_INFO_DIALOG:
          draft.litifyInfoDialog = litifyAlert;
          break;
        case INITIAL_SUBSCRIPTION_SUCCESS:
        case UPDATE_PLAN_DETAILS_SUCCESS:
          draft.subscriptionSuccess = success;
          break;
        case MANAGE_ARCHIVE_CASE:
          draft.manageArchiveCase = status;
          break;
        case UPDATE_EXISTED_PASSWORD_DATA:
          draft.passwordData = passwordData;
          break;
        case CLEAR_EXISTED_PASSWORD_DATA:
          draft.passwordData = false;
          break;
        case ADD_MORE_LICENSE_VALIDATION_SUCCESS:
          draft.licenseDetails = record;
          draft.loading = false;
          draft.success = false;
          draft.progress = false;
          draft.error = false;
          break;
        case LOAD_PROPOUND_DOCUMENT_EDITOR_SUCCESS:
        case LOAD_DOCUMENT_EDITOR_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case DOCUMENT_EDITOR_STATUS:
          draft.documentStatus = status;
          break;
        case ESERVE_POPUP_STATUS:
          draft.eServePopup = status;
          break;
        case PROPOUND_QUESTIONS_NOT_FOUND:
        case QUESTIONS_NOT_FOUND:
          draft.questionsNotFound = content;
          draft.loading = false;
          draft.success = false;
          draft.progress = false;
          break;
        case UPDATE_QUESTIONS_ERROR:
        case DELETE_HASH_WITH_FILE_ERROR:
          draft.loading = false;
          draft.success = false;
          draft.progress = false;
          draft.error = error;
          break;
        case LOAD_FROGS_QUESTIONS_SUCCESS:
          draft.frogsQuestions = records;
          break;
        case SHOW_QUICK_CREATE_DESCRIPTION:
          draft.showQuickCreateDescription = showQuickCreateDescription;
          break;
        case ADD_FROGS_QUESTIONS_SUCCESS:
        case UPDATE_FROGS_FORM_SUCCESS:
          draft.frogsDynamicQuestions = record;
          break;
        case ADD_FROGS_QUESTIONS_ERROR:
          draft.error = error;
          break;
        case FETCH_DISCOVERY_HISTORY:
          draft.progress = true;
          break;
        case FETCH_DISCOVERY_HISTORY_SUCCESS:
          draft.discoveryHistory = discoveryHistory;
          draft.progress = false;
          break;
        case SYNC_RECORDS:
          draft.records = state.records.find(r => record.id === r.id) ? state.records.map((r) => record.id === r.id ? Object.assign({}, r, record) : Object.assign({}, r)) : state.records.concat([Object.assign({}, record)]);
          break;
      }
    });

  };
}

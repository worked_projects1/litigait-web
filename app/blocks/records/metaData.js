
/*
 *
 *   metaData
 *
 */

import lodash from 'lodash';
import { stateOptionsInFormat, signatureDecoding, propoundingEnabledStates } from 'utils/tools';
import { userBasedRespondingPlans } from 'utils/plans';

/**
 * 
 * @param {object} metaData 
 */
function mapCases(metaData, store) {
    const state = store && store.record && store.record.state && store.record.state.toUpperCase() || false;
    if (metaData) {
        const { clients = [], users = [], partiesWithClient = [], states = [], client_id, templateName = [], modifiedTemplateOptions = [], plans = [], currentUserInfo = {} } = metaData;
        const clientsOptions = (clients || []).map(c => Object.assign({}, { label: c.name, value: c.id }));
        const usersOptions = (users || []).filter(_ => _.role != 'paralegal' && _.name).map(c => Object.assign({}, c, { label: c.name, value: c.id }));
        const usersList = (users || []).filter(_ => _.role).map(c => Object.assign({}, c, { label: c.name ? c.name : `All anonymous users`, value: c.id, color: c.color_code }));
        const attoneySignatureOptions = (users || []).filter(_ => _.role != 'paralegal' && _.name).map(c => Object.assign({}, c, { label: c.name, value: c.id, signature: signatureDecoding(c.signature, c.id) }));
        const partiesOptions = (partiesWithClient || []).map(c => Object.assign({}, c, { label: c.name, value: c.id }));
        const clientFilterOptions = (clients || []).filter(_ => client_id && _.id.toString() !== client_id.toString()).map(c => Object.assign({}, { label: c.name, value: c.id }));
        const totalStateOptions = (states && lodash.sortBy(states, ['state_name']) || []).map(_ => Object.assign({}, { label: `${_.state_name} (${_.state_code.toUpperCase()})`, value: _.state_code.toUpperCase() }));
        const stateOptions = totalStateOptions && stateOptionsInFormat(totalStateOptions);
        let templateNameOptions = state && (templateName || []) && templateName.filter(_ => _.state && _.state.toUpperCase() === state);
        templateNameOptions = templateNameOptions && templateNameOptions.length > 0 && templateNameOptions.map(c => Object.assign({}, { label: c.file_name, value: c.id, state : c.state, disc_type : c.disc_type })) || [];
        
        let planOptions;
        if(currentUserInfo?.practiceDetails?.billing_type === 'limited_users_billing') {
            planOptions = (plans || []).filter(_ => userBasedRespondingPlans.includes(_.plan_type) && _.validity > 0 && _.plan_category === 'responding');
        } else {
            planOptions = (plans || []).filter(_ => !userBasedRespondingPlans.includes(_.plan_type) && _.validity > 0 && _.plan_category === 'responding');
        }
        planOptions = (planOptions || []).map(c => Object.assign({}, c, { label: c.name, value: c.plan_id }));

        return Object.assign({}, metaData, { clientsOptions, usersOptions, usersFilterOptions: ([{ label: `Assigned Attorney: All`, value: '' }]).concat(usersOptions), partiesOptions, clientFilterOptions, stateOptions, templateNameOptions, modifiedTemplateOptions, respondingPlansOptions: planOptions, usersList, objectionsFilterOptions: ([{ label: `All Objections`, value: '' }, { label: `Shortlisted Objections`, value: usersOptions && usersOptions.length > 0 && usersOptions[0].value }]), attoneySignatureOptions })
    }
    return metaData;
}

/**
 * 
 * @param {object} metaData 
 */
function mapUsers(metaData) {
    if (metaData) {
        const { practices = [] } = metaData;
        const practicesOptions = (practices || []).map(c => Object.assign({}, { label: c.name, value: c.id }));
        return Object.assign({}, metaData, { practicesOptions })
    }
    return metaData;
}

/**
 * 
 * @param {object} metaData 
 */
function mapMedicalSummary(metaData) {
    if (metaData) {
        const { users = [] } = metaData;
        const usersOptions = (users || []).filter(_ => _.role === 'medicalExpert').map(c => Object.assign({}, { label: c.name, value: c.id }));
        return Object.assign({}, metaData, { usersOptions })
    }
    return metaData;
}


/**
 * 
 * @param {object} metaData 
 */
 function mapOtherParties(metaData) {
    if (metaData) {
        const { cases = [] } = metaData;
        const casesOptions = (cases || []).map(c => Object.assign({}, { label: c.case_title, value: c.id }));
        const casesWithClientOptions = (cases || []).map(c => Object.assign({}, { label: `${c.case_title} (Client: ${c.client_name}, Case: ${c.case_number})`, value: c.id }));
        return Object.assign({}, metaData, { casesOptions, casesWithClientOptions })
    }
    return metaData;
}


/**
 * 
 * @param {object} metaData 
 */
function mapObjections(metaData) {
    if (metaData) {
        const { states = [], users = [], } = metaData;
        const statesSorted = (states && lodash.sortBy(states, ['state_name']) || []);
        const totalStateOptions = statesSorted.map(_ => Object.assign({}, { label: `${_.state_name} (${_.state_code.toUpperCase()})`, value: _.state_code.toUpperCase() }));
        const stateOptions = totalStateOptions && stateOptionsInFormat(totalStateOptions);
        const totalStateFilterOptions = statesSorted.map(c => Object.assign({}, { label: c.state_name, value: c.state_code.toUpperCase() }));
        const stateFilterOptions = totalStateFilterOptions && stateOptionsInFormat(totalStateFilterOptions);
        const usersOptions = (users || []).filter(_ => _.role != 'paralegal' && _.name).map(c => Object.assign({}, c, { label: c.name, value: c.id }));
        return Object.assign({}, metaData, { stateOptions, stateFilterOptions, usersOptions });
    }
    return metaData;
}


/**
 * 
 * @param {object} metaData 
 * @param {object} record 
 */
function mapPlans(metaData, record) {

    let planType = record && record.respondingSubscriptions && record.respondingSubscriptions.planDetails && record.respondingSubscriptions.planDetails.plan_type || false;

    if(metaData) {
        const { respondingPlans = [], propoundingPlans = [] } = metaData;
        if (planType) {
            const planOptions = ['monthly', 'yearly', 'responding_monthly_349', 'responding_yearly_3490', 'responding_monthly_495', 'responding_yearly_5100', 'users_responding_monthly_license_495', 'users_responding_yearly_license_5100'].includes(planType) && respondingPlans && Array.isArray(respondingPlans) && respondingPlans.length > 0 ? lodash.orderBy(respondingPlans.filter(e => e.active && e.plan_type !== "pay_as_you_go" && e.plan_type !== "free_trial" && e.plan_type !== "tek_as_you_go"), ['order'], ['asc']) : (planType === 'pay_as_you_go' || planType === 'tek_as_you_go') ? lodash.orderBy(respondingPlans.filter(e => e.plan_type !== "free_trial"), ['order'], ['asc']) : planType === "free_trial" ? lodash.orderBy(respondingPlans.filter(e => (e.plan_type !== "pay_as_you_go" && e.plan_type !== "tek_as_you_go")), ['order'], ['asc']) : respondingPlans;

            const respondingPlansOptions = planOptions && Array.isArray(planOptions) && planOptions.length > 0 && planOptions.filter(_ => _.plan_category === 'responding');

            const propoundingPlansOptions = propoundingPlans && Array.isArray(propoundingPlans) && propoundingPlans.length > 0 && propoundingPlans.filter(_ => _.plan_category === 'propounding');

            return Object.assign({}, { ...metaData }, { respondingPlansOptions, propoundingPlansOptions });
        } else {
            const planOptions = !planType && respondingPlans && Array.isArray(respondingPlans) && respondingPlans.length > 0 ? lodash.orderBy(respondingPlans.filter(e => (e.plan_type !== "pay_as_you_go" && e.plan_type !== "tek_as_you_go" && e.plan_type !== "free_trial")), ['order'], ['asc']) : respondingPlans;
            const respondingPlansOptions = planOptions && Array.isArray(planOptions) && planOptions.length > 0 && planOptions.filter(_ => _.plan_category === 'responding');
            const propoundingPlansOptions = propoundingPlans && Array.isArray(propoundingPlans) && propoundingPlans.length > 0 && propoundingPlans.filter(_ => _.plan_category === 'propounding');
            return Object.assign({}, { ...metaData }, { respondingPlansOptions, propoundingPlansOptions });
        }
    }
    return metaData;
}


/**
 * 
 * @param {object} metaData 
 */
function mapPropound(metaData) {
    if (metaData) {
        const { states = [] } = metaData;
        const statesSorted = (states && lodash.sortBy(states, ['state_name']) || []);
        const totalStateOptions = statesSorted.map(_ => Object.assign({}, { label: `${_.state_name} (${_.state_code.toUpperCase()})`, value: _.state_code.toUpperCase() }));
        const stateOptions = totalStateOptions && stateOptionsInFormat(totalStateOptions);
        const totalStateFilterOptions = statesSorted.map(c => Object.assign({}, { label: c.state_name, value: c.state_code.toUpperCase() }));
        const stateFilterOptions = totalStateFilterOptions && stateOptionsInFormat(totalStateFilterOptions);
        const totalPropoundStateOptions = statesSorted.map(_ => Object.assign({}, { label: `${_.state_name} (${_.state_code.toUpperCase()})`, value: _.state_code.toUpperCase(), disabled: _.state_code && propoundingEnabledStates.includes(_.state_code.toUpperCase()) ? false : true }));
        const propoundStateOptions = totalPropoundStateOptions && stateOptionsInFormat(totalPropoundStateOptions);
        return Object.assign({}, metaData, { stateOptions, stateFilterOptions, propoundStateOptions })
    }
    return metaData;
}


/**
 * 
 * @param {object} metaData 
 */
function mapClients(metaData) {
    if (metaData) {
        const { clients = [] } = metaData;
        const clientFilterOptions = (clients || []).map(c => Object.assign({}, { label: c.name, value: c.id }));
        return Object.assign({}, metaData, { clientFilterOptions })
    }
    return metaData;
}


/**
 * 
 * @param {object} metaData 
 */
function mapQuickCreate(metaData) {
    if (metaData) {
        const { states = [], users = [], plans = [], currentUserInfo = {} } = metaData;
        const statesSorted = (states && lodash.sortBy(states, ['state_name']) || []);
        const totalStateOptions = statesSorted.map(_ => Object.assign({}, { label: `${_.state_name} (${_.state_code.toUpperCase()})`, value: _.state_code.toUpperCase() }));
        const stateOptions = totalStateOptions && stateOptionsInFormat(totalStateOptions);
        const totalStateFilterOptions = statesSorted.map(c => Object.assign({}, { label: c.state_name, value: c.state_code.toUpperCase() }));
        const stateFilterOptions = totalStateFilterOptions && stateOptionsInFormat(totalStateFilterOptions);
        const usersOptions = (users || []).filter(_ => _.role != 'paralegal' && _.name).map(c => Object.assign({}, c, { label: c.name, value: c.id }));

        let planOptions;
        if(currentUserInfo?.practiceDetails?.billing_type === 'limited_users_billing') {
            planOptions = (plans || []).filter(_ => userBasedRespondingPlans.includes(_.plan_type) && _.validity > 0 && _.plan_category === 'responding');
        } else {
            planOptions = (plans || []).filter(_ => !userBasedRespondingPlans.includes(_.plan_type) && _.validity > 0 && _.plan_category === 'responding');
        }
        planOptions = (planOptions || []).map(c => Object.assign({}, c, { label: c.name, value: c.plan_id }));

        return Object.assign({}, metaData, { stateOptions, stateFilterOptions, usersOptions, respondingPlansOptions: planOptions });
    }
    return metaData;
}


/**
 * @param {object} metaData 
 * @param {string} name 
 */
export default function mapRecordsMetaData(metaData, name, state) {
    switch (name) {
        case 'cases':
            return mapCases(metaData, state);
        case 'rest/users':
        case 'rest/practice/users':
        case 'globalSettings':
            return mapUsers(metaData);
        case 'rest/medical-history-summery':
            return mapMedicalSummary(metaData);
        case 'rest/other-parties':
            return mapOtherParties(metaData);
        case 'rest/customer-objections':
        case 'federal/customer-objections':
        case 'rest/admin-objections':
            return mapObjections(metaData);
        case 'settings':
            return mapPlans(metaData, state);
        case 'rest/propound':
        case 'rest/practice-template':
        case 'rest/practice-propound-template':
            return mapPropound(metaData, state);
        case 'clients':
            return mapClients(metaData);
        case 'rest/document-extraction-progress':
        case 'rest/document-extraction-support':
            return mapQuickCreate(metaData);
        default:
            return metaData;
    }
}
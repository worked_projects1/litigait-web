/*
 *
 *  Filevine remotes
 *
 */

import api, { filevineApi } from 'utils/api';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {


    /**
     * @param {object} secret 
     */
    function filevineSession(secret) {
        return filevineApi.post(`/session`, secret).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {number} offset 
     * @param {number} limit 
     */

    function filevineContacts(offset, limit) {
        return filevineApi.get(`/core/contacts?offset=${offset}&limit=${limit}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {number} offset 
     * @param {number} limit 
     */
      
    function filevineProject(offset, limit) {
        return filevineApi.get(`/core/projects?offset=${offset}&limit=${limit}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * 
     * @param {number} projectId 
     * @returns 
     */
    function filevineProjectCaseNumber(projectId) {
        return filevineApi.get(`/core/projects/${projectId}/vitals`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function creatFilevineDocumentUrl(record) {
        return filevineApi.post('/core/documents', record).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function addDocumentToProject(record) {
        return filevineApi.post(`/core/projects/${record.projectId}/documents/${record.documentId}`, record.body).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {array} records 
     * @returns 
     */
    function filevineSyncData(records) {
        return api.post('/rest/integrations/filevine/create-client-and-case', records).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function storeFilevineSecret(record) {
        return api.post('/rest/integrations/filevine-key-details', record).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function deleteFilevineSecret(record) {
        return api.delete(`/rest/integrations/filevine/${record.id}`).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function uploadFilevineDocument(record) {
        return api.post('/rest/integrations/filevine/file-upload', record).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function getDocumentSize(record) {
        return api.post('/rest/integrations/filevine/s3-file-size', record).then(response => response.data).catch(error => Promise.reject(error))
    }

    /**
     * @param {object} record
     */
    function updateUserRecord(record){
        return api.put(`/rest/user/update-details`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record
     * @returns
     */
    function syncFilevineRecords(record) {
        return api.put(`/rest/integrations/filevine/update-client-and-case`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        filevineSession,
        filevineContacts,
        filevineProject,
        filevineProjectCaseNumber,
        creatFilevineDocumentUrl,
        addDocumentToProject,
        filevineSyncData,
        storeFilevineSecret,
        deleteFilevineSecret,
        uploadFilevineDocument,
        getDocumentSize,
        updateUserRecord,
        syncFilevineRecords
    }
}


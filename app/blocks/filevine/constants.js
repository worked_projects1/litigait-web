/*
 *
 * Filevine constants
 *
 */


/**
 * 
 * @param {string} name 
 * @returns 
 */
 export default function (name) {
    const url = `lg/${name}`;
    
    return {
        FILEVINE_SESSION: `${url}/session/FILEVINE_SESSION`,
        FILEVINE_SESSION_SUCCESS: `${url}/session/FILEVINE_SESSION_SUCCESS`,
        FILEVINE_SESSION_ERROR: `${url}/session/FILEVINE_SESSION_ERROR`,
        FILEVINE_LOAD_RECORDS: `${url}/FILEVINE_LOAD_RECORDS`,
        FILEVINE_LOAD_RECORDS_SUCCESS: `${url}/FILEVINE_LOAD_RECORDS_SUCCESS`,
        FILEVINE_LOAD_RECORDS_ERROR: `${url}/FILEVINE_LOAD_RECORDS_ERROR`,
        FILEVINE_SYNC_DATA: `${url}/FILEVINE_SYNC_DATA`,
        FILEVINE_SYNC_DATA_SUCCESS: `${url}/FILEVINE_SYNC_DATA_SUCCESS`,
        FILEVINE_SYNC_DATA_ERROR: `${url}/FILEVINE_SYNC_DATA_ERROR`,
        AUTO_REFRESH_SESSION: `${url}/AUTO_REFRESH_SESSION`,
        AUTO_REFRESH_SESSION_ERROR: `${url}/AUTO_REFRESH_SESSION_ERROR`,
        AUTO_REFRESH_SESSION_SUCCESS: `${url}/AUTO_REFRESH_SESSION_SUCCESS`,
        FILEVINE_SYNC: `${url}/FILEVINE_SYNC`,
        FILEVINE_SYNC_ERROR: `${url}/FILEVINE_SYNC_ERROR`,
        FILEVINE_SYNC_SUCCESS: `${url}/FILEVINE_SYNC_SUCCESS`,
        SET_FILEVINE_HEADERS_DATA: `${url}/SET_FILEVINE_HEADERS_DATA`,
        REMOVE_FILEVINE_SECRET: `${url}/REMOVE_FILEVINE_SECRET`,
        REMOVE_FILEVINE_SECRET_SUCCESS: `${url}/REMOVE_FILEVINE_SECRET_SUCCESS`,
        REMOVE_FILEVINE_SECRET_ERROR: `${url}/REMOVE_FILEVINE_SECRET_ERROR`,
        VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION: `${url}/VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION`,
        VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_ERROR: `${url}/VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_ERROR`,
        VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_SUCCESS: `${url}/VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_SUCCESS`,
        UPLOAD_FILEVINE_DOCUMENT: `${url}/UPLOAD_FILEVINE_DOCUMENT`,
        UPLOAD_FILEVINE_DOCUMENT_SUCCESS: `${url}/UPLOAD_FILEVINE_DOCUMENT_SUCCESS`,
        UPLOAD_FILEVINE_DOCUMENT_ERROR: `${url}/UPLOAD_FILEVINE_DOCUMENT_ERROR`,
        CREATE_NEW_FILEVINE_SESSION: `${url}/CREATE_NEW_FILEVINE_SESSION`,
        CREATE_NEW_FILEVINE_SESSION_ERROR: `${url}/CREATE_NEW_FILEVINE_SESSION_ERROR`,
        CREATE_NEW_FILEVINE_SESSION_SUCCESS: `${url}/CREATE_NEW_FILEVINE_SESSION_SUCCESS`,
        FILEVINE_UPDATE_USER_DETAILS: `${url}/FILEVINE_UPDATE_USER_DETAILS`,
        FILEVINE_UPDATE_USER_DETAILS_ERROR: `${url}/FILEVINE_UPDATE_USER_DETAILS_ERROR`,
        FILEVINE_UPDATE_USER_DETAILS_SUCCESS: `${url}/FILEVINE_UPDATE_USER_DETAILS_SUCCESS`,
    }
}


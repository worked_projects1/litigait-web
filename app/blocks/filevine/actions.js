/*
 *
 * Filevine actions
 *
 */



/**
 * 
 * @param {object} constants 
 * @returns 
 */
 export default function (constants) {

  const {
    FILEVINE_SESSION,
    FILEVINE_SESSION_ERROR,
    FILEVINE_SESSION_SUCCESS,
    FILEVINE_LOAD_RECORDS, 
    FILEVINE_LOAD_RECORDS_SUCCESS, 
    FILEVINE_LOAD_RECORDS_ERROR,
    FILEVINE_SYNC_DATA,
    FILEVINE_SYNC_DATA_ERROR,
    FILEVINE_SYNC_DATA_SUCCESS,
    AUTO_REFRESH_SESSION,
    AUTO_REFRESH_SESSION_ERROR,
    AUTO_REFRESH_SESSION_SUCCESS,
    FILEVINE_SYNC,
    FILEVINE_SYNC_ERROR,
    FILEVINE_SYNC_SUCCESS,
    SET_FILEVINE_HEADERS_DATA,
    REMOVE_FILEVINE_SECRET,
    REMOVE_FILEVINE_SECRET_ERROR,
    REMOVE_FILEVINE_SECRET_SUCCESS,
    VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION,
    VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_ERROR,
    VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_SUCCESS,
    UPLOAD_FILEVINE_DOCUMENT,
    UPLOAD_FILEVINE_DOCUMENT_SUCCESS,
    UPLOAD_FILEVINE_DOCUMENT_ERROR,
    CREATE_NEW_FILEVINE_SESSION,
    CREATE_NEW_FILEVINE_SESSION_ERROR,
    CREATE_NEW_FILEVINE_SESSION_SUCCESS,
    FILEVINE_UPDATE_USER_DETAILS,
    FILEVINE_UPDATE_USER_DETAILS_ERROR,
    FILEVINE_UPDATE_USER_DETAILS_SUCCESS
  } = constants;

  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} setFilevinePopup 
   * @param {function} setStepper 
   * @param {function} setShowFileVineForm 
   */
  function filevineSession(record, form, setFilevinePopup, setStepper, setShowFileVineForm) {
    return {
      type: FILEVINE_SESSION,
      record,
      form,
      setFilevinePopup,
      setStepper,
      setShowFileVineForm
    };
  }

  /**
   * @param {string} success 
   */
  function filevineSessionSuccess(success) {
    return {
      type: FILEVINE_SESSION_SUCCESS,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function filevineSessionError(error) {
    return {
      type: FILEVINE_SESSION_ERROR,
      error,
    };
  }
  
  /**
   * @param {boolean} load
   * @param {function} setFilevinePopup
   * @param {function} setStepper
   */
  function filevineLoadRecords(load, setFilevinePopup, setStepper) {
    return {
      type: FILEVINE_LOAD_RECORDS,
      load,
      setFilevinePopup,
      setStepper
    };
  }
  
  /**
   * @param {object} records  
   * @param {string} success 
   */
  function filevineLoadRecordsSuccess(records) {
    return {
      type: FILEVINE_LOAD_RECORDS_SUCCESS,
      records,
    };
  }
  
  /**
   * @param {string} error 
   */
  function filevineLoadRecordsError(error) {
    return {
      type: FILEVINE_LOAD_RECORDS_ERROR,
      error,
    };
  }
  
  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} setFilevinePopup 
   * @param {function} setShowUserForm 
   * @returns 
   */
  function filevineSyncData(record, form, setFilevinePopup, setShowUserForm) {
    return {
      type: FILEVINE_SYNC_DATA,
      record,
      form,
      setFilevinePopup,
      setShowUserForm
    }
  }
  
  /**
   * 
   * @param {array} records 
   * @returns 
   */
  function filevineSyncDataSuccess(records) {
    return {
      type: FILEVINE_SYNC_DATA_SUCCESS,
      records
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function filevineSyncDataError(error) {
    return {
      type: FILEVINE_SYNC_DATA_ERROR,
      error
    }
  }

  /**
   * 
   * @param {boolean} load 
   * @param {function} setFilevinePopup 
   * @param {function} openModalPopup 
   * @param {function} setStepper 
   * @param {function} setFilvineLoader
   * @returns 
   */
  function autoRefreshSesssion(load, setFilevinePopup, openModalPopup, setStepper, setFilvineLoader) {
    return {
      type: AUTO_REFRESH_SESSION,
      load,
      setFilevinePopup,
      openModalPopup,
      setStepper,
      setFilvineLoader
    }
  }


  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function autoRefreshSesssionError(error) {
    return {
      type: AUTO_REFRESH_SESSION_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function autoRefreshSesssionSuccess(success) {
    return {
      type: AUTO_REFRESH_SESSION_SUCCESS,
      success
    }
  }


  /**
   * @param {boolean} load
   * @param {function} setFilevinePopup 
   * @param {function} openModalPopup 
   * @param {function} setStepper
   * @param {function} setFilevineLoader
   * @returns 
   */
  function filevineSync(load, setFilevinePopup, openModalPopup, setStepper, setFilevineLoader) {
    return {
      type: FILEVINE_SYNC,
      load,
      setFilevinePopup,
      openModalPopup,
      setStepper,
      setFilevineLoader
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function filevineSyncError(error) {
    return {
      type: FILEVINE_SYNC_ERROR,
      error
    }
  }
  
  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function filevineSyncSuccess(success) {
    return {
      type: FILEVINE_SYNC_SUCCESS,
      success
    }
  }


  /**
   * @param {object} record 
   */
  function setFilevineHeadersData(record) {
    return {
      type: SET_FILEVINE_HEADERS_DATA,
      record
    };
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function removeFilevineSecret(record) {
    return {
      type: REMOVE_FILEVINE_SECRET,
      record
    }
  }
  
  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function removeFilevineSecretSuccess(success) {
    return {
      type: REMOVE_FILEVINE_SECRET_SUCCESS,
      success
    }
  }
  
  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function removeFilevineSecretError(error) {
    return {
      type: REMOVE_FILEVINE_SECRET_ERROR,
      error
    }
  }


  /**
   * 
   * @param {object} record 
   * @param {function} setShowFileVineForm 
   * @param {function} setDocument 
   * @returns 
   */
  function verifyFilevineUploadDocumentSession(record, setShowFileVineForm, setDocument) {
    return {
      type: VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION,
      record,
      setShowFileVineForm,
      setDocument
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function verifyFilevineUploadDocumentSessionError(error) {
    return {
      type: VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function verifyFilevineUploadDocumentSessionSuccess(success) {
    return {
      type: VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {boolean} pageLoading 
   * @returns 
   */
  function uploadFilevineDocument(record, pageLoading) {
    return {
      type: UPLOAD_FILEVINE_DOCUMENT,
      record,
      pageLoading
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function uploadFilevineDocumentError(error) {
    return {
      type: UPLOAD_FILEVINE_DOCUMENT_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function uploadFilevineDocumentSuccess(success) {
    return {
      type: UPLOAD_FILEVINE_DOCUMENT_SUCCESS,
      success
    }
  } 
  
  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {object} editFormRecord
   * @param {string} formName
   * @returns 
   */
  function createNewFilevineSession(record, form, editFormRecord, formName) {
    return {
      type: CREATE_NEW_FILEVINE_SESSION,
      record,
      form,
      editFormRecord,
      formName
    }
  }


  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function createNewFilevineSessionError(error) {
    return {
      type: CREATE_NEW_FILEVINE_SESSION_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function createNewFilevineSessionSuccess(success) {
    return {
      type: CREATE_NEW_FILEVINE_SESSION_SUCCESS,
      success
    }
  }


  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {object} integration 
   * @returns 
   */
  function filvineUpdateUserDetails(record, form, integration) {
    return {
      type:  FILEVINE_UPDATE_USER_DETAILS,
      record,
      form,
      integration
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function filvineUpdateUserDetailsSuccess(success) {
    return {
      type: FILEVINE_UPDATE_USER_DETAILS_SUCCESS,
      success
    }
  }
  
  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function filvineUpdateUserDetailsError(error) {
    return {
      type: FILEVINE_UPDATE_USER_DETAILS_ERROR,
      error
    }
  }


  return { 
    filevineSession,
    filevineSessionSuccess,
    filevineSessionError,       
    filevineLoadRecords,
    filevineLoadRecordsSuccess,
    filevineLoadRecordsError,
    filevineSyncData,
    filevineSyncDataError,
    filevineSyncDataSuccess,
    autoRefreshSesssion,
    autoRefreshSesssionError,
    autoRefreshSesssionSuccess,
    filevineSync,
    filevineSyncError,
    filevineSyncSuccess,
    setFilevineHeadersData,
    removeFilevineSecret,
    removeFilevineSecretSuccess,
    removeFilevineSecretError,
    verifyFilevineUploadDocumentSession,
    verifyFilevineUploadDocumentSessionError,
    verifyFilevineUploadDocumentSessionSuccess,
    uploadFilevineDocument,
    uploadFilevineDocumentError,
    uploadFilevineDocumentSuccess,
    createNewFilevineSession,
    createNewFilevineSessionError,
    createNewFilevineSessionSuccess,
    filvineUpdateUserDetails,
    filvineUpdateUserDetailsError,
    filvineUpdateUserDetailsSuccess
  }
}

/*
 *
 *  Filevine sagas
 *
 */


import { push } from 'react-router-redux';
import { call, put, all, delay, takeLatest, select } from 'redux-saga/effects';
import store2 from 'store2';
import { destroy, startSubmit, stopSubmit } from 'redux-form/immutable';
import { setFilevineAuthToken, setFilevineBaseUrl } from 'utils/api';
import { selectUser } from 'blocks/session/selectors';
const CryptoJS = require("crypto-js");
const base64 = require('base-64');
import { verifySession as verifySessionAction, updateKey } from 'blocks/session/actions';
import { verifySession as verifySessionApi } from 'blocks/session/remotes';
import appRemotes from '../records/remotes';
import { selectForm } from 'blocks/session/selectors';
import { decryptApiKeyAndSecret } from 'utils/tools';


import {
  DEFAULT_FILEVINE_SESSION_ERROR,
  DEFAULT_FILEVINE_RECORDS_ERROR,
  DEFAULT_FILEVINE_URL_ERROR
} from 'utils/errors';


import records from 'blocks/records';

const casesSaga = records('cases');
const { actions: casesActions, selectors: casesSelectors } = casesSaga;

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    FILEVINE_LOAD_RECORDS, 
    FILEVINE_SESSION,
    FILEVINE_SYNC_DATA,
    AUTO_REFRESH_SESSION,
    FILEVINE_SYNC,
    REMOVE_FILEVINE_SECRET,
    UPLOAD_FILEVINE_DOCUMENT,
    VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION,
    CREATE_NEW_FILEVINE_SESSION,
    FILEVINE_UPDATE_USER_DETAILS
  } = constants;


  const {
    filevineLoadRecords: filevineLoadRecordsActions,
    filevineLoadRecordsSuccess,
    filevineLoadRecordsError,
    filevineSessionSuccess,
    filevineSessionError,
    filevineSyncDataSuccess,
    filevineSyncDataError,
    autoRefreshSesssionError,
    autoRefreshSesssion: autoRefreshSesssionActions,
    removeFilevineSecretSuccess,
    removeFilevineSecretError,
    uploadFilevineDocument: uploadFilevineDocumentActions,
    uploadFilevineDocumentError,
    uploadFilevineDocumentSuccess,
    verifyFilevineUploadDocumentSessionError,
    verifyFilevineUploadDocumentSessionSuccess,
    createNewFilevineSessionError,
    createNewFilevineSessionSuccess,
    filevineSyncData: filevineSyncDataAction,
    filvineUpdateUserDetailsError,
    filevineSyncError
  } = actions;


  const {
    filevineContacts, 
    filevineProject,
    filevineSession,
    filevineSyncData,
    storeFilevineSecret,
    deleteFilevineSecret,
    creatFilevineDocumentUrl,
    uploadFilevineDocument,
    addDocumentToProject,
    getDocumentSize,
    updateUserRecord,
    syncFilevineRecords
  } = remotes;


  function* createFilevineSessionSaga() {
    yield takeLatest(FILEVINE_SESSION, function* updater({ record, form, setFilevinePopup, setStepper, setShowFileVineForm }) {
      let apiKey = record && record.filevine_key || false;
      let apiSecret = record && record.filevine_secret || false;
      let apiUrl = record && record.filevine_api || false;
      if(record && apiKey && apiSecret && apiUrl) {
        yield put(startSubmit(form));
        setFilevineBaseUrl(apiUrl);
        try {
          const user_info = yield select(selectUser());
          const practiceId = user_info && user_info.practice_id;
          const timestamp = new Date().toISOString();
      
          // Ordering is important!
          const data = [apiKey, timestamp, apiSecret].join('/');
      
          let hash = CryptoJS.MD5(data).toString('');
  
          const result = yield call(filevineSession, Object.assign({}, { mode: "key", apiKey: `${apiKey}`, apiHash: `${hash}`, apiTimestamp: `${timestamp}`}));

          if(result) {
            const apiKeyEncoded = base64.encode(apiKey);
            const encryptApiKey = CryptoJS.AES.encrypt(apiKeyEncoded, practiceId).toString();
            const apiSecretEncoded = base64.encode(apiSecret);
            const encryptSecret = CryptoJS.AES.encrypt(apiSecretEncoded, practiceId).toString();

            const encodedRefreshToken = base64.encode(result.refreshToken);
            const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

            store2.set('filevine', result);
            setFilevineAuthToken(result);
            if (record.is_store_filevine_secret) {
              yield call(storeFilevineSecret, Object.assign({}, { filevine_key: encryptApiKey, filevine_secret: encryptSecret, filevine_user_id: result.userId, filevine_org_id: result.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: hash, filevine_timeStamp: timestamp, filevine_baseurl : apiUrl }));
            }
            yield put(filevineSessionSuccess('Filevine session created successfully'));
            yield delay(1000);
            if(setShowFileVineForm) {
              yield call(setShowFileVineForm, false);
            }
            yield put(stopSubmit(form));
            yield put(filevineLoadRecordsActions(true, setFilevinePopup, setStepper));
          } else {
            yield put(filevineSessionError(DEFAULT_FILEVINE_SESSION_ERROR));
            yield put(stopSubmit(form));
          }
          
        } catch(error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? DEFAULT_FILEVINE_SESSION_ERROR :  DEFAULT_FILEVINE_URL_ERROR;
          yield put(filevineSessionError(Err));
          yield put(stopSubmit(form));
        } finally {
          yield put(destroy(form));
        }
      }
    })
  }

  function* filevineLoadRecordsSaga() {
    yield takeLatest(FILEVINE_LOAD_RECORDS, function* updater({ load, setFilevinePopup, setStepper }) {
      if (load) {
        try {
          yield put(updateKey(0));
          if(setStepper) {
            yield call(setStepper, 'filevine');
          }
          const secret = store2.get('filevine');
          setFilevineAuthToken(secret);

          const casesRemotes = yield call(appRemotes, 'rest/cases');
          const cases = yield call(casesRemotes.loadRecords);
          const filter_case_records = cases && cases.filter(_ => _.integration_case_id) || [];
          const filter_case = filter_case_records && filter_case_records.length > 0 && filter_case_records.map(el => el.integration_case_id);

          let clients = [];
          let clients_cases = [];
          let finalRecord = [];
          let currentIndex = 0;
          let totalSize = 1000;
          delay(500);

          while(true) {
            try {
              let firstIndex = currentIndex  * totalSize;
              let lastIndex = firstIndex + totalSize;
              const contacts = yield call(filevineContacts, firstIndex, lastIndex);
              if(contacts && contacts.items && contacts.items.length > 0) {
                contacts.items && contacts.items.filter((el,i) => (el.personTypes && el.personTypes.includes("Client") && clients.push(el)));

                if(!contacts.hasMore) {
                  currentIndex = 0;
                  totalSize = 1000;
                  break;
                } else {
                  currentIndex++;
                }
              }
              yield put(updateKey(1));
              yield delay(500);
            } catch (error) {
              yield put(filevineLoadRecordsError('Failed to fetching client and cases'));
              return;
            }
            
          }

          //Get cases data from filevine
          while(true) {
            try {
              let firstIndex1 = currentIndex  * totalSize;
              let lastIndex2 = firstIndex1 + totalSize;
              const projectCases = yield call(filevineProject, firstIndex1, lastIndex2);

              if(projectCases && projectCases.items && projectCases.items.length > 0) {
                const activeCases = projectCases.items.reduce((acc, el) => {
                  if(el && !el.isArchived) {
                    acc.push(el)
                  }
                  return acc;
                }, [])

                if(activeCases && activeCases.length > 0) {
                  clients_cases.push(...activeCases)
                }

                if(!projectCases.hasMore) {
                  currentIndex = 0;
                  totalSize = 1000;
                  break;
                }else {
                  currentIndex++;
                }
              }
              yield put(updateKey(2));
              yield delay(500);
            } catch (error) {
              yield put(filevineLoadRecordsError('Failed to fetching client and cases'));
              return;
            }
            
          }

          if(clients.length > 0 && clients_cases.length > 0) {
            for (let i = 0; i < clients_cases.length; ++i) {
              let updatedItem = clients_cases[i];
              let newArray = [];
              for (let j = 0; j < clients.length; ++j) {
                var origItem = clients[j];
                const personId = origItem && origItem.personId && origItem.personId.native;
                const clientId = updatedItem && updatedItem.clientId && updatedItem.clientId.native;
                if (personId == clientId) {
                  newArray.push(Object.assign({}, updatedItem));
                  break;    
                }
              }
              let result = Object.assign({}, origItem, { cases : newArray });
              finalRecord.push(result);
            }
          }

          if(finalRecord.length > 0) {
            let submitRecord = finalRecord.map((el,i) => {
              delete el.uniqueId;
              let case_title = el && el.cases && el.cases[0] && el.cases[0].projectName;
              let project_id = el && el.cases && el.cases[0] && el.cases[0].projectId && el.cases[0].projectId.native;

              return Object.assign({}, el, { name: el.fullName, email: el.emails.length > 0 ? el.emails[0].address : '', phone: el.phones.length > 0 ? el.phones[0].rawNumber : '', dob: el.birthDate || '', case_title: case_title, id: i+1, project_id: project_id })
            });
            
            submitRecord = submitRecord.filter(_ => _.cases && _.cases.length > 0);
            const finalSubmitRecord = filter_case && filter_case.length > 0 ? submitRecord.reduce((acc, el) => {
              let res = filter_case.find(_ => _ == el.project_id);
              if (!res) {
                acc.push(el);
              }
              return acc;
            }, []) : submitRecord;

            const syncRecord = filter_case && filter_case.length > 0 && submitRecord.reduce((acc, el) => {
              let res = filter_case.find(_ => _ == el.project_id);
              if (res) {
                acc.push(el);
              }
              return acc;
            }, []);

            if (syncRecord && Array.isArray(syncRecord) && syncRecord.length > 0) {
              let loop = 0;
              let currentIndex = 1;
              let totalSize = 10;
              const totalSlice = syncRecord.length % totalSize == 0 ? Math.floor(syncRecord.length / totalSize) : Math.floor((syncRecord.length / totalSize) + 1);

              while (loop < totalSlice) {
                const firstIndex = (currentIndex - 1) * totalSize;
                const lastIndex = firstIndex + totalSize;
                const saveRecords = syncRecord.slice(firstIndex, lastIndex);
                const result = yield call(syncFilevineRecords, saveRecords);

                if (result) {
                  loop++;
                  currentIndex++;
                } else {
                  yield put(filevineLoadRecordsError('Failed to save records'));
                }
              }
            }

            if(finalSubmitRecord && finalSubmitRecord.length > 0) {
              yield put(filevineLoadRecordsSuccess(finalSubmitRecord));
              yield put(updateKey(3));
              yield call(setFilevinePopup, true);
            } else {
              yield put(filevineLoadRecordsSuccess([]));
              yield put(filevineLoadRecordsError(DEFAULT_FILEVINE_RECORDS_ERROR));
              const user = yield select(selectUser());
              if(user && !user.filevineDetails) {
                yield delay(500);
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              }
            }
          }
        } 
        catch (error) {
          yield put(filevineLoadRecordsError('Failed to fetching client and cases'));
        } finally {
          if (setStepper) {
            yield call(setStepper, false);
          }
        }
      }
    })
  }


  function* filevineSyncDataSaga() {
    yield takeLatest(FILEVINE_SYNC_DATA, function* updater({ record, form, setFilevinePopup, setShowUserForm }) {
      yield put(startSubmit(form));
      if(record) {
        let apiUrl = record && record.filevine_api || false;
        setFilevineBaseUrl(apiUrl);
        try {
          const user_info = yield call(verifySessionApi);
          if(user_info && (user_info?.role == 'lawyer' && !user_info.state_bar_number || !user_info.user_name) && setShowUserForm){
            yield call(setShowUserForm, true);
            yield put(stopSubmit(form));
          } else {
            try {
              if (record && Array.isArray(record) && record.length > 0) {
                let loop = 0;
                let currentIndex = 1;
                let totalSize = 50;
                const totalSlice = record.length % totalSize == 0 ? Math.floor(record.length / totalSize) : Math.floor((record.length / totalSize) + 1);
          
                while (loop < totalSlice) {
                  const firstIndex = (currentIndex - 1) * totalSize;
                  const lastIndex = firstIndex + totalSize;
                  const saveRecords = record.slice(firstIndex, lastIndex);
                  const result = yield call(filevineSyncData, saveRecords);
    
                  if (result) {
                    loop++;
                    currentIndex++;
                  } else {
                    yield put(filevineSyncDataError(DEFAULT_FILEVINE_SESSION_ERROR));
                  }
                }
                yield put(stopSubmit(form));
                yield put(filevineSyncDataSuccess('Filevine data sync successfully'));
                yield delay(1000);
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              } else {
                yield put(filevineSyncDataError(DEFAULT_FILEVINE_SESSION_ERROR));
              }
            } catch(error) {
              yield put(filevineSyncDataError(DEFAULT_FILEVINE_SESSION_ERROR));
              yield put(stopSubmit(form));
            } finally {
              yield call(setFilevinePopup, false);
              yield put(destroy(form));
            }
          }
        } catch(error) {
          yield put(filevineSyncDataError(DEFAULT_FILEVINE_SESSION_ERROR));
          yield put(stopSubmit(form));
        }
      }
    })
  }


  function* autoSesssionRefreshSaga() {
    yield takeLatest(AUTO_REFRESH_SESSION, function* updater({ load, setFilevinePopup, openModalPopup, setStepper, setFilvineLoader }) {
      if(load) {
        try {
          const user_info = yield select(selectUser());
          const practiceId = user_info && user_info.practice_id || false;
          const filevineDetails = user_info && user_info.filevineDetails || false;
          const apiKey = filevineDetails && filevineDetails.filevine_key || false;
          const apiSecret = filevineDetails && filevineDetails.filevine_secret || false;
          const apiUrl = filevineDetails && filevineDetails.filevine_baseurl || false;

          if(apiKey && apiSecret && apiUrl) {
            setFilevineBaseUrl(apiUrl);
            // Decryption
            const { decryptApiKey, decryptApiSecret } = decryptApiKeyAndSecret(apiKey, apiSecret, user_info);
            // TimeStamp
            const currentTimestamp = new Date().toISOString();
            // Ordering is important!
            const data = [decryptApiKey, currentTimestamp, decryptApiSecret].join('/');
            // Hash
            let currentHash = CryptoJS.MD5(data).toString('');
    
            const result = yield call(filevineSession, Object.assign({}, { mode: "key", apiKey: `${decryptApiKey}`, apiHash: `${currentHash}`, apiTimestamp: `${currentTimestamp}`}));

            if(result) {
              const encodedRefreshToken = base64.encode(result.refreshToken);
              const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

              store2.set('filevine', result);
              setFilevineAuthToken(result);
              yield call(storeFilevineSecret, Object.assign({}, { filevine_key: apiKey, filevine_secret: apiSecret, filevine_user_id: result.userId, filevine_org_id: result.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: currentHash, filevine_timeStamp: currentTimestamp, filevine_baseurl : apiUrl }));
              if(setFilevinePopup) {
                yield call(setFilvineLoader, false);
                yield put(filevineLoadRecordsActions(true, setFilevinePopup, setStepper));
              }
            } else {
              yield put(filevineSessionError(DEFAULT_FILEVINE_SESSION_ERROR));
              yield put(stopSubmit(form));
            }
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Filevine session expired.';
          if(Err === 'Invalid') {
            if(openModalPopup) {
              yield call(openModalPopup, true);
            }
          }
          yield put(autoRefreshSesssionError('Filevine session expired.'));
        } finally {
          if(setFilevinePopup) {
            yield call(setFilvineLoader, false);
          }
        }
      }
    })
  }


  function* filevineSyncSaga() {
    yield takeLatest(FILEVINE_SYNC, function* updater({ load, setFilevinePopup, openModalPopup, setStepper, setFilevineLoader }) {
      if(load) {
        try {
          const user_info = yield select(selectUser());
          const filevineDetails = user_info && user_info.filevineDetails || false;
          const apiKey = filevineDetails && filevineDetails.filevine_key || false;
          const apiSecret = filevineDetails && filevineDetails.filevine_secret || false;

          if(apiKey && apiSecret) {
              yield put(autoRefreshSesssionActions(true, setFilevinePopup, openModalPopup, setStepper, setFilevineLoader));
          } else {
            if(openModalPopup) {
              yield call(openModalPopup, true);
            }
          }
        } catch (error) {
          yield put(filevineSyncError('Failed to sync data with filevine.'));
        }
      }
    })
  }

  function* removeFilevineSecretSaga() {
    yield takeLatest(REMOVE_FILEVINE_SECRET, function* updater({ record }) {
      if(record) {
        try {
          const result = yield call(deleteFilevineSecret, record)

          if(result) {
            yield put(removeFilevineSecretSuccess('Filevine secret remove successfully'));
            yield delay(500);
            const secret = store2.get('secret');
            yield put(verifySessionAction(secret));
          }
        } catch (error) {
          yield put(removeFilevineSecretError('Failed to remove filevine secret.'));
        }
      }
    })
  }

  function* verifyFilevineUploadDocumentSessionSaga() {
    yield takeLatest(VERIFY_FILEVINE_UPLOAD_DOCUMENT_SESSION, function* updater({ record, setShowFileVineForm, setDocument }) {
      if(record) {
        try {
          const user_info = yield select(selectUser());
          const practiceId = user_info && user_info.practice_id || false;
          const filevineDetails = user_info && user_info.filevineDetails || false;
          const apiKey = filevineDetails && filevineDetails.filevine_key || false;
          const apiSecret = filevineDetails && filevineDetails.filevine_secret || false;
          const apiUrl = filevineDetails && filevineDetails.filevine_baseurl || false;

          if(apiKey && apiSecret && apiUrl) {
            setFilevineBaseUrl(apiUrl);
            // Decryption
            const { decryptApiKey, decryptApiSecret } = decryptApiKeyAndSecret(apiKey, apiSecret, user_info);
            
            // TimeStamp
            const currentTimestamp = new Date().toISOString();
          
            // Ordering is important!
            const data = [decryptApiKey, currentTimestamp, decryptApiSecret].join('/');
          
            //Hash
            let currentHash = CryptoJS.MD5(data).toString('');
      
            const result = yield call(filevineSession, Object.assign({}, { mode: "key", apiKey: `${decryptApiKey}`, apiHash: `${currentHash}`, apiTimestamp: `${currentTimestamp}`}));

            if(result) {
              const encodedRefreshToken = base64.encode(result.refreshToken);
              const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

              store2.set('filevine', result);
              setFilevineAuthToken(result);
              yield call(storeFilevineSecret, Object.assign({}, { filevine_key: apiKey, filevine_secret: apiSecret, filevine_user_id: result.userId, filevine_org_id: result.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: currentHash, filevine_timeStamp: currentTimestamp, filevine_baseurl : apiUrl }));
              yield put(uploadFilevineDocumentActions(record, false));
            }
          } else {
            if(setShowFileVineForm && setDocument) {
              yield call(setShowFileVineForm, true);
              yield call(setDocument, false);
            }
          }
        } catch (error) {
          yield put(verifyFilevineUploadDocumentSessionError('Filevine Session Expired'));
        }
      }
    })
  }

  function* uploadFilevineDocumentSaga() {
    yield takeLatest(UPLOAD_FILEVINE_DOCUMENT, function* updater({ record, pageLoading }) {
      if(record) {
        try {
          const document_type = record.document_type && record.document_type.toUpperCase() || false;
          let fileName = record.s3_file_key && record.s3_file_key.split('/');
          fileName = fileName && fileName.length > 0 && fileName[fileName.length - 1] || `${document_type}_${new Date().getTime()}`;
          const file = yield call(getDocumentSize, Object.assign({}, { s3_file_key: record.s3_file_key }));

          const submitData = Object.assign({}, { filename: fileName, size: file.file_size });
          const result = yield call(creatFilevineDocumentUrl, submitData);
          if(result && result.url) {
            const sumbitRecord = Object.assign({}, { contentType: result.contentType, url: result.url, s3_file_key: record.s3_file_key });
            let upload = yield call(uploadFilevineDocument, sumbitRecord)
            if(upload) {
              const document = yield call(addDocumentToProject, Object.assign({}, { projectId: record.integration_case_id, documentId: result.documentId && result.documentId.native, body: { documentId: result.documentId } }));
              if(document) {
                yield put(uploadFilevineDocumentSuccess('Document Uploaded Successfully'));
                yield delay(1000);
                const pageLoader = store2.get('pageLoader');
                const userLoader = store2.get('userLoader');
                if(pageLoading || pageLoader || userLoader) {
                  const secret = store2.get('secret');
                  yield put(verifySessionAction(secret));
                  pageLoader && store2.remove('pageLoader');
                  userLoader && store2.remove('userLoader');
                }
              }
            }
          }
        } catch (error) {
          yield put(uploadFilevineDocumentError('Failed to Upload Document'));
        }
      }
    })
  }

  function* createNewFilevineSessionSaga() {
    yield takeLatest(CREATE_NEW_FILEVINE_SESSION, function* updater({ record, form, editFormRecord, formName}) {
      let apiKey = record && record.filevine_key || false;
      let apiSecret = record && record.filevine_secret || false;
      let apiUrl = record && record.filevine_api || false;
      if(record && apiKey && apiSecret && apiUrl) {
        setFilevineBaseUrl(apiUrl);
        yield put(startSubmit(form));
        try {
          const store_secret = record.is_store_filevine_secret || false;
          const user_info = yield select(selectUser());
          const practiceId = user_info && user_info.practice_id;
          const timestamp = new Date().toISOString();
      
          // Ordering is important!
          const data = [apiKey, timestamp, apiSecret].join('/');
      
          let hash = CryptoJS.MD5(data).toString('');
  
          const result = yield call(filevineSession, Object.assign({}, { mode: "key", apiKey: `${apiKey}`, apiHash: `${hash}`, apiTimestamp: `${timestamp}`}));

          if(result) {
            const apiKeyEncoded = base64.encode(apiKey);
            const encryptApiKey = CryptoJS.AES.encrypt(apiKeyEncoded, practiceId).toString();
            const apiSecretEncoded = base64.encode(apiSecret);
            const encryptSecret = CryptoJS.AES.encrypt(apiSecretEncoded, practiceId).toString();

            const encodedRefreshToken = base64.encode(result.refreshToken);
            const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

            store2.set('filevine', result);
            setFilevineAuthToken(result);

            if(store_secret) {
              yield call(storeFilevineSecret, Object.assign({}, { filevine_key: encryptApiKey, filevine_secret: encryptSecret, filevine_user_id: result.userId, filevine_org_id: result.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: hash, filevine_timeStamp: timestamp, filevine_baseurl : apiUrl }));
            }
            yield put(createNewFilevineSessionSuccess('Filevine session created successfully'));
            yield put(stopSubmit(form));
            if (editFormRecord) {
              const updateRecordSaga = formName.includes('editRecordForm_') ? records('cases') : records('clients');
              const { actions: updateRecordAction, updateRecordSelectors } = updateRecordSaga;
              yield put(updateRecordAction.updateRecord(Object.assign({}, editFormRecord, record), formName, false, result));
            } else {
              yield put(uploadFilevineDocumentActions(record.uploadDocumentDetails, store_secret));
            }
          } else {
            yield put(createNewFilevineSessionError(DEFAULT_FILEVINE_SESSION_ERROR));
            yield put(stopSubmit(form));
          }
          
        } catch(error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? DEFAULT_FILEVINE_SESSION_ERROR :  DEFAULT_FILEVINE_URL_ERROR;
          yield put(createNewFilevineSessionError(Err));
          yield put(stopSubmit(form));
        } finally {
          yield put(destroy(form));
        }
      }
    })
  }

  function* filevineUpdateUserDetailsSaga() {
    yield takeLatest(FILEVINE_UPDATE_USER_DETAILS, function* updater({ record, form, integration }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateUserRecord, record);
          if (result) {
            const { integrationPopup, integration_form, setShowUserForm } = integration;
            yield put(stopSubmit(form));
            yield call(setShowUserForm, false);
            const storeForms = yield select(selectForm());
            const selectFormRecord = storeForms && storeForms[`${integration_form}`] && storeForms[`${integration_form}`].values && storeForms[`${integration_form}`]['values']['integrations'];
            if(selectFormRecord?.length > 0) {
                yield put(filevineSyncDataAction(selectFormRecord, integration_form, integrationPopup));
            }
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to Update User Record';
          yield put(stopSubmit(form));
          yield put(filvineUpdateUserDetailsError(Err));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      createFilevineSessionSaga(),
      filevineLoadRecordsSaga(),
      filevineSyncDataSaga(),
      autoSesssionRefreshSaga(),
      filevineSyncSaga(),
      removeFilevineSecretSaga(),
      verifyFilevineUploadDocumentSessionSaga(),
      uploadFilevineDocumentSaga(),
      createNewFilevineSessionSaga(),
      filevineUpdateUserDetailsSaga()
    ]);
  }

}
/*
 *
 * Users actions
 *
 */


/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {

  const {
    RESET_LOGIN_ATTEMPT,
    RESET_LOGIN_ATTEMPT_SUCCESS,
    RESET_LOGIN_ATTEMPT_ERROR,
    USER_LICENSE_VALIDATION,
    USER_LICENSE_VALIDATION_SUCCESS,
    USER_LICENSE_VALIDATION_ERROR
  } = constants;


  /**
   * @param {object} record 
   */
  function resetLoginAttempt(record) {
    return {
      type: RESET_LOGIN_ATTEMPT,
      record
    };
  }

  /**
   * @param {string} success 
   */
  function resetLoginAttemptSuccess(success) {
    return {
      type: RESET_LOGIN_ATTEMPT_SUCCESS,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function resetLoginAttemptError(error) {
    return {
      type: RESET_LOGIN_ATTEMPT_ERROR,
      error,
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} dialog 
   * @param {function} handleSubmitForm 
   * @returns 
   */
  function licenseValidation(record,form, dialog, handleSubmitForm) {
    return {
      type: USER_LICENSE_VALIDATION,
      record,
      form, 
      dialog, 
      handleSubmitForm
    }
  }


  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function licenseValidationSuccess(success) {
    return {
      type: USER_LICENSE_VALIDATION_SUCCESS,
      success
    }
  }


  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function licenseValidationError(error) {
    return {
      type: USER_LICENSE_VALIDATION_ERROR,
      error
    }
  }



  return {
    resetLoginAttempt,
    resetLoginAttemptSuccess,
    resetLoginAttemptError,
    licenseValidation,
    licenseValidationSuccess,
    licenseValidationError
  }
}

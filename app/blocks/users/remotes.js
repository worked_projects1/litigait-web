/*
 *
 *  Users remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * @param {array} record
     */
    function resetLoginAttempt(record) {
        return api.put(`/rest/users/reset-login-attempt`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {array} record
     */
    function practiceGetOne(record) {
        return api.get(`/practices/${record.practice_id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        resetLoginAttempt,
        practiceGetOne
    }

}


/*
 *
 * Users constants
 *
 */

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {
    const url = `lg/${name}`;

    return {
        RESET_LOGIN_ATTEMPT: `${url}/RESET_LOGIN_ATTEMPT`,
        RESET_LOGIN_ATTEMPT_SUCCESS: `${url}/RESET_LOGIN_ATTEMPT_SUCCESS`,
        RESET_LOGIN_ATTEMPT_ERROR: `${url}/RESET_LOGIN_ATTEMPT_ERROR`,
        USER_LICENSE_VALIDATION: `${url}/USER_LICENSE_VALIDATION`,
        USER_LICENSE_VALIDATION_SUCCESS: `${url}/USER_LICENSE_VALIDATION_SUCCESS`,
        USER_LICENSE_VALIDATION_ERROR: `${url}/USER_LICENSE_VALIDATION_ERROR`
    }
}


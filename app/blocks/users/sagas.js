/*
 *
 *  Users sagas
 *
 */

import { call, take, put, race, all, takeLatest } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form';

import {
  DEFAULT_RESET_LOGIN_ATTEMPT_ERROR
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    RESET_LOGIN_ATTEMPT,
    USER_LICENSE_VALIDATION
  } = constants;

  const {
    resetLoginAttemptError,
    resetLoginAttemptSuccess,
    licenseValidationSuccess,
    licenseValidationError,
  } = actions;

  const {
    resetLoginAttempt,
    practiceGetOne
  } = remotes;


  function* resetLoginAttemptSaga() {
    yield takeLatest(RESET_LOGIN_ATTEMPT, function* updater({ record }) {
      if(record) {
        try {
          const result = yield call(resetLoginAttempt, record);

          if (result) {
            yield put(resetLoginAttemptSuccess('Login Attempt cleared successfully'));
          } else {
            yield put(resetLoginAttemptError(DEFAULT_RESET_LOGIN_ATTEMPT_ERROR));
          }
        } catch (error) {
          yield put(resetLoginAttemptError(DEFAULT_RESET_LOGIN_ATTEMPT_ERROR));
        }
      }
    })
  }
  
  
  function* licenseValidationSaga() {
    yield takeLatest(USER_LICENSE_VALIDATION, function* updater({ record, form, dialog, handleSubmitForm }) {
      yield put(startSubmit(form));
      if(record) {
        try {
          if(record?.is_free_user) {
            yield call(handleSubmitForm);
          } else {
            const result = yield call(practiceGetOne, record);
            if(result) {
              if(result?.billing_type === "limited_users_billing") {
                if(result?.subscription && result?.subscription?.length == 0) {
                  yield call(handleSubmitForm);
                } else if(result?.active_users_count <= result?.license_count) {
                  yield call(handleSubmitForm);
                } else {
                  yield call(dialog, true);
                  yield put(stopSubmit(form));
                }
              } else {
                yield call(handleSubmitForm);
              }
            }
          }
        } catch (error) {
          yield put(stopSubmit(form));
          yield put(licenseValidationError('Failed to validate the practice'));
        }
      }
    })
  }


  return function* rootSaga() {
    yield all([
      resetLoginAttemptSaga(),
      licenseValidationSaga()
    ]);
  }


}

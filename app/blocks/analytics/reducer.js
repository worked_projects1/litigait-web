/*
 *
 * analytics reducer
 *
 */

import produce from 'immer';
import merge from 'lodash/merge';
import {
    LOAD_BUSINESS_METRICS,
    LOAD_BUSINESS_METRICS_SUCCESS,
    LOAD_BUSINESS_METRICS_ERROR,
    LOAD_HISTOGRAM_DATA,
    LOAD_HISTOGRAM_DATA_SUCCESS,
    LOAD_HISTOGRAM_DATA_ERROR,
    UPDATE_FILTER_DATA,
    UPDATE_CUSTOM_FILTER_DATA,
    UPDATE_DASHBOARD_FILTER_DATA,
    UPDATE_ACTIVE_CUSTOM_FILTER,
    UPDATE_DASHBOARD_TAB_VALUE
} from './constants';


const initialState = { loading: false, error: false, success: false, analytics: {}, filter: {}, customFilter: {}, filterData: false, createdDateFilter: {}, activeFilter: {}, tabValue: 0};

/**
 * @param {object} state 
 * @param {object} action 
 */
const appReducer = (state = initialState, { id, type, error, analytics, histogram, filter, customFilter, filterData, createdDateFilter, activeFilter, tabValue }) =>
    produce(state, draft => {
        switch (type) {
            case LOAD_BUSINESS_METRICS:
            case LOAD_HISTOGRAM_DATA:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                break;
            case LOAD_HISTOGRAM_DATA_ERROR:
            case LOAD_BUSINESS_METRICS_ERROR:
                draft.loading = false;
                draft.error = error;
                draft.success = false;
                break;
            case LOAD_BUSINESS_METRICS_SUCCESS:
                draft.analytics = Object.assign({}, draft.analytics, analytics);
                draft.loading = false;
                draft.error = false;
                draft.success = false;
                break;
            case LOAD_HISTOGRAM_DATA_SUCCESS:
                draft.analytics = id ? merge([], draft.analytics, { [id]: id.indexOf('Revenue') > -1 ? histogram && histogram.revenue || {} : histogram && histogram.practices || {} }) : merge([], draft.analytics, { cumulativeRevenue: histogram && histogram.revenue || {}, byMonthRevenue: histogram && histogram.revenue || {}, cumulativePractices: histogram && histogram.practices || {}, byMonthPractices: histogram && histogram.practices || {} });
                draft.loading = false;
                draft.error = false;
                draft.success = false;
                break;
            case UPDATE_FILTER_DATA:
                draft.filter = filter;
                draft.loading = false;
                draft.error = false;
                draft.success = false;
                break;
            case UPDATE_CUSTOM_FILTER_DATA:
                draft.customFilter = customFilter;
                draft.loading = false;
                draft.error = false;
                draft.success = false;
                break;
            case UPDATE_DASHBOARD_FILTER_DATA:
                draft.filterData = filterData;
                // draft.customFilter = {};
                // draft.filter = {};
                draft.loading = false;
                draft.error = false;
                draft.success = false;
                break;
            case UPDATE_ACTIVE_CUSTOM_FILTER:
                draft.createdDateFilter = createdDateFilter;
                draft.activeFilter = activeFilter;
                draft.loading = false;
                draft.success = false;
                draft.error = false;
                break;
            case UPDATE_DASHBOARD_TAB_VALUE:
                draft.tabValue = tabValue;
                draft.loading = false;
                draft.success = false;
                draft.error = false;
                break;
        }
    });

export default appReducer;

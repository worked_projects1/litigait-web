/*
 *
 *  analytics settings
 *
 */

import actions from './actions';
import constants from './constants';
import saga from './sagas';
import selectors from './selectors';
import remotes from './remotes';


export default {
    name: 'analytics',
    actions,
    constants,
    saga,
    selectors,
    remotes
}
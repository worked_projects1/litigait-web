/**
 * 
 * analytics selectors
 * 
 */

import { createSelector } from 'reselect';

export const selectDomain = () => (state) => state.analytics || false;

export const selectBusinessMetrics = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.analytics || {},
);

export const selectFilter = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filter || {},
);

export const selectLoading = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.loading || false,
);

export const selectError = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.error || false,
);

export const selectSuccess = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.success || false,
);

export const selectCustomFilter = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.customFilter || {},
);

export const selectFilterData = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filterData || false,
);

export const selectCreatedDateFilter = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.createdDateFilter || {}
);

export const selectActiveFilter = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.activeFilter || {}
);

export const selectDashboard = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.tabValue || 0
)

export default {
    selectDomain,
    selectBusinessMetrics,
    selectFilter,
    selectLoading,
    selectError,
    selectSuccess,
    selectCustomFilter,
    selectFilterData,
    selectCreatedDateFilter,
    selectActiveFilter,
    selectDashboard
}


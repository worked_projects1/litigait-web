/*
 *
 *  analytics remotes
 *
 */

import api from 'utils/api';
import { convertObjectToParams } from 'utils/tools';

/**
 * 
 * @param {object} data 
 * @returns 
 */
export function loadBusinessMetrics(data = {}) {
    return api.get(`rest/statistics?${convertObjectToParams(data)}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {object} data 
 * @returns 
 */
export function loadHistogramData(data = {}) {
    return api.get(`rest/statistics/practices-and-revenue?${convertObjectToParams(data)}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
    loadBusinessMetrics,
    loadHistogramData
}
/*
 *
 * analytics actions
 *
 */


import {
    LOAD_BUSINESS_METRICS,
    LOAD_BUSINESS_METRICS_ERROR,
    LOAD_BUSINESS_METRICS_SUCCESS,
    LOAD_HISTOGRAM_DATA,
    LOAD_HISTOGRAM_DATA_ERROR,
    LOAD_HISTOGRAM_DATA_SUCCESS,
    UPDATE_FILTER_DATA,
    UPDATE_CUSTOM_FILTER_DATA,
    UPDATE_DASHBOARD_FILTER_DATA,
    UPDATE_ACTIVE_CUSTOM_FILTER,
    UPDATE_DASHBOARD_TAB_VALUE
} from './constants';


/**
 * 
 * @param {object} data 
 * @returns 
 */
export function loadBusinessMetrics(data) {
    return {
        type: LOAD_BUSINESS_METRICS,
        data
    };
}

/**
 * @param {string} error 
 */
export function loadBusinessMetricsError(error) {
    return {
        type: LOAD_BUSINESS_METRICS_ERROR,
        error
    };
}

/**
 * @param {object} analytics 
 */
export function loadBusinessMetricsSuccess(analytics) {
    return {
        type: LOAD_BUSINESS_METRICS_SUCCESS,
        analytics
    };
}


/**
 * 
 * @param {object} data 
 * @returns 
 */
export function loadHistogramData(data) {
    return {
        type: LOAD_HISTOGRAM_DATA,
        data
    };
}

/**
 * @param {string} error 
 */
export function loadHistogramDataError(error) {
    return {
        type: LOAD_HISTOGRAM_DATA_ERROR,
        error
    };
}

/**
 * 
 * @param {number} id 
 * @param {object} histogram 
 * @returns 
 */
export function loadHistogramDataSuccess(id, histogram) {
    return {
        type: LOAD_HISTOGRAM_DATA_SUCCESS,
        id,
        histogram
    };
}

/**
 * 
 * @param {object} filter 
 */
export function updateFilterData(filter) {
    return {
        type: UPDATE_FILTER_DATA,
        filter
    };
}

/**
 * 
 * @param {object} customFilter 
 */
export function updateCustomFilterData(customFilter) {
    return {
        type: UPDATE_CUSTOM_FILTER_DATA,
        customFilter
    };
}

/**
 * 
 * @param {object} filterData 
 */
export function updateDashboardFilterData(filterData) {
    return {
        type: UPDATE_DASHBOARD_FILTER_DATA,
        filterData
    };
}

/**
 * 
 * @param {object} createdDateFilter   
 * @param {object} activeFilter
 * @returns 
 */
export function updateActiveCustomFilter(createdDateFilter, activeFilter) {
    return {
        type: UPDATE_ACTIVE_CUSTOM_FILTER,
        createdDateFilter,
        activeFilter
    }
}

/**
 * 
 * @param {number} tabValue 
 * @returns 
 */
export function updateDashboardTabValue(tabValue){
    return {
        type: UPDATE_DASHBOARD_TAB_VALUE,
        tabValue
    }
}

export default {
    loadBusinessMetrics,
    loadBusinessMetricsError,
    loadBusinessMetricsSuccess,
    loadHistogramData,
    loadHistogramDataError,
    loadHistogramDataSuccess,
    updateFilterData,
    updateCustomFilterData,
    updateDashboardFilterData,
    updateActiveCustomFilter,
    updateDashboardTabValue
}
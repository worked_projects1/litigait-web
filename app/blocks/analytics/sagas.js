/*
 *
 *  analytics sagas
 *
 */


import { call, take, put, race, all, delay } from 'redux-saga/effects';

import {
    DEFAULT_LOAD_BUSINESS_METRICS_ERROR,
    DEFAULT_LOAD_HISTOGRAM_DATA_ERROR
} from 'utils/errors';

import {
    LOAD_BUSINESS_METRICS,
    LOAD_HISTOGRAM_DATA
} from './constants';


import {
    loadHistogramDataError,
    loadHistogramDataSuccess,
    loadBusinessMetricsError,
    loadBusinessMetricsSuccess
} from './actions';


import {
    loadBusinessMetrics,
    loadHistogramData
} from './remotes';


export function* loadBusinessMetricsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { load } = yield race({
            load: take(LOAD_BUSINESS_METRICS),
        });

        const { data = {} } = load;
        if (load) {
            try {
                const result = yield call(loadBusinessMetrics, data);
                if (result) {
                    yield delay(500);
                    yield put(loadBusinessMetricsSuccess(result));
                } else {
                    yield put(loadBusinessMetricsError(DEFAULT_LOAD_BUSINESS_METRICS_ERROR));
                }
            } catch (error) {
                yield put(loadBusinessMetricsError(DEFAULT_LOAD_BUSINESS_METRICS_ERROR));
            }
        }
    }
}




export function* loadHistogramDataSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { load } = yield race({
            load: take(LOAD_HISTOGRAM_DATA),
        });

        const { data = {} } = load;
        const { id } = data;
        
        if (load) {
            try {
                const result = yield call(loadHistogramData, data);
                if (result) {
                    yield delay(500);
                    yield put(loadHistogramDataSuccess(id, result));
                } else {
                    yield put(loadBusinessMetricsError(DEFAULT_LOAD_HISTOGRAM_DATA_ERROR));
                }
            } catch (error) {
                yield put(loadBusinessMetricsError(DEFAULT_LOAD_HISTOGRAM_DATA_ERROR));
            }
        }
    }
}


export default function* rootSaga() {
    yield all([
        loadBusinessMetricsSaga(),
        loadHistogramDataSaga()
    ]);
}
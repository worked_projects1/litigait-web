/*
 *
 * Cases actions
 *
 */


/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function actions(constants) {

  const {
    UPLOAD_FORM,
    UPLOAD_FORM_ERROR,
    GENERATE_FORM,
    GENERATE_FORM_ERROR,
    CANCEL_FORM,
    CANCEL_FORM_ERROR,
    SAVE_FORM,
    SAVE_FORM_ERROR,
    SAVE_FORM_SUCCESS,
    LOAD_FORM,
    LOAD_FORM_ERROR,
    LOAD_FORM_SUCCESS,
    UPDATE_FORM,
    UPDATE_FORM_ERROR,
    UPDATE_FORM_SUCCESS,
    FETCH_FORM,
    FETCH_FORM_ERROR,
    FETCH_FORM_SUCCESS,
    GENERATE_DOCUMENT,
    GENERATE_DOCUMENT_ERROR,
    GENERATE_DOCUMENT_SUCCESS,
    SAVE_DOCUMENT,
    SAVE_DOCUMENT_ERROR,
    SAVE_DOCUMENT_SUCCESS,
    SEND_QUESTIONS,
    SEND_QUESTIONS_ERROR,
    SEND_QUESTIONS_SUCCESS,
    LOAD_BILLING_DETAILS,
    LOAD_BILLING_DETAILS_ERROR,
    LOAD_BILLING_DETAILS_SUCCESS,
    UPDATE_BILLING_DETAILS,
    UPDATE_BILLING_DETAILS_ERROR,
    UPDATE_BILLING_DETAILS_SUCCESS,
    UPDATE_KEY,
    DELETE_FORM,
    DELETE_FORM_ERROR,
    DELETE_FORM_SUCCESS,
    SAVE_STANDARD_FORM,
    SAVE_STANDARD_FORM_ERROR,
    SAVE_STANDARD_FORM_SUCCESS,
    SAVE_QUESTIONS_FORM,
    SAVE_QUESTIONS_FORM_ERROR,
    SAVE_QUESTIONS_FORM_SUCCESS,
    CREATE_MEDICAL_HISTORY,
    CREATE_MEDICAL_HISTORY_ERROR,
    CREATE_MEDICAL_HISTORY_SUCCESS,
    UPDATE_MEDICAL_HISTORY,
    UPDATE_MEDICAL_HISTORY_ERROR,
    UPDATE_MEDICAL_HISTORY_SUCCESS,
    DELETE_MEDICAL_HISTORY,
    DELETE_MEDICAL_HISTORY_ERROR,
    DELETE_MEDICAL_HISTORY_SUCCESS,
    VIEW_MEDICAL_HISTORY,
    VIEW_MEDICAL_HISTORY_ERROR,
    VIEW_MEDICAL_HISTORY_SUCCESS,
    GENERATE_MEDICAL_SUMMARY,
    GENERATE_MEDICAL_SUMMARY_ERROR,
    GENERATE_MEDICAL_SUMMARY_SUCCESS,
    REQUEST_HELP,
    REQUEST_HELP_ERROR,
    REQUEST_HELP_SUCCESS,
    REQUEST_TRANSLATION,
    REQUEST_TRANSLATION_ERROR,
    MERGE_TRANSLATION,
    MERGE_TRANSLATION_ERROR,
    MERGE_TRANSLATION_SUCCESS,
    SEND_VERIFICATION,
    SEND_VERIFICATION_ERROR,
    SEND_VERIFICATION_SUCCESS,
    AUTO_SAVE_FORM,
    AUTO_SAVE_FORM_ERROR,
    AUTO_SAVE_FORM_SUCCESS,
    COPY_TO_LAWYER_RESPONSE,
    COPY_TO_LAWYER_RESPONSE_ERROR,
    COPY_TO_LAWYER_RESPONSE_SUCCESS,
    SHRED,
    SHRED_ERROR,
    SHRED_SUCCESS,
    EDIT_FORM,
    EDIT_FORM_ERROR,
    EDIT_FORM_SUCCESS,
    ATTACH_CASE,
    ATTACH_CASE_SUCCESS,
    ATTACH_CASE_ERROR,
    CREATE_CLIENT_RECORD,
    CREATE_CLIENT_RECORD_SUCCESS,
    CREATE_CLIENT_RECORD_ERROR,
    SET_RESPONSE_DATE,
    SET_RESPONSE_DATE_SUCCESS,
    SET_RESPONSE_DATE_ERROR,
    RESEND_QUESTIONS,
    RESEND_QUESTIONS_SUCCESS,
    RESEND_QUESTIONS_ERROR,
    SAVE_ALL_FORM,
    SAVE_ALL_FORM_ERROR,
    SAVE_ALL_FORM_SUCCESS,
    SAVE_SUB_GROUP_FORM,
    SAVE_SUB_GROUP_FORM_ERROR,
    SAVE_SUB_GROUP_FORM_SUCCESS,
    GENERATE_POS_DOCUMENT,
    GENERATE_POS_DOCUMENT_ERROR,
    GENERATE_POS_DOCUMENT_SUCCESS,
    COPY_ALL_TO_LAWYER_RESPONSE,
    COPY_ALL_TO_LAWYER_RESPONSE_ERROR,
    COPY_ALL_TO_LAWYER_RESPONSE_SUCCESS,
    SAVE_COMMENT,
    SAVE_COMMENT_ERROR,
    SAVE_COMMENT_SUCCESS,
    ARCHIVE_CASE,
    ARCHIVE_CASE_SUCCESS,
    ARCHIVE_CASE_ERROR,
    UNARCHIVE_CASE,
    UNARCHIVE_CASE_ERROR,
    UNARCHIVE_CASE_SUCCESS,
    ADD_CONSULTATION,
    ADD_CONSULTATION_ERROR,
    ADD_CONSULTATION_SUCCESS,
    DELETE_CONSULTATION,
    DELETE_CONSULTATION_ERROR,
    DELETE_CONSULTATION_SUCCESS,
    UPDATE_EDITED_FLAG,
    UPDATE_EDITED_FLAG_ERROR,
    UPDATE_EDITED_FLAG_SUCCESS,
    SAVE_PROPOUND_FORM,
    SAVE_PROPOUND_FORM_ERROR,
    SAVE_PROPOUND_FORM_SUCCESS,
    LOAD_PROPOUND_RECORD,
    LOAD_PROPOUND_RECORD_ERROR,
    LOAD_PROPOUND_RECORD_SUCCESS,
    LOAD_PROPOUND_FORM,
    LOAD_PROPOUND_FORM_ERROR,
    LOAD_PROPOUND_FORM_SUCCESS,
    DELETE_PROPOUND_FORM,
    DELETE_PROPOUND_FORM_ERROR,
    DELETE_PROPOUND_FORM_SUCCESS,
    UPDATE_PROPOUND_FORM,
    UPDATE_PROPOUND_FORM_ERROR,
    UPDATE_PROPOUND_FORM_SUCCESS,
    GENERATE_PROPOUND_DOCUMENT,
    GENERATE_PROPOUND_DOCUMENT_ERROR,
    GENERATE_PROPOUND_DOCUMENT_SUCCESS,
    SAVE_PROPOUND_FORM_S3KEY,
    SAVE_PROPOUND_FORM_S3KEY_ERROR,
    SAVE_PROPOUND_FORM_S3KEY_SUCCESS,
    SEND_PROPOUND_QUESTIONS,
    SEND_PROPOUND_QUESTIONS_ERROR,
    SEND_PROPOUND_QUESTIONS_SUCCESS,
    SAVE_PROPOUNDER_SERVING_ATTORNEY,
    SAVE_PROPOUNDER_SERVING_ATTORNEY_ERROR,
    SAVE_PROPOUNDER_SERVING_ATTORNEY_SUCCESS,
    QUESTION_TEMPLATE,
    QUESTION_TEMPLATE_ERROR,
    QUESTION_TEMPLATE_SUCCESS,
    DESTROY_STANDARD_FORM,
    CREATE_CLIENT_CASE,
    CREATE_CLIENT_CASE_ERROR,
    CREATE_CLIENT_CASE_SUCCESS,
    CONTENT_NOT_FOUND,
    UPDATE_PLAN_DETAILS,
    UPDATE_PLAN_DETAILS_ERROR,
    UPDATE_PLAN_DETAILS_SUCCESS,
    SEND_RESPOND_QUESTIONS,
    SEND_RESPOND_QUESTIONS_ERROR,
    SEND_RESPOND_QUESTIONS_SUCCESS,
    QUICK_CREATE_SAVE_STANDARD_FORM,
    QUICK_CREATE_SAVE_STANDARD_FORM_ERROR,
    QUICK_CREATE_SAVE_STANDARD_FORM_SUCCESS,
    QUICK_CREATE_SAVE_QUESTIONS_FORM,
    QUICK_CREATE_SAVE_QUESTIONS_FORM_ERROR,
    QUICK_CREATE_SAVE_QUESTIONS_FORM_SUCCESS,
    DELETE_QUICK_CREATE_DOCUMENT,
    DELETE_QUICK_CREATE_DOCUMENT_ERROR,
    DELETE_QUICK_CREATE_DOCUMENT_SUCCESS,
    UPDATE_USER_RECORD,
    UPDATE_USER_RECORD_SUCCESS,
    UPDATE_USER_RECORD_ERROR,
    QUICK_CREATE_REDIRECT_PATH,
    UPDATE_VERIFY_SESSION,
    INVALID_SUBSCRIPTIONS,
    RESTORE_RESPONSE_HISTORY,
    RESTORE_RESPONSE_HISTORY_ERROR,
    RESTORE_RESPONSE_HISTORY_SUCCESS,
    ATTORNEY_RESPONSE_TRACKING,
    ATTORNEY_RESPONSE_TRACKING_SUCCESS,
    ATTORNEY_RESPONSE_TRACKING_ERROR,
    AUTO_SAVE_FORM_DETAILS,
    AUTO_SAVE_FORM_DETAILS_SUCCESS,
    AUTO_SAVE_FORM_DETAILS_ERROR,
    CLEAR_AUTO_SAVE_FORM_DETAILS,
    FILEVINE_SESSION_COMMON_UPDATE,
    FILEVINE_SESSION_COMMON_UPDATE_SUCCESS,
    FILEVINE_SESSION_COMMON_UPDATE_ERROR,
    LITIFY_UPLOAD_DOCUMENT,
    LITIFY_UPLOAD_DOCUMENT_SUCCESS,
    LITIFY_UPLOAD_DOCUMENT_ERROR,
    CREATE_LITIFY_BULK_RECORDS,
    CREATE_LITIFY_BULK_RECORDS_SUCCESS,
    CREATE_LITIFY_BULK_RECORDS_ERROR,
    LITIFY_UPDATE_USER_DETAILS,
    LITIFY_UPDATE_USER_DETAILS_SUCCESS,
    LITIFY_UPDATE_USER_DETAILS_ERROR,
    PLAINTIFF_DEFENDANT_SWITCH,
    INITIAL_SUBSCRIPTION,
    INITIAL_SUBSCRIPTION_SUCCESS,
    INITIAL_SUBSCRIPTION_ERROR,
    STORE_DEFENDANT_DETAILS,
    CREATE_LITIFY_CLIENT_CASE,
    CREATE_LITIFY_CLIENT_CASE_SUCCESS,
    CREATE_LITIFY_CLIENT_CASE_ERROR,
    MANAGE_ARCHIVE_CASE,
    LOAD_DOCUMENT_EDITOR,
    LOAD_DOCUMENT_EDITOR_SUCCESS,
    LOAD_DOCUMENT_EDITOR_ERROR,
    SAVE_DOCUMENT_EDITOR,
    SAVE_DOCUMENT_EDITOR_SUCCESS,
    SAVE_DOCUMENT_EDITOR_ERROR,
    DOCUMENT_EDITOR_STATUS,
    LOAD_PROPOUND_DOCUMENT_EDITOR,
    LOAD_PROPOUND_DOCUMENT_EDITOR_SUCCESS,
    LOAD_PROPOUND_DOCUMENT_EDITOR_ERROR,
    SAVE_PROPOUND_DOCUMENT_EDITOR,
    SAVE_PROPOUND_DOCUMENT_EDITOR_SUCCESS,
    SAVE_PROPOUND_DOCUMENT_EDITOR_ERROR,
    UPDATE_OBJECTION_FORM,
    UPDATE_OBJECTION_FORM_SUCCESS,
    UPDATE_OBJECTION_FORM_ERROR,
    DELETE_ATTORNEY_OBJECTION,
    DELETE_ATTORNEY_OBJECTION_SUCCESS,
    DELETE_ATTORNEY_OBJECTION_ERROR,
    AUTO_SAVE_OBJECTION_FORM,
    AUTO_SAVE_OBJECTION_FORM_SUCCESS,
    AUTO_SAVE_OBJECTION_FORM_ERROR,
    ESERVE_POPUP_STATUS,
    SAVE_DISCOVERY_FILENAME,
    SAVE_DISCOVERY_FILENAME_SUCCESS,
    SAVE_DISCOVERY_FILENAME_ERROR,
    SAVE_PROPOUND_FILENAME,
    SAVE_PROPOUND_FILENAME_SUCCESS,
    SAVE_PROPOUND_FILENAME_ERROR,
    QUESTIONS_NOT_FOUND,
    UPDATE_QUESTIONS,
    UPDATE_QUESTIONS_SUCCESS,
    UPDATE_QUESTIONS_ERROR,
    DELETE_HASH_WITH_FILE,
    DELETE_HASH_WITH_FILE_SUCCESS,
    DELETE_HASH_WITH_FILE_ERROR,
    LOAD_FROGS_QUESTIONS,
    LOAD_FROGS_QUESTIONS_SUCCESS,
    LOAD_FROGS_QUESTIONS_ERROR,
    UPDATE_FROGS_FORM,
    UPDATE_FROGS_FORM_SUCCESS,
    UPDATE_FROGS_FORM_ERROR,
    UPDATE_FROGS_OBJECTION_FORM,
    UPDATE_FROGS_OBJECTION_FORM_SUCCESS,
    UPDATE_FROGS_OBJECTION_FORM_ERROR,
    DELETE_ATTORNEY_FROGS_OBJECTION,
    DELETE_ATTORNEY_FROGS_OBJECTION_SUCCESS,
    DELETE_ATTORNEY_FROGS_OBJECTION_ERROR,
    SAVE_SUB_GROUP_FROGS_FORM,
    SAVE_SUB_GROUP_FROGS_FORM_SUCCESS,
    SAVE_SUB_GROUP_FROGS_FORM_ERROR,
    EDIT_PROPOUND_FORM,
    EDIT_PROPOUND_FORM_SUCCESS,
    EDIT_PROPOUND_FORM_ERROR,
    ADD_FROGS_QUESTIONS,
    ADD_FROGS_QUESTIONS_SUCCESS,
    ADD_FROGS_QUESTIONS_ERROR,
    DELETE_DUPLICATE_SET,
    DELETE_DUPLICATE_SET_SUCCESS,
    DELETE_DUPLICATE_SET_ERROR,
    DELETE_CONNECTED_QUESTION_SET,
    DELETE_CONNECTED_QUESTION_SET_SUCCESS,
    DELETE_CONNECTED_QUESTION_SET_ERROR,
    FETCH_DISCOVERY_HISTORY,
    FETCH_DISCOVERY_HISTORY_SUCCESS,
    FETCH_DISCOVERY_HISTORY_ERROR,
    GET_SFDT_TEXT,
    GET_SFDT_TEXT_SUCCESS,
    GET_SFDT_TEXT_ERROR
  } = constants;

  /**
   * @param {object} record 
   * @param {string} form 
   */
  function uploadForm(record, form) {
    return {
      type: UPLOAD_FORM,
      record,
      form
    };
  }

  /**
   * @param {string} error 
   */
  function uploadFormError(error) {
    return {
      type: UPLOAD_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function generateForm(record) {
    return {
      type: GENERATE_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function generateFormError(error) {
    return {
      type: GENERATE_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function cancelForm(record, loadRecord) {
    return {
      type: CANCEL_FORM,
      record,
      loadRecord
    };
  }

  /**
   * @param {string} error 
   */
  function cancelFormError(error) {
    return {
      type: CANCEL_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function saveForm(record) {
    return {
      type: SAVE_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function saveFormError(error) {
    return {
      type: SAVE_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function saveFormSuccess(success) {
    return {
      type: SAVE_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   * @param {function} setFormLoader 
   * @param {boolean} upload 
   * @returns 
   */
  function loadForm(record, setFormLoader, upload) {
    return {
      type: LOAD_FORM,
      record,
      setFormLoader,
      upload
    };
  }

  /**
   * @param {string} error 
   */
  function loadFormError(error) {
    return {
      type: LOAD_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function loadFormSuccess(record) {
    return {
      type: LOAD_FORM_SUCCESS,
      record
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} closeForm 
   * @param {boolean} subGroupForm 
   * @returns 
   */
  function updateForm(record, form, closeForm, subGroupForm) {
    return {
      type: UPDATE_FORM,
      record,
      form,
      closeForm,
      subGroupForm
    };
  }

  /**
   * @param {string} error 
   */
  function updateFormError(error) {
    return {
      type: UPDATE_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function updateFormSuccess(record) {
    return {
      type: UPDATE_FORM_SUCCESS,
      record
    };
  }

  /**
   * @param {object} record 
   * @param {string} generatedType 
   * @param {function} setFilevinePopUp 
   * @param {function} setMyCasePopUp 
   * @param {function} setProgress 
   * @param {boolean} signature
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   * @param {function} setOpenDocEditor
   * @param {object} customObject
   */
  function fetchForm(record, generatedType, setFilevinePopUp, setMyCasePopUp, setProgress, signature, setLitifyPopup, setClioPopup, setOpenDocEditor, customObject) {
    return {
      type: FETCH_FORM,
      record,
      generatedType,
      setFilevinePopUp,
      setMyCasePopUp,
      setProgress,
      signature,
      setLitifyPopup,
      setClioPopup,
      setOpenDocEditor,
      customObject
    };
  }

  /**
   * @param {string} error 
   */
  function fetchFormError(error) {
    return {
      type: FETCH_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function fetchFormSuccess(record) {
    return {
      type: FETCH_FORM_SUCCESS,
      record
    };
  }

  /**
   * @param {object} record 
   * @param {function} setFilevinePopUp 
   * @param {function} setMyCasePopUp 
   * @param {function} setProgress 
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   * @param {function} setOpenDocEditor
   */
  function generateDocument(record, setFilevinePopUp, setMyCasePopUp, setProgress, setLitifyPopup, setClioPopup, setOpenDocEditor) {
    return {
      type: GENERATE_DOCUMENT,
      record,
      setFilevinePopUp,
      setMyCasePopUp,
      setProgress,
      setLitifyPopup,
      setClioPopup,
      setOpenDocEditor
    };
  }

  /**
   * @param {string} error 
   */
  function generateDocumentError(error) {
    return {
      type: GENERATE_DOCUMENT_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function generateDocumentSuccess(record) {
    return {
      type: GENERATE_DOCUMENT_SUCCESS,
      record
    };
  }

  /**
   * @param {object} record 
   * @param {function} setFilevinePopUp 
   * @param {function} setMyCasePopUp 
   * @param {function} setProgress 
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   * @param {function} setOpenDocEditor
   * @param {object} setOpenDocEditor
   */
  function saveDocument(record, setFilevinePopUp, setMyCasePopUp, setProgress, setLitifyPopup, setClioPopup, setOpenDocEditor, customObject) {
    return {
      type: SAVE_DOCUMENT,
      record,
      setFilevinePopUp,
      setMyCasePopUp,
      setProgress,
      setLitifyPopup,
      setClioPopup,
      setOpenDocEditor,
      customObject
    };
  }

  /**
   * @param {string} error 
   */
  function saveDocumentError(error) {
    return {
      type: SAVE_DOCUMENT_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function saveDocumentSuccess(record,success) {
    return {
      type: SAVE_DOCUMENT_SUCCESS,
      record,
      success
    };
  }

  /**
   * @param {number} key 
   */
  function updateKey(key) {
    return {
      type: UPDATE_KEY,
      key
    };
  }

  /**
   * @param {object} record 
   */
  function sendQuestions(record) {
    return {
      type: SEND_QUESTIONS,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function sendQuestionsError(error) {
    return {
      type: SEND_QUESTIONS_ERROR,
      error
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} success 
   * @returns 
   */
  function sendQuestionsSuccess(record, success) {
    return {
      type: SEND_QUESTIONS_SUCCESS,
      record,
      success
    };
  }

  /**
   * 
   * @param {string} form 
   * @param {function} dialog 
   * @param {function} progress 
   * @param {object} record 
   * @param {string} generatedType 
   * @param {string} generateDocument 
   * @param {function} freeTrialDialog 
   * @param {object} signature 
   * @param {function} setFilevinePopUp 
   * @param {function} setMyCasePopUp 
   * @param {function} subscriptionAlert 
   * @param {function} openTemplateForm 
   * @param {function} openGeorgiaTemplate
   * @param {function} openRecordForm
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   * @param {function} setOpenDocEditor
   * @param {object} customObject
   * @returns 
   */
  function loadBillingDetails(form, dialog, progress, record, generatedType, generateDocument, posModal, freeTrialDialog, signature, setFilevinePopUp, setMyCasePopUp, subscriptionAlert, openTemplateForm, openGeorgiaTemplate, openRecordForm, setLitifyPopup, setClioPopup, setOpenDocEditor, customObject) {
    return {
      type: LOAD_BILLING_DETAILS,
      form,
      dialog,
      progress,
      record,
      generatedType,
      generateDocument,
      posModal,
      freeTrialDialog,
      signature,
      setFilevinePopUp,
      setMyCasePopUp,
      subscriptionAlert,
      openTemplateForm,
      openGeorgiaTemplate,
      openRecordForm,
      setLitifyPopup,
      setClioPopup,
      setOpenDocEditor,
      customObject
    };
  }

  /**
   * @param {string} error 
   */
  function loadBillingDetailsError(error) {
    return {
      type: LOAD_BILLING_DETAILS_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function loadBillingDetailsSuccess(record) {
    return {
      type: LOAD_BILLING_DETAILS_SUCCESS,
      record
    };
  }

  /**
   * @param {function} payment 
   * @param {object} record 
   * @param {function} progress 
   * @param {string} generatedType 
   * @param {string} form 
   * @param {function} posModal
   * @param {function} setFilevinePopUp 
   * @param {function} setMyCasePopUp 
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   */
  function updateBillingDetails(payment, record, progress, generatedType, form, posModal, setFilevinePopUp, setMyCasePopUp, setLitifyPopup, setClioPopup) {
    return {
      type: UPDATE_BILLING_DETAILS,
      payment,
      record,
      progress,
      generatedType,
      form,
      posModal,
      setFilevinePopUp,
      setMyCasePopUp,
      setLitifyPopup,
      setClioPopup
    };
  }

  /**
   * @param {string} error 
   */
  function updateBillingDetailsError(error) {
    return {
      type: UPDATE_BILLING_DETAILS_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function updateBillingDetailsSuccess(success) {
    return {
      type: UPDATE_BILLING_DETAILS_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   * @param {function} loadRecord 
   * @returns 
   */
  function deleteForm(record, loadRecord) {
    return {
      type: DELETE_FORM,
      record, 
      loadRecord
    };
  }

  /**
   * @param {string} error 
   */
  function deleteFormError(error) {
    return {
      type: DELETE_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function deleteFormSuccess(success) {
    return {
      type: DELETE_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   * @param {string} form 
   */
  function saveStandardForm(record, form) {
    return {
      type: SAVE_STANDARD_FORM,
      record,
      form
    };
  }

  /**
   * @param {string} error 
   */
  function saveStandardFormError(error) {
    return {
      type: SAVE_STANDARD_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function saveStandardFormSuccess(success) {
    return {
      type: SAVE_STANDARD_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function saveQuestionsForm(record) {
    return {
      type: SAVE_QUESTIONS_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function saveQuestionsFormError(error) {
    return {
      type: SAVE_QUESTIONS_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function saveQuestionsFormSuccess(success) {
    return {
      type: SAVE_QUESTIONS_FORM_SUCCESS,
      success
    };
  }

  /**
   * 
   * @param {array} records 
   * @param {string} form 
   * @param {function} setMedicalHistory 
   * @param {function} setSummaryConfirmation 
   * @returns 
   */
  function createMedicalHistory(records, form, setMedicalHistory, setSummaryConfirmation) {
    return {
      type: CREATE_MEDICAL_HISTORY,
      records,
      form,
      setMedicalHistory,
      setSummaryConfirmation
    };
  }

  /**
   * @param {string} error 
   */
  function createMedicalHistoryError(error) {
    return {
      type: CREATE_MEDICAL_HISTORY_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function createMedicalHistorySuccess(success) {
    return {
      type: CREATE_MEDICAL_HISTORY_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function updateMedicalHistory(record) {
    return {
      type: UPDATE_MEDICAL_HISTORY,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function updateMedicalHistoryError(error) {
    return {
      type: UPDATE_MEDICAL_HISTORY_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function updateMedicalHistorySuccess(success) {
    return {
      type: UPDATE_MEDICAL_HISTORY_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function deleteMedicalHistory(record) {
    return {
      type: DELETE_MEDICAL_HISTORY,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function deleteMedicalHistoryError(error) {
    return {
      type: DELETE_MEDICAL_HISTORY_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function deleteMedicalHistorySuccess(success) {
    return {
      type: DELETE_MEDICAL_HISTORY_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function viewMedicalHistory(record) {
    return {
      type: VIEW_MEDICAL_HISTORY,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function viewMedicalHistoryError(error) {
    return {
      type: VIEW_MEDICAL_HISTORY_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function viewMedicalHistorySuccess(success) {
    return {
      type: VIEW_MEDICAL_HISTORY_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function generateMedicalSummary(record) {
    return {
      type: GENERATE_MEDICAL_SUMMARY,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function generateMedicalSummaryError(error) {
    return {
      type: GENERATE_MEDICAL_SUMMARY_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function generateMedicalSummarySuccess(success) {
    return {
      type: GENERATE_MEDICAL_SUMMARY_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   * @param {function} stepperLoader 
   * @returns 
   */
  function requestHelp(record, stepperLoader) {
    return {
      type: REQUEST_HELP,
      record,
      stepperLoader
    };
  }

  /**
   * @param {string} error 
   */
  function requestHelpError(error) {
    return {
      type: REQUEST_HELP_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function requestHelpSuccess(success) {
    return {
      type: REQUEST_HELP_SUCCESS,
      success
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {function} spinner 
   */
  function requestTranslation(record, spinner) {
    return {
      type: REQUEST_TRANSLATION,
      record,
      spinner
    };
  }

  /**
   * @param {string} error 
   */
  function requestTranslationError(error) {
    return {
      type: REQUEST_TRANSLATION_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   * @param {number} charLimit
   */
  function mergeTranslation(record, charLimit) {
    return {
      type: MERGE_TRANSLATION,
      record,
      charLimit
    };
  }

  /**
   * @param {string} error 
   */
  function mergeTranslationError(error) {
    return {
      type: MERGE_TRANSLATION_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function mergeTranslationSuccess(success) {
    return {
      type: MERGE_TRANSLATION_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function sendVerification(record) {
    return {
      type: SEND_VERIFICATION,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function sendVerificationError(error) {
    return {
      type: SEND_VERIFICATION_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function sendVerificationSuccess(success) {
    return {
      type: SEND_VERIFICATION_SUCCESS,
      success
    };
  }


  /**
   * @param {object} record 
   * @param {Array} forms
   * @param {string} redirectURL
   * @returns 
   */
  function autoSaveForm(record, forms, redirectURL) {
    return {
      type: AUTO_SAVE_FORM,
      record,
      forms,
      redirectURL
    };
  }

  /**
   * @param {string} error 
   */
  function autoSaveFormError(error) {
    return {
      type: AUTO_SAVE_FORM_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function autoSaveFormSuccess(success) {
    return {
      type: AUTO_SAVE_FORM_SUCCESS,
      success
    };
  }


  /**
   * @param {string} record
   * @param {boolean} openForm
   * @param {number} charLimit
   */
  function copyToLawyerResponse(record, openForm, charLimit) {
    return {
      type: COPY_TO_LAWYER_RESPONSE,
      record,
      openForm,
      charLimit
    };
  }

  /**
   * @param {string} error 
   */
  function copyToLawyerResponseError(error) {
    return {
      type: COPY_TO_LAWYER_RESPONSE_ERROR,
      error,
    };
  }

  /**
   * @param {string} success 
   */
  function copyToLawyerResponseSuccess(success) {
    return {
      type: COPY_TO_LAWYER_RESPONSE_SUCCESS,
      success
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {function} loadRecord 
   * @returns 
   */
  function shred(record, loadRecord) {
    return {
      type: SHRED,
      record,
      loadRecord
    };
  }

  /**
   * @param {string} error 
   */
  function shredError(error) {
    return {
      type: SHRED_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function shredSuccess(success) {
    return {
      type: SHRED_SUCCESS,
      success
    };
  }


  /**
   * @param {object} record 
   */
  function editForm(record) {
    return {
      type: EDIT_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function editFormError(error) {
    return {
      type: EDIT_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function editFormSuccess(record, success) {
    return {
      type: EDIT_FORM_SUCCESS,
      record,
      success
    };
  }

  /**
   * @param {object} record 
   * @param {string} form 
   */
  function attachCase(record, form, setAttchCaseForm) {
    return {
      type: ATTACH_CASE,
      record,
      form,
      setAttchCaseForm
    }
  }

  /**
   * @param {string} success 
   */
  function attachCaseSuccess(success) {
    return {
      type: ATTACH_CASE_SUCCESS,
      success,
    };
  }

  /**
   * @param {string} error 
   */
  function attachCaseError(error) {
    return {
      type: ATTACH_CASE_ERROR,
      error,
    };
  }


  /**
   * @param {object} record 
   * @param {string} form 
   */
  function createClientRecord(record, form, setCreateClientForm) {
    return {
      type: CREATE_CLIENT_RECORD,
      record,
      form,
      setCreateClientForm
    }
  }

  /**
   * @param {string} success 
   */
  function createClientRecordSuccess(success) {
    return {
      type: CREATE_CLIENT_RECORD_SUCCESS,
      success,
    };
  }

  /**
   * @param {string} error 
   */
  function createClientRecordError(error) {
    return {
      type: CREATE_CLIENT_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   * @param {string} form
   * @param {function} setDueDate 
   * @returns 
   */
  function setResponseDate(record,form, setDueDate) {    
    return {
      type: SET_RESPONSE_DATE,
      record,
      form,
      setDueDate
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {string} success 
   * @returns 
   */
  function setResponseDateSuccess(record, success) {
    return {
      type: SET_RESPONSE_DATE_SUCCESS,
      record,
      success,
    };
  }

  /**
   * @param {string} error 
   */
  function setResponseDateError(error) {
    return {
      type: SET_RESPONSE_DATE_ERROR,
      error,
    };
  }


  /**
   * @param {object} record 
   */
  function resendQuestions(record) {
    return {
      type: RESEND_QUESTIONS,
      record
    };
  }

  /**
   * @param {string} success 
   */
  function resendQuestionsSuccess(success) {
    return {
      type: RESEND_QUESTIONS_SUCCESS,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function resendQuestionsError(error) {
    return {
      type: RESEND_QUESTIONS_ERROR,
      error
    };
  }


  /**
   * 
   * @param {string} form 
   * @param {object} record 
   * @param {function} setCloseEditor 
   * @returns 
   */
  function saveAllForm(form, record, setCloseEditor) {
    return {
      type: SAVE_ALL_FORM,
      form,
      record,
      setCloseEditor
    };
  }


  /**
   * @param {string} error 
   */
  function saveAllFormError(error) {
    return {
      type: SAVE_ALL_FORM_ERROR,
      error,
    };
  }


  /**
   * @param {string} success 
   */
  function saveAllFormSuccess(success) {
    return {
      type: SAVE_ALL_FORM_SUCCESS,
      success
    };
  }


  /**
   * 
   * @param {object} record 
   * @param {function} setOpenForm 
   * @returns 
   */
  function saveSubGroupForm(record, setOpenForm) {
    return {
      type: SAVE_SUB_GROUP_FORM,
      record,
      setOpenForm
    };
  }


  /**
   * @param {string} error 
   */
  function saveSubGroupFormError(error) {
    return {
      type: SAVE_SUB_GROUP_FORM_ERROR,
      error,
    };
  }


  /**
   * 
   * @param {object} record 
   * @param {string} success 
   * @returns 
   */
  function saveSubGroupFormSuccess(success) {
    return {
      type: SAVE_SUB_GROUP_FORM_SUCCESS,
      success
    };
  }


  /**
   * 
   * @param {object} record 
   * @param {function} posModal
   * @param {object} customObject
   * @returns 
   */
  function generatePOSDocument(record, posModal, customObject) {
    return {
      type: GENERATE_POS_DOCUMENT,
      record,
      posModal,
      customObject
    };
  }


  /**
   * @param {string} error 
   */
  function generatePOSDocumentError(error) {
    return {
      type: GENERATE_POS_DOCUMENT_ERROR,
      error
    };
  }


  /**
   * 
   * @param {object} record 
   * @param {string} success 
   * @returns 
   */
  function generatePOSDocumentSuccess(success) {
    return {
      type: GENERATE_POS_DOCUMENT_SUCCESS,
      success
    };
  }

  /**
   *  
   * @param {object} record 
   * @returns 
   */
  function copyAllToLawyerResponse(record, setCloseEditor) {
    return {
      type: COPY_ALL_TO_LAWYER_RESPONSE,
      record,
      setCloseEditor
    };
  }


  /**
   * @param {string} error 
   */
  function copyAllToLawyerResponseError(error) {
    return {
      type: COPY_ALL_TO_LAWYER_RESPONSE_ERROR,
      error,
    };
  }


  /**
   * @param {string} success 
   */
  function copyAllToLawyerResponseSuccess(success) {
    return {
      type: COPY_ALL_TO_LAWYER_RESPONSE_SUCCESS,
      success
    };
  }
  
  
  /**
   *  
   * @param {object} record 
   * @param {function} setMedicalHistory 
   * @returns 
   */
  function saveComment(record, setMedicalHistory) {
    return {
      type: SAVE_COMMENT,
      record,
      setMedicalHistory
    };
  }


  /**
   * @param {string} error 
   */
  function saveCommentError(error) {
    return {
      type: SAVE_COMMENT_ERROR,
      error,
    };
  }


  /**
   * @param {string} success 
   */
  function saveCommentSuccess(success) {
    return {
      type: SAVE_COMMENT_SUCCESS,
      success
    };
  }

  /**
   * @param {string} id 
   * @param {function} setArchiveLoader 
   */
  function archiveCase(id, setArchiveLoader) {
    return {
      type: ARCHIVE_CASE,
      id,
      setArchiveLoader
    }
  }

  /**
   * @param {string} success 
   */
  function archiveCaseSuccess(success) {
    return {
      type: ARCHIVE_CASE_SUCCESS,
      success
    }
  }

  /**
   * @param {string} error 
   */
  function archiveCaseError(error) {
    return {
      type: ARCHIVE_CASE_ERROR,
      error
    }
  }

  /**
   * @param {object} record 
   * @param {function} setLoader 
   */
  function unArchiveCase(record, setLoader) {
    return {
      type: UNARCHIVE_CASE,
      record,
      setLoader
    }
  }

  /**
   * @param {string} success 
   */
  function unArchiveCaseSuccess(success) {
    return {
      type: UNARCHIVE_CASE_SUCCESS,
      success
    }
  }

  /**
   * @param {string} error 
   */
  function unArchiveCaseError(error) {
    return {
      type: UNARCHIVE_CASE_ERROR,
      error
    }
  }


  /**
   * 
   * @param {object} record
   * @param {function} setBtnLoader
   */
  function addConsultation(record, setBtnLoader) {
    return {
      type: ADD_CONSULTATION,
      record,
      setBtnLoader
    }
  }
  
  
  /**
   * 
   * @param {object} record
   */
  function addConsultationSuccess(record) {
    return {
      type: ADD_CONSULTATION_SUCCESS,
      record
    }
  }
  
  
  /**
   * 
   * @param {object} record
   */
  function addConsultationError(error) {
    return {
      type: ADD_CONSULTATION_ERROR,
      error
    }
  }
  

  /**
   * 
   * @param {object} record
   * @param {function} setFormLoader
   */
  function deleteConsultation(record, setFormLoader) {
    return {
      type: DELETE_CONSULTATION,
      record,
      setFormLoader
    }
  }
  
  
  /**
   * 
   * @param {object} record
   * @param {string} success
   */
  function deleteConsultationSuccess(success) {
    return {
      type: DELETE_CONSULTATION_SUCCESS,
      success
    }
  }
  
  
  /**
   * 
   * @param {object} record
   */
  function deleteConsultationError(error) {
    return {
      type: DELETE_CONSULTATION_ERROR,
      error
    }
  }

  /**
   * @param {object} record 
   */
  function updateEditedFlag(record) {
    return {
      type: UPDATE_EDITED_FLAG,
      record
    }
  }


  /**
   * @param {string} success 
   */
  function updateEditedFlagSuccess(success) {
    return {
      type: UPDATE_EDITED_FLAG_SUCCESS,
      success
    }
  }


  /**
   * @param {string} error 
   */
  function updateEditedFlagError(error) {
    return {
      type: UPDATE_EDITED_FLAG_ERROR,
      error
    }
  }

  /**
  * @param {object} record 
  * @param {string} form 
  */
  function savePropoundForm(record, form) {
    return {
      type: SAVE_PROPOUND_FORM,
      record,
      form
    };
  }

  /**
   * @param {string} error 
   */
  function savePropoundFormError(error) {
    return {
      type: SAVE_PROPOUND_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function savePropoundFormSuccess(success) {
    return {
      type: SAVE_PROPOUND_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {string} id 
   */
  function loadPropoundRecord(id) {
    return {
      type: LOAD_PROPOUND_RECORD,
      id
    };
  }

  /**
   * @param {array} record
   */
  function loadPropoundRecordSuccess(record) {
    return {
      type: LOAD_PROPOUND_RECORD_SUCCESS,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function loadPropoundRecordError(error) {
    return {
      type: LOAD_PROPOUND_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} record
   * @param {function} setQuestions
   * @param {function} setFormLoader
   * @returns 
   */
  function loadPropoundForm(record, setQuestions, setFormLoader) {
    return {
      type: LOAD_PROPOUND_FORM,
      record,
      setQuestions,
      setFormLoader
    };
  }

  /**
   * @param {string} error 
   */
  function loadPropoundFormError(error) {
    return {
      type: LOAD_PROPOUND_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function loadPropoundFormSuccess(record) {
    return {
      type: LOAD_PROPOUND_FORM_SUCCESS,
      record
    };
  }

  /**
   * @param {object} record 
   * @param {function} loadPropoundRecord 
   * @returns 
   */
  function deletePropoundForm(record, loadPropoundRecord) {
    return {
      type: DELETE_PROPOUND_FORM,
      record,
      loadPropoundRecord
    };
  }

  /**
   * @param {string} error 
   */
  function deletePropoundFormError(error) {
    return {
      type: DELETE_PROPOUND_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function deletePropoundFormSuccess(success) {
    return {
      type: DELETE_PROPOUND_FORM_SUCCESS,
      success
    };
  }
  
  /**
   * @param {object} record 
   * @returns 
   */
  function updatePropoundForm(record) {
    return {
      type: UPDATE_PROPOUND_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function updatePropoundFormError(error) {
    return {
      type: UPDATE_PROPOUND_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function updatePropoundFormSuccess(record) {
    return {
      type: UPDATE_PROPOUND_FORM_SUCCESS,
      record
    };
  }

  /**
   * 
   * @param {object} record
   * @param {Object} caseInfo
   * @param {Object} userInfo
   * @param {string} generatedType
   * @param {object} signature
   * @returns 
   */
  function generatePropoundDocument(record, user_info, case_info, generatedType, signature, setProgress, setOpenDocEditor) {
    return {
      type: GENERATE_PROPOUND_DOCUMENT,
      record,
      user_info,
      case_info,
      generatedType,
      signature,
      setProgress, 
      setOpenDocEditor
    };
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function generatePropoundDocumentError(error) {
    return {
      type: GENERATE_PROPOUND_DOCUMENT_ERROR,
      error
    };
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function generatePropoundDocumentSuccess(success) {
    return {
      type: GENERATE_PROPOUND_DOCUMENT_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function savePropoundFormS3Key(record, setProgress, setOpenDocEditor) {
    return {
      type: SAVE_PROPOUND_FORM_S3KEY,
      record,
      setProgress,
      setOpenDocEditor
    };
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function savePropoundFormS3KeyError(error) {
    return {
      type: SAVE_PROPOUND_FORM_S3KEY_ERROR,
      error
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} success 
   * @returns 
   */
  function savePropoundFormS3KeySuccess(record) {
    return {
      type: SAVE_PROPOUND_FORM_S3KEY_SUCCESS,
      record
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} setDocument 
   * @param {function} setEmailPopup
   * @returns 
   */
  function sendPropoundQuestions(record, form, setDocument, setEmailPopup) {
    return {
      type: SEND_PROPOUND_QUESTIONS,
      record,
      form,
      setDocument,
      setEmailPopup
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function sendPropoundQuestionsError(error) {
    return {
      type: SEND_PROPOUND_QUESTIONS_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function sendPropoundQuestionsSuccess(success) {
    return {
      type: SEND_PROPOUND_QUESTIONS_SUCCESS,
      success
    }
  }


  /**
  * @param {object} record
  * @param {function} posModal
  * @param {function} signatureModal
  */
  function savePropounderServingAttorney(record, posModal, signatureModal) {
    return {
      type: SAVE_PROPOUNDER_SERVING_ATTORNEY,
      record,
      posModal,
      signatureModal
    };
  }

  /**
  * 
  * @param {string} error 
  * @returns 
  */
  function savePropounderServingAttorneyError(error) {
    return {
      type: SAVE_PROPOUNDER_SERVING_ATTORNEY_ERROR,
      error
    };
  }

  /**
  * 
  * @param {object} record 
  * @param {string} success 
  * @returns 
  */
  function savePropounderServingAttorneySuccess(record, success) {
    return {
      type: SAVE_PROPOUNDER_SERVING_ATTORNEY_SUCCESS,
      record,
      success
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} getFormLabel 
   * @param {Array} formColumns 
   * @returns 
   */
  function questionsTemplate(record, form, getFormLabel, formColumns) {
    return {
      type: QUESTION_TEMPLATE,
      record,
      form,
      getFormLabel,
      formColumns
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function questionsTemplateError(error) {
    return {
      type: QUESTION_TEMPLATE_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function questionsTemplateSuccess(success) {
    return {
      type: QUESTION_TEMPLATE_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {object} record
   * @param {boolean} quickCreate
   * @param {string} form 
   * @returns 
   */
  function destroyStandardForm(record, quickCreate, form) {
    return {
      type: DESTROY_STANDARD_FORM,
      record,
      quickCreate,
      form
    }
  }

  /**
   * @param {boolean} content 
   */
  function contentNotFound(content) {
    return {
      type: CONTENT_NOT_FOUND,
      content
    }
  }

  /**
   * @param {object} record 
   * @param {string} form 
   */
  function createClientCase(record, form) {
    return {
      type: CREATE_CLIENT_CASE,
      record,
      form
    }
  }
  
  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function createClientCaseError(error) {
    return {
      type: CREATE_CLIENT_CASE_ERROR,
      error
    }
  }
  
  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function createClientCaseSuccess(success) {
    return {
      type: CREATE_CLIENT_CASE_SUCCESS,
      success
    }
  }

  /**
   * @param {object} payment 
   * @param {object} cardDetails 
   * @param {string} form 
   * @param {object} record 
   * @param {function} openCardDetailForm 
   * @param {function} dialog  
   * @param {function} progress  
   * @param {string} generatedType 
   * @param {function} posModal 
   * @param {function} freeTrialDialog 
   * @param {function} setShowFilevinePopup
   * @param {function} setShowMyCasePopup
   * @param {function} openTemplateForm
   * @param {function} openGeorgiaTemplate
   * @param {object} actions
   * @param {function} setShowLitifyPopup
   * @param {function} setShowClioPopup
   * @param {object} customObject
   */
  function updatePlanDetails(payment, cardDetails, form, record, openCardDetailForm, dialog, progress, generatedType, posModal, freeTrialDialog, setShowFilevinePopup, setShowMyCasePopup, openTemplateForm, openGeorgiaTemplate, actions, setShowLitifyPopup, setShowClioPopup, customObject) {
    return {
      type: UPDATE_PLAN_DETAILS,
      payment, 
      cardDetails, 
      form, 
      record, 
      openCardDetailForm, 
      dialog, 
      progress, 
      generatedType, 
      posModal, 
      freeTrialDialog, 
      setShowFilevinePopup, 
      setShowMyCasePopup, 
      openTemplateForm,
      openGeorgiaTemplate,
      actions,
      setShowLitifyPopup,
      setShowClioPopup,
      customObject
    };
  }

  /**
   * @param {string} error 
   */
  function updatePlanDetailsError(error) {
    return {
      type: UPDATE_PLAN_DETAILS_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function updatePlanDetailsSuccess(success) {
    return {
      type: UPDATE_PLAN_DETAILS_SUCCESS,
      success
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} setDocument 
   * @param {function} setEmailPopup
   * @param {function} setFilevinePopup
   * @param {function} setMyCasePopup
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   * @returns 
   */
  function sendRespondQuestions(record, form, setDocument, setEmailPopup, setFilevinePopup, setMyCasePopup, setLitifyPopup, setClioPopup) {
    return {
      type: SEND_RESPOND_QUESTIONS,
      record,
      form,
      setDocument,
      setEmailPopup,
      setFilevinePopup,
      setMyCasePopup,
      setLitifyPopup,
      setClioPopup
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function sendRespondQuestionsError(error) {
    return {
      type: SEND_RESPOND_QUESTIONS_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function sendRespondQuestionsSuccess(success) {
    return {
      type: SEND_RESPOND_QUESTIONS_SUCCESS,
      success
    }
  }


  /**
   * @param {object} record 
   * @param {string} form 
   */
  function quickCreateSaveStandardForm(record, form) {
    return {
      type: QUICK_CREATE_SAVE_STANDARD_FORM,
      record,
      form
    };
  }

  /**
   * @param {string} error 
   */
  function quickCreateSaveStandardFormError(error) {
    return {
      type: QUICK_CREATE_SAVE_STANDARD_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function quickCreateSaveStandardFormSuccess(success) {
    return {
      type: QUICK_CREATE_SAVE_STANDARD_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {object} record 
   */
  function quickCreateSaveQuestionsForm(record) {
    return {
      type: QUICK_CREATE_SAVE_QUESTIONS_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function quickCreateSaveQuestionsFormError(error) {
    return {
      type: QUICK_CREATE_SAVE_QUESTIONS_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function quickCreateSaveQuestionsFormSuccess(success) {
    return {
      type: QUICK_CREATE_SAVE_QUESTIONS_FORM_SUCCESS,
      success
    };
  }


  /**
   * @param {object} record 
   * @param {boolean} isPropoundPage 
   * @param {string} form 
   */
  function deleteQuickCreateDocument(record, isPropoundPage, form) {
    return {
      type: DELETE_QUICK_CREATE_DOCUMENT,
      record,
      isPropoundPage,
      form
    }
  }

  /**
   * @param {string} error 
   */
  function deleteQuickCreateDocumentError(error) {
    return {
      type: DELETE_QUICK_CREATE_DOCUMENT_ERROR,
      error
    }
  }
  

  function deleteQuickCreateDocumentSuccess() {
    return {
      type: DELETE_QUICK_CREATE_DOCUMENT_SUCCESS,
    }
  }


  /**
   * 
   * @param {object} record 
   * @param {object} userRecord 
   * @param {string} form
   * @param {object} customObject
   * @returns 
   */
  function updateUserRecord(record, userRecord, form, customObject) {
    return {
      type: UPDATE_USER_RECORD,
      record,
      userRecord,
      form,
      customObject
    }
  }

  /**
   * @param {string} success
   */
  function updateUserRecordSuccess(success){
    return{
      type: UPDATE_USER_RECORD_SUCCESS,
      success
    }
  }

  /**
   * @param {string} error
   */
  function updateUserRecordError(error) {
    return {
      type: UPDATE_USER_RECORD_ERROR,
      error
    }
  }

  /**
   * @param {object} record 
   * @param {boolean} isPropoundPage 
   * @param {string} form 
   */
  function quickCreateRedirectPath(record, isPropoundPage, form) {
    return {
      type: QUICK_CREATE_REDIRECT_PATH,
      record,
      isPropoundPage,
      form
    }
  }

  /**
   * @param {boolean} status
   * @returns 
   */
  function updateVerifySession(status) {
    return {
      type: UPDATE_VERIFY_SESSION,
      status
    }
  }

  /**
   * @param {boolean} load
   * @param {function} setOpenAlertDialog
   * @param {object} metaData
   * @param {function} open
   * @param {function} setSendQuestions
   * @param {function} setDocument,
   * @param {function} setLoader
   * @param {array} partyIds
   * @returns
   */
   
  function inValidSubscriptions(load, setOpenAlertDialog, metaData, partyIds, open, setSendQuestions, setDocument, setLoader) {
     return {
      type: INVALID_SUBSCRIPTIONS,
      load,
      setOpenAlertDialog,
      metaData,
      partyIds,
      open,
      setSendQuestions,
      setDocument,
      setLoader
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {object} formRecord 
   * @param {function} setShowHistoryForm 
   * @param {function} setOpenForm 
   * @param {string} form 
   */
  function restoreResponseHistory(record, formRecord, setShowHistoryForm, setOpenForm, form) {
    return {
      type: RESTORE_RESPONSE_HISTORY,
      record,
      formRecord,
      setShowHistoryForm,
      setOpenForm,
      form
    }
  }  
  
  /**
   * 
   * @param {object} record 
   * @param {string} form 
   */
  function restoreResponseHistorySuccess(success) {
    return {
      type: RESTORE_RESPONSE_HISTORY_SUCCESS,
      success
    }
  }
  
  /**
   * 
   * @param {object} record 
   * @param {string} form 
   */
  function restoreResponseHistoryError(error) {
    return {
      type: RESTORE_RESPONSE_HISTORY_ERROR,
      error,
    }
  }

  /**
   * @param {boolean} record
   */
  function attorneyResponseTracking(record, caseActions) {
    return {
      type: ATTORNEY_RESPONSE_TRACKING,
      record,
      caseActions
    }
  }

  /**
   * @param {string} success
   */
  function attorneyResponseTrackingSuccess(record, success) {
    return {
      type:ATTORNEY_RESPONSE_TRACKING_SUCCESS,
      record,
      success
    }
  }

  /**
   * @param {string} error
   */
  function attorneyResponseTrackingError(error) {
    return {
      type: ATTORNEY_RESPONSE_TRACKING_ERROR,
      error
    }
  }

  /**
   * @param {object} record 
   */
  function autoSaveFormDetails(record) {
    return {
      type: AUTO_SAVE_FORM_DETAILS,
      record
    }
  }
  
 
  /**
   * @param {object} record 
   */
  function autoSaveFormDetailsSuccess(record) {
    return {
      type: AUTO_SAVE_FORM_DETAILS_SUCCESS,
      record
    }
  }
  
  /**
   * @param {object} record
   */
  function autoSaveFormDetailsError(record) {
    return {
      type: AUTO_SAVE_FORM_DETAILS_ERROR,
      record
    }
  }
  
  /**
   * @param {object} record 
   */
  function clearAutoSaveFormDetails(record) {
    return {
      type: CLEAR_AUTO_SAVE_FORM_DETAILS,
      record
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {object} formRecord 
   * @returns 
   */
  function filevineSessionCommonUpdate(record, form, formRecord) {
    return {
      type: FILEVINE_SESSION_COMMON_UPDATE,
      record,
      form,
      formRecord
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function filevineSessionCommonUpdateSuccess(success) {
    return {
      type: FILEVINE_SESSION_COMMON_UPDATE_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function filevineSessionCommonUpdateError(error) {
    return {
      type: FILEVINE_SESSION_COMMON_UPDATE_ERROR,
      error
    }
  }
  
  /**
   * 
   * @param {object} record 
   */
  function litifyUploadDocument(record) {
    return {
      type: LITIFY_UPLOAD_DOCUMENT,
      record
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function litifyUploadDocumentSuccess(success) {
    return {
      type: LITIFY_UPLOAD_DOCUMENT_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function litifyUploadDocumentError(error) {
    return {
      type: LITIFY_UPLOAD_DOCUMENT_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} records 
   * @param {string} form 
   * @param {function} setLitifyPopup 
   * @param {function} setShowUserForm
   * @returns 
   */
  function createLitifyBulkRecords(records, form, setLitifyPopup, setShowUserForm){
    return {
      type: CREATE_LITIFY_BULK_RECORDS,
      records,
      form,
      setLitifyPopup,
      setShowUserForm
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function createLitifyBulkRecordsSuccess(success) {
    return {
      type: CREATE_LITIFY_BULK_RECORDS_SUCCESS,
      success
    }
  }
  
  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function createLitifyBulkRecordsError(error){
    return {
      type: CREATE_LITIFY_BULK_RECORDS_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {object} integration 
   * @returns 
   */
  function litifyUpdateUserDetails(record, form, integration) {
    return {
      type: LITIFY_UPDATE_USER_DETAILS,
      record,
      form,
      integration
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function litifyUpdateUserDetailsSuccess(success) {
    return {
      type: LITIFY_UPDATE_USER_DETAILS_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function litifyUpdateUserDetailsError(error) {
    return {
      type: LITIFY_UPDATE_USER_DETAILS_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record 
   */
  function plaintiffDefendantSwitch(record) {
    return {
      type:PLAINTIFF_DEFENDANT_SWITCH,
      record
    }
  }

  /**
   * 
   * @param {object} payment 
   * @param {object} cardDetails 
   * @param {string} form  
   * @param {object} actions 
   * @param {function} closeModal 
   * @returns 
   */
  function initialSubscription(payment, cardDetails, form, actions, closeModal) {
    return {
      type: INITIAL_SUBSCRIPTION, 
      payment, 
      cardDetails, 
      form, 
      actions,
      closeModal
    }
  }
  
  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function initialSubscriptionSuccess(success) {
    return {
      type: INITIAL_SUBSCRIPTION_SUCCESS,
      success
    }
  }
  
  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function initialSubscriptionError(error) {
    return {
      type: INITIAL_SUBSCRIPTION_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} details 
   * @returns 
   */
  function storeDefendantDetials(details) {
    return {
      type: STORE_DEFENDANT_DETAILS,
      details
    }
  }

  /**
   * @param {object} record 
   * @param {string} form 
   */
  function createLitifyClientCase(record, form) {
    return {
      type: CREATE_LITIFY_CLIENT_CASE,
      record,
      form
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function createLitifyClientCaseError(error) {
    return {
      type: CREATE_LITIFY_CLIENT_CASE_ERROR,
      error
    }
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function createLitifyClientCaseSuccess(success) {
    return {
      type: CREATE_LITIFY_CLIENT_CASE_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {boolean} status 
   * @returns 
   */

  function manageArchiveCase(status) {
    return {
      type: MANAGE_ARCHIVE_CASE,
      status
    }
  }

  /**
   * @param {object} record
   * @param {function} setProgress 
   * @param {function} setDocEditor
   * @param {function} setLoader
   */
  function loadDocumentEditor(record, setProgress, setDocEditor, setLoader) {
    return {
      type: LOAD_DOCUMENT_EDITOR,      
      record,
      setProgress,
      setDocEditor,
      setLoader
    }
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function loadDocumentEditorSuccess(record) {
    return {
      type: LOAD_DOCUMENT_EDITOR_SUCCESS,
      record
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function loadDocumentEditorError(error) {
    return {
      type: LOAD_DOCUMENT_EDITOR_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {object} myFile
   * @param {function} setDocLoader
   * @param {function} setDocEditor
   * @param {string} optNode
   * @param {object} caseRecord
   * @param {function} setEmailPopup
   * @param {function} setFilevinePopup
   * @param {function} setMyCasePopup
   * @param {function} setLitifyPopup
   * @param {function} setClioPopup
   */
  function saveDocumentEditor(record, myFile, setDocLoader, setDocEditor, optNode, caseRecord, setEmailPopup, setFilevinePopup, setMyCasePopup, setLitifyPopup, setClioPopup, customObject) {
    return {
      type: SAVE_DOCUMENT_EDITOR,
      record,
      myFile,
      setDocLoader,
      setDocEditor,
      optNode,
      caseRecord,
      setEmailPopup,
      setFilevinePopup,
      setMyCasePopup,
      setLitifyPopup,
      setClioPopup,
      customObject
    }
  }

  /**
   * 
   * @param {object} record
   * @param {string} success 
   * @returns 
   */
  function saveDocumentEditorSuccess(record, success) {
    return {
      type: SAVE_DOCUMENT_EDITOR_SUCCESS,
      record,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function saveDocumentEditorError(error) {
    return {
      type: SAVE_DOCUMENT_EDITOR_ERROR,
      error
    }
  }

  function documentEditorStatus(status) {
    return {
      type: DOCUMENT_EDITOR_STATUS,
      status
    }
  }

  /**
   * @param {object} record
   * @param {function} setProgress
   * @param {function} setDocEditor
   * @param {function} setLoader
   */
  function loadPropoundDocumentEditor(record, setProgress, setDocEditor, setLoader) {
    return {
      type: LOAD_PROPOUND_DOCUMENT_EDITOR,
      record,
      setProgress,
      setDocEditor,
      setLoader
    }
  }

  /**
  * 
  * @param {object} record 
  * @returns 
  */
  function loadPropoundDocumentEditorSuccess(record) {
    return {
      type: LOAD_PROPOUND_DOCUMENT_EDITOR_SUCCESS,
      record
    }
  }

  /**
  * 
  * @param {string} error 
  * @returns 
  */
  function loadPropoundDocumentEditorError(error) {
    return {
      type: LOAD_PROPOUND_DOCUMENT_EDITOR_ERROR,
      error
    }
  }

  /**
   * @param {object} editorObj 
   */
  function savePropoundDocumentEditor(editorObj) {
    return {
      type: SAVE_PROPOUND_DOCUMENT_EDITOR,
      editorObj,
    }
  }

  /**
   * 
   * @param {object} record
   * @param {string} success 
   * @returns 
   */
  function savePropoundDocumentEditorSuccess(record, success) {
    return {
      type: SAVE_PROPOUND_DOCUMENT_EDITOR_SUCCESS,
      record,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function savePropoundDocumentEditorError(error) {
    return {
      type: SAVE_PROPOUND_DOCUMENT_EDITOR_ERROR,
      error
    }
  }


  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} closeForm 
   * @returns 
   */
  function updateObjectionForm(record, form, closeForm) {
    return {
      type: UPDATE_OBJECTION_FORM,
      record,
      form,
      closeForm
    };
  }

  /**
   * @param {string} error 
   */
  function updateObjectionFormError(error) {
    return {
      type: UPDATE_OBJECTION_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   */
  function updateObjectionFormSuccess(record) {
    return {
      type: UPDATE_OBJECTION_FORM_SUCCESS,
      record
    };
  }

  /**
   * @param {object} record 
   * @param {function} closeForm 
   * @param {function} setLoader 
   */
  function deleteAttorneyObjection(record, closeForm, setLoader) {
    return {
      type: DELETE_ATTORNEY_OBJECTION,
      record,
      closeForm,
      setLoader
    }
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function deleteAttorneyObjectionSuccess(record) {
    return {
      type: DELETE_ATTORNEY_OBJECTION_SUCCESS,
      record
    }
  }

  function deleteAttorneyObjectionError(error) {
    return {
      type: DELETE_ATTORNEY_OBJECTION_ERROR,
      error
    }
  }

  /**
   * @param {object} record 
   * @param {string} form 
   * @returns 
   */
  function autoSaveObjectionForm(record, form) {
    return {
      type: AUTO_SAVE_OBJECTION_FORM,
      record,
      form
    };
  }

  /**
   * @param {string} success 
   */
  function autoSaveObjectionFormSuccess(success) {
    return {
      type: AUTO_SAVE_OBJECTION_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function autoSaveObjectionFormError(error) {
    return {
      type: AUTO_SAVE_OBJECTION_FORM_ERROR,
      error,
    };
  }

  /**
   * 
   * @param {boolean} status 
   * @returns 
   */
  function eServePopupStatus(status) {
    return {
      type: ESERVE_POPUP_STATUS,
      status
    }
  }

  /**
   * @param {object} record
   * @param {function} setEditFile
   * @param {function} setLoader
   */
  function saveDiscoveryFilename(record, setEditFile, setLoader) {
    return {
      type: SAVE_DISCOVERY_FILENAME,
      record,
      setEditFile,
      setLoader
    }
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function saveDiscoveryFilenameSuccess(record) {
    return {
      type: SAVE_DISCOVERY_FILENAME_SUCCESS,
      record
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function saveDiscoveryFilenameError(error) {
    return {
      type: SAVE_DISCOVERY_FILENAME_ERROR,
      error
    }
  }

  /**
   * @param {object} record
   * @param {function} setEditFile
   * @param {function} setLoader
   */
  function savePropoundFilename(record, setEditFile, setLoader) {
    return {
      type: SAVE_PROPOUND_FILENAME,
      record,
      setEditFile,
      setLoader
    }
  }

  /**
  * 
  * @param {object} record 
  * @returns 
  */
  function savePropoundFilenameSuccess(record) {
    return {
      type: SAVE_PROPOUND_FILENAME_SUCCESS,
      record
    }
  }

  /**
  * 
  * @param {string} error 
  * @returns 
  */
  function savePropoundFilenameError(error) {
    return {
      type: SAVE_PROPOUND_FILENAME_ERROR,
      error
    }
  }

  /**
   * 
   * @param {boolean} content 
   * @returns 
   */
  function QuestionsNotFound(content) {
    return {
      type: QUESTIONS_NOT_FOUND,
      content
    }
  }

  /**
  * 
  * @param {object} record
  * @param {boolean} setShowUploadQuestionsForm 
  * @returns 
  */
  function updateQuestions(record, setShowUploadQuestionsForm) {
    return {
      type: UPDATE_QUESTIONS,
      record,
      setShowUploadQuestionsForm
    }
  }

  /**
   * @param {string} success 
   */
  function updateQuestionsSuccess(success) {
    return {
      type: UPDATE_QUESTIONS_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function updateQuestionsError(error) {
    return {
      type: UPDATE_QUESTIONS_ERROR,
      error
    }
  }


  /**
  * 
  * @param {object} record
  * @param {boolean} setShowDeleteHashQuestionsForm 
  * @returns 
  */
  function DeleteHashWithFile(record, setShowDeleteHashQuestionsForm) {
    return {
      type: DELETE_HASH_WITH_FILE,
      record,
      setShowDeleteHashQuestionsForm
    }
  }

  /**
   * @param {string} success 
   */
  function DeleteHashWithFileSuccess(success) {
    return {
      type: DELETE_HASH_WITH_FILE_SUCCESS,
      success
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function DeleteHashWithFileError(error) {
    return {
      type: DELETE_HASH_WITH_FILE_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function loadFrogsQuestions(record) {
    return {
      type: LOAD_FROGS_QUESTIONS,
      record
    }
  }

  /**
   * 
   * @param {Array} records 
   * @returns 
   */
  function loadFrogsQuestionsSuccess(records) {
    return {
      type: LOAD_FROGS_QUESTIONS_SUCCESS,
      records
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function loadFrogsQuestionsError(error) {
    return {
      type: LOAD_FROGS_QUESTIONS_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} closeForm 
   * @param {boolean} subGroupForm 
   * @returns 
   */
  function updateFrogsForm(record, form, closeForm, subGroupForm) {
    return {
      type: UPDATE_FROGS_FORM,
      record,
      form,
      closeForm,
      subGroupForm
    };
  }

  /**
   * @param {object} record 
   */
  function updateFrogsFormSuccess(record) {
    return {
      type: UPDATE_FROGS_FORM_SUCCESS,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function updateFrogsFormError(error) {
    return {
      type: UPDATE_FROGS_FORM_ERROR,
      error
    };
  }


  /**
   * 
   * @param {object} record 
   * @param {string} form 
   * @param {function} closeForm 
   * @returns 
   */
  function updateFrogsObjectionForm(record, form, closeForm) {
    return {
      type: UPDATE_FROGS_OBJECTION_FORM,
      record,
      form,
      closeForm
    };
  }

  /**
   * @param {object} record 
   */
  function updateFrogsObjectionFormSuccess(record) {
    return {
      type: UPDATE_FROGS_OBJECTION_FORM_SUCCESS,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function updateFrogsObjectionFormError(error) {
    return {
      type: UPDATE_FROGS_OBJECTION_FORM_ERROR,
      error
    };
  }

  /**
   * @param {object} record 
   * @param {function} closeForm 
   * @param {function} setLoader 
   */
  function deleteAttorneyFrogsObjection(record, closeForm, setLoader) {
    return {
      type: DELETE_ATTORNEY_FROGS_OBJECTION,
      record,
      closeForm,
      setLoader
    }
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function deleteAttorneyFrogsObjectionSuccess(record) {
    return {
      type: DELETE_ATTORNEY_FROGS_OBJECTION_SUCCESS,
      record
    }
  }

  function deleteAttorneyFrogsObjectionError(error) {
    return {
      type: DELETE_ATTORNEY_FROGS_OBJECTION_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record 
   * @param {function} setOpenForm 
   * @returns 
   */
  function saveSubGroupFrogsForm(record, setOpenForm) {
    return {
      type: SAVE_SUB_GROUP_FROGS_FORM,
      record,
      setOpenForm
    };
  }

  /**
   * 
   * @param {string} success 
   * @returns 
   */
  function saveSubGroupFrogsFormSuccess(success) {
    return {
      type: SAVE_SUB_GROUP_FROGS_FORM_SUCCESS,
      success
    };
  }

  /**
   * @param {string} error 
   */
  function saveSubGroupFrogsFormError(error) {
    return {
      type: SAVE_SUB_GROUP_FROGS_FORM_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   */
  function editPropoundForm(record) {
    return {
      type: EDIT_PROPOUND_FORM,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function editPropoundFormError(error) {
    return {
      type: EDIT_PROPOUND_FORM_ERROR,
      error
    };
  }

  /**
   * @param {string} success 
   */
  function editPropoundFormSuccess(record, success) {
    return {
      type: EDIT_PROPOUND_FORM_SUCCESS,
      record,
      success
    };
  }

  /**
   * 
   * @param {object} record 
   * @param {function} modalForm 
   * @param {function} setLinkLoader 
   * @returns 
   */
  function addFrogsQuestions(record, modalForm, setLinkLoader) {
    return {
      type: ADD_FROGS_QUESTIONS,
      record,
      modalForm,
      setLinkLoader
    }
  }
  
  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function addFrogsQuestionsSuccess(record) {
    return {
      type: ADD_FROGS_QUESTIONS_SUCCESS,
      record
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function addFrogsQuestionsError(error) {
    return {
      type: ADD_FROGS_QUESTIONS_ERROR,
      error
    }
  }

  /**
   * 
   * @param {object} record
   * @param {function} setFormLoader
   */
  function deleteDuplicateSet(record, setFormLoader) {
    return {
      type: DELETE_DUPLICATE_SET,
      record,
      setFormLoader
    }
  }
  
  
  /**
   * 
   * @param {object} record
   * @param {string} success
   */
  function deleteDuplicateSetSuccess(success) {
    return {
      type: DELETE_DUPLICATE_SET_SUCCESS,
      success
    }
  }
  
  
  /**
   * 
   * @param {object} record
   */
  function deleteDuplicateSetError(error) {
    return {
      type: DELETE_DUPLICATE_SET_ERROR,
      error
    }
  }
  
  /**
   * 
   * @param {object} record
   */
  function deleteConnectedQuestionSet(record) {
    return {
      type: DELETE_CONNECTED_QUESTION_SET,
      record      
    }
  }
  
  
  /**
   * 
   * @param {object} record
   * @param {string} success
   */
  function deleteConnectedQuestionSetSuccess(success) {
    return {
      type: DELETE_CONNECTED_QUESTION_SET_SUCCESS,
      success
    }
  }
  
  
  /**
   * 
   * @param {object} record
   */
  function deleteConnectedQuestionSetError(error) {
    return {
      type: DELETE_CONNECTED_QUESTION_SET_ERROR,
      error
    }
  }

 
  function fetchDiscoveryHistory(legalforms_id, setModal, discoveryData, openDocument) {
    return {
      type: FETCH_DISCOVERY_HISTORY,
      legalforms_id,
      setModal,
      discoveryData,
      openDocument
    }
  }

  function fetchDiscoveryHistorySuccess(discoveryHistory) {
    return {
      type: FETCH_DISCOVERY_HISTORY_SUCCESS,
      discoveryHistory      
    }
  }

  function fetchDiscoveryHistoryError(error) {
    return {
      type: FETCH_DISCOVERY_HISTORY_ERROR,
      error      
    }
  }

  function getSFDTtext(sfdt_s3key, setDocumentLoader, setSfdtData) {
    return {
      type: GET_SFDT_TEXT,
      sfdt_s3key,
      setDocumentLoader,
      setSfdtData  
    }
  }

  function getSFDTtextSuccess(success) {
    return {
      type: GET_SFDT_TEXT_SUCCESS,
      success      
    }
  }

  function getSFDTtextError(error) {
    return {
      type: GET_SFDT_TEXT_ERROR,
      error      
    }
  }


  return {
    uploadForm,
    uploadFormError,
    generateForm,
    generateFormError,
    cancelForm,
    cancelFormError,
    saveForm,
    saveFormError,
    saveFormSuccess,
    loadForm,
    loadFormError,
    loadFormSuccess,
    updateForm,
    updateFormError,
    updateFormSuccess,
    fetchForm,
    fetchFormError,
    fetchFormSuccess,
    generateDocument,
    generateDocumentError,
    generateDocumentSuccess,
    saveDocument,
    saveDocumentError,
    saveDocumentSuccess,
    sendQuestions,
    sendQuestionsError,
    sendQuestionsSuccess,
    loadBillingDetails,
    loadBillingDetailsError,
    loadBillingDetailsSuccess,
    updateBillingDetails,
    updateBillingDetailsError,
    updateBillingDetailsSuccess,
    updateKey,
    deleteForm,
    deleteFormError,
    deleteFormSuccess,
    saveStandardForm,
    saveStandardFormError,
    saveStandardFormSuccess,
    saveQuestionsForm,
    saveQuestionsFormError,
    saveQuestionsFormSuccess,
    createMedicalHistory,
    createMedicalHistoryError,
    createMedicalHistorySuccess,
    updateMedicalHistory,
    updateMedicalHistoryError,
    updateMedicalHistorySuccess,
    deleteMedicalHistory,
    deleteMedicalHistoryError,
    deleteMedicalHistorySuccess,
    viewMedicalHistory,
    viewMedicalHistoryError,
    viewMedicalHistorySuccess,
    generateMedicalSummary,
    generateMedicalSummaryError,
    generateMedicalSummarySuccess,
    requestHelp,
    requestHelpError,
    requestHelpSuccess,
    requestTranslation,
    requestTranslationError,
    mergeTranslation,
    mergeTranslationError,
    mergeTranslationSuccess,
    sendVerification,
    sendVerificationError,
    sendVerificationSuccess,
    autoSaveForm,
    autoSaveFormError,
    autoSaveFormSuccess,
    copyToLawyerResponse,
    copyToLawyerResponseError,
    copyToLawyerResponseSuccess,
    shred,
    shredError,
    shredSuccess,
    editForm,
    editFormError,
    editFormSuccess,
    attachCase,
    attachCaseSuccess,
    attachCaseError,
    createClientRecord,
    createClientRecordSuccess,
    createClientRecordError,
    setResponseDate,
    setResponseDateSuccess,
    setResponseDateError,
    resendQuestions,
    resendQuestionsSuccess,
    resendQuestionsError,
    saveAllForm,
    saveAllFormError,
    saveAllFormSuccess,
    saveSubGroupForm,
    saveSubGroupFormError,
    saveSubGroupFormSuccess,
    generatePOSDocument,
    generatePOSDocumentError,
    generatePOSDocumentSuccess,
    copyAllToLawyerResponse,
    copyAllToLawyerResponseError,
    copyAllToLawyerResponseSuccess,
    saveComment,
    saveCommentError,
    saveCommentSuccess,
    archiveCase,
    archiveCaseSuccess,
    archiveCaseError,
    unArchiveCase,
    unArchiveCaseSuccess,
    unArchiveCaseError,
    addConsultation,
    addConsultationError,
    addConsultationSuccess,
    deleteConsultation,
    deleteConsultationError,
    deleteConsultationSuccess,
    updateEditedFlag,
    updateEditedFlagError,
    updateEditedFlagSuccess,
    savePropoundForm,
    savePropoundFormError,
    savePropoundFormSuccess,
    loadPropoundRecord,
    loadPropoundRecordError,
    loadPropoundRecordSuccess,
    loadPropoundForm,
    loadPropoundFormError,
    loadPropoundFormSuccess,
    deletePropoundForm,
    deletePropoundFormError,
    deletePropoundFormSuccess,
    updatePropoundForm,
    updatePropoundFormSuccess,
    updatePropoundFormError,
    generatePropoundDocument,
    generatePropoundDocumentSuccess,
    generatePropoundDocumentError,
    savePropoundFormS3Key,
    savePropoundFormS3KeySuccess,
    savePropoundFormS3KeyError,
    sendPropoundQuestions,
    sendPropoundQuestionsError,
    sendPropoundQuestionsSuccess,
    savePropounderServingAttorney,
    savePropounderServingAttorneyError,
    savePropounderServingAttorneySuccess,
    questionsTemplate,
    questionsTemplateError,
    questionsTemplateSuccess,
    destroyStandardForm,
    contentNotFound,
    createClientCase,
    createClientCaseError,
    createClientCaseSuccess,
    updatePlanDetails,
    updatePlanDetailsSuccess,
    updatePlanDetailsError,
    sendRespondQuestions,
    sendRespondQuestionsSuccess,
    sendRespondQuestionsError,
    quickCreateSaveStandardForm,
    quickCreateSaveStandardFormError,
    quickCreateSaveStandardFormSuccess,
    quickCreateSaveQuestionsForm,
    quickCreateSaveQuestionsFormError,
    quickCreateSaveQuestionsFormSuccess,
    deleteQuickCreateDocument,
    deleteQuickCreateDocumentError,
    deleteQuickCreateDocumentSuccess,
    updateUserRecord,
    updateUserRecordSuccess,
    updateUserRecordError,
    quickCreateRedirectPath,
    updateVerifySession,
    inValidSubscriptions,
    restoreResponseHistory,
    restoreResponseHistoryError,
    restoreResponseHistorySuccess,
    attorneyResponseTracking,
    attorneyResponseTrackingError,
    attorneyResponseTrackingSuccess,
    autoSaveFormDetails,
    autoSaveFormDetailsSuccess,
    autoSaveFormDetailsError,
    clearAutoSaveFormDetails,
    filevineSessionCommonUpdate,
    filevineSessionCommonUpdateSuccess,
    filevineSessionCommonUpdateError,
    litifyUploadDocument,
    litifyUploadDocumentSuccess,
    litifyUploadDocumentError,
    createLitifyBulkRecords,
    createLitifyBulkRecordsSuccess,
    createLitifyBulkRecordsError,
    litifyUpdateUserDetails,
    litifyUpdateUserDetailsSuccess,
    litifyUpdateUserDetailsError,
    plaintiffDefendantSwitch,
    initialSubscription,
    initialSubscriptionSuccess,
    initialSubscriptionError,
    storeDefendantDetials,
    createLitifyClientCase,
    createLitifyClientCaseSuccess,
    createLitifyClientCaseError,
    manageArchiveCase,
    loadDocumentEditor,
    loadDocumentEditorSuccess,
    loadDocumentEditorError,
    saveDocumentEditor,
    saveDocumentEditorSuccess,
    saveDocumentEditorError,
    documentEditorStatus,
    loadPropoundDocumentEditor,
    loadPropoundDocumentEditorSuccess,
    loadPropoundDocumentEditorError,
    savePropoundDocumentEditor,
    savePropoundDocumentEditorSuccess,
    savePropoundDocumentEditorError,
    updateObjectionForm,
    updateObjectionFormSuccess,
    updateObjectionFormError,
    deleteAttorneyObjection,
    deleteAttorneyObjectionSuccess,
    deleteAttorneyObjectionError,
    autoSaveObjectionForm,
    autoSaveObjectionFormSuccess,
    autoSaveObjectionFormError,
    eServePopupStatus,
    saveDiscoveryFilename,
    saveDiscoveryFilenameSuccess,
    saveDiscoveryFilenameError,
    savePropoundFilename,
    savePropoundFilenameSuccess,
    savePropoundFilenameError,
    QuestionsNotFound,
    updateQuestions,
    updateQuestionsSuccess,
    updateQuestionsError,
    DeleteHashWithFile,
    DeleteHashWithFileSuccess,
    DeleteHashWithFileError,
    loadFrogsQuestions,
    loadFrogsQuestionsSuccess,
    loadFrogsQuestionsError,
    updateFrogsForm,
    updateFrogsFormSuccess,
    updateFrogsFormError,
    updateFrogsObjectionForm,
    updateFrogsObjectionFormSuccess,
    updateFrogsObjectionFormError,
    deleteAttorneyFrogsObjection,
    deleteAttorneyFrogsObjectionSuccess,
    deleteAttorneyFrogsObjectionError,
    saveSubGroupFrogsForm,
    saveSubGroupFrogsFormSuccess,
    saveSubGroupFrogsFormError,
    editPropoundForm,
    editPropoundFormSuccess,
    editPropoundFormError,
    addFrogsQuestions,
    addFrogsQuestionsSuccess,
    addFrogsQuestionsError,
    deleteDuplicateSet,
    deleteDuplicateSetSuccess,
    deleteDuplicateSetError,
    deleteConnectedQuestionSet,
    deleteConnectedQuestionSetSuccess,
    deleteConnectedQuestionSetError,
    fetchDiscoveryHistory,
    fetchDiscoveryHistorySuccess,
    fetchDiscoveryHistoryError,
    getSFDTtext,
    getSFDTtextSuccess,
    getSFDTtextError
  }
}
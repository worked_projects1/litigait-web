/**
 * 
 * cases selectors
 * 
 */

import { createSelector } from 'reselect';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function selectors(name) {
    const selectDomain = () => (state) => state[name] || state.settings || state['rest/document-extraction-progress'] || false;
    false;

    const selectBilling = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.billing || {},
    );
    
    
    return {
        selectDomain,
        selectBilling
    }
}




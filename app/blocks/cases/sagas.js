/*
 *
 *  cases sagas
 *
 */

import { push } from 'react-router-redux';
import { call, take, put, race, all, select, delay, takeLatest, fork, cancel, cancelled } from 'redux-saga/effects';
import { startSubmit, stopSubmit, destroy, change, initialize } from 'redux-form/immutable';
import { selectUser } from 'blocks/session/selectors';
import appRemotes from 'blocks/records/remotes';
import history from 'utils/history';
import { download, viewPDF, getInvoicePdfJson, CreatePDF, getDuplicates, isPropound, isRespondEntityUrl, TimeOutArr, stateListPOS, decryptApiKeyAndSecret } from 'utils/tools';
import { s3BucketPrivate } from 'blocks/upload/remotes';
import { getContentType } from 'components/FileUpload/utils';
import moment from 'moment';
import lodash from 'lodash';
import { reset } from 'redux-form';
import { createSubscriptionPlan, verifySession } from 'blocks/session/remotes';
import { verifySessionSuccess, getLitifyBulkRecordsSuccess, verifySession as verifySessionAction } from 'blocks/session/actions';
const CryptoJS = require("crypto-js");
const base64 = require('base-64');
import store2 from 'store2';

import schema from 'routes/schema';


import {
  DEFAULT_UPLOAD_FORM_ERROR,
  DEFAULT_GENERATE_FORM_ERROR,
  DEFAULT_CANCEL_FORM_ERROR,
  DEFAULT_SAVE_FORM_ERROR,
  DEFAULT_LOAD_FORM_ERROR,
  DEFAULT_UPDATE_FORM_ERROR,
  DEFAULT_FETCH_FORM_ERROR,
  DEFAULT_GENERATE_DOCUMENT_ERROR,
  DEFAULT_SAVE_DOCUMENT_ERROR,
  DEFAULT_SEND_QUESTIONS_ERROR,
  DEFAULT_LOAD_BILLING_DETAILS_ERROR,
  DEFAULT_UPDATE_BILLING_DETAILS_ERROR,
  DEFAULT_DELETE_FORM_ERROR,
  DEFAULT_SAVE_STANDARD_FORM_ERROR,
  DEFAULT_SAVE_QUESTIONS_FORM_ERROR,
  DEFAULT_SAVE_QUESTIONS_FORM_VALIDATE_ERROR,
  DEFAULT_CREATE_MEDICAL_HISTORY_ERROR,
  DEFAULT_UPDATE_MEDICAL_HISTORY_ERROR,
  DEFAULT_DELETE_MEDICAL_HISTORY_ERROR,
  DEFAULT_VIEW_MEDICAL_HISTORY_ERROR,
  DEFAULT_GENERATE_MEDICAL_SUMMARY_ERROR,
  DEFAULT_DOCUMENT_ERROR,
  DEFAULT_REQUEST_HELP_ERROR,
  DEFAULT_REQUEST_TRANSLATION_ERROR,
  DEFAULT_MERGE_TRANSLATION_ERROR,
  DEFAULT_SEND_VERIFICATION_ERROR,
  DEFAULT_AUTO_SAVE_FORM_ERROR,
  DEFAULT_COPY_TO_LAWYER_RESPONSE_ERROR,
  DEFAULT_SHRED_ERROR,
  DEFAULT_EDIT_FORM_ERROR,
  DEFAULT_ATTACH_CASE_ERROR,
  DEFAULT_CREATE_CLIENT_RECORD_ERROR,
  DEFAULT_SET_RESPONSE_DATE_ERROR,
  DEFAULT_RESEND_QUESTIONS_ERROR,
  DEFAULT_SAVE_ALL_FORM_ERROR,
  DEFAULT_SAVE_SUB_GROUP_FORM_ERROR,
  DEFAULT_GENERATE_POS_DOCUMENT_ERROR,
  DEFAULT_COPY_ALL_TO_LAWYER_RESPONSE_ERROR,
  DEFAULT_ARCHIVE_CASE_ERROR,
  DEFAULT_UNARCHIVE_CASE_ERROR,
  DEFAULT_SAVE_PROPOUND_FORM_ERROR,
  DEFAULT_GENERATE_PROPOUND_DOCUMENT_ERROR,
  DEFAULT_SAVE_PROPOUND_DOCUMENT_ERROR,
  DEFAULT_SEND_PROPOUND_QUESTIONS_ERROR,
  DEFAULT_DOCUMENT_CONTENT_NOT_FOUND_ERROR,
  DEFAULT_SEND_RESPOND_QUESTIONS_ERROR,
  DEFAULT_ATTORNEY_RESPONSE_TRACKING_ERROR,
  DEFAULT_FILEVINE_SESSION_ERROR,
  DEFAULT_ATTORNEY_OBJECTION_ERROR,
  DEFAULT_QUESTIONS_NOT_FOUND_ERROR
} from 'utils/errors';

import { uploadFile, setFilevineAuthToken, uploadDocument, setFilevineBaseUrl } from 'utils/api';

import { selectForm } from 'blocks/session/selectors';

import records from 'blocks/records';

const casesSaga = records('cases');
const supportRequestSaga = records('rest/help-request');
const { actions: casesActions, selectors: casesSelectors, remotes: casesRemotes } = casesSaga;
const { selectRecord, selectRecords, selectDefendantPracticeDetails, selectDocumentStatus, selectEServePopup, selectFrogsQuestions, selectDynamicFrogsQuestions } = casesSelectors;
const { loadRecordsCacheHit, updateRecordSuccess, clearIndividualStoreRecord, litifyInfoDialog: litifyInfoDialogAction } = casesActions;
const { updateRecord, filevineApiSession, storeFilevineSecret } = casesRemotes;
const standardFormColumns = schema().cases().standardForm;

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    UPLOAD_FORM,
    GENERATE_FORM,
    CANCEL_FORM,
    SAVE_FORM,
    LOAD_FORM,
    UPDATE_FORM,
    FETCH_FORM,
    GENERATE_DOCUMENT,
    SAVE_DOCUMENT,
    SEND_QUESTIONS,
    LOAD_BILLING_DETAILS,
    UPDATE_BILLING_DETAILS,
    DELETE_FORM,
    SAVE_STANDARD_FORM,
    SAVE_QUESTIONS_FORM,
    CREATE_MEDICAL_HISTORY,
    UPDATE_MEDICAL_HISTORY,
    DELETE_MEDICAL_HISTORY,
    VIEW_MEDICAL_HISTORY,
    GENERATE_MEDICAL_SUMMARY,
    REQUEST_HELP,
    REQUEST_TRANSLATION,
    MERGE_TRANSLATION,
    SEND_VERIFICATION,
    AUTO_SAVE_FORM,
    COPY_TO_LAWYER_RESPONSE,
    SHRED,
    EDIT_FORM,
    ATTACH_CASE,
    CREATE_CLIENT_RECORD,
    SET_RESPONSE_DATE,
    SET_RESPONSE_DATE_ERROR,
    RESEND_QUESTIONS,
    SAVE_ALL_FORM,
    SAVE_SUB_GROUP_FORM,
    GENERATE_POS_DOCUMENT,
    COPY_ALL_TO_LAWYER_RESPONSE,
    SAVE_COMMENT,
    ARCHIVE_CASE,
    UNARCHIVE_CASE,
    ADD_CONSULTATION,
    DELETE_CONSULTATION,
    UPDATE_EDITED_FLAG,
    SAVE_PROPOUND_FORM,
    LOAD_PROPOUND_RECORD,
    LOAD_PROPOUND_FORM,
    DELETE_PROPOUND_FORM,
    UPDATE_PROPOUND_FORM,
    GENERATE_PROPOUND_DOCUMENT,
    SAVE_PROPOUND_FORM_S3KEY,
    SEND_PROPOUND_QUESTIONS,
    SAVE_PROPOUNDER_SERVING_ATTORNEY,
    QUESTION_TEMPLATE,
    DESTROY_STANDARD_FORM,
    CREATE_CLIENT_CASE,
    UPDATE_PLAN_DETAILS,
    SEND_RESPOND_QUESTIONS,
    QUICK_CREATE_SAVE_STANDARD_FORM,
    QUICK_CREATE_SAVE_QUESTIONS_FORM,
    DELETE_QUICK_CREATE_DOCUMENT,
    UPDATE_USER_RECORD,
    QUICK_CREATE_REDIRECT_PATH,
    UPDATE_VERIFY_SESSION,
    INVALID_SUBSCRIPTIONS,
    RESTORE_RESPONSE_HISTORY,
    ATTORNEY_RESPONSE_TRACKING,
    FILEVINE_SESSION_COMMON_UPDATE,
    LITIFY_UPLOAD_DOCUMENT,
    CREATE_LITIFY_BULK_RECORDS,
    LITIFY_UPDATE_USER_DETAILS,
    PLAINTIFF_DEFENDANT_SWITCH,
    INITIAL_SUBSCRIPTION,
    CREATE_LITIFY_CLIENT_CASE,
    LOAD_DOCUMENT_EDITOR,
    SAVE_DOCUMENT_EDITOR,
    LOAD_PROPOUND_DOCUMENT_EDITOR,
    SAVE_PROPOUND_DOCUMENT_EDITOR,
    UPDATE_OBJECTION_FORM,
    DELETE_ATTORNEY_OBJECTION,
    AUTO_SAVE_FORM_DETAILS,
    SAVE_DISCOVERY_FILENAME,
    SAVE_PROPOUND_FILENAME,
    UPDATE_QUESTIONS,
    DELETE_HASH_WITH_FILE,
    LOAD_FROGS_QUESTIONS,
    UPDATE_FROGS_FORM,
    UPDATE_FROGS_OBJECTION_FORM,
    DELETE_ATTORNEY_FROGS_OBJECTION,
    SAVE_SUB_GROUP_FROGS_FORM,
    EDIT_PROPOUND_FORM,
    ADD_FROGS_QUESTIONS,
    DELETE_DUPLICATE_SET,
    DELETE_CONNECTED_QUESTION_SET,
    FETCH_DISCOVERY_HISTORY,
    GET_SFDT_TEXT
  } = constants;

  const {
    uploadFormError,
    generateForm: generateFormAction,
    generateFormError,
    cancelFormError,
    saveForm: SaveFormAction,
    saveFormError,
    saveFormSuccess,
    loadForm: loadFormAction,
    loadFormError,
    loadFormSuccess,
    updateForm: updateFormAction,
    updateFormError,
    updateFormSuccess,
    fetchForm: fetchFormAction,
    fetchFormError,
    fetchFormSuccess,
    generateDocument: generateDocumentAction,
    generateDocumentError,
    generateDocumentSuccess,
    saveDocument: saveDocumentAction,
    saveDocumentError,
    saveDocumentSuccess,
    sendQuestionsError,
    sendQuestionsSuccess,
    loadBillingDetails: loadBillingDetailsAction,
    loadBillingDetailsError,
    loadBillingDetailsSuccess,
    updateBillingDetailsError,
    updateBillingDetailsSuccess,
    updateKey,
    deleteFormError,
    deleteFormSuccess,
    saveStandardFormError,
    saveStandardFormSuccess,
    saveQuestionsFormError,
    saveQuestionsFormSuccess,
    createMedicalHistoryError,
    createMedicalHistorySuccess,
    updateMedicalHistoryError,
    updateMedicalHistorySuccess,
    deleteMedicalHistoryError,
    deleteMedicalHistorySuccess,
    viewMedicalHistoryError,
    viewMedicalHistorySuccess,
    generateMedicalSummary: generateMedicalSummaryAction,
    generateMedicalSummaryError,
    generateMedicalSummarySuccess,
    requestHelpError,
    requestHelpSuccess,
    requestTranslationError,
    mergeTranslationError,
    mergeTranslationSuccess,
    sendVerification: sendVerificationAction,
    sendVerificationError,
    sendVerificationSuccess,
    autoSaveFormError,
    autoSaveFormSuccess,
    copyToLawyerResponseError,
    copyToLawyerResponseSuccess,
    shredError,
    shredSuccess,
    editFormError,
    editFormSuccess,
    attachCaseSuccess,
    attachCaseError,
    createClientRecordSuccess,
    createClientRecordError,
    setResponseDateSuccess,
    setResponseDateError,
    resendQuestionsSuccess,
    resendQuestionsError,
    saveAllFormSuccess,
    saveAllFormError,
    saveSubGroupFormSuccess,
    saveSubGroupFormError,
    generatePOSDocument: generatePOSDocumentAction,
    generatePOSDocumentError,
    generatePOSDocumentSuccess,
    copyAllToLawyerResponseError,
    copyAllToLawyerResponseSuccess,
    saveCommentError,
    saveCommentSuccess,
    archiveCaseError,
    archiveCaseSuccess,
    unArchiveCaseError,
    unArchiveCaseSuccess,
    addConsultationError,
    addConsultationSuccess,
    deleteConsultationError,
    deleteConsultationSuccess,
    updateEditedFlagError,
    updateEditedFlagSuccess,
    savePropoundFormError,
    savePropoundFormSuccess,
    loadPropoundRecordError,
    loadPropoundRecordSuccess,
    loadPropoundFormError,
    loadPropoundFormSuccess,
    deletePropoundFormError,
    deletePropoundFormSuccess,
    updatePropoundFormError,
    updatePropoundFormSuccess,
    generatePropoundDocument: generatePropoundDocumentAction,
    generatePropoundDocumentError,
    generatePropoundDocumentSuccess,
    savePropoundFormS3Key: savePropoundFormS3KeyAction, 
    savePropoundFormS3KeySuccess,
    savePropoundFormS3KeyError,
    sendPropoundQuestionsError,
    sendPropoundQuestionsSuccess,
    savePropounderServingAttorneyError,
    savePropounderServingAttorneySuccess,
    questionsTemplateError,
    questionsTemplateSuccess,
    createClientCaseError,
    createClientCaseSuccess,
    contentNotFound,
    updatePlanDetailsSuccess,
    updatePlanDetailsError,
    sendRespondQuestionsSuccess,
    sendRespondQuestionsError,
    quickCreateSaveQuestionsFormError,
    quickCreateSaveQuestionsFormSuccess,
    quickCreateSaveStandardFormError,
    quickCreateSaveStandardFormSuccess,
    deleteQuickCreateDocument: deleteQuickCreateDocumentAction,
    deleteQuickCreateDocumentError,
    deleteQuickCreateDocumentSuccess,
    updateUserRecordError,
    quickCreateRedirectPath: quickCreateRedirectPathAction,
    restoreResponseHistoryError,
    restoreResponseHistorySuccess,
    attorneyResponseTrackingError,
    attorneyResponseTrackingSuccess,
    clearAutoSaveFormDetails,
    filevineSessionCommonUpdateSuccess,
    filevineSessionCommonUpdateError,
    litifyUploadDocumentSuccess,
    litifyUploadDocumentError,
    createLitifyBulkRecords: createLitifyBulkRecordsAction,
    createLitifyBulkRecordsSuccess,
    createLitifyBulkRecordsError,
    litifyUpdateUserDetailsError,
    initialSubscriptionSuccess,
    initialSubscriptionError,
    storeDefendantDetials: storeDefendantDetialsAction,
    createLitifyClientCaseSuccess,
    createLitifyClientCaseError,
    loadDocumentEditor: loadDocumentEditorAction,
    loadDocumentEditorSuccess,
    loadDocumentEditorError,
    saveDocumentEditorSuccess,
    saveDocumentEditorError,
    documentEditorStatus: documentEditorStatusAction,
    loadPropoundDocumentEditor: loadPropoundDocumentEditorAction,
    loadPropoundDocumentEditorSuccess,
    loadPropoundDocumentEditorError,
    savePropoundDocumentEditorSuccess,
    savePropoundDocumentEditorError,
    updateObjectionFormSuccess,
    updateObjectionFormError,
    deleteAttorneyObjectionSuccess,
    deleteAttorneyObjectionError,
    autoSaveFormDetailsSuccess,
    autoSaveFormDetailsError,
    eServePopupStatus,
    saveDiscoveryFilenameSuccess,
    saveDiscoveryFilenameError,
    savePropoundFilenameSuccess,
    savePropoundFilenameError,
    QuestionsNotFound,
    updateQuestionsSuccess,
    updateQuestionsError,
    DeleteHashWithFileSuccess,
    DeleteHashWithFileError,
    loadFrogsQuestions: loadFrogsQuestionsAction,
    loadFrogsQuestionsSuccess,
    loadFrogsQuestionsError,
    updateFrogsForm: updateFrogsFormAction,
    updateFrogsFormSuccess,
    updateFrogsFormError,
    updateFrogsObjectionFormSuccess,
    updateFrogsObjectionFormError,
    deleteAttorneyFrogsObjectionSuccess,
    deleteAttorneyFrogsObjectionError,
    saveSubGroupFrogsFormSuccess,
    saveSubGroupFrogsFormError,
    editPropoundFormSuccess,
    editPropoundFormError,
    addFrogsQuestionsSuccess,
    addFrogsQuestionsError,
    deleteDuplicateSetSuccess,
    deleteDuplicateSetError,
    deleteConnectedQuestionSetSuccess,
    deleteConnectedQuestionSetError,
    fetchDiscoveryHistory,
    fetchDiscoveryHistorySuccess,
    fetchDiscoveryHistoryError,
    getSFDTtextSuccess,
    getSFDTtextError
  } = actions;


  const {
    uploadForm,
    generateForm,
    cancelForm,
    saveForm,
    loadForm,
    updateForm,
    fetchForm,
    generateDocument,
    saveDocument,
    sendQuestions,
    loadBillingDetails,
    getClientSecret,
    updateBillingDetails,
    deleteForm,
    createMedicalHistory,
    updateMedicalHistory,
    deleteMedicalHistory,
    viewMedicalHistory,
    extractTotalPages,
    generateMedicalSummary,
    getDocumentStatus,
    requestHelp,
    requestTranslation,
    sendVerification,
    shred,
    editForm,
    attachCase,
    createClientRecord,
    setResponseDate,
    resendQuestions,
    saveAllForm,
    sendInvoice,
    saveSubGroupForm,
    generatePOSDocument,
    copyAllToLawyerResponse,
    archiveCase,
    unArchiveCase,
    addConsultation,
    deleteConsultation,
    updateEditedFlag,
    autoSaveForms,
    savePropoundForm,
    loadPropoundRecord,
    loadPropoundForm,
    deletePropoundForm,
    updatePropoundForm,
    savePropoundFormS3Key,
    sendPropoundQuestions,
    savePropounderServingAttorney,
    savePropounderQuestions,
    saveDefaultQuestionsTemplate,
    defaultQuestionsTemplate,
    createClientCase,
    updatePlanDetails,
    sendRespondQuestions,
    convertDocumentToPdf,
    savePdfS3Key,
    updateLegalForm,
    deleteQuickCreateDocument,
    updateUserRecord,
    filevineCommonUpdate,
    mycaseCommonUpdate,
    litifyCommonUpdate,
    litifyUploadDocument,
    createLitifyBulkRecords,
    clioCommonUpdate,
    createUserSubscriptions,
    importSfdtFromUrl,
    saveDocumentEditor,
    saveSfdtText,
    updateObjectionForm,
    saveAllObjectionForm,
    saveDiscoveryFilename,
    savePropoundFilename,
    hashedFileQuestions,
    uploadQuestionsFile,
    updateQuestions,
    deleteHashData,
    loadFrogsQuestions,
    editPropoundFormName,
    addFrogsQuestions,
    updateForgsQuestions,
    deleteFrogsDuplicateSetQuestions,
    deleteDuplicateSetQuestions,
    getDiscoveryVersions,
    getSfdtFromS3,
    getSfdtTextByUrl,
    docToPdfFileRename,
    pdfConversion
  } = remotes;




  function* uploadFormSaga() {
    yield takeLatest(UPLOAD_FORM, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          yield put(updateKey(0));
          yield put(updateRecordSuccess(Object.assign({}, { id: record.case_id, document_s3_key: null, legalforms_id: null, hash_id: null })));
          yield delay(1000);
          const result = yield call(uploadForm, record);
          if (result) {
            yield call(uploadFile, result.uploadURL, record.document_file[0], record.content_type);
            yield put(updateKey(1));
            yield put(updateRecordSuccess(Object.assign({}, { id: record.case_id, document_s3_key: result.s3_file_key, legalforms_id: result.legalForms.id, hash_id: null })));
            yield put(generateFormAction(Object.assign({}, { id: result.legalForms.id, document_type: record.document_type, s3_file_key: result.s3_file_key, case_id: record.case_id, client_id: record.client_id, filename: record.filename, state: record.state, federal: record.federal, federal_district: record.federal_district, practice_id: record.practice_id, file_name: record.file_name, case_id: result.legalForms.case_id, doc_textract_region : result?.doc_textract_region, doc_textract_region_count : result?.doc_textract_region_count })));
            yield put(stopSubmit(form));
          } else {
            yield put(uploadFormError(DEFAULT_DOCUMENT_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_DOCUMENT_ERROR }));
          }
        } catch (error) {
          yield put(uploadFormError(DEFAULT_DOCUMENT_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_DOCUMENT_ERROR }));
        }
      }
    })
  }



  function* generateFormSaga() {
    yield takeLatest(GENERATE_FORM, function* updater({ record }) {
      if (record) {
        try {

          // let result = {};
          // if (record.document_type && record.document_type === 'frogs') {
          //   result = {
          //     "questions": [
          //       {
          //         "id": "0e372128-5ca4-440d-ac02-ffc1b10f55bf",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 1,
          //         "question_number_text": "1.1",
          //         "question_number": "1.1",
          //         "question_text": "State the name, ADDRESS, telephone number, and relationship to you of each PERSON who prepared or assisted in the preparation of the responses to these interrogatories. (Do not identify anyone who simply typed or reproduced the responses.)",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d6086fbd-9a85-41bd-a809-ac1e9f5c54c5",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 2,
          //         "question_number_text": "2.1",
          //         "question_number": "2.1",
          //         "question_text": "State:\n(a) your name;\n(b) every name you have used in the past; \n(c) the dates you used each name.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "9bac560e-358f-4aa6-b38e-dadd9a7e37c7",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 3,
          //         "question_number_text": "2.2",
          //         "question_number": "2.2",
          //         "question_text": "State the date and place of your birth.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d474ae68-535e-4863-bacc-d79f7e697bdb",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 4,
          //         "question_number_text": "2.5",
          //         "question_number": "2.5",
          //         "question_text": "State: \n(a) your present residence ADDRESS; \n(b) your residence ADDRESSES for the past five years; and \n(c) the dates you lived at each ADDRESS.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d353e3f7-3257-4d60-b921-20edc610664e",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 5,
          //         "question_number_text": "2.7",
          //         "question_number": "2.7",
          //         "question_text": "State: \n(a) the name and ADDRESS of each school or other academic or vocational institution you have attended, beginning with high school; \n(b) the dates you attended; \n(c) the highest grade level you have completed; and \n(d) the degrees received.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "8ad6980e-5943-4eb1-82cd-202b0dd79884",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 6,
          //         "question_number_text": "3.2",
          //         "question_number": "3.2",
          //         "question_text": "Are you a partnership? If so, state: \n(a) the current partnership name; \n(b) all other names used by the partnership during the past 10 years and the dates each was used; \n(c) whether you are a limited partnership and, if so, under the laws of what jurisdiction; \n(d) the name and ADDRESS of each general partner; and \n(e) the ADDRESS of the principal place of business.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "e5cc0f6c-1f5e-4b24-8d3b-ddb308072e02",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 7,
          //         "question_number_text": "3.6",
          //         "question_number": "3.6",
          //         "question_text": "Have you done business under a fictitious name during the past 10 years? If so, for each fictitious name state: \n(a) the name; \n(b) the dates each was used; \n(c) the state and county of each fictitious name filing; and \n(d) the ADDRESS of the principal place of business.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "5c45b9d2-16fe-4d58-b00e-892b03fbdd16",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 8,
          //         "question_number_text": "3.7",
          //         "question_number": "3.7",
          //         "question_text": "Within the past five years has any public entity registered or licensed your business? If so, for each license or registration: \n(a) identify the license or registration; \n(b) state the name of the public entity; and \n(c) state the dates of issuance and expiration.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "e8c25ca8-0b1a-484b-9c21-921e3d7080a8",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 9,
          //         "question_number_text": "4.1",
          //         "question_number": "4.1",
          //         "question_text": "At the time of the INCIDENT, was there in effect any policy of insurance through which you were or might be insured in any manner (for example, primary, pro-rata, or excess liability coverage or medical expense coverage) for the damages, claims, or actions that have arisen out of the INCIDENT? If so, for each policy state: \n(a) the kind of coverage; \n(b) the name and ADDRESS of the insurance company; \n(c) the name, ADDRESS, and telephone number of each named insured; \n(d) the policy number; \n(e) the limits of coverage for each type of coverage contained in the policy; \n(f) whether any reservation of rights or controversy or coverage dispute exists between you and the insurance company; and \n(g) the name, ADDRESS, and telephone number of the custodian of the policy.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "06e0b790-269c-48ac-8434-83756a7bac0d",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 10,
          //         "question_number_text": "6.1",
          //         "question_number": "6.1",
          //         "question_text": "Do you attribute any physical, mental, or emotional injuries to the INCIDENT? (If your answer is ???’?‚¢??¢â?‚?š?‚¬???€?â?‚??“no,???’?‚¢??¢â?‚?š?‚¬???€??‚? do not answer interrogatories 6.2 through 6.7).",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "a1d629b0-e23b-4ef4-a7d7-a6fad81556dd",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 11,
          //         "question_number_text": "6.2",
          //         "question_number": "6.2",
          //         "question_text": "Identify each injury you attribute to the INCIDENT and the area of your body affected.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "bb1d42f2-0ab7-40cd-957b-e29102b02671",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 12,
          //         "question_number_text": "6.5",
          //         "question_number": "6.5",
          //         "question_text": "Have you taken any medication, prescribed or not, as a result of injuries that you attribute to the INCIDENT? If so, for each medication state: \n(a) the name; \n(b) the PERSON who prescribed or furnished it; \n(c) the date it was prescribed or furnished; \n(d) the dates you began and stopped taking it; and \n(e) the cost to date.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "f3ec533c-2f06-473a-a34c-5c8f7a168620",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 13,
          //         "question_number_text": "6.6",
          //         "question_number": "6.6",
          //         "question_text": "Are there any other medical services necessitated by the injuries that you attribute to the INCIDENT that were not previously listed (for example, ambulance, nursing, prosthetics)? If so, for each service state: \n(a) the nature; \n(b) the date; \n(c) the cost; and \n(d) the name, ADDRESS, and telephone number of each provider.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "1404d622-2a7b-4314-a6f9-00de9d434264",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 14,
          //         "question_number_text": "6.7",
          //         "question_number": "6.7",
          //         "question_text": "Has any HEALTH CARE PROVIDER advised that you may require future or additional treatment for any injuries that you attribute to the INCIDENT? If so, for each injury state: \n(a) the name and ADDRESS of each HEALTH CARE PROVIDER; \n(b) the complaints for which the treatment was advised; and \n(c) the nature, duration, and estimated cost of the treatment.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d9a7d9c0-eee9-4233-a55b-6c4add8ce8f4",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 15,
          //         "question_number_text": "7.2",
          //         "question_number": "7.2",
          //         "question_text": "Has a written estimate or evaluation been made for any item of property referred to in your answer to the preceding interrogatory? If so, for each estimate or evaluation state: \n(a) the name, ADDRESS, and telephone number of the PERSON who prepared it and the date prepared; \n(b) the name, ADDRESS, and telephone number of each PERSON who has a copy of it; and \n(c) the amount of damage stated.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "cab69146-6f80-4ac1-9278-98b9ef77ca14",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 16,
          //         "question_number_text": "8.1",
          //         "question_number": "8.1",
          //         "question_text": "Do you attribute any loss of income or earning capacity to the INCIDENT? (If your answer is no, do not answer interrogatories 8.2 through 8.8).",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "b46b94fc-294b-49f3-aca8-9df3b146fcb2",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 17,
          //         "question_number_text": "8.3",
          //         "question_number": "8.3",
          //         "question_text": "State the last date before the INCIDENT that you worked for compensation.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "98ba1cab-c3b3-4b50-a950-db9771c3bacd",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 18,
          //         "question_number_text": "8.4",
          //         "question_number": "8.4",
          //         "question_text": "State your monthly income at the time of the INCIDENT and how the amount was calculated.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "96ddbabf-3716-45e5-9995-9ccf10f1c2c7",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 19,
          //         "question_number_text": "8.5",
          //         "question_number": "8.5",
          //         "question_text": "State the date you returned to work at each place of employment following the INCIDENT.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "fcee0a4f-fee3-4ada-9e74-be053b8f0c48",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 20,
          //         "question_number_text": "8.6",
          //         "question_number": "8.6",
          //         "question_text": "State the dates you did not work and for which you lost income as a result of the INCIDENT.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "e2e27ff2-1aaf-4838-983e-d984edd8f8bb",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 21,
          //         "question_number_text": "8.7",
          //         "question_number": "8.7",
          //         "question_text": "State the total income you have lost to date as a result of the INCIDENT and how the amount was calculated.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "15bed48f-1651-4977-ab9a-cc2ce8b7ac21",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 22,
          //         "question_number_text": "10.1",
          //         "question_number": "10.1",
          //         "question_text": "At any time before the INCIDENT did you have complaints or injuries that involved the same part of your body claimed to have been injured in the INCIDENT? If so, for each state: \n(a) a description of the complaint or injury; \n(b) the dates it began and ended; and \n(c) the name, ADDRESS, and telephone number of each HEALTH CARE PROVIDER whom you consulted or who examined or treated you.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "50f1a3e5-0a4a-4ba7-896b-614408e55556",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 23,
          //         "question_number_text": "10.2",
          //         "question_number": "10.2",
          //         "question_text": "List all physical, mental, and emotional disabilities you had immediately before the INCIDENT. (You may omit mental or emotional disabilities unless you attribute any mental or emotional injury to the INCIDENT.)",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "4fa5e18e-e309-4338-b1d7-9547a2c5cf06",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 24,
          //         "question_number_text": "11.2",
          //         "question_number": "11.2",
          //         "question_text": "In the past 10 years have you made a written claim or demand for workers' compensation benefits? If so, for each claim or demand state: \n(a) the date, time, and place of the INCIDENT giving rise to the claim; \n(b) the name, ADDRESS, and telephone number of your employer at the time of the injury; \n(c) the name, ADDRESS, and telephone number of the workers compensation insurer and the claim number;\n(d) the period of time during which you received workers compensation benefits;\n(e) a description of the injury; \n(f) the name, ADDRESS, and telephone number of any HEALTH CARE PROVIDER who provided services; and \n(g) the case number at the Workers' Compensation Appeals Board.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d3a8378e-6a2f-46c5-b4a3-50b55cc9f7cd",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 25,
          //         "question_number_text": "12.5",
          //         "question_number": "12.5",
          //         "question_text": "Do YOU OR ANYONE ACTING ON YOUR BEHALF know of any diagram, reproduction, or model of any place or thing (except for items developed by expert witnesses covered by Code of Civil Procedure sections 2034.210-2034.310) concerning the INCIDENT? If so, for each item state: \n(a) the type (i.e., diagram, reproduction, or model); \n(b) the subject matter; and \n(c) the name, ADDRESS, and telephone number of each PERSON who has it.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "a534205b-8896-4f98-ac63-2b20173edce0",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 26,
          //         "question_number_text": "12.6",
          //         "question_number": "12.6",
          //         "question_text": "Was a report made by any PERSON concerning the INCIDENT? If so, state: \n(a) the name, title, identification number, and employer of the PERSON who made the report; \n(b) the date and type of report made; \n(c) the name, ADDRESS, and telephone number of the PERSON for whom the report was made; and \n(d) the name, ADDRESS, and telephone number of each PERSON who has the original or a copy of the report.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "f21e3fd3-683b-4332-83d2-73c407ae9486",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 27,
          //         "question_number_text": "14.1",
          //         "question_number": "14.1",
          //         "question_text": "Do YOU OR ANYONE ACTING ON YOUR BEHALF contend that any PERSON involved in the INCIDENT violated any statute, ordinance, or regulation and that the violation was a legal (proximate) cause of the INCIDENT? If so, identify the name, ADDRESS, and telephone number of each PERSON and the statute, ordinance, or regulation that was violated.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "ec5a33d4-e191-44e1-aef0-59db0e78e2e9",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 28,
          //         "question_number_text": "20.1",
          //         "question_number": "20.1",
          //         "question_text": "State the date, time, and place of the INCIDENT (closest street ADDRESS or intersection).",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "a37a3029-136d-4335-8c01-1b9fe4c4241b",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 29,
          //         "question_number_text": "20.2",
          //         "question_number": "20.2",
          //         "question_text": "For each vehicle involved in the INCIDENT, state:  \n(a) the year, make, model, and license number; \n(b) the name, ADDRESS, and telephone number of the driver; \n(c) the name, ADDRESS, and telephone number of each occupant other than the driver;  \n(d) the name, ADDRESS, and telephone number of each registered owner;  \n(e) the name, ADDRESS, and telephone number of each lessee; \n(f) the name, ADDRESS, and telephone number of each owner other than the registered owner or lien holder; and \n(g) the name of each owner who gave permission or consent to the driver to operate the vehicle.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "b19fd211-4139-4036-a13a-550e045c3979",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 30,
          //         "question_number_text": "20.5",
          //         "question_number": "20.5",
          //         "question_text": "State the name of the street or roadway, the lane of travel, and the direction of travel of each vehicle involved in the INCIDENT for the 500 feet of travel before the INCIDENT.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "0a0021bf-c6f6-40e1-98ab-ffb75c478494",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 31,
          //         "question_number_text": "20.6",
          //         "question_number": "20.6",
          //         "question_text": "Did the INCIDENT occur at an intersection? If so, describe all traffic control devices, signals, or signs at the intersection.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "27d8bccd-f598-4f33-8c71-408156af0240",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 32,
          //         "question_number_text": "20.7",
          //         "question_number": "20.7",
          //         "question_text": "Was there a traffic signal facing you at the time of the INCIDENT? If so, state: \n(a) your location when you first saw it; \n(b) the color; \n(c) the number of seconds it had been that color; and \n(d) whether the color changed between the time you first saw it and the INCIDENT.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "d85bfbda-e7ef-4c6c-aa39-c6baca4febad",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 33,
          //         "question_number_text": "20.9",
          //         "question_number": "20.9",
          //         "question_text": "Do you have information that a malfunction or defect in a vehicle caused the INCIDENT? If so: \n(a) identify the vehicle; \n(b) identify each malfunction or defect; \n(c) state the name, ADDRESS, and telephone number of each PERSON who is a witness to or has information about each malfunction or defect; and \n(d) state the name, ADDRESS, and telephone number of each PERSON who has custody of each defective part.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "01db91b6-7d8c-49c7-8c59-77d8633f57d5",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 34,
          //         "question_number_text": "20.10",
          //         "question_number": "20.10",
          //         "question_text": "Do you have information that any malfunction or defect in a vehicle contributed to the injuries sustained in the INCIDENT? If so: \n(a) identify the vehicle; \n(b) identify each malfunction or defect; \n(c) state the name, ADDRESS, and telephone number of each PERSON who is a witness to or has information about each malfunction or defect; and \n(d) state the name, ADDRESS, and telephone number of each PERSON who has custody of each defective part.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       },
          //       {
          //         "id": "ae04b8e7-28d3-4a0e-812f-1ebc84b65912",
          //         "case_id": "570f1459-f1fc-40d7-b2c0-505f465028c6",
          //         "practice_id": "41dcd3ab-0c4d-4ce2-a092-4de535f90076",
          //         "client_id": "de72a328-b632-454d-b830-5881739bf485",
          //         "legalforms_id": "d0739274-ea5f-46db-bf37-af7dc2d743fc",
          //         "document_type": "FROGS",
          //         "subgroup": null,
          //         "question_id": 35,
          //         "question_number_text": "20.11",
          //         "question_number": "20.11",
          //         "question_text": "State the name, ADDRESS, and telephone number of each owner and each PERSON who has had possession since the INCIDENT of each vehicle involved in the INCIDENT.",
          //         "question_section_id": "1",
          //         "question_section": "",
          //         "question_section_text": "",
          //         "question_options": "\"\"",
          //         "lawyer_response_text": null,
          //         "lawyer_response_status": "NotStarted",
          //         "client_response_text": null,
          //         "client_response_status": "NotSetToClient",
          //         "file_upload_status": null,
          //         "uploaded_documents": null,
          //         "last_sent_to_client": null,
          //         "last_updated_by_lawyer": null,
          //         "last_updated_by_client": null,
          //         "TargetLanguageCode": null,
          //         "createdAt": "2021-08-19T07:34:49.000Z",
          //         "updatedAt": "2021-08-19T07:34:49.000Z"
          //       }
          //     ]
          //   };
          // } else if (record.document_type) {
          //   result = {
          //     "questions": [
          //       {
          //         "question_number": "1",
          //         "question_id": 1,
          //         "question_text": "State  your  name,  any  other  names  by  which you have been known, and your ADDRESS."
          //       },
          //       {
          //         "question_number": "2",
          //         "question_id": 2,
          //         "question_text": "State the date and place of your birth."
          //       },
          //       {
          //         "question_number": "3",
          //         "question_id": 3,
          //         "question_text": "State, as of the time of the  INCIDENT,  your driver's license number, the state of issuance, the expiration date, and any restrictions."
          //       }
          //     ]
          //   };
          // } else {
          //   result = yield call(generateForm, record);
          // }

          const hashedFileQuestionsResult = yield call(hashedFileQuestions, Object.assign({}, { s3_file_key: record?.s3_file_key, legalform_id: record.id, filename: record.file_name, uploaded_type: "Responding", state: record.state, document_type: record.document_type }));
          const hashId = hashedFileQuestionsResult?.['hashResponse']?.id || false;
          yield put(updateRecordSuccess(Object.assign({}, { id: record.case_id, document_s3_key: record?.s3_file_key, legalforms_id: record.id, hash_id: hashId })));

          if (hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'NoQuestions') {
            yield put(QuestionsNotFound(true));
            yield put(generateFormError(DEFAULT_QUESTIONS_NOT_FOUND_ERROR));
            return;
          }

          if (hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'true') {
            yield put(QuestionsNotFound(false));
            const hashedFileQuestionsData = hashedFileQuestionsResult['hashResponse'].questions || [];
            yield put(loadFormSuccess(Object.assign({}, { id: record.id, questions: hashedFileQuestionsData, hash_id: hashId })));
            yield put(SaveFormAction(Object.assign({}, { questions: hashedFileQuestionsData, hash_id: hashId }, record)));
            if (record?.document_type?.toLowerCase() != 'frogs') {
              yield put(storeDefendantDetialsAction(Object.assign({}, { document_type: record?.document_type, legalform_id: record.id, defendant_practice_details: hashedFileQuestionsResult?.defendant_practice_details, opposing_counsel_info: hashedFileQuestionsResult?.opposing_counsel_info })));
            }
            yield delay(500);
            while (true) { // eslint-disable-line no-constant-condition
              const storeRecord = yield select(selectRecord(record.case_id));
              const user_info = yield select(selectUser());
              let isPropoundPractice = user_info && record && isPropound(user_info, record);
              if (storeRecord && storeRecord?.questions) {
                const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
                  if (!q && storeRecord.questions && storeRecord.questions.length > 0) {
                    const arr = storeRecord.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
                    if (arr)
                      q = nc;
                  }
                  return q;
                }, false);
                const FormColumns = DiscType ? standardFormColumns[DiscType] : [];
                const stateObject = Object.assign({}, { ...history.location.state });
                if (stateObject?.questions) {
                  delete stateObject?.questions;
                }
                if (record.document_type === 'frogs') {
                  yield put(push({
                    pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms/respond/standardForm` : `/casesCategories/cases/${record.case_id}/standardForm`,
                    state: Object.assign({}, { ...stateObject }, {
                      document_type: record.document_type,
                      legalform_id: record && record.id,
                      questions: FormColumns,
                      filename: record.filename,
                      defaultQuestions: storeRecord?.questions?.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
                      tabValue: '1/respond'
                    })
                  }));
                } else {
                  const defendantPracticeDetails = yield select(selectDefendantPracticeDetails());
                  const posStateList = ['CA', 'NV', 'AZ', 'WA', 'PL'];
                  let opposing_counsel_form = false;
                  if (defendantPracticeDetails && Object.keys(defendantPracticeDetails)?.length > 0) {
                    let storeQuestion = storeRecord?.questions?.[0];
                    opposing_counsel_form = storeQuestion?.document_type?.toLowerCase() === defendantPracticeDetails?.document_type?.toLowerCase() && defendantPracticeDetails?.legalform_id === storeQuestion?.legalforms_id && defendantPracticeDetails?.defendant_practice_details && (!storeRecord?.opposing_counsel || storeRecord?.opposing_counsel?.length <= 0) && !storeRecord.opposing_counsel_info && posStateList.includes(storeRecord.state) || false;
                  }
                  yield put(push({
                    pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms/respond/questionsForm` : `/casesCategories/cases/${record.case_id}/questionsForm`,
                    state: Object.assign({}, { ...stateObject }, {
                      document_type: record.document_type,
                      legalform_id: record && record.id,
                      questions: storeRecord?.questions?.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
                      filename: record.filename,
                      opposing_counsel_form: opposing_counsel_form,
                      tabValue: '1/respond'
                    })
                  }));
                }
                break;
              }
              yield delay(500);
            }
            yield put(updateKey(3));
          }
          else if (hashedFileQuestionsResult?.['hashResponse']?.questions_available == 'false') {
            yield put(QuestionsNotFound(false));
            const result = yield call(generateForm, Object.assign({}, record, { hash_id: hashId }));
            if (result) {
              yield put(updateKey(2));
              let timeIntervalArr = TimeOutArr(result);

              if (result.document_modal === 'scanned') {
                var i = 0;

                // Delays the dispatch of select record untill the store is populated with an initial list of Questions.
                while (i < timeIntervalArr.length) {
                  if (i == 0) {
                    yield delay(10000);
                  }
                  const DocumentStatus = yield call(getDocumentStatus, record);
                  yield delay(timeIntervalArr[i]);
                  yield put(contentNotFound(false));

                  if (DocumentStatus) {
                    const uploading_status = DocumentStatus && DocumentStatus.status || false;

                    if (uploading_status && uploading_status === 'Content_not_found') {
                      yield put(contentNotFound(true));
                      yield put(stopSubmit(`forms_${record.document_type.toLowerCase()}`));
                      yield put(generateFormError(DEFAULT_DOCUMENT_CONTENT_NOT_FOUND_ERROR));
                      return;
                    }

                    if (uploading_status && uploading_status === 'Completed') {
                      try {
                        const loadFormResult = yield call(loadForm, Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.id }));
                        if (loadFormResult?.questions) {
                          // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
                          while (true) { // eslint-disable-line no-constant-condition
                            const recordsInStore = yield select(selectRecords());
                            if (recordsInStore && recordsInStore.length > 0) {
                              break;
                            }
                            yield delay(500);
                          }
                          const questions = loadFormResult?.questions;
                          yield put(loadFormSuccess(Object.assign({}, { id: record.id, questions })));
                          if (questions?.[0]?.document_type?.toLowerCase() != 'frogs') {
                            yield put(storeDefendantDetialsAction(Object.assign({}, { document_type: questions?.[0]?.document_type, legalform_id: questions?.[0]?.legalforms_id, defendant_practice_details: loadFormResult?.defendant_practice_details, opposing_counsel_info: loadFormResult?.opposing_counsel_info })));
                          }
                        } else {
                          yield put(loadFormError(DEFAULT_LOAD_FORM_ERROR));
                        }
                      } catch (error) {
                        yield put(loadFormError(DEFAULT_LOAD_FORM_ERROR));
                      }
                      yield delay(4000);

                      while (true) { // eslint-disable-line no-constant-condition
                        const storeRecord = yield select(selectRecord(record.case_id));
                        const user_info = yield select(selectUser());
                        let isPropoundPractice = user_info && record && isPropound(user_info, record);
                        if (storeRecord && storeRecord?.questions) {
                          const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
                            if (!q && storeRecord.questions && storeRecord.questions.length > 0) {
                              const arr = storeRecord.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
                              if (arr)
                                q = nc;
                            }
                            return q;
                          }, false);
                          const FormColumns = DiscType ? standardFormColumns[DiscType] : [];
                          const stateObject = Object.assign({}, { ...history.location.state });
                          if (stateObject?.questions) {
                            delete stateObject?.questions;
                          }
                          if (record.document_type === 'frogs') {
                            yield put(push({
                              pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms/respond/standardForm` : `/casesCategories/cases/${record.case_id}/standardForm`,
                              state: Object.assign({}, { ...stateObject }, {
                                document_type: record.document_type,
                                legalform_id: record && record.id,
                                questions: FormColumns,
                                filename: record.filename,
                                defaultQuestions: storeRecord?.questions?.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
                                tabValue: '1/respond'
                              })
                            }));
                          } else {
                            const defendantPracticeDetails = yield select(selectDefendantPracticeDetails());
                            const posStateList = ['CA', 'NV', 'AZ', 'WA', 'PL'];
                            let opposing_counsel_form = false;
                            if (defendantPracticeDetails && Object.keys(defendantPracticeDetails)?.length > 0) {
                              let storeQuestion = storeRecord?.questions?.[0];
                              opposing_counsel_form = storeQuestion?.document_type?.toLowerCase() === defendantPracticeDetails?.document_type?.toLowerCase() && defendantPracticeDetails?.legalform_id === storeQuestion?.legalforms_id && defendantPracticeDetails?.defendant_practice_details && (!storeRecord?.opposing_counsel || storeRecord?.opposing_counsel?.length <= 0) && !storeRecord.opposing_counsel_info && posStateList.includes(storeRecord.state) || false;
                            }
                            yield put(push({
                              pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms/respond/questionsForm` : `/casesCategories/cases/${record.case_id}/questionsForm`,
                              state: Object.assign({}, { ...stateObject }, {
                                document_type: record.document_type,
                                legalform_id: record && record.id,
                                questions: storeRecord?.questions?.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
                                filename: record.filename,
                                opposing_counsel_form: opposing_counsel_form,
                                tabValue: '1/respond'
                              })
                            }));
                          }
                          break;
                        }
                        yield delay(500);
                      }

                      yield put(updateKey(3));
                      return;
                    } else if (uploading_status && uploading_status === 'cancelled') {
                      yield put(stopSubmit(`forms_${record.document_type.toLowerCase()}`));
                      return;
                    } else if ((i == (timeIntervalArr.length - 1)) || (uploading_status && uploading_status === 'Failed')) {
                      yield put(generateFormError(DEFAULT_DOCUMENT_ERROR));
                      return;
                    }
                  } else {
                    yield put(generateFormError(DEFAULT_DOCUMENT_ERROR));
                    return;
                  }
                  i++;
                }

              } else {
                yield put(SaveFormAction(Object.assign({}, result, record)));
              }
            } else {
              yield put(generateFormError(DEFAULT_DOCUMENT_ERROR));
            }
          }
          else {
            yield put(generateFormError(DEFAULT_DOCUMENT_ERROR));
          }
        } catch (error) {
          yield put(generateFormError(DEFAULT_DOCUMENT_ERROR));
        }
      }
    })
  }


  function* cancelFormSaga() {
    yield takeLatest(CANCEL_FORM, function* updater({ record, loadRecord }) {
      if (record) {
        try {
          const result = yield call(cancelForm, record);

          if (result) {
            yield put(loadRecord(record.case_id));
          } else {
            yield put(cancelFormError(DEFAULT_CANCEL_FORM_ERROR));
            yield put(loadRecord(record.case_id));
          }
        } catch (error) {
          yield put(cancelFormError(DEFAULT_CANCEL_FORM_ERROR));
          yield put(loadRecord(record.case_id));
        }
      }
    })
  }


  function* saveFormSaga() {
    yield takeLatest(SAVE_FORM, function* updater({ record }) {
      if (record) {

        try {
          const result = yield call(saveForm, record);
          if (result) {
            const user_info = yield select(selectUser());
            let isPropoundPractice = user_info && record && isPropound(user_info, record);
            yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.id })));
            yield delay(2000);

            // while (true) { // eslint-disable-line no-constant-condition
            //   const storeRecord = yield select(selectRecord(record.case_id));
            //   if (storeRecord && storeRecord.questions) {
            //     const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
            //       if (!q && storeRecord.questions && storeRecord.questions.length > 0) {
            //         const arr = storeRecord.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
            //         if (arr)
            //           q = nc;
            //       }
            //       return q;
            //     }, false);
            //     const FormColumns = DiscType ? standardFormColumns[DiscType] : [];
            //     if (record.document_type === 'frogs') {
            //       yield put(push({
            //         pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms/respond/standardForm` : `/casesCategories/cases/${record.case_id}/standardForm`,
            //         state: Object.assign({}, { ...history.location.state }, {
            //           document_type: record.document_type,
            //           questions: FormColumns,
            //           filename: record.filename,
            //           defaultQuestions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || []
            //         })
            //       }));
            //     } else {
            //       yield put(push({
            //         pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms/respond/questionsForm` : `/casesCategories/cases/${record.case_id}/questionsForm`,
            //         state: Object.assign({}, { ...history.location.state }, {
            //           document_type: record.document_type,
            //           questions: storeRecord.questions.map(q => Object.assign({}, { ...q }, { question_number: q.question_number_text })) || [],
            //           filename: record.filename
            //         })
            //       }));
            //     }
            //     break;
            //   }
            //   yield delay(500);
            // }

            yield put(updateKey(3));
          } else {
            yield put(saveFormError(DEFAULT_DOCUMENT_ERROR));
          }
        } catch (error) {
          yield put(saveFormError(DEFAULT_DOCUMENT_ERROR));
        }
      }
    })
  }


  function* loadFormSaga() {
    yield takeLatest(LOAD_FORM, function* updater({ record, setFormLoader, upload }) {
      if (record) {
        try {
          const result = yield call(loadForm, record);
          if (result?.questions) {
            // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
            while (true) { // eslint-disable-line no-constant-condition
              const recordsInStore = yield select(selectRecords());
              if (recordsInStore && recordsInStore.length > 0) {
                break;
              }
              yield delay(500);
            }
            const questions = result?.questions;
            yield put(loadFormSuccess(Object.assign({}, { id: record.id, questions })));
            if(upload && questions?.[0]?.document_type?.toLowerCase() != 'frogs') {
              yield put(storeDefendantDetialsAction(Object.assign({}, { document_type: questions?.[0]?.document_type, legalform_id: questions?.[0]?.legalforms_id, defendant_practice_details: result?.defendant_practice_details, opposing_counsel_info: result?.opposing_counsel_info })));
            }
            if (setFormLoader) {
              yield delay(500);
              yield call(setFormLoader, false);
            }
          } else {
            yield put(loadFormError(DEFAULT_LOAD_FORM_ERROR));
          }
        } catch (error) {
          yield put(loadFormError(DEFAULT_LOAD_FORM_ERROR));
        }
      }
    })
  }


  function* updateFormSaga() {
    yield takeLatest(UPDATE_FORM, function* updater({ record, form, closeForm, subGroupForm = false }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateForm, record);
          if (result) {
            yield delay(1000);
            const recordInStore = yield select(selectRecord(record.case_id));
            yield put(updateFormSuccess(Object.assign({}, { id: record.case_id, questions: recordInStore.questions.map((r) => result.id === r.id ? Object.assign({}, r, result) : Object.assign({}, r)) })));
            yield put(stopSubmit(form));
            yield call(closeForm, false);
            if(!subGroupForm) {
              yield put(destroy(form));
            }
            yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
          } else {
            yield put(updateFormError(DEFAULT_UPDATE_FORM_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
          }
        } catch (error) {
          yield put(updateFormError(DEFAULT_UPDATE_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
        } finally {
          if(subGroupForm) {
            yield put(destroy(form));
          }
        }
      }
    })
  }


  function* deleteFormSaga() {
    yield takeLatest(DELETE_FORM, function* updater({ record, loadRecord }) {
      if (record) {
        try {
          const result = yield call(deleteForm, record);

          if (result) {
            yield put(deleteFormSuccess("Form Deleted"));
            yield delay(500);
            yield put(loadRecord(record.case_id));
          } else {
            yield put(deleteFormError(DEFAULT_DELETE_FORM_ERROR));
          }
        } catch (error) {
          yield put(deleteFormError(DEFAULT_DELETE_FORM_ERROR));
        }
      }
    })
  }


  function* fetchFormSaga() {
    yield takeLatest(FETCH_FORM, function* updater({ record, generatedType, setFilevinePopUp, setMyCasePopUp, setProgress, signature, setLitifyPopup, setClioPopup, setOpenDocEditor }) {
      if (record) {

        try {
          yield put(updateKey(0));
          const result = yield call(fetchForm, record);

          if (result) {
            const user_info = yield select(selectUser())
            const case_info = yield select(selectRecord(record.id));
            const filterData = record && record.document_type && record.legalforms_id && record[record.document_type.toLowerCase()] && record[record.document_type.toLowerCase()].find(_ => _.legalform_id.toString() === record.legalforms_id.toString()) || {};
            const teksignData = filterData && filterData.client_signature_data && JSON.parse(filterData.client_signature_data) || {};
            const modified_template_s3_file_key_doc_type = `modified_template_s3_file_key_${record.document_type.toLowerCase()}`;

            const modified_template_s3_file_key_value = record && record[modified_template_s3_file_key_doc_type] || '';
            const modified_template_status = record && record.modified_template_status || '';
            const doc_gen_type = record && record.doc_gen_type || false;
            const plaintiffName = filterData && filterData.plaintiff_name || '';
            const defendantName = filterData && filterData.defendant_name || '';
            const posDocument = filterData && filterData.posDocument || '';
            const serving_attorney_name = filterData && filterData.serving_attorney_name || '';
            const serving_attorney_street = filterData && filterData.serving_attorney_street || '';
            const serving_attorney_city = filterData && filterData.serving_attorney_city || '';
            const serving_attorney_state = filterData && filterData.serving_attorney_state || '';
            const serving_attorney_zip_code = filterData && filterData.serving_attorney_zip_code || '';
            const serving_attorney_email = filterData && filterData.serving_attorney_email || '';
            const serving_date = filterData && filterData.serving_date || '';
            const custom_template_id = record && record.custom_template_id || '';
            const userLoader = store2.get('userLoader');

            const pos_signature = filterData?.pos_signature || false;
            const signatureRecord = pos_signature ? Object.assign({}, { ...signature }, { pos_signature }) : signature;
            const set_number = filterData && filterData?.set_number|| '';

            if (user_info && case_info) {
              yield put(fetchFormSuccess(Object.assign({}, { id: record.id, questions: result?.questions })));
              yield put(updateKey(1));
              let lawyersRecord; let userInfo;

              if (userLoader) {
                userInfo = yield call(verifySession);
                yield delay(1000);
                if (user_info && user_info.name) {
                  lawyersRecord = record && record.lawyers && record.lawyers.reduce((a, el) => {
                    if (el.id == userInfo.id) {
                      a.push(Object.assign({}, el, { state_bar_number: userInfo.state_bar_number }));
                    } else {
                      a.push(el);
                    }
                    return a;
                  }, []) || [];
                } else {
                  lawyersRecord = record && record.lawyers && record.lawyers.length > 0 && record.lawyers.filter(el => el.id != userInfo.id);
                  lawyersRecord = lawyersRecord && lawyersRecord.length > 0 && lawyersRecord.concat(Object.assign({}, userInfo)) || [userInfo];
                }
              } else {
                userInfo = user_info;
                lawyersRecord = record && record.lawyers || [];
              }
              yield put(generateDocumentAction(Object.assign({}, { user_info: userInfo, case_info: Object.assign({}, case_info, { lawyers: lawyersRecord }), case_id: record.id, generated_type: generatedType, document_type: record.document_type, teksign_date: teksignData.created_date || false, teksign_county: teksignData.County || false, client_signature: teksignData.client_signature || false, legalforms_id: record.legalforms_id, discovery_s3_file_key: record.discovery_s3_file_key, [modified_template_s3_file_key_doc_type]: modified_template_s3_file_key_value, modified_template_status: modified_template_status, doc_gen_type, plaintiff_name: plaintiffName, defendant_name: defendantName, posDocument, serving_attorney_name, serving_attorney_street, serving_attorney_city, serving_attorney_state, serving_attorney_zip_code, serving_attorney_email, serving_date, case_from: record.case_from, signature: signatureRecord, set_number, custom_template_id }), setFilevinePopUp, setMyCasePopUp, setProgress, setLitifyPopup, setClioPopup, setOpenDocEditor));
            } else {
              yield put(fetchFormError(DEFAULT_FETCH_FORM_ERROR));
            }
          } else {
            yield put(fetchFormError(DEFAULT_FETCH_FORM_ERROR));
          }
        } catch (error) {
          yield put(fetchFormError(DEFAULT_FETCH_FORM_ERROR));
        }
      }
    })
  }

  function* generateDocumentSaga() {
    yield takeLatest(GENERATE_DOCUMENT, function* updater({ record, setFilevinePopUp, setMyCasePopUp, setProgress, setLitifyPopup, setClioPopup, setOpenDocEditor }) {
      if (record) {
        try {
          const result = yield call(generateDocument, record);
          if (result) {
            yield put(generateDocumentSuccess(result));
            let user_info;
            const pageLoader = store2.get('pageLoader');
            if(pageLoader) {
              const user = yield call(verifySession);
              if(user) {
                user_info = user;
              }
            } else {
              user_info = yield select(selectUser());
            }
            const planType = user_info && user_info.plan_type && typeof user_info.plan_type === 'object' && user_info.plan_type['responding'] || false;
            const casesRemotes = yield call(appRemotes, 'cases');
            const loadRecord = yield call(casesRemotes.loadRecord, record.case_id);
            const case_info = loadRecord;
            const state = case_info && case_info.state || false;
            
            if(record && state && record.generated_type && record.generated_type === 'final' && stateListPOS(state)) {
              const practice_id = user_info && user_info.practiceDetails && user_info.practiceDetails.id || false;
              const environment = process.env.ENV === 'production' ? 'production' : 'staging';
              const publicKey = process.env.PUBLIC_KEY;

              const pdfFileResult = yield call(docToPdfFileRename, Object.assign({}, { legalform_id: record?.legalforms_id, s3_file_key: result.s3_key, case_id: record?.case_id, document_type: record.document_type }));

              if(pdfFileResult) {
                const submitRecord = JSON.stringify(Object.assign({}, { key: `${process.env.PRIVATE_KEY}`, s3_key: pdfFileResult.s3_file_key, env: environment, practice_id: practice_id }));
                const encrytedHash = CryptoJS.AES.encrypt(submitRecord, publicKey).toString();
                const pdf_url = yield call(convertDocumentToPdf, Object.assign({}, { data: encrytedHash, legalforms_id: record?.legalforms_id, s3_file_key: pdfFileResult.s3_file_key, practice_id: practice_id }));
                if(pdf_url) {
                  yield call(savePdfS3Key, Object.assign({}, { id: record.legalforms_id.toString(), s3_file_key: result.s3_key, pdf_s3_file_key: pdf_url.pdf_s3_key }));
                }
              }
            }
            yield put(saveDocumentAction(Object.assign({}, { case_id: record.case_id, document_type: record.document_type, document_generation_type: record.generated_type, generated_document: result.url, final_document: result.url, generated_document_s3_key: result.s3_key, final_document_s3_key: result.s3_key, legalforms_id: record.legalforms_id, plan_type : planType, case_from: record.case_from, doc_filename: result?.doc_filename }), setFilevinePopUp, setMyCasePopUp, setProgress, setLitifyPopup, setClioPopup, setOpenDocEditor));
          } else {
            yield put(generateDocumentError(DEFAULT_GENERATE_DOCUMENT_ERROR));
          }
        } catch (error) {
          yield put(generateDocumentError(DEFAULT_GENERATE_DOCUMENT_ERROR));
        } finally {
          yield put(destroy(`responderCustomTemplate_${record.generated_type}`));
          yield put(destroy(`respondCustomTemplateAndSignature_${record.generated_type}`));
          yield put(destroy(`selectTemplate_${record.generated_type}`));
        }
      }
    })
  }


  function* saveDocumentSaga() {
    yield takeLatest(SAVE_DOCUMENT, function* updater({ record, setFilevinePopUp, setMyCasePopUp, setProgress, setLitifyPopup, setClioPopup, setOpenDocEditor, customObject }) {
      if (record) {
        try {
          const result = yield call(saveDocument, record);
          if (result) {
            const casesRemotes = yield call(appRemotes, 'cases');
            const loadRecord = yield call(casesRemotes.loadRecord, record.case_id);
            const type = record && record.document_type.toLowerCase();
            const document_generation_type = record && record.document_generation_type;
            const documentRecord = loadRecord && loadRecord[type] && loadRecord[type].length > 0 && loadRecord[type].find(_ => _.legalform_id.toString() === record.legalforms_id.toString()) || {};
            const { generatedDocument, finalDocument, posDocument } = documentRecord;
            const user_info = yield select(selectUser());
            const documentEditedAccessIds = user_info && user_info.show_document_editor_ids && typeof user_info.show_document_editor_ids === 'string' && user_info.show_document_editor_ids.split(',') || [];
            const documentEditedAccess = user_info && user_info.practiceDetails && documentEditedAccessIds && documentEditedAccessIds.includes(user_info.practiceDetails.id) || false;
            if (document_generation_type === 'final') {
              if(documentEditedAccess) {
                yield put(loadDocumentEditorAction(Object.assign({}, documentRecord, { document_type: type }), setProgress, setOpenDocEditor));
              } else {
                yield call(download, finalDocument);
                yield put(updateKey(3));
                yield delay(500);
              }
            } else {
              yield call(download, document_generation_type === 'pos' ? posDocument : generatedDocument);
              yield put(updateKey(3));
            }
            const case_info = loadRecord;
            const DocTypeArray = case_info && case_info[type] && case_info[type].map(r => r.legalform_id.toString() === record.legalforms_id.toString() ? Object.assign({}, { ...documentRecord }) : r);
            const DocType = type;
            const caseFrom = case_info && case_info.case_from || false;
            const integration = caseFrom && ['mycase', 'filevine', 'litify', 'clio'].includes(caseFrom) || false;
            const userLoader = store2.get('userLoader');
            const pageLoader = store2.get('pageLoader');

            if(document_generation_type && document_generation_type === 'pos') {
              yield put(saveDocumentSuccess(Object.assign({}, case_info, { [DocType]: DocTypeArray }), 'Proof of Service Document Downloaded'));
            } else {
              yield put(saveDocumentSuccess(Object.assign({}, case_info, { [DocType]: DocTypeArray })));
            }

            if(customObject) {
              const { openNext } = customObject;
              if(openNext) return;
            }

            if (!integration && (pageLoader || userLoader) && document_generation_type != 'final') {
              const user = yield call(verifySession);
              if (user)
                yield put(verifySessionSuccess(user));
                store2.remove('pageLoader');
                store2.remove('userLoader');
            }
            
            if(integration && document_generation_type === 'final' && !documentEditedAccess) {
              yield delay(2000);
              yield call(setProgress, false);
              if(caseFrom && caseFrom === 'filevine') {
                if(setFilevinePopUp) yield call(setFilevinePopUp, true);
              } else if(caseFrom && caseFrom === 'mycase') {
                if(setMyCasePopUp) yield call(setMyCasePopUp, true);
              } else if(caseFrom && caseFrom === 'litify') {
                if(setLitifyPopup) yield call(setLitifyPopup, true);
              } else if (caseFrom && caseFrom === 'clio') {
                if (setClioPopup) yield call(setClioPopup, true);
              }
            }
          } else {
            yield put(saveDocumentError(DEFAULT_SAVE_DOCUMENT_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SAVE_DOCUMENT_ERROR;
          yield put(saveDocumentError(Err));
        }
      }
    })
  }

  function* sendQuestionsSaga() {
    yield takeLatest(SEND_QUESTIONS, function* updater({ record }) {
      if (record) {
        try {
          var result;
          if (record.party_ids && Array.isArray(record.party_ids) && record.party_ids.length > 5) {
            const count = record.party_ids.length / 5;
            const partyArr = Array.from(Array(Math.round(count)), (x, index) => index * 5);
            const sendArr = partyArr.reduce((a, el, key) => {
              a.push(record.party_ids.slice(el, el + 5));
              return a;
            }, []);
            let i = 0;
            while (i < sendArr.length) { // eslint-disable-line no-constant-condition
              result = yield call(sendQuestions, Object.assign({}, record, { party_ids: sendArr[i] }));
              i++;
              yield delay(200);
            }
          } else {
            result = yield call(sendQuestions, record);
          }
          if (result) {
            const case_info = yield select(selectRecord(record.case_id));
            const DocTypeArray = record && record.document_type && record.legalforms_id && case_info && case_info[record.document_type.toLowerCase()] && case_info[record.document_type.toLowerCase()].map(r => r.legalform_id === result.legalforms_id ? Object.assign({}, r, { QuestionsRespondedByClientLastSetofCount: result.QuestionsRespondedByClientLastSetofCount, QuestionsSentToClientLastSetofCount: result.QuestionsSentToClientLastSetofCount }) : r);
            const DocType = record && record.document_type;

            yield put(sendQuestionsSuccess(Object.assign({}, case_info, { [DocType]: DocTypeArray }), 'Questions sent to client'));
            yield put(destroy('selectResponseForm'));
          } else {
            yield put(sendQuestionsError(DEFAULT_SEND_QUESTIONS_ERROR));
          }
        } catch (error) {
          yield put(sendQuestionsError(DEFAULT_SEND_QUESTIONS_ERROR));
        }
      }
    })
  }

  function* loadBillingDetailsSaga() {
    yield takeLatest(LOAD_BILLING_DETAILS, function* updater({ form, dialog, progress, record, generatedType, generateDocument, posModal, freeTrialDialog, signature, setFilevinePopUp, setMyCasePopUp, subscriptionAlert, openTemplateForm, openGeorgiaTemplate, openRecordForm, setLitifyPopup, setClioPopup, setOpenDocEditor, customObject }) {

      if (record) {
        try {
          const finalRecord = generatedType === 'teksign' ? (Object.assign({}, { case_id: record.id, document_type: record.document_type, document_generation_type: generatedType, client_id: record.client_id, legalforms_id: record.legalforms_id })) : generatedType === 'propound_doc' ? (Object.assign({}, { case_id: record.id, document_type: record.document_type, document_generation_type: generatedType, client_id: record.client_id, propoundforms_id: record.propound_id })) : (Object.assign({}, { case_id: record.id, document_type: record.document_type, document_generation_type: generatedType === 'final' ? 'final_doc' : generatedType }));
          if(finalRecord) {
            const result = yield call(loadBillingDetails, finalRecord);
            const payment = yield call(getClientSecret);

            if (result) {
              yield put(loadBillingDetailsSuccess(Object.assign({}, { ...result }, { client_secret: payment.client_secret })));
              yield put(updateRecordSuccess(Object.assign({}, { id: record.id, lawyers: record.lawyers || [] })));

              const case_info = yield select(selectRecord(record.id));
              const user_info = yield call(verifySession);
              const planType = user_info && user_info.plan_type && typeof user_info.plan_type === 'object' && user_info.plan_type['responding'] || false;

              const tekasyougo_practices = user_info && user_info.tekasyougo_practices && typeof user_info.tekasyougo_practices === 'string' && user_info.tekasyougo_practices.split(',') || [];
              const document_access = user_info && user_info.practiceDetails && tekasyougo_practices && tekasyougo_practices.includes(user_info.practiceDetails.id) && planType && ['tek_as_you_go', 'pay_as_you_go'].includes(planType) || false;

              if (result.custom_quote_needed) {
                yield call(dialog);
              } else if(user_info && (user_info?.role == 'lawyer' && !user_info.state_bar_number || !user_info.user_name) && openRecordForm){
                yield call(openRecordForm);
              } else if(openGeorgiaTemplate && record.state == "GA" && generatedType == "template" && result.free_tier_available) {
                yield call(openGeorgiaTemplate);
              } else if((result.free_tier_available || planType && ['tek_as_you_go', 'pay_as_you_go'].includes(planType)) && openTemplateForm) {
                yield call(openTemplateForm);
              } else if (record.document_type && record.document_type != 'MEDICAL_HISTORY' && !document_access && ['tek_as_you_go', 'pay_as_you_go'].includes(planType) && freeTrialDialog) {
                yield call(freeTrialDialog);
              } else if (freeTrialDialog && record.document_type && !planType) {
                yield call(freeTrialDialog);
              } else if (result.free_tier_available && progress && record.document_type === 'TEKSIGN') {
                yield call(generateDocument);
              } else if (result.free_tier_available && progress && record.document_type === 'MEDICAL_HISTORY') {
                yield put(generateMedicalSummaryAction(Object.assign({}, {
                  practice_id: record.practice_id,
                  case_id: record.id,
                  client_id: record.client_id,
                  case_number: record.case_number,
                  case_title: record.case_title,
                  status: 'New Request',
                  assigned_to: '',
                  request_date: moment().format('MM/DD/YYYY HH:mm:ss'),
                  practice_name: record.practice_name,
                  summery_url: '',
                  plan_type: planType
                })));
              } else if (result.free_tier_available && progress && generatedType === 'pos') {
                yield put(generatePOSDocumentAction(record, posModal, customObject));
              } else if (generatedType === 'propound_doc' && result.free_tier_available && progress) {
                yield call(progress, generatedType);
                yield put(generatePropoundDocumentAction(record, user_info, case_info, generatedType, signature, progress, setOpenDocEditor));
              } else if (result.free_tier_available && progress) {
                yield call(progress, generatedType);
                yield put(fetchFormAction(record, generatedType, setFilevinePopUp, setMyCasePopUp, progress, signature, setLitifyPopup, setClioPopup, setOpenDocEditor));
              } else if (record.document_type !== 'TEKSIGN' && record.document_type !== 'MEDICAL_HISTORY' && (!result.free_tier_available && (planType === 'free_trial') && subscriptionAlert)) {
                yield call(subscriptionAlert);
              } else if (!result.free_tier_available && !planType && freeTrialDialog) {
                yield call(freeTrialDialog);
              } else if (!result.free_tier_available && !result.credit_card_details_available && form) {
                yield call(form, true);
              } else if (!result.free_tier_available && result.credit_card_details_available && dialog) {
                yield call(dialog);
              }
            } else {
              yield put(loadBillingDetailsError(DEFAULT_LOAD_BILLING_DETAILS_ERROR));
            }
          }
        } catch (error) {
          yield put(loadBillingDetailsError(DEFAULT_LOAD_BILLING_DETAILS_ERROR));
        }
      }
    })
  }

  function* updateBillingDetailsSaga() {
    yield takeLatest(UPDATE_BILLING_DETAILS, function* updater({ payment = {}, record, progress, generatedType, form, posModal, setFilevinePopUp, setMyCasePopUp, setLitifyPopup }) {
      
      if (payment) {
        yield put(startSubmit(form));
        try {

          if (payment && !payment.error) {
            const { setupIntent = {} } = payment;
            const result = yield call(updateBillingDetails, Object.assign({}, { payment_method: setupIntent.payment_method }));

            if (result) {
              const user_info = yield select(selectUser())
              const planType = user_info && user_info.plan_type && typeof user_info.plan_type === 'object' && user_info.plan_type['responding'] || false;
              if (progress && record.document_type === 'MEDICAL_HISTORY') {
                yield put(generateMedicalSummaryAction(Object.assign({}, {
                  practice_id: record.practice_id,
                  case_id: record.id,
                  client_id: record.client_id,
                  case_number: record.case_number,
                  case_title: record.case_title,
                  status: 'New Request',
                  assigned_to: '',
                  request_date: moment().format('MM/DD/YYYY HH:mm:ss'),
                  practice_name: record.practice_name,
                  summery_url: '',
                  plan_type: planType
                })));
                yield put(stopSubmit(form));
              } else if (progress) {
                if (record && generatedType === 'teksign') {
                  yield put(sendVerificationAction(Object.assign({}, record, { plan_type: planType })));
                  yield put(stopSubmit(form));
                } else if(record && generatedType === 'pos'){                  
                  yield put(generatePOSDocumentAction(record, posModal));
                } else {
                  yield call(progress, generatedType);
                  yield put(fetchFormAction(record, generatedType, setFilevinePopUp, setMyCasePopUp, progress, false, record.signature, setLitifyPopup));
                  yield put(stopSubmit(form));
                } 
              } else {
                yield put(loadBillingDetailsAction(false, false, false, { document_type: 'FROGS' }, 'template'));
                yield put(stopSubmit(form));
                yield put(updateBillingDetailsSuccess('Billing Details Updated'));
              }
            } else {
              yield put(updateBillingDetailsError(DEFAULT_UPDATE_BILLING_DETAILS_ERROR));
              yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
            }
          } else {
            yield put(updateBillingDetailsError(payment && payment.error && payment.error.message));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
          }

        } catch (error) {
          yield put(updateBillingDetailsError(DEFAULT_UPDATE_BILLING_DETAILS_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
        }
      }
    })
  }

  function* saveStandardFormSaga() {
    yield takeLatest(SAVE_STANDARD_FORM, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const upload = yield call(uploadForm, record);
          if (upload) {
            const DiscType = standardFormColumns && Object.keys(standardFormColumns).reduce((q, nc) => {
              if (!q && record.questions && record.questions.length > 0) {
                const arr = record.questions.find(d => standardFormColumns[nc].find(c => d['question_number'].toString() === c['question_number'].toString()));
                if (arr)
                  q = nc;
              }
              return q;
            }, false);
            const save = yield call(saveForm, Object.assign({}, { ...record }, { id: upload.legalForms.id, document_type: record.document_type.toUpperCase() }));
            const user_info = yield select(selectUser());
            let isPropoundPractice = user_info && record && isPropound(user_info, record);
            if(record && record.questionTemplate) {
              const submitRecord = Object.assign({}, { file_name: record.filename || record.file_name, questions: record.questions, disc_type: record.question_type ? record.question_type : DiscType, state: record.state });
              yield call(saveDefaultQuestionsTemplate, submitRecord)
            }
            if (save) {
              yield put(stopSubmit(form));
              yield put(saveStandardFormSuccess("Form Saved"));
              yield delay(500);
              yield put(destroy(form));
              yield put(push({ pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms` : `/casesCategories/cases/${record.case_id}/form`, state: Object.assign({}, { ...history.location.state }, { legalform_id: null, defaultQuestions: [], useSavedTemplate: false, filename: false })}));
            } else {
              yield put(saveStandardFormError(DEFAULT_SAVE_STANDARD_FORM_ERROR));
              yield put(stopSubmit(form, { _error: DEFAULT_SAVE_STANDARD_FORM_ERROR }));
            }
          } else {
            yield put(saveStandardFormError(DEFAULT_SAVE_STANDARD_FORM_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_SAVE_STANDARD_FORM_ERROR }));
          }
        } catch (error) {
          yield put(saveStandardFormError(DEFAULT_SAVE_STANDARD_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_SAVE_STANDARD_FORM_ERROR }));
        }
      }
    })
  }



  function* saveQuestionsFormSaga() {
    yield takeLatest(SAVE_QUESTIONS_FORM, function* updater({ record }) {
      if (record) {

        let { questions = [] } = record;
        const name = history && history.location.pathname;
        let duplicate;
        try {
          questions = typeof questions === 'string' && JSON.parse(questions) || questions;
          const questionCategory = questions && questions?.length > 0 && questions?.[0] && questions[0]?.question_category || false;
          if(questionCategory) {
            const categoryQuestion = questions && questions?.length > 0 && lodash.groupBy(questions, 'question_category_id') || false;
            if(categoryQuestion) {
              Object.keys(categoryQuestion).map(key => {
                duplicate = getDuplicates(categoryQuestion[key].map(_ => { return _.question_number }));

                if(duplicate) return 1;
                
              })
            }
          } else {
            if (record.form && record.form === 'questionsForm') {
              duplicate = getDuplicates(questions.map(_ => { return _.question_number }));
            }
          }
          
          const validate = questions.filter(_ => _.question_text === '' || _.question_number === '' || _.question_number_text === '');

          if (validate && validate.length === 0) {
            if (duplicate && duplicate != '' && record.form && record.form === 'questionsForm') {
              yield put(saveQuestionsFormError(`Question Number ${duplicate} duplicated`));
            }
            else {
              const result = record.from === 'questions_form' ? record : yield call(deleteForm, record);
              if (result) {
                const upload = yield call(uploadForm, record);
                if (upload) {
                  const save = yield call(saveForm, Object.assign({}, { ...record }, { id: upload.legalForms.id, document_type: record.document_type.toUpperCase(), questions }));
                  if (save) {
                    yield put(saveQuestionsFormSuccess("Questions Saved"));
                    if (name && name.indexOf('help-request') > -1) {
                      yield put(supportRequestSaga.actions.loadRecords(true));
                      yield put(push({ pathname: `/supportCategories/rest/help-request`, state: Object.assign({}, { ...history.location.state }, { legalform_id: null }) }));
                    } else {
                      const user_info = yield select(selectUser());
                      let isPropoundPractice = user_info && record && isPropound(user_info, record);

                      yield put(push({ pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms` : `/casesCategories/cases/${record.case_id}/form`, state: Object.assign({}, { ...history.location.state }, { legalform_id: null }) }));
                    }
                  } else {
                    yield put(saveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
                  }
                } else {
                  yield put(saveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
                }
              } else {
                yield put(saveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
              }
            }
          } else {
            yield put(saveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_VALIDATE_ERROR));
          }

        } catch (error) {
          yield put(saveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
        }
      }
    })
  }


  function* createMedicalHistorySaga() {
    yield takeLatest(CREATE_MEDICAL_HISTORY, function* updater({ records, form, setMedicalHistory, setSummaryConfirmation }) {
      if (records) {
        yield put(startSubmit(form));
        try {
          const uploadArr = yield all(records.map(el => call(s3BucketPrivate, el.file_name, getContentType(el.file_name), el.uploadFile, extractTotalPages, 'pages', el.comment, el)));
          const result = yield call(createMedicalHistory, uploadArr);
          if (result) {
            yield call(setMedicalHistory, false);
            const recordInStore = yield select(selectRecord());
            yield put(casesActions.loadRecord(recordInStore.id));
            yield put(stopSubmit(form));
            if(setSummaryConfirmation) {
              yield delay(2000);
              yield call(setSummaryConfirmation, true);
            }
          } else {
            yield put(createMedicalHistoryError(DEFAULT_CREATE_MEDICAL_HISTORY_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CREATE_MEDICAL_HISTORY_ERROR }));
          }
        } catch (error) {
          yield put(createMedicalHistoryError(DEFAULT_CREATE_MEDICAL_HISTORY_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_CREATE_MEDICAL_HISTORY_ERROR }));
        }
      }
    })
  }

  function* updateMedicalHistorySaga() {
    yield takeLatest(UPDATE_MEDICAL_HISTORY, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(updateMedicalHistory, record);
          if (result) {
            yield put(updateMedicalHistorySuccess('Medical Document Updated'));
          } else {
            yield put(updateMedicalHistoryError(DEFAULT_UPDATE_MEDICAL_HISTORY_ERROR));
          }
        } catch (error) {
          yield put(updateMedicalHistoryError(DEFAULT_UPDATE_MEDICAL_HISTORY_ERROR));
        }
      }
    })
  }

  function* deleteMedicalHistorySaga() {
    yield takeLatest(DELETE_MEDICAL_HISTORY, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(deleteMedicalHistory, record);
          if (result) {
            yield put(casesActions.loadRecord(record.case_id));
            yield put(deleteMedicalHistorySuccess("Document Deleted"));
          } else {
            yield put(deleteMedicalHistoryError(DEFAULT_DELETE_MEDICAL_HISTORY_ERROR));
          }
        } catch (error) {
          yield put(deleteMedicalHistoryError(DEFAULT_DELETE_MEDICAL_HISTORY_ERROR));
        }
      }
    })
  }

  function* viewMedicalHistorySaga() {
    yield takeLatest(VIEW_MEDICAL_HISTORY, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(viewMedicalHistory, record);
          if (result && result.publicUrl) {
            yield call(viewPDF, result.publicUrl);
            yield put(loadRecordsCacheHit());
          } else {
            yield put(viewMedicalHistoryError(DEFAULT_VIEW_MEDICAL_HISTORY_ERROR));
          }
        } catch (error) {
          yield put(viewMedicalHistoryError(DEFAULT_VIEW_MEDICAL_HISTORY_ERROR));
        }
      }
    })
  }


  function* generateMedicalSummarySaga() {
    yield takeLatest(GENERATE_MEDICAL_SUMMARY, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(generateMedicalSummary, record);

          if (result) {
            if(result.invoice_pdf_data) {
              var docDefinition = getInvoicePdfJson(result.invoice_pdf_data);
              let pdfmake = yield call(CreatePDF, '_blob', docDefinition, result.invoice_pdf_data.invoice_name);

              if (result.invoice_pdf_data.uploadURL) {
                yield call(uploadFile, result.invoice_pdf_data.uploadURL, pdfmake, pdfmake.type);
                yield call(sendInvoice, Object.assign({}, { invoice_s3_key: result.invoice_pdf_data.s3_file_key, invoice_name: result.invoice_pdf_data.invoice_name, client_id: result.client_id, case_id: result.case_id, practice_id: result.practice_id }))
              }
            }          
            yield put(generateMedicalSummarySuccess('Medical Summary Created'));
            const recordInStore = yield select(selectRecord());
            yield put(casesActions.loadRecord(recordInStore.id));
          } else {
            yield put(generateMedicalSummaryError(DEFAULT_GENERATE_MEDICAL_SUMMARY_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_GENERATE_MEDICAL_SUMMARY_ERROR;
          yield put(generateMedicalSummaryError(Err));
        }
      }
    })
  }



  function* requestHelpSaga() {
    yield takeLatest(REQUEST_HELP, function* updater({ record, stepperLoader }) {
      if (record) {

        try {
          const result = yield call(requestHelp, record);

          if (result) {
            yield put(requestHelpSuccess('Support request sent to EsquireTek. Our support team will extract questions from this document and update the system. You will receive an email once questions are updated.'));
            yield call(cancelForm, Object.assign({}, { case_id: record.case_id, client_id: record.client_id, status: 'cancelled', document_type: record.document_type, s3_file_key: record.document_s3_key, id: record.legalforms_id }));
          } else {
            yield put(requestHelpError(DEFAULT_REQUEST_HELP_ERROR));
          }
        } catch (error) {
          yield put(requestHelpError(DEFAULT_REQUEST_HELP_ERROR));
        } finally {
          if (stepperLoader) {
            yield call(stepperLoader, false);
          }
        }
      }
    })
  }


  function* requestTranslationSaga() {
    yield takeLatest(REQUEST_TRANSLATION, function* updater({ record, spinner }) {
      if (record) {
        try {
          const result = yield call(requestTranslation, record);

          if (result) {
            const recordInStore = yield select(selectRecord(record.case_id));
            const questions = recordInStore && recordInStore['questions'] || {};
            yield put(updateRecordSuccess(Object.assign({}, { id: record.case_id, questions: questions.map(data => data.id === record.id ? Object.assign({}, { ...data }, { translation: result.translateApiResponse.TranslatedText }) : data) })));
          } else {
            yield put(requestTranslationError(DEFAULT_REQUEST_TRANSLATION_ERROR));
          }
        } catch (error) {
          yield put(requestTranslationError(DEFAULT_REQUEST_TRANSLATION_ERROR));
        } finally {
          yield call(spinner, false);
        }
      } else {
        yield put(requestTranslationError(DEFAULT_REQUEST_TRANSLATION_ERROR));
      }
    })
  }


  function* mergeTranslationSaga() {
    yield takeLatest(MERGE_TRANSLATION, function* updater({ record, charLimit }) {
      if (record) {
        try {
          const htmlRegExp = /<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g;
          const lawyerResponse = record && record.lawyer_response_text;
          const lawyerResponseText = lawyerResponse && lawyerResponse.replace(htmlRegExp, '') || '';

          let translateText = record && record.translation;
          translateText = translateText && translateText.replace(htmlRegExp, '');
          const responseLength = lawyerResponseText.length + (translateText && translateText.length);
         
          if (charLimit >= responseLength) {
            const recordInStore = yield select(selectRecord(record.case_id));
            const questions = recordInStore && recordInStore['questions'] || {};
            const recordArr = Object.keys(record);

            for (var i = 0; i < Object.keys(record).length; i++) {
              if (recordArr[i] != 'lawyer_response_text')
                yield put(change(`detailForm_${record.id}`, recordArr[i], record[recordArr[i]]));
            }
            yield delay(200);
            yield put(change(`detailForm_${record.id}`, 'lawyer_response_text', `${record.lawyer_response_text || ''} \n${record.translation}`));
            yield put(updateRecordSuccess(Object.assign({}, { id: record.case_id, questions: questions.map(data => data.id === record.id ? Object.assign({}, { ...data }, { lawyer_response_text: `${data.lawyer_response_text || ''} \n${record.translation}`, translation: false }) : data) })));
            yield put(mergeTranslationSuccess('Translation Copied Successfully.'));
          }
        } catch (error) {
          yield put(mergeTranslationError(DEFAULT_MERGE_TRANSLATION_ERROR));
        }
      }
    })
  }

  function* sendVerificationSaga() {
    yield takeLatest(SEND_VERIFICATION, function* updater({ record }) {
      if (record) {
        const { case_id, document_type } = record;

        try {
          const result = yield call(sendVerification, record);

          if (result) {
            const user_info = yield select(selectUser());
            let isPropoundPractice = user_info && record && isPropound(user_info, record);

            yield put(sendVerificationSuccess('Finalized response sent to client for signature'));
            yield delay(2000);
            yield put(push({ pathname: isPropoundPractice ? `/casesCategories/cases/${case_id}/forms/respond/${document_type}/details` : `/casesCategories/cases/${case_id}/form/${document_type}`, state: history.location.state }));
          } else {
            yield put(sendVerificationError(DEFAULT_SEND_VERIFICATION_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SEND_VERIFICATION_ERROR;
          yield put(sendVerificationError(Err));
        }
      }
    })
  }


  // function* autoSaveFormSaga() {
  //   yield takeLatest(AUTO_SAVE_FORM, function* updater({ form }) {
  //     if (form) {
  //       try {
  //         const storeForms = yield select(selectForm());
  //         const selectedForms = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
  //           if (key.indexOf(form) > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
  //             arr.push(storeForms[key])
  //           }
  //           return arr;
  //         }, []).map(_ => _.values).filter(_ => !_.lawyer_response_status || (_.lawyer_response_status && _.lawyer_response_status != 'Final')) || [];
  //         let i = 0;
  //         while (i < selectedForms.length) {
  //           const result = yield call(updateForm, selectedForms[i]);
  //           if (result) {
  //             yield delay(1000);
  //             i++;
  //           } else {
  //             yield put(autoSaveFormError(DEFAULT_AUTO_SAVE_FORM_ERROR));
  //           }
  //         }
  //       } catch (error) {
  //         yield put(autoSaveFormError(DEFAULT_AUTO_SAVE_FORM_ERROR));
  //       }
  //     }
  //   })
  // }

  function* autoSaveFormSaga() {
    yield takeLatest(AUTO_SAVE_FORM, function* updater({ record, forms }) {
      if(record && forms) {
        try {
          while (true) { // eslint-disable-line no-constant-condition
            const recordsInStore = yield select(selectRecords());
            if (recordsInStore && recordsInStore.length > 0) {
              break;
            }
            yield delay(500);
          }

          let selectedForms;
          let autoSaveForm;
          let fieldValue;
          const storeForms = yield select(selectForm());
          const storeRecord = yield select(selectRecord());
         
          if(forms?.length > 0) {
            let i = 0;
            while(i < forms.length) {
              autoSaveForm = forms?.[i] === 'detailForm_' ? storeRecord?.autoSaveForm : forms?.[i] === 'objectionForm_' ? storeRecord?.autoSaveObjectionForm : false;
              fieldValue = forms?.[i] === 'detailForm_' ? 'lawyer_response_text' : forms?.[i] === 'objectionForm_' ? 'lawyer_objection_text' : false;
              const clearForm = forms?.[i] === 'detailForm_' ? Object.assign({}, { autoSaveForm: false }) : forms?.[i] === 'objectionForm_' ? Object.assign({}, { autoSaveForm: false }) : false;
              if(autoSaveForm) {
                selectedForms = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
                  if (key.indexOf(forms?.[i]) > -1 && autoSaveForm.includes(key) && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values'][fieldValue]) {
                    arr.push(storeForms[key]);
                  }
                  return arr;
                }, []).map(_ => _.values) || [];
              }
    
              if (selectedForms && Array.isArray(selectedForms) && selectedForms.length > 0) {
                if(forms[i] === 'objectionForm_') {
                  const recordInStore = yield select(selectRecord(record.case_id));
                  let storeQuestions = recordInStore && recordInStore.questions;
                  let questionGroups = lodash.groupBy(storeQuestions, 'question_number_text');
                  selectedForms = [...selectedForms];
                  let j = 0;
                  while(j < selectedForms.length) {
                    if(storeQuestions?.length > 0 && (storeQuestions.map(_ => _.id)).includes(selectedForms[j]?.id)) {
                      const question_number_text = selectedForms[j]?.question_number_text;
                      let detail = questionGroups[question_number_text];
                      if(detail?.length) {
                        selectedForms = selectedForms.map(e => e.id === selectedForms[j]?.id ? Object.assign({}, { ...e }, { subgroupIds: detail?.map(_ => _.id) }) : e);
                      }
                    }
                    j++;
                  }
                }

                let loop = 0;
                let currentIndex = 1;
                let totalSize = 20;
                const totalSlice = selectedForms.length % totalSize == 0 ? Math.floor(selectedForms.length / totalSize) : Math.floor((selectedForms.length / totalSize) + 1);
                if(clearForm) {
                  yield put(clearAutoSaveFormDetails(clearForm));
                }
                while (loop < totalSlice) {
                  const firstIndex = (currentIndex - 1) * totalSize;
                  const lastIndex = firstIndex + totalSize;
                  const saveRecords = selectedForms.slice(firstIndex, lastIndex);
                  let result;
                  if(forms[i] === 'objectionForm_') {
                    result = yield call(saveAllObjectionForm, saveRecords.filter(_ => _.lawyer_objection_text).map(r => Object.assign({}, r, { lawyer_objection_status: r.lawyer_objection_status && r.lawyer_objection_status.toString() === 'Final' ? 'Final' : 'Draft' })));
                  } else {
                    result = yield call(autoSaveForms, saveRecords.filter(_ => _.lawyer_response_text).map(r => Object.assign({}, r, { lawyer_response_status: r.lawyer_response_status && r.lawyer_response_status.toString() === 'Final' ? 'Final' : 'Draft' })));
                  }
    
                  if (result) {
                    loop++;
                    currentIndex++;
                  } else {
                    yield put(autoSaveFormError(DEFAULT_AUTO_SAVE_FORM_ERROR));
                  }
                }
              }
              i++;
            }
          }
        } catch (error) {
          yield put(autoSaveFormError(DEFAULT_AUTO_SAVE_FORM_ERROR));
        }
      }
    })
  }


  function* copyToLawyerResponseSaga() {
    yield takeLatest(COPY_TO_LAWYER_RESPONSE, function* updater({ record, openForm, charLimit }) {
      if (record) {
        try {
          const htmlRegExp = /<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g;
          const selectForms = yield select(selectForm());
          const selectFormRecord = selectForms && selectForms[`detailForm_${record.id}`] && selectForms[`detailForm_${record.id}`].values;

          const lawyerResponse = openForm ? selectFormRecord && selectFormRecord.lawyer_response_text : record && record.lawyer_response_text;
          const lawyerResponseText = lawyerResponse && lawyerResponse.replace(htmlRegExp, '') || '';

          let clientText = record && record.client_response_text;
          clientText = clientText && clientText.replace(htmlRegExp, '');

          const responseLength = lawyerResponseText.length + (clientText && clientText.length);

          if (responseLength <= charLimit) {
            const recordInStore = yield select(selectRecord(record.case_id));
            if (openForm) {
              yield put(change(`detailForm_${record.id}`, 'lawyer_response_text', `${lawyerResponse || ''} \n${record.client_response_text}`));
            } else {
              yield put(initialize(`detailForm_${record.id}`, record));
              yield put(change(`detailForm_${record.id}`, 'lawyer_response_text', `${lawyerResponse || ''} \n${record.client_response_text}`));
              yield put(change(`detailForm_${record.id}`, 'copyNote', true));
            }
            const questions = recordInStore && recordInStore['questions'] || {};
            
            const ques = questions.find(_ => _.id === record.id);
            yield put(updateRecordSuccess(Object.assign({}, { id: record.case_id, questions: questions.map(data => data.id === record.id ? Object.assign({}, { ...data }, { lawyer_response_text: `${lawyerResponse || ''} \n${record.client_response_text || ''}`, translation: false }) : data) })));
            yield put(copyToLawyerResponseSuccess(ques && ques.lawyer_response_text ? 'Client Response copied below existing text in the Attorney Response.' : 'Client Response Copied.'));
          }
        } catch (error) {
          yield put(copyToLawyerResponseError(DEFAULT_COPY_TO_LAWYER_RESPONSE_ERROR));
        }
      }
    })
  }


  function* shredSaga() {
    yield takeLatest(SHRED, function* updater({ record, loadRecord }) {
      if (record) {
        try {
          const result = yield call(shred, record);
          if (result) {
            yield put(shredSuccess('Discovery Document Deleted'));
            yield delay(500);
            yield put(loadRecord(record.case_id));
          } else {
            yield put(shredError(DEFAULT_SHRED_ERROR));
          }
        } catch (error) {
          yield put(shredError(DEFAULT_SHRED_ERROR));
        }
      }
    })
  }

  function* editFormSaga() {
    yield takeLatest(EDIT_FORM, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(editForm, record);
          if (result) {
            const editRecord = Object.assign({}, record, { [record.document_type]: record[record.document_type].map(r => r.legalform_id.toString() === record.legalFormId.toString() ? Object.assign({}, r, { filename: record.filename }) : r) });
            yield put(editFormSuccess(Object.assign({}, { id: record.id, [record.document_type]: editRecord[record.document_type] }), 'Form Name Changed Successfully.'));
          } else {
            yield put(editFormError(DEFAULT_EDIT_FORM_ERROR));
          }
        } catch (error) {
          yield put(editFormError(DEFAULT_EDIT_FORM_ERROR));
        }
      }
    })
  }

  function* attachCaseSaga() {
    yield takeLatest(ATTACH_CASE, function* updater({ record, form, setAttchCaseForm }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(attachCase, record);
          if (result) {
            yield put(attachCaseSuccess('The case cloned successfully'));
            yield put(casesActions.loadRecords(true));
            yield delay(500);
            yield call(setAttchCaseForm, false);
            yield put(push({ pathname: '/casesCategories/cases', state: history.location.state }));
          }
          else {
            yield put(attachCaseError(DEFAULT_ATTACH_CASE_ERROR))
            yield put(stopSubmit(form));
          }
        }
        catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_ATTACH_CASE_ERROR;
          yield put(attachCaseError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    })
  }


  function* createClientRecordSaga() {
    yield takeLatest(CREATE_CLIENT_RECORD, function* updater({ record, form, setCreateClientForm }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(createClientRecord, record);
          if (result) {
            yield put(createClientRecordSuccess('The case cloned successfully'));
            yield put(casesActions.loadRecords(true));
            yield delay(500);
            yield call(setCreateClientForm, false);
            yield put(push({ pathname: '/casesCategories/cases', state: history.location.state }));
          }
          else {
            yield put(createClientRecordError(DEFAULT_CREATE_CLIENT_RECORD_ERROR))
            yield put(stopSubmit(form));
          }
        }
        catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_CLIENT_RECORD_ERROR;
          yield put(createClientRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    })
  }


  function* setResponseDateSaga() {
    yield takeLatest(SET_RESPONSE_DATE, function* updater({ record, form, setDueDate }) {
      try {
        const result = yield call(setResponseDate, record);
        if (result) {
          const recordInStore = yield select(selectRecord(record.case_id));
          const editRecord = Object.assign({}, recordInStore, { [record.document_type]: recordInStore[record.document_type].map(r => r.legalform_id.toString() === record.legalforms_id.toString() ? Object.assign({}, r, { response_deadline_startdate: record.start_date, response_deadline_enddate: record.end_date }) : r) });
          yield put(setResponseDateSuccess(Object.assign({}, { id: record.case_id, due_date: result.due_date, [record.document_type]: editRecord[record.document_type] }), 'Set Due Date Saved'));
        } else {
          yield put(setResponseDateError(DEFAULT_SET_RESPONSE_DATE_ERROR));
        }
      } catch (error) {
        yield put(setResponseDateError(DEFAULT_SET_RESPONSE_DATE_ERROR));
      }
    })
  }


  function* resendQuestionsSaga() {
    yield takeLatest(RESEND_QUESTIONS, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(resendQuestions, record);
          if (result) {
            yield put(resendQuestionsSuccess('Questions resent to client'));
          } else {
            yield put(resendQuestionsError(DEFAULT_RESEND_QUESTIONS_ERROR));
          }
        } catch (error) {
          yield put(resendQuestionsError(DEFAULT_RESEND_QUESTIONS_ERROR));
        }
      }
    })
  }


  function* saveAllFormSaga() {
    yield takeLatest(SAVE_ALL_FORM, function* updater({ form, record, setCloseEditor }) {
      if (form) {
        try {
          // SaveAll Attorney Response forms
          const storeForms = yield select(selectForm());
          const draftRecords = record && record.questions && Array.isArray(record.questions) && record.questions.length > 0 ? record.questions.filter(_ => _.lawyer_response_status && _.lawyer_response_status === 'Draft' && _.lawyer_response_text) : [];
          const formRecords = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
            if (key.indexOf(form) > -1 && storeForms[key] && storeForms[key].values) {
              arr.push(storeForms[key]);
            }
            return arr;
          }, []).map(_ => Object.assign({}, _.values)) || [];
          let records = draftRecords && formRecords && draftRecords.length > 0 && formRecords.length > 0 ? draftRecords.concat(formRecords) : draftRecords && draftRecords.length > 0 ? draftRecords : formRecords && formRecords.length > 0 ? formRecords : [];
          records = record && records && Array.isArray(records) && records.length > 0 ? records.filter(_ => _.legalforms_id === record.legalforms_id) : []; 

          records = records?.length > 0 && [...new Map(records.map((r) => [r.id, r])).values()] || [];

          if (records && Array.isArray(records) && records.length > 0) {
            let loop = 0;
            let currentIndex = 1;
            let totalSize = 20;
            const totalSlice = records.length % totalSize == 0 ? Math.floor(records.length / totalSize) : Math.floor((records.length / totalSize) + 1);

            while (loop < totalSlice) {
              const firstIndex = (currentIndex - 1) * totalSize;
              const lastIndex = firstIndex + totalSize;
              const saveRecords = records.slice(firstIndex, lastIndex);
              const result = yield call(saveAllForm, saveRecords.filter(_ => _.lawyer_response_text).map(r => Object.assign({}, r, { lawyer_response_status: 'Final' })));  //Calling save all form API
              if (result) {
                /**
                 * Destroy all lawyer response form
                 */
                let i = 0;
                while (i < saveRecords.length) {
                   yield put(destroy(`${form}${saveRecords[i]['id']}`));
                  yield delay(20);
                  i++;
                }

                loop++;
                currentIndex++;
              } else {
                yield put(saveAllFormError(DEFAULT_SAVE_ALL_FORM_ERROR));
              }
            }
          }

          // SaveAll Attorney Objection forms
          const currentStoreForms = yield select(selectForm());
          const draftObjectionRecords = record && record.questions && Array.isArray(record.questions) && record.questions.length > 0 ? record.questions.filter(_ => _.lawyer_objection_status && _.lawyer_objection_status === 'Draft' && _.lawyer_objection_text) : [];
          const objectionFormRecords = currentStoreForms && Object.keys(currentStoreForms).length > 0 && Object.keys(currentStoreForms).reduce((arr, key) => {
            if (key.indexOf('objectionForm_') > -1 && currentStoreForms[key] && currentStoreForms[key].values) {
              arr.push(currentStoreForms[key]);
            }
            return arr;
          }, []).map(_ => Object.assign({}, _.values)) || [];
          let objectionRecords = draftObjectionRecords && objectionFormRecords && draftObjectionRecords.length > 0 && objectionFormRecords.length > 0 ? draftObjectionRecords.concat(objectionFormRecords) : draftObjectionRecords && draftObjectionRecords.length > 0 ? draftObjectionRecords : objectionFormRecords && objectionFormRecords.length > 0 ? objectionFormRecords : [];
          objectionRecords = objectionRecords && objectionRecords && Array.isArray(objectionRecords) && objectionRecords.length > 0 ? objectionRecords.filter(_ => _.legalforms_id === record.legalforms_id) : []; 

          objectionRecords = objectionRecords?.length > 0 && [...new Map(objectionRecords.map((r) => [r.id, r])).values()] || [];

          if (objectionRecords && Array.isArray(objectionRecords) && objectionRecords.length > 0) {
            const recordInStore = yield select(selectRecord(record.case_id));
            let storeQuestions = recordInStore && recordInStore.questions;
            let questionGroups = lodash.groupBy(storeQuestions, 'question_number_text');
            let loop = 0;
            let currentIndex = 1;
            let totalSize = 20;
            const totalSlice = objectionRecords.length % totalSize == 0 ? Math.floor(objectionRecords.length / totalSize) : Math.floor((objectionRecords.length / totalSize) + 1);
            let finalarray = [...objectionRecords];
            let i = 0;
            while(i < finalarray.length) {
              if(storeQuestions?.length > 0 && (storeQuestions.map(_ => _.id)).includes(finalarray[i]?.id)) {
                const question_number_text = finalarray[i]?.question_number_text;
                let detail = questionGroups[question_number_text];
                if(detail?.length) {
                  let subgroupIds = detail?.map(_ => _.id);
                  finalarray = finalarray.map(e => e.id === finalarray[i]?.id ? Object.assign({}, { ...e }, { subgroupIds }) : e);
                }
              }
              yield delay(20);
              i++;
            }

            if(finalarray?.length > 0) {
              while (loop < totalSlice) {
                const firstIndex = (currentIndex - 1) * totalSize;
                const lastIndex = firstIndex + totalSize;
                const finalRecords = finalarray.slice(firstIndex, lastIndex);
                const result = yield call(saveAllObjectionForm, finalRecords.filter(_ => _.lawyer_objection_text).map(r => Object.assign({}, r, { lawyer_objection_status: 'Final' })));  //Calling save all form API
                if (result) {
                  /**
                   * Destroy all lawyer objection form
                   */
                  let i = 0;
                  while (i < finalRecords.length) {
                     yield put(destroy(`objectionForm_${finalRecords[i]['id']}`));
                    yield delay(20);
                    i++;
                  }
  
                  loop++;
                  currentIndex++;
                } else {
                  yield put(saveAllFormError(DEFAULT_SAVE_ALL_FORM_ERROR));
                }
              }
            }
          }

          yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
          yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id })));
        } catch (error) {
          yield put(saveAllFormError(DEFAULT_SAVE_ALL_FORM_ERROR));
        } finally {
          yield call(setCloseEditor, false);
        }
      }
    })
  }


  function* saveSubGroupFormSaga() {
    yield takeLatest(SAVE_SUB_GROUP_FORM, function* updater({ record, setOpenForm }) {
      if (record) {
        try {
          if(record.subgroup){
            const storeForms = yield select(selectForm());
            const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
              if (key.indexOf('detailForm_') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
                arr.push(storeForms[key])
              }
              return arr;
            }, []).map(_ => _.values).find(_ => _.id === record.id) || false;
            
            if(selectedForm) {
              yield put(updateFormAction(Object.assign({}, selectedForm, { lawyer_response_status: 'Final', subgroup: record.subgroup }), `detailForm_${record.id}`, setOpenForm, true));
            }

          }

          const storeForms = yield select(selectForm());
          const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
              if (key.indexOf('detailForm_') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
                arr.push(storeForms[key])
              }
              return arr;
            }, []).map(_ => _.values).find(_ => _.id === record.id) || false;
          const recordInStore = yield select(selectRecord(record.case_id));

          const questionInStore = recordInStore && recordInStore.questions && recordInStore.questions.length > 0 ? recordInStore.questions.filter(_ => _.question_number_text === record.question_number_text) : [];
          let finalRecords;
          let saveRecords;
          if(record && record.is_consultation_set && record.consultation_set_no) {
            let res = lodash.groupBy(questionInStore, 'consultation_set_no');
            finalRecords = res[record.consultation_set_no];
            saveRecords = finalRecords.map(r => r.id === record.id ? selectedForm && (Object.assign({}, selectedForm, { lawyer_response_status: 'Final', subgroup: record.subgroup })) || record : Object.assign({}, r, { lawyer_response_text: record.subgroup ? `<p>See response ${record.question_number_text} ${record.question_section} in Consultation ${record.consultation_set_no}</p>` : r.lawyer_response_text, subgroup: false }));
          } else if(record && record?.is_duplicate && record.duplicate_set_no) {
            let res = lodash.groupBy(questionInStore, 'duplicate_set_no');
            finalRecords = res[record.duplicate_set_no];
            saveRecords = finalRecords.map(r => r.id === record.id ? selectedForm && (Object.assign({}, selectedForm, { lawyer_response_status: 'Final', subgroup: record.subgroup })) || record : Object.assign({}, r, { lawyer_response_text: record.subgroup ? `<p>See response ${record.question_number_text} ${record.question_section} in Set ${record.duplicate_set_no}</p>` : r.lawyer_response_text, subgroup: false }));
          } else if(record && !record?.is_duplicate && record.duplicate_set_no && record?.question_number_text && record?.question_number_text.toString() === '17.1') {
            let res = lodash.groupBy(questionInStore, 'duplicate_set_no');
            finalRecords = res[record.duplicate_set_no];
            saveRecords = finalRecords.map(r => r.id === record.id ? selectedForm && (Object.assign({}, selectedForm, { lawyer_response_status: 'Final', subgroup: record.subgroup })) || record : Object.assign({}, r, { lawyer_response_text: record.subgroup ? `<p>See response ${record.question_number_text} ${record.question_section}</p>` : r.lawyer_response_text, subgroup: false }));
          } else {
            finalRecords = questionInStore;
            saveRecords = finalRecords.map(r => r.id === record.id ? selectedForm && (Object.assign({}, selectedForm, { lawyer_response_status: 'Final', subgroup: record.subgroup })) || record : Object.assign({}, r, { lawyer_response_text: record.subgroup ? `<p>See response ${record.question_number_text} ${record.question_section}</p>` : r.lawyer_response_text, subgroup: false }));
          }

          const result = yield call(saveSubGroupForm, saveRecords);  //Calling save sub group form API

          if (result) {
            yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id, client_id: record.client_id || false, party_id: record.party_id || false })));
            yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
            yield delay(1000);
            yield put(saveSubGroupFormSuccess('Form Saved Successfully'));
          } else {
            yield put(saveSubGroupFormError(DEFAULT_SAVE_SUB_GROUP_FORM_ERROR));
          }
        } catch (error) {
          yield put(saveSubGroupFormError(DEFAULT_SAVE_SUB_GROUP_FORM_ERROR));
        }
      }
    })
  }


  function* generatePOSDocumentSaga() {
    yield takeLatest(GENERATE_POS_DOCUMENT, function* updater({ record, posModal, customObject }) {
      if (record) {
        try {
          if(posModal) yield call(posModal, false);          

          const filterData = record && record.document_type && record.legalforms_id && record[record.document_type.toLowerCase()] && record[record.document_type.toLowerCase()].find(_ => _.legalform_id.toString() === record.legalforms_id.toString()) || {};
          const plaintiffName = filterData && filterData.plaintiff_name || '';
          const defendantName = filterData && filterData.defendant_name || '';
          let user_info;
          const pageLoader = store2.get('pageLoader');
          if(pageLoader) {
            const user = yield call(verifySession);
            if(user) {
              user_info = user;
            }
          } else {
            user_info = yield select(selectUser());
          }
          const result = yield call(generatePOSDocument, Object.assign({}, { ...record }, { plaintiff_name: plaintiffName, defendant_name: defendantName, user_name: user_info.name, signature: record?.signature } ));

          if (result) {            
            const planType = user_info && user_info.plan_type && typeof user_info.plan_type === 'object' && user_info.plan_type['responding'] || false;
            yield put(saveDocumentAction(Object.assign({}, {
              case_id: record.id, 
              document_type: record.document_type, 
              document_generation_type: 'pos', 
              generated_document: result.url, 
              pos_document: result.url,
              generated_document_s3_key: result.s3_key, 
              pos_document_s3_key: result.s3_key, 
              legalforms_id: record.legalforms_id, 
              plan_type: planType,
              serving_attorney_name: record.serving_attorney_name, 
              serving_attorney_street: record.serving_attorney_street, 
              serving_attorney_city: record.serving_attorney_city, 
              serving_attorney_state: record.serving_attorney_state,
              serving_attorney_zip_code: record.serving_attorney_zip_code, 
              serving_attorney_email: record.serving_attorney_email,
              serving_date: record.serving_date,
              pos_signature: record?.signature?.pos_signature
            }), false, false, false, false, false, customObject));
            yield delay(1000);
            yield put(generatePOSDocumentSuccess('Proof of Service Document Generated')); 
            const userLoader = store2.get('userLoader');
            const pageLoader = store2.get('pageLoader'); 
            if(customObject && !userLoader && !pageLoader) {
              const { finalDocumentRef, setOpenNext, openNext } = customObject;
              if(openNext) {
                if(finalDocumentRef?.current && openNext?.openFinalDocument) {
                  finalDocumentRef?.current?.openDocument()
                }
                if(openNext?.openEmailPopup) {
                  openNext?.setEmailPopup(true);
                }
                setOpenNext(false);
              }
            }
          } else {
            yield put(generatePOSDocumentError(DEFAULT_GENERATE_POS_DOCUMENT_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_GENERATE_POS_DOCUMENT_ERROR;
          yield put(generatePOSDocumentError(Err));
        }
      }
    })
  }


  function* copyAllToLawyerResponseSaga() {
    yield takeLatest(COPY_ALL_TO_LAWYER_RESPONSE, function* updater({ record, setCloseEditor }) {
      if (record) {
        try {
          const clientResponsedQuestions = record && record.questions && Array.isArray(record.questions) && record.questions.length > 0 ? record.questions.filter(_ => _.client_response_text && _.client_response_text !== null && _.client_response_text) : [];

          const storeForms = yield select(selectForm());
          const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
            if (key.indexOf('detailForm_') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
              arr.push(storeForms[key])
            }
            return arr;
          }, []).map(_ => _.values);

          let filterClientResponsedQuestions = clientResponsedQuestions.filter(data => !selectedForm.some(_ => data.id === _.id))

          if ((filterClientResponsedQuestions && Array.isArray(filterClientResponsedQuestions) && filterClientResponsedQuestions.length > 0) || (selectedForm && Array.isArray(selectedForm) && selectedForm.length > 0)) {

            if (selectedForm && Array.isArray(selectedForm) && selectedForm.length > 0) {
              let selectFormData = selectedForm;

              selectFormData = selectFormData.map(el => {
                const clientResponse = el.client_response_text.split('\n');
                if (clientResponse && clientResponse.length > 0) {
                  return Object.assign({}, el, {
                    client_response_text: clientResponse.map(elm => {
                      if (elm == '') {
                        return `<p>&nbsp;</p>`
                      } else {
                        return `<p>${elm}</p>`
                      }
                    }).join("")
                  })
                } else {
                  return el;
                }
              });

              yield all(selectFormData.map(_ => put(change(`detailForm_${_.id}`, 'lawyer_response_text', `${_.lawyer_response_text || ''} \n${_.lawyer_response_text ? `<br/>${_.client_response_text}` : `${_.client_response_text}`}`))));
            }

            if (filterClientResponsedQuestions && Array.isArray(filterClientResponsedQuestions) && filterClientResponsedQuestions.length > 0) {
              let filterClientResponsData = filterClientResponsedQuestions;
              filterClientResponsData = filterClientResponsData.map(el => {
                const clientResponse = el.client_response_text.split('\n');
                if (clientResponse && clientResponse.length > 0) {
                  return Object.assign({}, el, {
                    client_response_text: clientResponse.map(elm => {
                      if (elm == '') {
                        return `<p>&nbsp;</p>`
                      } else {
                        return `<p>${elm}</p>`
                      }
                    }).join("")
                  })
                } else {
                  return el;
                }
              });

              yield all(filterClientResponsedQuestions.map(data => put(initialize(`detailForm_${data.id}`, data))));
              yield all(filterClientResponsData.map(_ => put(change(`detailForm_${_.id}`, 'lawyer_response_text', `${_.lawyer_response_text || ''} \n${_.lawyer_response_text ? `<br/>${_.client_response_text}` : `${_.client_response_text}`}`))));
            }

            const getStoreForms = yield select(selectForm());
            const formRecords = getStoreForms && Object.keys(getStoreForms).length > 0 && Object.keys(getStoreForms).reduce((arr, key) => {
              if (key.indexOf('detailForm_') > -1 && getStoreForms[key] && getStoreForms[key].values) {
                arr.push(getStoreForms[key]);
              }
              return arr;
            }, []).map(_ => Object.assign({}, _.values)) || [];

            const records = formRecords || [];
            const recordInStore = yield select(selectRecord(record.case_id));
            const questions = recordInStore && recordInStore['questions'] || {};

            for (let i = 0; i < records.length - 1; i++) {
              yield put(updateRecordSuccess(Object.assign({}, { id: records[i].case_id, questions: questions.map(data => data.id === records[i].id ? Object.assign({}, { ...data }, { lawyer_response_text: `${data.lawyer_response_text || ''} \n${records[i].client_response_text || ''}`, translation: false }) : data) })));
            }

            if (records && Array.isArray(records) && records.length > 0) {
              let loop = 0;
              let currentIndex = 1;
              let totalSize = 20;
              const totalSlice = records.length % totalSize == 0 ? Math.floor(records.length / totalSize) : Math.floor((records.length / totalSize) + 1);

              while (loop < totalSlice) {
                const firstIndex = (currentIndex - 1) * totalSize;
                const lastIndex = firstIndex + totalSize;
                const saveRecords = records.slice(firstIndex, lastIndex);
                const result = yield call(copyAllToLawyerResponse, saveRecords.filter(_ => _.lawyer_response_text).map(r => Object.assign({}, r, { lawyer_response_status: ['NotStarted', 'Draft'].includes(r.lawyer_response_status) ? 'Draft' : 'Final' })));  //Calling copy all lawyer response API
                if (result) {
                  /**
                   * Destroy all lawyer response form
                   */
                  let i = 0;
                  while (i < saveRecords.length) {
                    yield put(destroy(`${'detailForm_'}${saveRecords[i]['id']}`));
                    yield delay(20);
                    i++;
                  }

                  loop++;
                  currentIndex++;
                } else {
                  yield put(copyAllToLawyerResponseError(DEFAULT_COPY_ALL_TO_LAWYER_RESPONSE_ERROR));
                }
              }
            }

            yield put(copyAllToLawyerResponseSuccess('All Client Response Copied.'));
            yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
            yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id })));
          }

        } catch (error) {
          const storeForms = yield select(selectForm());
          const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
            if (key.indexOf('detailForm_') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
              arr.push(storeForms[key])
            }
            return arr;
          }, []).map(_ => _.values);
          /**
            * Api throw an error to destroy all lawyer response form
            */
          let i = 0;
          while (i < selectedForm.length) {
            yield put(destroy(`${'detailForm_'}${selectedForm[i]['id']}`));
            yield delay(20);
            i++;
          }
          yield put(copyAllToLawyerResponseError(DEFAULT_COPY_ALL_TO_LAWYER_RESPONSE_ERROR));
        } finally {
          yield call(setCloseEditor, false)
        }
      }
    })
  }

  function* saveCommentSaga() {
    yield takeLatest(SAVE_COMMENT, function* updater({ record, setMedicalHistory }) {
      if (record) {
        try {
          const storeForms = yield select(selectForm());
          const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
            if (key.indexOf('medicalHistoryForm') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values'] && storeForms[key]['values']['medicalHistory']) {
              arr.push(...storeForms[key]['values']['medicalHistory'])
            }
            return arr;
          }, []) || false;

          if(record.note){
            if(selectedForm) {
              setMedicalHistory(selectedForm.map(r => r.file_name === record.file_name ? Object.assign({}, record, { disabled: !record.note }): Object.assign({}, r, { comment: record.comment, disabled: record.note, note: !record.note })));
              yield put(saveCommentSuccess('Comments Saved Successfully'));
            }
          } else {
            if(selectedForm) {
              setMedicalHistory(selectedForm.map(r => Object.assign({}, r, { note: false, disabled: false })))
              yield put(saveCommentSuccess('Comments Saved Successfully'));
            }
          }
        } catch (error) {
          yield put(saveCommentError('Faild to save comments'));
        }
      }
    })
  }

  function* archiveCaseSaga() {
    yield takeLatest(ARCHIVE_CASE, function* updater({ id, setArchiveLoader }) {
      if (id) {
        try {
          const res = yield call(archiveCase, Object.assign({}, { id: id }))
          if (res && res.status && res.status.toLowerCase() === 'ok') {
            yield put(archiveCaseSuccess('Case archived successfully'));
            yield delay(500);
            yield put(push({ pathname: '/casesCategories/rest/archive/cases', state: history.location.state }));
            return;
          }
          else {
            yield put(archiveCaseError(DEFAULT_ARCHIVE_CASE_ERROR));
          }
        } catch (error) {
          yield put(archiveCaseError(DEFAULT_ARCHIVE_CASE_ERROR));
        } finally {
          if(setArchiveLoader){
            setArchiveLoader(false);
          }          
        }
      }
    })
  }

  function* unArchiveCaseSaga() {
    yield takeLatest(UNARCHIVE_CASE, function* updater({ record, setLoader }) {
      if (record) {
        try {
          const result = yield call(unArchiveCase, record);
          if (result) {
            const user_info = yield select(selectUser());
            const isPropoundPractice = isPropound(user_info, record);
            setLoader(false);

            yield put(unArchiveCaseSuccess('Case unarchived successfully'));
            yield delay(1000);
            yield put(push({ pathname: isPropoundPractice ? `/casesCategories/cases/${record.case_id}/forms` : `/casesCategories/cases/${record.case_id}/form`, state: history.location.state }));
            return;
          } else {
            yield put(unArchiveCaseError(DEFAULT_UNARCHIVE_CASE_ERROR));
          }
        } catch (error) {
          yield put(unArchiveCaseError(DEFAULT_UNARCHIVE_CASE_ERROR));
        } finally {
          setLoader(false);
        }
      }
    })
  }


  function* addConsultationSaga() {
    yield takeLatest(ADD_CONSULTATION, function* updater({ record, setBtnLoader }) {
      if (record) {
        try {
          const result = yield call(addConsultation, record.questions);
          if (result) {
            yield put(addConsultationSuccess(Object.assign({}, { id: record.id, questions: result })));
          } else {
            yield put(addConsultationError(DEFAULT_LOAD_FORM_ERROR));
          }
        } catch (error) {
          yield put(addConsultationError(DEFAULT_LOAD_FORM_ERROR));
        } finally {
          yield call(setBtnLoader, false)
        }
      }
    })
  }


  function* deleteConsultationSaga() {
    yield takeLatest(DELETE_CONSULTATION, function* updater({ record, setFormLoader }) {
      if (record) {
        try {
          const result = yield call(deleteConsultation, record);
          const message = record && record.document_type == 'IDC' ? `Questions deleted Successfully` : `Consultation deleted Successfully`;
          if (result) {
            yield put(deleteConsultationSuccess(message));
            yield delay(2000);
            yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id, client_id: record.client_id || false, party_id: record.party_id || false })));            
          } else {
            yield put(deleteConsultationError(record && record.document_type == 'idc' ? `Questions deleted Successfully` : `Consultation deleted Successfully`));
          }
        } catch (error) {
          yield put(deleteConsultationError(record && record.document_type == 'idc' ? `Questions deleted Successfully` : `Consultation deleted Successfully`));
        } finally {
          yield call(setFormLoader,false)
        }
      }
    })
  }

  function* updateEditedFlagSaga() {
    yield takeLatest(UPDATE_EDITED_FLAG, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(updateEditedFlag, Object.assign({}, { id: record.id, legalforms_id: record.legalforms_id, case_id: record.case_id, practice_id: record.practice_id, document_type: record.document_type }));
          if (result) {
            yield put(updateEditedFlagSuccess('Flag Updated Successfully'));
            yield delay(1000);
            yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id, client_id: record.client_id || false, party_id: record.party_id || false })));
          } else {
            yield put(updateEditedFlagError('Failed to update flag'));
          }
        } catch (error) {
          yield put(updateEditedFlagError('Failed to update flag'));
        }
      }
    })
  }

  function* savePropoundFormSaga() {
    yield takeLatest(SAVE_PROPOUND_FORM, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const save = yield call(savePropoundForm, Object.assign({}, { ...record }, { document_type: record && record.document_type && record.document_type.toUpperCase() }));

          const redirectUrl = `/casesCategories/cases/${record.case_id}/forms/0/propound`;
          if (save) {
            yield put(stopSubmit(form));
            yield put(savePropoundFormSuccess("Propound Form Saved"));
            yield delay(500);
            yield put(destroy(form));
            yield put(push({ pathname: redirectUrl, state: Object.assign({}, { ...history.location.state }, { filename: false }) }));
          } else {
            yield put(savePropoundFormError(DEFAULT_SAVE_PROPOUND_FORM_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_SAVE_PROPOUND_FORM_ERROR }));
          }
        } catch (error) {
          yield put(savePropoundFormError(DEFAULT_SAVE_PROPOUND_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_SAVE_PROPOUND_FORM_ERROR }));
        } finally {
          yield put(reset(form));
        }
      }
    })
  }

  function* loadPropoundRecordSaga() {
    yield takeLatest(LOAD_PROPOUND_RECORD, function* updater({ id }) {
      if (id) {
        try {
          const result = yield call(loadPropoundRecord, id);
          if (result) {
            while (true) { // eslint-disable-line no-constant-condition
              const recordsInStore = yield select(selectRecords());
              if (recordsInStore && recordsInStore.length > 0) {
                break;
              }
              yield delay(500);
            }
            yield put(loadPropoundRecordSuccess(result));
          } else {
            yield put(loadPropoundRecordError(DEFAULT_LOAD_FORM_ERROR));
          }
        } catch (error) {
          yield put(loadPropoundRecordError(DEFAULT_LOAD_FORM_ERROR));
        }
      }
    })
  }

  function* loadPropoundFormSaga() {
    yield takeLatest(LOAD_PROPOUND_FORM, function* updater({ record, setQuestions, setFormLoader }) {
      if (record) {
        try {
          const result = yield call(loadPropoundForm, record);
          if (result) {
            // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
            while (true) { // eslint-disable-line no-constant-condition
              const recordsInStore = yield select(selectRecords());
              if (recordsInStore && recordsInStore.length > 0) {
                break;
              }
              yield delay(1000);
            }
            yield put(loadPropoundFormSuccess(Object.assign({}, { id: record.case_id, questions: result && result.questions })));
            if(setQuestions) {
              yield call(setQuestions, result.questions);
            }
            if(setFormLoader) {
              yield call(setFormLoader, false);
            }

          } else {
            yield put(loadPropoundFormError(DEFAULT_LOAD_FORM_ERROR));
          }
        } catch (error) {
          yield put(loadPropoundFormError(DEFAULT_LOAD_FORM_ERROR));
        }
      }
    })
  }

  function* deletePropoundFormSaga() {
    yield takeLatest(DELETE_PROPOUND_FORM, function* updater({ record, loadPropoundRecord }) {
      if (record) {
        try {
          const result = yield call(deletePropoundForm, record);
          if (result) {
            yield put(deletePropoundFormSuccess("Propound Form Deleted"));
            yield delay(500);
            yield put(loadPropoundRecord(record.case_id));
          } else {
            yield put(deletePropoundFormError(DEFAULT_DELETE_FORM_ERROR));
          }
        } catch (error) {
          yield put(deletePropoundFormError(DEFAULT_DELETE_FORM_ERROR));
        }
      }
    })
  }

  function* updatePropoundFormSaga() {
    yield takeLatest(UPDATE_PROPOUND_FORM, function* updater({ record }) {
      if (record) {
        let { questions = [] } = record;
        let duplicate;
        try {
          questions = typeof questions === 'string' && JSON.parse(questions) || questions;
          if (record.form && record.form === 'questionsForm') {
            duplicate = getDuplicates(questions.map(_ => { return _.question_number }));
          }
          const validate = questions.filter(_ => _.question_text === '' || _.question_number === '' || _.question_number_text === '');

          if (validate && validate.length === 0) {
            if (duplicate && duplicate != '' && record.form && record.form === 'questionsForm') {
              yield put(updatePropoundFormError(`Question Number ${duplicate} duplicated`));
            } else {
              let questionsData = questions;
              questionsData = questionsData && questionsData.length > 0 && questionsData || questions;
              const finalRecord = Object.assign({}, record, { questions: questionsData });
              const result = yield call(updatePropoundForm, finalRecord);
              if (result) {
                yield delay(1000);
                const recordInStore = yield select(selectRecord(record.case_id));
                yield put(updatePropoundFormSuccess(Object.assign({}, { id: record.case_id, questions: recordInStore.questions.map((r) => result.id === r.id ? Object.assign({}, r, result) : Object.assign({}, r)) })));
                yield delay(500);
                
                const redirectUrl = `/casesCategories/cases/${record.case_id}/forms/propound/${record.document_type.toLowerCase()}/${result.id}/details`;
                yield put(push({ pathname: redirectUrl, state: Object.assign({}, { ...history.location.state, legalforms_id: result?.id }) }));
              } else {
                yield put(updatePropoundFormError(DEFAULT_UPDATE_FORM_ERROR));
              }
            }
          } else {
            yield put(updatePropoundFormError(DEFAULT_UPDATE_FORM_ERROR));
          }
        } catch (error) {
          yield put(updatePropoundFormError(DEFAULT_UPDATE_FORM_ERROR));
        }
      }
    })
  }

  function* generatePropoundDocumentSaga() {
    yield takeLatest(GENERATE_PROPOUND_DOCUMENT, function* updater({ record, user_info, case_info, generatedType, signature, setProgress, setOpenDocEditor }) {
      yield put(updateKey(0));
      if (record) {
        try {
          yield put(updateKey(1));
          const modified_template_s3_file_key_doc_type = `modified_template_s3_file_key_${record.document_type.toLowerCase()}`;
          const modified_template_s3_file_key_value = record && record[modified_template_s3_file_key_doc_type] || '';
          const modified_template_status = record && record.modified_template_status || '';
          const custom_template_id = record && record.custom_template_id || '';

          const propoundFormDetails = case_info && case_info['propounding_form_details'][record && record.document_type.toLowerCase()];
          const formData = propoundFormDetails.filter(r => r.id === record.propound_id);

          const caseInfo = Object.assign({}, case_info, { questions: record && record.questions, propounder_serving_attorney_city: formData[0].propounder_serving_attorney_city, propounder_serving_attorney_email: formData[0].propounder_serving_attorney_email, propounder_serving_attorney_name: formData[0].propounder_serving_attorney_name, propounder_serving_attorney_state: formData[0].propounder_serving_attorney_state, propounder_serving_attorney_street: formData[0].propounder_serving_attorney_street, propounder_serving_attorney_zip_code: formData[0].propounder_serving_attorney_zip_code, propounder_serving_date: formData[0].propounder_serving_date });
          const filteredRecord = Object.assign({}, { user_info: user_info, case_info: caseInfo, case_id: record.id, generated_type: generatedType, document_type: record.document_type, signature: signature, lawyers: record.lawyers || [], [modified_template_s3_file_key_doc_type]: modified_template_s3_file_key_value, modified_template_status: modified_template_status, custom_template_id });
          const result = yield call(generateDocument, filteredRecord);

          if (result) {
            yield put(generatePropoundDocumentSuccess(result));
            yield put(updateKey(2));
            yield put(savePropoundFormS3KeyAction(Object.assign({}, record, result), setProgress, setOpenDocEditor));
            const planType = user_info && user_info.plan_type && typeof user_info.plan_type === 'object' && user_info.plan_type || false;
            const propoundingPlanType = planType && planType['propounding'] || false;
            
            yield call(savePropounderQuestions, Object.assign({}, { id: record.propound_id, case_id: record.id, document_type: record.document_type, generated_document_questions: record.questions, client_id: record.client_id, propoundforms_id: record.propound_id, plan_type: propoundingPlanType }));
          } else {
            yield put(generatePropoundDocumentError(DEFAULT_GENERATE_PROPOUND_DOCUMENT_ERROR));
          }
        } catch (error) {
          yield put(generatePropoundDocumentError(DEFAULT_GENERATE_PROPOUND_DOCUMENT_ERROR));
        } finally {
          yield put(destroy('customTemplateAndSignature'));
          yield put(destroy(`propoundStandardForm_${record.id}`));
        }
      }
    })
  }

  function* savePropoundFormS3KeySaga() {
    yield takeLatest(SAVE_PROPOUND_FORM_S3KEY, function* updater({ record, setProgress, setOpenDocEditor }) {
      if (record) {
        try {
          const user_info = yield select(selectUser());
          const practice_id = user_info && user_info.practiceDetails && user_info.practiceDetails.id || false;
          const environment = process.env.ENV === 'production' ? 'production' : 'staging';

          const pdfFileResult = yield call(docToPdfFileRename, Object.assign({}, { propoundforms_id: record.propound_id, s3_file_key: record.s3_key, case_id: record?.id, document_type: record?.document_type }));
          if(pdfFileResult) {
            const submitRecord = JSON.stringify(Object.assign({}, { key: `${process.env.PRIVATE_KEY}`, s3_key: pdfFileResult.s3_file_key, env: environment, practice_id: practice_id }));

            const publicKey = process.env.PUBLIC_KEY;
            const encrytedHash = CryptoJS.AES.encrypt(submitRecord, publicKey).toString();
            const pdf_url = yield call(convertDocumentToPdf, Object.assign({}, { data: encrytedHash, propound_id: record.propound_id, s3_file_key: pdfFileResult.s3_file_key, practice_id: practice_id  }));
  
            if(pdf_url) {
              const result = yield call(savePropoundFormS3Key, Object.assign({}, { pdf_s3_file_key: pdf_url.pdf_s3_key, s3_file_key: record.s3_key, propound_id: record && record.propound_id, doc_filename: record?.doc_filename }));
              if (result) {
                const case_info = yield select(selectRecord(record.case_id));
                const DocTypeArray = record && record.document_type && record.propound_id && case_info && case_info.propounding_form_details && case_info.propounding_form_details[record.document_type.toLowerCase()] && case_info.propounding_form_details[record.document_type.toLowerCase()].map(r => r.id === result.id ? result : r);
                const DocType = record && record.document_type && record.document_type.toLowerCase();
                let propounding_form_details = case_info && case_info.propounding_form_details;
                propounding_form_details = Object.assign({}, propounding_form_details, { [DocType]: DocTypeArray });
                const documentEditedAccessIds = user_info && user_info.show_document_editor_ids && typeof user_info.show_document_editor_ids === 'string' && user_info.show_document_editor_ids.split(',') || [];
                const documentEditedAccess = user_info && user_info.practiceDetails && documentEditedAccessIds && documentEditedAccessIds.includes(user_info.practiceDetails.id) || false;
                if(documentEditedAccess) {
                  yield put(loadPropoundDocumentEditorAction(Object.assign({}, record, { s3_file_key: record.s3_key }), setProgress, setOpenDocEditor));
                } else {
                  yield put(updateKey(3));
                  const propoundGeneratedDoc = result && result.public_url;
                  yield call(download, propoundGeneratedDoc);
                }
                
                yield put(savePropoundFormS3KeySuccess(Object.assign({}, case_info, { propounding_form_details })));
              } else {
                yield put(savePropoundFormS3KeyError(DEFAULT_SAVE_PROPOUND_DOCUMENT_ERROR));
              }
            }
          }          
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SAVE_PROPOUND_DOCUMENT_ERROR;
          yield put(savePropoundFormS3KeyError(Err));
        }
      }
    })
  }

  function* sendPropoundQuestionsSaga() {
    yield takeLatest(SEND_PROPOUND_QUESTIONS, function* updater({ record, form, setDocument, setEmailPopup }) {
      if (record) {
        yield put(startSubmit(form));
        if (setEmailPopup) {
          yield call(setEmailPopup, false);
        }
        try {
          const result = yield call(sendPropoundQuestions, record);
          if (result) {
            yield put(sendPropoundQuestionsSuccess('Email sent successfully'));
            yield put(stopSubmit(form));
          } else {
            yield put(stopSubmit(form, { _error: DEFAULT_SAVE_PROPOUND_FORM_ERROR }));
            yield put(sendPropoundQuestionsError(DEFAULT_SEND_PROPOUND_QUESTIONS_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SEND_PROPOUND_QUESTIONS_ERROR;
          yield put(stopSubmit(form));
          yield put(sendPropoundQuestionsError(Err));
        } finally {
            yield put(destroy(`propoundStandardForm_${record.propounder_case_id}`));
            if (setDocument) {
              yield call(setDocument, false);
            }
        }
      }
    })
  }

  function* savePropounderServingAttorneySaga() {
    yield takeLatest(SAVE_PROPOUNDER_SERVING_ATTORNEY, function* updater({ record, posModal, signatureModal }) {
      if (record) {
        try {
          const result = yield call(savePropounderServingAttorney, record);
          if (result) {
            yield call(posModal, false);            

            const case_info = yield select(selectRecord(record.case_id));
            const DocTypeArray = record && record.document_type && record.propound_id && case_info && case_info.propounding_form_details && case_info.propounding_form_details[record.document_type.toLowerCase()] && case_info.propounding_form_details[record.document_type.toLowerCase()].map(r => r.id === result.id ? Object.assign({}, r, { propounder_serving_attorney_city: result.propounder_serving_attorney_city, propounder_serving_attorney_email: result.propounder_serving_attorney_email, propounder_serving_attorney_name: result.propounder_serving_attorney_name, propounder_serving_attorney_state: result.propounder_serving_attorney_state, propounder_serving_attorney_street: result.propounder_serving_attorney_street, propounder_serving_attorney_zip_code: result.propounder_serving_attorney_zip_code, propounder_serving_date: result.propounder_serving_date }) : r);
            const DocType = record && record.document_type && record.document_type.toLowerCase();
            let propounding_form_details = case_info && case_info.propounding_form_details;
            propounding_form_details = Object.assign({}, propounding_form_details, { [DocType]: DocTypeArray })
                        
            yield put(savePropounderServingAttorneySuccess(Object.assign({}, case_info, { propounding_form_details })));
            yield call(signatureModal);
          } else {
            yield put(savePropounderServingAttorneyError(DEFAULT_SAVE_PROPOUND_DOCUMENT_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SAVE_PROPOUND_DOCUMENT_ERROR;
          yield put(savePropounderServingAttorneyError(Err));
        }
      }
    })
  }

  function* questionTemplateSaga() {
    yield takeLatest(QUESTION_TEMPLATE, function* updater({ record, form, getFormLabel, formColumns }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const user_info = yield select(selectUser());
          let isPropoundPractice = user_info && record && isPropound(user_info, record);
          if(record.template_id) {
            let defaultQuestionsRecord = yield call(defaultQuestionsTemplate, Object.assign({}, { id: record.template_id }));

            if(defaultQuestionsRecord && defaultQuestionsRecord.questions && Array.isArray(defaultQuestionsRecord.questions) && defaultQuestionsRecord.questions.length > 0) {
              const defaultQuestions = defaultQuestionsRecord && defaultQuestionsRecord.questions || [];
              const disc_type = defaultQuestionsRecord && defaultQuestionsRecord.state === "CA" && defaultQuestionsRecord.disc_type == "InitialDisclosure" ? "CAInitialDisclosure" :  defaultQuestionsRecord && defaultQuestionsRecord.disc_type || false;

              const document_type = defaultQuestionsRecord && defaultQuestionsRecord.disc_type && ['CAInitialDisclosure', 'InitialDisclosure'].includes(defaultQuestionsRecord.disc_type) ? "idc" :  'frogs' || false;
              yield put(stopSubmit(form));
              yield put(push({
                pathname: isPropoundPractice ? `/casesCategories/cases/${record.id}/forms/respond/standardForm` : `/casesCategories/cases/${record.id}/standardForm`,
                state: Object.assign({}, { ...history.location.state }, { filename: false, questions: formColumns[disc_type], formLabel: getFormLabel(disc_type), defaultQuestions, tabValue: isPropoundPractice && '1/respond', useSavedTemplate: true, document_type: document_type })
              }));
            }
          }
        } catch (error) {
          yield put(stopSubmit(form));
          yield put(questionsTemplateError('Failed to load question template'));
        } finally {
          yield put(destroy(form));
        }
      }
    })
  }

  function* destroyStandardFormSaga() {
    yield takeLatest(DESTROY_STANDARD_FORM, function* updater({ record, quickCreate, form }) {
      if (record) {
        const user_info = yield select(selectUser());
        const isPropoundPractice = isPropound(user_info, record);
        if(quickCreate) {
          yield put(deleteQuickCreateDocumentAction(record, isPropoundPractice, form));
        } else {
          let pathname = isPropoundPractice ? (history && history.location && history.location.state && history.location.state && history.location.state.tabValue ? `/casesCategories/cases/${record.id}/forms/${history.location.state.tabValue}` : `/casesCategories/cases/${record.id}/forms`) : `/casesCategories/cases/${record.id}/form`;
          yield put(destroy(form));
          history.push({ pathname: pathname, state: Object.assign({}, { ...history.location.state }, { defaultQuestions: [], useSavedTemplate: false, filename: false }) });
        }
      }
    })
  }

  function* createClientCaseSaga() {
    yield takeLatest(CREATE_CLIENT_CASE, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(createClientCase, record);
          if (result) {
            const user_info = yield select(selectUser());
            const isPropoundPractice = isPropound(user_info, record);
            if (record?.case_from === 'mycase') {
              const submitRecord = Object.assign({}, record, { integration_client_id: record?.integration_case_data?.client_id })
              yield call(mycaseCommonUpdate, submitRecord);
            }
            if (record?.case_from === 'filevine') {
              const practiceId = user_info && user_info.practice_id || false;
              const filevineDetails = user_info && user_info.filevineDetails || false;
              const apiKey = filevineDetails && filevineDetails.filevine_key || false;
              const apiSecret = filevineDetails && filevineDetails.filevine_secret || false;
              const apiUrl = filevineDetails && filevineDetails.filevine_baseurl || false;
              setFilevineBaseUrl(apiUrl);

              if (apiKey && apiSecret && apiUrl) {
                // Decryption
                const { decryptApiKey, decryptApiSecret } = decryptApiKeyAndSecret(apiKey, apiSecret, user_info);
                // TimeStamp
                const currentTimestamp = new Date().toISOString();
                // Ordering is important!
                const data = [decryptApiKey, currentTimestamp, decryptApiSecret].join('/');
                // Hash
                let currentHash = CryptoJS.MD5(data).toString('');
                const result = yield call(filevineApiSession, Object.assign({}, { mode: "key", apiKey: `${decryptApiKey}`, apiHash: `${currentHash}`, apiTimestamp: `${currentTimestamp}` }));

                store2.set('filevine', result);
                setFilevineAuthToken(result);

                const encodedAccessToken = base64.encode(result.accessToken);
                const encryptedAccessToken = CryptoJS.AES.encrypt(encodedAccessToken, practiceId).toString();
                const encodedRefreshToken = base64.encode(result.refreshToken);
                const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();
                yield call(storeFilevineSecret, Object.assign({}, { filevine_key: apiKey, filevine_secret: apiSecret, filevine_user_id: result.userId, filevine_org_id: result.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: currentHash, filevine_timeStamp: currentTimestamp, filevine_baseurl : apiUrl}));

                if (result) {
                  const submitRecords = Object.assign({}, record, { access_token: encryptedAccessToken, refresh_token: encryptedRefreshToken, filevine_org_id: result.orgId, filevine_user_id: result.userId, integration_client_id: record?.integration_case_data?.personId?.native, filevine_baseurl : apiUrl });
                  yield call(filevineCommonUpdate, submitRecords);
                }
              }
            }
            if (record?.case_from === 'clio') {
              const submitRecord = Object.assign({}, record, { integration_client_id: record?.integration_case_data?.client_id });
              yield call(clioCommonUpdate, submitRecord);
            }

            yield put(createClientCaseSuccess('Client & Case details updated successfully.'));
            yield delay(500);
            let pathname = isPropoundPractice ? (history && history.location && history.location.state && history.location.state && history.location.state.tabValue ? `/casesCategories/cases/${record.id}/forms/${history.location.state.tabValue}` : `/casesCategories/cases/${record.id}/forms`) : `/casesCategories/cases/${record.id}/form`;
            history.push({ pathname: pathname, state: Object.assign({}, { ...history.location.state }, { defaultQuestions: [], useSavedTemplate: false }) });
            const recordInStore = yield select(selectRecord());
            yield put(casesActions.loadRecord(recordInStore.id));
          }
        } catch (error) {
          yield put(createClientCaseError('Failed to Updated Client & Case'));
          yield put(stopSubmit(form));
        }
      }
    })
  }

  function* updatePlanDetailsSaga() {
    yield takeLatest(UPDATE_PLAN_DETAILS, function* updater({ payment = {}, cardDetails, form, record, openCardDetailForm, dialog, progress, generatedType, posModal, freeTrialDialog, setShowFilevinePopup, setShowMyCasePopup, openTemplateForm, openGeorgiaTemplate, actions, setShowLitifyPopup, setShowClioPopup, customObject }) {

      if (payment) {
        yield put(startSubmit(form));
        try {
          const user_info = yield select(selectUser());
          if (payment && !payment.error && !payment.credit_card_details_available) {
            const { setupIntent = {} } = payment;
            const result = yield call(updatePlanDetails, Object.assign({}, { payment_method: setupIntent.payment_method }));

            if (result) {
              let subscriptionPlan;
              const submitRecord = Object.assign({}, { plan_id: cardDetails.plan_id, page: 'settings', selected_plan: cardDetails.plan_id, plan_category: cardDetails.plan_category, discount_code: cardDetails.discount_code || false });
              if(user_info?.practiceDetails?.billing_type === 'limited_users_billing') {
                subscriptionPlan = yield call(createUserSubscriptions, submitRecord);
              } else {
                subscriptionPlan = yield call(createSubscriptionPlan, submitRecord);
              }

              if (subscriptionPlan) {
                store2.set('pageLoader', true);
                yield put(updatePlanDetailsSuccess('Subscription updated successfully.'));
                yield delay(1000);
                yield put(actions.settingsDiscountCodeSuccess([]));
                if (record && generatedType === 'pos') {                  
                  yield put(generatePOSDocumentAction(record, posModal, customObject));
                } else {
                  yield put(loadBillingDetailsAction(openCardDetailForm, dialog, progress, record, generatedType, false, posModal, freeTrialDialog, false, setShowFilevinePopup, setShowMyCasePopup, false, openTemplateForm, openGeorgiaTemplate, false, setShowLitifyPopup, setShowClioPopup, false, customObject));
                  yield put(stopSubmit(form));
                }
              }
            } else {
              yield put(updatePlanDetailsError(DEFAULT_UPDATE_BILLING_DETAILS_ERROR));
              yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
            }
          } else if(payment && payment.credit_card_details_available) {
            let subscriptionPlan;
            const submitRecord = Object.assign({}, { plan_id: cardDetails.plan_id, page: 'settings', selected_plan: cardDetails.plan_id, plan_category: cardDetails.plan_category, discount_code: cardDetails.discount_code || false });
            if(user_info?.practiceDetails?.billing_type === 'limited_users_billing') {
              subscriptionPlan = yield call(createUserSubscriptions, submitRecord);
            } else {
              subscriptionPlan = yield call(createSubscriptionPlan, submitRecord);
            }
            if (subscriptionPlan) {
              store2.set('pageLoader', true);
              yield put(updatePlanDetailsSuccess('Subscription updated successfully.'));
              yield delay(1000);
              yield put(actions.settingsDiscountCodeSuccess([]));
              if (record && generatedType === 'pos') {
                yield put(generatePOSDocumentAction(record, posModal, true));
              } else {
                yield put(loadBillingDetailsAction(openCardDetailForm, dialog, progress, record, generatedType, false, posModal, freeTrialDialog, false, setShowFilevinePopup, setShowMyCasePopup, false, openTemplateForm, true, false, setShowLitifyPopup, setShowClioPopup, false, customObject));
                yield put(stopSubmit(form));
              }
            } else {
              yield put(updatePlanDetailsError(DEFAULT_UPDATE_BILLING_DETAILS_ERROR));
              yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
            }
          } else {
            const errorMessage = payment && payment.error && payment.error.message && payment.error.message || DEFAULT_UPDATE_BILLING_DETAILS_ERROR
            yield put(updatePlanDetailsError(errorMessage));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_BILLING_DETAILS_ERROR;
          yield put(updatePlanDetailsError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        } finally {
          yield put(reset(form));
        }
      }
    })
  }

  function* sendRespondQuestionsSaga() {
    yield takeLatest(SEND_RESPOND_QUESTIONS, function* updater({ record, form, setDocument, setEmailPopup, setFilevinePopup, setMyCasePopup, setLitifyPopup, setClioPopup }) {
      if (record) {
        yield put(startSubmit(form));
        if (setEmailPopup) {
          yield call(setEmailPopup, false);
        }
        try {
          const result = yield call(sendRespondQuestions, record);
          if (result) {
            const caseFrom = record?.case_from || false;
            const integration = caseFrom && ['mycase', 'filevine', 'litify', 'clio'].includes(caseFrom) || false;
            const userLoader = store2.get('userLoader');
            const pageLoader = store2.get('pageLoader');
            const eServeStatus = yield select(selectEServePopup());

            yield put(sendRespondQuestionsSuccess('Email sent successfully'));
            yield put(stopSubmit(form));

            if (eServeStatus) {
              if (!integration && (pageLoader || userLoader)) {
                const user = yield call(verifySession);
                if (user)
                  yield put(verifySessionSuccess(user));
                store2.remove('pageLoader');
                store2.remove('userLoader');
              } else if (integration) {
                yield delay(2000);
                if (caseFrom && caseFrom === 'filevine') {
                  if (setFilevinePopup) yield call(setFilevinePopup, true);
                } else if (caseFrom && caseFrom === 'mycase') {
                  if (setMyCasePopup) yield call(setMyCasePopup, true);
                } else if (caseFrom && caseFrom === 'litify') {
                  if (setLitifyPopup) yield call(setLitifyPopup, true);
                } else if (caseFrom && caseFrom === 'clio') {
                  if (setClioPopup) yield call(setClioPopup, true);
                }
              }
              yield put(eServePopupStatus(false));
            }
          } else {
            yield put(stopSubmit(form, { _error: DEFAULT_SEND_RESPOND_QUESTIONS_ERROR }));
            yield put(sendRespondQuestionsError(DEFAULT_SEND_RESPOND_QUESTIONS_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_SEND_PROPOUND_QUESTIONS_ERROR;
          yield put(stopSubmit(form));
          yield put(sendRespondQuestionsError(Err));
        } finally {
          if (setDocument) {
            yield call(setDocument, false);
          }
        }
      }
    })
  }

  function* quickCreateSaveStandardFormSaga() {
    yield takeLatest(QUICK_CREATE_SAVE_STANDARD_FORM, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateLegalForm, record);
          if (result) {
            const user_info = yield select(selectUser());
            let isPropoundPage = user_info && record && isPropound(user_info, record);
            yield put(stopSubmit(form));
            yield put(quickCreateSaveStandardFormSuccess("Form Saved"));
            yield put(quickCreateRedirectPathAction(record, isPropoundPage));
            yield delay(500);
            yield put(destroy(form));
          } else {
            yield put(quickCreateSaveStandardFormError(DEFAULT_SAVE_STANDARD_FORM_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_SAVE_STANDARD_FORM_ERROR }));
          }
        } catch (error) {
          yield put(quickCreateSaveStandardFormError(DEFAULT_SAVE_STANDARD_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_SAVE_STANDARD_FORM_ERROR }));
        }
      }
    })
  }

  function* quickCreateSaveQuestionsFormSaga() {
    yield takeLatest(QUICK_CREATE_SAVE_QUESTIONS_FORM, function* updater({ record }) {
      if (record) {

        let { questions = [] } = record;
        const name = history && history.location.pathname;
        let duplicate;
        try {
          questions = typeof questions === 'string' && JSON.parse(questions) || questions;
          if (record.form && record.form === 'questionsForm') {
            duplicate = getDuplicates(questions.map(_ => { return _.question_number }));
          }
          const validate = questions.filter(_ => _.question_text === '' || _.question_number === '' || _.question_number_text === '');

          if (validate && validate.length === 0) {
            if (duplicate && duplicate != '' && record.form && record.form === 'questionsForm') {
              yield put(quickCreateSaveQuestionsFormError(`Question Number ${duplicate} duplicated`));
            }
            else {
              const result = yield call(updateLegalForm, record);
              if (result) {
                yield put(quickCreateSaveQuestionsFormSuccess("Questions Saved"));
                const user_info = yield select(selectUser());
                let isPropoundPage = user_info && record && isPropound(user_info, record);
                yield put(quickCreateRedirectPathAction(record, isPropoundPage));
              } else {
                yield put(quickCreateSaveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
              }
            }
          } else {
            yield put(quickCreateSaveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_VALIDATE_ERROR));
          }

        } catch (error) {
          yield put(quickCreateSaveQuestionsFormError(DEFAULT_SAVE_QUESTIONS_FORM_ERROR));
        }
      }
    })
  }
  
  function* deleteQuickCreateDocumentSaga() {
    yield takeLatest(DELETE_QUICK_CREATE_DOCUMENT, function* updater({ record, isPropoundPage, form }) {
      if (record) {
        try {
          yield call(deleteQuickCreateDocument, record.id);
          yield put(quickCreateRedirectPathAction(record, isPropoundPage, form));
        } catch (error) {
          yield put(deleteQuickCreateDocumentError('Failed to delete the Document details'));
        }
      }
    })
  }

  function* updateUserRecordSaga() {
    yield takeLatest(UPDATE_USER_RECORD, function* updater({ record, userRecord, form, customObject }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateUserRecord, userRecord);
          if (result) {
            store2.set('userLoader', true);
            const { form, dialog, setProgress, submitRecords, generatedType, posModal, freeTrialDialog, setShowFilevinePopup, setShowMyCasePopup, openTemplateForm, subscriptionAlert, openGeorgiaTemplate, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor } = record;
            yield put(loadBillingDetailsAction(form, dialog, setProgress, submitRecords, generatedType, false, posModal, freeTrialDialog, false, setShowFilevinePopup, setShowMyCasePopup, subscriptionAlert, openTemplateForm, openGeorgiaTemplate, false, setShowLitifyPopup, setShowClioPopup, setOpenDocEditor, customObject));
            yield put(stopSubmit(form));
          } else {
            yield put(stopSubmit(form, { _error: 'Failed to Update User Record' }));
            yield put(updateUserRecordError('Failed to Update User Record'));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to Update User Record';
          yield put(stopSubmit(form));
          yield put(updateUserRecordError(Err));
        }
      }
    })
  }

  function* quickCreateRedirectPathSaga() {
    yield takeLatest(QUICK_CREATE_REDIRECT_PATH, function* updater({ record, isPropoundPage, form }) {
      if(record) {
        const user_info = yield select(selectUser());
        const document_type = record && record.document_type;
        let pathname = isRespondEntityUrl(user_info, Object.assign({}, record, { value: document_type && document_type.toLowerCase() }), 'details');
        const result = yield call(loadForm, Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id }));
        if(result?.questions) {
          const questions = result?.questions;
          const firstQuestion = questions && questions[0];
          yield put(initialize(`detailForm_${firstQuestion.id}`, firstQuestion));
        }
        yield put(clearIndividualStoreRecord());
        if(form) {
          yield put(destroy(form));
        }
        yield put(deleteQuickCreateDocumentSuccess());
        yield put(push({ pathname: pathname || `/casesCategories/cases/${record.case_id}/form`, state: Object.assign({}, { ...history.location.state }, { legalforms_id: record.legalforms_id, tabValue: isPropoundPage ? '1/respond' : false, filename: false }, document_type) }));
      }
    })
  }

  function* updateVerifySession() {
    yield takeLatest(UPDATE_VERIFY_SESSION, function* updater({ status }) {
      if (status) {
        try {
          const user = yield call(verifySession);
          if (user)
            yield put(verifySessionSuccess(user));
        }
        catch (error) {
          console.log('error', error);
        }
      }
    })
  }

  function* inValidSubscriptionsSaga() {
    yield takeLatest(INVALID_SUBSCRIPTIONS, function* updater({ load, setOpenAlertDialog, metaData, partyIds, open, setSendQuestions, setDocument, setLoader }) {
      if (load) {
        try {
          const user = yield call(verifySession);
          const freeTierCount = user && user.free_tier_count || false;
          if (user && freeTierCount == 0) {
            yield call(setOpenAlertDialog, true);
            yield call(setLoader, false);
          } else {
            if (metaData && metaData['parties'] && metaData['parties'].length > 0 && !partyIds) {
              open();
            } else {
              setSendQuestions(true);
              setDocument(Object.assign({}, { type: 'SendQuestions' }));
            }
            yield call(setLoader, false);
          }
        } catch (error) {
          console.log('error', error);
        }
      }
    })
  }

  function* restoreResponseHistorySaga() {
    yield takeLatest(RESTORE_RESPONSE_HISTORY, function* updater({ record, formRecord, setShowHistoryForm, setOpenForm, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const lawyer_response_text = record?.response;
          const formValue = Object.assign({}, { ...formRecord }, { lawyer_response_text });
          const storeForms = yield select(selectForm());

          

          const result = yield call(updateForm, formValue);
          if (result) {
            const selectedForms = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
              if (key === `detailForm_${result.id}` && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
                arr.push(storeForms[key])
              }
              return arr;
            }, []).map(_ => _.values) || [];
            const recordInStore = yield select(selectRecord(formRecord.case_id));
            yield put(updateFormSuccess(Object.assign({}, { id: formRecord.case_id, questions: recordInStore.questions.map((r) => result.id === r.id ? Object.assign({}, r, result) : Object.assign({}, r)) })));
            yield put(delay(500));
            yield put(restoreResponseHistorySuccess('Response successfully restored.'));
            yield put(stopSubmit(form));
            yield call(setShowHistoryForm, false);
            yield put(destroy(form));

            if(setOpenForm && selectedForms?.length > 0) {
              yield call(setOpenForm, false)
              yield put(destroy(`detailForm_${result.id}`));
            }
          } 
        } catch (error) {
          yield put(restoreResponseHistoryError('Failed to restore attoney response'));
        }
      }
    })
  }


  function* attorneyResponseTrackingSaga() {
    yield takeLatest(ATTORNEY_RESPONSE_TRACKING, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(updateRecord, record);
          if (result) {
            yield put(attorneyResponseTrackingSuccess(result, 'Attorney Response Tracking Status Updated'));
          } else {
            yield put(attorneyResponseTrackingError(DEFAULT_ATTORNEY_RESPONSE_TRACKING_ERROR));
          }
        } catch (error) {
          yield put(attorneyResponseTrackingError(DEFAULT_ATTORNEY_RESPONSE_TRACKING_ERROR));
        }
      }
    })
  }

  function* filevineSessionCommonUpdateSaga() {
    yield takeLatest(FILEVINE_SESSION_COMMON_UPDATE, function* updater({ record, form, formRecord }) {
      let apiKey = record && record.filevine_key || false;
      let apiSecret = record && record.filevine_secret || false;
      let apiUrl = record && record.filevine_api || false;    
      if (record && apiKey && apiSecret && apiUrl) {
        setFilevineBaseUrl(apiUrl);
        yield put(startSubmit(form));
        try {
          const store_secret = record.is_store_filevine_secret || false;
          const user_info = yield select(selectUser());
          const isPropoundPractice = isPropound(user_info, record);
          const practiceId = user_info && user_info.practice_id;
          const timestamp = new Date().toISOString();

          // Ordering is important!
          const data = [apiKey, timestamp, apiSecret].join('/');

          let hash = CryptoJS.MD5(data).toString('');

          const result = yield call(filevineApiSession, Object.assign({}, { mode: "key", apiKey: `${apiKey}`, apiHash: `${hash}`, apiTimestamp: `${timestamp}` }));

          if (result) {
            const apiKeyEncoded = base64.encode(apiKey);
            const encryptApiKey = CryptoJS.AES.encrypt(apiKeyEncoded, practiceId).toString();
            const apiSecretEncoded = base64.encode(apiSecret);
            const encryptSecret = CryptoJS.AES.encrypt(apiSecretEncoded, practiceId).toString();

            const encodedRefreshToken = base64.encode(result.refreshToken);
            const encryptedRefreshToken = CryptoJS.AES.encrypt(encodedRefreshToken, practiceId).toString();

            const encodedAccessToken = base64.encode(result.accessToken);
            const encryptedAccessToken = CryptoJS.AES.encrypt(encodedAccessToken, practiceId).toString();

            store2.set('filevine', result);
            setFilevineAuthToken(result);

            if (store_secret) {
              yield call(storeFilevineSecret, Object.assign({}, { filevine_key: encryptApiKey, filevine_secret: encryptSecret, filevine_user_id: result.userId, filevine_org_id: result.orgId, filevine_refresh_token: encryptedRefreshToken, filevine_hash: hash, filevine_timeStamp: timestamp, filevine_baseurl : apiUrl}));
            }
            yield put(filevineSessionCommonUpdateSuccess('Filevine session created successfully'));

            const response = yield call(createClientCase, formRecord);
            yield delay(1000);
            if (response) {
              const submitRecords = Object.assign({}, formRecord, { access_token: encryptedAccessToken, refresh_token: encryptedRefreshToken, filevine_org_id: result.orgId, filevine_user_id: result.userId, integration_client_id: formRecord?.integration_case_data?.personId?.native, filevine_baseurl : apiUrl });
              yield call(filevineCommonUpdate, submitRecords);
              yield delay(500);

              let pathname = isPropoundPractice ? (history && history.location && history.location.state && history.location.state && history.location.state.tabValue ? `/casesCategories/cases/${formRecord.id}/forms/${history.location.state.tabValue}` : `/casesCategories/cases/${formRecord.id}/forms`) : `/casesCategories/cases/${formRecord.id}/form`;
              history.push({ pathname: pathname, state: Object.assign({}, { ...history.location.state }, { defaultQuestions: [], useSavedTemplate: false }) });
              const recordInStore = yield select(selectRecord());
              yield put(casesActions.loadRecord(recordInStore.id));
            }
            yield put(stopSubmit(form));
            yield put(destroy(form));
          } else {
            yield put(filevineSessionCommonUpdateError(DEFAULT_FILEVINE_SESSION_ERROR));
            yield put(stopSubmit(form));
          }

        } catch (error) {
          yield put(filevineSessionCommonUpdateError('Failed to Updated Client & Case'));
          yield put(stopSubmit(form));
        } 
      }
    })
  }

  function* litifyUploadDocumentSaga() {
    yield takeLatest(LITIFY_UPLOAD_DOCUMENT, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(litifyUploadDocument, record);
          if (result && result.status && result.status.toLowerCase() === 'ok') {
            yield put(litifyUploadDocumentSuccess('Document Uploaded Successfully'));
          } else {
            yield put(litifyUploadDocumentError('Failed to Upload Document'));
          }
        } catch (error) {
          yield put(litifyUploadDocumentError('Failed to Upload Document'));
        }
      }
    })
  }

  function* createLitifyBulkRecordsSaga() {
    yield takeLatest(CREATE_LITIFY_BULK_RECORDS, function* updater({ records, form, setLitifyPopup, setShowUserForm }) {
      if (records) {
        yield put(startSubmit(form));
        try {
          const user_info = yield call(verifySession);
          if (user_info && (user_info?.role == 'lawyer' && !user_info.state_bar_number || !user_info.user_name) && setShowUserForm) {
            yield call(setShowUserForm, true);
            yield put(stopSubmit(form));
          } else {
            try {
              if (records && Array.isArray(records) && records.length > 0) {
                let loop = 0;
                let currentIndex = 1;
                let totalSize = 10;
                const totalSlice = records.length % totalSize == 0 ? Math.floor(records.length / totalSize) : Math.floor((records.length / totalSize) + 1);

                while (loop < totalSlice) {
                  const firstIndex = (currentIndex - 1) * totalSize;
                  const lastIndex = firstIndex + totalSize;
                  const saveRecords = records.slice(firstIndex, lastIndex);
                  const result = yield call(createLitifyBulkRecords, saveRecords);

                  if (result) {
                    loop++;
                    currentIndex++;
                  } else {
                    yield put(createLitifyBulkRecordsError('Failed to save records'));
                  }
                }
                yield put(stopSubmit(form));
                yield put(createLitifyBulkRecordsSuccess('Litify data sync successfully'));
                yield delay(1000);
                yield put(push({ pathname: '/casesCategories/cases', state: Object.assign({}, { ...history.location.state }, { integrationModal: false }) }));
                yield put(casesActions.loadRecords(true));
                yield put(getLitifyBulkRecordsSuccess([]));
                const secret = store2.get('secret');
                yield put(verifySessionAction(secret));
              }
            } catch (error) {
              yield put(createLitifyBulkRecordsError('Failed to save records'));
              yield put(stopSubmit(form));
            } finally {
              if (setLitifyPopup) {
                yield call(setLitifyPopup, false);
              }
              yield put(destroy(form));
            }
          }
        } catch (error) {
          yield put(createLitifyBulkRecordsError('Failed to save records'));
          yield put(stopSubmit(form));
        }
      }
    })
  }

  function* litifyUpdateUserDetailsSaga() {
    yield takeLatest(LITIFY_UPDATE_USER_DETAILS, function* updater({ record, form, integration }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(updateUserRecord, record);
          if (result) {
            const { integrationPopup, integration_form, setShowUserForm } = integration;
            yield put(stopSubmit(form));
            yield call(setShowUserForm, false);
            const storeForms = yield select(selectForm());
            const selectFormRecord = storeForms && storeForms[`${integration_form}`] && storeForms[`${integration_form}`].values && storeForms[`${integration_form}`]['values']['integrations'];
            if(selectFormRecord?.length > 0) {
                yield put(createLitifyBulkRecordsAction(selectFormRecord, integration_form, integrationPopup));
            }
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to Update User Record';
          yield put(stopSubmit(form));
          yield put(litifyUpdateUserDetailsError(Err));
        }
      }
    })
  }

  function* plaintiffDefendantSwitchSaga() {
    yield takeLatest(PLAINTIFF_DEFENDANT_SWITCH, function* updater({ record }) {
      if(record){
        yield put(change(`editRecordForm_${record.id}`, 'case_plaintiff_name', record.case_defendant_name));
        yield put(change(`editRecordForm_${record.id}`, 'case_defendant_name', record.case_plaintiff_name));
      }
    })
  }

  function* initalSubscriptionSaga() {
    yield takeLatest(INITIAL_SUBSCRIPTION, function* updater({ payment = {}, cardDetails, form, actions, closeModal }) {
      if (payment) {
        yield put(startSubmit(form));
        try {
          const user_info = yield select(selectUser());
          const userBasedBilling = user_info?.practiceDetails?.billing_type === 'limited_users_billing' ? true : false;
          if (payment && !payment.error && !payment.credit_card_details_available) {
            const { setupIntent = {} } = payment;
            const result = yield call(updatePlanDetails, Object.assign({}, { payment_method: setupIntent.payment_method }));
            const secret = store2.get('secret');
            if (result) {
              let subscriptionPlan;
              const record = Object.assign({}, { plan_id: cardDetails.plan_id, page: 'settings', selected_plan: cardDetails.plan_id, plan_category: cardDetails.plan_category, discount_code: cardDetails.discount_code || false });
              if(userBasedBilling) {
                subscriptionPlan = yield call(createUserSubscriptions, record);
              } else {
                subscriptionPlan = yield call(createSubscriptionPlan, record);
              }

              if (subscriptionPlan) {
                yield put(actions.settingsDiscountCodeSuccess([]));
                yield put(initialSubscriptionSuccess('Subscription updated successfully.'));
                yield put(stopSubmit(form));
                yield call(closeModal, false);
                yield put(delay(2000));
                yield put(loadBillingDetailsAction(false, false, false, { document_type: 'FROGS' }, 'template'));
                if(secret) {
                  yield put(verifySessionAction(secret));
                }
              }
            }
          } else {
            const errorMessage = payment && payment.error && payment.error.message && payment.error.message || DEFAULT_UPDATE_BILLING_DETAILS_ERROR
            yield put(initialSubscriptionError(errorMessage));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_BILLING_DETAILS_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_BILLING_DETAILS_ERROR;
          yield put(initialSubscriptionError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        } finally {
          yield put(reset(form));
        }
      }
    })
  }

  function* createLitifyClientCaseSaga() {
    yield takeLatest(CREATE_LITIFY_CLIENT_CASE, function* updater({ record, form }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const submitRecord = Object.assign({}, record, { integration_client_id: record?.integration_case_data?.litify_pm__Client__c })
          const res = yield call(litifyCommonUpdate, submitRecord);
          if (res) {
            const user_info = yield select(selectUser());
            const isPropoundPractice = isPropound(user_info, record);
            const result = yield call(createClientCase, record);

            if (result) {
              yield put(createLitifyClientCaseSuccess('Client & Case details updated successfully.'));
              yield delay(500);
              let pathname = isPropoundPractice ? (history && history.location && history.location.state && history.location.state && history.location.state.tabValue ? `/casesCategories/cases/${record.id}/forms/${history.location.state.tabValue}` : `/casesCategories/cases/${record.id}/forms`) : `/casesCategories/cases/${record.id}/form`;
              history.push({ pathname: pathname, state: Object.assign({}, { ...history.location.state }, { defaultQuestions: [], useSavedTemplate: false }) });
              const recordInStore = yield select(selectRecord());
              yield put(casesActions.loadRecord(recordInStore.id));
            } else {
              yield put(createLitifyClientCaseError('Failed to Updated Client & Case'));
              yield put(stopSubmit(form));
            }
          }
        } catch (error) {
          const errorMsg = error.response && error.response.data && error.response.data.error;
          if (errorMsg && typeof errorMsg == 'object') {
            yield put(stopSubmit(form));
            yield put(litifyInfoDialogAction(errorMsg));
          } else {
            yield put(createLitifyClientCaseError('Failed to Updated Client & Case'));
            yield put(stopSubmit(form));
          }
        }
      }
    })
  }

  function* loadDocumentEditorSaga() {
    yield takeLatest(LOAD_DOCUMENT_EDITOR, function* updater({ record, setProgress, setDocEditor, setLoader }) {
      if (record) {
        try {
          const { discovery_s3_file_key, document_type, legalform_id } = record;
          const s3KeyRecord = Object.assign({}, { s3_file_key: discovery_s3_file_key });
          const result = yield call(importSfdtFromUrl, s3KeyRecord);
          if (result) {
            const storeRecord = yield select(selectRecord());
            let modifiedRecord = storeRecord && storeRecord[document_type]?.length > 0 && storeRecord[document_type].map((el) => {
              if (el.legalform_id == legalform_id) {
                return Object.assign({}, el, { sfdt_s3key: result?.sfdt_s3key })
              } else {
                return el;
              }
            });
            yield put(loadDocumentEditorSuccess(Object.assign({}, storeRecord, { [document_type]: modifiedRecord })));

            const response = yield call(saveSfdtText, Object.assign({}, { legalforms_id: record.legalform_id, sfdt_s3key: result?.sfdt_s3key }));

            if (response) {
              yield put(fetchDiscoveryHistory(record.legalform_id, false, false, false));
              yield delay(3000);
              yield put(updateKey(3));
              if (setLoader) {
                yield put(documentEditorStatusAction(legalform_id));
                yield call(setLoader, false);
              }
              if (setDocEditor) {
                yield call(setDocEditor, true);
              }
              if (setProgress) {
                yield call(setProgress, false);
              }
            }
          }
          else {
            yield put(loadDocumentEditorError('Something went wrong'));
          }
        }
        catch (error) {
          yield put(loadDocumentEditorError('Something went wrong'));
        }
      }
    })
  }

  function* saveDocumentEditorSaga() {
    yield takeLatest(SAVE_DOCUMENT_EDITOR, function* updater({ record, myFile, setDocLoader, setDocEditor, optNode, caseRecord, setEmailPopup, setFilevinePopup, setMyCasePopup, setLitifyPopup, setClioPopup, customObject }) {
      if (record) {
        try {
          const user_info = yield select(selectUser());
          const { discovery_s3_file_key, document_type, legalform_id } = record;

          const result = yield call(saveDocumentEditor, Object.assign({}, { s3_file_key: discovery_s3_file_key, legalform_id: legalform_id, document_type: document_type }));

          if (result) {
            yield call(uploadFile, result.doc_upload_url, myFile, myFile.type);
            const s3KeyRecord = Object.assign({}, { s3_file_key: discovery_s3_file_key });
            const sfdtFormat = yield call(importSfdtFromUrl, s3KeyRecord);

            if (sfdtFormat) {
              const saveSfdt = yield call(saveSfdtText, Object.assign({}, { legalforms_id: legalform_id }, { sfdt_s3key: sfdtFormat.sfdt_s3key }));
              if (saveSfdt) {
                const caseFrom = caseRecord?.case_from || false;
                const integration = caseFrom && ['mycase', 'filevine', 'litify', 'clio'].includes(caseFrom) || false;
                const userLoader = store2.get('userLoader');
                const pageLoader = store2.get('pageLoader');

                let pdfS3key;
                if(stateListPOS(caseRecord?.state)){
                  const pdfFileResult = yield call(docToPdfFileRename, Object.assign({}, { legalform_id: legalform_id, s3_file_key: discovery_s3_file_key, case_id: record?.case_id, document_type: document_type }));

                  const practice_id = user_info && user_info.practiceDetails && user_info.practiceDetails.id || false;
                  const environment = process.env.ENV === 'production' ? 'production' : 'staging';
                  const publicKey = process.env.PUBLIC_KEY;
                  const submitRecord = JSON.stringify(Object.assign({}, { key: `${process.env.PRIVATE_KEY}`, s3_key: pdfFileResult?.s3_file_key, env: environment, practice_id: practice_id }));
                  const encrytedHash = CryptoJS.AES.encrypt(submitRecord, publicKey).toString();

                  if(pdfFileResult) {
                    const pdf_url = yield call(convertDocumentToPdf, Object.assign({}, { data: encrytedHash, legalforms_id: legalform_id, s3_file_key: pdfFileResult?.s3_file_key, practice_id: practice_id }));
                    if(pdf_url) pdfS3key = pdf_url.pdf_s3_key;
                  }
                }       
                yield call(savePdfS3Key, Object.assign({}, { id: legalform_id, s3_file_key: discovery_s3_file_key, pdf_s3_file_key: pdfS3key }));
                const storeRecord = yield select(selectRecord());
                let modifiedRecord = storeRecord && storeRecord[document_type]?.length > 0 && storeRecord[document_type].map((el) => {
                  if (el.legalform_id == legalform_id) {
                    return Object.assign({}, el, { sfdt_s3key: sfdtFormat.sfdt_s3key })
                  } else {
                    return el;
                  }
                });
                if (optNode) {
                  yield put(saveDocumentEditorSuccess(Object.assign({}, storeRecord, { [document_type]: modifiedRecord }), 'Discovery document saved successfully!'));
                  const documentStatus = yield select(selectDocumentStatus());

                  if (documentStatus) {
                    yield put(documentEditorStatusAction(false));
                  }

                } else {
                  yield put(saveDocumentEditorSuccess(Object.assign({}, storeRecord, { [document_type]: modifiedRecord }), false));
                }
                yield call(download, result.doc_download_url);
                if (setDocEditor) {
                  yield call(setDocEditor, false);
                }

                if (result.doc_download_url && record.posDocument) {
                  if (setEmailPopup) {
                    yield call(setEmailPopup, true);
                    yield put(eServePopupStatus(true));
                  }
                } else if (!record.posDocument && !integration && (pageLoader || userLoader)) {
                  const user = yield call(verifySession);
                  if (user)
                    yield put(verifySessionSuccess(user));
                  store2.remove('pageLoader');
                  store2.remove('userLoader');
                } else if (integration && result.doc_download_url && !record.posDocument) {
                  if (customObject) {
                    const { posDocumentRef, setOpenNext } = customObject;
                    if (posDocumentRef) {
                      posDocumentRef.current.openDialog();
                      setOpenNext({ openEmailPopup: true, setEmailPopup })
                    }
                  }
                  yield delay(2000);
                  if (caseFrom && caseFrom === 'filevine') {
                    if (setFilevinePopup) yield call(setFilevinePopup, true);
                  } else if (caseFrom && caseFrom === 'mycase') {
                    if (setMyCasePopup) yield call(setMyCasePopup, true);
                  } else if (caseFrom && caseFrom === 'litify') {
                    if (setLitifyPopup) yield call(setLitifyPopup, true);
                  } else if (caseFrom && caseFrom === 'clio') {
                    if (setClioPopup) yield call(setClioPopup, true);
                  }
                }
                else if (result.doc_download_url && !record.posDocument) {
                  if (customObject) {
                    const { posDocumentRef, setOpenNext } = customObject;
                    if (posDocumentRef) {
                      posDocumentRef.current.openDialog();
                      setOpenNext({ openEmailPopup: true, setEmailPopup });
                    }
                  }
                }

              }
            }
          } else {
            yield put(saveDocumentEditorError('Failed to save the final document'));
          }
        }
        catch (error) {
          yield put(saveDocumentEditorError('Failed to save the final document'));
        }
        finally {
          yield call(setDocLoader, false);
        }
      }
    })
  }

  function* loadPropoundDocumentEditorSaga() {
    yield takeLatest(LOAD_PROPOUND_DOCUMENT_EDITOR, function* updater({ record, setProgress, setDocEditor, setLoader }) {
      if (record) {
        try {
          const { s3_file_key, propound_id, document_type } = record;
          let docType = document_type && document_type.toLowerCase();
          const s3KeyRecord = Object.assign({}, { s3_file_key });
          const result = yield call(importSfdtFromUrl, s3KeyRecord);
          if (result) {
            let storeRecord = yield select(selectRecord());
            let modifiedRecord = storeRecord && storeRecord.propounding_form_details[docType]?.length > 0 && storeRecord.propounding_form_details[docType].map((el) => {
              return Object.assign({}, el, { sfdt_s3key: result.sfdt_s3key });
            }) || [];
            storeRecord.propounding_form_details[docType] = modifiedRecord;
            yield put(loadPropoundDocumentEditorSuccess(Object.assign({}, storeRecord)));
            const response = yield call(saveSfdtText, Object.assign({}, { propoundforms_id: propound_id, sfdt_s3key: result.sfdt_s3key }));
            if (response) {
              yield put(updateKey(3));
              if (setLoader) {
                yield put(documentEditorStatusAction(propound_id));
                yield call(setLoader, false);
              }
              if (setDocEditor) {
                yield call(setDocEditor, true);
              }
              if (setProgress) {
                yield call(setProgress, false);
              }
            }
          }
          else {
            yield put(loadPropoundDocumentEditorError('Something went wrong'));
          }
        }
        catch (error) {
          yield put(loadPropoundDocumentEditorError('Something went wrong'));
        }
      }
    })
  }

  function* savePropoundDocumentEditorSaga() {
    yield takeLatest(SAVE_PROPOUND_DOCUMENT_EDITOR, function* updater({ editorObj }) {
      const { record, myFile, setDocLoader, setDocEditor, optNode, setEmailPopup } = editorObj;
      if (record) {
        try {
          const user_info = yield select(selectUser());
          const { s3_file_key, document_type, id } = record;
          let docType = document_type && document_type.toLowerCase();
          //generate upload url
          const result = yield call(saveDocumentEditor, Object.assign({}, { s3_file_key: s3_file_key, propoundforms_id: id, document_type: document_type }));

          if (result) {
            //file upload in bucket
            const res = yield call(uploadDocument, result.doc_upload_url, myFile, myFile.type);

            yield delay(5000);
            if (res) {
              const s3KeyRecord = Object.assign({}, { s3_file_key: s3_file_key });
              const sfdtFormat = yield call(importSfdtFromUrl, s3KeyRecord);

              if (sfdtFormat) {
                //save sfdt
                const saveSfdt = yield call(saveSfdtText, Object.assign({}, { propoundforms_id: id }, { sfdt_s3key: sfdtFormat.sfdt_s3key }));
                if (saveSfdt) {
                  const practice_id = user_info && user_info.practiceDetails && user_info.practiceDetails.id || false;
                  const environment = process.env.ENV === 'production' ? 'production' : 'staging';
                  const publicKey = process.env.PUBLIC_KEY;

                  const pdfFileResult = yield call(docToPdfFileRename, Object.assign({}, { propoundforms_id: id, s3_file_key: s3_file_key, case_id: record?.case_id, document_type : document_type }));
                  if(pdfFileResult) {
                    const submitRecord = JSON.stringify(Object.assign({}, { key: `${process.env.PRIVATE_KEY}`, s3_key: pdfFileResult?.s3_file_key, env: environment, practice_id: practice_id }));
                    const encrytedHash = CryptoJS.AES.encrypt(submitRecord, publicKey).toString();
    
                    //generate pdf
                    const pdf_url = yield call(convertDocumentToPdf, Object.assign({}, { data: encrytedHash, propound_id: id, s3_file_key: pdfFileResult?.s3_file_key, practice_id: practice_id }));
    
                    if (pdf_url) {
  
                      //save pdf s3key
                      yield call(savePropoundFormS3Key, Object.assign({}, { pdf_s3_file_key: pdf_url.pdf_s3_key, s3_file_key: s3_file_key, propound_id: id }));
    
                      let storeRecord = yield select(selectRecord());
                      let modifiedRecord = storeRecord && storeRecord.propounding_form_details[docType]?.length > 0 && storeRecord.propounding_form_details[docType].map((el) => {
                        return Object.assign({}, el, { sfdt_s3key: sfdtFormat.sfdt_s3key });
                      }) || {};
                      storeRecord.propounding_form_details[docType] = modifiedRecord;
    
                      if (optNode) {
                        yield put(savePropoundDocumentEditorSuccess(storeRecord, 'Propound document saved successfully!'));
                        const documentStatus = yield select(selectDocumentStatus());
                        if (documentStatus) {
                          yield put(documentEditorStatusAction(false));
                        }
                        if(setEmailPopup){
                          yield call(setEmailPopup, true);
                        }
                      } else {
                        yield put(savePropoundDocumentEditorSuccess(storeRecord, false));
                      }
                      yield call(download, result.doc_download_url);
                      if (setDocEditor) {
                        yield call(setDocEditor, false);
                      }
                    }
                  }
                }
              }
            }
          } else {
            yield put(savePropoundDocumentEditorError('Failed to save the propound document'));
          }
        } catch (error) {
          yield put(savePropoundDocumentEditorError('Failed to save the propound document'));
        } finally {
          yield call(setDocLoader, false);
        }
      }
    })
  }


  function* updateFormObjectionSaga() {
    yield takeLatest(UPDATE_OBJECTION_FORM, function* updater({ record, form, closeForm }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          if(record?.subgroupIds?.length > 0) {
            const recordInStore = yield select(selectRecord(record.case_id));
            const result = yield call(updateObjectionForm, Object.assign({}, { case_id: record.case_id, legalforms_id: record.legalforms_id, subgroupIds: record?.subgroupIds, lawyer_objection_text : record?.lawyer_objection_text, lawyer_objection_status: record?.lawyer_objection_status, document_type: record.document_type }));
            
            if (result?.length > 0) {
              let questions = recordInStore?.questions;
              let i = 0;
              while(i < result.length) {
                questions = questions?.map((r) => result[i]?.id === r.id ? Object.assign({}, result[i]) : Object.assign({}, r));
                i++;
              }
              yield put(updateObjectionFormSuccess(Object.assign({}, { id: record?.case_id, questions })));
              yield call(closeForm, false);
              yield put(stopSubmit(form));
              yield put(destroy(form));
              yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
            }
          } else {
            yield put(updateObjectionFormError(DEFAULT_UPDATE_FORM_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
          }
        } catch (error) {
          yield put(updateObjectionFormError(DEFAULT_UPDATE_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
        }
      }
    })
  }
  
  function* deleteAttorneyObjectionSaga() {
    yield takeLatest(DELETE_ATTORNEY_OBJECTION, function* updater({ record, closeForm, setLoader }) {
      if (record) {
        try {
          if(record?.subgroupIds?.length > 0) {
            const recordInStore = yield select(selectRecord(record.case_id));
            const result = yield call(updateObjectionForm, Object.assign({}, { case_id: record.case_id, legalforms_id: record.legalforms_id, subgroupIds: record?.subgroupIds, lawyer_objection_text : record?.lawyer_objection_text, lawyer_objection_status: record?.lawyer_objection_status, document_type: record.document_type }));
            
            if (result?.length > 0) {
              let questions = recordInStore?.questions;
              let i = 0;
              while(i < result.length) {
                questions = questions?.map((r) => result[i]?.id === r.id ? Object.assign({}, result[i]) : Object.assign({}, r));
                i++;
              }
              yield put(deleteAttorneyObjectionSuccess(Object.assign({}, { id: record?.case_id, questions })));
              yield call(closeForm, false);
              yield put(destroy(`objectionForm_${record.id}`));
              yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
            }
          } else {
            yield put(deleteAttorneyObjectionError(DEFAULT_ATTORNEY_OBJECTION_ERROR));
          }
        } catch (error) {
          yield put(deleteAttorneyObjectionError(DEFAULT_ATTORNEY_OBJECTION_ERROR));
        } finally {
          if(setLoader) {
            yield call(setLoader, false);
          }
        }
      }
    })
  }

  function* autoSaveFormDetailsSage() {
    yield takeLatest(AUTO_SAVE_FORM_DETAILS, function* updater({ record }) {
      if(record) {
        const storeRecord = yield select(selectRecord());
        try {
          let submitRecord;
          if(record?.autoSaveObjectionForm) {
            if(storeRecord?.autoSaveObjectionForm?.length > 0) {
              if(!storeRecord?.autoSaveObjectionForm?.includes(record?.autoSaveObjectionForm)) {
                submitRecord = Object.assign({}, { autoSaveObjectionForm: [...storeRecord?.autoSaveObjectionForm, record?.autoSaveObjectionForm] });
              }
            } else {
              submitRecord = Object.assign({}, { autoSaveObjectionForm: [record?.autoSaveObjectionForm] });
            }
            if(submitRecord) {
              submitRecord = Object.assign({}, { ...submitRecord }, { autoSaveForm : storeRecord?.autoSaveForm });
              yield put(autoSaveFormDetailsSuccess(submitRecord));
            }
          }
          if (record?.autoSaveForm) {
            if(storeRecord?.autoSaveForm?.length > 0) {
              if(!storeRecord?.autoSaveForm?.includes(record?.autoSaveForm)) {
                submitRecord = Object.assign({}, { autoSaveForm: [...storeRecord?.autoSaveForm, record?.autoSaveForm] });
              }
            } else {
              submitRecord = Object.assign({}, { autoSaveForm: [record?.autoSaveForm] });
            }

            if(submitRecord) {
              submitRecord = Object.assign({}, { ...submitRecord }, { autoSaveObjectionForm : storeRecord?.autoSaveObjectionForm });
              yield put(autoSaveFormDetailsSuccess(submitRecord));
            }
          }
        } catch(error) {
          yield put(autoSaveFormDetailsError('Failed to save the auto save form details'));
        }
      }
    })
  }

  function* saveDiscoveryFilenameSaga() {
    yield takeLatest(SAVE_DISCOVERY_FILENAME, function* updater({ record, setEditFile, setLoader }) {
      if (record) {
        try {
          const result = yield call(saveDiscoveryFilename, record);

          if (result) {
            const selectForms = yield select(selectForm());
            const storeRecord = yield select(selectRecord());
            const selectRespondEmailDoc = selectForms && selectForms[`respondEmailDocument`] && selectForms[`respondEmailDocument`].values;
            const selectedForms = selectRespondEmailDoc && selectRespondEmailDoc.selected_forms;
            const originalRecord = record?.document_type && storeRecord[record.document_type];

            const storeFormRecords = selectedForms && selectedForms?.length > 0 && selectedForms || false;
            const filterRecord = storeFormRecords && storeFormRecords.reduce((acc, el) => {
              if (el.legalform_id == record.legalform_id) {
                acc.push(Object.assign({}, el, { discovery_s3_file_key: result.new_s3_file_key, finalDocument: result.docDownloadUrl, pdf_s3_file_key: result.new_pdf_s3_file_key, pdf_filename: result.filename }));
              } else {
                acc.push(el);
              }
              return acc
            }, []);

            if (filterRecord && filterRecord?.length > 0) {
              yield put(change(`respondEmailDocument`, 'selected_forms', filterRecord));
            }

            if (originalRecord && originalRecord?.length > 0) {
              const filterRecords = originalRecord && originalRecord?.length > 0 && originalRecord.map((el, index) => {
                if (record.legalform_id == el.legalform_id) {
                  return Object.assign({}, el, { discovery_s3_file_key: result.new_s3_file_key, finalDocument: result.docDownloadUrl, pdf_s3_file_key: result.new_pdf_s3_file_key, pdf_filename: result.filename })
                } else {
                  return el;
                }
              });

              const submitRecord = Object.assign({}, storeRecord, { [record.document_type]: filterRecords });
              yield put(saveDiscoveryFilenameSuccess(submitRecord));
            }
          } else {
            yield put(saveDiscoveryFilenameError("Something went wrong"));
          }
        } catch (error) {
          yield put(saveDiscoveryFilenameError("Something went wrong"));
        } finally {
          if (setEditFile) {
            yield call(setEditFile, false);
          }
          if (setLoader) {
            yield call(setLoader, false);
          }
        }
      }
    })
  }

  function* savePropoundFilenameSaga() {
    yield takeLatest(SAVE_PROPOUND_FILENAME, function* updater({ record, setEditFile, setLoader }) {
      if (record) {
        const { propoundforms_id, document_type } = record;
        const docType = document_type && document_type.toLowerCase();
        try {
          const result = yield call(savePropoundFilename, record);

          if (result) {

            const selectForms = yield select(selectForm());
            let storeRecord = yield select(selectRecord());

            const selectPropoundEmailDoc = selectForms && selectForms[`emailDocument`] && selectForms[`emailDocument`].values;
            const selectedForms = selectPropoundEmailDoc && selectPropoundEmailDoc.selected_forms;

            const filterRecords = selectedForms && selectedForms?.length > 0 && selectedForms.map((el, index) => {
              if (propoundforms_id == el.id) {
                return Object.assign({}, el, { s3_file_key: result.new_s3_file_key, public_url: result.docDownloadUrl, pdf_s3_file_key: result.new_pdf_s3_file_key, pdf_filename: result.filename })
              } else {
                return el;
              }
            });

            yield put(change(`emailDocument`, 'selected_forms', filterRecords));
            
            let modifiedRecord = storeRecord && storeRecord.propounding_form_details[docType]?.length > 0 && storeRecord.propounding_form_details[docType].map((el) => {
              return Object.assign({}, el, { s3_file_key: result.new_s3_file_key, public_url: result.docDownloadUrl, pdf_s3_file_key: result.new_pdf_s3_file_key, pdf_filename: result.filename });
            }) || {};

            storeRecord.propounding_form_details[docType] = modifiedRecord;
            yield put(savePropoundFilenameSuccess(modifiedRecord));
          } else {
            yield put(savePropoundFilenameError("Something went wrong"));
          }
        } catch (error) {
          yield put(savePropoundFilenameError("Something went wrong"));
        } finally {
          if (setEditFile) {
            yield call(setEditFile, false);
          }
          if (setLoader) {
            yield call(setLoader, false);
          }
        }
      }
    })
  }


  function* UpdateQuestionsForHashSaga() {
    yield takeLatest(UPDATE_QUESTIONS, function* updater({ record, setShowUploadQuestionsForm }) {
      const { form } = record;
      if (record) {
        yield put(startSubmit(form));
        const fileName = record?.fileData[0]?.name;
        try {
          const result = yield call(uploadQuestionsFile, Object.assign({}, { filename: fileName }));
          if (result) {
            yield call(uploadFile, result.uploadURL, record.fileData[0], record?.fileData[0]?.type);
            const uploadResult = yield call(updateQuestions, Object.assign({}, { s3_file_key: result?.s3_file_key, questions: record.questions }));
            yield put(updateQuestionsSuccess(uploadResult && uploadResult.message || "Questions Uploaded Successfully."));
            yield put(stopSubmit(form));
            yield call(setShowUploadQuestionsForm, false);
            yield put(destroy(form));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Failed to upload questions.";
          yield put(updateQuestionsError(Err));
          yield put(stopSubmit(form));
          yield call(setShowUploadQuestionsForm, false);
          yield put(destroy(form));
        }
      } else {
        yield put(updateQuestionsError("Failed to upload questions."));
        yield put(stopSubmit(form));
        yield put(destroy(form));
      }
    })
  }

  function* DeleteHashWithFileSaga() {
    yield takeLatest(DELETE_HASH_WITH_FILE, function* updater({ record, setShowDeleteHashQuestionsForm }) {
      const { form } = record;
      if (record) {
        yield put(startSubmit(form));
        const fileName = record?.fileData[0]?.name;
        try {
          const result = yield call(uploadQuestionsFile, Object.assign({}, { filename: fileName }));
          if (result) {
            yield call(uploadFile, result.uploadURL, record.fileData[0], record?.fileData[0]?.type);
            const deleteHashResult = yield call(deleteHashData, Object.assign({}, { s3_file_key: result?.s3_file_key }));
            yield put(DeleteHashWithFileSuccess(deleteHashResult && deleteHashResult.message || "Questions Uploaded Successfully."));
            yield put(stopSubmit(form));
            yield call(setShowDeleteHashQuestionsForm, false);
            yield put(destroy(form));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Failed to delete questions.";
          yield put(DeleteHashWithFileError(Err));
          yield put(stopSubmit(form));
          yield call(setShowDeleteHashQuestionsForm, false);
          yield put(destroy(form));
        }
      } else {
        yield put(DeleteHashWithFileError("Failed to delete questions."));
        yield put(stopSubmit(form));
        yield call(setShowDeleteHashQuestionsForm, false);
        yield put(destroy(form));
      }
    })
  }

  function* loadFrogsQuestionsSaga() {
    yield takeLatest(LOAD_FROGS_QUESTIONS, function* ({ record }) {
      if(record) {
        try {
          if(record?.case_id) {
            const storeRecord = yield select(selectRecord(record.case_id));
            if(storeRecord) {
              const frogsLegalFormsId = storeRecord?.frogs && storeRecord?.frogs?.length > 0 ? storeRecord?.frogs?.map(e => e.legalform_id) : false;
              if(frogsLegalFormsId && frogsLegalFormsId?.length > 0) {
                const result = yield call(loadFrogsQuestions, Object.assign({}, record, { legalforms_id: frogsLegalFormsId }));
                if(result) {
                  const filterQuestions = result && Array.isArray(result) && result?.length > 0 && result?.filter(e => e.questions && e.questions?.length > 0) || [];
                  yield put(loadFrogsQuestionsSuccess(filterQuestions));
                }
              }
            }
          } else {
            yield put(loadFrogsQuestionsSuccess(false));
          }
        } catch (error) {
          yield put(loadFrogsQuestionsError('Faild to load frogs questions'));
        }
      }
    })
  }

  function* updateFrogsFormSaga() {
    yield takeLatest(UPDATE_FROGS_FORM, function* updater({ record, form, closeForm, subGroupForm = false }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          const dynamicQuestions = yield select(selectDynamicFrogsQuestions());
          if(dynamicQuestions && Object.keys(dynamicQuestions)?.length > 0) {
            const filterRecord = dynamicQuestions.id == record?.rfaRecord?.id ? dynamicQuestions : false;
            const filterForm = filterRecord && filterRecord?.forms && filterRecord?.forms?.filter((e) => e.legalforms_id === record?.data?.legalforms_id);
            let questions = filterForm && filterForm?.length > 0 && filterForm[0] && filterForm[0]?.questions.map(e => e?.id === record?.data?.id ? Object.assign({}, record?.data, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1 }) : Object.assign({}, e, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1 }));

            const result = yield call(updateForgsQuestions, Object.assign({}, { frogs_legalform_id: record?.data?.legalforms_id, save_form_id: record?.data?.id, rfa_legalform_id: record?.rfaRecord?.legalforms_id, rfa_form_id: record?.rfaRecord?.id, case_id: record?.data?.case_id, questions }));

            
            if (result) {
              yield delay(1000);
              const dynamicQuestions = yield select(selectDynamicFrogsQuestions());
              if(dynamicQuestions && dynamicQuestions?.id === result?.id && dynamicQuestions.legalforms_id == result?.legalforms_id) {
                let forms = dynamicQuestions?.forms.map(e => {
                  if(e.legalforms_id == result?.frogs_legalform_id) {
                    return Object.assign({}, e, { questions: result?.questions })
                  }
                  return e;
                });

                if(forms && forms?.length > 0) {                
                  yield put(updateFrogsFormSuccess(Object.assign({}, dynamicQuestions, { forms: forms })));
                  yield put(stopSubmit(form));
                  yield call(closeForm, false);
                  if(!subGroupForm) {
                    yield put(destroy(form));
                  }
                }
              }
            } else {
              yield put(updateFrogsFormError(DEFAULT_UPDATE_FORM_ERROR));
              yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
            }
          } 
          
        } catch (error) {
          yield put(updateFrogsFormError(DEFAULT_UPDATE_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
        } finally {
          if(subGroupForm) {
            yield put(destroy(form));
          }
        }
      }
    })
  }

  function* updateFrogsObjectionFormSaga() {
    yield takeLatest(UPDATE_FROGS_OBJECTION_FORM, function* updater({ record, form, closeForm }) {
      if (record) {
        yield put(startSubmit(form));
        try {
          if(record?.subgroupIds?.length > 0) {
            const result = yield call(updateObjectionForm, Object.assign({}, { case_id: record.case_id, legalforms_id: record.legalforms_id, subgroupIds: record?.subgroupIds, lawyer_objection_text : record?.lawyer_objection_text, lawyer_objection_status: record?.lawyer_objection_status, document_type: record.document_type }));
            
            if (result?.length > 0) {
              const frogsQuestionsInStore = yield select(selectFrogsQuestions());
              const filterRecord = record?.legalforms_id && frogsQuestionsInStore && Array.isArray(frogsQuestionsInStore) && frogsQuestionsInStore?.length > 0 && frogsQuestionsInStore.filter(e => e.legalforms_id === record?.legalforms_id);
              let questions = filterRecord && filterRecord[0] && filterRecord[0]?.questions;
              let i = 0;
              while(i < result.length) {
                questions = questions?.map((r) => result[i]?.id === r.id ? Object.assign({}, result[i]) : Object.assign({}, r));
                i++;
              }
              yield put(updateFrogsObjectionFormSuccess(Object.assign({}, { legalforms_id: record?.legalforms_id, questions })));
              yield call(closeForm, false);
              yield put(stopSubmit(form));
              yield put(destroy(form));
              yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
            }
          } else {
            yield put(updateObjectionFormError(DEFAULT_UPDATE_FORM_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
          }
        } catch (error) {
          yield put(updateObjectionFormError(DEFAULT_UPDATE_FORM_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_FORM_ERROR }));
        }
      }
    })
  }
  
  function* deleteAttorneyFrogsObjectionSaga() {
    yield takeLatest(DELETE_ATTORNEY_FROGS_OBJECTION, function* updater({ record, closeForm, setLoader }) {
      if (record) {
        try {
          if(record?.subgroupIds?.length > 0) {
            const result = yield call(updateObjectionForm, Object.assign({}, { case_id: record.case_id, legalforms_id: record.legalforms_id, subgroupIds: record?.subgroupIds, lawyer_objection_text : record?.lawyer_objection_text, lawyer_objection_status: record?.lawyer_objection_status, document_type: record.document_type }));
            
            if (result?.length > 0) {
              const frogsQuestionsInStore = yield select(selectFrogsQuestions());
              
              const filterRecord = record?.legalforms_id && frogsQuestionsInStore && Array.isArray(frogsQuestionsInStore) && frogsQuestionsInStore?.length > 0 && frogsQuestionsInStore.filter(e => e.legalforms_id === record?.legalforms_id);
              let questions = filterRecord && filterRecord[0] && filterRecord[0]?.questions;
              let i = 0;
              while(i < result.length) {
                questions = questions?.map((r) => result[i]?.id === r.id ? Object.assign({}, result[i]) : Object.assign({}, r));
                i++;
              }
              yield put(deleteAttorneyFrogsObjectionSuccess(Object.assign({}, { legalforms_id: record?.legalforms_id, questions })));
              yield call(closeForm, false);
              yield put(destroy(`objectionForm_${record.id}`));
              yield put(clearAutoSaveFormDetails(Object.assign({}, { autoSaveForm: false, autoSaveObjectionForm: false })));
            }
          } else {
            yield put(deleteAttorneyObjectionError(DEFAULT_ATTORNEY_OBJECTION_ERROR));
          }
        } catch (error) {
          yield put(deleteAttorneyObjectionError(DEFAULT_ATTORNEY_OBJECTION_ERROR));
        } finally {
          if(setLoader) {
            yield call(setLoader, false);
          }
        }
      }
    })
  }

  function* saveSubGroupFrogsFormSaga() {
    yield takeLatest(SAVE_SUB_GROUP_FROGS_FORM, function* updater({ record, setOpenForm }) {
      if (record) {
        try {
          const dynamicQuestions = yield select(selectDynamicFrogsQuestions());
          if(dynamicQuestions && Object.keys(dynamicQuestions)?.length > 0) {
            const filterRecord = dynamicQuestions.id == record?.rfaRecord?.id ? dynamicQuestions : false;
            const filterForm = filterRecord && filterRecord?.forms && filterRecord?.forms?.filter((e) => e.legalforms_id === record?.data?.legalforms_id);
            let questions;

            if(record?.data?.subgroup){
              const storeForms = yield select(selectForm());
              const selectedForm = storeForms && Object.keys(storeForms).length > 0 && Object.keys(storeForms).reduce((arr, key) => {
                if (key.indexOf('detailForm_') > -1 && storeForms[key] && storeForms[key].values && storeForms[key]['values']['case_id'] && storeForms[key]['values']['document_type'] && storeForms[key]['values']['lawyer_response_text']) {
                  arr.push(storeForms[key])
                }
                return arr;
              }, []).map(_ => _.values).find(_ => _.id === record?.data?.id) || false;

              const commonRecord = { lawyer_response_status: 'Final', subgroup: record?.data?.subgroup };
              if(selectedForm) {
                const subGroupRecord = Object.assign({}, selectedForm, commonRecord);
                questions = filterForm && filterForm?.length > 0 && filterForm[0] && filterForm[0]?.questions.map(e => {
                  if(e?.id === record?.data?.id) {
                    return Object.assign({}, subGroupRecord, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1 })
                  } else {
                    return Object.assign({}, e, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1, lawyer_response_text: record?.data?.subgroup ? `<p>See response ${e.question_number_text} ${record?.data?.question_section} in Set ${e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1}</p>` : e.lawyer_response_text, subgroup: false });
                  }
                })

                yield put(destroy(`detailForm_${record?.data?.id}`));
                yield call(setOpenForm, false);
              } else {
                questions = filterForm && filterForm?.length > 0 && filterForm[0] && filterForm[0]?.questions.map(e => {
                  if(e?.id === record?.data?.id) {
                    return Object.assign({}, e, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1, ...commonRecord })
                  } else {
                    return Object.assign({}, e, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1, lawyer_response_text: record?.data?.subgroup ? `<p>See response ${e.question_number_text} ${record?.data?.question_section} in Set ${e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1}</p>` : e.lawyer_response_text, subgroup: false })
                  }
                });
              }  
            } else {
              questions = filterForm && filterForm?.length > 0 && filterForm[0] && filterForm[0]?.questions.map(e => e?.id === record?.data?.id ? Object.assign({}, e, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1, subgroup: false }) : Object.assign({}, e, { duplicate_set_no: e.connected_document_ids ? e.duplicate_set_no : filterForm[0]?.max_duplicate_set_no + 1, lawyer_response_text: e.lawyer_response_text, subgroup: false }));
            }

            const result = yield call(updateForgsQuestions, Object.assign({}, { frogs_legalform_id: record?.data?.legalforms_id, save_form_id: record?.data?.id, rfa_legalform_id: record?.rfaRecord?.legalforms_id, rfa_form_id: record?.rfaRecord?.id, case_id: record?.data?.case_id, questions }));

            if (result) {
              yield delay(1000);
              const dynamicQuestions = yield select(selectDynamicFrogsQuestions());
              if(dynamicQuestions && dynamicQuestions?.id === result?.id && dynamicQuestions.legalforms_id == result?.legalforms_id) {
                let forms = dynamicQuestions?.forms.map(e => {
                  if(e.legalforms_id == result?.frogs_legalform_id) {
                    return Object.assign({}, e, { questions: result?.questions });
                  }
                  return e;
                });

                if(forms && forms?.length > 0) {                
                  yield put(updateFrogsFormSuccess(Object.assign({}, dynamicQuestions, { forms: forms })));
                }
              }
            } else {
              yield put(updateFrogsFormError(DEFAULT_UPDATE_FORM_ERROR));
            }
          }          
        } catch (error) {
          yield put(saveSubGroupFormError(DEFAULT_SAVE_SUB_GROUP_FORM_ERROR));
        }
      }
    })
  }

  function* editPropoundFormSaga() {
    yield takeLatest(EDIT_PROPOUND_FORM, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(editPropoundFormName, Object.assign({}, { id: record?.legalFormId, file_name: record?.filename, case_id: record?.case_id, document_type: record?.document_type }));
          if (result) {
            const propounding_form = record?.propounding_form_details && record?.propounding_form_details[record.document_type] && record?.propounding_form_details[record.document_type]?.map(e => e.id.toString() === record?.legalFormId.toString() ? Object.assign({}, e, { file_name: record.filename }) : e);

            const propounding_form_details = Object.assign({}, { ...record?.propounding_form_details }, { [record.document_type]: propounding_form })

            yield put(editPropoundFormSuccess(Object.assign({}, { id: record.id, propounding_form_details }), 'Form Name Changed Successfully.'));
          } else {
            yield put(editPropoundFormError(DEFAULT_EDIT_FORM_ERROR));
          }
        } catch (error) {
          yield put(editPropoundFormError(DEFAULT_EDIT_FORM_ERROR));
        }
      }
    })
  }
  
  
  function* addFrogsQuestionsSaga() {
    yield takeLatest(ADD_FROGS_QUESTIONS, function* updater({ record, modalForm, setLinkLoader }) {
      if (record) {
        try {
          const result = yield call(addFrogsQuestions, record);
          if (result) {
            yield put(addFrogsQuestionsSuccess(result));
            yield delay(500);
            yield call(modalForm, true);
          } else {
            yield put(addFrogsQuestionsError('Failed to create a frogs questions'));
          }
        } catch (error) {
          yield put(addFrogsQuestionsError('Failed to create a frogs questions'));
        } finally {
          yield call(setLinkLoader, false);
        }
      }
    })
  }

  function* deleteDuplicateSetSaga() {
    yield takeLatest(DELETE_DUPLICATE_SET, function* updater({ record, setFormLoader }) {
      if (record) {
        try {
          const result = yield call(deleteFrogsDuplicateSetQuestions, record);
          if (result) {
            yield put(deleteDuplicateSetSuccess('Frogs set deleted Successfully'));
            yield delay(2000);
            yield put(loadFormAction(Object.assign({}, { client_response_status_filter: "All", document_type: record.document_type.toUpperCase(), id: record.case_id, lawyer_response_status_filter: "All", legalforms_id: record.legalforms_id, client_id: record.client_id || false, party_id: record.party_id || false })));            
          } else {
            yield put(deleteDuplicateSetError('Failed to delete Frogs set'));
          }
        } catch (error) {
          yield put(deleteDuplicateSetError('Failed to delete Frogs set'));
        } finally {
          yield call(setFormLoader,false);
        }
      }
    })
  }
  
  function* deleteConnectedQuestionSetSaga() {
    yield takeLatest(DELETE_CONNECTED_QUESTION_SET, function* updater({ record }) {
      if (record) {
        try {
          const result = yield call(deleteDuplicateSetQuestions, record);
          if (result) {
            yield put(deleteConnectedQuestionSetSuccess('Frogs set deleted Successfully'));          
          } else {
            yield put(deleteConnectedQuestionSetError('Failed to delete Frogs set'));
          }
        } catch (error) {
          yield put(deleteConnectedQuestionSetError('Failed to delete Frogs set'));
        }
      }
    })
  }

  function* fetchDiscoveryHistorySaga() {
    yield takeLatest(FETCH_DISCOVERY_HISTORY, function* updater({ legalforms_id, setModal, discoveryData, openDocument }) {
      if (legalforms_id) {
        try {
          const result = yield call(getDiscoveryVersions, legalforms_id);
          if (result) {
            yield put(fetchDiscoveryHistorySuccess(result)); 
            // if (setOpenDocEditor) yield put(setOpenDocEditor(true)); 
            if (result && result.length){
              yield put(setModal(true));
            }
            else{
              yield put(openDocument(discoveryData));
            }
          } else {
            yield put(fetchDiscoveryHistoryError('Failed to fetch discovery history'));
          }
        } catch (error) {
          yield put(fetchDiscoveryHistoryError('Failed to fetch discovery history'));
        } 
      }
    })
  }


  function* getSFDTextSaga() {
    yield takeLatest(GET_SFDT_TEXT, function* updater({ sfdt_s3key, setDocumentLoader, setSfdtData }) {
      if (sfdt_s3key) {
        if (setDocumentLoader) yield call(setDocumentLoader, true);
        try {
          const result = yield call(getSfdtFromS3, Object.assign({}, {sfdt_s3key : sfdt_s3key}));
          if(result){
            const sfdtRawText = yield call(getSfdtTextByUrl, result?.sfdtPublicUrl );
            if(sfdtRawText){
              yield call(setDocumentLoader, false);
              yield call(setSfdtData, sfdtRawText?.SFDText);
              yield put(getSFDTtextSuccess(sfdtRawText?.SFDText));
            } 
          }
        } catch (error) {
          yield put(getSFDTtextError('Failed to load document editor'));
        } 
      }
    })
  }


  return function* rootSaga() {
    yield all([
      uploadFormSaga(),
      generateFormSaga(),
      cancelFormSaga(),
      saveFormSaga(),
      loadFormSaga(),
      updateFormSaga(),
      fetchFormSaga(),
      generateDocumentSaga(),
      saveDocumentSaga(),
      sendQuestionsSaga(),
      loadBillingDetailsSaga(),
      updateBillingDetailsSaga(),
      deleteFormSaga(),
      saveStandardFormSaga(),
      saveQuestionsFormSaga(),
      createMedicalHistorySaga(),
      updateMedicalHistorySaga(),
      deleteMedicalHistorySaga(),
      viewMedicalHistorySaga(),
      generateMedicalSummarySaga(),
      requestHelpSaga(),
      requestTranslationSaga(),
      mergeTranslationSaga(),
      sendVerificationSaga(),
      autoSaveFormSaga(),
      copyToLawyerResponseSaga(),
      shredSaga(),
      editFormSaga(),
      attachCaseSaga(),
      createClientRecordSaga(),
      setResponseDateSaga(),
      resendQuestionsSaga(),
      saveAllFormSaga(),
      saveSubGroupFormSaga(),
      generatePOSDocumentSaga(),
      copyAllToLawyerResponseSaga(),
      saveCommentSaga(),
      archiveCaseSaga(),
      unArchiveCaseSaga(),
      addConsultationSaga(),
      deleteConsultationSaga(),
      updateEditedFlagSaga(),
      savePropoundFormSaga(),
      loadPropoundRecordSaga(),
      loadPropoundFormSaga(),
      deletePropoundFormSaga(),
      updatePropoundFormSaga(),
      generatePropoundDocumentSaga(),
      savePropoundFormS3KeySaga(),
      sendPropoundQuestionsSaga(),
      savePropounderServingAttorneySaga(),
      questionTemplateSaga(),
      destroyStandardFormSaga(),
      createClientCaseSaga(),
      updatePlanDetailsSaga(),
      sendRespondQuestionsSaga(),
      quickCreateSaveStandardFormSaga(),
      quickCreateSaveQuestionsFormSaga(),
      deleteQuickCreateDocumentSaga(),
      updateUserRecordSaga(),
      quickCreateRedirectPathSaga(),
      updateVerifySession(),
      inValidSubscriptionsSaga(),
      restoreResponseHistorySaga(),
      attorneyResponseTrackingSaga(),
      filevineSessionCommonUpdateSaga(),
      litifyUploadDocumentSaga(),
      createLitifyBulkRecordsSaga(),
      litifyUpdateUserDetailsSaga(),
      plaintiffDefendantSwitchSaga(),
      initalSubscriptionSaga(),
      createLitifyClientCaseSaga(),
      loadDocumentEditorSaga(),
      saveDocumentEditorSaga(),
      loadPropoundDocumentEditorSaga(),
      savePropoundDocumentEditorSaga(),
      updateFormObjectionSaga(),
      deleteAttorneyObjectionSaga(),
      autoSaveFormDetailsSage(),
      saveDiscoveryFilenameSaga(),
      savePropoundFilenameSaga(),
      UpdateQuestionsForHashSaga(),
      DeleteHashWithFileSaga(),
      loadFrogsQuestionsSaga(),
      updateFrogsFormSaga(),
      updateFrogsObjectionFormSaga(),
      deleteAttorneyFrogsObjectionSaga(),
      saveSubGroupFrogsFormSaga(),
      editPropoundFormSaga(),
      addFrogsQuestionsSaga(),
      deleteDuplicateSetSaga(),
      deleteConnectedQuestionSetSaga(),
      fetchDiscoveryHistorySaga(),
      getSFDTextSaga()
    ]);
  }
}



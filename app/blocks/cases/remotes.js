/*
 *
 *  cases remotes
 *
 */

import api from 'utils/api';
import axios from 'axios';
/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * @param {object} record 
     */
    function uploadForm(record) {
        return api.post(`legalforms/upload-legal-form`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function generateForm(record) {
        return api.get(`${process.env.SCANNED_URL}/extract?key=${record.s3_file_key}&document_type=${record.document_type.toUpperCase()}&id=${record.id}&case_id=${record.case_id}&client_id=${record.client_id}&state=${record.state}&federal=${record.federal}&federal_district=${record.federal_district}&practice_id=${record.practice_id}&hash_id=${record.hash_id}&doc_textract_region=${record.doc_textract_region}&doc_textract_region_count=${record.doc_textract_region_count}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function cancelForm(record) {
        return api.put(`rest/document-upload-progreess/set-status`, Object.assign({}, record, { legalforms_id: record.id })).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function saveForm(record) {
        return api.post(`legalforms/save-form-data/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function loadForm(record) {
        let params;
        if ((record.party_id && record.client_id && record.client_id != record.party_id) || (record.party_id && !record.client_id)) {
            params = `lawyer_response_status_filter=${record.lawyer_response_status_filter}&client_response_status_filter=${record.client_response_status_filter}&party_id=${record.party_id}&legalforms_id=${record.legalforms_id || ''}`;
        } else {
            params = `lawyer_response_status_filter=${record.lawyer_response_status_filter}&client_response_status_filter=${record.client_response_status_filter}&legalforms_id=${record.legalforms_id || ''}`;
        }
        return api.get(`forms/data/${record.id}/${record.document_type}?${params}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateForm(record) {
        return api.put(`forms/data/${record.case_id}/${record.document_type}`, Object.assign({}, record, { lawyer_response_text: record.lawyer_response_text ? record.lawyer_response_text.replace(/[^\x00-\x7F]/g, ""): record.lawyer_response_text })).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function fetchForm(record) {
        return api.get(`forms/data/${record.id}/${record.document_type}?lawyer_response_status_filter=${record.lawyer_response_status_filter}&client_response_status_filter=${record.client_response_status_filter}&legalforms_id=${record.legalforms_id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function generateDocument(record) {
        return api.post(`${process.env.SCANNED_URL}/docGen`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function saveDocument(record) {
        return api.put(`legalforms/save-generated-document`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function sendQuestions(record) {
        return api.post(`forms/send-questions-to-client?sending_type=${record.sending_type}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function loadBillingDetails(record) {
        return api.post(`billing/fetch-billing-status`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function getClientSecret() {
        return api.get(`/billing/set-payment-intent`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateBillingDetails(record) {
        return api.post(`billing/save-card-data`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function deleteForm(record) {
        return api.post(`legalforms/delete-form`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function createMedicalHistory(record) {
        return api.post(`rest/medical-history`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateMedicalHistory(record) {
        return api.put(`rest/medical-history/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function deleteMedicalHistory(record) {
        return api.delete(`rest/medical-history/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function viewMedicalHistory(record) {
        return api.post(`rest/practices/get-secure-public-url-of-private-file`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function extractTotalPages(record, comment) {
        const url = comment ? `${process.env.CLIENT_URL}/extractTotalPages?key=${record.s3_file_key}&comment=${comment}` : `${process.env.CLIENT_URL}/extractTotalPages?key=${record.s3_file_key}`;
        return api.get(url).then((response) => response.data && response.data.total_pages).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function generateMedicalSummary(record) {
        return api.post(`rest/medical-history-summery`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function getDocumentStatus(record) {
        return api.get(`rest/document-upload-progreess/get-status?case_id=${record.case_id}&document_type=${record.document_type.toUpperCase()}&legalforms_id=${record.id}`).then((response) => response.data && response.data.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function requestHelp(record) {
        return api.post(`rest/help-request/send-help-request`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function requestTranslation(record) {
        return api.post(`/rest/translate`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function sendVerification(record) {
        return api.post(`/rest/forms/send-layer-answer-verification-to-client`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record  
     */
    function shred(record) {
        return api.post(`rest/legalforms/shred`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record  
     */
    function editForm(record) {
        return api.put(`/rest/legalforms/update-filename/${record.legalFormId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function attachCase(record) {
        return api.post(`rest/${name}/clone`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function createClientRecord(record) {
        return api.post(`/rest/${name}/clone-client`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }


    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function setResponseDate(record) {
        return api.post(`/rest/legalforms/forms-responsedeadline-setdate`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function resendQuestions(record) {
        return api.post(`/rest/forms/resend-previous-questions`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {array} records 
     */
    function saveAllForm(records) {
        return api.put(`/rest/forms/saveall-lawyer-response`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function sendInvoice(record) {
        return api.post(`/rest/send-invoice`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {array} records 
     */
    function saveSubGroupForm(records) {
        return api.put(`/rest/forms/saveall-lawyer-response`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function generatePOSDocument(record) {
        return api.post(`${process.env.SCANNED_URL}/posGen`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {array} records 
     */
    function copyAllToLawyerResponse(records) {
        return api.put(`/rest/forms/saveall-lawyer-response`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     * @returns
     */
    function archiveCase(record) {
         return api.post(`/rest/archive/cases`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     * @returns
     */
    function unArchiveCase(record) {
        return api.post(`/rest/unarchive/cases`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {array} records 
     */
    function addConsultation(records) {
        return api.post(`/rest/forms/add-new-consultation`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {array} records 
     */
    function deleteConsultation(records) {
        return api.post(`/rest/forms/remove-consultation`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {array} record 
     */
    function updateEditedFlag(record) {
        return api.put(`/rest/forms/update-client-response-edited-status`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {array} records 
     */
    function autoSaveForms(records) {
        return api.put(`/rest/forms/lawyer-response-auto-save`, records).then((response) => response.data).catch((error) => Promise.reject(error))
    }

    /**
     * @param {object} record 
     */
    function savePropoundForm(record) {
        return api.post(`/rest/cases/propound`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function loadPropoundRecord(id) {
        return api.get(`/rest/cases/propound?case_id=${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function loadPropoundForm(record) {
        return api.get(`/rest/cases/propound/${record.id}/${record.case_id}/${record.document_type}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function deletePropoundForm(record) {
        return api.delete(`/rest/cases/propound/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updatePropoundForm(record) {
        return api.put(`/rest/cases/propound/${record.id}/${record.case_id}/${record.document_type}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function savePropoundFormS3Key(record) {
        return api.put(`/rest/cases/propound/save-s3-key/${record.propound_id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function sendPropoundQuestions(record) {
        return api.post(`/rest/cases/propound/send-propound-template`, record).then((response) => response.data).catch((error) => Promise.reject(error))
    }

    /**
     * @param {object} record 
     */
    function savePropounderServingAttorney(record) {
        return api.put(`/rest/cases/propoundforms/save-serving-attorney-details/${record.propound_id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function savePropounderQuestions(record) {
        return api.put(`/rest/cases/propound/generated-document-questions`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function saveDefaultQuestionsTemplate(record) {
        return api.post(`/rest/frogs-template`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function defaultQuestionsTemplate(record) {
        return api.get(`/rest/frogs-template/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record  
     */
    function createClientCase(record) {
        return api.post(`/rest/integrations/update-client-and-case`, record).then(res => res.data).catch(err => Promise.reject(err))
    }

    /**
     * @param {object} record 
     */
     function updatePlanDetails(record) {
        return api.post(`billing/save-card-data`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function sendRespondQuestions(record) {
        return api.post(`/rest/legalforms/send-email-forms`, record).then((response) => response.data).catch((error) => Promise.reject(error))
    }

    /**
     * @param {object} record 
     */
    function convertDocumentToPdf(record) {
        return api.post(`${process.env.PDF_CONVERSION_URL}`, record, { timeout: 120000 }).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function savePdfS3Key(record) {
        return api.put(`/rest/legal-forms/save-s3-key/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateLegalForm(record) {
        return api.put(`/rest/document-extraction-progress/${record.legalforms_id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {string} id 
     */
    function deleteQuickCreateDocument(id) {
        return api.delete(`/rest/document-extraction-progress/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function updateUserRecord(record){
        return api.put(`/rest/user/update-details`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function filevineCommonUpdate(record) {
        return api.put(`/rest/integrations/common-update-filevine`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function mycaseCommonUpdate(record) {
        return api.put(`/rest/integrations/common-update-mycase`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function litifyCommonUpdate(record) {
        return api.put(`/rest/integrations/common-update-litify`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     */
    function litifyUploadDocument(record){
        return api.post('/rest/integrations/litify-file-upload', record).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} records 
     * @returns 
     */
    function createLitifyBulkRecords(records){
        return api.post('/rest/integrations/bulk-create-litify', records).then(response => response.data).catch(error => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function clioCommonUpdate(record) {
        return api.put(`/rest/integrations/common-update-clio`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function createUserSubscriptions(record) {
        return api.post(`/rest/users-license/subscription`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     * @returns
     */
    function importSfdtFromUrl(record) {
        return api.post(`${process.env.DOCUMENT_SFDT_URL}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function saveDocumentEditor(record) {
        return api.post(`/rest/doc-editor/url-generator`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record
     * @returns
     */
    function saveSfdtText(record) {
        return api.put(`/rest/legal-forms/save-doc-SFDT`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateObjectionForm(record) {
        return api.put(`/rest/forms/save-lawyer-objections`, Object.assign({}, record, { lawyer_objection_text: record.lawyer_objection_text.replace(/[^\x00-\x7F]/g, "") })).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {Array} records
     */
    function saveAllObjectionForm(records) {
        return api.put(`/rest/forms/save-multiple-lawyer-objections`, records).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function saveDiscoveryFilename(record) {
        return api.put(`/rest/file-rename`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function savePropoundFilename(record) {
        return api.put(`/rest/file-rename`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }


    /**
     * @param {object} record
     */
    function hashedFileQuestions(record) {
        return api.post(`/rest/hash/file-hashing`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function uploadQuestionsFile(record) {
        return api.post(`/rest/hash/file-upload-url`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function updateQuestions(record) {
        return api.put(`/rest/hash/update-question`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function deleteHashData(record) {
        return api.post(`/rest/hash/delete-hash-data`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function loadFrogsQuestions(record) {
        return api.post(`/rest/legalforms/get-frogs-questions`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function editPropoundFormName(record) {
        return api.put(`/rest/propound-forms/update-filename`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function addFrogsQuestions(record) {
        return api.post(`/rest/forms/create-duplicate-data-for-frogs`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function updateForgsQuestions(record) {
        return api.post(`/rest/forms/update-connected-forms-data`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record
     */
    function deleteFrogsDuplicateSetQuestions(record) {
        return api.post(`/rest/forms/remove-connected-form-set`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    } 
    
    /**
     * @param {object} record
     */
    function deleteDuplicateSetQuestions(record) {
        return api.post(`/rest/forms/remove-all-frogs-connected-form`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {string} legalforms_id 
     */
    function getDiscoveryVersions(legalforms_id) {
        return api.get(`/rest/document-versioning?legalforms_id=${legalforms_id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function getSfdtFromS3(record) {
        return api.post(`/rest/doc-editor/public-url-generator`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function getSfdtTextByUrl(url) {
        return axios.get(`${url}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function docToPdfFileRename(record) {
        return api.post(`/rest/doc-to-pdf-file-rename`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }


    /**
     * @param {object} record 
     */
    function pdfConversion(record) {
        return api.post(`/rest/pdf-conversion`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        uploadForm,
        generateForm,
        cancelForm,
        saveForm,
        loadForm,
        updateForm,
        generateDocument,
        saveDocument,
        loadBillingDetails,
        getClientSecret,
        updateBillingDetails,
        deleteForm,
        createMedicalHistory,
        updateMedicalHistory,
        deleteMedicalHistory,
        viewMedicalHistory,
        extractTotalPages,
        generateMedicalSummary,
        getDocumentStatus,
        requestHelp,
        requestTranslation,
        sendVerification,
        sendQuestions,
        fetchForm,
        shred,
        editForm,
        attachCase,
        createClientRecord,
        setResponseDate,
        resendQuestions,
        saveAllForm,
        sendInvoice,
        saveSubGroupForm,
        generatePOSDocument,
        copyAllToLawyerResponse,
        archiveCase,
        unArchiveCase,
        addConsultation,
        deleteConsultation,
        updateEditedFlag,
        autoSaveForms,
        savePropoundForm,
        loadPropoundRecord,
        loadPropoundForm,
        deletePropoundForm,
        updatePropoundForm,
        savePropoundFormS3Key,
        sendPropoundQuestions,
        savePropounderServingAttorney,
        savePropounderQuestions,
        saveDefaultQuestionsTemplate,
        defaultQuestionsTemplate,
        createClientCase,
        updatePlanDetails,
        sendRespondQuestions,
        convertDocumentToPdf,
        savePdfS3Key,
        updateLegalForm,
        deleteQuickCreateDocument,
        updateUserRecord,
        filevineCommonUpdate,
        mycaseCommonUpdate,
        litifyCommonUpdate,
        litifyUploadDocument,
        createLitifyBulkRecords,
        clioCommonUpdate,
        createUserSubscriptions,
        importSfdtFromUrl,
        saveDocumentEditor,
        saveSfdtText,
        updateObjectionForm,
        saveAllObjectionForm,
        saveDiscoveryFilename,
        savePropoundFilename,
        hashedFileQuestions,
        uploadQuestionsFile,
        updateQuestions,
        deleteHashData,
        loadFrogsQuestions,
        editPropoundFormName,
        addFrogsQuestions,
        updateForgsQuestions,
        deleteFrogsDuplicateSetQuestions,
        deleteDuplicateSetQuestions,
        getDiscoveryVersions,
        getSfdtFromS3,
        getSfdtTextByUrl,
        docToPdfFileRename,
        pdfConversion
    }

}

/*
 *
 *  customTemplate sagas
 *
 */


import { call, takeLatest, put, all, delay } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';

import {
  DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR,
  DEFAULT_CREATE_RECORD_ERROR
} from 'utils/errors';

import { uploadFile } from 'utils/api';
import { s3BucketModifiedTemplate, s3BucketCustomTemplate } from '../upload/remotes';
import { push } from 'react-router-redux';

/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    UPDATE_PRACTICE_MODIFIED_TEMPLATE,
    CREATE_CUSTOM_TEMPLATE,
    UPDATE_CUSTOM_TEMPLATE
  } = constants;


  const {
    updatePracticeModifiedTemplateError,
    updatePracticeModifiedTemplateSuccess,
    createCustomTemplateSuccess,
    createCustomTemplateError,
    updateCustomTemplateSuccess,
    updateCustomTemplateError
  } = actions;


  const {
    updateModifiedTemplate,
    createCustomTemplate,
    updateCustomTemplate
  } = remotes;


  function* updatePracticeModifiedTemplateSaga() {
    yield takeLatest(UPDATE_PRACTICE_MODIFIED_TEMPLATE, function* updater({ record, form, queryParams }) {
      let response, modified_template, modified_template_s3_file_key;
      if (record) {
        yield put(startSubmit(form));
        try {
          const uploadedFiles = record && record.modified_template && record.modified_template[0].name;
          const redirectURL = queryParams && (queryParams === 'propounding' && `/customCategories/rest/practice-propound-template` || queryParams === 'responding' && `/customCategories/rest/practice-template`) || `/rest/practice-template`;
          if (record && record.document_type !== 'template') {
            modified_template = `modified_template_${record.document_type.toLowerCase()}`;
            modified_template_s3_file_key = `modified_template_s3_file_key_${record.document_type.toLowerCase()}`;
          }
          if (uploadedFiles) {
            const result = yield call(s3BucketModifiedTemplate, record.modified_template[0].name, record.modified_template[0].type, record.practice_id, record.document_type);
            yield call(uploadFile, result.uploadURL, record.modified_template[0], record.modified_template[0].type);
            const modifiedTemplateRecord = record && record.document_type === 'template' ? Object.assign({}, record, { template: result.public_url, template_s3_file_key: result.s3_file_key }) : Object.assign({}, record, { [modified_template]: result.public_url, [modified_template_s3_file_key]: result.s3_file_key, practice_id: record.practice_id });
            response = yield call(updateModifiedTemplate, modifiedTemplateRecord, queryParams);
          } else {
            response = yield call(updateModifiedTemplate, record, queryParams);
          }
          if (response) {
            yield put(updatePracticeModifiedTemplateSuccess(response, 'Modified Template Updated'));
            yield put(push(process.env.PUBLIC_PATH || redirectURL));
            yield put(stopSubmit(form));
          } else {
            yield put(updatePracticeModifiedTemplateError(DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR }));
          }
        } catch (error) {
          yield put(updatePracticeModifiedTemplateError(DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR }));
        }
      }
    })
  }

  function* createCustomTemplateSaga() {
    yield takeLatest(CREATE_CUSTOM_TEMPLATE, function* updater({ record, form, loadRecords, queryParams }) {
      if (record) {
        
        yield put(startSubmit(form));
        try {
          let result;
          const response = yield call(s3BucketCustomTemplate, record.custom_template[0].name, record.custom_template[0].type);
          if (response) {
            yield call(uploadFile, response.uploadURL, record.custom_template[0], record.custom_template[0].type);
          }
          result = yield call(createCustomTemplate, Object.assign({}, record, { custom_template: response.public_url, custom_template_s3_file_key: response.s3_file_key }), queryParams);
          const redirectURL = queryParams && queryParams === 'responding' && `/customCategories/rest/practice-template` || queryParams === 'propounding' && `/customCategories/rest/practice-propound-template` || `/rest/practice-template`;

          if (result) {
            yield put(createCustomTemplateSuccess(result, 'You will receive an email notification once template file is processed and ready for use'));
            yield put(stopSubmit(form));
            yield put(push(process.env.PUBLIC_PATH || redirectURL));
            yield put(loadRecords(true, queryParams));
          } else {
            yield put(createCustomTemplateError(DEFAULT_CREATE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CREATE_RECORD_ERROR }));
          }

        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_RECORD_ERROR;
          yield put(createCustomTemplateError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    })
  }

  function* updateCustomTemplateSaga() {
    yield takeLatest(UPDATE_CUSTOM_TEMPLATE, function* updater({ record, form, loadRecords, queryParams }) {

      if (record) {
        yield put(startSubmit(form));
        try {
          const uploadedFiles = record && record.custom_template && record.custom_template[0].name;
          const redirectURL = queryParams && (queryParams === 'propounding' && `/customCategories/rest/practice-propound-template` || queryParams === 'responding' && `/customCategories/rest/practice-template`) || `/rest/practice-template`;
          let result;
          if(uploadedFiles){
            const response = yield call(s3BucketCustomTemplate, record.custom_template[0].name, record.custom_template[0].type);
            if (response) {
              yield call(uploadFile, response.uploadURL, record.custom_template[0], record.custom_template[0].type);
            }
            result = yield call(updateCustomTemplate, Object.assign({}, record, { custom_template: response.public_url, custom_template_s3_file_key: response.s3_file_key }),queryParams);
          } else {
            result = yield call(updateCustomTemplate, record, queryParams);
          }

          if (result) {
            yield put(updateCustomTemplateSuccess(result, 'Modified Template Updated'));
            yield put(push(process.env.PUBLIC_PATH || redirectURL));
            yield put(stopSubmit(form));
            yield put(loadRecords(true, queryParams));
          } else {
            yield put(updateCustomTemplateError(DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR }));
          }
        } catch (error) {
          yield put(updateCustomTemplateError(DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR }));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      updatePracticeModifiedTemplateSaga(),
      createCustomTemplateSaga(),
      updateCustomTemplateSaga()
    ]);
  }
}


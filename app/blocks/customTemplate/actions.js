/*
 *
 * customTemplate actions
 *
 */

/**
 * 
 * @param {object} constants 
 * @returns 
 */
 export default function (constants) {

    const {
        UPDATE_PRACTICE_MODIFIED_TEMPLATE,
        UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR,
        UPDATE_PRACTICE_MODIFIED_TEMPLATE_SUCCESS,
        CREATE_CUSTOM_TEMPLATE,
        CREATE_CUSTOM_TEMPLATE_SUCCESS,
        CREATE_CUSTOM_TEMPLATE_ERROR,
        UPDATE_CUSTOM_TEMPLATE,
        UPDATE_CUSTOM_TEMPLATE_SUCCESS,
        UPDATE_CUSTOM_TEMPLATE_ERROR
    } = constants;

    /**
     * @param {object} record 
     * @param {string} form 
     * @param {string} queryParams
     */
    function updatePracticeModifiedTemplate(record, form, queryParams) {
        return {
            type: UPDATE_PRACTICE_MODIFIED_TEMPLATE,
            record,
            form,
            queryParams
        };
    }

    /**
     * @param {string} error 
     */
    function updatePracticeModifiedTemplateError(error) {
        return {
            type: UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updatePracticeModifiedTemplateSuccess(record, success) {
        return {
            type: UPDATE_PRACTICE_MODIFIED_TEMPLATE_SUCCESS,
            record,
            success
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     * @param {function} loadRecords 
     * @param {string} queryParams
     */
    function createCustomTemplate(record, form, loadRecords, queryParams) {
        return {
            type: CREATE_CUSTOM_TEMPLATE,
            record,
            form,
            loadRecords,
            queryParams
        };
    }

    /**
     * @param {object} record
     * @param {string} success
     */
    function createCustomTemplateSuccess(record, success) {
        return {
            type: CREATE_CUSTOM_TEMPLATE_SUCCESS,
            record,
            success
        };
    }

    /**
     * @param {string} error 
     */
    function createCustomTemplateError(error) {
        return {
            type: CREATE_CUSTOM_TEMPLATE_ERROR,
            error,
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     * @param {function} loadRecords 
     * @param {string} queryParams
     */
    function updateCustomTemplate(record, form, loadRecords, queryParams) {
        return {
            type: UPDATE_CUSTOM_TEMPLATE,
            record,
            form,
            loadRecords,
            queryParams
        };
    }

    /**
     * @param {string} error 
     */
    function updateCustomTemplateError(error) {
        return {
            type: UPDATE_CUSTOM_TEMPLATE_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updateCustomTemplateSuccess(record, success) {
        return {
            type: UPDATE_CUSTOM_TEMPLATE_SUCCESS,
            record,
            success
        };
    }

    return {
        updatePracticeModifiedTemplate,
        updatePracticeModifiedTemplateError,
        updatePracticeModifiedTemplateSuccess,
        createCustomTemplate,
        createCustomTemplateSuccess,
        createCustomTemplateError,
        updateCustomTemplate,
        updateCustomTemplateSuccess,
        updateCustomTemplateError
    }
}

/*
 *
 *  customTemplate remotes
 *
 */

import api from 'utils/api';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    /**
     * @param {object} record 
     * @param {string} queryParams
     */
    function updateModifiedTemplate(record, queryParams) {
        const url = queryParams ? `rest/practice-template/${record.id}?type=${queryParams}` : `rest/practice-template/${record.id}?type=responding`
        return api.put(url, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} record
     * @param {string} queryParams
     * @returns
     */
     function createCustomTemplate(record, queryParams) {
        const url = queryParams ? `/rest/practice-template?type=${queryParams}` : `/rest/practice-template?type=responding`;
        return api.post(url, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     * @param {string} queryParams
     */
     function updateCustomTemplate(record, queryParams) {
        const url = queryParams ? `rest/practice-template/${record.id}?type=${queryParams}` : `rest/practice-template/${record.id}?type=responding`;
        return api.put(url, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        updateModifiedTemplate,
        createCustomTemplate,
        updateCustomTemplate
    }
}


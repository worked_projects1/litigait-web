import api, { uploadFile } from 'utils/api';
import axios from 'axios';

/**
 * @param {string} file_name 
 * @param {string} content_type 
 */
function getSignature(file_name, content_type) {
  return api.post(`/upload-file`, Object.assign({}, { file_name, content_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * @param {string} file_name 
 * @param {string} content_type 
 */
export function getSecretSignature(file_name, content_type) {
  return api.post(`/rest/practices/upload-private-file`, Object.assign({}, { file_name, content_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * @param {string} s3_file_key 
 */
export async function getSecretURL(s3_file_key) {
  return await api.post(`/rest/practices/get-secure-public-url-of-private-file`, Object.assign({}, { s3_file_key })).then((response) => response.data && response.data.publicUrl).catch((error) => Promise.reject(error));
}

/**
 * @param {string} s3_file_key 
 */
export async function getSecureFormURL(s3_file_key) {
  return await api.post(`/rest/practices/get-secure-public-url-of-form-file`, Object.assign({}, { s3_file_key })).then((response) => response.data && response.data.publicUrl).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {string} file_name 
 * @param {string} content_type 
 * @param {File} file 
 * @param {function} callback 
 * @param {object} node 
 * @param {object} record 
 * @returns 
 */
 export async function s3BucketPrivate(file_name, content_type, file, callback, node, comment, record) {
  return await api.post(`/rest/practices/upload-private-file`, Object.assign({}, { file_name, content_type })).then(async (response) => {
      await axios.put(response.data.uploadURL, file, Object.assign({}, { headers: { 'Content-Type': file.type }})).then((response) => response).catch((error) => Promise.reject(error));
      if(callback){
        const data = await callback(response.data, comment);
        return Object.assign({}, record, response.data, { file_name, [node]: data });
      } else {
        return response.data;
      }
  }).catch((error) => Promise.reject(error));
}

export async function s3BucketPublic(file_name, content_type, file, record) {
  return await api.post(`/upload-file`, Object.assign({}, { file_name, content_type })).then(async (response) => {
      await axios.put(response.data.uploadURL, file, Object.assign({}, { headers: { 'Content-Type': file.type }})).then((response) => response).catch((error) => Promise.reject(error));      
      return  Object.assign({}, {public_url : response.data.public_url, s3_file_key: response.data.s3_file_key, file_name: file_name});
  }).catch((error) => Promise.reject(error));
}

/**
 * @param {string} file_name 
 * @param {string} content_type 
 */

export async function s3BucketCustomTemplate(file_name, content_type) {
  return await api.post(`/rest/practice-template/upload-url`, Object.assign({}, { file_name, content_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * @param {string} file_name 
 * @param {string} content_type 
 * @param {string} practice_id 
 * @param {string} document_type 
 */

export async function s3BucketModifiedTemplate(file_name, content_type, practice_id, document_type) {
  return await api.post(`/rest/practice-template/upload-url`, Object.assign({}, { file_name, content_type, practice_id, document_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * @param {string} s3_file_key 
 */
export async function getTemplateSecureFormURL(s3_file_key) {
  return await api.post(`/rest/propound-public-url`, Object.assign({}, { s3_file_key })).then((response) => response.data && response.data.publicUrl).catch((error) => Promise.reject(error));
}

/**
 * @param {string} file_name 
 * @param {string} content_type 
 */
function getAttachDocuments(file_name, content_type) {
  return api.post(`/rest/propound-forms/upload-file`, Object.assign({}, { file_name, content_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  getSignature,
  getSecretSignature,
  uploadFile,
  getSecretURL,
  getSecureFormURL,
  s3BucketPrivate,
  s3BucketPublic,
  s3BucketCustomTemplate,
  s3BucketModifiedTemplate,
  getTemplateSecureFormURL,
  getAttachDocuments
};

/**
 * 
 * Subscriptions selectors
 * 
 */

import { createSelector } from 'reselect';


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    const selectDomain = () => (state) => state[name] || state['rest/document-extraction-progress'] || false;

    const selectRespondingSubscriptions = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.respondingSubscriptions || {},
    );
    
    const selectPropoundingSubscriptions = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.propoundingSubscriptions || {},
    );

    const selectSettingsDiscount = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.discountDetail || false,
    );
    
    const selectLicenseDetails = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.licenseDetails || false,
    );


    return {
        selectRespondingSubscriptions,
        selectPropoundingSubscriptions,
        selectSettingsDiscount,
        selectLicenseDetails
    }

}


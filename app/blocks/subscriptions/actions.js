/*
 *
 * Subscriptions actions
 *
 */

/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {


    const {
        LOAD_SUBSCRIPTIONS,
        LOAD_SUBSCRIPTIONS_SUCCESS,
        LOAD_SUBSCRIPTIONS_ERROR,
        CREATE_SUBSCRIPTIONS,
        CREATE_SUBSCRIPTIONS_SUCCESS,
        CREATE_SUBSCRIPTIONS_ERROR,
        UPDATE_SUBSCRIPTIONS,
        UPDATE_SUBSCRIPTIONS_SUCCESS,
        UPDATE_SUBSCRIPTIONS_ERROR,
        CANCEL_SUBSCRIPTIONS,
        CANCEL_SUBSCRIPTIONS_SUCCESS,
        CANCEL_SUBSCRIPTIONS_ERROR,
        SETTINGS_DISCOUNT_CODE,
        SETTINGS_DISCOUNT_CODE_SUCCESS,
        SETTINGS_DISCOUNT_CODE_ERROR,
        CREATE_PROPOUND_SUBSCRIPTIONS,
        CREATE_PROPOUND_SUBSCRIPTIONS_SUCCESS,
        CREATE_PROPOUND_SUBSCRIPTIONS_ERROR,
        SUBSCRIPTION_RENEWALS,
        SUBSCRIPTION_RENEWALS_SUCCESS,
        SUBSCRIPTION_RENEWALS_ERROR,
        ADD_MORE_LICENSE,
        ADD_MORE_LICENSE_SUCCESS,
        ADD_MORE_LICENSE_ERROR,
        ADD_MORE_LICENSE_VALIDATION,
        ADD_MORE_LICENSE_VALIDATION_SUCCESS,
        ADD_MORE_LICENSE_VALIDATION_ERROR,
        SUBSCRIPTION_LICENSE_VALIDATION,
        SUBSCRIPTION_LICENSE_VALIDATION_SUCCESS,
        SUBSCRIPTION_LICENSE_VALIDATION_ERROR
    } = constants;


    function loadSubscriptions() {
        return {
            type: LOAD_SUBSCRIPTIONS
        };
    }

    /**
     * 
     * @param {object} record 
     * @param {object} recordsMetaData 
     * @returns 
     */
    function loadSubscriptionsSuccess(record, recordsMetaData) {
        return {
            type: LOAD_SUBSCRIPTIONS_SUCCESS,
            record,
            recordsMetaData
        };
    }

    /**
     * @param {string} error 
     */
    function loadSubscriptionsError(error) {
        return {
            type: LOAD_SUBSCRIPTIONS_ERROR,
            error
        };
    }


   /**
    * 
    * @param {object} record 
    * @param {string} form 
    * @param {function} setShowForm 
    * @param {function} setFormLoader 
    * @returns 
    */
    function createSubscriptions(record, form, setShowForm, setFormLoader) {
        return {
            type: CREATE_SUBSCRIPTIONS,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * 
     * @param {string} success 
     *  
     */
    function createSubscriptionsSuccess(success) {
        return {
            type: CREATE_SUBSCRIPTIONS_SUCCESS,
            success
        };
    }

    /**
     * @param {string} error 
     */
    function createSubscriptionsError(error) {
        return {
            type: CREATE_SUBSCRIPTIONS_ERROR,
            error
        };
    }

    /**
     * 
     * @param {object} record 
     * @param {string} form 
     * @param {function} setShowForm 
     * @param {function} setFormLoader 
     * @returns 
     */
    function updateSubscriptions(record, form, setShowForm, setFormLoader) {
        return {
            type: UPDATE_SUBSCRIPTIONS,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * 
     *  @param {string} success 
     */
    function updateSubscriptionsSuccess(success) {
        return {
            type: UPDATE_SUBSCRIPTIONS_SUCCESS,
            success
        };
    }

    /**
     * @param {string} error 
     */
    function updateSubscriptionsError(error) {
        return {
            type: UPDATE_SUBSCRIPTIONS_ERROR,
            error
        };
    }

    /**
     * 
     * @param {object} record 
     * @param {string} form 
     * @param {function} setShowForm 
     * @param {function} setFormLoader 
     * @returns 
     */
    function cancelSubscriptions(record, form, setShowForm, setFormLoader) {
        return {
            type: CANCEL_SUBSCRIPTIONS,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * 
     * @param {string} success 
     *  
     */
    function cancelSubscriptionsSuccess(success) {
        return {
            type: CANCEL_SUBSCRIPTIONS_SUCCESS,
            success
        };
    }

    /**
     * @param {string} error 
     */
    function cancelSubscriptionsError(error) {
        return {
            type: CANCEL_SUBSCRIPTIONS_ERROR,
            error
        };
    }


    /**
     * 
     * @param {object} record 
     * @param {string} form 
     * @param {function} dialog 
     * @param {boolean} cardDetails 
     * @param {function} handleSubmit 
     * @returns 
     */
    function settingsDiscountCode(record, form, dialog, cardDetails, handleSubmit) {
        return {
            type: SETTINGS_DISCOUNT_CODE,
            record,
            form,
            dialog,
            cardDetails,
            handleSubmit
        }
    }
  
    /**
     * 
     * @param {string} error 
     * @returns 
     */
    function settingsDiscountCodeError(error) {
        return {
            type: SETTINGS_DISCOUNT_CODE_ERROR,
            error
        }
    }
  
    /**
     * 
     * @param {array} records 
     * @returns 
     */
    function settingsDiscountCodeSuccess(records) {
        return {
            type: SETTINGS_DISCOUNT_CODE_SUCCESS,
            records
        }
    }

    /**
     * 
     * @param {object} record 
     * @param {string} form 
     * @param {function} setShowForm 
     * @param {function} setFormLoader 
     * @returns 
     */
    function createPropoundSubscriptions(record, form, setShowForm, setFormLoader) {
        return {
            type: CREATE_PROPOUND_SUBSCRIPTIONS,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * 
     * @param {string} success 
     *  
     */
    function createPropoundSubscriptionsSuccess(success) {
        return {
            type: CREATE_PROPOUND_SUBSCRIPTIONS_SUCCESS,
            success
        };
    }

    /**
     * @param {string} error 
     */
    function createPropoundSubscriptionsError(error) {
        return {
            type: CREATE_PROPOUND_SUBSCRIPTIONS_ERROR,
            error
        };
    }

    /**
     * 
     * @param {object} record 
     * @param {function} setLoader 
     * @returns 
     */
    function subscriptionRenewals(record, setLoader) {
        return {
            type: SUBSCRIPTION_RENEWALS,
            record,
            setLoader
        };
    }

    /**
     * 
     * @param {string} success 
     *  
     */
    function subscriptionRenewalsSuccess(success) {
        return {
            type: SUBSCRIPTION_RENEWALS_SUCCESS,
            success
        };
    }

    /**
     * @param {string} error 
     */
    function subscriptionRenewalsError(error) {
        return {
            type: SUBSCRIPTION_RENEWALS_ERROR,
            error
        };
    }


    /**
     * 
     * @param {object} record 
     * @param {function} setLicenseForm 
     * @param {form} form 
     * @returns 
     */
    function addMoreLicense(record, setLicenseForm, form) {
        return {
            type: ADD_MORE_LICENSE,
            record,
            setLicenseForm,
            form
        }
    }
    
    /**
     * 
     * @param {string} success 
     * @returns 
     */
    function addMoreLicenseSuccess(success) {
        return {
            type: ADD_MORE_LICENSE_SUCCESS,
            success
        }
    }
    
    /**
     * 
     * @param {string} error 
     * @returns 
     */
    function addMoreLicenseError(error) {
        return {
            type: ADD_MORE_LICENSE_ERROR,
            error
        }
    }

    /**
     * 
     * @param {object} record 
     * @param {function} dialog 
     * @param {string} form 
     * @returns 
     */
    function addMoreLicenseValidation(record, dialog, form) {
        return {
            type: ADD_MORE_LICENSE_VALIDATION,
            record,
            dialog,
            form
        }
    }

    /**
     * 
     * @param {object} record 
     * @returns 
     */
    function addMoreLicenseValidationSuccess(record) {
        return {
            type: ADD_MORE_LICENSE_VALIDATION_SUCCESS,
            record
        }
    }

    /**
     * 
     * @param {string} error 
     * @returns 
     */
    function addMoreLicenseValidationError(error) {
        return {
            type: ADD_MORE_LICENSE_VALIDATION_ERROR,
            error
        }
    }
    
    /**
     * 
     * @param {object} record 
     * @param {string} form 
     * @param {function} dialog 
     * @param {string} cardDetails 
     * @param {string} handleSubmit 
     * @returns 
     */
    function subscriptionLicenseValidation(record, form, dialog, cardDetails, handleSubmit) {
        return {
            type: SUBSCRIPTION_LICENSE_VALIDATION,
            record,
            form,
            dialog,
            cardDetails, 
            handleSubmit
        }
    }

    /**
     * 
     * @param {Array} records
     * @returns 
     */
    function subscriptionLicenseValidationSuccess(records) {
        return {
            type: SUBSCRIPTION_LICENSE_VALIDATION_SUCCESS,
            records
        }
    }

    /**
     * 
     * @param {string} error 
     * @returns 
     */
    function subscriptionLicenseValidationError(error) {
        return {
            type: SUBSCRIPTION_LICENSE_VALIDATION_ERROR,
            error
        }
    }


    return {
        loadSubscriptions,
        loadSubscriptionsSuccess,
        loadSubscriptionsError,
        createSubscriptions,
        createSubscriptionsSuccess,
        createSubscriptionsError,
        updateSubscriptions,
        updateSubscriptionsSuccess,
        updateSubscriptionsError,
        cancelSubscriptions,
        cancelSubscriptionsSuccess,
        cancelSubscriptionsError,
        settingsDiscountCode,
        settingsDiscountCodeError,
        settingsDiscountCodeSuccess,
        createPropoundSubscriptions,
        createPropoundSubscriptionsSuccess,
        createPropoundSubscriptionsError,
        subscriptionRenewals,
        subscriptionRenewalsSuccess,
        subscriptionRenewalsError,
        addMoreLicense,
        addMoreLicenseSuccess,
        addMoreLicenseError,
        addMoreLicenseValidation,
        addMoreLicenseValidationSuccess,
        addMoreLicenseValidationError,
        subscriptionLicenseValidation,
        subscriptionLicenseValidationSuccess,
        subscriptionLicenseValidationError
    }
}
/*
 *
 *  Subscriptions sagas
 *
 */

import { call, take, put, race, all, delay, takeLatest, select } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { verifySession as verifySessionAction } from 'blocks/session/actions';
import lodash from 'lodash';
import store2 from 'store2';
import history from 'utils/history';
import { push } from 'react-router-redux';
import records from 'blocks/records';

const subscriptionYearlyRenewals = records('rest/subscription-yearly-renewal');
import { selectUser } from 'blocks/session/selectors';

import {
  DEFAULT_LOAD_SUBSCRIPTIONS_ERROR,
  DEFAULT_CREATE_SUBSCRIPTIONS_ERROR,
  DEFAULT_UPDATE_SUBSCRIPTIONS_ERROR,
  DEFAULT_CANCEL_SUBSCRIPTIONS_ERROR
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {

  const {
    LOAD_SUBSCRIPTIONS,
    CREATE_SUBSCRIPTIONS,
    UPDATE_SUBSCRIPTIONS,
    CANCEL_SUBSCRIPTIONS,
    SETTINGS_DISCOUNT_CODE,
    CREATE_PROPOUND_SUBSCRIPTIONS,
    SUBSCRIPTION_RENEWALS,
    ADD_MORE_LICENSE,
    ADD_MORE_LICENSE_VALIDATION,
    SUBSCRIPTION_LICENSE_VALIDATION
  } = constants;

  const {
    loadSubscriptions: loadSubscriptionsAction,
    loadSubscriptionsError,
    loadSubscriptionsSuccess,
    createSubscriptionsError,
    createSubscriptionsSuccess,
    updateSubscriptionsError,
    updateSubscriptionsSuccess,
    cancelSubscriptionsError,
    cancelSubscriptionsSuccess,
    settingsDiscountCodeError,
    settingsDiscountCodeSuccess,
    createPropoundSubscriptionsSuccess,
    createPropoundSubscriptionsError,
    subscriptionRenewalsSuccess,
    subscriptionRenewalsError,
    addMoreLicenseSuccess,
    addMoreLicenseError,
    addMoreLicenseValidationSuccess,
    addMoreLicenseValidationError,
    subscriptionLicenseValidationSuccess,
    subscriptionLicenseValidationError
  } = actions;

  const {
    loadSubscriptions,
    createSubscriptions,
    updateSubscriptions,
    cancelSubscriptions,
    loadSettingsPlan,
    settingsDiscountCode,
    createPropoundSubscriptions,
    subscriptionRenewals,
    createUserSubscriptions,
    updateUserSubscriptions,
    createPropoundUserSubscriptions,
    cancelPropoundUserSubscriptions,
    addMoreLicenses,
    addMoreLicensesValidation,
    subscriptionValidation,
    cancelUserBasedSubcription
  } = remotes;

  const {
    selectLicenseDetails
  } = selectors;


  function* loadSubscriptionsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { load } = yield race({
        load: take(LOAD_SUBSCRIPTIONS),
      });
      if (load) {
        try {
          const result = yield call(loadSubscriptions);
          let recordsMetaData = {};
          let respondingPlans = [];
          const user_info = yield select(selectUser());
          if (result) {
            const page = history && history.location && history.location.pathname && history.location.pathname === `/settings` ? 'settings' : history.location.pathname;
            const plansResult = yield call(loadSettingsPlan, page);

            const userBasedBillingPlans = [
              'users_responding_monthly_license_495', 
              'users_responding_yearly_license_5100', 
              'users_responding_monthly_license_89', 
              'users_responding_yearly_license_948', 
              'users_propounding_monthly_license_199', 
              'users_propounding_yearly_license_2149_20', 
              'users_propounding_monthly_license_39', 
              'users_propounding_yearly_license_420'
            ];
            const userRespondingLicensePlans = ['users_responding_monthly_license_89', 'users_responding_yearly_license_948'];
            
            const userPropoudingLicensePlans = ['users_propounding_monthly_license_39', 'users_propounding_yearly_license_420'];
            if(plansResult) {
              let plans = plansResult.filter(_ => !['responding_vip', 'propounding_vip'].includes(_.plan_type));
              const respondingCurrentPlan = plans && Array.isArray(plans) && plans.length > 0 && result && Array.isArray(result) && result.length > 0 && result[0] && result[0].plan_id && plans.find(e => e.plan_id === result[0].plan_id) || false;
              const propoundingCurrentPlan = plans && Array.isArray(plans) && plans.length > 0 && result && Array.isArray(result) && result.length > 0 && result[1] && result[1].plan_id && plans.find(e => e.plan_id === result[1].plan_id) || false;

              const userBasedBilling = user_info?.practiceDetails?.billing_type === 'limited_users_billing' ? true : false;
              let subscriptionRecords;
              if(userBasedBilling) {
                const userBasedRespondingPlans = ['free_trial', 'users_responding_monthly_license_495', 'users_responding_yearly_license_5100'];
                const userBasedPropoundingPlans = ['users_propounding_monthly_license_199', 'users_propounding_yearly_license_2149_20'];

                if (plans) {
                  const filterRespondingPlans = plans.filter(_ => userBasedRespondingPlans.includes(_.plan_type));
                  const filterPropoundingPlans = plans.filter(_ => userBasedPropoundingPlans.includes(_.plan_type));

                  respondingPlans = filterRespondingPlans?.map((r) => Object.assign({}, r, { label: r.name, value: r.plan_id, disabled: respondingCurrentPlan && respondingCurrentPlan.plan_type.includes('yearly') && r.plan_type.includes('monthly') ? true : false }));

                  const propoundingPlans = filterPropoundingPlans && Array.isArray(filterPropoundingPlans) && filterPropoundingPlans.length > 0 && lodash.orderBy(filterPropoundingPlans.filter(e => e.active && e.plan_category === 'propounding'), ['order'], ['asc']).map(r => Object.assign({}, r, { label: r.name, value: r.plan_id, disabled: propoundingCurrentPlan && propoundingCurrentPlan.plan_type.includes('yearly') && r.plan_type.includes('monthly') ? true : false }));

                  const respondingLicensePlans = plans?.filter(r => userRespondingLicensePlans.includes(r.plan_type));
                  const propoundingLicensePlans = plans?.filter(r => userPropoudingLicensePlans.includes(r.plan_type))
  
                  recordsMetaData = { respondingPlans, propoundingPlans, allRespondingPlans: respondingPlans, respondingLicensePlans, propoundingLicensePlans };
                }
              } else {
                const existing_user_new_plans = ['responding_monthly_349','responding_yearly_3490'];
                const existing_user_unlimited_plans = ['monthly', 'yearly'];
                const new_user_plans = ['pay_as_you_go', 'free_trial', 'responding_monthly_495', 'responding_yearly_5100'];

                plans = plans.filter(_  => !userBasedBillingPlans.includes(_.plan_type));
                
                const check_existing_user_plans = plans.filter(e => existing_user_new_plans.includes(e.plan_type));
                if(check_existing_user_plans && Array.isArray(check_existing_user_plans) && check_existing_user_plans.length == 2) {
  
                  const respondingFilteredPlans = plans && Array.isArray(plans) && plans.length > 0 && lodash.orderBy(plans.filter((e) => e.active && e.plan_category === 'responding'), ['order'], ['asc']);
                  const currentPlanType = respondingCurrentPlan && respondingCurrentPlan.plan_type || false;
                  
                  const modifiedRespondingPlan = respondingFilteredPlans && Array.isArray(respondingFilteredPlans) && respondingFilteredPlans.length > 0 && respondingFilteredPlans.reduce((acc, el) => {
                    
                    if (currentPlanType && currentPlanType === 'monthly' && !['yearly', 'responding_monthly_349'].includes(el.plan_type)) {
                      acc.push(el);
                    } else if (currentPlanType && currentPlanType === 'yearly' && !existing_user_new_plans.includes(el.plan_type)) {
                      acc.push(el);
                    } else if (currentPlanType && ['free_trial', 'pay_as_you_go', 'tek_as_you_go'].includes(currentPlanType) && !existing_user_unlimited_plans.includes(el.plan_type)) {
                      acc.push(el);
                    } else if (currentPlanType && existing_user_new_plans.includes(currentPlanType) && !existing_user_unlimited_plans.includes(el.plan_type)) {
                      acc.push(el);
                    } else if (!currentPlanType && !existing_user_unlimited_plans.includes(el.plan_type)) {
                      acc.push(el);
                    }
  
                    return acc;
                  }, []).map((r) => Object.assign({}, r, { label: r.name, value: r.plan_id, disabled: respondingCurrentPlan && respondingCurrentPlan.plan_type.includes('yearly') && r.plan_type.includes('monthly') ? true : false }));
  
                  respondingPlans = modifiedRespondingPlan;
                } else {
                  respondingPlans = plans && Array.isArray(plans) && plans.length > 0 && lodash.orderBy(plans.filter(e => e.active && e.plan_category === 'responding' && !existing_user_new_plans.includes(e.plan_type)), ['order'], ['asc']).map(r => Object.assign({}, r, { label: r.name, value: r.plan_id, disabled: respondingCurrentPlan && respondingCurrentPlan.plan_type.includes('yearly') && r.plan_type.includes('monthly') ? true : false }));
                }
  
                const propoundingPlans = plans && Array.isArray(plans) && plans.length > 0 && lodash.orderBy(plans.filter(e => e.active && e.plan_category === 'propounding'), ['order'], ['asc']).map(r => Object.assign({}, r, { label: r.name, value: r.plan_id, disabled: propoundingCurrentPlan && propoundingCurrentPlan.plan_type.includes('yearly') && r.plan_type.includes('monthly') ? true : false }));
  
                const allRespondingPlans = plans && Array.isArray(plans) && plans.length > 0 && lodash.orderBy(plans.filter(e => e.active && e.plan_category === 'responding'), ['order'], ['asc']).map(r => Object.assign({}, r, { label: r.name, value: r.plan_id, disabled: respondingCurrentPlan && respondingCurrentPlan.plan_type.includes('yearly') && r.plan_type.includes('monthly') ? true : false }));
  
                recordsMetaData = { respondingPlans, propoundingPlans, allRespondingPlans };
                subscriptionRecords = result && Array.isArray(result) && result.length > 0 && Object.assign({}, { respondingSubscriptions: result[0] ? Object.assign({}, {...result[0]}, { terms: false }) : {}, propoundingSubscriptions: result[1] ? Object.assign({}, {...result[1]}, { terms: false }) : { plan_id: false, terms: false, plan_category: 'propounding' } });
                
              }
              subscriptionRecords = result && Array.isArray(result) && result.length > 0 && Object.assign({}, { respondingSubscriptions: result[0] ? Object.assign({}, {...result[0]}, { terms: false }) : {}, propoundingSubscriptions: result[1] ? Object.assign({}, {...result[1]}, { terms: false }) : { plan_id: false, terms: false, plan_category: 'propounding' } });
              yield put(loadSubscriptionsSuccess(subscriptionRecords, recordsMetaData));
            }
          } else {
            yield put(loadSubscriptionsError(DEFAULT_LOAD_SUBSCRIPTIONS_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_LOAD_SUBSCRIPTIONS_ERROR;
          yield put(loadSubscriptionsError(Err));
        }
      }
    }
  }


  function* createSubscriptionsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { create } = yield race({
        create: take(CREATE_SUBSCRIPTIONS),
      });

      const { record, form, setShowForm, setFormLoader } = create || {};

      if (record) {
        yield put(startSubmit(form));
        try {
          const user_info = yield select(selectUser());
          let result;
          if (user_info?.practiceDetails?.billing_type === 'limited_users_billing') {
            result = yield call(createUserSubscriptions, record)
          } else {
            result = yield call(createSubscriptions, record);
          }
          const secret = store2.get('secret');
          const successMessage = record && record.plan_category && (record.plan_category === 'responding' ? 'Responding Plan Changed.' : record.plan_category === 'propounding' ? 'Propounding Plan Changed.' : 'Plan Changed.') || 'Plan Changed.';
          
          if (result) {
            yield call(setShowForm, false);
            yield call(setFormLoader, false);
            yield put(createSubscriptionsSuccess(successMessage));
            if(secret) {
              yield put(verifySessionAction(secret));
            }
            yield delay(2000);
            yield put(loadSubscriptionsAction());
            yield put(settingsDiscountCodeSuccess([]));
            yield put(stopSubmit(form));
          } else {
            yield put(createSubscriptionsError(DEFAULT_CREATE_SUBSCRIPTIONS_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CREATE_SUBSCRIPTIONS_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_SUBSCRIPTIONS_ERROR;
          yield put(createSubscriptionsError(Err && Err.includes('This customer has no attached payment source or default payment method.') && 'Please provide the credit card information on Credit Card Info settings and try again.' || Err));
          yield put(stopSubmit(form, { _error: Err && Err.includes('This customer has no attached payment source or default payment method.') && 'Please provide the credit card information on Credit Card Info settings and try again.' || Err }));
        } finally {
          yield call(setShowForm, false);
          yield call(setFormLoader, false);
        }
      }
    }
  }


  function* updateSubscriptionsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { update } = yield race({
        update: take(UPDATE_SUBSCRIPTIONS),
      });

      const { record, form, setShowForm, setFormLoader } = update || {};

      if (record) {
        yield put(startSubmit(form));
        try {
          const user_info = yield select(selectUser());
          let result;
          if (user_info?.practiceDetails?.billing_type === 'limited_users_billing') {
            const cancelSubscription = yield call(cancelUserBasedSubcription);
            if(cancelSubscription) {
              result = yield call(updateUserSubscriptions, record);
            }
          } else {
            result = yield call(updateSubscriptions, record);
          }
          const secret = store2.get('secret');
          const successMessage = record && record.plan_category && (record.plan_category === 'responding' ? 'Responding Plan Changed.' : record.plan_category === 'propounding' ? 'Propounding Plan Changed.' : 'Plan Changed.') || 'Plan Changed.';

          if (result) {
            yield call(setShowForm, false);
            yield call(setFormLoader, false);
            yield put(updateSubscriptionsSuccess(successMessage));
            if(secret) {
              yield put(verifySessionAction(secret));
            }
            yield delay(2000);
            yield put(loadSubscriptionsAction());
            yield put(settingsDiscountCodeSuccess([]));
            yield put(stopSubmit(form));
          } else {
            yield put(updateSubscriptionsError(DEFAULT_UPDATE_SUBSCRIPTIONS_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_SUBSCRIPTIONS_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_SUBSCRIPTIONS_ERROR;
          yield put(updateSubscriptionsError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        } finally {
          yield call(setShowForm, false);
          yield call(setFormLoader, false);
        }
      }
    }
  }


  function* cancelSubscriptionsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { cancel } = yield race({
        cancel: take(CANCEL_SUBSCRIPTIONS),
      });

      const { record, form, setShowForm, setFormLoader } = cancel || {};

      if (record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(cancelSubscriptions, record);

          if (result) {
            yield call(setShowForm, false);
            yield call(setFormLoader, false);
            yield put(cancelSubscriptionsSuccess('Plan Changed.'));
            yield delay(2000);
            yield put(loadSubscriptionsAction());
            yield put(stopSubmit(form));
          } else {
            yield put(cancelSubscriptionsError(DEFAULT_CANCEL_SUBSCRIPTIONS_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CANCEL_SUBSCRIPTIONS_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CANCEL_SUBSCRIPTIONS_ERROR;
          yield put(cancelSubscriptionsError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        } finally {
          yield call(setShowForm, false);
          yield call(setFormLoader, false);
        }
      }
    }
  }


  function* settingsDiscountCodeSaga() {
    yield takeLatest(SETTINGS_DISCOUNT_CODE, function* updater({ record, form, dialog, cardDetails, handleSubmit }) {
      if(record) {
        yield put(startSubmit(form));
        try {
          if(record && record.discount_code && record.discount_code !== '') {
            const res = yield call(settingsDiscountCode, Object.assign({}, { discount_code: record.discount_code, plan_id: record.plan_id, type: record.type }));
            if(res && Array.isArray(res)) {
              yield put(settingsDiscountCodeSuccess(res));
            }
          }
          if(dialog && cardDetails) {
            yield call(dialog, true);
          } else {
            yield call(handleSubmit);
          }
        } catch (error) {
          yield put(settingsDiscountCodeError('This promo code is invalid.'));
          yield put(stopSubmit(form, { _error: 'This promo code is invalid.' }));
        } finally {
          yield put(stopSubmit(form));
        }
      }
    })
  }

  function* createPropoundSubscriptionsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { create } = yield race({
        create: take(CREATE_PROPOUND_SUBSCRIPTIONS),
      });

      const { record, form, setShowForm, setFormLoader } = create || {};

      if (record) {
        yield put(startSubmit(form));
        try {
          const user_info = yield select(selectUser());
          let result;
          if (user_info?.practiceDetails?.billing_type === 'limited_users_billing') {
            result = yield call(createPropoundUserSubscriptions, record);
          } else {
            result = yield call(createPropoundSubscriptions, record);
          }
          const secret = store2.get('secret');
          const successMessage = record && record.plan_category && (record.plan_category === 'responding' ? 'Responding Plan Changed.' : record.plan_category === 'propounding' ? 'Propounding Plan Changed.' : 'Plan Changed.') || 'Plan Changed.';
          
          if (result) {
            yield call(setShowForm, false);
            yield call(setFormLoader, false);
            yield put(createPropoundSubscriptionsSuccess(successMessage));
            if(secret) {
              yield put(verifySessionAction(secret));
            }
            yield delay(2000);
            yield put(loadSubscriptionsAction());
            yield put(settingsDiscountCodeSuccess([]));
            yield put(stopSubmit(form));
          } else {
            yield put(createPropoundSubscriptionsError(DEFAULT_CREATE_SUBSCRIPTIONS_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CREATE_SUBSCRIPTIONS_ERROR }));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_SUBSCRIPTIONS_ERROR;
          yield put(createPropoundSubscriptionsError(Err && Err.includes('This customer has no attached payment source or default payment method.') && 'Please provide the credit card information on Credit Card Info settings and try again.' || Err));
          yield put(stopSubmit(form, { _error: Err && Err.includes('This customer has no attached payment source or default payment method.') && 'Please provide the credit card information on Credit Card Info settings and try again.' || Err }));
        } finally {
          yield call(setShowForm, false);
          yield call(setFormLoader, false);
        }
      }
    }
  }

  function* subscriptionRenewalsSaga() {
    yield takeLatest(SUBSCRIPTION_RENEWALS, function* updater({ record, setLoader }) {
      if(record) {
        try {
          if(record) {
            const result = yield call(subscriptionRenewals, Object.assign({}, record, {practice_id: record.id}));
            if(result) {
              yield call(setLoader, false);
              yield put(subscriptionRenewalsSuccess('Subscriptions updated successfully'));
              yield delay(2000);
              yield put(subscriptionYearlyRenewals.actions.loadRecords(true));
              yield put(push({ pathname: `/renewalCategories/rest/subscription-yearly-renewal`, state: history.location.state }));
            } else {
              yield put(subscriptionRenewalsError('Failed to Update the Subscriptions'));    
            }
          }
        } catch (error) {
          yield put(subscriptionRenewalsError('Failed to Update the Subscriptions'));
        } finally {
          yield call(setLoader, false);
        }
      }
    })
  }
  
  function* addMoreLicenseSaga() {
    yield takeLatest(ADD_MORE_LICENSE, function* updater({ record, setLicenseForm, form }) {
      if(record) {
        yield put(startSubmit(form));
        try {
          if(record) {
            const licenseCount = yield select(selectLicenseDetails());
            const submitRecord = Object.assign({}, { ...record });
            if(licenseCount?.responding) {
              if(licenseCount?.responding?.prorated) {
                submitRecord.responding = Object.assign({}, { license_total_price: licenseCount?.responding?.prorated?.discount_license_total_price });
              } else {
                submitRecord.responding = Object.assign({}, { license_total_price: licenseCount?.responding?.total_license_price });
              }
            }

            if(licenseCount?.propounding) {
              if(licenseCount?.propounding?.prorated) {
                submitRecord.propounding = Object.assign({}, { license_total_price: licenseCount?.propounding?.prorated?.discount_license_total_price });
              } else {
                submitRecord.propounding = Object.assign({}, { license_total_price: licenseCount?.propounding?.total_license_price });
              }
            }

            const result = yield call(addMoreLicenses, Object.assign({}, submitRecord));
            if(result) {
              yield call(setLicenseForm, false);
              yield put(addMoreLicenseSuccess('License successfully purchased.'));
              
              yield delay(2000);
              const secret = store2.get('secret');
              if(secret) {
                yield put(verifySessionAction(secret));
              }
            } else {
              yield put(stopSubmit(form));
              yield put(addMoreLicenseError('Failed to purchase license.'));
            }
          }
        } catch (error) {
          yield put(stopSubmit(form));
          yield put(addMoreLicenseError('Failed to purchase license.'));
        } finally {
          yield put(addMoreLicenseValidationSuccess({}));
        }
      }
    })
  }

  function* addMoreLicenseValidationSaga() {
    yield takeLatest(ADD_MORE_LICENSE_VALIDATION, function* updater({ record, dialog, form }) {
      if(record) {
        yield put(startSubmit(form));
        yield put(addMoreLicenseValidationSuccess({}));
        try {
          const result = yield call(addMoreLicensesValidation, record);
          if(result) {
            yield put(addMoreLicenseValidationSuccess(result));
          }
          yield put(stopSubmit(form));
          if(dialog) {
            yield call(dialog, true);
          }
        } catch (error) {
          yield put(addMoreLicenseValidationError('Failed to purchase user license'));
          yield put(stopSubmit(form));
        }
      }
    })
  }

  function* subscriptionLicenseValidationSaga() {
    yield takeLatest(SUBSCRIPTION_LICENSE_VALIDATION, function* updater({ record, form, dialog, cardDetails, handleSubmit }) {
      if(record) {
        yield put(startSubmit(form));
        try {
          const result = yield call(subscriptionValidation, record);
          if(result) {
            yield put(subscriptionLicenseValidationSuccess(result));
          }
          yield put(stopSubmit(form));
          if(dialog && cardDetails) {
            yield call(dialog, true);
          } else {
            yield call(handleSubmit);
          }
        } catch (error) {
          const Err = error?.response?.data?.error && typeof error?.response?.data?.error === 'string' && error?.response?.data?.error?.toLowerCase().indexOf ('invalid discount code') > -1 ? 'This promo code is invalid.' : 'Failed to fetch the plan price';
          yield put(subscriptionLicenseValidationError(Err));
          yield put(stopSubmit(form));
        }
      }
    })
  }

  return function* rootSaga() {
    yield all([
      loadSubscriptionsSaga(),
      createSubscriptionsSaga(),
      updateSubscriptionsSaga(),
      cancelSubscriptionsSaga(),
      settingsDiscountCodeSaga(),
      createPropoundSubscriptionsSaga(),
      subscriptionRenewalsSaga(),
      addMoreLicenseSaga(),
      addMoreLicenseValidationSaga(),
      subscriptionLicenseValidationSaga()
    ]);
  }

}

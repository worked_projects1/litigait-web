/*
 *
 *  subscriptions
 *
 */

import actions from './actions';
import constants from './constants';
import sagas from './sagas';
import selectors from './selectors';
import remotes from './remotes';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function(name) {

  const actualConstants = constants(name);
  const actualActions = actions(actualConstants);
  const actualRemotes = remotes(name);
  const actualSelectors = selectors(name);
  let actualSagas = sagas(actualConstants, actualActions, actualRemotes, actualSelectors, name);

  return {
    actions: actualActions,
    constants: actualConstants,
    saga: actualSagas,
    selectors: actualSelectors,
    remotes: actualRemotes
  };

}
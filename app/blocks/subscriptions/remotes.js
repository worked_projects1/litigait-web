/*
 *
 *  Subscriptions remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    function loadSubscriptions() {
        return api.get(`/rest/subscriptions`).then((response) => response.data && response.data.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function createSubscriptions(record) {
        return api.post(`/rest/subscriptions`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function updateSubscriptions(record) {
        return api.put(`/rest/subscriptions/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function cancelSubscriptions(record) {
        return api.put(`/rest/subscriptions/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {string} page 
     */
    function loadSettingsPlan(page) {
        return api
        .get(`/rest/plans?page=${page}`)
        .then((response) => response.data)
        .catch((error) => Promise.reject(error));
    }

  
    function settingsDiscountCode(record) {
        return api
          .put(`/rest/validate/discount-code?code=${record.discount_code}&type=${record.type}`, record)
          .then(response => response.data)
          .catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
    function createPropoundSubscriptions(record) {
        return api.post(`/rest/subscriptions/propounding`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record
     */
     function subscriptionRenewals(record) {
        return api.post(`/rest/subscription-yearly-renewal`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function createUserSubscriptions(record) {
        return api.post(`/rest/users-license/subscription`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    } 
    
    /**
     * @param {object} record 
     */
    function updateUserSubscriptions(record) {
        return api.put(`/rest/users-license/subscription/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    } 
    
    /**
     * @param {object} record 
     */
    function createPropoundUserSubscriptions(record) {
        return api.post(`/rest/users-license/subscription/propounding`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function cancelPropoundUserSubscriptions(record) {
        return api.put(`/rest/users-license/subscription/cancel-propounding`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function addMoreLicenses(record) {
        return api.post(`/rest/users/license-purchase`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    
    /**
     * @param {object} record 
     */
    function addMoreLicensesValidation(record) {
        return api.post(`/rest/users-license/billing`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function subscriptionValidation(record) {
        return api.post(`/rest/users-license/subscripton-billing`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function cancelUserBasedSubcription() {
        return api.post(`/rest/users-license/subscription-cancel`).then((response => response.data)).catch((error) => Promise.reject(error));
    }

    return {
        loadSubscriptions,
        createSubscriptions,
        updateSubscriptions,
        cancelSubscriptions,
        loadSettingsPlan,
        settingsDiscountCode,
        createPropoundSubscriptions,
        subscriptionRenewals,
        createUserSubscriptions,
        updateUserSubscriptions,
        createPropoundUserSubscriptions,
        cancelPropoundUserSubscriptions,
        addMoreLicenses,
        addMoreLicensesValidation,
        subscriptionValidation,
        cancelUserBasedSubcription
    }
}


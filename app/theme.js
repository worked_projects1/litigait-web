

import { createMuiTheme } from '@material-ui/core/styles';




const theme = createMuiTheme({
    button: {
        textTransform: 'none'
    },
    typography: {
        "fontFamily": "Avenir-Regular",
        "fontSize": 14,
        "fontWeightLight": 300,
        "fontWeightRegular": 400,
        "fontWeightMedium": 500,
        h5: {
            "fontFamily": "Avenir-Bold",
            "textTransform": "Capitalize"
        }
    },
    palette: {
        cardElementColor: {
            main: '#2ca01c'
        },
        cardBorderColor: {
            main: '#89d380',
        },
        cardBorderColor2: {
            main: '#248b16',
        },
        cardBackgroundColor: {
            main: '#edffee'
        },
        cardBackgroundColor2: {
            main: '#248b16'
        },
    }
});



export default theme;

/**
 * 
 * Operator
 * 
 */


import schema from './schema';
import ResourceCategories from './resourceCategories';
import ResetLoginAttempts from 'components/ResetLoginAttempts';
import CreatePracticeUserBtn  from 'components/CreatePracticeUserBtn';
import CustomCreateUserBtn from 'components/CustomCreateUserBtn';
import { selectUser, selectError as sessionError, selectSuccess as sessionSuccess } from 'blocks/session/selectors';

const filterColumns = schema().filterColumns();
const objectionColumns = schema().adminObjections().columns;
const practicesColumns = schema().practices().columns;
const globalSettingsColumns = schema().globalSettings;
const subscriptionsPlansColumns = schema().plans().columns;
const federalObjectionsColumns = schema().federalObjections().columns;
const adminEsquiretekUsersColumns = schema().adminEsquiretekUsers().columns;
const adminPracticeUsersColumns = schema().adminPracticeUsers().columns;
const globalOtherSettingsColumns = schema().globalOtherSettings;
const adminPracticeConfirmationMessage = schema().adminPracticeUsers().customDescription;

export default function (simpleLazyLoadedRoute) {

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Dashboard',
                path: '/',
                route: true
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `practices`,
            name: 'practices',
            pageName: 'practices',
            require: ['RecordsPage', 'practices'],
            id: 'Practices',
            data: {
                path: '/practices',
                title: 'Practices',
                icon: 'Practice'
            },
            container: function RecordsPage(practices) {
                return this('practices', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors, filterColumns, false, false, false, true)
            }
        }),
        ResourceCategories('userCategories', 'userCategories', 'Users', 'Users', false, 'AdminUsers', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/esquiretek/users`,
                name: 'esquireTekUsers',
                pageName: 'esquireTekUsers',
                queryParams: 'admin',
                require: ['RecordsPage', 'rest/esquiretek/users'],
                chunk: ['users'],
                data: {
                    path: '/rest/esquiretek/users',
                    title: 'EsquireTek Users',
                    icon: 'Users'
                },
                container: function RecordsPage(esquireTekUsers) {
                    return this('esquireTekUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, adminEsquiretekUsersColumns, esquireTekUsers.actions, esquireTekUsers.selectors, filterColumns, true, true, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'esquireTekUsers.create',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['CreateRecordPage', 'rest/esquiretek/users'],
                        container: function CreateRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, schema().adminEsquiretekUsers, esquireTekUsers.actions, esquireTekUsers.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'esquireTekUsers.edit',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['EditRecordPage', 'rest/esquiretek/users'],
                        container: function EditRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, schema().adminEsquiretekUsers, esquireTekUsers.actions, esquireTekUsers.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'esquireTekUsers.view',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['ViewRecordPage', 'rest/esquiretek/users'],
                        chunk: ['users'],
                        container: function ViewRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, adminEsquiretekUsersColumns, esquireTekUsers.actions, esquireTekUsers.selectors, ResetLoginAttempts)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/practice/users`,
                name: 'practiceUsers',
                pageName: 'practiceUsers',
                queryParams: 'practice',
                require: ['RecordsPage', 'rest/practice/users'],
                chunk: ['users'],
                data: {
                    path: '/rest/practice/users',
                    title: 'Practice Users',
                    icon: 'Users'
                },
                container: function RecordsPage(practiceUsers) {
                    return this('practiceUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, filterColumns, true, true, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'practiceUsers.create',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['CreateRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function CreateRecordPage(practiceUsers) {
                            return this('practiceUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors, false, false, false, CustomCreateUserBtn, adminPracticeConfirmationMessage, false, false, CreatePracticeUserBtn)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'practiceUsers.edit',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['EditRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function EditRecordPage(practiceUsers) {
                            return this('practiceUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors, false, CreatePracticeUserBtn)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'practiceUsers.view',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['ViewRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function ViewRecordPage(practiceUsers) {
                            return this('practiceUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, ResetLoginAttempts)
                        }
                    })
                ]
            })
        ]),
        ResourceCategories('objectionsCategories', 'objectionsCategories', 'Objections', 'Objections', false, 'AdminObjection', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/admin-objections`,
                name: 'objections',
                pageName: 'objections',
                require: ['RecordsPage', 'rest/admin-objections'],
                data: {
                    path: '/rest/admin-objections',
                    title: 'Objections',
                    icon: 'Objections'
                },
                container: function RecordsPage(objections) {
                    return this('objections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/admin-objections`, objectionColumns, objections.actions, objections.selectors, filterColumns)
                }
            }),
            simpleLazyLoadedRoute({
                path: `federal/admin-objections`,
                name: 'federalObjections',
                pageName: 'federalObjections',
                require: ['ObjectionsFederalPage', 'federal/admin-objections'],
                data: {
                    path: '/federal/admin-objections',
                    title: 'Federal',
                    icon: 'Objections'
                },
                container: function ObjectionsFederalPage(objections) {
                    return this('federalObjections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/admin-objections`, federalObjectionsColumns, objections.actions, Object.assign({}, objections.selectors, { selectUser, sessionError, sessionSuccess }), filterColumns)
                }
            })
        ]),
        simpleLazyLoadedRoute({
            path: `globalSettings`,
            name: 'globalSettings',
            pageName: 'globalSettings',  
            require: ['GlobalSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'GlobalSettings',
            data: {
                path: '/globalSettings',
                title: 'Pricing',
                icon: 'Pricing'
            },
            container: function GlobalSettingsPage(settings) {
                return this('settings', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalSettingsColumns, settings.actions, settings.selectors, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:tab`,
                    name: 'globalSettings.view',
                    pageName: 'globalSettings',
                    require: ['ViewGlobalSettingsPage', 'globalSettings'],
                    chunk: ['globalSettings'],
                    container: function GlobalSettingsPage(settings) {
                        return this('settings.view', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalSettingsColumns, settings.actions, settings.selectors, false)
                    },
                    childRoutes: [
                        simpleLazyLoadedRoute({
                            path: `rest/plans`,
                            name: 'plans',
                            pageName: 'plans',
                            require: ['RecordsPage', 'rest/plans'],
                            container: function RecordsPage(plans) {
                                return this('plans', `${process.env.PUBLIC_PATH || ''}/globalSettings/0/rest/plans`, subscriptionsPlansColumns, plans.actions, plans.selectors, filterColumns)
                            }
                        })
                    ]
                })
            ]
        }),
        simpleLazyLoadedRoute({
            path: `otherSettings`,
            name: 'otherSettings',
            pageName: 'globalSettings',
            require: ['ViewGlobalOtherSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'OtherSettings',
            data: {
                path: '/otherSettings',
                title: 'Settings',
                icon: 'Settings'
            },
            container: function ViewGlobalOtherSettingsPage(settings) {
                return this('otherSettings', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/otherSettings`, globalOtherSettingsColumns, settings.actions, Object.assign({}, { ...settings.selectors, selectUser }), false)
            }
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}

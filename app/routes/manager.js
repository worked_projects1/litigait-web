
/**
 * 
 * Manager
 * 
 */


import schema from './schema';

import ResetLoginAttempts from 'components/ResetLoginAttempts';
import CancelSubscription  from 'components/CancelSubscription';
import CreatePracticeUserBtn  from 'components/CreatePracticeUserBtn';
import CustomCreateUserBtn from 'components/CustomCreateUserBtn';
import ResourceCategories from './resourceCategories';

import { selectUser, selectError as sessionError, selectSuccess as sessionSuccess } from 'blocks/session/selectors';

const filterColumns = schema().filterColumns();
const objectionColumns = schema().adminObjections().columns;
const practicesColumns = schema().practices().columns;
const practicesSettingsColumns = schema().practices().settings;
const medicalHistoryColumns = schema().adminMedicalHistory().columns;
const ordersColumns = schema().adminOrders().columns;
const supportRequestColumns = schema().supportRequest().columns;
const globalSettingsColumns = schema().globalSettings;
const subscriptionsPlansColumns = schema().plans().columns;
const subscriptionsHistoryColumns = schema().adminSubscriptionsHistory().columns;
const federalObjectionsColumns = schema().federalObjections().columns;
const adminEsquiretekUsersColumns = schema().adminEsquiretekUsers().columns;
const adminPracticeUsersColumns = schema().adminPracticeUsers().columns;
const feeWaiverColumns = schema().feeWaiver().columns;
const feeWaiverCustomNote = schema().feeWaiver().customNote();
const excelExportColumns = schema().excelExport().columns;
const globalOtherSettingsColumns = schema().globalOtherSettings;
const propoundSupportRequestColumns = schema().propoundSupportRequest().columns;
const discountCodeColumns = schema().discountCode().columns;
const discountCodeNote = schema().discountCode().customNote();
const discountCodeConfirmMessage = schema().discountCode().customConfirmMessage;
const extractionSupportRequestColumn = schema().extractionSupportRequest().columns;
const adminPracticeConfirmationMessage = schema().adminPracticeUsers().customDescription;
const missedQuestionsFormField = schema().missedQuestionsFormField().columns;
const deleteQuestionsWithHashFormField = schema().deleteQuestionsWithHashForm().columns;


export default function (simpleLazyLoadedRoute) {

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Home',
                path: '/',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'dashboard',
            pageName: 'analytics',
            exact: true,
            id: 'Dashboard',
            data: {
                title: 'Dashboard',
                path: '/dashboard',
                icon: 'Dashboard',
                filterColumns: filterColumns
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `practices`,
            name: 'practices',
            pageName: 'practices',
            require: ['RecordsPage', 'practices'],
            chunk: ['practices'],
            id: 'Practices',
            data: {
                path: '/practices',
                title: 'Practices',
                icon: 'Practice'
            },
            container: function RecordsPage(practices) {
                return this('practices', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors, filterColumns, true, true, false, true, false, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'practices.create',
                    pageName: 'practices',
                    require: ['CreateRecordPage', 'practices'],
                    container: function CreateRecordPage(practices) {
                        return this('practices.create', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'practices.edit',
                    pageName: 'practices',
                    require: ['EditRecordPage', 'practices'],
                    container: function EditRecordPage(practices) {
                        return this('practices.edit', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/settings`,
                    name: 'practices.settings',
                    pageName: 'practices',
                    require: ['ViewPracticeSettingsPage', 'practices'],
                    chunk: ['practices'],
                    container: function ViewPracticeSettingsPage(practices) {
                        return this('practices.settings', `${process.env.PUBLIC_PATH || ''}/practices`, practicesSettingsColumns, practices.actions, practices.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'practices.view',
                    pageName: 'practices',
                    require: ['ViewPracticePage', 'practices'],
                    chunk: ['practices'],
                    container: function ViewPracticePage(practices) {
                        return this('practices.view', `${process.env.PUBLIC_PATH || ''}/practices`, schema().practices, practices.actions, practices.selectors, CancelSubscription)
                    }
                })
            ]
        }),
        ResourceCategories('orderCategories', 'orderCategories', 'Orders', 'Orders', false, 'AdminOrder', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/admin/orders`,
                name: 'orders',
                pageName: 'orders',
                require: ['RecordsPage', 'rest/admin/orders'],
                data: {
                    path: '/rest/admin/orders',
                    title: 'Orders'
                },
                container: function RecordsPage(orders) {
                    return this('adminOrders', `${process.env.PUBLIC_PATH || ''}/orderCategories/rest/admin/orders`, ordersColumns, orders.actions, orders.selectors, filterColumns, false, false, false, true)
                }
            }),
            simpleLazyLoadedRoute({
                path: `rest/subscriptions/admin/history`,
                name: 'subscriptions',
                pageName: 'subscriptions',
                require: ['RecordsPage', 'rest/subscriptions/admin/history'],
                data: {
                    path: 'rest/subscriptions/admin/history',
                    title: 'Subscriptions'
                },
                container: function RecordsPage(subscriptions) {
                    return this('subscriptions', `${process.env.PUBLIC_PATH || ''}/orderCategories/rest/subscriptions/admin/history`, subscriptionsHistoryColumns, subscriptions.actions, subscriptions.selectors, filterColumns, false, false, false, true, false, true)
                }
            })
        ]),
        ResourceCategories('userCategories', 'userCategories', 'Users', 'Users', false, 'AdminUsers', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/esquiretek/users`,
                name: 'esquireTekUsers',
                pageName: 'esquireTekUsers',
                queryParams: 'admin',
                require: ['RecordsPage', 'rest/esquiretek/users'],
                chunk: ['users'],
                data: {
                    path: '/rest/esquiretek/users',
                    title: 'EsquireTek Users',
                    icon: 'Users'
                },
                container: function RecordsPage(esquireTekUsers) {
                    return this('esquireTekUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, adminEsquiretekUsersColumns, esquireTekUsers.actions, esquireTekUsers.selectors, filterColumns, true, true, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'esquireTekUsers.create',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['CreateRecordPage', 'rest/esquiretek/users'],
                        container: function CreateRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, schema().adminEsquiretekUsers, esquireTekUsers.actions, esquireTekUsers.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'esquireTekUsers.edit',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['EditRecordPage', 'rest/esquiretek/users'],
                        container: function EditRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, schema().adminEsquiretekUsers, esquireTekUsers.actions, esquireTekUsers.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'esquireTekUsers.view',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['ViewRecordPage', 'rest/esquiretek/users'],
                        chunk: ['users'],
                        container: function ViewRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, adminEsquiretekUsersColumns, esquireTekUsers.actions, esquireTekUsers.selectors, ResetLoginAttempts)
                        }
                    })
                ]

            }),
            simpleLazyLoadedRoute({
                path: `rest/practice/users`,
                name: 'practiceUsers',
                pageName: 'practiceUsers',
                queryParams: 'practice',
                require: ['RecordsPage', 'rest/practice/users'],
                chunk: ['users'],
                data: {
                    path: '/rest/practice/users',
                    title: 'Practice Users',
                    icon: 'Users'
                },
                container: function RecordsPage(practiceUsers) {
                    return this('practiceUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, filterColumns, true, true, false, true, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'practiceUsers.create',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['CreateRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function CreateRecordPage(practiceUsers) {
                            return this('practiceUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors, false, false, false, CustomCreateUserBtn, adminPracticeConfirmationMessage, false, false, CreatePracticeUserBtn)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'practiceUsers.edit',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['EditRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function EditRecordPage(practiceUsers) {
                            return this('practiceUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors, false, CreatePracticeUserBtn)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'practiceUsers.view',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['ViewRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function ViewRecordPage(practiceUsers) {
                            return this('practiceUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, ResetLoginAttempts)
                        }
                    })
                ]

            })
        ]),
        simpleLazyLoadedRoute({
            path: `rest/medical-history-summery`,
            name: 'medicalHistory',
            pageName: 'medicalHistory',
            require: ['MedicalPage', 'rest/medical-history-summery'],
            id: 'MedicalHistory',
            data: {
                path: '/rest/medical-history-summery',
                title: 'Medical History',
                icon: 'MedicalHistory'
            },
            container: function MedicalPage(medicalHistory) {
                return this('medicalHistory', `${process.env.PUBLIC_PATH || ''}/rest/medical-history-summery`, Object.assign({}, { medicalHistoryColumns, excelExportColumns }), medicalHistory.actions, medicalHistory.selectors, filterColumns, false, true)
            }
        }),
        // simpleLazyLoadedRoute({
        //     path: `rest/help-request`,
        //     name: 'supportRequest',
        //     pageName: 'supportRequest',
        //     require: ['SupportRequestPage', 'rest/help-request'],
        //     chunk: ['cases'],
        //     data: {
        //         path: '/rest/help-request',
        //         title: 'Support Request',
        //         icon: 'SupportRequest'
        //     },
        //     container: function SupportRequestPage(supportRequest) {
        //         return this('supportRequest', `${process.env.PUBLIC_PATH || ''}/rest/help-request`, supportRequestColumns, supportRequest.actions, supportRequest.selectors, filterColumns)
        //     }
        // }),
        ResourceCategories('supportCategories', 'supportCategories', 'Support Request', 'SupportRequest', false, 'SupportRequest', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/help-request`,
                name: 'supportRequest',
                pageName: 'supportRequest',
                require: ['SupportRequestPage', 'rest/help-request'],
                chunk: ['cases'],
                data: {
                    path: '/rest/help-request',
                    title: 'Responding',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(supportRequest) {
                    return this('supportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/help-request`, supportRequestColumns, supportRequest.actions, supportRequest.selectors, filterColumns)
                }
            }),
            simpleLazyLoadedRoute({
                path: `rest/propound-support`,
                name: 'propoundSupportRequest',
                pageName: 'propoundSupportRequest',
                require: ['SupportRequestPage', 'rest/propound-support'],
                chunk: ['propound'],
                data: {
                    path: '/rest/propound-support',
                    title: 'Propounding',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(propoundSupportRequest) {
                    return this('propoundSupportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/propound-support`, propoundSupportRequestColumns, propoundSupportRequest.actions, propoundSupportRequest.selectors, filterColumns)
                }
            }),
            simpleLazyLoadedRoute({
                path: 'rest/document-extraction-support',
                name: 'extractionSupportRequest',
                pageName: 'extractionSupportRequest',
                require: ['SupportRequestPage', 'rest/document-extraction-support'],
                chunk: ['quickCreate'],
                data: {
                    path: '/rest/document-extraction-support',
                    title: 'Quick Create',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(extractionSupportRequest) {
                    return this('extractionSupportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/document-extraction-support`, extractionSupportRequestColumn, extractionSupportRequest.actions, extractionSupportRequest.selectors, filterColumns)
                }
            })
        ]),
        simpleLazyLoadedRoute({
            path: `rest/practice-missed-questions-file`,
            name: 'missedQuestionFile',
            pageName: 'missedQuestionFile',
            require: ['MissedQuestionsSupportPage', 'rest/practice-missed-questions-file'],
            chunk: ['cases'],
            id: 'MissedQuestionFile',
            data: {
                path: '/rest/practice-missed-questions-file',
                title: 'Cache Files',
                icon: 'HashFile'
            },
            container: function MissedQuestionsSupportPage(missedQuestionFile) {
                return this('missedQuestionFile', `${process.env.PUBLIC_PATH || ''}/rest/questions-file`, Object.assign({}, { missedQuestionsFormField, deleteQuestionsWithHashFormField }), missedQuestionFile.actions, missedQuestionFile.selectors)
            },         
        }),
        ResourceCategories('objectionsCategories', 'objectionsCategories', 'Objections', 'Objections', false, 'AdminObjections', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/admin-objections`,
                name: 'objections',
                pageName: 'objections',
                require: ['RecordsPage', 'rest/admin-objections'],
                data: {
                    path: '/rest/admin-objections',
                    title: 'Objections',
                    icon: 'Objections'
                },
                container: function RecordsPage(objections) {
                    return this('objections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/admin-objections`, objectionColumns, objections.actions, objections.selectors, filterColumns)
                },
            }),
            simpleLazyLoadedRoute({
                path: `federal/admin-objections`,
                name: 'federalObjections',
                pageName: 'federalObjections',
                require: ['ObjectionsFederalPage', 'federal/admin-objections'],
                data: {
                    path: '/federal/admin-objections',
                    title: 'Federal',
                    icon: 'Objections'
                },
                container: function ObjectionsFederalPage(objections) {
                    return this('federalObjections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/admin-objections`, federalObjectionsColumns, objections.actions, Object.assign({}, objections.selectors, { selectUser, sessionError, sessionSuccess }), filterColumns)
                }
            })
        ]),
        simpleLazyLoadedRoute({
            path: `globalSettings`,
            name: 'globalSettings',
            pageName: 'globalSettings',
            require: ['GlobalSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'GlobalSettings',
            data: {
                path: '/globalSettings',
                title: 'Pricing',
                icon: 'Pricing'
            },
            container: function GlobalSettingsPage(settings) {
                return this('settings', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalSettingsColumns, settings.actions, settings.selectors, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:tab`,
                    name: 'globalSettings.view',
                    pageName: 'globalSettings',
                    require: ['ViewGlobalSettingsPage', 'globalSettings'],
                    chunk: ['globalSettings'],
                    container: function GlobalSettingsPage(settings) {
                        return this('settings.view', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalSettingsColumns, settings.actions, settings.selectors, false)
                    },
                    childRoutes: [
                        simpleLazyLoadedRoute({
                            path: `rest/plans`,
                            name: 'plans',
                            pageName: 'plans',
                            require: ['RecordsPage', 'rest/plans'],
                            container: function RecordsPage(plans) {
                                return this('plans', `${process.env.PUBLIC_PATH || ''}/globalSettings/0/rest/plans`, subscriptionsPlansColumns, plans.actions, plans.selectors, filterColumns)
                            }
                        }),
                        simpleLazyLoadedRoute({
                            path: `rest/fee-waiver`,
                            name: 'feeWaiver',
                            pageName: 'feeWaiver',
                            require: ['RecordsPage', 'rest/fee-waiver'],
                            container: function RecordsPage(feeWaiver) {
                                return this('feeWaiver', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/rest/fee-waiver`, feeWaiverColumns, feeWaiver.actions, feeWaiver.selectors, filterColumns, true, true, feeWaiverCustomNote)
                            },
                            childRoutes: [
                                simpleLazyLoadedRoute({
                                    path: `create`,
                                    name: 'feeWaiver.create',
                                    pageName: 'feeWaiver',
                                    require: ['CreateRecordPage', 'rest/fee-waiver'],
                                    container: function CreateRecordPage(feeWaiver) {
                                        return this('feeWaiver.create', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/rest/fee-waiver`, feeWaiverColumns, feeWaiver.actions, feeWaiver.selectors)
                                    }
                                }),
                                simpleLazyLoadedRoute({
                                    path: `:id`,
                                    name: 'feeWaiver.view',
                                    pageName: 'feeWaiver',
                                    require: ['ViewRecordPage', 'rest/fee-waiver'],
                                    container: function ViewRecordPage(feeWaiver) {
                                        return this('feeWaiver.view', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/rest/fee-waiver`, feeWaiverColumns, feeWaiver.actions, feeWaiver.selectors)
                                    }
                                })
                            ]
                        }),
                        simpleLazyLoadedRoute({
                            path: `rest/discount-code`,
                            name: 'discountCode',
                            pageName: 'discountCode',
                            require: ['RecordsPage', 'rest/discount-code'],
                            container: function RecordsPage(discountCode) {
                                return this('discountCode', `${process.env.PUBLIC_PATH || ''}/globalSettings/4/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors, filterColumns, true, true, discountCodeNote, false, discountCodeConfirmMessage)
                            },
                            childRoutes: [
                                simpleLazyLoadedRoute({
                                    path: `create`,
                                    name: 'discountCode.create',
                                    pageName: 'discountCode',
                                    require: ['CreateRecordPage', 'rest/discount-code'],
                                    container: function CreateRecordPage(discountCode) {
                                        return this('discountCode.create', `${process.env.PUBLIC_PATH || ''}/globalSettings/4/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors, false, false, discountCodeConfirmMessage)
                                    }
                                }),
                                simpleLazyLoadedRoute({
                                    path: `:id/edit`,
                                    name: 'discountCode.edit',
                                    pageName: 'discountCode',
                                    require: ['EditRecordPage', 'rest/discount-code'],
                                    container: function EditRecordPage(discountCode) {
                                        return this('discountCode.edit', `${process.env.PUBLIC_PATH || ''}/globalSettings/4/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors)
                                    }
                                }),
                                simpleLazyLoadedRoute({
                                    path: `:id`,
                                    name: 'discountCode.view',
                                    pageName: 'discountCode',
                                    require: ['ViewRecordPage', 'rest/discount-code'],
                                    container: function ViewRecordPage(discountCode) {
                                        return this('discountCode.view', `${process.env.PUBLIC_PATH || ''}/globalSettings/4/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors)
                                    }
                                })
                            ]
                        })
                        // simpleLazyLoadedRoute({
                        //     path: `activationCategories`,
                        //     name: 'activation',
                        //     pageName: 'activation',
                        //     require: ['ActivationFeeCategoriesPage', 'activation'],
                        //     chunk: ['globalSettings'],
                        //     container: function ActivationFeeCategoriesPage(settings) {
                        //         return this('activation', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories`, activationFeePricingColumns, settings.actions, settings.selectors, false)
                        //     },
                        //     childRoutes: [
                        //         simpleLazyLoadedRoute({
                        //             path: `:subTab/rest/fee-waiver`,
                        //             name: 'feeWaiver',
                        //             pageName: 'feeWaiver',
                        //             require: ['RecordsPage', 'rest/fee-waiver'],
                        //             container: function RecordsPage(feeWaiver) {
                        //                 return this('feeWaiver', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/0/rest/fee-waiver`, feeWaiverColumns, feeWaiver.actions, feeWaiver.selectors, filterColumns, true, true, feeWaiverCustomNote)
                        //             },
                        //             childRoutes: [
                        //                 simpleLazyLoadedRoute({
                        //                     path: `create`,
                        //                     name: 'feeWaiver.create',
                        //                     pageName: 'feeWaiver',
                        //                     require: ['CreateRecordPage', 'rest/fee-waiver'],
                        //                     container: function CreateRecordPage(feeWaiver) {
                        //                         return this('feeWaiver.create', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/0/rest/fee-waiver`, feeWaiverColumns, feeWaiver.actions, feeWaiver.selectors)
                        //                     }
                        //                 }),
                        //                 simpleLazyLoadedRoute({
                        //                     path: `:id`,
                        //                     name: 'feeWaiver.view',
                        //                     pageName: 'feeWaiver',
                        //                     require: ['ViewRecordPage', 'rest/fee-waiver'],
                        //                     container: function ViewRecordPage(feeWaiver) {
                        //                         return this('feeWaiver.view', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/0/rest/fee-waiver`, feeWaiverColumns, feeWaiver.actions, feeWaiver.selectors)
                        //                     }
                        //                 })
                        //             ]
                        //         }),
                        //         simpleLazyLoadedRoute({
                        //             path: `:subTab/rest/discount-code`,
                        //             name: 'discountCode',
                        //             pageName: 'discountCode',
                        //             require: ['RecordsPage', 'rest/discount-code'],
                        //             container: function RecordsPage(discountCode) {
                        //                 return this('discountCode', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/1/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors, filterColumns, true, true, discountCodeNote, false, discountCodeConfirmMessage)
                        //             },
                        //             childRoutes: [
                        //                 simpleLazyLoadedRoute({
                        //                     path: `create`,
                        //                     name: 'discountCode.create',
                        //                     pageName: 'discountCode',
                        //                     require: ['CreateRecordPage', 'rest/discount-code'],
                        //                     container: function CreateRecordPage(discountCode) {
                        //                         return this('discountCode.create', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/1/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors, false, false, discountCodeConfirmMessage)
                        //                     }
                        //                 }),
                        //                 simpleLazyLoadedRoute({
                        //                     path: `:id/edit`,
                        //                     name: 'discountCode.edit',
                        //                     pageName: 'discountCode',
                        //                     require: ['EditRecordPage', 'rest/discount-code'],
                        //                     container: function EditRecordPage(discountCode) {
                        //                         return this('discountCode.edit', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/1/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors)
                        //                     }
                        //                 }),
                        //                 simpleLazyLoadedRoute({
                        //                     path: `:id`,
                        //                     name: 'discountCode.view',
                        //                     pageName: 'discountCode',
                        //                     require: ['ViewRecordPage', 'rest/discount-code'],
                        //                     container: function ViewRecordPage(discountCode) {
                        //                         return this('discountCode.view', `${process.env.PUBLIC_PATH || ''}/globalSettings/3/activationCategories/1/rest/discount-code`, discountCodeColumns, discountCode.actions, discountCode.selectors)
                        //                     }
                        //                 })
                        //             ]
                        //         })
                        //     ]
                        // })
                    ]
                })
            ]
        }),
        simpleLazyLoadedRoute({
            path: `otherSettings`,
            name: 'otherSettings',
            pageName: 'globalSettings',
            require: ['ViewGlobalOtherSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'OtherSettings',
            data: {
                path: '/otherSettings',
                title: 'Settings',
                icon: 'Settings'
            },
            container: function ViewGlobalOtherSettingsPage(settings) {
                return this('otherSettings', `${process.env.PUBLIC_PATH || ''}/otherSettings`, globalOtherSettingsColumns, settings.actions, Object.assign({}, { ...settings.selectors, selectUser }), false)
            }
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}

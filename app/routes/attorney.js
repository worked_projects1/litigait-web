
/**
 * 
 * Attorney
 * 
 */

import schema from './schema';

import ClientSubmitButton from 'components/ClientSubmitButton';
import FilevineSessionForm from 'components/FilevineSessionForm';
import LitifyInfoDialog from 'components/LitifyInfoDialog';
import { propoundingPlansArray } from 'utils/plans';
import ResourceCategories from './resourceCategories';

import { selectUser, selectSessionExpand, selectError as sessionError, selectMyCaseRecords, selectButtonLoader as sessionLoader, selectSessionkey as sessionKey, selectStepper, selectClioRecords } from 'blocks/session/selectors';
import { clearCache, myCaseLoadRecords, verifySession as verifySessionAction, clioLoadRecords } from 'blocks/session/actions';

const clientsColumns = schema().clients().columns;
const casesColumns = schema().cases().columns;
const ordersColumns = schema().orders().columns;
const filterColumns = schema().filterColumns();
const legalForms = schema().legalForms();
const customizedForm = schema().cases().customizedForm;
const standardFormColumns = schema().cases().standardForm;
const disclosureFormColumns = schema().cases().disclosureForm;
const questionFormColumns = schema().questionForm().columns;
const partiesColumns = schema().parties().columns;
const partiesForm = schema().cases().partiesForm;
const subscriptionsHistoryColumns = schema().subscriptionsHistory;
const standardEditFormColumns = schema().standardEditForm().columns;
const attachCaseForm = schema().cases().attachCase;
const clientsForm = schema().cases().clientsForm;
const POSForm = schema().POS();
const opposingCounselFormColumns = schema().opposingCounselForm().columns;
const viewCasesColumns = schema().viewCases();
const propoundPOSForm = schema().propoundPOS();
const propoundEmailDocumentColumns = schema().propoundEmailDocument;
const templateNameForm = schema().cases().templateName;
const modifiedCustomTemplateOptions = schema().respondCustomTemplate().modifiedTemplateOptions;
const filevineForm = schema().filevineSecret().columns;
const filevineList = schema().filevineList().columns;
const clientCaseForm = schema().clientWithCase;
const filevineInstructionNote = schema().filevineSecret().instructionNote();
const myCaseListView = schema().myCaseListView().columns;
const respondEmailDocumentColumns = schema().respondEmailDocument;
const selectQuestionTemplateColumns = schema().selectQuestionTemplate().columns;
const userRecordFormColumns = schema().userRecordForm;
const createDocumentFormColumns = schema().createDocument;
const quickCreateDocumentInfoFormColumns = schema().quickCreateDocumentInfo;
const quickCreateClientCaseForm = schema().quickCreateClientCase;
const litifyListView = schema().litifyListView().columns;
const upShell = schema().upShell;
const quickCreateDescriptionModal = schema().quickCreateDescriptionModal().description;
const emailSentHistoryModal = schema().emailSentHistoryModal().description;

export default function (simpleLazyLoadedRoute, user) {
    const propoundingPlanType = user && user.plan_type &&  typeof user.plan_type === 'object' && user.plan_type['propounding'] || false;
    const isPropoundPagePlan = propoundingPlanType && propoundingPlansArray.includes(propoundingPlanType) || false;

    const isPropoundAccess = isPropoundPagePlan;

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Dashboard',
                path: '/',
                route: true
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `quickCreate`,
            name: 'quickCreate',
            pageName: 'quickCreate',
            queryParams: 'quickCreate',
            require: ['QuickCreateDocumentPage', 'rest/document-extraction-progress'],
            chunk: ['quickCreate', 'cases', 'subscriptions'],
            id: 'QuickCreate',
            data: {
                path: '/quickCreate',
                title: 'Quick Create',
                icon: 'quickCreate'
            },
            container: function QuickCreateDocumentPage(quickCreate) {
                return this('quickCreate', `${process.env.PUBLIC_PATH || ''}/quickCreate`, Object.assign({}, { subscriptionDetailsColumns: schema().subscriptionDetails, respondEmailDocumentColumns, upShellColumns: upShell, quickCreateDescriptionModal }), schema().legalForms, Object.assign({}, quickCreate.actions, { clearCache, verifySessionAction }), Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser, sessionError, sessionLoader, sessionKey, selectStepper }), filterColumns)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:id/reviewDocument`,
                    name: 'quickCreate.reviewDocument',
                    pageName: 'quickCreate',
                    queryParams: 'quickCreate',
                    require: ['ReviewDocumentPage', 'rest/document-extraction-progress'],
                    chunk: ['quickCreate', 'cases', 'subscriptions'],
                    container: function ReviewDocumentPage(quickCreate) {
                        return this('quickCreate.reviewDocument', `${process.env.PUBLIC_PATH || ''}/quickCreate`, Object.assign({}, { createDocumentFormColumns, quickCreateDocumentInfoFormColumns }), quickCreate.actions, Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser }))
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/questionsForm`,
                    name: 'quickCreate.questionsForm',
                    pageName: 'quickCreate',
                    queryParams: 'quickCreate',
                    require: ['ViewQuestionsFormPage', 'rest/document-extraction-progress'],
                    chunk: ['quickCreate', 'cases', 'subscriptions'],
                    container: function ViewQuestionsFormPage(quickCreate) {
                        return this('quickCreate.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), quickCreate.actions, Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser }))
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/standardForm`,
                    name: 'quickCreate.standardForm',
                    pageName: 'quickCreate',
                    queryParams: 'quickCreate',
                    require: ['ViewStandardFormPage', 'rest/document-extraction-progress'],
                    chunk: ['quickCreate', 'cases', 'subscriptions'],
                    container: function ViewStandardFormPage(quickCreate) {
                        return this('quickCreate.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, quickCreate.actions, Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser }))
                    }
                }),
            ]
        }),
        ResourceCategories('clientCategories', 'clientCategories', 'Clients', 'Clients', false, 'Clients', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `clients`,
                name: 'clients',
                pageName: 'clients',
                require: ['RecordsPage', 'clients'],
                chunk: ['myCase', 'filevine'],
                data: {
                    path: '/clients',
                    title: 'Clients'
                },
                container: function RecordsPage(clients) {
                    return this('clients', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, clientsColumns, clients.actions, clients.selectors, filterColumns, true, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'clients.create',
                        pageName: 'clients',
                        require: ['CreateRecordPage', 'clients'],
                        chunk: ['myCase', 'filevine'],
                        container: function CreateRecordPage(clients) {
                            return this('clients.create', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, schema().clients, clients.actions, clients.selectors, false, ClientSubmitButton)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'clients.edit',
                        pageName: 'clients',
                        require: ['EditRecordPage', 'clients'],
                        chunk: ['myCase', 'filevine'],
                        container: function EditRecordPage(clients) {
                            return this('clients.edit', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, schema().clients, clients.actions, clients.selectors, [FilevineSessionForm, LitifyInfoDialog])
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'clients.view',
                        pageName: 'clients',
                        require: ['ViewRecordPage', 'clients'],
                        chunk: ['myCase', 'filevine'],
                        container: function ViewRecordPage(clients) {
                            return this('clients.view', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, clientsColumns, clients.actions, clients.selectors)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/other-parties`,
                name: 'other-parties',
                pageName: 'other-parties',
                require: ['RecordsPage', 'rest/other-parties'],
                data: {
                    path: 'rest/other-parties',
                    title: 'Non-Parties'
                },
                container: function RecordsPage(otherParties) {
                    return this('other-parties', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, partiesColumns, otherParties.actions, otherParties.selectors, filterColumns, true, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'other-parties.create',
                        pageName: 'other-parties',
                        require: ['CreateRecordPage', 'rest/other-parties'],
                        container: function CreateRecordPage(otherParties) {
                            return this('other-parties.create', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, schema().parties, otherParties.actions, otherParties.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'other-parties.edit',
                        pageName: 'other-parties',
                        require: ['EditRecordPage', 'rest/other-parties'],
                        container: function EditRecordPage(otherParties) {
                            return this('other-parties.edit', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, schema().parties, otherParties.actions, otherParties.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'other-parties.view',
                        pageName: 'other-parties',
                        require: ['ViewRecordPage', 'rest/other-parties'],
                        container: function ViewRecordPage(otherParties) {
                            return this('other-parties.view', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, partiesColumns, otherParties.actions, otherParties.selectors)
                        }
                    })
                ]
            })
        ]),
        ResourceCategories('casesCategories', 'casesCategories', 'Cases', 'Cases', false, 'Cases', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `cases`,
                name: 'cases',
                pageName: 'cases',
                require: ['CasesPage', 'cases'],
                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                data: {
                    path: '/cases',
                    title: 'Cases',
                    icon: 'Cases'
                },
                container: function CasesPage(cases) {
                    return this('cases', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, schema().cases, Object.assign({}, cases.actions, { myCaseLoadRecords, clearCache, verifySessionAction, clioLoadRecords }), Object.assign({}, cases.selectors, { selectSessionExpand, selectUser, selectMyCaseRecords, sessionError, sessionLoader, sessionKey, selectStepper, selectClioRecords }), filterColumns, { customizedForm, attachCaseForm, clientsForm, filevineForm, filevineList, myCaseListView, userRecordFormColumns, litifyListView }, filevineInstructionNote)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'cases.create',
                        pageName: 'cases',
                        require: ['CreateRecordPage', 'cases'],
                        container: function CreateRecordPage(cases) {
                            return this('cases.create', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, schema().cases, cases.actions, cases.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'cases.edit',
                        pageName: 'cases',
                        require: ['EditRecordPage', 'cases'],
                        container: function EditRecordPage(cases) {
                            return this('cases.edit', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, schema().cases, cases.actions, cases.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/form/:type`,
                        name: 'cases.detail',
                        pageName: 'cases',
                        require: ['ViewCasesDetailPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewCasesDetailPage(cases) {
                            return this('cases.detail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, partiesColumns: partiesForm, POSColumns: POSForm, modifiedCustomTemplateOptions, filevineForm, subscriptionDetailsColumns: schema().subscriptionDetails, respondEmailDocumentColumns, selectQuestionTemplateColumns, userRecordFormColumns, upShellColumns: upShell, objectionColumns: legalForms.objectionColumns, customLegalColumns: legalForms.customLegalColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns, filevineInstructionNote)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/signature/:type/:legalforms_id`,
                        name: 'cases.signature',
                        pageName: 'cases',
                        require: ['ViewSignaturePage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewSignaturePage(cases) {
                            return this('cases.signature', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, disclosureFormColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/form`,
                        name: 'cases.view',
                        pageName: 'cases',
                        require: ['ViewCasesPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewCasesPage(cases) {
                            return this('cases.view', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, templateNameForm, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/forms`,
                        name: 'cases.view',
                        pageName: 'cases',
                        require: ['ViewCasesCategoriesPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewCasesCategoriesPage(cases) {
                            return this('cases.view', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, viewCasesColumns, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                        },
                        childRoutes: [
                            simpleLazyLoadedRoute({
                                path: `:tab/respond`,
                                name: 'cases.respond',
                                pageName: 'cases',
                                require: ['ViewRespondPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewRespondPage(cases) {
                                    return this('cases.respond', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, templateNameForm, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `respond/:type/details`,
                                name: 'cases.detail',
                                pageName: 'cases',
                                require: ['ViewCasesDetailPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewCasesDetailPage(cases) {
                                    return this('cases.detail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, partiesColumns: partiesForm, POSColumns: POSForm, modifiedCustomTemplateOptions, filevineForm, subscriptionDetailsColumns: schema().subscriptionDetails, respondEmailDocumentColumns, selectQuestionTemplateColumns, userRecordFormColumns, upShellColumns: upShell, objectionColumns: legalForms.objectionColumns, customLegalColumns: legalForms.customLegalColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns, filevineInstructionNote)
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `signature/:type/:legalforms_id`,
                                name: 'cases.signature',
                                pageName: 'cases',
                                require: ['ViewSignaturePage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewSignaturePage(cases) {
                                    return this('cases.signature', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, disclosureFormColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `respond/standardForm`,
                                name: 'cases.standardForm',
                                pageName: 'cases',
                                require: ['ViewStandardFormPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewStandardFormPage(cases) {
                                    return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `respond/questionsForm`,
                                name: 'cases.questionsForm',
                                pageName: 'cases',
                                require: ['ViewQuestionsFormPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewQuestionsFormPage(cases) {
                                    return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `:tab/propound`,
                                name: 'cases.propound',
                                pageName: 'cases',
                                require: ['ViewPropoundPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewPropoundPage(cases) {
                                    return this('cases.propound', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `propound/:type/:pid/details`,
                                name: 'cases.propoundDetail',
                                pageName: 'cases',
                                require: ['ViewPropoundDetailPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewPropoundDetailPage(cases) {
                                    return this('cases.propoundDetail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { uploadColumns: legalForms.billingForm, standardFormColumns, propoundEmailDocumentColumns, POSColumns: propoundPOSForm }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `propound/:type/:pid/standardForm`,
                                name: 'cases.standardForm',
                                pageName: 'cases',
                                require: ['ViewStandardFormPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewStandardFormPage(cases) {
                                    return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `propound/:type/:pid/questionsForm`,
                                name: 'cases.questionsForm',
                                pageName: 'cases',
                                require: ['ViewPropoundFormEditPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewPropoundFormEditPage(cases) {
                                    return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `:tab/medicalHistory`,
                                name: 'cases.medicalHistory',
                                pageName: 'cases',
                                require: ['ViewCasesMedicalHistoryPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewCasesMedicalHistoryPage(cases) {
                                    return this('cases.medicalHistory', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                                }
                            })
                        ]
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/questionsForm`,
                        name: 'cases.questionsForm',
                        pageName: 'cases',
                        require: ['ViewQuestionsFormPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewQuestionsFormPage(cases) {
                            return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/standardForm`,
                        name: 'cases.standardForm',
                        pageName: 'cases',
                        require: ['ViewStandardFormPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewStandardFormPage(cases) {
                            return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/propound`,
                        name: 'cases.propound',
                        pageName: 'cases',
                        require: ['ViewPropoundPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewPropoundPage(cases) {
                            return this('cases.propound', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, clientCaseForm, quickCreateClientCaseForm }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/:type/propound/:pid/details`,
                        name: 'cases.propoundDetail',
                        pageName: 'cases',
                        require: ['ViewPropoundDetailPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewPropoundDetailPage(cases) {
                            return this('cases.propoundDetail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { uploadColumns: legalForms.billingForm, standardFormColumns, propoundEmailDocumentColumns, POSColumns: propoundPOSForm }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/:type/propound/:pid/standardForm`,
                        name: 'cases.standardForm',
                        pageName: 'cases',
                        require: ['ViewStandardFormPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewStandardFormPage(cases) {
                            return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/:type/propound/:pid/questionsForm`,
                        name: 'cases.questionsForm',
                        pageName: 'cases',
                        require: ['ViewPropoundFormEditPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewPropoundFormEditPage(cases) {
                            return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand }))
                        }
                    })
                ].filter(childRoute => childRoute && typeof childRoute === 'object')
            }),
            simpleLazyLoadedRoute({
                path: `rest/archive/cases`,
                name: 'archiveCases',
                pageName: 'archiveCases',
                require: ['ArchiveCasesPage', 'rest/archive/cases'],
                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                data: {
                    path: 'rest/archive/cases',
                    title: 'Archived cases'
                },
                container: function ArchiveCasesPage(cases) {
                    return this('archiveCases', `${process.env.PUBLIC_PATH || ''}/casesCategories/rest/archive/cases`, schema().archiveCases, Object.assign({}, cases.actions, { clearCache }), Object.assign({}, cases.selectors, { selectSessionExpand }), filterColumns, false, false)
                }
            })
        ]),
        ResourceCategories('orderCategories', 'orderCategories', 'Orders', 'Orders', false, 'Orders', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `orders`,
                name: 'orders',
                pageName: 'orders',
                require: ['RecordsPage', 'orders'],
                data: {
                    path: '/orders',
                    title: 'Orders'
                },
                container: function RecordsPage(orders) {
                    return this('orders', `${process.env.PUBLIC_PATH || ''}/orderCategories/orders`, ordersColumns, orders.actions, orders.selectors, filterColumns)
                }
            }),
            simpleLazyLoadedRoute({
                path: `rest/subscriptions/history`,
                name: 'subscriptions',
                pageName: 'subscriptions',
                require: ['RecordsPage', 'rest/subscriptions/history'],
                data: {
                    path: 'rest/subscriptions/history',
                    title: 'Subscriptions'
                },
                container: function RecordsPage(subscriptions) {
                    return this('subscriptions', `${process.env.PUBLIC_PATH || ''}/orderCategories/rest/subscriptions/history`, subscriptionsHistoryColumns, subscriptions.actions, subscriptions.selectors, filterColumns)
                }
            })
        ]),
        simpleLazyLoadedRoute({
            path: `help`,
            name: 'help',
            pageName: 'help',
            require: ['HelpPage', 'help'],
            chunk: ['practices'],
            id: 'Help',
            data: {
                path: '/help',
                title: 'Help',
                icon: 'Help',
                nestedMenu: true,
                nestedMenuOption: [
                    {
                        id: 1,
                        label: 'FAQs',
                        url: 'https://www.esquiretek.com/frequently-asked-questions/',
                        icon: 'Faqs'
                    }, 
                    {
                        id: 2,
                        label: 'Tek Academy Videos',
                        url: 'https://www.esquiretek.com/help/',
                        icon: 'Video'
                    }
                ],
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'redirect',
            pageName: 'redirect',
            exact: true,
            container: 'RedirectPage'
        })
    ]
}
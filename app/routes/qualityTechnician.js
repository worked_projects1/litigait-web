/**
 * 
 * Quality Technician
 * 
 */

import schema from './schema';
import ResourceCategories from './resourceCategories';
import { selectUser, selectSessionExpand } from 'blocks/session/selectors';

const filterColumns = schema().filterColumns();
const supportRequestColumns = schema().supportRequest().columns;
const questionFormColumns = schema().questionForm().columns;
const bulkQuestionsFormColumns = schema().questionForm().bulkQuestionsForm;
const propoundSupportRequestColumns = schema().propoundSupportRequest().columns;
const extractionSupportRequestColumn = schema().extractionSupportRequest().columns;
const clientCaseInfoColumns = schema().clientCaseInfo().columns;
const deleteSupportRequestColumn = schema().deleteSupportRequest().columns;
const missedQuestionsFormField = schema().missedQuestionsFormField().columns;
const deleteQuestionsWithHashFormField = schema().deleteQuestionsWithHashForm().columns;


export default function (simpleLazyLoadedRoute) {

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Dashboard',
                path: '/',
                route: true
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: process.env.ENV === 'production' ? 'NotFoundPage' : 'LoginPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: process.env.ENV === 'production' ? 'NotFoundPage' : 'SignupPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: process.env.ENV === 'production' ? 'NotFoundPage' : 'ForgotPasswordPage'
        }),
        // simpleLazyLoadedRoute({
        //     path: `rest/help-request`,
        //     name: 'supportRequest',
        //     pageName: 'supportRequest',
        //     require: ['SupportRequestPage', 'rest/help-request'],
        //     chunk: ['cases'],
        //     data: {
        //         path: '/rest/help-request',
        //         title: 'Support Request',
        //         icon: 'SupportRequest'
        //     },
        //     container: function SupportRequestPage(supportRequest) {
        //         return this('supportRequest', `${process.env.PUBLIC_PATH || ''}/rest/help-request`, supportRequestColumns, supportRequest.actions, supportRequest.selectors, filterColumns, false, true)
        //     },
        //     childRoutes: [
        //         simpleLazyLoadedRoute({
        //             path: `:id`,
        //             name: 'supportRequest.view',
        //             pageName: 'supportRequest',
        //             require: ['ViewSupportRequestPage', 'rest/help-request'],
        //             chunk: ['cases'],
        //             container: function ViewSupportRequestPage(supportRequest) {
        //                 return this('supportRequest.view', `${process.env.PUBLIC_PATH || ''}/rest/help-request`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns}), supportRequest.actions, Object.assign({}, { ...supportRequest.selectors }, { selectUser, selectSessionExpand }), false)
        //             }
        //         })
        //     ]
        // }),
        ResourceCategories('supportCategories', 'supportCategories', 'Support Request', 'SupportRequest', false, 'SupportRequest', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/help-request`,
                name: 'supportRequest',
                pageName: 'supportRequest',
                require: ['SupportRequestPage', 'rest/help-request'],
                chunk: ['cases'],
                data: {
                    path: '/rest/help-request',
                    title: 'Responding',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(supportRequest) {
                    return this('supportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/help-request`, supportRequestColumns, supportRequest.actions, supportRequest.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'supportRequest.view',
                        pageName: 'supportRequest',
                        require: ['ViewSupportRequestPage', 'rest/help-request'],
                        chunk: ['cases'],
                        container: function ViewSupportRequestPage(supportRequest) {
                            return this('supportRequest.view', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/help-request`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns, clientCaseInfoColumns, deleteSupportRequestColumn }), supportRequest.actions, Object.assign({}, { ...supportRequest.selectors }, { selectUser, selectSessionExpand }), false)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/propound-support`,
                name: 'propoundSupportRequest',
                pageName: 'propoundSupportRequest',
                require: ['SupportRequestPage', 'rest/propound-support'],
                chunk: ['propound'],
                data: {
                    path: '/rest/propound-support',
                    title: 'Propounding',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(propoundSupportRequest) {
                    return this('propoundSupportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/propound-support`, propoundSupportRequestColumns, propoundSupportRequest.actions, propoundSupportRequest.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'propoundSupportRequest.view',
                        pageName: 'propoundSupportRequest',
                        require: ['ViewSupportRequestPage', 'rest/propound-support'],
                        chunk: ['propound'],
                        container: function ViewSupportRequestPage(propoundSupportRequest) {
                            return this('propoundSupportRequest.view', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/propound-support`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns, clientCaseInfoColumns, deleteSupportRequestColumn }), propoundSupportRequest.actions, Object.assign({}, { ...propoundSupportRequest.selectors }, { selectUser, selectSessionExpand }), true)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: 'rest/document-extraction-support',
                name: 'extractionSupportRequest',
                pageName: 'extractionSupportRequest',
                require: ['SupportRequestPage', 'rest/document-extraction-support'],
                chunk: ['quickCreate'],
                data: {
                    path: '/rest/document-extraction-support',
                    title: 'Quick Create',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(extractionSupportRequest) {
                    return this('extractionSupportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/document-extraction-support`, extractionSupportRequestColumn, extractionSupportRequest.actions, extractionSupportRequest.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'extractionSupportRequest.view',
                        pageName: 'extractionSupportRequest',
                        require: ['ViewSupportRequestPage', 'rest/document-extraction-support'],
                        chunk: ['quickCreate'],
                        container: function ViewSupportRequestPage(extractionSupportRequest) {
                            return this('extractionSupportRequest.view', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/document-extraction-support`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns, clientCaseInfoColumns, deleteSupportRequestColumn }), extractionSupportRequest.actions, Object.assign({}, { ...extractionSupportRequest.selectors }, { selectUser, selectSessionExpand }), 'extraction')
                        }
                    })
                ]
            })
        ]),
        simpleLazyLoadedRoute({
            path: `rest/practice-missed-questions-file`,
            name: 'missedQuestionFile',
            pageName: 'missedQuestionFile',
            require: ['MissedQuestionsSupportPage', 'rest/practice-missed-questions-file'],
            chunk: ['cases'],
            id: 'MissedQuestionFile',
            data: {
                path: '/rest/practice-missed-questions-file',
                title: 'Cache Files',
                icon: 'HashFile'
            },
            container: function MissedQuestionsSupportPage(missedQuestionFile) {
                return this('missedQuestionFile', `${process.env.PUBLIC_PATH || ''}/rest/questions-file`, Object.assign({}, { missedQuestionsFormField, deleteQuestionsWithHashFormField }), missedQuestionFile.actions, missedQuestionFile.selectors)
            },         
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}
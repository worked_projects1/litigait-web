
/**
 * 
 * Schema
 * 
 */

import React, { useState } from 'react';
import moment from 'moment';
import { capitalizeFirstLetter, viewPDF, planConfirmationMessage, dateDiffInDays, stateListPOS, buyLicenseConfirmationMessage } from 'utils/tools';
import { AsYouType } from 'libphonenumber-js';
import SVG from 'react-inlinesvg';
import { getSecretURL, getSecureFormURL, getTemplateSecureFormURL } from 'blocks/upload/remotes';
import TextField from '@material-ui/core/TextField';
import { Table, TableBody, TableCell, TableContainer, TableRow, FormControlLabel, Checkbox, Grid, Typography, Radio, Switch, Tooltip, IconButton } from '@material-ui/core';
import ReactHtmlParser from 'react-html-parser';
import Disc001 from '../translations/disc001.json';
import Disc002 from '../translations/disc002.json';
import Disc003 from '../translations/disc003.json';
import Disc004 from '../translations/disc004.json';
import Disc005 from '../translations/disc005.json';
import FL145 from '../translations/FL145.json';
import InitialDisclosure from '../translations/InitialDisclosure.json';
import FamilyLawDisclosure from '../translations/FamilyLawDisclosure.json';
import ManageCases from 'components/ManageCases';
import AlertDialog from 'components/AlertDialog';
import InfoIcon from '@material-ui/icons/Info';
import SyncAltOutlinedIcon from '@material-ui/icons/SyncAltOutlined';
import Icons from '../containers/ViewCasesDetailPage/components/PricingModalDialog/utils';
import { userBasedRespondingPlans, planCategoryArray, bothUserBasedPlans } from 'utils/plans';
import { parseIntPrice, cancelSubscription } from 'utils/tools';
import { Link } from 'react-router-dom';
import EmailRespondForm from 'components/EmailRespondForm';
import EmailPropoundForm from 'components/EmailPropoundForm';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import CAInitialDisclosure from '../translations/CAInitialDisclosure.json';



export default function () {

    function clients(record = {}) {
        const addressRequired = record && (record.street || record.city || record.state || record.zip_code) ? true : false;
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'LAST/FIRST NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true,
                    limit: true,
                    asterisks: true
                },
                {
                    id: 2,
                    value: 'first_name',
                    label: 'FIRST NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    limit: true,
                    asterisks: true
                },
                {
                    id: 3,
                    value: 'middle_name',
                    label: 'MIDDLE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    limit: true,
                },
                {
                    id: 4,
                    value: 'last_name',
                    label: 'LAST NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    limit: true,
                    asterisks: true
                },
                {
                    id: 5,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 6,
                    value: 'createdAt',
                    label: 'REGISTERED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'date'
                },
                {
                    id: 7,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone',
                    asterisks: true
                },
                {
                    id: 9,
                    value: 'dob',
                    label: 'BIRTHDATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    placeholder: 'mm/dd/yyyy',
                },
                {
                    id: 10,
                    value: 'address',
                    label: 'ADDRESS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 11,
                    value: 'street',
                    label: 'STREET',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    asterisks: addressRequired,
                    type: 'input',
                },
                {
                    id: 12,
                    value: 'city',
                    label: 'CITY',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    asterisks: addressRequired,
                    type: 'input',
                },
                {
                    id: 13,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    asterisks: addressRequired,
                    type: 'input',
                },
                {
                    id: 14,
                    value: 'zip_code',
                    label: 'ZIP CODE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    asterisks: addressRequired,
                    type: 'input'
                },
                {
                    id: 15,
                    value: 'totalCases',
                    label: 'CASE COUNT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'number'
                }
            ]
        }
    }

    function cases(record = {}, metaData, forms, props) {
        const plaintiff_name = record && record.case_plaintiff_name || false;
        const defendant_name = record && record.case_defendant_name || false;
        const case_title = plaintiff_name && defendant_name ? `${plaintiff_name} v. ${defendant_name}` : plaintiff_name ? plaintiff_name : defendant_name ? defendant_name : '';
        const showAttorneys = metaData && metaData['usersOptions'] && metaData['usersOptions'].length >= 1 ? true : false;
        const case_from = record?.matter_id && record?.case_from === 'mycase' ? true : false;
        const clientFormValue = forms && forms[`clientsForm_${record.id}`] && forms[`clientsForm_${record.id}`].values;
        const addressRequired = clientFormValue && Object.keys(clientFormValue).length > 0 && (clientFormValue.street || clientFormValue.city || clientFormValue.state || clientFormValue.zip_code) ? true : false;

        return {
            columns: [
                {
                    id: 1,
                    value: 'client_name',
                    label: 'CLIENT NAME',
                    sortColumn: 'Clients.name',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    limit: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    // sort: true,
                    // sortColumn: 'case_number',
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'matter_id',
                    label: 'MATTER ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'matter_id',
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'claim_number',
                    label: 'CLAIM NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'state',
                    label: 'STATE',
                    sortColumn: 'Cases.state',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? (row['state'] == 'PL' || row['state'] == 'NPL') ? `Other (${row['state']})` : row['state'] : '-',
                    disableOptons: {
                        edit: true
                    },
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'client_id',
                    label: 'CLIENT NAME',
                    editRecord: true,
                    disableRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'clientsOptions',
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    defaultValue: record.id || false,
                    type: 'autoComplete',
                    asterisks: true
                },
                {
                    id: 7,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'case_plaintiff_name',
                    label: 'PLAINTIFF NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    gridSize: 5,
                    asterisks: true

                },
                {
                    id: 9,
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'component',
                    mandatory: true,
                    gridSize: 2,
                    edit: true,
                    style: { display: 'flex', justifyContent: 'center', alignItems: 'center' },
                    render: ({ classes }) => {
                        const { actions, dispatch } = props;
                        const handleChange = () => {
                            dispatch(actions.plaintiffDefendantSwitch(record));
                        }
                        return (<Grid>
                            <IconButton onClick={handleChange}
                                className={classes.doubleArrow}>
                                <SyncAltOutlinedIcon />
                            </IconButton>
                        </Grid>)
                    },
                    asterisks: true
                },
                {
                    id: 10,
                    value: 'case_defendant_name',
                    label: 'DEFENDANT NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    gridSize: 5,
                    asterisks: true
                },
                {
                    id: 11,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    create: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'case_title',
                    type: 'input',
                    limit: true,
                    mandatory: true,
                    defaultValue: `${case_title}`,
                    asterisks: true
                },
                {
                    id: 12,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    edit: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    limit: true,
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 13,
                    value: 'due_date',
                    label: 'DUE DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    sortColumn: 'due_date',
                    html: (row) => {
                        const dueDate = row && row['due_date'] && moment(row['due_date']).format('MM/DD/YY');
                        const diffDays = moment(dueDate).diff(moment().format('MM/DD/YY'), 'day');
                        return dueDate && <span style={{ color: diffDays > 14 ? 'black' : diffDays > 7 && diffDays <= 14 ? '#FF8C00' : diffDays <= 7 && 'red' }}>{dueDate}</span>
                    },
                    type: 'input',
                },
                {
                    id: 14,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'Cases.createdAt',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 15,
                    value: 'date_of_loss',
                    label: 'DATE OF LOSS',
                    sortColumn: 'Cases.date_of_loss',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input',
                    html: (row) => row['date_of_loss'] && moment(row['date_of_loss']).format('MM/DD/YY'),
                    placeholder: 'mm/dd/yyyy'
                },
                {
                    id: 16,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    disableOptons: {
                        edit: record?.case_number && record?.case_from == 'filevine' ? true : false
                    },
                    asterisks: true
                },
                {
                    id: 17,
                    value: 'matter_id',
                    label: 'MATTER ID',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    disableOptons: {
                        edit: case_from ? true : false
                    },
                },
                {
                    id: 18,
                    value: 'claim_number',
                    label: 'CLAIM NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 19,
                    value: 'federal',
                    label: 'FEDERAL',
                    editRecord: true,
                    disableRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    html: (row) => row['federal'] ? 'Yes' : 'No',
                    type: 'checkbox',
                    style: { paddingBottom: '0px' }
                },
                {
                    id: 20,
                    value: 'federal_district',
                    label: 'FEDERAL DISTRICT',
                    editRecord: (record && record.federal) ? true : false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    style: { paddingTop: '0px' },
                    asterisks: true
                },
                {
                    id: 21,
                    value: 'county',
                    label: 'COUNTY',
                    editRecord: (record && record.federal) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    style: { paddingTop: '0px' },
                    asterisks: true
                },
                {
                    id: 22,
                    value: 'state',
                    label: 'STATE',
                    editRecord: (record && record.federal) ? false : true,
                    disableRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'autoComplete',
                    asterisks: true
                },
                {
                    id: 23,
                    value: 'attorney_name',
                    label: `ASSIGNED ATTORNEY`,
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input'

                },
                {
                    id: 24,
                    value: 'attorneys',
                    label: `ASSIGNED ATTORNEY`,
                    editRecord: showAttorneys ? true : false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    selectAll: true,
                    options: 'usersOptions',
                    html: (row, metaData) => metaData['usersOptions'] && metaData['usersOptions'].length > 0 && row['attorneys'] ? metaData['usersOptions'].filter(_ => row['attorneys'].split(',').includes(_.value)).map(_ => _.label).join(',') : row['attorneys'],
                    type: 'multiSelect'
                },
                {
                    id: 25,
                    value: 'opposing_counsel_info',
                    label: 'OPPOSING COUNSEL INFO (FOR PoS)',
                    editRecord: (record && record.state && stateListPOS(record.state) && !record.federal) ? true : false,
                    disableRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    html: (row) => row['opposing_counsel_info'] ? 'Yes' : 'No',
                    note: record && record.opposing_counsel_info ? 'Note: More opposing counsels can be added later from cases page' : false,
                    type: 'checkbox',
                    style: { paddingBottom: '0px' }
                },
                {
                    id: 26,
                    value: 'opposing_counsel_attorney_name',
                    label: 'OPPOSING COUNSEL ATTORNEY NAME',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    type: 'multiInput',
                    inputLimit: 5,
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 27,
                    value: 'opposing_counsel_office_name',
                    label: 'OPPOSING COUNSEL PRACTICE NAME',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 28,
                    value: 'opposing_counsel_street',
                    label: 'STREET',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    placeholder: 'eg: 101 Main Street',
                    asterisks: true
                },
                {
                    id: 29,
                    value: 'opposing_counsel_city',
                    label: 'CITY',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 30,
                    value: 'opposing_counsel_state',
                    label: 'STATE',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 31,
                    value: 'opposing_counsel_zip_code',
                    label: 'ZIP CODE',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 32,
                    value: 'opposing_counsel_email',
                    label: 'EMAIL',
                    editRecord: (record && record.opposing_counsel_info) ? true : false,
                    viewRecord: true,
                    create: true,
                    viewMode: false,
                    visible: false,
                    placeholder: "Type valid email and press enter/return key",
                    type: 'multiEmail',
                    mandatory: true,
                    asterisks: true
                },
                {
                    id: 33,
                    value: 'opposing_counsel',
                    label: 'OPPOSING COUNSEL INFO',
                    editRecord: record && record.state && stateListPOS(record.state) ? true : false,
                    edit: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'fieldArray',
                    title: 'Counsel',
                    max: 5,
                    btnLabel: 'Add additional counsel info',
                    mandatory: true,
                    defaultField: false,
                    fieldArray: [
                        {
                            id: 1,
                            value: 'opposing_counsel_attorney_name',
                            label: 'OPPOSING COUNSEL ATTORNEY NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'multiInput',
                            inputLimit: 5,
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 2,
                            value: 'opposing_counsel_office_name',
                            label: 'OPPOSING COUNSEL PRACTICE NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 3,
                            value: 'opposing_counsel_street',
                            label: 'STREET',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            placeholder: 'eg: 101 Main Street',
                            asterisks: true
                        },
                        {
                            id: 4,
                            value: 'opposing_counsel_city',
                            label: 'CITY',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 5,
                            value: 'opposing_counsel_state',
                            label: 'STATE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 6,
                            value: 'opposing_counsel_zip_code',
                            label: 'ZIP CODE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 7,
                            value: 'opposing_counsel_email',
                            label: 'EMAIL',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            placeholder: "Type valid email and press enter/return key",
                            type: 'multiEmail',
                            mandatory: true,
                            asterisks: true
                        }
                    ]
                },
                {
                    id: 34,
                    value: 'archive_cases',
                    label: 'ARCHIVE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const record = row && Object.assign({}, { ...row });
                        return <ManageCases
                            data={record}
                            type={'ArchiveOutlined'}
                            message={"Archiving closed cases improves usability by not showing closed/past cases and improving page load speed. Archived cases are not deleted and can be un-archived. Do you want to proceed?"}
                            {...props} />
                    },
                    type: 'component',
                }
            ],
            standardForm: {
                Disc001,
                Disc004,
                Disc002,
                Disc003,
                Disc005,
                FL145
            },
            disclosureForm: {
                InitialDisclosure,
                FamilyLawDisclosure,
                CAInitialDisclosure
            },
            customizedForm: [
                {
                    id: 1,
                    value: 'fee_terms',
                    label: 'Customized Fee Agreement',
                    colType: 'terms',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'textarea',
                    placeholder: 'Copy the customized Fee Agreement.'
                }
            ],
            partiesForm: [
                {
                    id: 1,
                    value: 'party_ids',
                    label: 'Select Recipients',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'partiesOptions',
                    mandatory: true,
                    selectAll: true,
                    type: 'multiSelect'
                }
            ],
            attachCase: [
                {
                    id: 1,
                    value: 'client_id',
                    label: 'Client Name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    options: 'clientFilterOptions',
                    type: 'select'
                }
            ],
            clientsForm: [
                {
                    id: 1,
                    value: 'first_name',
                    label: 'FIRST NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    limit: true
                },
                {
                    id: 2,
                    value: 'middle_name',
                    label: 'MIDDLE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    limit: true,
                },
                {
                    id: 3,
                    value: 'last_name',
                    label: 'LAST NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    limit: true
                },
                {
                    id: 4,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 5,
                    value: 'createdAt',
                    label: 'REGISTERED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'date'
                },
                {
                    id: 6,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 8,
                    value: 'dob',
                    label: 'BIRTHDATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    placeholder: 'mm/dd/yyyy'
                },
                {
                    id: 9,
                    value: 'street',
                    label: 'STREET',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 10,
                    value: 'city',
                    label: 'CITY',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 11,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 12,
                    value: 'zip_code',
                    label: 'ZIP CODE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 13,
                    value: 'totalCases',
                    label: 'CASE COUNT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'number'
                }
            ],
            templateName: [
                {
                    id: 1,
                    value: 'template_id',
                    label: 'Template Name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    options: 'templateNameOptions',
                    type: 'select'
                }
            ],
        }
    }

    function archiveCases(props = {}) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'client_name',
                    label: 'CLIENT NAME',
                    sortColumn: 'Clients.name',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    limit: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    sortColumn: 'CaseArchives.case_number',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'matter_id',
                    label: 'MATTER ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'claim_number',
                    label: 'CLAIM NUMBER',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'state',
                    label: 'STATE',
                    sortColumn: 'CaseArchives.state',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? row['state'] : '-',
                    disableOptons: {
                        edit: true
                    },
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    sortColumn: 'CaseArchives.case_title',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'due_date',
                    label: 'DUE DATE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const dueDate = row && row['due_date'] && moment(row['due_date']).format('MM/DD/YY');
                        const diffDays = moment(dueDate).diff(moment().format('MM/DD/YY'), 'day');
                        return dueDate && <span style={{ color: diffDays > 14 ? 'black' : diffDays > 7 && diffDays <= 14 ? '#FF8C00' : diffDays <= 7 && 'red' }}>{dueDate}</span>
                    },
                    type: 'input',
                },
                {
                    id: 8,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'CaseArchives.createdAt',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'date_of_loss',
                    label: 'DATE OF LOSS',
                    sortColumn: 'Cases.date_of_loss',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input',
                    html: (row) => row['date_of_loss'] && moment(row['date_of_loss']).format('MM/DD/YY')
                },
                {
                    id: 10,
                    value: 'attorney_name',
                    label: `ASSIGNED ATTORNEY`,
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 11,
                    value: 'unArchive_cases',
                    label: 'UNARCHIVE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const record = row && Object.assign({}, { ...row });
                        return <ManageCases data={record} type={"UnArchive"} message={"Do you want to unarchive this case?"} {...props} />
                    },
                    type: 'component',
                }
            ]
        }
    }

    function orders() {
        return {
            columns: [
                // {
                //     id: 1,
                //     value: 'order_id',
                //     label: 'ORDER NUMBER',
                //     sortColumn: 'Orders.order_date',
                //     editRecord: false,
                //     viewRecord: true,
                //     viewMode: false,
                //     visible: true,
                //     sort: true,
                //     type: 'input'
                // },
                {
                    id: 1,
                    value: 'plan_type',
                    label: 'PLAN TYPE',
                    sortColumn: 'Orders.plan_type',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => {
                        const plans = row && row['plan_type'] && row['plan_type'].includes('VIP') && "VIP ACCOUNT" || row && row['plan_type'] && row['plan_type'].toUpperCase()
                        return plans;
                    },
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'client_name',
                    label: 'CLIENT NAME',
                    sortColumn: 'Clients.name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'user_name',
                    label: 'USER',
                    sortColumn: 'Users.name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'practice_id',
                    label: 'PRACTICE ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'user_id',
                    label: 'USER ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'case_id',
                    label: 'CASE ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'order_date',
                    label: 'ORDER DATE',
                    sortColumn: 'Orders.order_date',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    defaultValue: moment(),
                    html: (row) => row && row['order_date'] && moment(row['order_date']).format('MM/DD/YY HH:mm:ss'),
                    sort: true,
                    type: 'date'
                },
                {
                    id: 10,
                    value: 'filename',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => row['filename'] && row['filename'].length > 25 ? row['filename'].substring(0, 25) + '...' : row['filename'],
                    type: 'input'
                },
                {
                    id: 11,
                    value: 'document_type',
                    label: () => {
                        const documentType = ['IDC - Initial Disclosures', 'FROGS - Form Interrogatories', 'SPROGS - Specially Prepared Interrogatories', 'RFPD - Requests For Production of Documents', 'RFA - Request For Admissions', 'BOP - Bill of Particulars', 'ODD - Omnibus Discovery Demands', 'NDI - Notice for Discovery and Inspection','NTA - Notice to Admit'];

                        return <AlertDialog
                            description={() => {
                                return <>{documentType?.map(e => <p>{e}</p>)}</>
                            }}
                            customDialog
                            closeBtn
                            onConfirmPopUpClose>
                            {(open) => {
                                return <>
                                    PRODUCT <InfoIcon style={{ cursor: "pointer", width: "18px", color: "#47AC39", marginTop: "-3px", marginRight: "2px" }} onClick={open}/>
                                </>
                            }}
                        </AlertDialog>
                    },
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 12,
                    value: 'document_generation_type',
                    label: 'TYPE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 13,
                    value: 'amount_charged',
                    label: 'PRICE',
                    sortColumn: 'Orders.amount_charged',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 14,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: ['completed', 'pending'],
                    type: 'select'
                }
            ]
        }
    }

    function adminOrders() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'order_date',
                    label: 'ORDER DATE',
                    sortColumn: 'Orders.order_date',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    defaultValue: moment(),
                    html: (row) => row && row['order_date'] && moment(row['order_date']).format('MM/DD/YY HH:mm:ss'),
                    sort: true,
                    type: 'date'
                },
                {
                    id: 2,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'Practices.name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'user_name',
                    label: 'USER',
                    sortColumn: 'Users.name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                // {
                //     id: 4,
                //     value: 'order_id',
                //     label: 'ORDER NUMBER',
                //     sortColumn: 'Orders.order_id',
                //     editRecord: false,
                //     viewRecord: true,
                //     viewMode: false,
                //     visible: true,
                //     sort: true,
                //     type: 'input'
                // },
                {
                    id: 4,
                    value: 'plan_type',
                    label: 'PLAN TYPE',
                    sortColumn: 'Orders.plan_type',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row && row['plan_type'] && row['plan_type'].toUpperCase(),
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'practice_id',
                    label: 'PRACTICE ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'user_id',
                    label: 'USER ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'case_id',
                    label: 'CASE ID',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 10,
                    value: 'document_type',
                    label: 'PRODUCT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 11,
                    value: 'document_generation_type',
                    label: 'TYPE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 12,
                    value: 'amount_charged',
                    label: 'PRICE',
                    sortColumn: 'Orders.amount_charged',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 13,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: ['completed', 'pending'],
                    type: 'select'
                }
            ]
        }
    }

    function users(record = {}) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    disableOptons: {
                        edit: true
                    },
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: {
                        create: true
                    }
                },
                {
                    id: 4,
                    value: 'role',
                    label: 'ROLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['role'] === 'lawyer' ? 'Attorney' : row['role'] === 'paralegal' ? 'Non-Attorney' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'],
                    options: [
                        {
                            label: 'Attorney',
                            value: 'lawyer',
                        },
                        {
                            label: 'Non-Attorney',
                            value: 'paralegal',
                        }
                    ],
                    type: 'select',
                    mandatory: true
                },
                {
                    id: 5,
                    value: 'state_bar_number',
                    label: 'STATE BAR NUMBER',
                    editRecord: (record.role === 'lawyer') ? true : false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 6,
                    value: 'is_admin',
                    label: 'ADMIN',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    visible: true,
                    html: (row) => row['is_admin'] ? 'Yes' : 'No',
                    type: 'checkbox'
                },
                {
                    id: 7,
                    value: 'is_admin',
                    label: 'Make a user an Admin',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    sort: true,
                    visible: false,
                    type: 'checkbox'
                }
            ],
            customDescription: (metaData, formRecord) => {
                return `This user account has already been created in another practice. Do you want to link this user to your practice?`
            },
            customConfirmMessage: (user) => {
                if(user?.practiceDetails?.billing_type !== 'limited_users_billing') {
                    const { total_no_of_user_count, yearly_user_limit, monthly_user_limit, plan_type } = user;
                    const planType = plan_type && plan_type && typeof plan_type === 'object' && plan_type['responding'] || false;
                    const userLimitation = planType == '' ? total_no_of_user_count >= monthly_user_limit : planType.includes('monthly') ? total_no_of_user_count >= monthly_user_limit : planType.includes('yearly') ? total_no_of_user_count >= yearly_user_limit : total_no_of_user_count >= monthly_user_limit;

                    return userLimitation ? `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them in order to create new user accounts.` : false;
                }
            },
            userLimitationCustomConfirmMessage: (user) => {
                const planType = user?.plan_type?.responding;
                if(user?.practiceDetails?.billing_type === 'limited_users_billing' && planType && userBasedRespondingPlans.includes(planType)) {
                    const license_count = user?.practiceDetails?.license_count || 0;
                    const user_count = user?.total_no_of_user_count;
                    const message = () => <>Your account has reached the maximum number of users allowed. To add new users, please go to <Link to={{ pathname: '/settings' }} style={{ color: '#2ca01c', textDecoration: 'underline'}}>Settings</Link> and purchase additional license.</>
                    return (user_count >= license_count) ? message : false;
                }
            }
        }
    }

    function objections() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'objection_title',
                    label: 'OBJECTION TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    max: 50,
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'objection_text',
                    label: 'OBJECTION TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row.objection_text ? ReactHtmlParser(row.objection_text) : '-',
                    type: 'textEditor',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    html: (row, metaData) => {
                        const stateObj = row && metaData && metaData['stateOptions'] && metaData['stateOptions'].length > 0 ? metaData['stateOptions'].find(_ => _.value === row.state) : {};
                        return stateObj && stateObj.label || '-';
                    },
                    mandatory: true,
                    type: 'autoComplete',
                },
                {
                    id: 4,
                    value: 'attorneys',
                    label: `ATTORNEYS`,
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    selectAll: true,
                    options: 'usersOptions',
                    html: (row, metaData) => metaData['usersOptions'] && metaData['usersOptions'].length > 0 && row['attorneys'] ? metaData['usersOptions'].filter(_ => row['attorneys'].split(',').includes(_.id)).map(_ => _.label).join(', ') : row['attorneys'],
                    element: (<Tooltip placement='top-start' title={<span style={{ fontSize: '12px' }}>Add to the shortlisted objections for the following attorneys.</span>}>
                        <InfoIcon style={{ color: '#2ca01c', cursor: 'pointer' }} />
                    </Tooltip>),
                    type: 'multiSelect'
                }
            ]
        }
    }

    function federalObjections() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'objection_title',
                    label: 'OBJECTION TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    max: 50,
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'objection_text',
                    label: 'OBJECTION TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row.objection_text ? ReactHtmlParser(row.objection_text) : '',
                    type: 'textEditor',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'attorneys',
                    label: `ATTORNEYS`,
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    selectAll: true,
                    options: 'usersOptions',
                    html: (row, metaData) => metaData['usersOptions'] && metaData['usersOptions'].length > 0 && row['attorneys'] ? metaData['usersOptions'].filter(_ => row['attorneys'].split(',').includes(_.id)).map(_ => _.label).join(', ') : row['attorneys'],
                    element: (<Tooltip placement='top-start' title={<span style={{ fontSize: '12px' }}>Add to the shortlisted objections for the following attorneys.</span>}>
                        <InfoIcon style={{ color: '#2ca01c', cursor: 'pointer' }} />
                    </Tooltip>),
                    type: 'multiSelect'
                },
            ]
        }
    }

    function adminObjections() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'objection_title',
                    label: 'OBJECTION TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    max: 50,
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'objection_text',
                    label: 'OBJECTION TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row.objection_text ? ReactHtmlParser(row.objection_text) : '-',
                    type: 'textEditor',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    html: (row, metaData) => {
                        const stateObj = row && metaData && metaData['stateOptions'] && metaData['stateOptions'].length > 0 ? metaData['stateOptions'].find(_ => _.value === row.state) : {};
                        return stateObj && stateObj.label || '-';
                    },
                    mandatory: true,
                    type: 'autoComplete',
                }
            ]
        }
    }

    function adminFederalObjections() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'objection_title',
                    label: 'OBJECTION TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    max: 50,
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'objection_text',
                    label: 'OBJECTION TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row.objection_text ? ReactHtmlParser(row.objection_text) : '',
                    type: 'textEditor',
                    mandatory: true
                }
            ]
        }
    }

    function adminUsers(record = {}) {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'EsquireTek Users',
                    addNewLabel: 'EsquireTek User',
                    create: true,
                    view: true,
                    filter: (records) => records && records.filter(_ => !_.practice_id) || records,
                    columns: [
                        {
                            id: 1,
                            value: 'id',
                            label: 'ID',
                            editRecord: false,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                        {
                            id: 2,
                            value: 'name',
                            label: 'NAME',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 3,
                            value: 'email',
                            label: 'EMAIL',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            disableOptons: {
                                edit: true
                            },
                            sort: true,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 4,
                            value: 'password',
                            label: 'PASSWORD',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            sort: true,
                            type: 'password',
                            mandatory: {
                                create: true
                            }
                        },
                        {
                            id: 5,
                            value: 'role',
                            label: 'ROLE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            html: (row) => {
                                return row['role'] === 'superAdmin' ? 'Super Admin' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'];
                            },
                            options: [
                                {
                                    label: 'Super Admin',
                                    value: 'superAdmin',
                                },
                                {
                                    label: 'Manager',
                                    value: 'manager',
                                },
                                {
                                    label: 'Operator',
                                    value: 'operator',
                                },
                                {
                                    label: 'Medical Expert',
                                    value: 'medicalExpert',
                                },
                                {
                                    label: 'Quality Technician',
                                    value: 'QualityTechnician',
                                }
                            ],
                            type: 'select',
                            mandatory: true
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Practice Users',
                    addNewLabel: 'Practice User',
                    create: true,
                    view: true,
                    filter: (records) => records && records.filter(_ => _.practice_id) || records,
                    columns: [
                        {
                            id: 1,
                            value: 'practice_name',
                            label: 'PRACTICE NAME',
                            editRecord: false,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            type: 'input'
                        },
                        {
                            id: 2,
                            value: 'practice_id',
                            label: 'PRACTICE NAME',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            options: 'practicesOptions',
                            disableOptons: {
                                edit: true
                            },
                            mandatory: true,
                            type: 'autoComplete'
                        },
                        {
                            id: 3,
                            value: 'name',
                            label: 'NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 4,
                            value: 'email',
                            label: 'EMAIL',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            disableOptons: {
                                edit: true
                            },
                            sort: true,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 5,
                            value: 'password',
                            label: 'PASSWORD',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: {
                                create: true
                            },
                        },
                        {
                            id: 6,
                            value: 'role',
                            label: 'ROLE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            sort: true,
                            html: (row) => row['role'] === 'lawyer' ? 'Attorney' : row['role'] === 'paralegal' ? 'Non-Attorney' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'],
                            options: [
                                {
                                    label: 'Attorney',
                                    value: 'lawyer',
                                },
                                {
                                    label: 'Non-Attorney',
                                    value: 'paralegal',
                                }
                            ],
                            type: 'select',
                            mandatory: true
                        },
                        {
                            id: 7,
                            value: 'state_bar_number',
                            label: 'STATE BAR NUMBER',
                            editRecord: (record.role === 'lawyer') ? true : false,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 8,
                            value: 'is_admin',
                            label: 'ADMIN',
                            editRecord: false,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            sort: true,
                            visible: true,
                            html: (row) => row['is_admin'] ? 'Yes' : 'No',
                            type: 'checkbox'
                        },
                        {
                            id: 9,
                            value: 'is_admin',
                            label: 'Make a user an Admin',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            sort: true,
                            visible: false,
                            type: 'checkbox'
                        }
                    ]
                }

            ]
        }
    }



    function practices(record = {}) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'name',
                    editRecord: true,
                    viewRecord: record && record.plan_cancel_at_period ? true : false,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'subscriptions',
                    label: 'PLAN TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    html: (row) => {
                        const valueArr = row['subscriptions'] && row['subscriptions'] || [];
                        const vipPlan = valueArr[0]?.plan_type && ['responding_vip', 'propounding_vip'].includes(valueArr[0]?.plan_type) ? true : false;
                        let options = vipPlan ? ['VIP ACCOUNT'] : valueArr && valueArr.length > 0 && valueArr.map(el => {
                            if (el) {
                                return el.plan_type && el.plan_type != '' && el?.plan_category ? (['free_trial'].includes(el.plan_type) ? 'Free Trial' : el.plan_type.includes('yearly') ? `${el.plan_category} yearly` : el.plan_type.includes('monthly') ? `${el.plan_category} monthly` : el.plan_type) : el.plan_type;
                            }
                        }).filter(x => x);
                        options = options && options.join(', ')
                        return options ? <div style={{ textTransform: 'capitalize' }}>{options}</div> : null;
                    },
                    type: 'input'
                },
                // {
                //     id: 4,
                //     value: 'total_orders_count',
                //     label: 'ORDERS',
                //     editRecord: false,
                //     viewRecord: false,
                //     viewMode: true,
                //     visible: true,
                //     type: 'input'
                // },
                {
                    id: 5,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'createdAt',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'street',
                    label: 'STREET',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    placeholder: 'eg: 101 Main Street'
                },
                {
                    id: 7,
                    value: 'city',
                    label: 'CITY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'zip_code',
                    label: 'ZIP CODE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 10,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 11,
                    value: 'fax',
                    label: 'FAX',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 12,
                    value: 'recent_login_ts',
                    label: 'LAST LOGIN',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => row['recent_login_ts'] && moment(row['recent_login_ts']).format('MM/DD/YY HH:mm:ss')
                },
                {
                    id: 13,
                    value: 'no_of_users',
                    label: 'NO. OF USERS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 14,
                    value: 'no_of_orders',
                    label: 'NO. OF ORDERS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                // {
                //     id: 10,
                //     value: 'logoFile',
                //     label: 'LOGO',
                //     editRecord: true,
                //     viewRecord: true,
                //     viewMode: false,
                //     visible: true,
                //     upload: true,
                //     contentType: 'image/*',
                //     type: 'upload'
                // }
            ],
            settings: (props) => [
                {
                    schemaId: 1,
                    name: 'Customized Pricing',
                    title: (settings) => (settings && settings.billing_type && settings.billing_type === 'customized') ? 'Customized Pricing' : 'Global Pricing',
                    btnLabel: (settings) => (settings && settings.billing_type && settings.billing_type === 'customized') ? 'Change Pricing' : 'Create Custom Pricing',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'settings',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'no_of_docs_free_tier',
                            label: 'Number of docs in free tier',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: false,
                            type: 'number'
                        },
                        {
                            id: 2,
                            value: 'template_generation_price.FROGS',
                            label: 'FROGS Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'FROGS',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 3,
                            value: 'final_doc_generation_price.FROGS',
                            label: 'FROGS Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'FROGS',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 4,
                            value: 'template_generation_price.SPROGS',
                            label: 'SPROGS Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'SPROGS',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 5,
                            value: 'final_doc_generation_price.SPROGS',
                            label: 'SPROGS Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            docType: 'SPROGS',
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },

                        {
                            id: 6,
                            value: 'template_generation_price.RFPD',
                            label: 'RFPD Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFPD',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 7,
                            value: 'final_doc_generation_price.RFPD',
                            label: 'RFPD Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFPD',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 8,
                            value: 'template_generation_price.RFA',
                            label: 'RFA Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFA',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 9,
                            value: 'final_doc_generation_price.RFA',
                            label: 'RFA Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFA',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 10,
                            value: 'teksign',
                            label: 'TekSign',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'teksign',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 11,
                            value: 'pos',
                            label: 'Proof of Service',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'pos',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 12,
                            value: 'free_user_limit',
                            label: 'Free User Limit',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'number'
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Medical History Pricing Tiers',
                    title: (settings) => (settings && settings.billing_type && settings.billing_type === 'customized') ? 'Medical History Customized Pricing Tiers' : 'Medical History Global Pricing Tiers',
                    btnLabel: (settings) => (settings && settings.billing_type && settings.billing_type === 'customized') ? 'Change Medical History Pricing Tiers' : 'Create Custom Medical History Pricing Tiers',
                    create: true,
                    edit: true,
                    view: true,
                    value: `settings`,
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    initialValues: {
                        medical_history_minimum_pricing: 0,
                        medical_history_pricing_tier: [
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                custom_quote_needed: true
                            }
                        ]
                    },
                    columns: [
                        {
                            id: 1,
                            value: 'medical_history_minimum_pricing',
                            label: 'Minimum Pricing',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFA',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number',
                            style: {
                                position: 'relative',
                                top: '18px',
                                marginLeft: '15px',
                                marginRight: '15px'
                            }
                        },
                        {
                            id: 2,
                            value: 'medical_history_pricing_tier',
                            label: '',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            render: ({ input, classes, meta: { touched, error, warning } }) => {
                                let InputValue = input.value && Array.isArray(input.value) && input.value.length > 0 ? input.value : [{ "from": 0, "to": 0, "price": 0 }, { "from": 0, "to": 0, "price": 0 }, { "from": 0, "to": 0, "price": 0 }, { "from": 0, "to": 0, "price": 0 }, { "from": 0, "custom_quote_needed": true }];
                                const [touch, setTouch] = useState(false);
                                const [validationError, setValidationError] = useState(false);
                                const [pageVal, setpageVal] = useState(false);
                                const InputChange = (value, name, index) => {
                                    let newInputValues = InputValue.map(obj => ({ ...obj }));
                                    const changedValues = newInputValues.reduce((a, row, rowIndex) => {
                                        if (rowIndex === index && name === 'from') {
                                            row.from = parseInt(value);
                                        } else if (rowIndex === index && name === 'to') {
                                            row.to = parseInt(value);
                                        } else if (rowIndex === index && name === 'price') {
                                            row.price = parseFloat(value);
                                        }
                                        if (rowIndex === 4) {
                                            row.from = parseInt(newInputValues[3].to);
                                            setpageVal(parseInt(newInputValues[3].to));
                                        }
                                        a.push(row);
                                        return a;
                                    }, []);
                                    input.onChange(changedValues);
                                    setTouch(true)
                                    setValidationError(false);
                                    newInputValues.map((item, pos, rows) => {

                                        if (rows.length - 1 != pos) {
                                            if ((item.to >= item.from) && (pos == 0 || (item.from - rows[pos - 1].to === 1)) && item.price >= 0) {

                                            } else {
                                                setValidationError("Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.");
                                                // errors.medical_history_pricing_tier = 'Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.';
                                            }
                                        }
                                    })
                                }

                                return (<div style={{ maxWidth: '550px' }}>
                                    <TableContainer>
                                        <Table aria-label="spanning table" className={"medicalPrice"}>
                                            <TableBody >
                                                {InputValue.map((r, i) => <TableRow key={i}>
                                                    <TableCell>{`Tier ${i + 1}:  Pages `}</TableCell>
                                                    {i === (InputValue.length - 1) ? null :
                                                        <TableCell>
                                                            <TextField className={classes.fieldColor} type="number" disabled={i == 0} defaultValue={r.from} onChange={(e) => InputChange(e.target.value, "from", i)} />
                                                        </TableCell>}
                                                    {i === (InputValue.length - 1) ? null : <TableCell>--</TableCell>}
                                                    {i === (InputValue.length - 1) ? <TableCell>
                                                        <TextField className={classes.fieldColor} disabled value={pageVal || r.from} onChange={(e) => InputChange(e.target.value, "from", i)} />
                                                    </TableCell> : <TableCell>
                                                        <TextField className={classes.fieldColor} type="number" defaultValue={r.to} onChange={(e) => InputChange(e.target.value, "to", i)} />:
                                                    </TableCell>}
                                                    <TableCell colSpan={i === (InputValue.length - 1) ? 3 : 1}>
                                                        {i === 0 ? 'Free' : i === (InputValue.length - 1) ? 'Call for quote and order' : <div style={{ display: 'flex', alignItems: 'center' }}>
                                                            <span style={{ marginBottom: '4px' }}>$</span>
                                                            <TextField className={classes.fieldColor} onChange={(e) => InputChange(e.target.value, "price", i)} defaultValue={r.price} />
                                                        </div>}
                                                    </TableCell>
                                                </TableRow>)}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className={classes.error}>{validationError}</div>
                                    {/* <div className={classes.error}>{((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div> */}

                                </div>)
                            },
                            type: 'component'
                        }
                    ]
                },
                {
                    schemaId: 3,
                    name: 'Free VIP Account',
                    customTitle: (settings, record) => {
                        return <>
                            <span>Free VIP Account for {record?.name}</span>
                            <Tooltip
                                title={<span style={{ fontSize: '12px' }}>If enabled, all users in this practice will get 100% free access to all features.
                                    If the practice is currently a paid subscriber, the paid subscription will be canceled. </span>}
                                placement="right">
                                <InfoIcon style={{ cursor: "pointer", width: "20px", color: "#47AC39", marginLeft: "4px" }} />
                            </Tooltip>
                        </>
                    },
                    title: 'Free VIP Account',
                    create: true,
                    edit: false,
                    view: true,
                    value: 'settings',
                    filter: (record) => record,
                    confirmMessage: false,
                    columns: [
                        {
                            id: 1,
                            value: 'is_vip_account',
                            label: 'Status',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            mandatory: true,
                            size: true,
                            type: 'component',
                            viewSwitch: true,
                            render: ({ record, classes }) => {
                                const { actions, dispatch } = props;
                                const value = record?.is_vip_account;
                                const handleSubmitCheckBox = (isValue) => {
                                    const submitData = Object.assign({}, { practice_id: record.id, is_vip_account: !value });
                                    if (isValue) {
                                        dispatch(actions.vipSubscription(submitData));
                                    } else {
                                        dispatch(actions.vipCancelSubscription(submitData));
                                    }
                                }
                                return (
                                    <AlertDialog
                                        description={value ? `Do you want to disable free VIP account?` : `Do you want to enable free VIP account?<br />If enabled, all users in this practice will get 100% free access to all features.<br /> 
                                        If the practice is currently a paid subscriber, the paid subscription will be canceled.`}
                                        onConfirm={() => handleSubmitCheckBox(!value)}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'No'}
                                        onConfirmPopUpClose >
                                        {(open) => {
                                            return <p style={{ fontWeight: 'bold', fontSize: '14px' }}>
                                                <FormControlLabel
                                                    name="vip_subscription"
                                                    control={<Switch
                                                        className={classes.switchBtn}
                                                        color='primary'
                                                        checked={value ? true : false}
                                                        onChange={open}
                                                    />}
                                                />
                                            </p>
                                        }}
                                    </AlertDialog>
                                )
                            }
                        }
                    ]
                }
            ]
        }
    }



    function settings(user = {}, props) {
        let playType = user?.plan_type?.responding;
        playType = playType && userBasedRespondingPlans.includes(playType);
        const practiceDetails = user?.practiceDetails;

        return {
            section: [
                {
                    schemaId: 1,
                    name: 'Practice Profile',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'practice',
                    filter: (record) => Object.assign({}, { ...record }, { phone: record.phone && new AsYouType('US').input(record.phone) || record.phone, fax: record.fax && new AsYouType('US').input(record.fax) || record.fax }),
                    confirmMessage: "Changes you make to practice settings will apply for all users in your practice. Do you want to continue?",
                    columns: [
                        {
                            id: 1,
                            value: 'id',
                            label: 'ID',
                            editRecord: false,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                        {
                            id: 2,
                            value: 'name',
                            label: 'Practice Name',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            sort: true,
                            type: 'input'
                        },
                        {
                            id: 3,
                            value: 'street',
                            label: 'Street',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input',
                            placeholder: 'eg: 101 Main Street'
                        },
                        {
                            id: 4,
                            value: 'city',
                            label: 'City',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 5,
                            value: 'state',
                            label: 'State',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 6,
                            value: 'zip_code',
                            label: 'Zip Code',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 7,
                            value: 'phone',
                            label: 'Phone',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            max: 14,
                            type: 'phone'
                        },
                        {
                            id: 8,
                            value: 'fax',
                            label: 'Fax',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            max: 14,
                            type: 'phone'
                        },
                        {
                            id: 9,
                            value: 'logoFile',
                            label: 'Logo',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            upload: true,
                            contentType: 'image/*',
                            type: 'upload'
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Responding Plan Type',
                    create: true,
                    view: true,
                    edit: (record) => {
                        return record?.plan_id != "responding_vip" ? true : false;
                    },
                    value: 'responding_subscriptions',
                    viewBtnLabel: 'Plan Type',
                    disableSubmitBtn : (formValue, initialValue) => formValue?.plan_id === initialValue?.plan_id,
                    alertMessage: (row, subscriptions) => {
                        const { propounding_subscriptions } = subscriptions;
                        const expiryDate = row && row.stripe_subscription_data && row.stripe_subscription_data.current_period_end && moment.unix(`${row.stripe_subscription_data.current_period_end}`).format('MM/DD/YYYY') || false;

                        const propoundingSubscriptions = propounding_subscriptions && propounding_subscriptions.stripe_subscription_id && !cancelSubscription(propounding_subscriptions) || false;
                        const planCategory = row && row.plan_category && row.plan_category;

                        return (row && expiryDate && planCategory && propoundingSubscriptions) ? `Do you want to cancel auto-renewal for your practice? Your account is pre-paid for ${planCategory} & ${propounding_subscriptions.plan_category} till ${expiryDate} - users in your practice can continue to use the service till this date.` : (row && expiryDate && planCategory) ? `Do you want to cancel auto-renewal for your practice? Your account is pre-paid for ${planCategory} till ${expiryDate} - users in your practice can continue to use the service till this date.` : `Do you want to cancel auto-renewal for your practice?`;
                    },
                    customConfirmMessage: (row, metaData, formValues, discountDetail, responding_subscriptions, propounding_subscriptions, props) => {
                        const { respondingPlansOptions = [] } = metaData;
                        const currentPlan = respondingPlansOptions && respondingPlansOptions && respondingPlansOptions.length > 0 && formValues && formValues.plan_id && respondingPlansOptions.find(_ => _.value === formValues.plan_id) || false;

                        const oneTimeActivationFee = user && user.practiceDetails && user.practiceDetails.one_time_activation_fee || false;

                        return planConfirmationMessage(currentPlan, discountDetail, oneTimeActivationFee, formValues, 'responding', 'settings', row.plan_id, responding_subscriptions, props, propounding_subscriptions);

                    },
                    discountCode: (formValues) => {
                        if(practiceDetails?.billing_type === 'limited_users_billing') {
                            return true;
                        }
                        return formValues && formValues.discount_code ? true : false;
                    },
                    filter: (record) => {
                        return Object.assign({}, { ...record }, { license_count: practiceDetails?.license_count }) || {}
                    },
                    columns: (record = {}, initialRecord = {}) => {
                        return [
                            {
                                id: 1,
                                value: 'plan_id',
                                label: 'Current Plan',
                                editRecord: true,
                                viewRecord: true,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'select',
                                children: <div className='viewPlanDetails'><span onClick={() => { window.open('https://www.esquiretek.com/pricing/', '_blank') }}>View Plan Details</span></div>,
                                options: "respondingPlansOptions",
                                html: (row, metaData) => {
                                    const { respondingPlansOptions = [] } = metaData;
                                    const vipAccount = row?.plan_id === 'responding_vip' || false;
                                    if (vipAccount) {
                                        return <p style={{ paddingLeft: '25px' }} dangerouslySetInnerHTML={{ __html: 'Free VIP Account' }} />;
                                    } else {
                                        const currentPlan = row && row['plan_id'] && respondingPlansOptions && Array.isArray(respondingPlansOptions) && respondingPlansOptions.length > 0 && respondingPlansOptions.find(_ => _.plan_id === row['plan_id']) || false;
                                        const expiryDate = row && row.stripe_subscription_data && row.stripe_subscription_data.current_period_end && moment.unix(`${row.stripe_subscription_data.current_period_end}`).format('MM/DD/YYYY');

                                        return <p style={{ paddingLeft: '25px' }} dangerouslySetInnerHTML={{ __html: (currentPlan && currentPlan.label || '-') + (row && row.stripe_subscription_id && row['stripe_subscription_data'] && row['stripe_subscription_data']['cancel_at_period_end'] && `<br/> Note: Your plan will end only after this period (${expiryDate})` || '') }} />;
                                    }

                                }
                            },
                            {
                                id: 2,
                                value: "discount_code",
                                label: "Promo Code (If any)",
                                editRecord: (record && initialRecord && record.plan_id && initialRecord.plan_id && record.plan_id !== initialRecord.plan_id || initialRecord && !initialRecord.plan_id) ? true : false,
                                viewRecord: false,
                                viewMode: false,
                                visible: false,
                                type: 'input'
                            },
                            {
                                id: 3,
                                value: 'terms',
                                label: `EsquireTek Terms of Use for Subscription Plans`,
                                editRecord: true,
                                viewRecord: false,
                                viewMode: false,
                                visible: false,
                                type: 'component',
                                render: () => {
                                    return <span dangerouslySetInnerHTML={{ __html: `By subscribing, you agree to our <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'>EsquireTek Terms of Use for Subscription Plans</a>` }} />
                                }
                            },
                            {
                                id: 4,
                                value: 'license_count',
                                label: 'Licenses',
                                editRecord: false,
                                viewRecord: playType ? true : false, 
                                viewMode: false,
                                visible: false,
                                type: 'imput',
                                subLabel: 'Add more licenses'
                            },
                            // {
                            //     id: 3,
                            //     value: 'terms',
                            //     label: `I have read and accepted <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'>EsquireTek Terms of Use for Subscription Plans</a>`,
                            //     editRecord: record && record.plan_id != 'pay_as_you_go' && initialRecord && initialRecord.plan_id !== record.plan_id ? true : false,
                            //     viewRecord: false,
                            //     viewMode: false,
                            //     visible: false,
                            //     mandatory: true,
                            //     type: 'checkbox',
                            // }
                        ]
                    },
                    buyLicenseColumns: (record = {}, initialRecord = {}) => {
                        return [
                            {
                                id: 1,
                                value: 'license_count',
                                label: 'Licenses count',
                                editRecord: true,
                                viewRecord: true,
                                viewMode: false,
                                visible: false,
                                type: 'number',
                                enablePlusMinusIcon: true,
                                mandatory: true,
                                disabledArrow: true,
                                inputProps: { min: 1 },
                                addMoreLicense: record?.license_count >= 1 ? true : false,
                            },
                            // {
                            //     id: 2,
                            //     value: 'discount_code',
                            //     label: 'Promo Code (If any)',
                            //     editRecord: true,
                            //     viewRecord: false,
                            //     viewMode: false,
                            //     visible: false,
                            //     type: 'input',
                            // },
                            {
                                id: 3,
                                value: 'terms',
                                label: `EsquireTek Terms of Use for Subscription Plans`,
                                editRecord: true,
                                viewRecord: false,
                                viewMode: false,
                                visible: false,
                                type: 'component',
                                render: ({ classes }) => {
                                    return <div>
                                        <span  dangerouslySetInnerHTML={{ __html: `By subscribing, you agree to our <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'> EsquireTek Terms of Use for Subscription Plans</a>` }} className={classes.link} />
                                    </div>
                                }
                            },
                        ]
                    },
                    buyCustomLicenseConfirmationMessage: (formValue, licenseDetails, metaData, responding_subscriptions, propounding_subscriptions, props) => {
                        return buyLicenseConfirmationMessage(formValue, licenseDetails, metaData, responding_subscriptions, propounding_subscriptions, props);
                    },
                },
                {
                    schemaId: 3,
                    name: 'Propounding Feature Add-on',
                    create: true,
                    view: true,
                    enableSubmitBtn: true,
                    edit: (propoundingSubscriptions, respondingSubscriptions) => {
                        const respondingCancelSubscription = respondingSubscriptions && respondingSubscriptions.stripe_subscription_id && cancelSubscription(respondingSubscriptions) || false;

                        const vip_account = propoundingSubscriptions?.plan_id === 'propounding_vip' || false;
                        const subscriptionCancel = propoundingSubscriptions?.stripe_subscription_id || false;
                        if(vip_account) return false;

                        return respondingCancelSubscription ? false : !subscriptionCancel ? true : false;
                    },
                    label: (record) => {
                        let labelValue = record && record.plan_id ? `Change` : `Upgrade`;
                        return labelValue;
                    },
                    infoMessage: true,
                    value: 'propounding_subscriptions',
                    alertMessage: (row) => {
                        const expiryDate = row && row.stripe_subscription_id && row.stripe_subscription_data && row.stripe_subscription_data.current_period_end && moment.unix(`${row.stripe_subscription_data.current_period_end}`).format('MM/DD/YYYY') || row && row.stripe_subscription_id && row.stripe_subscription_data && row.subscribed_valid_till && moment(`${row.subscribed_valid_till}`).format('MM/DD/YYYY') || false;

                        const planCategory = row && row.plan_category && row.plan_category;
                        return (row && expiryDate && planCategory) && `Do you want to cancel auto-renewal for propounding? Your account is pre-paid for ${planCategory} till ${expiryDate} - users in your practice can continue to use the service till this date.` || `Do you want to cancel auto-renewal for propounding?`;
                    },
                    customConfirmMessage: (row, metaData, formValues, discountDetail, responding_subscriptions, propounding_subscriptions) => {
                        const { propoundingPlansOptions: propoundPlans = [], respondingPlansOptions: respondPlans = [] } = metaData;

                        const currentRespondingPlan = responding_subscriptions && responding_subscriptions['plan_id'] && respondPlans && Array.isArray(respondPlans) && respondPlans.length > 0 && respondPlans.find(_ => _.plan_id === responding_subscriptions['plan_id']) || false;

                        const currentPlan = currentRespondingPlan && currentRespondingPlan['plan_id'] && propoundPlans && Array.isArray(propoundPlans) && propoundPlans.length > 0 && propoundPlans.find(_ => _.validity === currentRespondingPlan['validity']) || false;

                        const oneTimeActivationFee = user && user.practiceDetails && user.practiceDetails.one_time_activation_fee || false;

                        return planConfirmationMessage(currentPlan, discountDetail, oneTimeActivationFee, formValues, 'propounding', 'settings', row.plan_id, responding_subscriptions, props);

                    },
                    customAlertMessage: (planType) => (planType && ['free_trial'].includes(planType)) ? `Propounding feature is not available for free trial accounts. Once you start your subscription, you will be able to add this feature.` : (planType && ['tek_as_you_go', 'pay_as_you_go'].includes(planType) || !planType) ? `Propounding feature is not available for your account. Once you start your subscription, you will be able to add this feature.` : false,
                    discountCode: (formValues) => {
                        if(practiceDetails?.billing_type === 'limited_users_billing') {
                            return true;
                        }
                        return formValues && formValues.discount_code ? true : false;
                    },
                    filter: (record) => record || {},
                    columns: (record = {}, initialRecord = {}) => [
                        {
                            id: 1,
                            value: 'plan_id',
                            label: 'Status',
                            editRecord: false,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            type: 'select',
                            children: <div className='viewPlanDetails'><span onClick={() => { window.open('https://www.esquiretek.com/pricing/', '_blank') }}>View Plan Details</span></div>,
                            options: "propoundingPlansOptions",
                            html: (row, metaData) => {
                                const { propoundingPlansOptions = [] } = metaData;
                                const { practice } = props;
                                const currentPlan = row && row['plan_id'] && propoundingPlansOptions && Array.isArray(propoundingPlansOptions) && propoundingPlansOptions.length > 0 && propoundingPlansOptions.find(_ => _.plan_id === row['plan_id']);
                                const vipAccount = row?.plan_id === 'propounding_vip' || false;
                                if (vipAccount) {
                                    return <p style={{ paddingLeft: '25px' }} dangerouslySetInnerHTML={{ __html: 'Free VIP Account' }} />;
                                } else {
                                    const expiryDate = row && row.stripe_subscription_id && (row.stripe_subscription_id.startsWith('sub_') && row.stripe_subscription_data && row.stripe_subscription_data.current_period_end && moment.unix(`${row.stripe_subscription_data.current_period_end}`).format('MM/DD/YYYY') || row.stripe_subscription_id.startsWith('pi_') && row.subscribed_valid_till && moment(row.subscribed_valid_till).format('MM/DD/YYYY'));

                                    return <p style={{ paddingLeft: '25px' }} dangerouslySetInnerHTML={{ __html: (currentPlan ? `Feature added to the plan` : `Feature not added to the plan. Upgrade to use Propounding feature<div className='viewPlanDetails'><a href='https://www.esquiretek.com/pricing/' style='color:#2ca01c' target='_blank'>View Plan Details</a></div>`) + (row && row['stripe_subscription_id'] && cancelSubscription(row, practice) ? `<br/> Note: Your add-on will end only after this period (${expiryDate})` : '') || '-' }} />;
                                }
                            }
                        },
                        {
                            id: 2,
                            value: 'propounding_note',
                            label: '',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            render: () => {
                                const { responding_subscriptions, practice } = props;
                                const respondingPlanDetails = responding_subscriptions && responding_subscriptions['planDetails'] && responding_subscriptions['planDetails'];

                                const subscription_cancel_at_period_end = responding_subscriptions && responding_subscriptions.stripe_subscription_id && cancelSubscription(responding_subscriptions) || false;

                                const planType = respondingPlanDetails && respondingPlanDetails['plan_type'] && respondingPlanDetails['plan_type'].includes('yearly') ? 'yearly' : 'monthly';

                                const is_plan_type = respondingPlanDetails && respondingPlanDetails['plan_type'] && respondingPlanDetails['plan_type'].includes('yearly') ? 'year' : 'month';

                                const licenseCount = practice?.propounding_price?.total_licese_count;

                                let propounding_current_invoice = practice?.propounding_price?.current_invoice;
                                propounding_current_invoice = propounding_current_invoice && parseIntPrice(propounding_current_invoice) || propounding_current_invoice;
                                let propound_actual_price = practice?.propounding_price?.actual_price;
                                propound_actual_price = propound_actual_price && parseIntPrice(propound_actual_price) || propound_actual_price;
                                let propound_next_invoice = practice?.propounding_price?.next_invoice;
                                propound_next_invoice = propound_next_invoice && parseIntPrice(propound_next_invoice) || propound_next_invoice;

                                let responding_next_invoice = practice?.responding_price?.next_invoice;
                                responding_next_invoice = responding_next_invoice && parseIntPrice(responding_next_invoice) || responding_next_invoice;
                                const plan_validity = practice?.propounding_price?.plan_validity;
                                const propoundinf_day_diff = practice?.propounding_price?.prorated?.days_difference;
                                const dayDiff = plan_validity && propoundinf_day_diff ? parseInt(plan_validity) - parseInt(propoundinf_day_diff) : false;

                                const subscribe_on = responding_subscriptions && responding_subscriptions.stripe_subscription_id && responding_subscriptions.subscribed_on;
                                const dateDiff = subscribe_on && dateDiffInDays(subscribe_on) || false;

                                let propounding_plan_note;
                                const next_recurring_charges = !subscription_cancel_at_period_end && `For the next recurring payment onwards, you will see two charges - $${responding_next_invoice}/${planType} for responding and $${propound_next_invoice}/${planType} for propounding.` || '';
                                if(practice?.billing_type === 'limited_users_billing') {
                                    
                                    let userLicense = `for ${licenseCount} user licenses`;

                                    propounding_plan_note = dateDiff ? `Propounding feature costs $${propound_actual_price}/${is_plan_type} ${userLicense} (in addition to your current recurring charge of $${responding_next_invoice}/${is_plan_type}). Today, you will be charged only the prorated portion for the remaining days in the ${is_plan_type} (${dayDiff} days, $${propounding_current_invoice}). ${next_recurring_charges}` : `Propounding feature costs $${propound_actual_price}/${is_plan_type} ${userLicense} (in addition to your current recurring charge of $${responding_next_invoice}/${is_plan_type}). ${next_recurring_charges}`;
                                } else {
                                    propounding_plan_note = dateDiff ? `Propounding feature costs $${propound_actual_price}/${is_plan_type} (in addition to your current recurring charge of $${responding_next_invoice}/${is_plan_type}). Today, you will be charged only the prorated portion for the remaining days in the ${is_plan_type} (${dayDiff} days, $${propounding_current_invoice}). ${next_recurring_charges}` : `Propounding feature costs $${propound_actual_price}/${is_plan_type} (in addition to your current recurring charge of $${responding_next_invoice}/${is_plan_type}). ${next_recurring_charges}`;
                                }

                                return (<div style={{ fontSize: '14px', fontWeight: 'bold' }}>{propounding_plan_note}</div>);
                            },
                            type: 'component'
                        },
                        {
                            id: 2,
                            value: "discount_code",
                            label: "Promo Code (If any)",
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                        {
                            id: 3,
                            value: 'terms',
                            label: `EsquireTek Terms of Use for Subscription Plans`,
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'component',
                            render: () => {
                                return <span dangerouslySetInnerHTML={{ __html: `By subscribing, you agree to our <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'>EsquireTek Terms of Use for Subscription Plans</a>` }} />
                            }
                        },
                        // {
                        //     id: 3,
                        //     value: 'terms',
                        //     label: `I have read and accepted <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'>EsquireTek Terms of Use for Subscription Plans</a>`,
                        //     editRecord: true,
                        //     viewRecord: false,
                        //     viewMode: false,
                        //     visible: false,
                        //     mandatory: true,
                        //     type: 'checkbox',
                        // }
                    ]
                },
                {
                    schemaId: 4,
                    name: () => {
                        return <>
                            <span>Attorney Response Tracking</span>
                            <Tooltip
                                title={<span style={{ fontSize: '12px' }}>When enabled, you can see previous versions of responses to refer to or restore from.</span>}
                                placement="right">
                                <InfoIcon style={{ cursor: "pointer", width: "20px", color: "#47AC39", marginLeft: "4px" }} />
                            </Tooltip>
                        </>
                    },
                    create: true,
                    view: true,
                    value: 'practice',
                    filter: (record) => record || {},
                    columns: [
                        {
                            id: 1,
                            value: 'global_attorney_response_tracking',
                            label: 'Status',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            mandatory: true,
                            type: 'component',
                            responseTracking: true,
                            render: ({ record, classes }) => {
                                const { actions, dispatch } = props;
                                const tracking = record?.global_attorney_response_tracking;
                                const handleSubmitCheckBox = () => {
                                    const data = Object.assign({}, { ...record, global_attorney_response_tracking: !tracking });
                                    dispatch(actions.globalAttorneyResponseTracking(data));
                                }
                                return (
                                    <AlertDialog
                                        description={tracking ? `Do you want to disable the attorney response tracking?` : `Do you want to enable the attorney response tracking?`}
                                        onConfirm={handleSubmitCheckBox}
                                        btnLabel1={'Yes'}
                                        btnLabel2={'No'}
                                        onConfirmPopUpClose >
                                        {(open) => {
                                            return <div>
                                                <FormControlLabel
                                                    style={{ paddingLeft: '25px' }}
                                                    name="tracing"
                                                    control={<Switch
                                                        className={classes.switchBtn}
                                                        color='primary'
                                                        checked={tracking ? true : false}
                                                        onChange={open}
                                                    />}
                                                />
                                            </div>
                                        }}
                                    </AlertDialog>
                                )
                            }
                        }
                    ]
                },
                {
                    schemaId: 5,
                    name: 'Credit Card Info',
                    create: true,
                    view: true,
                    edit: true,
                    value: 'billing',
                    confirmMessage: "Changes to credit card settings will apply for all future orders from this practice. Do you want to continue?",
                    filter: (record) => record && record.stripe_payment_methods && record.stripe_payment_methods.length > 0 && Object.assign({}, { ...record.stripe_payment_methods[0].billing_details || {} }, { card: `XXXX XXXX XXXX ${record.stripe_payment_methods[0].card.last4}`, lastUpdated: moment.unix(`${record.stripe_payment_methods[0].created}`).format('MM/DD/YYYY') }) || {},
                    columns: [
                        {
                            id: 1,
                            value: 'name',
                            label: 'Card Holder’s Name',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'input'
                        },
                        {
                            id: 2,
                            value: 'card',
                            label: 'Card Number',
                            editRecord: false,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                        {
                            id: 3,
                            value: 'cardDetails',
                            label: 'Card Details',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            showRequired: true,
                            type: 'stripe'
                        },
                        {
                            id: 4,
                            value: 'badge',
                            label: false,
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'component',
                            style: { paddingBottom: 0, paddingTop: 0 },
                            render: () => {
                                const cards = ['visa', 'masterCard', 'amex', 'discover', 'cardO', 'unionPay'];
                                return (
                                    <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                        <div>
                                            {cards?.map((e, i) => <Icons type={e} key={i} style={{ width: '40px', marginRight: '6px' }} />)}
                                        </div>
                                        <div>
                                            <Icons type={'stripe'} style={{ width: '100px' }} />
                                        </div>
                                    </div>
                                )
                            }
                        },
                        {
                            id: 5,
                            value: 'lastUpdated',
                            label: 'Last Updated',
                            editRecord: false,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        }

                    ]

                },
                {
                    schemaId: 6,
                    name: 'App Version',
                    create: true,
                    view: user && (user.email === 'shahidulkareem@gmail.com' || user.email === 'siva@ospitek.com') ? true : false,
                    edit: true,
                    value: 'version',
                    confirmMessage: "Changing the version will reset the cache data for all Users.Do you want to continue?",
                    filter: (record) => record || {},
                    columns: [
                        {
                            id: 1,
                            value: 'version',
                            label: 'Version',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: false,
                            type: 'number'
                        }
                    ]
                }
            ]
        }
    }


    function globalSettings(user) {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'subscriptions',
                    label: 'SUBSCRIPTION PRICING',
                    path: '/rest/plans',
                    create: false,
                    edit: false,
                    view: false,
                    admin: true
                },
                {
                    schemaId: 2,
                    name: 'Pricing',
                    label: 'ON DEMAND PRICING',
                    path: '',
                    create: true,
                    edit: true,
                    view: true,
                    admin: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Pricing changes will apply for all practices. Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'no_of_docs_free_tier',
                            label: 'Number of docs in free tier',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: false,
                            type: 'number'
                        },
                        {
                            id: 2,
                            value: 'template_generation_price.FROGS',
                            label: 'FROGS Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'FROGS',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 3,
                            value: 'final_doc_generation_price.FROGS',
                            label: 'FROGS Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'FROGS',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 4,
                            value: 'template_generation_price.SPROGS',
                            label: 'SPROGS Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'SPROGS',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 5,
                            value: 'final_doc_generation_price.SPROGS',
                            label: 'SPROGS Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            docType: 'SPROGS',
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 6,
                            value: 'template_generation_price.RFPD',
                            label: 'RFPD Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFPD',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 7,
                            value: 'final_doc_generation_price.RFPD',
                            label: 'RFPD Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFPD',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 8,
                            value: 'template_generation_price.RFA',
                            label: 'RFA Template Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFA',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 9,
                            value: 'final_doc_generation_price.RFA',
                            label: 'RFA Final Doc Generation',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFA',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 10,
                            value: 'teksign',
                            label: 'TekSign',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'teksign',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 11,
                            value: 'pos',
                            label: 'Proof of Service',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'pos',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number'
                        },
                        {
                            id: 12,
                            value: 'free_user_limit',
                            label: 'Free User Limit',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'number'
                        }
                    ]
                },
                {
                    schemaId: 3,
                    name: 'Medical History Pricing Tiers',
                    label: 'MEDICAL HISTORY PRICING',
                    path: '',
                    create: true,
                    edit: true,
                    view: true,
                    admin: true,
                    value: `pricing`,
                    filter: (record) => record,
                    confirmMessage: "Medical History Pricing Tiers changes will apply for all practices. Do you want to save this change?",
                    initialValues: {
                        medical_history_minimum_pricing: 0,
                        medical_history_pricing_tier: [
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                to: 0,
                                price: 0
                            },
                            {
                                from: 0,
                                custom_quote_needed: true
                            }
                        ]
                    },
                    columns: [
                        {
                            id: 1,
                            value: 'medical_history_minimum_pricing',
                            label: 'Minimum Pricing',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            docType: 'RFA',
                            mandatory: true,
                            prefix: '$',
                            dollar: true,
                            type: 'number',
                            style: {
                                position: 'relative',
                                top: '18px',
                                marginLeft: '15px',
                                marginRight: '15px'
                            }
                        },
                        {
                            id: 2,
                            value: 'medical_history_pricing_tier',
                            label: '',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            render: ({ input, classes, meta: { touched, error, warning } }) => {
                                let InputValue = input.value && Array.isArray(input.value) && input.value.length > 0 ? input.value : [{ "from": 0, "to": 0, "price": 0 }, { "from": 0, "to": 0, "price": 0 }, { "from": 0, "to": 0, "price": 0 }, { "from": 0, "to": 0, "price": 0 }, { "from": 0, "custom_quote_needed": true }];
                                const [touch, setTouch] = useState(false);
                                const [validationError, setValidationError] = useState(false);
                                const [pageVal, setpageVal] = useState(false);
                                const InputChange = (value, name, index) => {
                                    let newInputValues = InputValue.map(obj => ({ ...obj }));
                                    const changedValues = newInputValues.reduce((a, row, rowIndex) => {
                                        if (rowIndex === index && name === 'from') {
                                            row.from = parseInt(value);
                                        } else if (rowIndex === index && name === 'to') {
                                            row.to = parseInt(value);
                                        } else if (rowIndex === index && name === 'price') {
                                            row.price = parseFloat(value);
                                        }
                                        if (rowIndex === 4) {
                                            row.from = parseInt(newInputValues[3].to);
                                            setpageVal(parseInt(newInputValues[3].to));
                                        }
                                        a.push(row);
                                        return a;
                                    }, []);
                                    input.onChange(changedValues);
                                    setTouch(true)
                                    setValidationError(false);
                                    newInputValues.map((item, pos, rows) => {

                                        if (rows.length - 1 != pos) {
                                            if ((item.to >= item.from) && (pos == 0 || (item.from - rows[pos - 1].to === 1)) && item.price >= 0) {

                                            } else {
                                                setValidationError("Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.");
                                                // errors.medical_history_pricing_tier = 'Tier setup looks incorrect. This can lead to billing errors, please ensure that Tiers don’t overlap and there are no gaps between Tiers.';
                                            }
                                        }
                                    })
                                }

                                return (<div style={{ maxWidth: '550px' }}>
                                    <TableContainer>
                                        <Table aria-label="spanning table" className={"medicalPrice"}>
                                            <TableBody >
                                                {InputValue.map((r, i) => <TableRow key={i}>
                                                    <TableCell>{`Tier ${i + 1}:  Pages `}</TableCell>
                                                    {i === (InputValue.length - 1) ? null :
                                                        <TableCell>
                                                            <TextField className={classes.fieldColor} type="number" disabled={i == 0} defaultValue={r.from} onChange={(e) => InputChange(e.target.value, "from", i)} />
                                                        </TableCell>}
                                                    {i === (InputValue.length - 1) ? null : <TableCell>--</TableCell>}
                                                    {i === (InputValue.length - 1) ? <TableCell>
                                                        <TextField className={classes.fieldColor} disabled value={pageVal || r.from} onChange={(e) => InputChange(e.target.value, "from", i)} />
                                                    </TableCell> : <TableCell>
                                                        <TextField className={classes.fieldColor} type="number" defaultValue={r.to} onChange={(e) => InputChange(e.target.value, "to", i)} />:
                                                    </TableCell>}
                                                    <TableCell colSpan={i === (InputValue.length - 1) ? 3 : 1}>
                                                        {i === 0 ? 'Free' : i === (InputValue.length - 1) ? 'Call for quote and order' : <div style={{ display: 'flex', alignItems: 'center' }}>
                                                            <span style={{ marginBottom: '4px' }}>$</span>
                                                            <TextField className={classes.fieldColor} onChange={(e) => InputChange(e.target.value, "price", i)} defaultValue={r.price} />
                                                        </div>}
                                                    </TableCell>
                                                </TableRow>)}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className={classes.error}>{validationError}</div>
                                    {/* <div className={classes.error}>{((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div> */}

                                </div>)
                            },
                            type: 'component'
                        }
                    ]
                },
                {
                    schemaId: 4,
                    name: 'Activation Fee Waiver',
                    label: 'ACTIVATION FEE WAIVER',
                    path: '/rest/fee-waiver',
                    create: false,
                    edit: false,
                    view: false,
                    value: `activation`,
                    admin: user && user.role == 'manager' ? true : false,
                    style: { marginTop: '25px' }
                },
                {
                    schemaId: 5,
                    name: 'Activation Fee Discount Codes',
                    label: 'ACTIVATION FEE PROMO CODES',
                    path: '/rest/discount-code',
                    create: false,
                    edit: false,
                    view: false,
                    value: `activation`,
                    admin: user && user.role == 'manager' ? true : false,
                    style: { marginTop: '25px' }
                }
                // {
                //     schemaId: 4,
                //     name: 'HIPAA Complaince',
                //     label: 'OTHER SETTINGS',
                //     path: '',
                //     create: true,
                //     edit: true,
                //     view: true,
                //     value: 'pricing',
                //     filter: (record) => record,
                //     confirmMessage: "Do you want to save this change?",
                //     columns: [
                //         {
                //             id: 1,
                //             value: 'session_timeout',
                //             label: 'Session Expiration Timeout (in seconds)',
                //             editRecord: true,
                //             viewRecord: true,
                //             viewMode: false,
                //             visible: false,
                //             mandatory: true,
                //             type: 'number'
                //         }
                //     ]
                // },
            ]
        }
    }

    function adminMedicalHistory() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'practice_name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'assigned_to',
                    label: 'ASSIGNED TO',
                    sortColumn: 'assigned_to',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'request_date',
                    label: 'REQUEST DATE',
                    sortColumn: 'request_date',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row && row['request_date'] && moment(row['request_date']).format('MM/DD/YY HH:mm:ss'),
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'pages',
                    label: 'PAGES',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'amount_charged',
                    label: 'PRICE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                }
            ]
        }
    }

    function medicalHistory() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'practice_name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'assigned_to',
                    label: 'ASSIGNED TO',
                    sortColumn: 'assigned_to',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'request_date',
                    label: 'REQUEST DATE',
                    sortColumn: 'request_date',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'pages',
                    label: 'PAGES',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                }
            ],
            medicalDocuments: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'createdAt',
                    label: 'UPLOADED DATE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row && row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                },
                {
                    id: 4,
                    value: 'comment',
                    label: 'COMMENT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'pages',
                    label: 'PAGES',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 's3_file_key',
                    label: 'DOCUMENT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const value = row.s3_file_key;
                        return <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(await getSecretURL(value))} />
                    },
                    type: 'download'
                }
            ],
            uploadSummary: [
                {
                    id: 1,
                    value: 'summery_url',
                    label: 'Upload Document',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf, .doc, .docx, .xls, .xlsx, .xlsb, .xlsm, .csv',
                    multiple: true,
                    max: 4,
                    type: 'upload',
                    errorType: true
                }
            ],
            statusForm: (record = {}) => [
                {
                    id: 1,
                    value: 'assigned_to',
                    label: 'Assigned To',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    style: { marginLeft: '15px', minWidth: '150px' },
                    disableOptons: {
                        edit: record.assigned_to && record.assigned_to != 'N/A' && record.status && ['In Progress', 'Complete'].includes(record.status) ? true : false
                    },
                    options: 'usersOptions',
                    mandatory: true,
                    type: 'select'
                },
                {
                    id: 2,
                    value: 'status',
                    label: 'Status',
                    editRecord: true,
                    disableOptons: {
                        edit: record.assigned_to && record.assigned_to != 'N/A' ? false : true
                    },
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    style: { marginLeft: '15px', minWidth: '150px' },
                    options: [
                        {
                            value: 'New Request',
                            label: 'New Request',
                            disabled: true
                        },
                        {
                            value: 'Assigned',
                            label: 'Assigned'
                        },
                        {
                            value: 'In Progress',
                            label: 'In Progress'
                        },
                        {
                            value: 'Complete',
                            label: 'Complete',
                            disabled: record.summery_url && record.summery_url != '' ? false : true
                        }
                    ],
                    mandatory: true,
                    type: 'select'
                }
            ]
        }
    }


    function filterColumns() {
        return {
            cases: [
                {
                    value: 'attorneys',
                    options: 'usersFilterOptions'
                }
            ],
            histogram: [
                {
                    value: '2024',
                    label: '2024'
                },
                {
                    value: '2023',
                    label: '2023'
                },
                {
                    value: '2022',
                    label: '2022'
                },
                {
                    value: '2021',
                    label: '2021'
                },
                {
                    value: '2020',
                    label: '2020'
                }
            ],
            statistics: [
                {
                    value: 'last_30_days',
                    label: 'Last 30 days'
                },
                {
                    value: 'this_month',
                    label: 'This Month'
                },
                {
                    value: 'this_quarter',
                    label: 'This Quarter'
                },
                {
                    value: 'this_year',
                    label: 'This Year'
                },
                {
                    value: 'last_month',
                    label: 'Last Month'
                },
                {
                    value: 'last_quarter',
                    label: 'Last Quarter'
                },
                {
                    value: 'last_year',
                    label: 'Last Year'
                },
                {
                    value: 'custom_filter',
                    label: 'Custom'
                }
            ],
            lawyerFilter: {
                value: 'lawyer_response_filter',
                options: [
                    {
                        value: 'All',
                        label: 'Attorney Response: All'
                    },
                    {
                        value: 'NotStarted',
                        label: 'Not Started'
                    },
                    {
                        value: 'Draft',
                        label: 'Draft Saved'
                    },
                    {
                        value: 'Final',
                        label: 'Response Finalized'
                    },
                ]
            },
            clientFilter: {
                value: 'client_response_filter',
                options: [
                    {
                        value: 'All',
                        label: 'Client Response: All'
                    },
                    {
                        value: 'SentToClient',
                        label: 'Questions Sent To Client'
                    },
                    {
                        value: 'ClientResponseAvailable',
                        label: 'Responded By Client'
                    },
                ]
            },
            partiesFilter: {
                value: 'party_id',
                options: 'partiesOptions'
            },
            orders: [
                {
                    value: 'product',
                    options: [
                        {
                            value: '',
                            label: 'Product: All',
                        },
                        {
                            value: 'IDC',
                            label: 'IDC (Initial Disclosures)'
                        },
                        {
                            value: 'FROGS',
                            label: 'FROGS'
                        },
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        },
                        {
                            label: 'BOP (Bill of Particulars)',
                            value: 'BOP'
                        },
                        {
                            label: 'ODD (Omnibus Discovery Demands)',
                            value: 'ODD'
                        },
                        {
                            label: 'NDI (Notice for Discovery and Inspection)',
                            value: 'NDI'
                        },
                        {
                            label: 'NTA (Notice to Admit)',
                            value: 'NTA'
                        },
                        {
                            value: 'MEDICAL HISTORY',
                            label: 'MEDICAL HISTORY'
                        },
                        {
                            value: 'TEKSIGN',
                            label: 'TEKSIGN'
                        }
                    ]
                },
                {
                    value: 'type',
                    options: [
                        {
                            value: '',
                            label: 'Type: All',
                        },
                        {
                            value: 'template',
                            label: 'Shell'
                        },
                        {
                            value: 'final_doc',
                            label: 'Discovery'
                        },
                        {
                            value: 'medical',
                            label: 'Medical'
                        },
                        {
                            value: 'teksign',
                            label: 'TekSign'
                        },
                        {
                            value: 'pos',
                            label: 'PoS'
                        }
                    ]
                }
            ],
            adminOrders: [
                {
                    value: 'product',
                    options: [
                        {
                            value: '',
                            label: 'Product: All',
                        },
                        {
                            value: 'IDC',
                            label: 'IDC'
                        },
                        {
                            value: 'FROGS',
                            label: 'FROGS'
                        },
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        },
                        {
                            label: 'BOP',
                            value: 'BOP'
                        },
                        {
                            label: 'ODD',
                            value: 'ODD'
                        },
                        {
                            label: 'NDI',
                            value: 'NDI'
                        },
                        {
                            label: 'NTA',
                            value: 'NTA'
                        },
                        {
                            value: 'MEDICAL HISTORY',
                            label: 'MEDICAL HISTORY'
                        },
                        {
                            value: 'TEKSIGN',
                            label: 'TEKSIGN'
                        }
                    ]
                },
                {
                    value: 'type',
                    options: [
                        {
                            value: '',
                            label: 'Type: All',
                        },
                        {
                            value: 'template',
                            label: 'Shell'
                        },
                        {
                            value: 'final_doc',
                            label: 'Discovery'
                        },
                        {
                            value: 'medical',
                            label: 'Medical'
                        },
                        {
                            value: 'teksign',
                            label: 'TekSign'
                        },
                        {
                            value: 'pos',
                            label: 'PoS'
                        }
                    ]
                }
            ],
            medicalHistory: [
                {
                    value: 'status',
                    options: [
                        {
                            value: '',
                            label: 'Status: All',
                        },
                        {
                            value: 'New Request',
                            label: 'New Request'
                        },
                        {
                            value: 'Assigned',
                            label: 'Assigned'
                        },
                        {
                            value: 'In Progress',
                            label: 'In Progress'
                        },
                        {
                            value: 'Complete',
                            label: 'Complete'
                        }
                    ]
                }
            ],
            objections: [
                {
                    value: 'state',
                    options: 'stateFilterOptions'
                }
            ],
            practices: [
                {
                    value: 'type',
                    options: [
                        {
                            value: '',
                            label: 'Plan Type: All'
                        },
                        {
                            value: 'free_trial',
                            label: 'Free Trial'
                        },
                        {
                            value: 'paid_accounts',
                            label: 'Paid Accounts'
                        },
                        {
                            value: 'vip_accounts',
                            label: 'VIP Accounts'
                        },
                    ]
                }
            ],
            practicesCustomFilter: {
                value: 'duration',
                options: [
                    {
                        value: 'week',
                        label: '1 Week'
                    },
                    {
                        value: 'month',
                        label: '1 Month'
                    },
                    {
                        value: 'quarter',
                        label: '1 Quarter'
                    },
                    {
                        value: 'year',
                        label: '1 year'
                    },
                ]
            }
            // practices: [
            //     {
            //         value: 'plan_type',
            //         options: [
            //             {
            //                 value: 'all',
            //                 label: 'Plan Type: All'
            //             },
            //             {
            //                 value: 'pay_as_you_go',
            //                 label: 'Tek As You Go'
            //             },
            //             {
            //                 value: 'free_trial',
            //                 label: 'Free Trial'
            //             },
            //             {
            //                 value: 'responding_monthly',
            //                 label: 'Responding Monthly'
            //             },
            //             {
            //                 value: 'responding_yearly',
            //                 label: 'Responding Yearly'
            //             },
            //             {
            //                 value: 'propounding_monthly',
            //                 label: 'Propounding Monthly'
            //             },
            //             {
            //                 value: 'propounding_yearly',
            //                 label: 'Propounding Yearly'
            //             },
            //             {
            //                 value: 'canceled_subscription',
            //                 label: 'Subscription Canceled'
            //             }
            //         ]
            //     }                
            // ],
            // practiceUsers: [
            //     {
            //         value: 'plan_type',
            //         options: [
            //             {
            //                 value: 'all',
            //                 label: 'Plan Type: All'
            //             },
            //             {
            //                 value: 'pay_as_you_go',
            //                 label: 'Tek As You Go'
            //             },
            //             {
            //                 value: 'free_trial',
            //                 label: 'Free Trial'
            //             },
            //             {
            //                 value: 'responding_monthly',
            //                 label: 'Responding Monthly'
            //             },
            //             {
            //                 value: 'responding_yearly',
            //                 label: 'Responding Yearly'
            //             },
            //             {
            //                 value: 'propounding_monthly',
            //                 label: 'Propounding Monthly'
            //             },
            //             {
            //                 value: 'propounding_yearly',
            //                 label: 'Propounding Yearly'
            //             },
            //             {
            //                 value: 'canceled_subscription',
            //                 label: 'Subscription Canceled'
            //             }
            //         ]
            //     }                
            // ]
        }
    }


    function legalForms(record = {}, metaData) {
        const state = metaData && metaData.states && metaData.states.length > 0 && metaData.states.filter(state => record.state === state.state_code.toUpperCase());
        const docType = state && state.length > 0 && state[0].document_type && typeof state[0].document_type === 'string' && state[0].document_type.split(',') || [];

        return {
            forms: [
                {
                    id: 1,
                    value: 'idc',
                    label: 'IDC',
                    title: 'Initial Disclosures',
                    formType: 'idc',
                    content: 'Please use the option below to select questions for Initial Disclosures. You can create multiple Initial Disclosure forms',
                    editForm: true,
                    viewForm: docType.includes('idc') ? true : false,
                    btnLabel: 'Initial Disclosure Form',
                    btnLabel1: 'Use Judicial Council Form',
                    btnLabel2: 'Initial Disclosure Form',
                    btnLabel3: 'Use Saved Templates',
                    disableCancelButton: true,
                    type: 'document'
                },
                {
                    id: 1,
                    value: 'frogs',
                    label: 'FROGS',
                    title: `Form Interrogatories (FROGS)`,
                    content: 'Please use the link below to upload the .pdf version of the Form Interrogatories you have received from opposing counsel. Once you see the green check mark click \"Upload\" to finish the process. After you click \"upload\" our machine will get to work and will extract each and every question sent to you by opposing counsel. If the document does not read correctly you can click \"Use Judicial Council Form\" to click the questions that you need formatted.',
                    editForm: true,
                    viewForm: docType.includes('frogs') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    btnLabel1: 'Use Judicial Council Form',
                    btnLabel2: 'Initial Disclosure Form',
                    btnLabel3: 'Use Saved Templates',
                    disableCancelButton: true,
                    type: 'document'
                },
                {
                    id: 2,
                    value: 'sprogs',
                    label: 'SPROGS',
                    title: `${record.state !== 'CA' ? 'Interrogatories' : 'Specially Prepared Interrogatories (SPROGS)'}`,
                    content: `Please use the link below to upload ${record.state !== 'CA' ? 'Interrogatories' : 'Special Interrogatories'} you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.`,
                    editForm: true,
                    viewForm: docType.includes('sprogs') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 3,
                    value: 'rfpd',
                    label: 'RFPD',
                    title: 'Requests For Production of Documents (RFPD)',
                    content: "Please use the link below to upload the Request for Production of Documents you have received from opposing counsel.  Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('rfpd') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 4,
                    value: 'rfa',
                    label: 'RFA',
                    title: 'Request For Admissions (RFA)',
                    content: "Please use the link below to upload Requests for Admissions you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('rfa') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    style: { marginLeft: 'auto' },
                    type: 'document'
                }

            ],
            federalForms: [
                {
                    id: 1,
                    value: 'sprogs',
                    label: 'SPROGS',
                    title: 'Interrogatories',
                    content: "Please use the link below to upload Interrogatories you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: true,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 2,
                    value: 'rfpd',
                    label: 'RFPD',
                    title: 'Requests For Production of Documents (RFPD)',
                    content: "Please use the link below to upload the Request for Production of Documents you have received from opposing counsel.  Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: true,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 3,
                    value: 'rfa',
                    label: 'RFA',
                    title: 'Request For Admissions (RFA)',
                    content: "Please use the link below to upload Requests for Admissions you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: true,
                    btnLabel: 'Upload Propounded Discovery',
                    style: { marginLeft: 'auto' },
                    type: 'document'
                }
            ],
            medicalHistory: [
                {
                    id: 1,
                    value: 'medical_history',
                    label: 'Medical History',
                    title: 'Medical History',
                    content: "Please use the link below to upload the .pdf version of the Form Interrogatories you have received from opposing counsel. Once you see the green check mark click \"Upload\" to finish the process. After you click \"upload\" our machine will get to work and will extract each and every question sent to you by opposing counsel. If the document does not read correctly you can click \"Use Judicial Council Form\" to click the questions that you need formatted.",
                    editForm: true,
                    viewForm: record.state ? true : false,
                    btnLabel: 'Upload Medical History Doc',
                    btnLabel1: 'Create Summary Doc',
                    btnAdditional: 'Add Additional Docs',
                    style: { marginRight: 'auto' },
                    type: 'document'
                },
            ],
            uploadColumns: [
                {
                    id: 1,
                    value: 'practice_id',
                    label: 'PRACTICE ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'case_id',
                    label: 'CASE ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'client_id',
                    label: 'CLIENT ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'content_type',
                    label: 'CONTENT TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'document_file',
                    label: 'File',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf',
                    type: 'upload'
                },
                {
                    id: 8,
                    value: 'filename',
                    label: 'File Name',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'comment',
                    label: 'Comment',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    rows: 2,
                    type: 'textarea'
                }
            ],
            medicalHistoryColumns: [
                {
                    id: 1,
                    value: 'practice_id',
                    label: 'PRACTICE ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'case_id',
                    label: 'CASE ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'client_id',
                    label: 'CLIENT ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'content_type',
                    label: 'CONTENT TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 's3_file_key',
                    label: 'files',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    multiple: true,
                    viewFiles: false,
                    contentType: 'application/pdf',
                    max: 50,
                    type: 'upload'
                },
                {
                    id: 8,
                    value: 'comment',
                    label: 'Comment',
                    placeholder: 'If there are any password protected files. Please note that here.',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    rows: 2,
                    type: 'textarea'
                }
            ],
            legalColumns: [
                {
                    id: 1,
                    value: 'lawyer_response_text',
                    label: 'Attorney Response',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    autoFocus: true,
                    status: 'lawyer_response_status',
                    charLimit: 50000,
                    type: 'textarea'
                },
                {
                    id: 2,
                    value: 'client_response_text',
                    label: 'Response',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    status: 'client_response_status',
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'lawyer_response_status',
                    label: 'Status',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: [
                        {
                            value: 'Draft',
                            label: 'Draft'
                        },
                        {
                            value: 'Final',
                            label: 'Final'
                        }
                    ],
                    type: 'select'
                }
            ],
            billingForm: [
                {
                    id: 1,
                    value: 'name',
                    label: 'Card Holder’s Name*',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'email',
                    label: 'Email*',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'cardDetails',
                    label: 'Card Details',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'stripe'
                },
                {
                    id: 4,
                    value: 'badge',
                    label: false,
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'component',
                    style: { paddingTop: 0 },
                    render: () => {
                        const cards = ['visa', 'masterCard', 'amex', 'discover', 'cardO', 'unionPay'];
                        return (
                            <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                <div>
                                    {cards?.map((e, i) => {
                                        return <Icons type={e} key={i} style={{ width: '40px', marginRight: '6px' }} />
                                    })}
                                </div>
                                <div>
                                    <Icons type={'stripe'} style={{ width: '100px' }} />
                                </div>

                            </div>
                        )
                    }
                }
            ],
            clientsForm: [
                {
                    id: 1,
                    value: 'client_id',
                    label: 'Choose Clients',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'clientsOptions',
                    type: 'multiSelect'
                }
            ],
            changeFormColumns: [
                {
                    id: 1,
                    value: 'legalFormId',
                    label: '',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'filesOptions',
                    mandatory: true,
                    variant: 'outlined',
                    type: 'select'
                }
            ],
            editFormColumns: [
                {
                    id: 1,
                    value: 'filename',
                    label: 'FILENAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                }
            ],
            calendarDaysColumns: [
                {
                    id: 1,
                    value: 'proofOfServiceDate',
                    label: 'PROOF OF SERVICE DATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    dateFormat: 'MM/dd',
                    type: 'calendar'
                },
                {
                    id: 2,
                    value: 'numberOfDays',
                    label: 'NUMBER OF DAYS (FROM PROOF OF SERVICE)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    max: 2,
                    mandatory: true,
                    type: 'number'
                }
            ],
            propoundForms: [
                // {
                //     id: 1,
                //     value: 'frogs',
                //     label: 'FROGS',
                //     title: `${record.state === 'TX' ? 'Initial Disclosures' : 'Form Interrogatories (FROGS)'}`,
                //     content: `${record.state === 'TX' ? 'Please use the option below to select questions for Initial Disclosures. You can create multiple Initial Disclosure forms.' : 'Please use the link below to upload the .pdf version of the Form Interrogatories you have received from opposing counsel. Once you see the green check mark click \"Upload\" to finish the process. After you click \"upload\" our machine will get to work and will extract each and every question sent to you by opposing counsel. If the document does not read correctly you can click \"Use Judicial Council Form\" to click the questions that you need formatted.'}`,
                //     editForm: true,
                //     viewForm: docType.includes('frogs') ? true : false,
                //     btnView: 'View Form',
                //     btnLabel1: 'Use Judicial Council Form',
                //     btnLabel2: 'Initial Disclosure Form',
                //     type: 'document'
                // },
                {
                    id: 1,
                    value: 'sprogs',
                    label: 'SPROGS',
                    title: `${record.state !== 'CA' ? 'Interrogatories' : 'Specially Prepared Interrogatories (SPROGS)'}`,
                    content: `Please use the link below to upload ${record.state !== 'CA' ? 'Interrogatories' : 'Special Interrogatories'} you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.`,
                    editForm: true,
                    viewForm: docType.includes('sprogs') ? true : false,
                    btnView: 'View Form',
                    type: 'document'
                },
                {
                    id: 2,
                    value: 'rfpd',
                    label: 'RFPD',
                    title: 'Requests For Production of Documents (RFPD)',
                    content: "Please use the link below to upload the Request for Production of Documents you have received from opposing counsel.  Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('rfpd') ? true : false,
                    btnView: 'View Form',
                    type: 'document'
                },
                {
                    id: 3,
                    value: 'rfa',
                    label: 'RFA',
                    title: 'Request For Admissions (RFA)',
                    content: "Please use the link below to upload Requests for Admissions you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('rfa') ? true : false,
                    btnView: 'View Form',
                    type: 'document'
                }

            ],
            respondForms: [
                {
                    id: 1,
                    value: 'idc',
                    label: 'IDC',
                    title: 'Initial Disclosures',
                    formType: 'idc',
                    content: 'Please use the option below to select questions for Initial Disclosures. You can create multiple Initial Disclosure forms',
                    editForm: true,
                    viewForm: docType.includes('idc') && (record?.state == "CA") ? true : false,
                    btnLabel: 'Initial Disclosure Form',
                    btnLabel1: 'Use Judicial Council Form',
                    btnLabel2: 'Initial Disclosure Form',
                    btnLabel3: 'Use Saved Templates',
                    disableCancelButton: true,
                    type: 'document'
                },
                {
                    id: 2,
                    value: 'frogs',
                    label: 'FROGS',
                    title: `${record.state === 'TX' ? 'Initial Disclosures' : 'Form Interrogatories (FROGS)'}`,
                    content: `${record.state === 'TX' ? 'Please use the option below to select questions for Initial Disclosures. You can create multiple Initial Disclosure forms.' : 'Please use the link below to upload the .pdf version of the Form Interrogatories you have received from opposing counsel. Once you see the green check mark click \"Upload\" to finish the process. After you click \"upload\" our machine will get to work and will extract each and every question sent to you by opposing counsel. If the document does not read correctly you can click \"Use Judicial Council Form\" to click the questions that you need formatted.'}`,
                    editForm: true,
                    viewForm: docType.includes('frogs') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    btnLabel1: 'Use Judicial Council Form',
                    btnLabel2: 'Initial Disclosure Form',
                    btnLabel3: 'Use Saved Templates',
                    disableCancelButton: true,
                    type: 'document'
                },
                {
                    id: 3,
                    value: 'sprogs',
                    label: 'SPROGS',
                    title: `${record.state !== 'CA' ? 'Interrogatories' : 'Specially Prepared Interrogatories (SPROGS)'}`,
                    content: `Please use the link below to upload ${record.state !== 'CA' ? 'Interrogatories' : 'Special Interrogatories'} you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.`,
                    editForm: true,
                    viewForm: docType.includes('sprogs') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 4,
                    value: 'rfpd',
                    label: 'RFPD',
                    title: 'Requests For Production of Documents (RFPD)',
                    content: "Please use the link below to upload the Request for Production of Documents you have received from opposing counsel.  Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('rfpd') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 5,
                    value: 'rfa',
                    label: 'RFA',
                    title: 'Request For Admissions (RFA)',
                    content: "Please use the link below to upload Requests for Admissions you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('rfa') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                }
            ],
            uploadDocumentColumns: [
                {
                    id: 1,
                    value: 'document_file',
                    label: 'File',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf',
                    type: 'upload'
                },
                {
                    id: 2,
                    value: 'filename',
                    label: 'File Name',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                }
            ],
            objectionColumns: [
                {
                    id: 1,
                    value: 'lawyer_objection_text',
                    label: 'Attorney Objections (Common to all sub-questions)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    autoFocus: true,
                    status: 'lawyer_objection_status',
                    charLimit: 50000,
                    type: 'textarea'
                },
            ],
            selectPropoundingQuestions: (options) => {
                return [
                    {
                        id: 1,
                        value: 'template_id',
                        label: 'Propounding Questions',
                        editRecord: true,
                        viewRecord: true,
                        viewMode: false,
                        visible: true,
                        mandatory: true,
                        options: options,
                        type: 'select'
                    },
                ]
            },
            customLegalColumns: [
                {
                    id: 1,
                    value: 'lawyer_response_text',
                    label: 'Attorney Response',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    autoFocus: true,
                    status: 'lawyer_response_status',
                    charLimit: 50000,
                    type: 'textarea'
                },
            ],
            newYorkForms: [
                {
                    id: 1,
                    value: 'bop',
                    label: 'BOP',
                    title: `Bill of Particulars`,
                    content: `Please use the link below to upload the Bill of Particulars you have received from opposing counsel.  Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.`,
                    editForm: true,
                    viewForm: docType.includes('bop') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 2,
                    value: 'odd',
                    label: 'ODD',
                    title: 'Omnibus Discovery Demands',
                    content: "Please use the link below to upload the Omnibus Discovery Demands you have received from opposing counsel.  Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('odd') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 3,
                    value: 'sprogs',
                    label: 'SPROGS',
                    title: `Interrogatories`,
                    content: `Please use the link below to upload the Special Interrogatories you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.`,
                    editForm: true,
                    viewForm: docType.includes('sprogs') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 4,
                    value: 'ndi',
                    label: 'NDI',
                    title: 'Notice for Discovery and Inspection',
                    content: "Please use the link below to upload Notice for Discovery and Inspection you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('ndi') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                },
                {
                    id: 5,
                    value: 'nta',
                    label: 'NTA',
                    title: 'Notice to Admit',
                    content: "Please use the link below to upload Notice to Admit you have received from opposing counsel. Once you see the green check mark click \"Upload\". After you click upload our machine will get to work, it will identify each individual question and allow you to proceed to the next step.",
                    editForm: true,
                    viewForm: docType.includes('nta') ? true : false,
                    btnLabel: 'Upload Propounded Discovery',
                    type: 'document'
                }
            ],
        }
    }


    function changePassword() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'password',
                    label: 'Password',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'new_password',
                    label: 'Confirm Password',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: true
                }
            ]
        }
    }


    function questionForm() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'question_number',
                    title: 'Question Number',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'number',
                    mandatory: true,
                    autoFocus: true
                },
                {
                    id: 2,
                    value: 'question_text',
                    title: 'Question Text',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'textarea',
                    mandatory: true,
                    autoFocus: true
                }
            ],
            bulkQuestionsForm: [
                {
                    id: 1,
                    value: 'questions',
                    label: 'Questions JSON',
                    placeholder: 'Paste a JSON here.',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'textarea'
                }
            ]
        }
    }


    function supportRequest() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'user_email',
                    label: 'USER EMAIL',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    sort: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 3,
                    value: 'client_name',
                    label: 'CLIENT NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'createdAt',
                    label: 'REQUEST DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row && row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'document_s3_key',
                    label: 'DOCUMENT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const value = row['document_s3_key'];
                        return <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(await getSecureFormURL(value))} />
                    },
                    type: 'download'
                }
            ]
        }
    }

    function questionsTranslations() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'question_number',
                    label: 'QUESTION NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'SourceLanguageCode',
                    label: 'SOURCE LANGUAGE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    html: (row) => row['SourceLanguageCode'] === 'en' && 'English' || row['SourceLanguageCode'] === 'es' && 'Spanish' || row['SourceLanguageCode'],
                    options: [
                        {
                            value: 'en',
                            label: 'English'
                        },
                        {
                            value: 'es',
                            label: 'Spanish'
                        }
                    ],
                    type: 'select'
                },
                {
                    id: 3,
                    value: 'TargetLanguageCode',
                    label: 'TARGET LANGUAGE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    html: (row) => row['TargetLanguageCode'] === 'en' && 'English' || row['TargetLanguageCode'] === 'es' && 'Spanish' || row['TargetLanguageCode'],
                    options: [
                        {
                            value: 'en',
                            label: 'English'
                        },
                        {
                            value: 'es',
                            label: 'Spanish'
                        }
                    ],
                    type: 'select'
                },
                {
                    id: 4,
                    value: 'SourceLanguageText',
                    label: 'SOURCE LANGUAGE TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'TargetLanguageText',
                    label: 'TARGET LANGUAGE TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    options: [
                        {
                            value: 'IDC',
                            label: 'IDC'
                        },
                        {
                            value: 'FROGS',
                            label: 'FROGS'
                        },
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        },
                        {
                            label: 'BOP',
                            value: 'BOP'
                        },
                        {
                            label: 'ODD',
                            value: 'ODD'
                        },
                        {
                            label: 'NDI',
                            value: 'NDI'
                        },
                        {
                            label: 'NTA',
                            value: 'NTA'
                        },
                    ],
                    type: 'select'
                }
            ],
            bulkTranslations: [
                {
                    id: 1,
                    value: 'json_data',
                    label: 'Questions Translations JSON',
                    placeholder: 'Paste a JSON here.',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'textarea'
                }
            ]
        }
    }

    function login() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'identifier',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    disabled: true,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'secret',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'password'
                }
            ]
        }
    }

    function plans() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'input',
                },
                {
                    id: 3,
                    value: 'plan_id',
                    label: 'PLAN ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 4,
                    value: 'plan_type',
                    label: 'PLAN TYPE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'autoComplete',
                    disableOptons: {
                        edit: true
                    },
                    html: (row) => {
                        let plan = row && row['plan_type'];
                        switch (plan) {
                            case 'pay_as_you_go':
                                return 'TEK AS YOU GO';
                            case 'free_trial':
                                return 'FREE TRIAL';
                            case 'monthly':
                                return 'MONTHLY';
                            case 'yearly':
                                return 'YEARLY';
                            case 'responding_monthly_349':
                            case 'responding_yearly_3490':
                            case 'responding_monthly_495':
                            case 'responding_yearly_5100':
                            case 'propounding_monthly_199':
                            case 'propounding_yearly_2199':
                            case 'users_responding_monthly_license_495':
                            case 'users_responding_yearly_license_5100':
                            case 'users_responding_monthly_license_89':
                            case 'users_responding_yearly_license_948':
                            case 'users_propounding_monthly_license_199':
                            case 'users_propounding_yearly_license_2149_20':
                            case 'users_propounding_monthly_license_39':
                            case 'users_propounding_yearly_license_420':
                            case 'users_responding_monthly_free_license_495':
                            case 'users_responding_yearly_free_license_5100':
                            case 'users_propounding_monthly_free_license_199':
                            case 'users_propounding_yearly_free_license_2149_20':
                                return plan && plan.toUpperCase().replace(/_/g, ' ');
                            case 'responding_vip':
                                return 'VIP RESPONDING';
                            case 'propounding_vip':
                                return 'VIP PROPOUNDING';
                            default:
                                return plan && plan.toUpperCase();
                        }
                    },
                    options: [
                        {
                            label: 'TEK AS-YOU-GO',
                            value: 'pay_as_you_go'
                        },
                        {
                            label: 'FREE TRIAL',
                            value: 'free_trial'
                        },
                        {
                            label: 'TEK UNLIMITED MONTHLY',
                            value: 'monthly'
                        },
                        {
                            label: 'TEK UNLIMITED YEARLY',
                            value: 'yearly'
                        },
                        {
                            label: 'TEK MONTHLY RESPONDING 349',
                            value: 'responding_monthly_349'
                        },
                        {
                            label: 'TEK MONTHLY RESPONDING 495',
                            value: 'responding_monthly_495'
                        },
                        {
                            label: 'TEK YEARLY RESPONDING 3490',
                            value: 'responding_yearly_3490'
                        },
                        {
                            label: 'TEK YEARLY RESPONDING 5100',
                            value: 'responding_yearly_5100'
                        },
                        {
                            label: 'TEK MONTHLY PROPOUNDING 199',
                            value: 'propounding_monthly_199'
                        },
                        {
                            label: 'TEK YEARLY PROPOUNDING 2199',
                            value: 'propounding_yearly_2199'
                        },
                        {
                            label: 'VIP RESPONDING',
                            value: 'responding_vip'
                        },
                        {
                            label: 'VIP PROPOUNDING',
                            value: 'propounding_vip'
                        },
                        {
                            label: 'USER RESPONDING MONTHLY 495',
                            value: 'users_responding_monthly_license_495'
                        },
                        {
                            label: 'USER RESPONDING YEARLY 5100',
                            value: 'users_responding_yearly_license_5100'
                        },
                        {
                            label: 'USER RESPONDING MONTHLY 89',
                            value: 'users_responding_monthly_license_89'
                        },
                        {
                            label: 'USER RESPONDING YEARLY 948',
                            value: 'users_responding_yearly_license_948'
                        },
                        {
                            label: 'USER PROPOUNDING MONTHLY 199',
                            value: 'users_propounding_monthly_license_199'
                        },
                        {
                            label: 'USER PROPOUNDING YEARLY 2149.20',
                            value: 'users_propounding_yearly_license_2149_20'
                        },
                        {
                            label: 'USER PROPOUNDING MONTHLY 39',
                            value: 'users_propounding_monthly_license_39'
                        },
                        {
                            label: 'USER PROPOUNDING YEARLY 420',
                            value: 'users_propounding_yearly_license_420'
                        },
                        {
                            label: 'USER RESPONDING MONTHLY FREE 495',
                            value: 'users_responding_monthly_free_license_495'
                        },
                        {
                            label: 'USER RESPONDING YEARLY FREE 5100',
                            value: 'users_responding_yearly_free_license_5100'
                        },
                        {
                            label: 'USER PROPOUNDING MONTHLY FREE 199',
                            value: 'users_propounding_monthly_free_license_199'
                        },
                        {
                            label: 'USER PROPOUNDING YEARLY FREE 2149.20',
                            value: 'users_propounding_yearly_free_license_2149_20'
                        },
                    ]
                },
                {
                    id: 5,
                    value: 'plan_category',
                    label: 'PLAN CATEGORY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    type: 'select',
                    disableOptons: {
                        edit: true
                    },
                    html: (row) => row && row['plan_category'] && row['plan_category'].toUpperCase(),
                    options: [
                        {
                            label: 'RESPONDING',
                            value: 'responding'
                        },
                        {
                            label: 'PROPOUNDING',
                            value: 'propounding'
                        }
                    ]
                },
                {
                    id: 6,
                    value: 'price',
                    label: 'PRICE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'input',
                },
                {
                    id: 7,
                    value: 'description',
                    label: 'DESCRIPTION',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    disableOptons: {
                        edit: true
                    },
                    rows: 2,
                    type: 'textarea',
                },
                {
                    id: 8,
                    value: 'validity',
                    label: 'VALIDITY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'number',
                },
                {
                    id: 9,
                    value: 'features_included',
                    label: 'Features included in subscription',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    html: (row) => {
                        const valueArr = row['features_included'] && row['features_included'].split(',') || [];
                        return (<div>
                            {valueArr && valueArr.length > 0 ? valueArr.map((arr, index) => (<div key={index} style={arr !== 'pos' ? { textTransform: 'capitalize' } : null}>
                                {arr && arr === 'pos' ? 'PoS' : arr}
                            </div>)) : '-'}
                        </div>)
                    },
                    options: [
                        {
                            label: 'Shell',
                            value: 'shell'
                        },
                        {
                            label: 'Discovery',
                            value: 'discovery'
                        },
                        {
                            label: 'TekSign',
                            value: 'teksign'
                        },
                        {
                            label: 'Medical History',
                            value: 'medicalHistory'
                        },
                        {
                            label: 'PoS',
                            value: 'pos'
                        },
                        {
                            label: 'Propound Document',
                            value: 'propound_doc'
                        }
                    ],
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input.value && typeof input.value === 'string' && input.value.split(',') || [];
                        return (<div>
                            <label style={{ fontSize: '1rem', marginBottom: '10px' }}>Features included in subscription</label>
                            <div>
                                {options.map((opt, index) => (<FormControlLabel
                                    control={<Checkbox
                                        key={index}
                                        style={{ color: "#2ca01c" }}
                                        value={opt.value}
                                        checked={inputArr.includes(opt.value) || false}
                                        onChange={(e) => {
                                            if (e.target.checked) {
                                                inputArr.push(e.target.value);
                                            } else {
                                                inputArr = inputArr.filter(val => val !== opt.value);
                                            }
                                            input.onChange(inputArr.join(','));
                                        }} />}
                                    label={<span dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                ))}
                            </div>
                        </div>)
                    },
                    type: 'component'
                },
                {
                    id: 10,
                    value: 'custom_pricing_text',
                    label: 'CUSTOM PRICING TEXT',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    placeholder: 'Paste the JSON here.',
                    disableOptons: {
                        edit: true
                    },
                    type: 'textarea',
                },
                {
                    id: 11,
                    value: 'active',
                    label: 'ACTIVE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['active'] ? 'Yes' : 'No',
                    type: 'checkbox'
                },
                {
                    id: 12,
                    value: 'order',
                    label: 'ORDER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    placeholder: 'Enter between 1-10',
                    sort: true,
                    type: 'number'
                },
                {
                    id: 13,
                    value: 'created_by_admin_name',
                    label: 'CREATED BY',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 14,
                    value: 'created_by_admin_id',
                    label: 'CREATED BY',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    html: (row, metaData) => metaData['users'] && metaData['users'].length > 0 && row['created_by_admin_id'] && metaData['users'].find(_ => row['created_by_admin_id'].toString() === _.id.toString()) && metaData['users'].find(_ => row['created_by_admin_id'].toString() === _.id.toString()).name || row['created_by_admin_id'],
                    type: 'input',
                }
            ],
            decoratorView: () => (<div>Only minimal editing is allowed on existing pricing plans since they are all attached with existing customers. Features supported for the plan and the Order in which the plan appear can be edited, other fields cannot be edited (Create new plans if required - If new plan replace an existing plan, mark the existing plan as inactive)</div>)
        }
    }


    function parties(record = {}) {
        const addressRequired = record && (record.street || record.city || record.state || record.zip_code) ? true : false;
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'case_id',
                    label: 'CASE TITLE',
                    editRecord: true,
                    disableRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'casesWithClientOptions',
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    html: (row, metaData) => {
                        const data = metaData && metaData.casesOptions && metaData.casesOptions.length > 0 && metaData.casesOptions.find(_ => _.value === row.case_id) || false;
                        return data && data.label || '-';
                    },
                    type: 'select'
                },
                {
                    id: 4,
                    value: 'client_name',
                    label: 'CLIENT NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    limit: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'name',
                    label: 'LAST/FIRST NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 6,
                    value: 'first_name',
                    label: 'FIRST NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    limit: true
                },
                {
                    id: 7,
                    value: 'middle_name',
                    label: 'MIDDLE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    limit: true,
                },
                {
                    id: 8,
                    value: 'last_name',
                    label: 'LAST NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    limit: true
                },
                {
                    id: 9,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 10,
                    value: 'createdAt',
                    label: 'REGISTERED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'date'
                },
                {
                    id: 11,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 12,
                    value: 'dob',
                    label: 'BIRTHDATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                    placeholder: 'mm/dd/yyyy'
                },
                {
                    id: 13,
                    value: 'address',
                    label: 'ADDRESS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 11,
                    value: 'street',
                    label: 'STREET',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 12,
                    value: 'city',
                    label: 'CITY',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 13,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 14,
                    value: 'zip_code',
                    label: 'ZIP CODE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: addressRequired,
                    type: 'input'
                },
                {
                    id: 15,
                    value: 'totalCases',
                    label: 'CASE COUNT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'number'
                }
            ]
        }
    }

    function subscriptionsHistory(user) {
        const practiceDetails =  user && user?.practiceDetails || false;
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'subscribed_by',
                    label: 'USER',
                    sortColumn: 'subscribed_by',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'plan_type',
                    label: 'PLAN TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'price',
                    label: 'PRICE',
                    sortColumn: 'price',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'license_count',
                    label: 'LICENSE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: (practiceDetails && Object.keys(practiceDetails).length && practiceDetails?.billing_type === "limited_users_billing") ? true : false,
                    type: 'input',
                    html: (row) => {
                        return row?.billing_type === 'limited_users_billing' ? row?.license_count : '-'
                    }
                },
                {
                    id: 6,
                    value: 'order_date',
                    label: 'ORDER DATE',
                    sortColumn: 'order_date',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'date'
                },
                {
                    id: 7,
                    value: 'subscription_valid_till',
                    label: 'VALID UPTO',
                    sortColumn: 'subscription_valid_till',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'date'
                },
                {
                    id: 8,
                    value: 'payment_type',
                    label: 'TYPE',
                    sortColumn: 'payment_type',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'status',
                    label: 'STATUS',
                    sortColumn: 'status',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                }
            ]
        }
    }


    function adminSubscriptionsHistory() {

        return {

            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'order_date',
                    label: 'ORDER DATE',
                    sortColumn: 'order_date',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'date'
                },
                {
                    id: 3,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'practice_name',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'subscribed_by',
                    label: 'USER',
                    sortColumn: 'subscribed_by',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'plan_type',
                    label: 'PLAN TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'price',
                    label: 'PRICE',
                    sortColumn: 'price',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'license_count',
                    label: 'LICENSE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        return row?.billing_type === 'limited_users_billing' ? row?.license_count : '-'
                    }
                },
                {
                    id: 7,
                    value: 'subscription_valid_till',
                    label: 'VALID UPTO',
                    sortColumn: 'subscription_valid_till',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'date'
                },
                {
                    id: 8,
                    value: 'payment_type',
                    label: 'TYPE',
                    sortColumn: 'payment_type',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                // {
                //     id: 9,
                //     value: 'billing_type',
                //     label: 'BILLING TYPE',
                //     editRecord: false,
                //     viewRecord: false,
                //     viewMode: true,
                //     visible: true,
                //     html: (row) => row['billing_type'] && row['billing_type'].toUpperCase().replace(/_/g, ' '),
                //     type: 'input'
                // },
                {
                    id: 10,
                    value: 'status',
                    label: 'STATUS',
                    sortColumn: 'status',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                }
            ]
        }
    }

    function userSettings() {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'Change Password',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'changePassword',
                    message: 'Password must be changed every 90 days',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    confirmButton: false,
                    enableSubmitBtn: true,
                    confirmMessage: '',
                    disableMessage: '',
                    columns: [
                        {
                            id: 1,
                            value: 'password',
                            label: 'Password',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: true
                        },
                        {
                            id: 2,
                            value: 'new_password',
                            label: 'Confirm Password',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: true
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Two Factor Authentication',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'enabledTwoFactorAuthentication',
                    message: 'Two Factor Authentication provides additional security. While logging in, a code will be sent to your email to complete login.',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    enableSubmitBtn: true,
                    confirmButton: true,
                    disableCancelBtn: true,
                    confirmMessage: (user) => `This will require you to access your email (${user.email}) in order to login in future. Do you want to make this change?`,
                    disableMessage: 'Are you sure you want to disable two factor authentication?',
                    columns: [
                        {
                            id: 1,
                            value: 'twofactor',
                            label: 'Enable/Disable Two Factor Authentication',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'switch',
                            mandatory: true
                        },
                    ]
                },
                {
                    schemaId: 3,
                    name: 'Switch Practice',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'switchPractice',
                    message: 'Switch Practice',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    confirmButton: false,
                    enableSubmitBtn: true,
                    confirmButton: true,
                    confirmMessage: (user, formValue, metaData) => {
                        const { practicesData = [] } = metaData;
                        const currentPracticeData = (practicesData && practicesData.length && formValue) ? practicesData.filter((val) => val.value === formValue) : [];

                        const currentPracticeName = (Array.isArray(currentPracticeData) && currentPracticeData.length) ? currentPracticeData[0]["label"] : "";
                        return `Do you want to switch ${currentPracticeName}?`;
                    },
                    disableMessage: '',
                    columns: [
                        {
                            id: 1,
                            value: 'currentPractice',
                            label: 'Switch Practice',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'select',
                            mandatory: true,
                            options : 'practicesData'
                        },
                    ]
                }
            ]
        }
    }


    function standardEditForm() {
        return {
            columns: [{
                id: 1,
                value: 'filename',
                label: 'FILENAME',
                editRecord: true,
                viewRecord: true,
                viewMode: false,
                visible: false,
                mandatory: true,
                type: 'input'
            }]
        }
    }

    function customTemplate() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'practice_name',
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'file_name',
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'state',
                    label: 'STATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'state',
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? (row['state'] == 'PL' || row['state'] == 'NPL') ? `Other (${row['state']})` : row['state'] : '-',
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'createdAt',
                    html: (row) => row && row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date',
                },
                {
                    id: 5,
                    value: 'custom_template',
                    label: 'CUSTOM TEMPLATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input',
                    html: (row) => {
                        const custom = row && row['custom_template'];
                        return custom ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(custom)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 6,
                    value: 'modified_template_frogs',
                    label: 'MODIFIED FROGS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedFrogs = row && row['modified_template_frogs'];
                        return modifiedFrogs ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedFrogs)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 7,
                    value: 'modified_template_sprogs',
                    label: 'MODIFIED SPROGS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedSprogs = row && row['modified_template_sprogs'];
                        return modifiedSprogs ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedSprogs)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 8,
                    value: 'modified_template_rfpd',
                    label: 'MODIFIED RFPD',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedRfpd = row && row['modified_template_rfpd'];
                        return modifiedRfpd ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedRfpd)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 9,
                    value: 'modified_template_rfa',
                    label: 'MODIFIED RFA',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedRfa = row && row['modified_template_rfa'];
                        return modifiedRfa ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedRfa)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 10,
                    value: 'modified_template_idc',
                    label: 'MODIFIED IDC',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedIdc = row && row['modified_template_idc'];
                        return modifiedIdc ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedIdc)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 11,
                    value: 'modified_template_bop',
                    label: 'MODIFIED BOP',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedBop = row && row['modified_template_bop'];
                        return modifiedBop ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedBop)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 12,
                    value: 'modified_template_odd',
                    label: 'MODIFIED ODD',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedOdd = row && row['modified_template_odd'];
                        return modifiedOdd ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedOdd)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 13,
                    value: 'modified_template_ndi',
                    label: 'MODIFIED NDI',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedNdi = row && row['modified_template_ndi'];
                        return modifiedNdi ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedNdi)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 14,
                    value: 'modified_template_nta',
                    label: 'MODIFIED NTA',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedNta = row && row['modified_template_nta'];
                        return modifiedNta ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedNta)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 15,
                    value: 'modified_template',
                    label: 'MODIFIED TEMPLATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    contentType: '.doc, .docx',
                    type: 'upload'
                },
                // {
                //     id: 11,
                //     value: 'template',
                //     label: 'TEMPLATE',
                //     editRecord: false,
                //     viewRecord: true,
                //     viewMode: false,
                //     visible: true,
                //     type: 'input',
                //     html: (row) => {
                //         const template = row && row['template'];
                //         return template ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(template)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                //     }
                // },
                {
                    id: 15,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'status',
                    options: [
                        {
                            value: 'Processing',
                            label: 'Processing'
                        },
                        {
                            value: 'Applied',
                            label: 'Applied'
                        }
                    ],
                    type: 'select'
                },
                {
                    id: 16,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: [
                        {
                            value: 'IDC',
                            label: 'IDC'
                        },
                        {
                            value: 'FROGS',
                            label: 'FROGS'
                        },
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        },
                        {
                            label: 'BOP',
                            value: 'BOP'
                        },
                        {
                            label: 'ODD',
                            value: 'ODD'
                        },
                        {
                            label: 'NDI',
                            value: 'NDI'
                        },
                        {
                            label: 'NTA',
                            value: 'NTA'
                        },
                        // {
                        //     value: 'template',
                        //     label: 'TEMPLATE'
                        // }
                    ],
                    type: 'select'
                }
            ]
        }
    }


    function POS(record = {}) {
        return [
            {
                schemaId: 1,
                editForm: true,
                key: 'serving_pos_date',
                label: 'Serving/PoS Date',
                columns: [
                    {
                        id: 1,
                        value: 'serving_date',
                        label: 'PoS DATE',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'calendar'
                    },
                    {
                        id: 2,
                        value: 'serving_date',
                        label: 'PoS DATE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => row && row['serving_date'] && moment(row['serving_date']).format('MM/DD/YYYY'),
                        type: 'input'
                    }
                ]
            },
            {
                schemaId: 2,
                editForm: true,
                key: 'opposing_counsel',
                label: 'Opposing Counsel',
                columns: [
                    {
                        id: 1,
                        value: 'opposing_counsel',
                        label: 'OPPOSING COUNSEL INFO',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        type: 'fieldArray',
                        title: 'Counsel',
                        max: 5,
                        btnLabel: 'Add additional counsel info',
                        defaultField: true,
                        mandatory: true,
                        fieldArray: [
                            {
                                id: 1,
                                value: 'opposing_counsel_attorney_name',
                                label: 'OPPOSING COUNSEL ATTORNEY NAME',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'multiInput',
                                inputLimit: 5,
                            },
                            {
                                id: 2,
                                value: 'opposing_counsel_office_name',
                                label: 'OPPOSING COUNSEL PRACTICE NAME',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 3,
                                value: 'opposing_counsel_street',
                                label: 'STREET',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input',
                                placeholder: 'eg: 101 Main Street'
                            },
                            {
                                id: 4,
                                value: 'opposing_counsel_city',
                                label: 'CITY',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 5,
                                value: 'opposing_counsel_state',
                                label: 'STATE',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 6,
                                value: 'opposing_counsel_zip_code',
                                label: 'ZIP CODE',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 7,
                                value: 'opposing_counsel_email',
                                label: 'EMAIL',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                placeholder: "Type valid email and enter",
                                type: 'multiEmail',
                            }
                        ]
                    },
                    {
                        id: 2,
                        value: 'opposing_counsel_attorney_name',
                        label: 'NAME',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 3,
                        value: 'opposing_counsel_office_name',
                        label: 'PRACTICE NAME',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 4,
                        value: 'opposing_counsel_address',
                        label: 'ADDRESS',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            const opposing_address = row && row['opposing_counsel_city'] && row['opposing_counsel_street'] && row['opposing_counsel_state'] && row['opposing_counsel_zip_code'] && row['opposing_counsel_street'];
                            return <span dangerouslySetInnerHTML={{ __html: (opposing_address ? row['opposing_counsel_street'] + '<br/>' + row['opposing_counsel_city'] + ', ' + row['opposing_counsel_state'] + ' ' + row['opposing_counsel_zip_code'] : '-') }} />;
                        },
                        type: 'input'
                    },
                    {
                        id: 5,
                        value: 'opposing_counsel_email',
                        label: 'EMAIL',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            const opposing_counsel_email = row && row['opposing_counsel_email'] && row['opposing_counsel_email'].split(',');
                            const email = opposing_counsel_email && opposing_counsel_email.map(el => <><span>{el}</span><br /></>);
                            return email
                        },
                        type: 'input',
                    }
                ]
            },
            {
                schemaId: 3,
                editForm: true,
                key: 'serving_attorney',
                label: 'Serving Attorney',
                columns: [
                    {
                        id: 1,
                        value: 'serving_attorney_name',
                        label: 'ATTORNEY NAME',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 2,
                        value: 'serving_attorney_street',
                        label: 'STREET',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input',
                        placeholder: 'eg: 101 Main Street'
                    },
                    {
                        id: 3,
                        value: 'serving_attorney_city',
                        label: 'CITY',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 4,
                        value: 'serving_attorney_state',
                        label: 'STATE',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 5,
                        value: 'serving_attorney_zip_code',
                        label: 'ZIP CODE',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 6,
                        value: 'serving_attorney_email',
                        label: 'EMAIL',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input',
                    },
                    {
                        id: 7,
                        value: 'serving_attorney_name',
                        label: 'NAME',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 8,
                        value: 'serving_attorney_address',
                        label: 'ADDRESS',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            return <span dangerouslySetInnerHTML={{ __html: (row && row['serving_attorney_street'] && row['serving_attorney_city'] && row['serving_attorney_state'] && row['serving_attorney_zip_code'] ? row['serving_attorney_street'] + '<br/>' + row['serving_attorney_city'] + ', ' + row['serving_attorney_state'] + ' ' + row['serving_attorney_zip_code'] : '-') }} />;
                        },
                        type: 'input'
                    },
                    {
                        id: 9,
                        value: 'serving_attorney_email',
                        label: 'EMAIL',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input',
                    }
                ]
            },
            {
                schemaId: 4,
                editForm: false,
                key: 'case_info',
                label: 'Case Information',
                columns: [
                    {
                        id: 1,
                        value: 'case_number',
                        label: 'CASE NUMBER',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 2,
                        value: 'case_title',
                        label: 'CASE TITLE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 3,
                        value: 'document_type',
                        label: 'DOCUMENT TYPE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            const doc_type = row && row['document_type'] === 'FROGS' ? 'Form Interrogatories' : row['document_type'] === 'SPROGS' ? (row['state'] !== 'CA' ? 'Interrogatories' : 'Specially Prepared Interrogatories') : row['document_type'] === 'RFPD' ? 'Requests For Production of Documents' : row['document_type'] === 'RFA' ? 'Request For Admissions' : row['document_type'];
                            return doc_type && doc_type;
                        },
                        type: 'input'
                    },
                    {
                        id: 4,
                        value: 'state',
                        label: 'STATE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => row && row.state == 'CA' ? 'California' : row.state == 'NV' ? 'Nevada' : row.state == 'TN' ? 'Tennessee' : row.state == 'AZ' ? 'Arizona' : row.state == 'WA' ? 'Washington' : row.state,
                        type: 'input',
                    },
                    {
                        id: 5,
                        value: 'county',
                        label: 'COUNTY',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    }
                ]
            }
        ]
    }

    function state() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'state_name',
                    label: 'STATE NAME',
                    sortColumn: 'State.name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true,
                    html: (row) => capitalizeFirstLetter && capitalizeFirstLetter(row['state_name']) || row['state_name']
                },
                {
                    id: 2,
                    value: 'state_code',
                    label: 'STATE CODE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    mandatory: true,
                    html: (row) => row['state_code'] && row['state_code'].toUpperCase()
                },
                {
                    id: 3,
                    value: 'document_type',
                    label: 'Document included in state',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    html: (row) => {
                        const valueArr = row['document_type'] && row['document_type'].split(',') || [];
                        return (<div>
                            {valueArr && valueArr.length > 0 ? valueArr.map((arr, index) => (<div key={index} style={{ textTransform: 'capitalize' }}>
                                {arr}
                            </div>)) : '-'}
                        </div>)
                    },
                    options: [
                        {
                            label: 'IDC',
                            value: 'idc'
                        },
                        {
                            label: 'FROGS',
                            value: 'frogs'
                        },
                        {
                            label: 'SPROGS',
                            value: 'sprogs'
                        },
                        {
                            label: 'RFPD',
                            value: 'rfpd'
                        },
                        {
                            label: 'RFA',
                            value: 'rfa'
                        },
                        {
                            label: 'BOP',
                            value: 'bop'
                        },
                        {
                            label: 'ODD',
                            value: 'odd'
                        },
                        {
                            label: 'NDI',
                            value: 'ndi'
                        },
                        {
                            label: 'NTA',
                            value: 'nta'
                        },
                    ],
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input.value && typeof input.value === 'string' && input.value.split(',') || [];
                        return (<div>
                            <label style={{ fontSize: '1rem', marginBottom: '10px' }}>Document included in state</label>
                            <div>
                                {options.map((opt, index) => (<FormControlLabel
                                    control={<Checkbox
                                        key={index}
                                        style={{ color: "#2ca01c" }}
                                        value={opt.value}
                                        checked={inputArr.includes(opt.value) || false}
                                        onChange={(e) => {
                                            if (e.target.checked) {
                                                inputArr.push(e.target.value);
                                            } else {
                                                inputArr = inputArr.filter(val => val !== opt.value);
                                            }
                                            input.onChange(inputArr.join(','));
                                        }} />}
                                    label={<span dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                ))}
                            </div>
                        </div>)
                    },
                    type: 'component'
                },
                // {
                //     id: 4,
                //     value: 'default_objections',
                //     label: 'OBJECTIONS TEXT',
                //     editRecord: true,
                //     viewRecord: false,
                //     viewMode: false,
                //     visible: false,
                //     placeholder: 'Paste the JSON here.',
                //     disableOptons: {
                //         edit: true
                //     },
                //     type: 'textarea',
                // },
                {
                    id: 4,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'updatedAt',
                    label: 'UPDATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['updatedAt'] && moment(row['updatedAt']).format('MM/DD/YY'),
                    type: 'input'
                }
            ]
        }
    }

    function formOtp() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'client_name',
                    label: 'CLIENT NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'otp_code',
                    label: 'OTP CODE',
                    sortColumn: 'otp_code',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'otp_secret',
                    label: 'OTP SECRET',
                    sortColumn: 'otp_secret',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'TargetLanguageCode',
                    label: 'TARGET LANGUAGE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => row && row['TargetLanguageCode'] && row['TargetLanguageCode'] === 'vi' ? 'Vietnamese' : row['TargetLanguageCode'] === 'es' ? 'Spanish' : 'English',
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'createdAt',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
            ]
        }
    }

    function otherPartiesFormOtp() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'client_name',
                    label: 'NON-PARTIES NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'otp_code',
                    label: 'OTP CODE',
                    sortColumn: 'otp_code',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'otp_secret',
                    label: 'OTP SECRET',
                    sortColumn: 'otp_secret',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'TargetLanguageCode',
                    label: 'TARGET LANGUAGE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => row && row['TargetLanguageCode'] && row['TargetLanguageCode'] === 'vi' ? 'Vietnamese' : row['TargetLanguageCode'] === 'es' ? 'Spanish' : 'English',
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'createdAt',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
            ]
        }
    }

    function adminEsquiretekUsers() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'NAME',
                    sortColumn: 'name',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'createdAt',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'email',
                    label: 'EMAIL',
                    sortColumn: 'email',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    disableOptons: {
                        edit: true
                    },
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 5,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    sort: true,
                    type: 'password',
                    mandatory: {
                        create: true
                    }
                },
                {
                    id: 6,
                    value: 'role',
                    label: 'ROLE',
                    sortColumn: 'role',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => {
                        return row['role'] === 'superAdmin' ? 'Super Admin' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'];
                    },
                    options: [
                        {
                            label: 'Super Admin',
                            value: 'superAdmin',
                        },
                        {
                            label: 'Manager',
                            value: 'manager',
                        },
                        {
                            label: 'Operator',
                            value: 'operator',
                        },
                        {
                            label: 'Medical Expert',
                            value: 'medicalExpert',
                        },
                        {
                            label: 'Quality Technician',
                            value: 'QualityTechnician',
                        }
                    ],
                    type: 'select',
                    mandatory: true
                }
            ]
        }
    }

    function adminPracticeUsers(record = {}) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'Practices.name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'practice_id',
                    label: 'PRACTICE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'practicesOptions',
                    disableOptons: {
                        edit: true
                    },
                    mandatory: true,
                    type: 'autoComplete'
                },
                {
                    id: 3,
                    value: 'subscriptions',
                    label: 'PLAN TYPE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    html: (row) => {
                        const valueArr = row['subscriptions'] && row['subscriptions'] || [];
                        let options = valueArr && valueArr.length > 0 && valueArr.map(el => {
                            if (el) {
                                return el.plan_type && el.plan_type != '' && el.plan_category && (el.plan_type && ['free_trial'].includes(el.plan_type) && 'Free Trial') || (el.plan_type && el.plan_type.includes('yearly') && `${el.plan_category} yearly`) || (el.plan_type && el.plan_type.includes('monthly') && `${el.plan_category} monthly`) || el.plan_type;
                            }
                        }).filter(x => x);
                        options = options && options.join(', ')
                        return options ? <div style={{ textTransform: 'capitalize' }}>{options}</div> : null;

                    },
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'name',
                    label: 'NAME',
                    sortColumn: 'Users.name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 5,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    sortColumn: 'createdAt',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'email',
                    label: 'EMAIL',
                    sortColumn: 'Users.email',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    disableOptons: {
                        edit: true
                    },
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 7,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: {
                        create: true
                    },
                },
                {
                    id: 8,
                    value: 'role',
                    label: 'ROLE',
                    sortColumn: 'Users.role',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['role'] === 'lawyer' ? 'Attorney' : row['role'] === 'paralegal' ? 'Non-Attorney' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'],
                    options: [
                        {
                            label: 'Attorney',
                            value: 'lawyer',
                        },
                        {
                            label: 'Non-Attorney',
                            value: 'paralegal',
                        }
                    ],
                    type: 'select',
                    mandatory: true
                },
                {
                    id: 9,
                    value: 'state_bar_number',
                    label: 'STATE BAR NUMBER',
                    editRecord: (record.role === 'lawyer') ? true : false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 10,
                    value: 'is_admin',
                    label: 'ADMIN',
                    sortColumn: 'Users.is_admin',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    visible: true,
                    html: (row) => row['is_admin'] ? 'Yes' : 'No',
                    type: 'checkbox'
                },
                {
                    id: 11,
                    value: 'is_admin',
                    label: 'Make a user an Admin',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    sort: true,
                    visible: false,
                    type: 'checkbox',
                    style: { paddingBottom: '0px' }
                },
                {
                    id: 12,
                    value: 'is_free_user',
                    label: 'Test/Free User',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    visible: false,
                    type: 'checkbox',
                    style: { paddingTop: '0px' }
                },
                {
                    id: 13,
                    value: 'is_free_user',
                    label: 'Test/Free User',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'checkbox',
                    html: (row) => row['is_free_user'] ? 'Yes' : 'No',
                }
            ],
            customDescription: (metaData, formRecord) => {
                const { practicesOptions = [] } = metaData;
                const practiceName = formRecord && Object.keys(formRecord).length && practicesOptions && practicesOptions.length && formRecord.practice_id && practicesOptions.filter((val) => val.value === formRecord.practice_id)[0][`label`] || "";

                return `This user account has already been created in another practice. Do you want to link this user to ${practiceName}?`
            }
        }
    }


    function opposingCounselForm() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'opposing_counsel',
                    label: 'OPPOSING COUNSEL INFO',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'fieldArray',
                    title: 'Counsel',
                    max: 5,
                    btnLabel: 'Add additional counsel info',
                    defaultField: true,
                    mandatory: true,
                    fieldArray: [
                        {
                            id: 1,
                            value: 'opposing_counsel_attorney_name',
                            label: 'OPPOSING COUNSEL ATTORNEY NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'multiInput',
                            inputLimit: 5,
                            mandatory: true
                        },
                        {
                            id: 2,
                            value: 'opposing_counsel_office_name',
                            label: 'OPPOSING COUNSEL PRACTICE NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 3,
                            value: 'opposing_counsel_street',
                            label: 'STREET',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            placeholder: 'eg: 101 Main Street'
                        },
                        {
                            id: 4,
                            value: 'opposing_counsel_city',
                            label: 'CITY',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 5,
                            value: 'opposing_counsel_state',
                            label: 'STATE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 6,
                            value: 'opposing_counsel_zip_code',
                            label: 'ZIP CODE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 7,
                            value: 'opposing_counsel_email',
                            label: 'EMAIL',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            placeholder: "Type valid email and press enter/return key",
                            type: 'multiEmail',
                            mandatory: true
                        }
                    ]
                }
            ]
        }
    }

    function feeWaiver() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true,
                },
                {
                    id: 2,
                    value: 'createdAt',
                    label: 'SENT DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'expiry_date',
                    label: 'EXPIRY DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['expiry_date'] && moment(row['expiry_date']).format('MM/DD/YY'),
                    type: 'input'
                }
            ],
            customNote: () => (`New customers are charged a one time activation fee in addition to the monthly or yearly subscription fee. You can waive the activation fee for a customer by entering the primary email id that would be used for sign up. Waivers would be valid for 7 days - i.e. if the customer sign up in the next 7 days activation fee will be waived.`)
        }
    }

    function excelExport() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'from_date',
                    label: 'FROM DATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'calendar',
                    mandatory: true,
                    placeholder: 'mm/dd/yyyy'
                },
                {
                    id: 2,
                    value: 'to_date',
                    label: 'TO DATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'calendar',
                    mandatory: true,
                    placeholder: 'mm/dd/yyyy'
                }
            ]
        }
    }

    function globalOtherSettings(user = {}) {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'HIPAA Compliance',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'session_timeout',
                            label: 'Session Expiration Timeout (in seconds)',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'number'
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Account Quotas',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'monthly_user_limit',
                            label: 'No. of users in monthly accounts',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'number'
                        },
                        {
                            id: 1,
                            value: 'yearly_user_limit',
                            label: 'No. of users in yearly accounts',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'number'
                        }
                    ]
                },
                {
                    schemaId: 3,
                    name: 'Custom Template Limitations',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'custom_template_limit',
                            label: 'No. of templates allowed',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'number'
                        }
                    ]
                },
                {
                    schemaId: 4,
                    name: 'Propounding Access',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: false,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Propound template changes will apply for selected practices. Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'propound_template_practices',
                            label: `Practices`,
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            selectAll: true,
                            options: 'practicesOptions',
                            type: 'multiSelect'
                        },
                        {
                            id: 2,
                            value: 'propound_template_practices_name',
                            label: `Practices`,
                            editRecord: false,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                    ]
                },
                {
                    schemaId: 5,
                    name: 'Tek-as-you-go Access',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Tek-as-you-go access changes will apply for selected practices. Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'tekasyougo_practices',
                            label: `Practices`,
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            selectAll: true,
                            options: 'practicesOptions',
                            type: 'multiSelect'
                        },
                        {
                            id: 2,
                            value: 'tekasyougo_practices_name',
                            label: `Practices`,
                            editRecord: false,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                    ]
                },
                {
                    schemaId: 6,
                    name: 'Pricing Date',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: user && ['siva@miotiv.com', 'admin@test.com'].includes(user.email) ? true : false,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'old_pricing_till',
                            label: `Old Pricing Till Date`,
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            placeholder: 'mm/dd/yyyy HH:mm:ss',
                            html: (row) => {
                                return row && row['old_pricing_till'] && <p>{moment(row['old_pricing_till']).format('MM/DD/YY HH:mm:ss')}</p>
                            }
                        },
                        {
                            id: 2,
                            value: 'new_pricings_from',
                            label: `New Pricing From Date`,
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            placeholder: 'mm/dd/yyyy HH:mm:ss',
                            html: (row) => {
                                return row && row['new_pricings_from'] && <p>{moment(row['new_pricings_from']).format('MM/DD/YY HH:mm:ss')}</p>
                            }
                        },
                        // {
                        //     id: 3,
                        //     value: 'new_responding_feature_from',
                        //     label: `New Responding Feature From Date`,
                        //     editRecord: true,
                        //     viewRecord: true,
                        //     viewMode: false,
                        //     visible: false,
                        //     type: 'input',
                        //     placeholder: 'mm/dd/yyyy HH:mm:ss',
                        //     html: (row) => {
                        //         return row && row['new_responding_feature_from'] && <p>{moment(row['new_responding_feature_from']).format('MM/DD/YY HH:mm:ss')}</p>
                        //     }
                        // },
                    ]
                },
                {
                    schemaId: 7,
                    name: 'Version History Limit',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: user && ['siva@miotiv.com', 'admin@test.com'].includes(user.email) ? true : false,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'response_history_limit',
                            label: `Limit`,
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'number',
                        },
                    ]
                },
                {
                    schemaId: 8,
                    name: 'Document Editor Access',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Document Editor access changes will apply for selected practices. Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'show_document_editor_ids',
                            label: `Practices`,
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            selectAll: true,
                            options: 'practicesOptions',
                            type: 'multiSelect'
                        },
                        {
                            id: 2,
                            value: 'show_document_editor_practice_name',
                            label: `Practices`,
                            editRecord: false,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                    ]
                },
                {
                    schemaId: 9,
                    name: 'AI Prompt',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'summary_question',
                            label: 'Doc Splitter Prompt',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'textarea'
                        },
                        {
                            id: 2,
                            value: 'summary_final_question',
                            label: 'Doc Splitter Final Prompt',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'textarea'
                        },
                        {
                            id: 3,
                            value: 'summary_template',
                            label: 'Generation Prompt',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'textarea'
                        },
                        {
                            id: 4,
                            value: 'headings_to_add',
                            label: 'Headings',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            html: (row) => {
                                return row && row['headings_to_add'];
                            },
                            type: 'textarea'
                        }
                    ]
                },
            ]
        }
    }

    function propoundUploadTemplate() {
        return {
            columns: [
                {
                    id: 1,
                    value: 's3_file_key',
                    label: 'file',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf, .doc, .docx',
                    type: 'upload'
                },
                {
                    id: 2,
                    value: 'file_name',
                    label: 'File Name',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'state',
                    label: 'State',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'propoundStateOptions',
                    mandatory: true,
                    type: 'autoComplete',
                },
                {
                    id: 4,
                    value: 'document_type',
                    label: 'Document Type',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: [
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        }
                    ],
                    type: 'select'
                }
            ]
        }
    }


    function propound() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'state',
                    label: 'STATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? (row['state'] == 'PL' || row['state'] == 'NPL') ? `Other (${row['state']})` : row['state'] : '-',
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'input'
                }
            ]
        }
    }

    function viewCases() {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'Propounding',
                    label: 'PROPOUNDING',
                    path: 'propound',
                    create: true,
                    edit: true,
                    view: true,
                    filter: (record) => record,
                },
                {
                    schemaId: 2,
                    name: 'Responding',
                    label: 'RESPONDING',
                    path: 'respond',
                    create: false,
                    edit: false,
                    view: false
                },
                {
                    schemaId: 3,
                    name: 'Medical history',
                    label: 'MEDICAL HISTORY',
                    path: 'medicalHistory',
                    create: true,
                    edit: true,
                    view: true,
                },
            ]
        }
    }


    function signature(user) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'signature',
                    label: 'Signature',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'signature',
                    showSignature: user && user.signature ? true : false,
                    signature: user && user.signature,
                }
            ]
        }
    }


    function propoundPOS(record = {}) {
        return [
            {
                schemaId: 1,
                editForm: true,
                key: 'serving_pos_date',
                label: 'Serving/PoS Date',
                columns: [
                    {
                        id: 1,
                        value: 'propounder_serving_date',
                        label: 'PoS DATE',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'calendar'
                    },
                    {
                        id: 2,
                        value: 'propounder_serving_date',
                        label: 'PoS DATE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => row && row['propounder_serving_date'] && moment(row['propounder_serving_date']).format('MM/DD/YYYY'),
                        type: 'input'
                    }
                ]
            },
            {
                schemaId: 2,
                editForm: true,
                key: 'propounder_opposing_counsel',
                label: 'Opposing Counsel',
                columns: [
                    {
                        id: 1,
                        value: 'opposing_counsel',
                        label: 'OPPOSING COUNSEL INFO',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        type: 'fieldArray',
                        title: 'Counsel',
                        max: 5,
                        btnLabel: 'Add additional counsel info',
                        defaultField: true,
                        mandatory: true,
                        fieldArray: [
                            {
                                id: 1,
                                value: 'opposing_counsel_attorney_name',
                                label: 'OPPOSING COUNSEL ATTORNEY NAME',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'multiInput',
                                inputLimit: 5,
                            },
                            {
                                id: 2,
                                value: 'opposing_counsel_office_name',
                                label: 'OPPOSING COUNSEL PRACTICE NAME',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 3,
                                value: 'opposing_counsel_street',
                                label: 'STREET',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input',
                                placeholder: 'eg: 101 Main Street'
                            },
                            {
                                id: 4,
                                value: 'opposing_counsel_city',
                                label: 'CITY',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 5,
                                value: 'opposing_counsel_state',
                                label: 'STATE',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 6,
                                value: 'opposing_counsel_zip_code',
                                label: 'ZIP CODE',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                type: 'input'
                            },
                            {
                                id: 7,
                                value: 'opposing_counsel_email',
                                label: 'EMAIL',
                                editRecord: true,
                                viewRecord: false,
                                viewMode: true,
                                visible: true,
                                mandatory: true,
                                placeholder: "Type valid email and enter",
                                type: 'multiEmail',
                            }
                        ]
                    },
                    {
                        id: 2,
                        value: 'opposing_counsel_attorney_name',
                        label: 'NAME',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 3,
                        value: 'opposing_counsel_office_name',
                        label: 'PRACTICE NAME',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 4,
                        value: 'opposing_counsel_address',
                        label: 'ADDRESS',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            const opposing_address = row && row['opposing_counsel_city'] && row['opposing_counsel_street'] && row['opposing_counsel_state'] && row['opposing_counsel_zip_code'] && row['opposing_counsel_street'];
                            return <span dangerouslySetInnerHTML={{ __html: (opposing_address ? row['opposing_counsel_street'] + '<br/>' + row['opposing_counsel_city'] + ', ' + row['opposing_counsel_state'] + ' ' + row['opposing_counsel_zip_code'] : '-') }} />;
                        },
                        type: 'input'
                    },
                    {
                        id: 5,
                        value: 'opposing_counsel_email',
                        label: 'EMAIL',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            const opposing_counsel_email = row && row['opposing_counsel_email'] && row['opposing_counsel_email'].split(',');
                            const email = opposing_counsel_email && opposing_counsel_email.map(el => <><span>{el}</span><br /></>);
                            return email
                        },
                        type: 'input',
                    }
                ]
            },
            {
                schemaId: 3,
                editForm: true,
                key: 'serving_attorney',
                label: 'Serving Attorney',
                columns: [
                    {
                        id: 1,
                        value: 'propounder_serving_attorney_name',
                        label: 'ATTORNEY NAME',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 2,
                        value: 'propounder_serving_attorney_street',
                        label: 'STREET',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input',
                        placeholder: 'eg: 101 Main Street'
                    },
                    {
                        id: 3,
                        value: 'propounder_serving_attorney_city',
                        label: 'CITY',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 4,
                        value: 'propounder_serving_attorney_state',
                        label: 'STATE',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 5,
                        value: 'propounder_serving_attorney_zip_code',
                        label: 'ZIP CODE',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input'
                    },
                    {
                        id: 6,
                        value: 'propounder_serving_attorney_email',
                        label: 'EMAIL',
                        editRecord: true,
                        viewRecord: false,
                        viewMode: true,
                        visible: true,
                        mandatory: true,
                        type: 'input',
                    },
                    {
                        id: 7,
                        value: 'propounder_serving_attorney_name',
                        label: 'NAME',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 8,
                        value: 'propounder_serving_attorney_address',
                        label: 'ADDRESS',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            return <span dangerouslySetInnerHTML={{ __html: (row && row['propounder_serving_attorney_street'] && row['propounder_serving_attorney_city'] && row['propounder_serving_attorney_state'] && row['propounder_serving_attorney_zip_code'] ? row['propounder_serving_attorney_street'] + '<br/>' + row['propounder_serving_attorney_city'] + ', ' + row['propounder_serving_attorney_state'] + ' ' + row['propounder_serving_attorney_zip_code'] : '-') }} />;
                        },
                        type: 'input'
                    },
                    {
                        id: 9,
                        value: 'propounder_serving_attorney_email',
                        label: 'EMAIL',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input',
                    }
                ]
            },
            {
                schemaId: 4,
                editForm: false,
                key: 'case_info',
                label: 'Case Information',
                columns: [
                    {
                        id: 1,
                        value: 'case_number',
                        label: 'CASE NUMBER',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 2,
                        value: 'case_title',
                        label: 'CASE TITLE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    },
                    {
                        id: 3,
                        value: 'document_type',
                        label: 'DOCUMENT TYPE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => {
                            const doc_type = row && row['document_type'] === 'FROGS' ? 'Form Interrogatories' : row['document_type'] === 'SPROGS' ? (row['state'] !== 'CA' ? 'Interrogatories' : 'Specially Prepared Interrogatories') : row['document_type'] === 'RFPD' ? 'Requests For Production of Documents' : row['document_type'] === 'RFA' ? 'Request For Admissions' : row['document_type'];
                            return doc_type && doc_type;
                        },
                        type: 'input'
                    },
                    {
                        id: 4,
                        value: 'state',
                        label: 'STATE',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        html: (row) => row && row.state == 'CA' ? 'California' : row.state == 'NV' ? 'Nevada' : row.state == 'TN' ? 'Tennessee' : row.state == 'AZ' ? 'Arizona' : row.state == 'WA' ? 'Washington' : row.state,
                        type: 'input',
                    },
                    {
                        id: 5,
                        value: 'county',
                        label: 'COUNTY',
                        editRecord: false,
                        viewRecord: true,
                        viewMode: true,
                        visible: true,
                        type: 'input'
                    }
                ]
            }
        ]
    }

    function propoundEmailDocument(records, caseInfo, actions, dispatch, documentStatus) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'to_opposing_counsel_email',
                    label: 'TO',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'multiEmail'
                },
                {
                    id: 2,
                    value: 'cc_propounder_email',
                    label: 'CC',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'selected_forms',
                    label: 'Select the document generated forms',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: records || [],
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];

                        const handleChange = (val, checked) => {
                            if (checked) {
                                if (inputArr.map(el => val.id != el.id)) {
                                    inputArr.push(val);
                                }
                            } else {
                                inputArr = inputArr.filter(a => val.id != a.id);
                            }

                            input.onChange(inputArr);
                        }

                        return (<Grid item xs={12}>
                            <EmailPropoundForm inputArr={inputArr} options={options} handleChange={handleChange} documentStatus={documentStatus} recordProps={Object.assign({}, { caseInfo, actions, dispatch })} />
                            <div style={(error || warning) && { fontSize: '14px', color: 'red', marginTop: '5px' } || {}}>
                                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
                            </div>
                        </Grid>)
                    },
                    type: 'component'
                },
                {
                    id: 4,
                    value: 'attach_documents',
                    label: 'Additional Documents (If any)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    contentType: 'application/pdf',
                    multiple: true,
                    max: 5,
                    labelValue: 'Files',
                    type: 'customFileUpload',
                }
            ]
        }
    }


    function propoundSupportRequest() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'user_email',
                    label: 'USER EMAIL',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    sort: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 3,
                    value: 'createdAt',
                    label: 'REQUEST DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row && row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'document_s3_key',
                    label: 'DOCUMENT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const value = row['document_s3_key'];
                        return <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(await getTemplateSecureFormURL(value))} />
                    },
                    type: 'download'
                }
            ]
        }
    }

    function respondCustomTemplate() {

        return {
            columns: [
                {
                    id: 1,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'file_name',
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'state',
                    label: 'STATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'state',
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? (row['state'] == 'PL' || row['state'] == 'NPL') ? `Other (${row['state']})` : row['state'] : '-',
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'createdAt',
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                },
                {
                    id: 3,
                    value: 'custom_template',
                    label: 'CUSTOM TEMPLATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input',
                    html: (row) => {
                        const custom = row['custom_template'];
                        return custom ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(custom)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 4,
                    value: 'custom_template',
                    label: 'Custom Document Template',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf, .doc, .docx',
                    type: 'upload'
                },
                {
                    id: 5,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    mandatory: true,
                    type: 'autoComplete',
                },
                {
                    id: 7,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'status',
                    type: 'input'
                }
            ],
            customNote: () => (`Once Template file is processed and ready for use, you will receive an email notification.`),
            modifiedTemplateOptions: (forms, type, selectTemplate) => {
                const formValue = forms && type && forms[`responderCustomTemplate_${type}`] && forms[`responderCustomTemplate_${type}`].values && forms[`responderCustomTemplate_${type}`].values;

                return [
                    {
                        id: 1,
                        value: 'custom_template',
                        label: 'Choose Template',
                        editRecord: true,
                        viewRecord: true,
                        viewMode: false,
                        visible: true,
                        mandatory: true,
                        options: 'modifiedTemplateOptions',
                        type: 'select'
                    },
                    {
                        id: 2,
                        value: 'selectTemplate',
                        label: 'Select Template',
                        editRecord: selectTemplate && formValue && Object.keys(formValue).length > 0 && formValue.custom_template ? true : false,
                        viewRecord: true,
                        viewMode: false,
                        visible: false,
                        mandatory: true,
                        options: [
                            {
                                label: 'Response With Questions',
                                value: 'withQuestions',
                            },
                            {
                                label: 'Response Without Questions',
                                value: 'withoutQuestions',
                            }
                        ],
                        render: ({ input, options, meta: { touched, error, warning } }) => {
                            let inputArr = input.value && typeof input.value === 'string' && input.value.split(',') || [];
                            const handleChange = (event) => {
                                if (event.target.value && event.target.checked) {
                                    input.onChange(event.target.value)
                                } else if (!event.target.checked) {
                                    input.onChange('');
                                }
                            };

                            return (<div style={{ display: 'flex' }}>
                                {options.map((opt, index) => (!opt.disableRecord &&
                                    <Grid item xs={5}>
                                        <FormControlLabel
                                            control={<Radio
                                                key={index}
                                                style={{ color: "#2ca01c" }}
                                                value={opt.value}
                                                checked={inputArr.includes(opt.value) || false}
                                                onChange={(e) => handleChange(e, opt)} />}
                                            label={<span style={{ fontSize: '15px' }} dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                    </Grid>
                                ))}
                            </div>)
                        },
                        type: 'component'
                    }
                ]
            },
            customConfirmMessage: (user, records) => {
                const planType = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['responding'] || false;
                const customTemplateRecords = records && records.length > 0 && records;
                const customTemplateLimit = user && user.custom_template_limit || false;

                return planType == '' ? `Sorry! Our custom template option is only available to our unlimited subscribers and it looks like you have no proper plan. If you would like to subscribe, please click "change plan" and pick one of the unlimited options or contact your account executive today.
                <br /><br />Thank you for using EsquireTek` : ['tek_as_you_go', 'pay_as_you_go', 'free_trial'].includes(planType) ? `Sorry! Our custom template option is only available to our unlimited subscribers and it looks like you are on our ${['tek_as_you_go', 'pay_as_you_go'].includes(planType) ? 'Pay as You Go' : 'Free trial'} Plan. If you would like to switch, please click "change plan" and pick one of the unlimited options or contact your account executive today.
                <br /><br />Thank you for using EsquireTek` : customTemplateRecords && customTemplateRecords.length >= customTemplateLimit ? 'You have reached the maximum number of templates allowed.' : false;
            }
        }
    }

    function propoundCustomTemplateSignature(user, modifiedTemplate, validSubscription, { attorneySignature, userRole }) {
        const record = modifiedTemplate || [];
        const showAttorneySignature = attorneySignature?.length > 1 && userRole === 'paralegal' ? true : false;

        return {
            section: [
                {
                    schemaId: 1,
                    editForm: showAttorneySignature,
                    key: 'signature',
                    title: 'Signature for Propounding',
                    optionalLabel: '(Optional)',
                    columns: [
                        {
                            id: 1,
                            value: 'attorney_signature',
                            label: 'Assigned Attorney Signature',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            showRequired: false,
                            type: 'select',
                            options: attorneySignature
                           
                        }
                    ]
                },
                {
                    schemaId: 2,
                    editForm: true,
                    key: 'signature',
                    title: showAttorneySignature ? 'Signature for PoS' : 'Signature for Propounding',
                    customTitleStyle: false,
                    optionalLabel: showAttorneySignature ? false : '(Optional)',
                    style: showAttorneySignature ? { paddingTop: '10px' } : {},
                    columns: [
                        {
                            id: 1,
                            value: 'signature',
                            label: 'Signature',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            showRequired: false,
                            type: 'customSignature',
                            signature: user && user.signature,
                        }
                    ]
                },
                {
                    schemaId: 3,
                    editForm: validSubscription && record && record.length > 1 && true || false,
                    key: 'custom_template',
                    title: 'Custom Template',
                    style: { paddingTop: '10px' },
                    columns: [
                        {
                            id: 1,
                            value: 'custom_template',
                            label: 'Choose Template',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            mandatory: true,
                            options: 'modifiedTemplateOptions',
                            type: 'select'
                        }
                    ]
                }
            ]
        }
    }

    function discountCode() {
        const discountCodeLimit = 50;
        return {
            columns: [
                {
                    id: 1,
                    value: 'discount_code',
                    label: 'PROMO CODE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    disableOptons: {
                        edit: true
                    },
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'discount_percentage',
                    label: 'DISCOUNT PERCENTAGE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    max: 3,
                    type: 'number'
                },
                {
                    id: 3,
                    value: 'plan_type',
                    label: 'PLANS INCLUDED',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        let valueArr = row['plan_type'] && row['plan_type'].split(',') || [];
                        valueArr = valueArr?.length > 0 && valueArr.filter(_ => !bothUserBasedPlans.includes(_)) || valueArr;
                        let options = valueArr && valueArr.length > 0 && valueArr.map((el) => (el && el === 'activation_fee' ? 'Activation Fee' : el === 'responding_monthly_495' ? 'Responding Monthly' : el === 'responding_yearly_5100' ? 'Responding Yearly' : el === 'propounding_monthly_199' ? 'Propounding Monthly' : el === 'propounding_yearly_2199' ? 'Propounding Yearly' : el));
                        options = options.join(', ')
                        return options ? <div style={{ textTransform: 'capitalize' }}>{options}</div> : null;

                    }
                },
                {
                    id: 4,
                    value: 'plan_type',
                    label: 'Plans included in promo code',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    html: (row) => {
                        let valueArr = row['plan_type'] && row['plan_type'].split(',') || [];
                        valueArr = valueArr?.length > 0 && valueArr.filter(_ => !bothUserBasedPlans.includes(_)) || valueArr;
                        return (<div>
                            {valueArr && valueArr.length > 0 ? valueArr.map((el, index) => (<div key={index} style={{ textTransform: 'capitalize' }}>
                                {el && el === 'activation_fee' ? 'Activation Fee': el === 'responding_monthly_495' ? 'Responding Monthly': el === 'responding_yearly_5100' ? 'Responding Yearly': el === 'propounding_monthly_199' ? 'Propounding Monthly': el === 'propounding_yearly_2199' ? 'Propounding Yearly': el}
                            </div>)) : '-'}
                        </div>)
                    },
                    options: [
                        {
                            label: 'Activation Fee',
                            value: 'activation_fee'
                        },
                        {
                            label: 'Responding Monthly',
                            value: 'responding_monthly_495'
                        },
                        {
                            label: 'Responding Yearly',
                            value: 'responding_yearly_5100'
                        },
                        {
                            label: 'Propounding Monthly',
                            value: 'propounding_monthly_199'
                        },
                        {
                            label: 'Propounding Yearly',
                            value: 'propounding_yearly_2199'
                        }
                    ],
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input.value && typeof input.value === 'string' && input.value.split(',') || [];
                        let contionalOptions;
                        if (inputArr && inputArr.length > 0) {
                            contionalOptions = options && options.length > 0 && options.reduce((acc, el) => {
                                let conditionalCheck = planCategoryArray.filter(_ => inputArr.some(e => e === _));
                                if (conditionalCheck && conditionalCheck.length > 0) {
                                    planCategoryArray.includes(el.value) ? acc.push(Object.assign({}, { ...el }, { disabled: false })) : acc.push(Object.assign({}, { ...el }, { disabled: true }));
                                } else {
                                    planCategoryArray.includes(el.value) ? acc.push(Object.assign({}, { ...el }, { disabled: true })) : acc.push(Object.assign({}, { ...el }, { disabled: false }));
                                }
                                return acc;
                            }, []);
                        } else {
                            contionalOptions = options;
                        }

                        const user_based_plans = {
                            'responding_monthly_495': 'users_responding_monthly_license_495',
                            'responding_yearly_5100': 'users_responding_yearly_license_5100',
                            'propounding_monthly_199': 'users_propounding_monthly_license_199',
                            'propounding_yearly_2199': 'users_propounding_yearly_license_2149_20'
                        }

                        return (<div>
                            <label style={{ fontSize: '1rem', marginBottom: '10px' }}>Plans included in promo code</label>
                            <div>
                                {contionalOptions.map((opt, index) => (<FormControlLabel
                                    disabled={opt.disabled ? true : false}
                                    control={<Checkbox
                                        key={index}
                                        style={!opt.disabled ? { color: "#2ca01c" } : {}}
                                        value={opt.value}
                                        checked={inputArr.includes(opt.value) || false}
                                        onChange={(e) => {
                                            if (e.target.checked) {
                                                inputArr.push(e.target.value);
                                                let additionalPlan = user_based_plans[e.target.value] || false;
                                                if(additionalPlan) {
                                                    inputArr.push(additionalPlan);
                                                }
                                            } else {
                                                inputArr = inputArr.filter(val => val !== opt.value);
                                                let additionalPlan = user_based_plans[opt.value] || false;
                                                if(additionalPlan) {
                                                    inputArr = inputArr.filter(val => val !== additionalPlan)
                                                }
                                            }
                                            input.onChange(inputArr.join(','));
                                        }} />}
                                    label={<span dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                ))}
                            </div>
                            {touched && error && <span style={{ color: 'red' }}>{error}</span>}
                        </div>)
                    },
                    type: 'component'
                },
                {
                    id: 5,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'createdAt',
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                }
            ],
            customNote: () => (`This page is to manage promo codes. Currently promo codes are supported for (a) activation fees, (b) subscription fees, and (c) propounding feature add on. Promo codes are not supported for medical history feature. System supports up-to ${discountCodeLimit} promo codes.`),
            customConfirmMessage: (user, records) => {
                const discountCodeRecords = records && records.length > 0 && records;
                return discountCodeRecords && discountCodeRecords.length >= discountCodeLimit ? 'You have reached the maximum number of promo codes allowed.' : false;
            }
        }
    }


    function filevineSecret() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'filevine_key',
                    label: 'API KEY',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'filevine_secret',
                    label: 'SECRET',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'filevine_api',
                    label: 'API BASE URL',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'is_store_filevine_secret',
                    label: 'Save this API key and secret for future use',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    sort: true,
                    visible: false,
                    type: 'checkbox',
                    style: { paddingBottom: '0px' }
                }
            ],
            instructionNote: () => (<>To get API Key and Secret, you need to follow the steps using this <a href="https://support.filevine.com/hc/en-us/articles/360032298611-Set-Up-API-v2" target="_blank" style={{ textDecoration: 'underline' }}>link</a>. Once you generate the keys, you can find the API Base Url from Orgs menu in the filevine portal.</>)
        }
    }

    function filevineList() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'CLIENT NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },

                {
                    id: 3,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 4,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    limit: true,
                    mandatory: true,
                }
            ]
        }
    }


    function clientWithCase(record, user, metaData) {
        const attorneys = metaData && metaData['users'] && metaData['users'].length > 0 && metaData['users'].reduce((acc, el) => {
            if (el && el.name && el.id && el.role === 'lawyer') {
                acc.push(el);
            }
            return acc;
        }, []) || false;

        return {
            columns: [
                {
                    id: 1,
                    value: 'client_phone',
                    label: 'CLIENT PHONE',
                    editRecord: record && record.client_phone ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 2,
                    value: 'case_plaintiff_name',
                    label: 'PLAINTIFF NAME',
                    editRecord: record && record.case_plaintiff_name ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'case_defendant_name',
                    label: 'DEFENDANT NAME',
                    editRecord: record && record.case_defendant_name ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 4,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: record && record.case_title ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    limit: true,
                    mandatory: true,
                },
                {
                    id: 5,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: record && record.case_number ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'county',
                    label: 'COUNTY',
                    editRecord: record && record.county ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                },
                {
                    id: 7,
                    value: 'state',
                    label: 'STATE',
                    editRecord: (record && record.state) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    mandatory: true,
                    type: 'autoComplete',
                },
                {
                    id: 8,
                    value: 'attorneys',
                    label: `ASSIGNED ATTORNEY`,
                    editRecord: record && record.attorneys ? false : attorneys && attorneys.length > 0 ? true : false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    selectAll: true,
                    options: 'usersOptions',
                    type: 'multiSelect'
                },
            ]
        }
    }

    function myCaseListView() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'CLIENT NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 3,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 4,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    limit: true,
                    mandatory: true,
                }
            ]
        }
    }

    function subscriptionDetails(record, billing) {
        const cardDetails = billing && billing.credit_card_details_available && billing.credit_card_details_available || false;

        return {
            columns: [
                {
                    id: 1,
                    value: 'plan_id',
                    label: 'Choose Plan',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    options: 'respondingPlansOptions',
                    type: 'select'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'Card Holder’s Name',
                    editRecord: cardDetails ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'cardDetails',
                    label: 'Card Details',
                    editRecord: cardDetails ? false : true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    showRequired: true,
                    type: 'stripe'
                },
                {
                    id: 4,
                    value: 'badge',
                    label: false,
                    editRecord: cardDetails ? false : true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'component',
                    style: { paddingBottom: 0, paddingTop: 0 },
                    render: () => {
                        const cards = ['visa', 'masterCard', 'amex', 'discover', 'cardO', 'unionPay'];
                        return (
                            <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                <div>
                                    {cards?.map((e, i) => {
                                        return <Icons type={e} key={i} style={{ width: '40px', marginRight: '6px' }} />
                                    })}
                                </div>
                                <div>
                                    <Icons type={'stripe'} style={{ width: '100px' }} />
                                </div>

                            </div>
                        )
                    }
                },
                {
                    id: 5,
                    value: 'discount_code',
                    label: 'Promo Code (If any)',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'terms',
                    label: `EsquireTek Terms of Use for Subscription Plans`,
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'component',
                    render: () => {
                        return <span dangerouslySetInnerHTML={{ __html: `By subscribing, you agree to our <a href='https://esquiretek-policy.s3-us-west-1.amazonaws.com/subscription_terms.html' style='color:#0077c5' target='_blank'>EsquireTek Terms of Use for Subscription Plans</a>` }} />
                    }
                },
            ],
            subscriptionBadge: {
                logo: 'guarantee',
                guaranteeText: `30-Days Money Back Guarantee`
            }

        }
    }


    function customTemplateSignature(user, modifiedTemplate, validSubscription, forms, type, selectTemplate, customDetails) {
        const record = modifiedTemplate || [];
        const formValue = forms && forms[`respondCustomTemplateAndSignature_${type}`] && forms[`respondCustomTemplateAndSignature_${type}`].values && forms[`respondCustomTemplateAndSignature_${type}`].values;
        const documentTypeCheck = type && type != 'pos';
        const userRole = user && user?.role;
        const hideSignature = type && userRole && userRole === 'paralegal' && type === 'final' ? true : false;
        const { showSignature, attorneySignatureOptions } = customDetails;

        return {
            section: [
                {
                    schemaId: 1,
                    editForm: type != 'template' && showSignature ? true : false,
                    key: 'signature',
                    title: 'Signature ',
                    optionalLabel: '(Optional)',
                    columns: [
                        {
                            id: 1,
                            value: 'signature',
                            label: 'Signature',
                            editRecord: !hideSignature ? true : false,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            showRequired: false,
                            type: 'customSignature',
                            signature: user && user.signature,
                        },
                        {
                            id: 2,
                            value: 'signature',
                            label: 'Assigned Attorney Signature',
                            editRecord: hideSignature ? true : false,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            showRequired: false,
                            type: 'select',
                            options : attorneySignatureOptions,
                        }
                    ]
                },
                {
                    schemaId: 2,
                    editForm: documentTypeCheck && validSubscription && record && record.length > 1 ? true : false,
                    key: 'custom_template',
                    title: 'Custom Template',
                    style: { paddingTop: '10px' },
                    columns: [
                        {
                            id: 1,
                            value: 'custom_template',
                            label: 'Choose Template',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            mandatory: true,
                            options: 'modifiedTemplateOptions',
                            type: 'select'
                        }
                    ]
                },
                {
                    schemaId: 3,
                    editForm: documentTypeCheck && documentTypeCheck && validSubscription && selectTemplate && record && record.length > 1 && formValue && Object.keys(formValue).length > 0 && formValue.custom_template ? true : false,
                    key: 'select_template',
                    title: '',
                    columns: [
                        {
                            id: 1,
                            value: 'selectTemplate',
                            label: 'Select Template',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            options: [
                                {
                                    label: 'Response With Questions',
                                    value: 'withQuestions',
                                },
                                {
                                    label: 'Response Without Questions',
                                    value: 'withoutQuestions',
                                }
                            ],
                            render: ({ input, options, meta: { touched, error, warning } }) => {
                                let inputArr = input.value && typeof input.value === 'string' && input.value || '';
                                const handleChange = (event) => {
                                    if (event.target.value && event.target.checked) {
                                        input.onChange(event.target.value)
                                    } else if (!event.target.checked) {
                                        input.onChange('')
                                    }
                                };

                                return (<div style={{ display: 'flex', paddingTop: '15px' }}>
                                    {options.map((opt, index) => (!opt.disableRecord &&
                                        <Grid item xs={5}>
                                            <FormControlLabel
                                                control={<Radio
                                                    key={index}
                                                    style={{ color: "#2ca01c" }}
                                                    value={opt.value}
                                                    checked={inputArr.includes(opt.value) || false}
                                                    onChange={(e) => handleChange(e, opt)} />}
                                                label={<span style={{ fontSize: '15px' }} dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                        </Grid>
                                    ))}
                                </div>)
                            },
                            type: 'component'
                        }
                    ]
                },
                {
                    schemaId: 4,
                    editForm: documentTypeCheck && documentTypeCheck && selectTemplate && record && record.length <= 1 ? true : false,
                    key: 'select_template',
                    title: 'Select Template',
                    style: { paddingTop: '10px' },
                    columns: [
                        {
                            id: 1,
                            value: 'selectTemplate',
                            label: 'Select Template',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            options: [
                                {
                                    label: 'Response With Questions',
                                    value: 'withQuestions',
                                },
                                {
                                    label: 'Response Without Questions',
                                    value: 'withoutQuestions',
                                },
                            ],
                            type: 'select',
                            mandatory: true
                        }
                    ]
                }
            ]
        }
    }

    function respondEmailDocument(records, caseInfo, actions, dispatch, documentStatus) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'to_opposing_counsel_email',
                    label: 'TO',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    disableOptons: {
                        edit: true
                    },
                    type: 'multiEmail'
                },
                {
                    id: 2,
                    value: 'cc_responder_email',
                    label: 'CC',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    limit: true,
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'selected_forms',
                    label: 'Select the document generated forms',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: records || [],
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];

                        const handleChange = (val, checked) => {
                            if (checked) {
                                if (inputArr.map(el => val.id != el.id)) {
                                    inputArr.push(val);
                                }
                            } else {
                                inputArr = inputArr.filter(a => val.id != a.id);
                            }

                            input.onChange(inputArr);
                        }

                        return (<Grid item xs={12}>
                            <EmailRespondForm inputArr={inputArr} options={options} handleChange={handleChange} documentStatus={documentStatus} recordProps={Object.assign({}, { caseInfo, actions, dispatch })} />
                            <div style={{ fontSize: '14px', color: 'red', marginTop: '5px' } || {}}>
                                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
                            </div>
                        </Grid>)
                    },
                    type: 'component'
                },
                {
                    id: 4,
                    value: 'attach_documents',
                    label: 'Additional Documents (If any)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    contentType: 'application/pdf',
                    multiple: true,
                    max: 5,
                    labelValue: 'Files',
                    type: 'customFileUpload',
                }
            ]
        }
    }

    function propoundCustomTemplate() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'file_name',
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'state',
                    label: 'STATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'state',
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? (row['state'] == 'PL' || row['state'] == 'NPL') ? `Other (${row['state']})` : row['state'] : '-',
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'createdAt',
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                },
                {
                    id: 3,
                    value: 'custom_template',
                    label: 'CUSTOM TEMPLATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input',
                    html: (row) => {
                        const custom = row['custom_template'];
                        return custom ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(custom)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 4,
                    value: 'custom_template',
                    label: 'Custom Document Template',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf, .doc, .docx',
                    type: 'upload'
                },
                {
                    id: 5,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'propoundStateOptions',
                    mandatory: true,
                    type: 'autoComplete',
                },
                {
                    id: 7,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'status',
                    type: 'input'
                }
            ]
        }
    }

    function cancelSubcriptionsOptions() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'cancel_subscription_options',
                    label: '',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: [
                        {
                            label: `Cancel Responding Plan`,
                            value: 'responding'
                        },
                        {
                            label: 'Cancel Propounding Plan',
                            value: 'propounding'
                        }
                    ],
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input.value && typeof input.value === 'string' && input.value.split(',') || [];
                        const handleChange = (evt, opt) => {
                            const value = evt.target.value;
                            if (evt.target.checked) {
                                value.split(',').map(x => {
                                    if (!inputArr.includes(x.id))
                                        inputArr.push(x);
                                });
                            } else {
                                inputArr = inputArr.filter(val => val !== opt.value);
                            }
                            input.onChange(inputArr.join(','));
                        }

                        return (<div>
                            {options.map((opt, index) => (!opt.disableRecord &&
                                <Grid>
                                    <FormControlLabel
                                        control={<Checkbox
                                            key={index}
                                            style={{ color: "#2ca01c" }}
                                            value={opt.value}
                                            checked={inputArr.includes(opt.value) || false}
                                            onChange={(e) => handleChange(e, opt)} />}
                                        label={<span style={{ fontSize: '16px' }} dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                </Grid>
                            ))}
                        </div>)
                    },
                    type: 'component'
                }
            ]
        }
    }

    function subscriptionRenewalMonthly() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'plan_name',
                    label: 'PLAN TYPE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row['plan_name'] && row['plan_name'].toUpperCase(),
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'subscribed_on',
                    label: 'SUBSCRIBED ON',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => row['subscribed_on'] && moment(row['subscribed_on']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'subscribed_valid_till',
                    label: 'VALID UPTO',
                    sortColumn: 'subscribed_valid_till',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['subscribed_valid_till'] && moment(row['subscribed_valid_till']).format('MM/DD/YY'),
                    type: 'date'
                }
            ]
        }
    }

    function subscriptionRenewalYearly() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'plan_name',
                    label: 'PLAN TYPE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => {
                        const planType = row && row['plan_name'] && row['plan_name'];
                        return planType && planType.includes('YEARLY') ? 'YEARLY' : planType && planType.includes('MONTHLY') ? 'MONTHLY' : planType
                    },
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'subscribed_on',
                    label: 'SUBSCRIBED ON',
                    sortColumn: 'subscribed_on',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row['subscribed_on'] && moment(row['subscribed_on']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'subscribed_valid_till',
                    label: 'VALID UPTO',
                    sortColumn: 'subscribed_valid_till',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row['subscribed_valid_till'] && moment(row['subscribed_valid_till']).format('MM/DD/YY'),
                    sort: true,
                    type: 'date'
                }
            ]
        }
    }

    function discountCodeHistory() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'PRACTICE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'promocode',
                    label: 'DISCOUNT CODE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'base_price',
                    label: 'BASE PRICE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row['base_price'] && `$${row['base_price']}`,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'discount_percentage',
                    label: 'DISCOUNT PERCENTAGE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'discount_amount',
                    label: 'DISCOUNT AMOUNT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row['discount_amount'] && `$${row['discount_amount']}`,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'discounted_price',
                    label: 'DISCOUNTED PRICE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row['discounted_price'] && `$${row['discounted_price']}`,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'discount_for',
                    label: 'DISCOUNT FOR',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const value = row['discount_for'] && row['discount_for'] || [];
                        let options = value === 'activationFee' ? 'Activation Fee' : value === 'responding_monthly' ? 'Responding Monthly' : value === 'responding_yearly' ? 'Responding Yearly' : value === 'propounding_monthly' ? 'Propounding Monthly' : value === 'propounding_yearly' ? 'Propounding Yearly' : value;
                        return options ? <div style={{ textTransform: 'capitalize' }}>{options}</div> : null;

                    }
                },
                {
                    id: 8,
                    value: 'plan_category',
                    label: 'PLAN CATEGORY',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    html: (row) => row && row['plan_category'] && row['plan_category'].toUpperCase()
                },
                {
                    id: 9,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY'),
                    type: 'date'
                },
            ]
        }
    }

    function selectQuestionTemplate() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'selectTemplate',
                    label: 'Select Template',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    options: [
                        {
                            label: 'Response With Questions',
                            value: 'withQuestions',
                        },
                        {
                            label: 'Response Without Questions',
                            value: 'withoutQuestions',
                        },
                    ],
                    type: 'select',
                    mandatory: true
                }
            ]
        }
    }

    function customDateFilter() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'from_date',
                    label: 'FROM DATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'calendar',
                    mandatory: true,
                    placeholder: 'mm/dd/yyyy'
                },
                {
                    id: 2,
                    value: 'to_date',
                    label: 'TO DATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'calendar',
                    mandatory: true,
                    placeholder: 'mm/dd/yyyy'
                }
            ]
        }
    }

    function activeFilter(name) {
        const label = name && name == 'practiceUsers' ? 'Users' : name == 'practices' ? 'Practices' : name;
        return {
            columns: [
                {
                    id: 1,
                    value: 'active',
                    label: <Typography variant="body2">{`Active ${label}`}</Typography>,
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'switch',
                    labelPlacement: 'start',
                    customStyle: { marginRight: 'auto', marginLeft: '3px' },
                    size: 'small'
                }
            ]
        }
    }


    function authLogin() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'identifier',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    disabled: true,
                    type: 'input',
                },
            ]
        }
    }

    function createDocument(record, metaData, initialRecord) {
        const attorneys = metaData && metaData['users'] && metaData['users'].length > 1 ? true : false;
        const document_type = initialRecord && initialRecord.document_type && initialRecord.document_type.toUpperCase() || false;
        const state = initialRecord?.state;

        return {
            section: [
                {
                    schemaId: 1,
                    create: true,
                    view: true,
                    columns: [
                        // {
                        //     id: 1,
                        //     value: 'client_name',
                        //     label: 'CLIENT NAME',
                        //     editRecord: true,
                        //     viewRecord: false,
                        //     viewMode: true,
                        //     visible: true,
                        //     type: 'input',
                        //     mandatory: true
                        // },
                        {
                            id: 1,
                            value: 'first_name',
                            label: 'FIRST NAME',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            limit: true,
                            asterisks: true
                        },
                        {
                            id: 2,
                            value: 'middle_name',
                            label: 'MIDDLE NAME',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            limit: true,
                        },
                        {
                            id: 3,
                            value: 'last_name',
                            label: 'LAST NAME',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            limit: true,
                            asterisks: true
                        },
                        {
                            id: 2,
                            value: 'case_plaintiff_name',
                            label: 'PLAINTIFF NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 3,
                            value: 'case_defendant_name',
                            label: 'DEFENDANT NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 4,
                            value: 'case_title',
                            label: 'CASE TITLE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            limit: true,
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 5,
                            value: 'case_number',
                            label: 'CASE NUMBER',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        },

                    ]
                },
                {
                    schemaId: 2,
                    create: true,
                    view: true,
                    columns: [
                        {
                            id: 1,
                            value: 'county',
                            label: 'COUNTY',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            style: { paddingTop: '0px' },
                            asterisks: true
                        },
                        {
                            id: 2,
                            value: 'state',
                            label: 'STATE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            options: 'stateOptions',
                            disableOptons: {
                                create: document_type && document_type === 'FROGS' ? true : false
                            },
                            mandatory: true,
                            type: 'autoComplete',
                            asterisks: true
                        },
                        {
                            id: 3,
                            value: 'document_type',
                            label: 'DOCUMENT TYPE',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            disableOptons: {
                                create: document_type && document_type === 'FROGS' ? true : false
                            },
                            options: record?.state === 'NY' ? [
                                {
                                    label: 'BOP',
                                    value: 'BOP'
                                },
                                {
                                    label: 'ODD',
                                    value: 'ODD'
                                },
                                {
                                    value: 'SPROGS',
                                    label: 'ROGS'
                                },
                                {
                                    label: 'NDI',
                                    value: 'NDI'
                                },
                                {
                                    label: 'NTA',
                                    value: 'NTA'
                                },
                            ]: document_type && document_type !== 'FROGS' ? [
                                {
                                    value: 'SPROGS',
                                    label: state === 'CA' ? 'SPROGS' : 'ROGS'
                                },
                                {
                                    value: 'RFPD',
                                    label: 'RFPD'
                                },
                                {
                                    value: 'RFA',
                                    label: 'RFA'
                                }
                            ] : [
                                {
                                    value: 'SPROGS',
                                    label: 'SPROGS'
                                },
                                {
                                    value: 'RFPD',
                                    label: 'RFPD'
                                },
                                {
                                    value: 'RFA',
                                    label: 'RFA'
                                }
                            ],
                            type: 'select',
                            asterisks: true
                        },
                        {
                            id: 4,
                            value: 'set_number',
                            label: 'SET NUMBER',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            asterisks: true
                        }
                    ]
                },
                {
                    schemaId: 3,
                    create: true,
                    view: true,
                    columns: [
                        {
                            id: 1,
                            value: 'phone',
                            label: 'PHONE',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: true,
                            visible: true,
                            type: 'phone',
                            max: 14,
                            mandatory: true,
                            asterisks: true
                        },
                        {
                            id: 2,
                            value: 'email',
                            label: 'EMAIL',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input',
                        },
                        {
                            id: 3,
                            value: 'dob',
                            label: 'BIRTHDATE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'input',
                            mandatory: true,
                            placeholder: 'mm/dd/yyyy',
                        },
                        {
                            id: 4,
                            value: 'attorneys',
                            label: `ASSIGNED ATTORNEY`,
                            editRecord: attorneys ? true : false,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            selectAll: true,
                            options: 'usersOptions',
                            type: 'multiSelect'
                        },
                    ]
                },
                {
                    schemaId: 4,
                    create: true,
                    view: true,
                    columns: [
                        {
                            value: 'opposing_counsel',
                            label: 'OPPOSING COUNSEL INFO',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            type: 'fieldArray',
                            title: 'Counsel',
                            max: 5,
                            btnLabel: 'Add additional counsel info',
                            defaultField: true,
                            mandatory: true,
                            fieldArray: [
                                {
                                    id: 1,
                                    value: 'opposing_counsel_attorney_name',
                                    label: 'OPPOSING COUNSEL ATTORNEY NAME',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    type: 'multiInput',
                                    inputLimit: 5,
                                    mandatory: true,
                                    asterisks: true
                                },
                                {
                                    id: 2,
                                    value: 'opposing_counsel_office_name',
                                    label: 'OPPOSING COUNSEL PRACTICE NAME',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    type: 'input',
                                    mandatory: true,
                                    asterisks: true
                                },
                                {
                                    id: 3,
                                    value: 'opposing_counsel_street',
                                    label: 'STREET',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    type: 'input',
                                    mandatory: true,
                                    placeholder: 'eg: 101 Main Street',
                                    asterisks: true
                                },
                                {
                                    id: 4,
                                    value: 'opposing_counsel_city',
                                    label: 'CITY',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    type: 'input',
                                    mandatory: true,
                                    asterisks: true
                                },
                                {
                                    id: 5,
                                    value: 'opposing_counsel_state',
                                    label: 'STATE',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    type: 'input',
                                    mandatory: true,
                                    asterisks: true
                                },
                                {
                                    id: 6,
                                    value: 'opposing_counsel_zip_code',
                                    label: 'ZIP CODE',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    type: 'input',
                                    mandatory: true,
                                    asterisks: true
                                },
                                {
                                    id: 7,
                                    value: 'opposing_counsel_email',
                                    label: 'EMAIL',
                                    editRecord: true,
                                    viewRecord: true,
                                    viewMode: false,
                                    visible: false,
                                    placeholder: "Type valid email and press enter/return key",
                                    type: 'multiEmail',
                                    mandatory: true,
                                    asterisks: true
                                }
                            ]
                        }
                    ]

                },
            ]
        }
    }

    function userRecordForm(record = {}) {
        const state_bar_number = record && record.state_bar_number && record.state_bar_number || false;
        const user_name = record && record.user_name && record.user_name || false;
        const userRole = record?.role === 'lawyer';
        return {
            columns: [
                {
                    id: 1,
                    value: 'user_name',
                    label: 'ATTORNEY NAME',
                    editRecord: !user_name ? true : false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'state_bar_number',
                    label: 'STATE BAR NUMBER',
                    editRecord: !state_bar_number && userRole ? true : false,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                }
            ]
        }
    }

    function extractionSupportRequest() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    sort: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'legalform_filename',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    sort: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 3,
                    value: 'createdAt',
                    label: 'REQUEST DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row && row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'public_url',
                    label: 'DOCUMENT',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => {
                        const value = row['public_url'];
                        return <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(value)} />
                    },
                    type: 'download'
                }
            ]
        }
    }

    function clientCaseInfo(formValue) {
        return {
            columns: [
                // {
                //     id: 1,
                //     value: 'name',
                //     label: 'CLIENT NAME',
                //     editRecord: true,
                //     viewRecord: true,
                //     viewMode: true,
                //     visible: true,
                //     sort: true,
                //     type: 'input',
                //     mandatory: true
                // },
                {
                    id: 1,
                    value: 'first_name',
                    label: 'FIRST NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    mandatory: true,
                    limit: true
                },
                {
                    id: 2,
                    value: 'middle_name',
                    label: 'MIDDLE NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    limit: true,
                },
                {
                    id: 3,
                    value: 'last_name',
                    label: 'LAST NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    mandatory: true,
                    limit: true
                },
                {
                    id: 4,
                    value: 'case_plaintiff_name',
                    label: 'PLAINTIFF NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 5,
                    value: 'case_defendant_name',
                    label: 'DEFENDANT NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 6,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    limit: true,
                },
                {
                    id: 7,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'county',
                    label: 'COUNTY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                },
                {
                    id: 9,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    mandatory: true,
                    type: 'autoComplete',
                },
                {
                    id: 10,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: formValue?.state === 'NY' ? [
                        {
                            label: 'BOP',
                            value: 'BOP'
                        },
                        {
                            label: 'ODD',
                            value: 'ODD'
                        },
                        {
                            value: 'SPROGS',
                            label: 'ROGS'
                        },
                        {
                            label: 'NDI',
                            value: 'NDI'
                        },
                        {
                            label: 'NTA',
                            value: 'NTA'
                        },
                    ] : [
                        {
                            value: 'FROGS',
                            label: 'FROGS',
                            disabled: formValue && formValue.state && formValue.state == 'CA' ? false : true
                        },
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        }
                    ],
                    type: 'select'
                },
                {
                    id: 11,
                    value: 'set_number',
                    label: 'SET NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                }
            ]
        }
    }

    function quickCreateDocumentInfo(record) {
        const state = record && record.existing_case_state && record.existing_case_state.toUpperCase() || false;

        return {
            columns: [
                {
                    id: 1,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: state && state === 'NY' ? [
                        {
                            label: 'BOP',
                            value: 'BOP'
                        },
                        {
                            label: 'ODD',
                            value: 'ODD'
                        },
                        {
                            value: 'SPROGS',
                            label: 'ROGS'
                        },
                        {
                            label: 'NDI',
                            value: 'NDI'
                        },
                        {
                            label: 'NTA',
                            value: 'NTA'
                        },
                    ]: [
                        {
                            value: 'SPROGS',
                            label: state && state != 'CA' ? 'ROGS' : 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        }
                    ],
                    type: 'select'
                },
            ]
        }
    }

    function quickCreateClientCase(record) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'client_phone',
                    label: 'CLIENT PHONE',
                    editRecord: (record && record.client_phone) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 2,
                    value: 'case_plaintiff_name',
                    label: 'PLAINTIFF NAME',
                    editRecord: (record && record.case_plaintiff_name) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'case_defendant_name',
                    label: 'DEFENDANT NAME',
                    editRecord: (record && record.case_defendant_name) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 4,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: (record && record.case_title) ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                    limit: true,
                    mandatory: true,
                },
                {
                    id: 5,
                    value: 'case_number',
                    label: 'CASE NUMBER',
                    editRecord: (record && record.case_number) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'county',
                    label: 'COUNTY',
                    editRecord: (record && record.county) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    type: 'input',
                    mandatory: true,
                },
                {
                    id: 7,
                    value: 'state',
                    label: 'STATE',
                    editRecord: (record && record.state) ? false : true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    options: 'stateOptions',
                    mandatory: true,
                    type: 'autoComplete',
                },
            ]
        }
    }

    function litifyListView() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'CLIENT NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 3,
                    value: 'phone',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    max: 14,
                    type: 'phone'
                },
                {
                    id: 4,
                    value: 'case_title',
                    label: 'CASE TITLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    limit: true,
                    mandatory: true,
                }
            ]
        }
    }

    function helpFAQs() {
        return {
            helpFAQs: [
                {
                    schemaId: 1,
                    heading: "getting started",
                    list: [
                        {
                            title: "HOW DO I GET STARTED?",
                            paragraph: 'First, you create an account by clicking SIGN UP in the top right corner, fill in the required information, choose your pay option and you are off to the races! If you need help, you can schedule a demo anytime by <a href="https://www.esquiretek.com/#demo" target="_blank" style="color:#2ca01c">clicking here.</a><br></br>You can also visit our <a href="https://www.youtube.com/playlist?list=PLQgKdxe3wrYudTGAfo1P7Nvf1VHFtaO2K" target="_blank" style="color:#2ca01c">Tek Academy</a> on YouTube.<br></br>To run your first discovery set, just click CLIENTS to create a client file, then click CASES and create a case for that client. Once the case is created, click on the case file and you will be taken to our discovery page where you can start uploading!'
                        },
                        {
                            title: "HOW DO I CONTACT CUSTOMER SERVICE?",
                            paragraph: 'You can email us at <a  href="mailto:support@esquiretek.com" target="_blank" style="color: #2ca01c">support@esquiretek.com</a>.'
                        },
                        {
                            title: "CAN I USE THIS IN MY AREA OF LAW?",
                            paragraph: 'If you are a civil litigator, then EsquireTek is for you! We have a myriad of lawyers who use us for their discovery needs. Personal injury and employment lawyers make up the majority of our users, but we also have plenty of family law lawyers, business litigators, insurance defense lawyers, and government entity lawyers.'
                        },
                        {
                            title: "IS THIS ONLY FOR PROPOUNDING DISCOVERY OR RESPONDING TO DISCOVERY?",
                            paragraph: 'As of now, this is only for responding to discovery. There are new features being released quarterly and we are sure we will have a solution for propounding discovery very soon.'
                        },
                        {
                            title: "DO YOU SUPPORT FEDERAL DOCUMENTS?",
                            paragraph: 'Yes, we support federal documents.'
                        },
                        {
                            title: "IS ESQUIRETEK AVAILABLE IN MY STATE?",
                            paragraph: 'Esquire is currently available in California, Florida, Georgia, Indiana, Iowa, Minnesota, Nevada, Tennessee, and Texas! If you don’t see your state on the list please don’t hesitate to reach out as we are always adding new states to our service area.'
                        }
                    ]
                },
                {
                    schemaId: 2,
                    heading: "Account Questions",
                    list: [
                        {
                            title: "HOW DO I ACCESS MY FREE TRIAL?",
                            listItems: [
                                `Go to <a href="https://www.esquiretek.com" target="_blank" style="color: #2ca01c">esquiretek.com</a>`,
                                `In the upper right-hand corner click <strong>‘Sign Up’</strong>`,
                                `Add practice name, attorney, email, password state bar number`,
                                `Check the <strong>‘I have read and accepted EsquireTek Privacy Policy’</strong> box`,
                                `Check the <strong>‘I have read and accepted EsquireTek Terms of Use’</strong> box`,
                                `Click <strong>‘Register’</strong>`,
                                `Close out of the pay screen and the system will automatically load your free credits`,
                                `Sign In and start using EsquireTek`
                            ],
                        },
                        {
                            title: "HOW DO I CHANGE THE USER EMAIL?",
                            paragraph: 'Unfortunately, you can’t change the user email at this time. You will have to delete the user profile and add a user with the new email. But don’t worry! No work will be lost as the cases are stored on the cloud.'
                        },
                        {
                            title: "HOW DO I CHANGE MY PASSWORD?",
                            paragraph: 'You can change it manually on any screen within the system. At the top of your screen, you will see a key icon next to your email address. Click on the key and a screen will pop up, prompting you to change your password.'
                        },
                        {
                            title: "HOW DO I ADD A USER?",
                            listItems: [
                                `Go to the <strong>‘Users’</strong> Tab — Only admin accounts have this tab. If you don’t see it in your profile you’ll have to reach out to your admin, this may be the attorney or IT, to create the profile.</a>`,
                                `In the top right click <strong>‘Add New User’</strong>`,
                                `Add the user’s name`,
                                `Add the user’s e-mail`,
                                `Assign the user a password,`,
                                `Choose the user role (attorney or non-attorney). If they are an attorney you’ll need to enter their bar number.`,
                                `Check the <strong>‘Make a User an Admin’</strong> box to give them admin status. Making someone an admin gives them access to modify users, objections, templates, and the firm’s profile.`,
                                `Click <strong>‘Create’</strong>`
                            ],
                            paragraph1: 'The user will get an email asking them to sign in to EsquireTek where they will have to change their password.'
                        },
                        {
                            title: "HOW DO I DELETE A USER?",
                            listItems: [
                                `Go to the <strong>‘Users’</strong> Tab — Only admin accounts have this tab. If you don’t see it in your profile you’ll have to reach out to your admin, this may be the attorney or IT, to create the profile.`,
                                `Click the user`,
                                `Click <strong>‘Delete’</strong>`,
                                `Click <strong>‘Yes’</strong>`

                            ],
                        },
                        {
                            title: "HOW DO I CHANGE MY PLAN?",
                            paragraph: 'If you are the admin of your account, you can change your plan anytime by clicking on SETTINGS in the navigation bar. Once in the settings tab, you will see a CHANGE PLAN button. Depending on the plan you are on, if you change your plan, your new plan will start at the new billing cycle.'
                        },
                        {
                            title: "HOW DO I CANCEL MY SUBSCRIPTION?",
                            paragraph: 'We’re sorry to see you go! Please contact <a href="mailto:support@esquiretek.com" style="color:#2ca01c">support@esquiretek.com</a> to cancel your subscription and deactivate your account.'
                        },
                    ]
                },
                {
                    schemaId: 3,
                    heading: "User Questions",
                    list: [
                        {
                            title: "HOW DO I ADD A NEW CLIENT?",
                            paragraph: 'Go to the <strong>‘Client’ Tab</strong>',
                            listItems: [
                                `In the top right click <strong>‘Add New Client’</strong>`,
                                `Add the client name the way you want it seen in your response</li><li>Add the client e-mail (optional)`,
                                `Add the client cell phone number`,
                                `Add the client’s birthdate and address (optional)`,
                                `Click <strong>‘Create’</strong>`,
                                `Do you want to add a new case for this client now? Click  <strong>‘Yes’</strong>`

                            ],
                            paragraph1: 'The system will redirect you to the <strong>‘Cases’</strong> Tab and prompt you to create a case'
                        },
                        {
                            title: "HOW DO I CREATE A CASE?",
                            paragraph: 'Go to the <strong>‘Cases’ Tab</strong>',
                            listItems: [
                                `Click <strong>’Add New Case’</strong>`,
                                `Choose an existing client (If it’s a new client, ADD A NEW CLIENT first)`,
                                `Add Plaintiff name as you want it seen on your pleading (ex. John Doe, an Individual)`,
                                `Add Defendant name as you want it seen on your pleading (ex. Jose Doe, et. al.)`,
                                `Add the date of loss`,
                                `Add the court case number`,
                                `Add matter ID (optional)`,
                                `Add claim number (optional)`,
                                `Add county`,
                                `Add State`,
                                `If it’s a federal case check the federal box and insert the federal district`,
                                `Add Assigned Attorney in the order you want them to appear on your response`,
                                `Add opposing counsel Info (optional)`,
                                `Click <strong>‘Create’</strong>`
                            ],
                        },
                        {
                            title: "I AM TRYING TO CREATE A CASE, BUT THE SYSTEM SAYS 'FAILED TO CREATE RECORD'?",
                            paragraph: 'Your case title might be too long. Shorten the name of the Plaintiff and the Defendant.',
                            paragraph1: 'If the problem persists, please contact us via the <strong>chat box</strong> or via email at <a href="mailto:support@esquiretek.com" style="color:#2ca01c">support@esquiretek.com.</a>',
                        },
                        {
                            title: "HOW DO I EDIT THE QUESTIONS?",
                            paragraph: 'You can edit the questions before you start responding and in the downloaded Word document. Unfortunately, you can’t edit the questions once you start responding on the platform.<br></br> To edit the questions before you start responding:',
                            listItems: [
                                `Upload the PDF of the propounded discovery`,
                                `On the uploaded questions page, click ‘Edit’`,
                                `On the next page, you will be able to add questions by clicking the <strong>‘+’</strong> button and delete questions by clicking the <strong>‘trash’</strong> button`,
                                `Click <strong>‘Save’</strong>`
                            ],
                        },
                        {
                            title: "ESQUIRETEK IS NOT DOWNLOADING MY RESPONSES?",
                            paragraph: '<p>There are two download buttons, <strong>‘Create Shell’</strong> and <strong>‘Create Discovery’</strong>.</p><p>Click <strong>‘Create Discovery’</strong> to download the questions AND responses.</p><p>Click <strong>‘Create Shell’</strong> to download shells with questions ONLY.</p><p>If you’ve clicked <strong>‘Create Discovery’</strong> and are still experiencing difficulties, please reach out to us via the <strong>chat box</strong> or via email at <a href="mailto:support@esquiretek.com" style="color:#2ca01c">support@esquiretek.com.</a></p>'
                        },
                        {
                            title: "HOW DO I MODIFY THE OBJECTIONS?",
                            listItems: [
                                `Go to the <strong>‘Objections’</strong> Tab – Only admin accounts have this tab. If you don’t see it in your profile you’ll have to reach out to your admin, this may be the attorney or IT, to modify the objections.`,
                                'Select your state from the drop-down menu',
                                'Click on the objection',
                                'Click <strong>‘Edit’</strong>',
                                'Modify the language',
                                'Click <strong>‘Update’</strong>'
                            ]
                        },
                        {
                            title: "MY DOCUMENT ISN'T WORKING. WHAT DO I DO?",
                            paragraph: 'We are so sorry to hear that! We have an artificial intelligence system that is getting better every day. We cannot promise that all documents work, but as the system sees different variations of documents, it will get better and better. Please email the document that failed to <a href="mailto:support@esquiretek.com" style="color:#2ca01c">support@esquiretek.com</a> and we will have our team work on getting it fixed.'
                        },
                        {
                            title: "MY CASE HAS SETTLED. HOW DO I GET RID OF IT?",
                            listItems: [
                                `In the <strong>‘Cases’</strong> Tab click on the case`,
                                `In the upper right-hand corner click <strong>‘Archive’</strong>`,
                                `Click <strong>‘Yes’</strong>`
                            ],
                        }
                    ]
                },
                {
                    schemaId: 4,
                    heading: "Client Communication Questions",
                    list: [
                        {
                            id: 1,
                            title: "HOW DO I SEND QUESTIONS TO MY CLIENT?",
                            paragraph: 'In the <b>‘Cases’</b> Tab',
                            listItems: [
                                `Click on the case</li><li>Click <strong>‘View Form’</strong>`,
                                `In the top left click <strong>‘Send Questions’</strong>`,
                                `Click <strong>‘Share attorney response draft with client’</strong> (optional)`,
                                `Edit the <strong>‘Message to Client’</strong> (optional)</strong>`,
                                `Click <strong>‘Select Questions to Send’</strong>`,
                                `Check the box(es) of the questions you would like to send`,
                                `In the top left click <strong>‘Send Questions’</strong>`,
                                `Click <strong>‘Send Selected Questions’</strong>`

                            ],
                            paragraph1: 'To view the questions you have sent to the client click the <strong>‘Client Response: All’</strong> drop-down menu and select <strong>‘Questions Sent to Client’.</strong>'
                        },
                        {
                            id: 2,
                            title: "HOW DO I REQUEST DOCUMENTS FROM MY CLIENTS?",
                            paragraph: 'You can request pdf/word/jpg documents from clients when responding to Request for Production of Documents. The steps are as follows:',
                            listItems: [
                                `Click <strong>‘Send Questions’</strong>`,
                                `Click <strong>‘Select Questions to Send’</strong>`,
                                `Select the question`,
                                `Select <strong>‘Request Files from Client’</strong>`,
                                `Click <strong>‘Send Questions’</strong>`,
                                `Click <strong>‘Send Selected Questions’</strong>`
                            ],
                        },
                        {
                            id: 3,
                            title: "HOW DO I SEND THE CLIENT VERIFICATION/SIGNATURE PAGE?", paragraph: 'In the <b>‘Cases’</b> Tab',
                            listItems: [
                                `Click on the case`,
                                `Click <strong>‘View Form’</strong>`,
                                `Respond to discovery`,
                                `Click <strong>‘Save As Final’</strong>`,
                                `Click <strong>‘TekSign’</strong>`,
                                `Click <strong>‘Confirm & Teksign’</strong>`,
                                `Chose the language (optional)`,
                                `Click <strong>‘Yes’</strong>`
                            ],
                        },
                        {
                            id: 4,
                            title: "HOW DOES MY CLIENT SEND RESPONSES?",
                            img: 'https://dpn4uge7raq1v.cloudfront.net/www.esquiretek.com/images/wp-content/uploads/2022/10/Code.webp',
                            paragraph: 'Your client will receive a text message with a questionnaire link and verification code to the mobile number on file. They will receive a questionnaire link in their email, but not the code. The client should:',
                            listItems: [
                                `Click on the link`,
                                `Enter the 4-Digit Verification Code`,
                                `Click ‘Continue’`,
                                `Answer the question`,
                                `Click ‘Save & Continue’`,
                                `Repeat steps 4 and 5 until they get to the last question`,
                                `Click ‘Save & Finish’`
                            ],
                            paragraph1: 'The system will then show them a ‘Thank You’ screen and give them the option to modify their responses.'
                        },
                        {
                            id: 5,
                            title: "MY CLIENT DID NOT GET A CODE", paragraph: 'The verification code will only be sent via text message to the mobile phone number on file. Please double-check that the client is using a mobile phone number and not a VOIP number (i.e. Google Voice/Ring Central). If you’ve double-checked the number and the problem persists, please contact us via the <strong>chat box</strong> or via email at <a href="mailto:support@esquiretek.com" style="color:#2ca01c">support@esquiretek.com.</a>'
                        },
                        {
                            id: 6,
                            title: "THE SYSTEM IS REQUESTING AN 8-DIGIT VERIFICATION CODE", paragraph: 'Please re-send your client the questions they have not responded to so the system can generate a new 4-Digit verification code.'
                        }
                    ]
                },
                {
                    schemaId: 5,
                    heading: "Feature Questions",
                    list: [
                        {
                            title: "WHAT ARE MEDICAL SUMMARIES?", paragraph: 'We have a HIPAA compliant team of 15 lawyers overseas who will take your complex and voluminous medical records and create a chronological summary for you. For those who work in medical malpractice, personal injury, or any type of work that requires reading and organizing medical records, our team of medical record experts can help save you a ton of time.'
                        },
                        {
                            title: "WHAT IS TEKSIGN?", paragraph: 'TekSign is our proprietary feature that allows you to have your clients sign verifications through their cell phone. Your client will receive a secure text message link, and upon entering the portal, they will have a chance to review all questions and answers, sign with their finger and click SEND. Once the client is finished, you will receive a PDF with their verifications to discovery. '
                        }
                    ]
                }
            ],
            helpVideos: [
                { id: 1, url: 'https://www.youtube.com/embed/-C7KnfYrlZk', title: 'ESQUIRETEK EXPLAINER VIDEO' },
                { id: 2, url: 'https://www.youtube.com/embed/YlMXRniK3GQ', title: 'INTRODUCTION TO TEK ACADEMY' },
                // { id: 3, url: 'https://www.youtube.com/embed/gM8zTV_iYcc', title: 'HOW TO CREATE AN ACCOUNT' },
                { id: 3, url: 'https://www.youtube.com/embed/Aaw535z4PH0', title: 'GET FAMILIAR WITH THE SITE' },
                { id: 4, url: 'https://www.youtube.com/embed/KycjUWGI97c', title: 'CLIENTS TAB' },
                { id: 5, url: 'https://www.youtube.com/embed/vu9CCUEn_xw', title: 'CASES TAB' },
                { id: 6, url: 'https://www.youtube.com/embed/K-F3fhJZV7k', title: 'CART OR ORDER HISTORY TAB' },
                // { id: 8, url: 'https://www.youtube.com/embed/VkrR2PYQXQs', title: 'USERS TAB' },
                { id: 7, url: 'https://www.youtube.com/embed/pFDzBRdInDk', title: 'OBJECTIONS TAB' },
                // { id: 10, url: 'https://www.youtube.com/embed/35gOuNVinwM', title: 'SETTINGS TAB' }
            ]
        }
    }

    function upShell(user) {
        let billingType = user?.practiceDetails?.billing_type === 'limited_users_billing';
        return {
            headLine: "Unlock the full power of AI risk-free",
            features: billingType ? [
                "AI Powered Automation",
                "1-Click Objections",
                "Text & Email Tools",
                "Proof of Service",
                "Unlimited Documents",
                "Unlimited TekSign",
                "Complimentary Training",
                "5 User Accounts"
            ] : [
                "AI Powered Automation",
                "1-Click Objections",
                "Text & Email Tools",
                "Proof of Service",
                "Unlimited Documents",
                "Unlimited TekSign",
                "Complimentary Training",
            ],
            freeTrialFeatures: [
                {
                    label: "AI Powered Automation",
                    strike: false,
                },
                {
                    label: "1-Click Objections",
                    strike: false,
                },
                {
                    label: "Text & Email Tools",
                    strike: false,
                },
                {
                    label: "Proof of Service",
                    strike: false,
                },
                {
                    label: "No Activation Fee",
                    strike: false,
                },
                {
                    label: "Unlimited Documents",
                    strike: true,
                },
                {
                    label: "Unlimited TekSign",
                    strike: true,
                },
                {
                    label: "Complimentary Training",
                    strike: true,
                }
            ],
            unlimitedFeatures: billingType ? [
                {
                    label: "AI Powered Automation",
                    strike: false,
                },
                {
                    label: "1-Click Objections",
                    strike: false,
                },
                {
                    label: "Text & Email Tools",
                    strike: false,
                },
                {
                    label: "Proof of Service",
                    strike: false,
                },

                {
                    label: "Unlimited Documents",
                    strike: false,
                },
                {
                    label: "Unlimited TekSign",
                    strike: false,
                },
                {
                    label: "Complimentary Training",
                    strike: false,
                },
                {
                    label: "5 User Accounts",
                    strike: false,
                },
            ] : [
                {
                    label: "AI Powered Automation",
                    strike: false,
                },
                {
                    label: "1-Click Objections",
                    strike: false,
                },
                {
                    label: "Text & Email Tools",
                    strike: false,
                },
                {
                    label: "Proof of Service",
                    strike: false,
                },

                {
                    label: "Unlimited Documents",
                    strike: false,
                },
                {
                    label: "Unlimited TekSign",
                    strike: false,
                },
                {
                    label: "Complimentary Training",
                    strike: false,
                },
            ],
            details: [
                {
                    logo: `logo1`,
                    name: "Peter J. Poles",
                    role: "Director of Litigation",
                    comment: `"EsquireTek is a game changer. &nbsp A must have for a litigation firm looking to be more efficient."`,
                    rating: 'rating',
                    profile: 'profile1'
                },
                {
                    logo: 'logo2',
                    name: "Tabitha Reyes",
                    role: "Senior Paralegal",
                    comment: `"We will NEVER go back to the old school way of doing discovery."`,
                    rating: 'rating',
                    profile: 'profile2'
                },
                {
                    logo: 'logo3',
                    name: "Robert Simon",
                    role: "Co-Founder - Simon Law Group",
                    comment: `"100% best legal tech product of the year. Making discovery fun again. &nbsp AND way less time, and WAY less cost to it."`,
                    rating: 'rating',
                    profile: 'profile3'
                }
            ],
            reviewDetails: [
                {
                    logo: 'review1',
                    rating: 'rating1',
                    comment: `"This software is INCREDIBLE.<br/> The ease of use and time it has saved is unmatched."`,
                    style: { width: '120px' }
                },
                {
                    logo: 'review2',
                    rating: 'rating2',
                    comment: `"Game Changer.<br/> I didn't know software like this existed. I'm grateful I do now."`,
                    style: { width: '140px' }
                },
                {
                    logo: 'review3',
                    rating: 'rating3',
                    comment: `"Love it! Get it! Use it! <br/>Truly a wonderful tool."`,
                    style: { width: '65px' }
                },
            ],
            guaranteeText: `30-Day Money Back Guarantee`,
            description: `Get the most out of EsquireTek with a paid plan - risk free!<br /> If you don't love it, contact us within 30 days and we'll issue a full refund.`,
            reviewTitle: `Reviews From Real Users`,

        }
    }

    function webAuthentication() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'password',
                    mandatory: true
                },
            ]
        }
    }

    function userMenuSettings() {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'Change Password',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'changePassword',
                    message: 'Password must be changed every 90 days',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    confirmButton: false,
                    enableSubmitBtn: true,
                    disableMessage: '',
                    title: 'Change Password',
                    columns: [
                        {
                            id: 1,
                            value: 'password',
                            label: 'Password',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: true
                        },
                        {
                            id: 2,
                            value: 'new_password',
                            label: 'Confirm Password',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: true
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Two Factor Authentication',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'enabledTwoFactorAuthentication',
                    message: 'Two Factor Authentication provides additional security. While logging in, a code will be sent to your email to complete login.',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    enableSubmitBtn: true,
                    confirmButton: true,
                    disableCancelBtn: true,
                    confirmMessage: (user) => `This will require you to access your email (${user.email}) in order to login in future. Do you want to make this change?`,
                    disableMessage: 'Are you sure you want to disable two factor authentication?',
                    title: '',
                    columns: [
                        {
                            id: 1,
                            value: 'twofactor',
                            label: 'Enable/Disable Two Factor Authentication',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'switch',
                            // mandatory: true
                        },
                    ]
                },
                {
                    schemaId: 3,
                    name: 'Switch Practice',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'switchPractice',
                    // message: 'Switch Practice',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    enableSubmitBtn: true,
                    confirmButton: true,
                    confirmMessage: (user, formValue, metaData) => {
                        const { practicesData = [] } = metaData;
                        const currentPracticeData = (practicesData && practicesData.length && formValue && formValue?.currentPractice) ? practicesData.filter((val) => val.value === formValue.currentPractice) : [];

                        const currentPracticeName = (Array.isArray(currentPracticeData) && currentPracticeData.length) ? currentPracticeData[0]["label"] : "";
                        return `Do you want to switch ${currentPracticeName}?`;
                    },
                    disableMessage: '',
                    title: 'Switch Practice',
                    columns: [
                        {
                            id: 1,
                            value: 'currentPractice',
                            label: 'Switch Practice',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'select',
                            mandatory: true,
                            options : 'practicesData'
                        },
                    ]
                }
            ]
        }
    }

    function medicalExportSettings() {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'AI Prompt',
                    label: 'OTHER SETTINGS',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'pricing',
                    filter: (record) => record,
                    confirmMessage: "Do you want to save this change?",
                    columns: [
                        {
                            id: 1,
                            value: 'summary_question',
                            label: 'Doc Splitter Prompt',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'textarea'
                        },
                        {
                            id: 2,
                            value: 'summary_final_question',
                            label: 'Doc Splitter Final Prompt',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'textarea'
                        },
                        {
                            id: 3,
                            value: 'summary_template',
                            label: 'Generation Prompt',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: true,
                            type: 'textarea'
                        },
                        {
                            id: 4,
                            value: 'headings_to_add',
                            label: 'Headings',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            mandatory: true,
                            html: (row) => {
                                return row && row['headings_to_add'];
                            },
                            type: 'textarea'
                        }
                    ]
                }
            ]
        }
    }

    function deleteSupportRequest() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'alert_text',
                    label: ``,
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'component',
                    render: () => {
                        return <span style={{fontSize : '16px'}} dangerouslySetInnerHTML={{ __html: `Do you want to delete this request? Please select the reason` }} />
                    }
                },
                {
                    id: 2,
                    value: 'deleteReason',
                    label: 'Select Reason',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: [
                        {
                            label: 'Duplicate Record',
                            value: 'Duplicate',
                        },
                        {
                            label: 'No Questions Found',
                            value: 'NoQuestions',
                        }
                    ],
                    style : {paddingTop : '0px'},
                    render: ({ input, options, meta: { touched, error, warning } }) => {
                        let inputArr = input.value && typeof input.value === 'string' && input.value || '';
                        const handleChange = (event) => {
                            if (event.target.value && event.target.checked) {
                                input.onChange(event.target.value)
                            } else if (!event.target.checked) {
                                input.onChange('')
                            }
                        };

                        return (<div style={{ display: 'flex' }}>
                            {options.map((opt, index) => (!opt.disableRecord &&
                                <Grid item xs={5}>
                                    <FormControlLabel
                                        control={<Radio
                                            key={index}
                                            style={{ color: "#2ca01c" }}
                                            value={opt.value}
                                            checked={inputArr.includes(opt.value) || false}
                                            onChange={(e) => handleChange(e, opt)} />}
                                        label={<span style={{ fontSize: '15px' }} dangerouslySetInnerHTML={{ __html: opt.label }} />} />
                                </Grid>
                            ))}
                        </div>)
                    },
                    type: 'component'
                }
            ]
        }
    }

    function missedQuestionsFormField() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'fileData',
                    label: 'file',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf',
                    type: 'upload'
                },
                {
                    id: 2,
                    value: 'questions',
                    label: 'Questions JSON',
                    placeholder: 'Paste a JSON here.',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'textarea'
                }
            ]
        }
    }

    function deleteQuestionsWithHashForm() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'fileData',
                    label: 'file',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: 'application/pdf',
                    type: 'upload'
                }
            ]
        }
    }

    function quickCreateDescriptionModal() {
        return {
            id: 1,
            editRecord: true,
            viewRecord: false,
            viewMode: false,
            visible: false,
            type: 'component',
            mandatory: true,
            edit: true,
            description: ({ actions, dispatch, classes }) => {
                const [checked, isChecked] = useState(false);
                return (<Grid container>
                    <Grid item xs={12} style={{ paddingTop: '20px' }}>
                        <Typography>California Form Interrogatories are not supported via Quick Create. You must create a new client or a new case to upload Form Interrogatories. Thank you for your understanding.</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.checkBoxLable} style={{ paddingTop: '10px' }}>
                        <FormControlLabel
                            control={<Checkbox
                                key={"description"}
                                style={{
                                    color: "#2ca01c",
                                }}
                                value={'show_again'}
                                checked={checked}
                                onChange={(e) => {
                                    isChecked(e.target.checked);
                                    dispatch(actions.showQuickCreateDescription(e.target.checked));
                                }} />}
                            label={'Do not show this message again'} />
                    </Grid>
                </Grid>)
            }
        }
    }

    function emailSentHistoryModal() {
        return {
            id: 1,
            editRecord: true,
            viewRecord: false,
            viewMode: false,
            visible: false,
            type: 'component',
            mandatory: true,
            edit: true,
            description: ({ mailHistoryData }) => {
                const {emails = [], cc_email} = mailHistoryData;
                let mailList;
                if(cc_email){
                    const cc_emailId = `${cc_email}(cc)`;
                    mailList = [cc_emailId, ...emails];
                } else {
                    mailList = [...emails];
                }               
                return (<Grid container style={{height : 'auto'}}>
                    <Grid container id={'mail-list'}> 
                        <Grid item sm={12} md={12} style={{  paddingTop: '0px', paddingBottom: '0px' }}>
                            <Divider />
                            <List dense={true}>
                                {mailList.length && mailList.map((name) => {
                                    return <ListItem style={{ paddingTop: '0px', paddingBottom: '0px' }}>
                                        {/* <Typography style={{fontSize : '15px'}}>{name}</Typography> */}
                                        <AlternateEmailIcon style={{color : 'green'}}/>
                                        <ListItemText
                                            style={{paddingLeft: '5px'}}
                                            primary={name}
                                            secondary={''}
                                        /> 
                                       
                                    </ListItem>
                                })}
                            </List>
                        </Grid>
                    </Grid>             
                </Grid>)
            }
        }
    }

    function adminPropoundCustomTemplate() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'practice_name',
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'file_name',
                    label: 'FILE NAME',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'file_name',
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'state',
                    label: 'STATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'state',
                    html: (row) => row['state'] && row['state'] !== 'FEDERAL' ? (row['state'] == 'PL' || row['state'] == 'NPL') ? `Other (${row['state']})` : row['state'] : '-',
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'createdAt',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'createdAt',
                    html: (row) => row && row['createdAt'] && moment(row['createdAt']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date',
                },
                {
                    id: 5,
                    value: 'custom_template',
                    label: 'CUSTOM TEMPLATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input',
                    html: (row) => {
                        const custom = row && row['custom_template'];
                        return custom ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(custom)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 6,
                    value: 'modified_template_frogs',
                    label: 'MODIFIED FROGS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedFrogs = row && row['modified_template_frogs'];
                        return modifiedFrogs ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedFrogs)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 7,
                    value: 'modified_template_sprogs',
                    label: 'MODIFIED SPROGS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedSprogs = row && row['modified_template_sprogs'];
                        return modifiedSprogs ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedSprogs)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 8,
                    value: 'modified_template_rfpd',
                    label: 'MODIFIED RFPD',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedRfpd = row && row['modified_template_rfpd'];
                        return modifiedRfpd ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedRfpd)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 9,
                    value: 'modified_template_rfa',
                    label: 'MODIFIED RFA',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row) => {
                        const modifiedRfa = row && row['modified_template_rfa'];
                        return modifiedRfa ? <SVG src={require('images/icons/attach.svg')} style={{ width: '24px', height: 'auto', cursor: 'pointer' }} onClick={async () => await viewPDF(modifiedRfa)} /> : <p style={{ paddingLeft: '25px', fontWeight: 'bold', fontSize: '14px' }}>{'-'}</p>
                    }
                },
                {
                    id: 10,
                    value: 'modified_template',
                    label: 'MODIFIED TEMPLATE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    contentType: '.doc, .docx',
                    type: 'upload'
                },
                {
                    id: 11,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'status',
                    options: [
                        {
                            value: 'Processing',
                            label: 'Processing'
                        },
                        {
                            value: 'Applied',
                            label: 'Applied'
                        }
                    ],
                    type: 'select'
                },
                {
                    id: 12,
                    value: 'document_type',
                    label: 'DOCUMENT TYPE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    options: [
                        {
                            value: 'FROGS',
                            label: 'FROGS'
                        },
                        {
                            value: 'SPROGS',
                            label: 'SPROGS'
                        },
                        {
                            value: 'RFPD',
                            label: 'RFPD'
                        },
                        {
                            value: 'RFA',
                            label: 'RFA'
                        },
                    ],
                    type: 'select'
                }
            ]
        }
    }


    return {
        clients,
        cases,
        orders,
        adminOrders,
        filterColumns,
        legalForms,
        users,
        adminUsers,
        practices,
        settings,
        globalSettings,
        changePassword,
        adminObjections,
        objections,
        adminMedicalHistory,
        medicalHistory,
        questionForm,
        supportRequest,
        questionsTranslations,
        login,
        plans,
        parties,
        subscriptionsHistory,
        adminSubscriptionsHistory,
        userSettings,
        standardEditForm,
        customTemplate,
        federalObjections,
        adminFederalObjections,
        POS,
        state,
        formOtp,
        otherPartiesFormOtp,
        adminEsquiretekUsers,
        adminPracticeUsers,
        opposingCounselForm,
        archiveCases,
        feeWaiver,
        excelExport,
        globalOtherSettings,
        propoundUploadTemplate,
        propound,
        viewCases,
        signature,
        propoundPOS,
        propoundEmailDocument,
        propoundSupportRequest,
        respondCustomTemplate,
        propoundCustomTemplateSignature,
        discountCode,
        filevineSecret,
        filevineList,
        clientWithCase,
        myCaseListView,
        subscriptionDetails,
        customTemplateSignature,
        respondEmailDocument,
        propoundCustomTemplate,
        cancelSubcriptionsOptions,
        subscriptionRenewalMonthly,
        subscriptionRenewalYearly,
        discountCodeHistory,
        selectQuestionTemplate,
        customDateFilter,
        activeFilter,
        authLogin,
        createDocument,
        userRecordForm,
        extractionSupportRequest,
        clientCaseInfo,
        quickCreateDocumentInfo,
        quickCreateClientCase,
        litifyListView,
        helpFAQs,
        upShell,
        webAuthentication,
        userMenuSettings,
        medicalExportSettings,
        deleteSupportRequest,
        missedQuestionsFormField,
        deleteQuestionsWithHashForm,
        quickCreateDescriptionModal,
        emailSentHistoryModal,
        adminPropoundCustomTemplate
    }
}
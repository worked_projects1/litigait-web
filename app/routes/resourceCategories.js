/**
 * 
 * Resource Categories
 * 
 */

/**
 * 
 * @param {string} name 
 * @param {string} path 
 * @param {string} title 
 * @param {string} icon 
 * @param {Array} users 
 * @param {function} simpleLazyLoadedRoute 
 * @param {Array} childRoutes 
 * @returns 
 */

const ResourceCategories = (name, path, title, icon, users, id, simpleLazyLoadedRoute, childRoutes) => {
    return simpleLazyLoadedRoute({
        path,
        name,
        require: ['CategoriesPage', name],
        id,
        data: {
            path: `/${path}`,
            title,
            users,
            icon
        },
        container: function CategoriesPage() {
            return this(name, `${process.env.PUBLIC_PATH || ''}/${path}`, childRoutes)
        },
        childRoutes
    })
}

export default ResourceCategories;
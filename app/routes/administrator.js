
/**
 * 
 * Administrator
 * 
 */

import schema from './schema';

import ResetLoginAttempts from 'components/ResetLoginAttempts';
import CancelSubscription  from 'components/CancelSubscription';
import CreatePracticeUserBtn  from 'components/CreatePracticeUserBtn';
import CustomCreateUserBtn from 'components/CustomCreateUserBtn';
import ResourceCategories from './resourceCategories';
import { selectUser, selectSessionExpand, selectError as sessionError, selectSuccess as sessionSuccess } from 'blocks/session/selectors';
import { updatePracticeObjections, clearCache } from 'blocks/session/actions';


const filterColumns = schema().filterColumns();
const ordersColumns = schema().adminOrders().columns;
const medicalHistoryColumns = schema().adminMedicalHistory().columns;
const objectionColumns = schema().adminObjections().columns;
const practicesColumns = schema().practices().columns;
const practicesSettingsColumns = schema().practices().settings;
const globalSettingsColumns = schema().globalSettings;
const medicalDocumentsColumns = schema().medicalHistory().medicalDocuments;
const uploadSummaryColumns = schema().medicalHistory().uploadSummary;
const statusFormColumns = schema().medicalHistory().statusForm;
const supportRequestColumns = schema().supportRequest().columns;
const questionFormColumns = schema().questionForm().columns;
const bulkQuestionsFormColumns = schema().questionForm().bulkQuestionsForm;
const questionsTranslationsColumns = schema().questionsTranslations().columns;
const subscriptionsPlansColumns = schema().plans().columns;
const subscriptionsPlansView = schema().plans().decoratorView;
const subscriptionsHistoryColumns = schema().adminSubscriptionsHistory().columns;
const customTemplateColumns = schema().customTemplate().columns;
const propoundCustomTemplateColumns = schema().adminPropoundCustomTemplate().columns;
const federalObjectionsColumns = schema().adminFederalObjections().columns;
const stateColumns = schema().state().columns;
const formOtpColumns = schema().formOtp().columns;
const otherPartiesFormOtpColumns = schema().otherPartiesFormOtp().columns;
const adminEsquiretekUsersColumns = schema().adminEsquiretekUsers().columns;
const adminPracticeUsersColumns = schema().adminPracticeUsers().columns;
const adminPracticeConfirmationMessage = schema().adminPracticeUsers().customDescription;
const excelExportColumns = schema().excelExport().columns;
const globalOtherSettingsColumns = schema().globalOtherSettings;
const propoundSupportRequestColumns = schema().propoundSupportRequest().columns;
const subscriptionRenewalYearlyColumns = schema().subscriptionRenewalYearly().columns;
const subscriptionRenewalMonthlyColumns = schema().subscriptionRenewalMonthly().columns;
const discountCodeHistoryColumns = schema().discountCodeHistory().columns;
const extractionSupportRequestColumn = schema().extractionSupportRequest().columns;
const deleteSupportRequestColumn = schema().deleteSupportRequest().columns;
const clientCaseInfoFields = schema().clientCaseInfo;
const missedQuestionsFormField = schema().missedQuestionsFormField().columns;
const deleteQuestionsWithHashFormField = schema().deleteQuestionsWithHashForm().columns;

const devUsers = ['m.shahidulkareem@rifluxyss.com', 'siva@miotiv.com','admin@test.com'];

export default function (simpleLazyLoadedRoute) {

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Home',
                path: '/',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'dashboard',
            pageName: 'analytics',
            exact: true,
            id: 'dashboard',
            data: {
                title: 'Dashboard',
                path: '/dashboard',
                icon: 'Dashboard',
                filterColumns: filterColumns
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `practices`,
            name: 'practices',
            pageName: 'practices',
            require: ['RecordsPage', 'practices'],
            chunk: ['practices'],
            id: 'Practices',
            data: {
                path: '/practices',
                title: 'Practices',
                icon: 'Practice'
            },
            container: function RecordsPage(practices) {
                return this('practices', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors, filterColumns, true, true, false, true, false, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'practices.create',
                    pageName: 'practices',
                    require: ['CreateRecordPage', 'practices'],
                    container: function CreateRecordPage(practices) {
                        return this('practices.create', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'practices.edit',
                    pageName: 'practices',
                    require: ['EditRecordPage', 'practices'],
                    container: function EditRecordPage(practices) {
                        return this('practices.edit', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/settings`,
                    name: 'practices.settings',
                    pageName: 'practices',
                    require: ['ViewPracticeSettingsPage', 'practices'],
                    chunk: ['practices'],
                    container: function ViewPracticeSettingsPage(practices) {
                        return this('practices.settings', `${process.env.PUBLIC_PATH || ''}/practices`, practicesSettingsColumns, practices.actions, practices.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'practices.view',
                    pageName: 'practices',
                    require: ['ViewPracticePage', 'practices'],
                    chunk: ['practices'],
                    container: function ViewPracticePage(practices) {
                        return this('practices.view', `${process.env.PUBLIC_PATH || ''}/practices`, schema().practices, practices.actions, practices.selectors, CancelSubscription)
                    }
                })
            ]
        }),
        ResourceCategories('orderCategories', 'orderCategories', 'Orders', 'Orders', false, 'AdminOrders', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/admin/orders`,
                name: 'orders',
                pageName: 'orders',
                require: ['RecordsPage', 'rest/admin/orders'],
                data: {
                    path: '/rest/admin/orders',
                    title: 'Orders'
                },
                container: function RecordsPage(orders) {
                    return this('adminOrders', `${process.env.PUBLIC_PATH || ''}/orderCategories/rest/admin/orders`, ordersColumns, orders.actions, orders.selectors, filterColumns, false, false, false, true)
                }
            }),
            simpleLazyLoadedRoute({
                path: `rest/subscriptions/admin/history`,
                name: 'subscriptions',
                pageName: 'subscriptions',
                require: ['RecordsPage', 'rest/subscriptions/admin/history'],
                data: {
                    path: 'rest/subscriptions/admin/history',
                    title: 'Subscriptions'
                },
                container: function RecordsPage(subscriptions) {
                    return this('subscriptions', `${process.env.PUBLIC_PATH || ''}/orderCategories/rest/subscriptions/admin/history`, subscriptionsHistoryColumns, subscriptions.actions, subscriptions.selectors, filterColumns, false, false, false, true, false, true)
                }
            })
        ]),
        ResourceCategories('userCategories', 'userCategories', 'Users', 'Users', false, 'AdminUsers', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/esquiretek/users`,
                name: 'esquireTekUsers',
                pageName: 'esquireTekUsers',
                queryParams: 'admin',
                require: ['RecordsPage', 'rest/esquiretek/users'],
                chunk: ['users'],
                data: {
                    path: '/rest/esquiretek/users',
                    title: 'EsquireTek Users',
                    icon: 'Users'
                },
                container: function RecordsPage(esquireTekUsers) {
                    return this('esquireTekUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, adminEsquiretekUsersColumns, esquireTekUsers.actions, esquireTekUsers.selectors, filterColumns, true, true, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'esquireTekUsers.create',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['CreateRecordPage', 'rest/esquiretek/users'],
                        container: function CreateRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, schema().adminEsquiretekUsers, esquireTekUsers.actions, esquireTekUsers.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'esquireTekUsers.edit',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['EditRecordPage', 'rest/esquiretek/users'],
                        container: function EditRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, schema().adminEsquiretekUsers, esquireTekUsers.actions, esquireTekUsers.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'esquireTekUsers.view',
                        pageName: 'esquireTekUsers',
                        queryParams: 'admin',
                        require: ['ViewRecordPage', 'rest/esquiretek/users'],
                        chunk: ['users'],
                        container: function ViewRecordPage(esquireTekUsers) {
                            return this('esquireTekUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/esquiretek/users`, adminEsquiretekUsersColumns, esquireTekUsers.actions, esquireTekUsers.selectors, ResetLoginAttempts)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/practice/users`,
                name: 'practiceUsers',
                pageName: 'practiceUsers',
                queryParams: 'practice',
                require: ['RecordsPage', 'rest/practice/users'],
                chunk: ['users'],
                data: {
                    path: '/rest/practice/users',
                    title: 'Practice Users',
                    icon: 'Users'
                },
                container: function RecordsPage(practiceUsers) {
                    return this('practiceUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, filterColumns, true, true, false, true, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'practiceUsers.create',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['CreateRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function CreateRecordPage(practiceUsers) {
                            return this('practiceUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors, false, false, false, CustomCreateUserBtn, adminPracticeConfirmationMessage, false, false, CreatePracticeUserBtn)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'practiceUsers.edit',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['EditRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function EditRecordPage(practiceUsers) {
                            return this('practiceUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors, false, CreatePracticeUserBtn)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'practiceUsers.view',
                        pageName: 'practiceUsers',
                        queryParams: 'practice',
                        require: ['ViewRecordPage', 'rest/practice/users'],
                        chunk: ['users'],
                        container: function ViewRecordPage(practiceUsers) {
                            return this('practiceUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, ResetLoginAttempts)
                        }
                    })
                ]
            })
        ]),
        simpleLazyLoadedRoute({
            path: `rest/medical-history-summery`,
            name: 'medicalHistory',
            pageName: 'medicalHistory',
            require: ['MedicalPage', 'rest/medical-history-summery'],
            chunk: ['medicalHistory'],
            id: 'MedicalHistory',
            data: {
                path: '/rest/medical-history-summery',
                title: 'Medical History',
                icon: 'MedicalHistory'
            },
            container: function MedicalPage(medicalHistory) {
                return this('medicalHistory', `${process.env.PUBLIC_PATH || ''}/rest/medical-history-summery`, Object.assign({}, { medicalHistoryColumns, excelExportColumns }), medicalHistory.actions, medicalHistory.selectors, filterColumns, true, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:id/form`,
                    name: 'medicalHistory.edit',
                    pageName: 'medicalHistory',
                    require: ['ViewMedicalHistoryPage', 'rest/medical-history-summery'],
                    chunk: ['medicalHistory'],
                    data: {
                        path: '/rest/medical-history-summery',
                        title: 'Medical History'
                    },
                    container: function ViewMedicalHistoryPage(medicalHistory) {
                        return this('cases.detail', `${process.env.PUBLIC_PATH || ''}/rest/medical-history-summery`, Object.assign({}, { medicalDocumentsColumns, uploadSummaryColumns, statusFormColumns }), medicalHistory.actions, Object.assign({}, medicalHistory.selectors, { selectSessionExpand }), filterColumns)
                    }
                })
            ]
        }),
        ResourceCategories('customCategories', 'customCategories', 'Custom Templates', 'CustomTemplate', false, 'AdminCustomTemplate', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/practice-template`,
                name: 'respondingCustomTemplate',
                pageName: 'respondingCustomTemplate',
                queryParams: 'responding',
                require: ['RecordsPage', 'rest/practice-template'],
                chunk: ['customTemplate'],
                data: {
                    path: '/rest/practice-template',
                    title: 'Responding',
                    icon: 'CustomTemplate'
                },
                container: function RecordsPage(customTemplate) {
                    return this('respondingCustomTemplate', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, customTemplateColumns, customTemplate.actions, customTemplate.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'respondingCustomTemplate.edit',
                        pageName: 'respondingCustomTemplate',
                        queryParams: 'responding',
                        require: ['EditRecordPage', 'rest/practice-template'],   
                        chunk: ['customTemplate'],                 
                        container: function EditRecordPage(customTemplate) {
                            return this('respondingCustomTemplate.edit', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, schema().customTemplate, customTemplate.actions, customTemplate.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'respondingCustomTemplate.view',
                        pageName: 'respondingCustomTemplate',
                        queryParams: 'responding',
                        require: ['ViewRecordPage', 'rest/practice-template'],
                        container: function ViewRecordPage(customTemplate) {
                            return this('respondingCustomTemplate.view', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, customTemplateColumns, customTemplate.actions, customTemplate.selectors)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/practice-propound-template`,
                name: 'propoundingCustomTemplate',
                pageName: 'propoundingCustomTemplate',
                queryParams: 'propounding',
                require: ['RecordsPage', 'rest/practice-propound-template'],
                chunk: ['customTemplate'],
                data: {
                    path: '/rest/practice-propound-template',
                    title: 'Propounding',
                    icon: 'CustomTemplate'
                },
                container: function RecordsPage(customTemplate) {
                    return this('propoundingCustomTemplate', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, propoundCustomTemplateColumns, customTemplate.actions, customTemplate.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'propoundingCustomTemplate.edit',
                        pageName: 'propoundingCustomTemplate',
                        queryParams: 'propounding',
                        require: ['EditRecordPage', 'rest/practice-propound-template'],   
                        chunk: ['customTemplate'],                 
                        container: function EditRecordPage(customTemplate) {
                            return this('propoundingCustomTemplate.edit', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, schema().customTemplate, customTemplate.actions, customTemplate.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'propoundingCustomTemplate.view',
                        pageName: 'propoundingCustomTemplate',
                        queryParams: 'propounding',
                        require: ['ViewRecordPage', 'rest/practice-propound-template'],
                        container: function ViewRecordPage(customTemplate) {
                            return this('propoundingCustomTemplate.view', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, propoundCustomTemplateColumns, customTemplate.actions, customTemplate.selectors)
                        }
                    })
                ]
            })
        ]),
        // simpleLazyLoadedRoute({
        //     path: `rest/practice-template`,
        //     name: 'customTemplate',
        //     pageName: 'customTemplate',
        //     require: ['RecordsPage', 'rest/practice-template'],
        //     chunk: ['customTemplate'],
        //     data: {
        //         path: '/rest/practice-template',
        //         title: 'Custom Template',
        //         icon: 'CustomTemplate'
        //     },
        //     container: function RecordsPage(customTemplate) {
        //         return this('customTemplate', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, customTemplateColumns, customTemplate.actions, customTemplate.selectors, filterColumns, false, true)
        //     },
        //     childRoutes: [
        //         simpleLazyLoadedRoute({
        //             path: `:id/edit`,
        //             name: 'customTemplate.edit',
        //             pageName: 'customTemplate',
        //             require: ['EditRecordPage', 'rest/practice-template'],   
        //             chunk: ['customTemplate'],                 
        //             container: function EditRecordPage(customTemplate) {
        //                 return this('customTemplate.edit', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, schema().customTemplate, customTemplate.actions, customTemplate.selectors)
        //             }
        //         }),
        //         simpleLazyLoadedRoute({
        //             path: `:id`,
        //             name: 'customTemplate.view',
        //             pageName: 'customTemplate',
        //             require: ['ViewRecordPage', 'rest/practice-template'],
        //             container: function ViewRecordPage(customTemplate) {
        //                 return this('customTemplate.view', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, customTemplateColumns, customTemplate.actions, customTemplate.selectors)
        //             }
        //         })
        //     ]
        // }),
        // simpleLazyLoadedRoute({
        //     path: `rest/help-request`,
        //     name: 'supportRequest',
        //     pageName: 'supportRequest',
        //     require: ['SupportRequestPage', 'rest/help-request'],
        //     chunk: ['cases'],
        //     data: {
        //         path: '/rest/help-request',
        //         title: 'Support Request',
        //         icon: 'SupportRequest'
        //     },
        //     container: function SupportRequestPage(supportRequest) {
        //         return this('supportRequest', `${process.env.PUBLIC_PATH || ''}/rest/help-request`, supportRequestColumns, supportRequest.actions, supportRequest.selectors, filterColumns, false, true)
        //     },
        //     childRoutes: [
        //         simpleLazyLoadedRoute({
        //             path: `:id`,
        //             name: 'supportRequest.view',
        //             pageName: 'supportRequest',
        //             require: ['ViewSupportRequestPage', 'rest/help-request'],
        //             chunk: ['cases'],
        //             container: function ViewSupportRequestPage(supportRequest) {
        //                 return this('supportRequest.view', `${process.env.PUBLIC_PATH || ''}/rest/help-request`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns }), supportRequest.actions, Object.assign({}, { ...supportRequest.selectors }, { selectUser, selectSessionExpand }), false)
        //             }
        //         })
        //     ]
        // }),
        ResourceCategories('supportCategories', 'supportCategories', 'Support Request', 'SupportRequest', false, 'SupportRequest', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/help-request`,
                name: 'supportRequest',
                pageName: 'supportRequest',
                require: ['SupportRequestPage', 'rest/help-request'],
                chunk: ['cases'],
                data: {
                    path: '/rest/help-request',
                    title: 'Responding',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(supportRequest) {
                    return this('supportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/help-request`, supportRequestColumns, supportRequest.actions, supportRequest.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'supportRequest.view',
                        pageName: 'supportRequest',
                        require: ['ViewSupportRequestPage', 'rest/help-request'],
                        chunk: ['cases'],
                        container: function ViewSupportRequestPage(supportRequest) {
                            return this('supportRequest.view', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/help-request`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns, clientCaseInfoFields, deleteSupportRequestColumn }), supportRequest.actions, Object.assign({}, { ...supportRequest.selectors }, { selectUser, selectSessionExpand }), 'respond')
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/propound-support`,
                name: 'propoundSupportRequest',
                pageName: 'propoundSupportRequest',
                require: ['SupportRequestPage', 'rest/propound-support'],
                chunk: ['propound'],
                data: {
                    path: '/rest/propound-support',
                    title: 'Propounding',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(propoundSupportRequest) {
                    return this('propoundSupportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/propound-support`, propoundSupportRequestColumns, propoundSupportRequest.actions, propoundSupportRequest.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'propoundSupportRequest.view',
                        pageName: 'propoundSupportRequest',
                        require: ['ViewSupportRequestPage', 'rest/propound-support'],
                        chunk: ['propound'],
                        container: function ViewSupportRequestPage(propoundSupportRequest) {
                            return this('propoundSupportRequest.view', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/propound-support`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns, clientCaseInfoFields, deleteSupportRequestColumn }), propoundSupportRequest.actions, Object.assign({}, { ...propoundSupportRequest.selectors }, { selectUser, selectSessionExpand }), 'propound')
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: 'rest/document-extraction-support',
                name: 'extractionSupportRequest',
                pageName: 'extractionSupportRequest',
                require: ['SupportRequestPage', 'rest/document-extraction-support'],
                chunk: ['quickCreate'],
                data: {
                    path: '/rest/document-extraction-support',
                    title: 'Quick Create',
                    icon: 'SupportRequest'
                },
                container: function SupportRequestPage(extractionSupportRequest) {
                    return this('extractionSupportRequest', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/document-extraction-support`, extractionSupportRequestColumn, extractionSupportRequest.actions, extractionSupportRequest.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'extractionSupportRequest.view',
                        pageName: 'extractionSupportRequest',
                        require: ['ViewSupportRequestPage', 'rest/document-extraction-support'],
                        chunk: ['quickCreate'],
                        container: function ViewSupportRequestPage(extractionSupportRequest) {
                            return this('extractionSupportRequest.view', `${process.env.PUBLIC_PATH || ''}/supportCategories/rest/document-extraction-support`, Object.assign({}, { questionColumns: questionFormColumns, bulkQuestionsColumns: bulkQuestionsFormColumns, clientCaseInfoFields, deleteSupportRequestColumn }), extractionSupportRequest.actions, Object.assign({}, { ...extractionSupportRequest.selectors }, { selectUser, selectSessionExpand }), 'extraction')
                        }
                    })
                ]
            })
        ]),
        simpleLazyLoadedRoute({
            path: `rest/practice-missed-questions-file`,
            name: 'missedQuestionFile',
            pageName: 'missedQuestionFile',
            require: ['MissedQuestionsSupportPage', 'rest/practice-missed-questions-file'],
            chunk: ['cases'],
            id: 'MissedQuestionFile',
            data: {
                path: '/rest/practice-missed-questions-file',
                title: 'Cache Files',
                icon: 'HashFile'
            },
            container: function MissedQuestionsSupportPage(missedQuestionFile) {
                return this('missedQuestionFile', `${process.env.PUBLIC_PATH || ''}/rest/questions-file`, Object.assign({}, { missedQuestionsFormField, deleteQuestionsWithHashFormField }), missedQuestionFile.actions, missedQuestionFile.selectors)
            },
            
        }),
        ResourceCategories('objectionsCategories', 'objectionsCategories', 'Objections', 'Objections', false, 'AdminObjection', simpleLazyLoadedRoute, [
        simpleLazyLoadedRoute({
            path: `rest/admin-objections`,
            name: 'objections',
            pageName: 'objections',
            require: ['ObjectionsPage', 'rest/admin-objections'],
            data: {
                path: '/rest/admin-objections',
                title: 'State',
                icon: 'Objections'
            },
            container: function ObjectionsPage(objections) {
                return this('objections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/admin-objections`, objectionColumns, objections.actions, Object.assign({}, objections.selectors, { selectUser, sessionError, sessionSuccess }), filterColumns, true, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'objections.create',
                    pageName: 'objections',
                    require: ['CreateRecordPage', 'rest/admin-objections'],
                    container: function CreateRecordPage(objections) {
                        return this('objections.create', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/admin-objections`, schema().adminObjections, objections.actions, objections.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'objections.edit',
                    pageName: 'objections',
                    require: ['EditRecordPage', 'rest/admin-objections'],
                    container: function EditRecordPage(objections) {
                        return this('objections.edit', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/admin-objections`, schema().adminObjections, objections.actions, objections.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'objections.view',
                    pageName: 'objections',
                    require: ['ViewRecordPage', 'rest/admin-objections'],
                    container: function ViewRecordPage(objections) {
                        return this('objections.view', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/admin-objections`, objectionColumns, objections.actions, objections.selectors)
                    }
                })
            ]
        }),
        simpleLazyLoadedRoute({
            path: `federal/admin-objections`,
            name: 'federalObjections',
            pageName: 'federalObjections',
            require: ['ObjectionsFederalPage', 'federal/admin-objections'],
            data: {
                path: '/federal/admin-objections',
                title: 'Federal',
                icon: 'Objections'
            },
            container: function ObjectionsFederalPage(objections) {
                return this('federalObjections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/admin-objections`, federalObjectionsColumns, Object.assign({}, objections.actions, { updatePracticeObjections, clearCache }), Object.assign({}, objections.selectors, { selectUser, sessionError, sessionSuccess }), filterColumns, true, true, false)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'federalObjections.create',
                    pageName: 'federalObjections',
                    require: ['CreateRecordPage', 'federal/admin-objections'],
                    container: function CreateRecordPage(objections) {
                        return this('federalObjections.create', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/admin-objections`, schema().adminFederalObjections, objections.actions, objections.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'federalObjections.edit',
                    pageName: 'federalObjections',
                    require: ['EditRecordPage', 'federal/admin-objections'],
                    container: function EditRecordPage(objections) {
                        return this('federalObjections.edit', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/admin-objections`, schema().adminFederalObjections, objections.actions, objections.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'federalObjections.view',
                    pageName: 'federalObjections',
                    require: ['ViewRecordPage', 'federal/admin-objections'],
                    container: function ViewRecordPage(objections) {
                        return this('federalObjections.view', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/admin-objections`, federalObjectionsColumns, objections.actions, objections.selectors)
                    }
                })
            ]
        })
        ]),
        // simpleLazyLoadedRoute({
        //     path: `rest/questions-translations`,
        //     name: 'questionsTranslations',
        //     pageName: 'questionsTranslations',
        //     require: ['RecordsPage', 'rest/questions-translations'],
        //     chunk: ['translations'],
        //     data: {
        //         path: '/rest/questions-translations',
        //         title: 'Questions Translations',
        //         users: ['m.shahidulkareem@rifluxyss.com', 'siva@miotiv.com','admin@test.com'],
        //         icon: 'Language'
        //     },
        //     container: function RecordsPage(questionsTranslations) {
        //         return this('questionsTranslations', `${process.env.PUBLIC_PATH || ''}/rest/questions-translations`, questionsTranslationsColumns, questionsTranslations.actions, questionsTranslations.selectors, filterColumns, true, true)
        //     },
        //     childRoutes: [
        //         simpleLazyLoadedRoute({
        //             path: `create`,
        //             name: 'questionsTranslations.create',
        //             pageName: 'questionsTranslations',
        //             require: ['CreateRecordPage', 'rest/questions-translations'],
        //             container: function CreateRecordPage(questionsTranslations) {
        //                 return this('questionsTranslations.create', `${process.env.PUBLIC_PATH || ''}/rest/questions-translations`, questionsTranslationsColumns, questionsTranslations.actions, questionsTranslations.selectors)
        //             }
        //         }),
        //         simpleLazyLoadedRoute({
        //             path: `:id/edit`,
        //             name: 'questionsTranslations.edit',
        //             pageName: 'questionsTranslations',
        //             require: ['EditRecordPage', 'rest/questions-translations'],
        //             container: function EditRecordPage(questionsTranslations) {
        //                 return this('questionsTranslations.edit', `${process.env.PUBLIC_PATH || ''}/rest/questions-translations`, questionsTranslationsColumns, questionsTranslations.actions, questionsTranslations.selectors)
        //             }
        //         }),
        //         simpleLazyLoadedRoute({
        //             path: `:id`,
        //             name: 'questionsTranslations.view',
        //             pageName: 'questionsTranslations',
        //             require: ['ViewRecordPage', 'rest/questions-translations'],
        //             container: function ViewRecordPage(questionsTranslations) {
        //                 return this('questionsTranslations.view', `${process.env.PUBLIC_PATH || ''}/rest/questions-translations`, questionsTranslationsColumns, questionsTranslations.actions, questionsTranslations.selectors)
        //             }
        //         })
        //     ]
        // }),
        simpleLazyLoadedRoute({
            path: `rest/state`,
            name: 'state',
            pageName: 'state',
            require: ['RecordsPage', 'rest/state'],
            id: 'AdminState',
            data: {
                path: '/rest/state',
                title: 'State',
                users: devUsers,
                icon: 'State'
            },
            container: function RecordsPage(state) {
                return this('state', `${process.env.PUBLIC_PATH || ''}/rest/state`, stateColumns, state.actions, state.selectors, filterColumns, true, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'state.create',
                    pageName: 'state',
                    require: ['CreateRecordPage', 'rest/state'],
                    container: function CreateRecordPage(state) {
                        return this('state.create', `${process.env.PUBLIC_PATH || ''}/rest/state`, stateColumns, state.actions, state.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'state.edit',
                    pageName: 'state',
                    require: ['EditRecordPage', 'rest/state'],
                    container: function EditRecordPage(state) {
                        return this('state.edit', `${process.env.PUBLIC_PATH || ''}/rest/state`, stateColumns, state.actions, state.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'state.view',
                    pageName: 'state',
                    require: ['ViewRecordPage', 'rest/state'],
                    container: function ViewRecordPage(state) {
                        return this('state.view', `${process.env.PUBLIC_PATH || ''}/rest/state`, stateColumns, state.actions, state.selectors)
                    }
                })
            ]
        }),
        ResourceCategories('formOtpCategories', 'formOtpCategories', 'Form OTP', 'FormOtp', devUsers, 'AdminFormOtps', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/forms/otps`,
                name: 'formOtps',
                pageName: 'formOtps',
                require: ['RecordsPage', 'rest/forms/otps'],
                data: {
                    path: 'rest/forms/otps',
                    title: 'clients',
                },
                container: function RecordsPage(formOtps) {
                    return this('formOtps', `${process.env.PUBLIC_PATH || ''}/formOtpCategories/rest/forms/otps`, formOtpColumns, formOtps.actions, formOtps.selectors, filterColumns)
                }
            }),
            simpleLazyLoadedRoute({
                path: `rest/OtherParties/otps`,
                name: 'otherPartiesFormOtp',
                pageName: 'otherPartiesFormOtp',
                require: ['RecordsPage', 'rest/OtherParties/otps'],
                data: {
                    path: 'rest/OtherParties/otps',
                    title: 'non-parties'
                },
                container: function RecordsPage(otherPartiesFormOtp) {
                    return this('otherPartiesFormOtp', `${process.env.PUBLIC_PATH || ''}/formOtpCategories/rest/OtherParties/otps`, otherPartiesFormOtpColumns, otherPartiesFormOtp.actions, otherPartiesFormOtp.selectors, filterColumns)
                }
            })
        ]),
        simpleLazyLoadedRoute({
            path: `rest/discount-code/history`,
            name: 'discountCodeHistory',
            pageName: 'discountCodeHistory',
            require: ['RecordsPage', 'rest/discount-code/history'],
            id: 'DiscountCodeHistory',
            data: {
                path: '/rest/discount-code/history',
                title: 'Discount Code History',
                users: devUsers,
                icon: 'Discount'
            },
            container: function RecordsPage(discountCodeHistory) {
                return this('discountCodeHistory', `${process.env.PUBLIC_PATH || ''}/rest/discount-code/history`, discountCodeHistoryColumns, discountCodeHistory.actions, discountCodeHistory.selectors, filterColumns)
            }
        }),
        ResourceCategories('renewalCategories', 'renewalCategories', 'Subscription Renewals', 'SubscriptionRenewals', devUsers, 'Renewal', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/subscription-yearly-renewal`,
                name: 'yearlyRenewal',
                pageName: 'yearlyRenewal',
                require: ['RecordsPage', 'rest/subscription-yearly-renewal'],
                chunk: ['subscriptions'],
                data: {
                    path: 'rest/subscription-yearly-renewal',
                    title: 'Yearly Subscriptions'                    
                },
                container: function RecordsPage(yearlyRenewal) {
                    return this('yearlyRenewal', `${process.env.PUBLIC_PATH || ''}/renewalCategories/rest/subscription-yearly-renewal`, subscriptionRenewalYearlyColumns, yearlyRenewal.actions, yearlyRenewal.selectors, filterColumns, false, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'yearlyRenewal.view',
                        pageName: 'yearlyRenewal',
                        require: ['ViewSubscriptionRenewalsPage', 'rest/subscription-yearly-renewal'],
                        chunk: ['subscriptions'],
                        container: function ViewSubscriptionRenewalsPage(yearlyRenewal) {
                            return this('yearlyRenewal.view', `${process.env.PUBLIC_PATH || ''}/renewalCategories/rest/subscription-yearly-renewal`, subscriptionRenewalYearlyColumns, yearlyRenewal.actions, yearlyRenewal.selectors)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/subscription-monthly-renewal`,
                name: 'monthlyRenewal',
                pageName: 'monthlyRenewal',
                require: ['RecordsPage', 'rest/subscription-monthly-renewal'],
                data: {
                    path: 'rest/subscription-monthly-renewal',
                    title: 'Monthly Subscriptions',
                },
                container: function RecordsPage(monthlyRenewal) {
                    return this('monthlyRenewal', `${process.env.PUBLIC_PATH || ''}/renewalCategories/rest/subscription-monthly-renewal`, subscriptionRenewalMonthlyColumns, monthlyRenewal.actions, monthlyRenewal.selectors, filterColumns)
                }
            })            
        ]),
        simpleLazyLoadedRoute({
            path: `globalSettings`,
            name: 'globalSettings',
            pageName: 'globalSettings',  
            require: ['GlobalSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'GlobalSettings',
            data: {
                path: '/globalSettings',
                title: 'Pricing',
                icon: 'Pricing'
            },
            container: function GlobalSettingsPage(settings) {
                return this('settings', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalSettingsColumns, settings.actions, settings.selectors, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:tab`,
                    name: 'globalSettings.view',
                    pageName: 'globalSettings',
                    require: ['ViewGlobalSettingsPage', 'globalSettings'],
                    chunk: ['globalSettings'],
                    container: function GlobalSettingsPage(settings) {
                        return this('settings.view', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalSettingsColumns, settings.actions, settings.selectors, true)
                    },
                    childRoutes: [
                        simpleLazyLoadedRoute({
                            path: `rest/plans`,
                            name: 'plans',
                            pageName: 'plans',
                            require: ['RecordsPage', 'rest/plans'],
                            container: function RecordsPage(plans) {
                                return this('plans', `${process.env.PUBLIC_PATH || ''}/globalSettings/0/rest/plans`, subscriptionsPlansColumns, plans.actions, plans.selectors, filterColumns, true, true)
                            },
                            childRoutes: [
                                simpleLazyLoadedRoute({
                                    path: `create`,
                                    name: 'plans.create',
                                    pageName: 'plans',
                                    require: ['CreateRecordPage', 'rest/plans'],
                                    container: function CreateRecordPage(plans) {
                                        return this('plans.create', `${process.env.PUBLIC_PATH || ''}/globalSettings/0/rest/plans`, subscriptionsPlansColumns, plans.actions, plans.selectors)
                                    }
                                }),
                                simpleLazyLoadedRoute({
                                    path: `:id/edit`,
                                    name: 'plans.edit',
                                    pageName: 'plans',
                                    require: ['EditRecordPage', 'rest/plans'],
                                    container: function EditRecordPage(plans) {
                                        return this('plans.edit', `${process.env.PUBLIC_PATH || ''}/globalSettings/0/rest/plans`, subscriptionsPlansColumns, plans.actions, plans.selectors, [subscriptionsPlansView])
                                    }
                                }),
                                simpleLazyLoadedRoute({
                                    path: `:id`,
                                    name: 'plans.view',
                                    pageName: 'plans',
                                    require: ['ViewRecordPage', 'rest/plans'],
                                    container: function ViewRecordPage(plans) {
                                        return this('plans.view', `${process.env.PUBLIC_PATH || ''}/globalSettings/0/rest/plans`, subscriptionsPlansColumns, plans.actions, plans.selectors)
                                    }
                                })
                            ]
                        })
                    ]
                })
            ]
        }),
        simpleLazyLoadedRoute({
            path: `otherSettings`,
            name: 'otherSettings',
            pageName: 'globalSettings',
            require: ['ViewGlobalOtherSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'OtherSettings',
            data: {
                path: '/otherSettings',
                title: 'Settings',
                icon: 'Settings'
            },
            container: function ViewGlobalOtherSettingsPage(settings) {
                return this('otherSettings', `${process.env.PUBLIC_PATH || ''}/otherSettings`, globalOtherSettingsColumns, settings.actions, Object.assign({}, { ...settings.selectors, selectUser }), true)
            }
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}


/**
 * 
 * Medical expert
 * 
 */


import schema from './schema';
import { selectSessionExpand, selectUser } from 'blocks/session/selectors';

const filterColumns = schema().filterColumns();
const medicalHistoryColumns = schema().medicalHistory().columns;
const medicalDocumentsColumns = schema().medicalHistory().medicalDocuments;
const uploadSummaryColumns = schema().medicalHistory().uploadSummary;
const statusFormColumns = schema().medicalHistory().statusForm;
const excelExportColumns = schema().excelExport().columns;
const medicalExportSettingsColumns = schema().medicalExportSettings;


export default function (simpleLazyLoadedRoute) {

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Dashboard',
                path: '/',
                route: true
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `rest/medical-history-summery`,
            name: 'medicalHistory',
            pageName: 'medicalHistory',
            require: ['MedicalPage', 'rest/medical-history-summery'],
            chunk: ['medicalHistory'],
            id: 'MedicalHistory',
            data: {
                path: '/rest/medical-history-summery',
                title: 'Medical History',
                icon: 'MedicalHistory'
            },
            container: function MedicalPage(medicalHistory) {
                return this('medicalHistory', `${process.env.PUBLIC_PATH || ''}/rest/medical-history-summery`, Object.assign({}, { medicalHistoryColumns, excelExportColumns }), medicalHistory.actions, medicalHistory.selectors, filterColumns, true, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:id/form`,
                    name: 'medicalHistory.edit',
                    pageName: 'medicalHistory',
                    require: ['ViewMedicalHistoryPage', 'rest/medical-history-summery'],
                    chunk: ['medicalHistory'],
                    data: {
                        path: '/rest/medical-history-summery',
                        title: 'Medical History'
                    },
                    container: function ViewMedicalHistoryPage(medicalHistory) {
                        return this('cases.detail', `${process.env.PUBLIC_PATH || ''}/rest/medical-history-summery`, Object.assign({}, { medicalDocumentsColumns, uploadSummaryColumns, statusFormColumns }), medicalHistory.actions, Object.assign({}, medicalHistory.selectors, { selectSessionExpand }), filterColumns)
                    }
                })
            ]
        }),
        simpleLazyLoadedRoute({
            path: `otherSettings`,
            name: 'otherSettings',
            pageName: 'globalSettings',
            require: ['ViewGlobalOtherSettingsPage', 'globalSettings'],
            chunk: ['globalSettings'],
            id: 'OtherSettings',
            data: {
                path: '/otherSettings',
                title: 'Medical Summary',
                icon: 'Prompt'
            },
            container: function ViewGlobalOtherSettingsPage(settings) {
                return this('otherSettings', `${process.env.PUBLIC_PATH || ''}/otherSettings`, medicalExportSettingsColumns, settings.actions, Object.assign({}, { ...settings.selectors, selectUser }), true)
            }
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}
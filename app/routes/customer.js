/**
 * 
 * Customer
 * 
 */

import schema from './schema';
import ClientSubmitButton from 'components/ClientSubmitButton';
import ResourceCategories from './resourceCategories';
import FilevineSessionForm from 'components/FilevineSessionForm';
import LitifyInfoDialog from 'components/LitifyInfoDialog';
import CustomCreateUserBtn from 'components/CustomCreateUserBtn';
import CustomCreateButton from 'components/CustomCreateButton';
import { propoundingPlansArray } from 'utils/plans';

import { selectUser, selectVersion, selectSessionExpand, selectError as sessionError, selectSuccess as sessionSuccess, selectMyCaseRecords, selectButtonLoader as sessionLoader, selectSessionkey as sessionKey, selectStepper, selectDiscountDetail, selectClioRecords } from 'blocks/session/selectors';
import { updateVersion, updatePracticeObjections, clearCache, myCaseLoadRecords, verifySession as verifySessionAction, clioLoadRecords } from 'blocks/session/actions';

const clientsColumns = schema().clients().columns;
const casesColumns = schema().cases().columns;
const ordersColumns = schema().orders().columns;
const filterColumns = schema().filterColumns();
const legalForms = schema().legalForms();
const usersColumns = schema().users().columns;
const objectionColumns = schema().objections().columns;
const settingsColumns = schema().settings;
const customizedForm = schema().cases().customizedForm;
const standardFormColumns = schema().cases().standardForm;
const disclosureFormColumns = schema().cases().disclosureForm;
const questionFormColumns = schema().questionForm().columns;
const partiesColumns = schema().parties().columns;
const partiesForm = schema().cases().partiesForm;
const subscriptionsHistoryColumns = schema().subscriptionsHistory;
const standardEditFormColumns = schema().standardEditForm().columns;
const attachCaseForm = schema().cases().attachCase;
const clientsForm = schema().cases().clientsForm;
const federalObjectionsColumns = schema().federalObjections().columns;
const POSForm = schema().POS();
const opposingCounselFormColumns = schema().opposingCounselForm().columns;
const propoundColumns = schema().propound().columns;
const propoundUploadTemplateColumns = schema().propoundUploadTemplate().columns;
const viewCasesColumns = schema().viewCases();
const propoundPOSForm = schema().propoundPOS();
const propoundEmailDocumentColumns = schema().propoundEmailDocument;
const templateNameForm = schema().cases().templateName;
const respondCustomTemplateColumns = schema().respondCustomTemplate().columns;
const customTemplateNote = schema().respondCustomTemplate().customNote();
const modifiedCustomTemplateOptions = schema().respondCustomTemplate().modifiedTemplateOptions;
const userLimitationConfirmMessage = schema().users().customConfirmMessage;
const customUserCreateDescription = schema().users().customDescription;
const customTemplateConfirmMessage = schema().respondCustomTemplate().customConfirmMessage;
const filevineForm = schema().filevineSecret().columns;
const filevineList = schema().filevineList().columns;
const clientCaseForm = schema().clientWithCase;
const filevineInstructionNote = schema().filevineSecret().instructionNote();
const myCaseListView = schema().myCaseListView().columns;
const respondEmailDocumentColumns = schema().respondEmailDocument;
const propoundCustomTemplateColumns = schema().propoundCustomTemplate().columns;
const selectQuestionTemplateColumns = schema().selectQuestionTemplate().columns;
const userRecordFormColumns = schema().userRecordForm;
const createDocumentFormColumns = schema().createDocument;
const quickCreateDocumentInfoFormColumns = schema().quickCreateDocumentInfo;
const quickCreateClientCaseForm = schema().quickCreateClientCase;
const litifyListView = schema().litifyListView().columns;
const upShell = schema().upShell;
const userBasedLimitationConfirmMessage = schema().users().userLimitationCustomConfirmMessage;
const quickCreateDescriptionModal = schema().quickCreateDescriptionModal().description;
const emailSentHistoryModal = schema().emailSentHistoryModal().description;

export default function (simpleLazyLoadedRoute, user) {

    const propoundingPlanType = user && user.plan_type && typeof user.plan_type === 'object' && user.plan_type['propounding'] || false;
    const isPropoundPagePlan = propoundingPlanType && propoundingPlansArray.includes(propoundingPlanType) || false;
    const isPropoundAccess = isPropoundPagePlan;
    const isPropounding = propoundingPlansArray;

    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Dashboard',
                path: '/',
                route: true
            },
            container: 'DashboardPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `quickCreate`,
            name: 'quickCreate',
            pageName: 'quickCreate',
            queryParams: 'quickCreate',
            require: ['QuickCreateDocumentPage', 'rest/document-extraction-progress'],
            chunk: ['quickCreate', 'cases', 'subscriptions'],
            id: 'QuickCreate',
            data: {
                path: '/quickCreate',
                title: 'Quick Create',
                icon: 'quickCreate'
            },
            container: function QuickCreateDocumentPage(quickCreate) {
                return this('quickCreate', `${process.env.PUBLIC_PATH || ''}/quickCreate`, Object.assign({}, { subscriptionDetailsColumns: schema().subscriptionDetails, respondEmailDocumentColumns, upShellColumns: upShell, quickCreateDescriptionModal }), schema().legalForms, Object.assign({}, quickCreate.actions, { clearCache, verifySessionAction }), Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser, sessionError, sessionLoader, sessionKey, selectStepper }), filterColumns)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:id/reviewDocument`,
                    name: 'quickCreate.reviewDocument',
                    pageName: 'quickCreate',
                    queryParams: 'quickCreate',
                    require: ['ReviewDocumentPage', 'rest/document-extraction-progress'],
                    chunk: ['quickCreate', 'cases', 'subscriptions'],
                    container: function ReviewDocumentPage(quickCreate) {
                        return this('quickCreate.reviewDocument', `${process.env.PUBLIC_PATH || ''}/quickCreate`, Object.assign({}, { createDocumentFormColumns, quickCreateDocumentInfoFormColumns }), quickCreate.actions, Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser }))
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/questionsForm`,
                    name: 'quickCreate.questionsForm',
                    pageName: 'quickCreate',
                    queryParams: 'quickCreate',
                    require: ['ViewQuestionsFormPage', 'rest/document-extraction-progress'],
                    chunk: ['quickCreate', 'cases', 'subscriptions'],
                    container: function ViewQuestionsFormPage(quickCreate) {
                        return this('quickCreate.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), quickCreate.actions, Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser }))
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/standardForm`,
                    name: 'quickCreate.standardForm',
                    pageName: 'quickCreate',
                    queryParams: 'quickCreate',
                    require: ['ViewStandardFormPage', 'rest/document-extraction-progress'],
                    chunk: ['quickCreate', 'cases', 'subscriptions'],
                    container: function ViewStandardFormPage(quickCreate) {
                        return this('quickCreate.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, quickCreate.actions, Object.assign({}, quickCreate.selectors, { selectSessionExpand, selectUser }))
                    }
                }),
            ]
        }),
        ResourceCategories('clientCategories', 'clientCategories', 'Clients', 'Clients', false, 'Clients', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `clients`,
                name: 'clients',
                pageName: 'clients',
                require: ['RecordsPage', 'clients'],
                chunk: ['myCase', 'filevine'],
                data: {
                    path: '/clients',
                    title: 'Clients'
                },
                container: function RecordsPage(clients) {
                    return this('clients', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, clientsColumns, clients.actions, clients.selectors, filterColumns, true, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'clients.create',
                        pageName: 'clients',
                        require: ['CreateRecordPage', 'clients'],
                        chunk: ['myCase', 'filevine'],
                        container: function CreateRecordPage(clients) {
                            return this('clients.create', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, schema().clients, clients.actions, clients.selectors, false, ClientSubmitButton)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'clients.edit',
                        pageName: 'clients',
                        require: ['EditRecordPage', 'clients'],
                        chunk: ['myCase', 'filevine'],
                        container: function EditRecordPage(clients) {
                            return this('clients.edit', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, schema().clients, clients.actions, clients.selectors, [FilevineSessionForm, LitifyInfoDialog])
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'clients.view',
                        pageName: 'clients',
                        require: ['ViewRecordPage', 'clients'],
                        chunk: ['myCase', 'filevine'],
                        container: function ViewRecordPage(clients) {
                            return this('clients.view', `${process.env.PUBLIC_PATH || ''}/clientCategories/clients`, clientsColumns, clients.actions, clients.selectors)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `rest/other-parties`,
                name: 'other-parties',
                pageName: 'other-parties',
                require: ['RecordsPage', 'rest/other-parties'],
                data: {
                    path: 'rest/other-parties',
                    title: 'Non-Parties'
                },
                container: function RecordsPage(otherParties) {
                    return this('other-parties', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, partiesColumns, otherParties.actions, otherParties.selectors, filterColumns, true, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'other-parties.create',
                        pageName: 'other-parties',
                        require: ['CreateRecordPage', 'rest/other-parties'],
                        container: function CreateRecordPage(otherParties) {
                            return this('other-parties.create', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, schema().parties, otherParties.actions, otherParties.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'other-parties.edit',
                        pageName: 'other-parties',
                        require: ['EditRecordPage', 'rest/other-parties'],
                        container: function EditRecordPage(otherParties) {
                            return this('other-parties.edit', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, schema().parties, otherParties.actions, otherParties.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'other-parties.view',
                        pageName: 'other-parties',
                        require: ['ViewRecordPage', 'rest/other-parties'],
                        container: function ViewRecordPage(otherParties) {
                            return this('other-parties.view', `${process.env.PUBLIC_PATH || ''}/clientCategories/rest/other-parties`, partiesColumns, otherParties.actions, otherParties.selectors)
                        }
                    })
                ]
            })
        ]),
        ResourceCategories('casesCategories', 'casesCategories', 'Cases', 'Cases', false, 'Cases', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `cases`,
                name: 'cases',
                pageName: 'cases',
                require: ['CasesPage', 'cases'],
                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                data: {
                    path: '/cases',
                    title: 'Cases',
                    icon: 'Cases'
                },
                container: function CasesPage(cases) {
                    return this('cases', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, schema().cases, Object.assign({}, cases.actions, { myCaseLoadRecords, clearCache, verifySessionAction, clioLoadRecords }), Object.assign({}, cases.selectors, { selectSessionExpand, selectUser, selectMyCaseRecords, sessionError, sessionLoader, sessionKey, selectStepper, selectClioRecords }), filterColumns, { customizedForm, attachCaseForm, clientsForm, filevineForm, filevineList, myCaseListView, userRecordFormColumns, litifyListView }, filevineInstructionNote)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'cases.create',
                        pageName: 'cases',
                        require: ['CreateRecordPage', 'cases'],
                        container: function CreateRecordPage(cases) {
                            return this('cases.create', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, schema().cases, cases.actions, cases.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'cases.edit',
                        pageName: 'cases',
                        require: ['EditRecordPage', 'cases'],
                        container: function EditRecordPage(cases) {
                            return this('cases.edit', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, schema().cases, cases.actions, cases.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/form/:type`,
                        name: 'cases.detail',
                        pageName: 'cases',
                        require: ['ViewCasesDetailPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewCasesDetailPage(cases) {
                            return this('cases.detail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, partiesColumns: partiesForm, POSColumns: POSForm, modifiedCustomTemplateOptions, filevineForm, subscriptionDetailsColumns: schema().subscriptionDetails, respondEmailDocumentColumns, selectQuestionTemplateColumns, userRecordFormColumns, upShellColumns: upShell, objectionColumns: legalForms.objectionColumns, customLegalColumns: legalForms.customLegalColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns, filevineInstructionNote)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/signature/:type/:legalforms_id`,
                        name: 'cases.signature',
                        pageName: 'cases',
                        require: ['ViewSignaturePage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewSignaturePage(cases) {
                            return this('cases.signature', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, disclosureFormColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/form`,
                        name: 'cases.view',
                        pageName: 'cases',
                        require: ['ViewCasesPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewCasesPage(cases) {
                            return this('cases.view', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, templateNameForm, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/forms`,
                        name: 'cases.view',
                        pageName: 'cases',
                        require: ['ViewCasesCategoriesPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewCasesCategoriesPage(cases) {
                            return this('cases.view', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, viewCasesColumns, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                        },
                        childRoutes: [
                            simpleLazyLoadedRoute({
                                path: `:tab/respond`,
                                name: 'cases.respond',
                                pageName: 'cases',
                                require: ['ViewRespondPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewRespondPage(cases) {
                                    return this('cases.respond', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, templateNameForm, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `respond/:type/details`,
                                name: 'cases.detail',
                                pageName: 'cases',
                                require: ['ViewCasesDetailPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewCasesDetailPage(cases) {
                                    return this('cases.detail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, partiesColumns: partiesForm, POSColumns: POSForm, modifiedCustomTemplateOptions, filevineForm, subscriptionDetailsColumns: schema().subscriptionDetails, respondEmailDocumentColumns, selectQuestionTemplateColumns, userRecordFormColumns, upShellColumns: upShell,objectionColumns: legalForms.objectionColumns, customLegalColumns: legalForms.customLegalColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns, filevineInstructionNote)
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `signature/:type/:legalforms_id`,
                                name: 'cases.signature',
                                pageName: 'cases',
                                require: ['ViewSignaturePage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewSignaturePage(cases) {
                                    return this('cases.signature', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { legalColumns: legalForms.legalColumns, uploadColumns: legalForms.billingForm, standardFormColumns, disclosureFormColumns }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `respond/standardForm`,
                                name: 'cases.standardForm',
                                pageName: 'cases',
                                require: ['ViewStandardFormPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewStandardFormPage(cases) {
                                    return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `respond/questionsForm`,
                                name: 'cases.questionsForm',
                                pageName: 'cases',
                                require: ['ViewQuestionsFormPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewQuestionsFormPage(cases) {
                                    return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `:tab/propound`,
                                name: 'cases.propound',
                                pageName: 'cases',
                                require: ['ViewPropoundPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewPropoundPage(cases) {
                                    return this('cases.propound', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, clientCaseForm, quickCreateClientCaseForm, emailSentHistoryModal }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `propound/:type/:pid/details`,
                                name: 'cases.propoundDetail',
                                pageName: 'cases',
                                require: ['ViewPropoundDetailPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewPropoundDetailPage(cases) {
                                    return this('cases.propoundDetail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { uploadColumns: legalForms.billingForm, standardFormColumns, propoundEmailDocumentColumns, POSColumns: propoundPOSForm }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `propound/:type/:pid/standardForm`,
                                name: 'cases.standardForm',
                                pageName: 'cases',
                                require: ['ViewStandardFormPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewStandardFormPage(cases) {
                                    return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `propound/:type/:pid/questionsForm`,
                                name: 'cases.questionsForm',
                                pageName: 'cases',
                                require: ['ViewPropoundFormEditPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewPropoundFormEditPage(cases) {
                                    return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand }))
                                }
                            }),
                            simpleLazyLoadedRoute({
                                path: `:tab/medicalHistory`,
                                name: 'cases.medicalHistory',
                                pageName: 'cases',
                                require: ['ViewCasesMedicalHistoryPage', 'cases'],
                                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                                container: function ViewCasesMedicalHistoryPage(cases) {
                                    return this('cases.medicalHistory', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                                }
                            })
                        ]
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/questionsForm`,
                        name: 'cases.questionsForm',
                        pageName: 'cases',
                        require: ['ViewQuestionsFormPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewQuestionsFormPage(cases) {
                            return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/standardForm`,
                        name: 'cases.standardForm',
                        pageName: 'cases',
                        require: ['ViewStandardFormPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewStandardFormPage(cases) {
                            return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/propound`,
                        name: 'cases.propound',
                        pageName: 'cases',
                        require: ['ViewPropoundPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewPropoundPage(cases) {
                            return this('cases.propound', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { standardFormColumns, casesColumns, disclosureFormColumns, clientCaseForm, quickCreateClientCaseForm }), schema().legalForms, cases.actions, Object.assign({}, cases.selectors, { selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/:type/propound/:pid/details`,
                        name: 'cases.propoundDetail',
                        pageName: 'cases',
                        require: ['ViewPropoundDetailPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewPropoundDetailPage(cases) {
                            return this('cases.propoundDetail', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { uploadColumns: legalForms.billingForm, standardFormColumns, propoundEmailDocumentColumns, POSColumns: propoundPOSForm }), cases.actions, Object.assign({}, { ...cases.selectors }, { selectSessionExpand }), filterColumns)
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/:type/propound/:pid/standardForm`,
                        name: 'cases.standardForm',
                        pageName: 'cases',
                        require: ['ViewStandardFormPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewStandardFormPage(cases) {
                            return this('cases.standardForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, standardEditFormColumns, cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand, selectUser }))
                        }
                    }),
                    isPropoundAccess && simpleLazyLoadedRoute({
                        path: `:id/:type/propound/:pid/questionsForm`,
                        name: 'cases.questionsForm',
                        pageName: 'cases',
                        require: ['ViewPropoundFormEditPage', 'cases'],
                        chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                        container: function ViewPropoundFormEditPage(cases) {
                            return this('cases.questionsForm', `${process.env.PUBLIC_PATH || ''}/casesCategories/cases`, Object.assign({}, { questionFormColumns, opposingCounselFormColumns }), cases.actions, Object.assign({}, cases.selectors, { selectSessionExpand }))
                        }
                    })
                ].filter(childRoute => childRoute && typeof childRoute === 'object')
            }),
            simpleLazyLoadedRoute({
                path: `rest/archive/cases`,
                name: 'archiveCases',
                pageName: 'archiveCases',
                require: ['ArchiveCasesPage', 'rest/archive/cases'],
                chunk: ['cases', 'filevine', 'myCase', 'subscriptions', 'clio'],
                data: {
                    path: 'rest/archive/cases',
                    title: 'Archived cases'
                },
                container: function ArchiveCasesPage(cases) {
                    return this('archiveCases', `${process.env.PUBLIC_PATH || ''}/casesCategories/rest/archive/cases`, schema().archiveCases, Object.assign({}, cases.actions, { clearCache }), Object.assign({}, cases.selectors, { selectSessionExpand }), filterColumns, false, false)
                }
            })
        ]),
        ResourceCategories('orderCategories', 'orderCategories', 'Orders', 'Orders', false, 'Orders', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `orders`,
                name: 'orders',
                pageName: 'orders',
                require: ['RecordsPage', 'orders'],
                data: {
                    path: '/orders',
                    title: 'Orders'
                },
                container: function RecordsPage(orders) {
                    return this('orders', `${process.env.PUBLIC_PATH || ''}/orderCategories/orders`, ordersColumns, orders.actions, orders.selectors, filterColumns)
                }
            }),
            simpleLazyLoadedRoute({
                path: `rest/subscriptions/history`,
                name: 'subscriptions',
                pageName: 'subscriptions',
                require: ['RecordsPage', 'rest/subscriptions/history'],
                data: {
                    path: 'rest/subscriptions/history',
                    title: 'Subscriptions'
                },
                container: function RecordsPage(subscriptions) {
                    return this('subscriptions', `${process.env.PUBLIC_PATH || ''}/orderCategories/rest/subscriptions/history`, subscriptionsHistoryColumns, subscriptions.actions, subscriptions.selectors, filterColumns)
                }
            })
        ]),
        simpleLazyLoadedRoute({
            path: `help`,
            name: 'help',
            pageName: 'help',
            require: ['HelpPage', 'help'],
            chunk: ['practices'],
            id: 'Help',
            data: {
                path: '/help',
                title: 'Help',
                icon: 'Help',
                nestedMenu: true,
                nestedMenuOption: [
                    {
                        id: 1,
                        label: 'FAQs',
                        url: 'https://www.esquiretek.com/frequently-asked-questions/',
                        icon: 'Faqs'
                    }, 
                    {
                        id: 2,
                        label: 'Tek Academy Videos',
                        url: 'https://www.esquiretek.com/help/',
                        icon: 'Video'
                    }
                ],
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `rest/users`,
            name: 'users',
            pageName: 'users',
            require: ['RecordsPage', 'rest/users'],
            id: 'Users',
            data: {
                path: '/rest/users',
                title: 'Users',
                icon: 'Users',
                separator: true
            },
            container: function RecordsPage(users) {
                return this('users', `${process.env.PUBLIC_PATH || ''}/rest/users`, usersColumns, users.actions, users.selectors, filterColumns, true, true, false, false, userLimitationConfirmMessage, false, userBasedLimitationConfirmMessage)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'users.create',
                    pageName: 'users',
                    require: ['CreateRecordPage', 'rest/users'],
                    container: function CreateRecordPage(users) {
                        return this('users.create', `${process.env.PUBLIC_PATH || ''}/rest/users`, schema().users, users.actions, users.selectors, false, false, userLimitationConfirmMessage, CustomCreateUserBtn, customUserCreateDescription, CustomCreateButton, userBasedLimitationConfirmMessage)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'users.edit',
                    pageName: 'users',
                    require: ['EditRecordPage', 'rest/users'],
                    container: function EditRecordPage(users) {
                        return this('users.edit', `${process.env.PUBLIC_PATH || ''}/rest/users`, schema().users, users.actions, users.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'users.view',
                    pageName: 'users',
                    require: ['ViewRecordPage', 'rest/users'],
                    container: function ViewRecordPage(users) {
                        return this('users.view', `${process.env.PUBLIC_PATH || ''}/rest/users`, usersColumns, users.actions, users.selectors)
                    }
                })
            ]
        }),
        ResourceCategories('objectionsCategories', 'objectionsCategories', 'Objections', 'Objections', false, 'Objections', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: `rest/customer-objections`,
                name: 'objections',
                pageName: 'objections',
                require: ['ObjectionsPage', 'rest/customer-objections'],
                data: {
                    path: '/rest/customer-objections',
                    title: 'State',
                    icon: 'Objections'
                },
                container: function ObjectionsPage(objections) {
                    return this('objections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/customer-objections`, objectionColumns, Object.assign({}, objections.actions, { updatePracticeObjections, clearCache }), Object.assign({}, objections.selectors, { selectUser, sessionError, sessionSuccess }), filterColumns, true, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'objections.create',
                        pageName: 'objections',
                        require: ['CreateRecordPage', 'rest/customer-objections'],
                        container: function CreateRecordPage(objections) {
                            return this('objections.create', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/customer-objections`, objectionColumns, objections.actions, objections.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'objections.edit',
                        pageName: 'objections',
                        require: ['EditRecordPage', 'rest/customer-objections'],
                        container: function EditRecordPage(objections) {
                            return this('objections.edit', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/customer-objections`, objectionColumns, objections.actions, objections.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'objections.view',
                        pageName: 'objections',
                        require: ['ViewRecordPage', 'rest/customer-objections'],
                        container: function ViewRecordPage(objections) {
                            return this('objections.view', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/rest/customer-objections`, objectionColumns, objections.actions, objections.selectors)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: `federal/customer-objections`,
                name: 'federalObjections',
                pageName: 'federalObjections',
                require: ['ObjectionsFederalPage', 'federal/customer-objections'],
                data: {
                    path: '/federal/customer-objections',
                    title: 'Federal',
                    icon: 'Objections'
                },
                container: function ObjectionsFederalPage(objections) {
                    return this('federalObjections', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/customer-objections`, federalObjectionsColumns, Object.assign({}, objections.actions, { updatePracticeObjections, clearCache }), Object.assign({}, objections.selectors, { selectUser, sessionError, sessionSuccess }), filterColumns, true, true, true)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'federalObjections.create',
                        pageName: 'federalObjections',
                        require: ['CreateRecordPage', 'federal/customer-objections'],
                        container: function CreateRecordPage(objections) {
                            return this('federalObjections.create', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/customer-objections`, federalObjectionsColumns, objections.actions, objections.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'federalObjections.edit',
                        pageName: 'federalObjections',
                        require: ['EditRecordPage', 'federal/customer-objections'],
                        container: function EditRecordPage(objections) {
                            return this('federalObjections.edit', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/customer-objections`, federalObjectionsColumns, objections.actions, objections.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'federalObjections.view',
                        pageName: 'federalObjections',
                        require: ['ViewRecordPage', 'federal/customer-objections'],
                        container: function ViewRecordPage(objections) {
                            return this('federalObjections.view', `${process.env.PUBLIC_PATH || ''}/objectionsCategories/federal/customer-objections`, federalObjectionsColumns, objections.actions, objections.selectors)
                        }
                    })
                ]
            })
        ]),
        isPropoundAccess && ResourceCategories('customCategories', 'customCategories', 'Custom Templates', 'CustomTemplate', false, 'CustomTemplate', simpleLazyLoadedRoute, [
            simpleLazyLoadedRoute({
                path: 'rest/practice-template',
                name: 'respondingTemplate',
                pageName: 'respondingTemplate',
                queryParams: 'responding',
                require: ['RecordsPage', 'rest/practice-template'],
                chunk: ['customTemplate'],
                data: {
                    path: '/rest/practice-template',
                    title: 'Responding',
                    icon: 'CustomTemplate'
                },
                container: function RecordsPage(customTemplate) {
                    return this('respondingTemplate', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, respondCustomTemplateColumns, customTemplate.actions, customTemplate.selectors, filterColumns, true, true, customTemplateNote, false, customTemplateConfirmMessage)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'respondingTemplate.create',
                        pageName: 'respondingTemplate',
                        queryParams: 'responding',
                        require: ['CreateRecordPage', 'rest/practice-template'],
                        chunk: ['customTemplate'],
                        container: function CreateRecordPage(customTemplate) {
                            return this('respondingTemplate.create', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, schema().respondCustomTemplate, customTemplate.actions, customTemplate.selectors, false, false, customTemplateConfirmMessage)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'respondingTemplate.edit',
                        pageName: 'respondingTemplate',
                        queryParams: 'responding',
                        require: ['EditRecordPage', 'rest/practice-template'],
                        chunk: ['customTemplate'],
                        container: function EditRecordPage(customTemplate) {
                            return this('respondingTemplate.edit', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, schema().respondCustomTemplate, customTemplate.actions, customTemplate.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'respondingTemplate.view',
                        pageName: 'respondingTemplate',
                        queryParams: 'responding',
                        require: ['ViewRecordPage', 'rest/practice-template'],
                        chunk: ['customTemplate'],
                        container: function ViewRecordPage(customTemplate) {
                            return this('respondingTemplate.view', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-template`, respondCustomTemplateColumns, customTemplate.actions, customTemplate.selectors)
                        }
                    })
                ]
            }),
            simpleLazyLoadedRoute({
                path: 'rest/practice-propound-template',
                name: 'propoundingTemplate',
                pageName: 'propoundingTemplate',
                queryParams: 'propounding',
                require: ['RecordsPage', 'rest/practice-propound-template'],
                chunk: ['customTemplate'],
                data: {
                    path: 'rest/practice-propound-template',
                    title: 'Propounding',
                    icon: 'CustomTemplate'
                },
                container: function RecordsPage(customTemplate) {
                    return this('propoundingTemplate', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, propoundCustomTemplateColumns, customTemplate.actions, customTemplate.selectors, filterColumns, true, true, customTemplateNote, false, customTemplateConfirmMessage)
                },
                childRoutes: [
                    simpleLazyLoadedRoute({
                        path: `create`,
                        name: 'propoundingTemplate.create',
                        pageName: 'propoundingTemplate',
                        queryParams: 'propounding',
                        require: ['CreateRecordPage', 'rest/practice-propound-template'],
                        chunk: ['customTemplate'],
                        container: function CreateRecordPage(customTemplate) {
                            return this('propoundingTemplate.create', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, propoundCustomTemplateColumns, customTemplate.actions, customTemplate.selectors, false, false, customTemplateConfirmMessage)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id/edit`,
                        name: 'propoundingTemplate.edit',
                        pageName: 'propoundingTemplate',
                        queryParams: 'propounding',
                        require: ['EditRecordPage', 'rest/practice-propound-template'],
                        chunk: ['customTemplate'],
                        container: function EditRecordPage(customTemplate) {
                            return this('propoundingTemplate.edit', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, propoundCustomTemplateColumns, customTemplate.actions, customTemplate.selectors)
                        }
                    }),
                    simpleLazyLoadedRoute({
                        path: `:id`,
                        name: 'propoundingTemplate.view',
                        pageName: 'propoundingTemplate',
                        queryParams: 'propounding',
                        require: ['ViewRecordPage', 'rest/practice-propound-template'],
                        chunk: ['customTemplate'],
                        container: function ViewRecordPage(customTemplate) {
                            return this('propoundingTemplate.view', `${process.env.PUBLIC_PATH || ''}/customCategories/rest/practice-propound-template`, propoundCustomTemplateColumns, customTemplate.actions, customTemplate.selectors)
                        }
                    })
                ]
            }),
        ]),
        !isPropoundAccess && simpleLazyLoadedRoute({
            path: 'rest/practice-template',
            name: 'respondingTemplate',
            pageName: 'respondingTemplate',
            require: ['RecordsPage', 'rest/practice-template'],
            chunk: ['customTemplate'],
            id: 'CustomTemplate',
            data: {
                path: '/rest/practice-template',
                title: 'Custom Templates',
                icon: 'CustomTemplate'
            },
            container: function RecordsPage(customTemplate) {
                return this('respondingTemplate', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, respondCustomTemplateColumns, customTemplate.actions, customTemplate.selectors, filterColumns, true, true, customTemplateNote, false, customTemplateConfirmMessage)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'respondingTemplate.create',
                    pageName: 'respondingTemplate',
                    require: ['CreateRecordPage', 'rest/practice-template'],
                    chunk: ['customTemplate'],
                    container: function CreateRecordPage(customTemplate) {
                        return this('respondingTemplate.create', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, schema().respondCustomTemplate, customTemplate.actions, customTemplate.selectors, false, false, customTemplateConfirmMessage)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'respondingTemplate.edit',
                    pageName: 'respondingTemplate',
                    require: ['EditRecordPage', 'rest/practice-template'],
                    chunk: ['customTemplate'],
                    container: function EditRecordPage(customTemplate) {
                        return this('respondingTemplate.edit', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, schema().respondCustomTemplate, customTemplate.actions, customTemplate.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'respondingTemplate.view',
                    pageName: 'respondingTemplate',
                    require: ['ViewRecordPage', 'rest/practice-template'],
                    chunk: ['customTemplate'],
                    container: function ViewRecordPage(customTemplate) {
                        return this('respondingTemplate.view', `${process.env.PUBLIC_PATH || ''}/rest/practice-template`, respondCustomTemplateColumns, customTemplate.actions, customTemplate.selectors)
                    }
                })
            ]
        }),
        isPropoundAccess && simpleLazyLoadedRoute({
            path: `rest/propound`,
            name: 'propound',
            pageName: 'propound',
            require: ['PropoundPage', 'rest/propound'],
            chunk: ['propound'],
            id: 'Propound',
            data: {
                path: '/rest/propound',
                title: 'Propounding Questions',
                icon: 'PropoundTemplate',
                isPropoundAccess: isPropounding
            },
            container: function PropoundPage(propound) {
                return this('propound', `${process.env.PUBLIC_PATH || ''}/rest/propound`, Object.assign({}, { propoundColumns, propoundUploadTemplateColumns }), Object.assign({}, { ...propound.actions }), Object.assign({}, { ...propound.selectors, selectUser }))
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `:id/propoundQuestions/:type`,
                    name: 'propound.questions',
                    pageName: 'propound',
                    require: ['ViewPropoundQuestionsPage', 'rest/propound'],
                    chunk: ['propound'],
                    container: function ViewPropoundQuestionsPage(propound) {
                        return this('propound.questions', `${process.env.PUBLIC_PATH || ''}/rest/propound`, Object.assign({}, { questionFormColumns }), propound.actions, Object.assign({}, propound.selectors, { selectSessionExpand }))
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'propound.view',
                    pageName: 'propound',
                    require: ['ViewPropoundTemplatePage', 'rest/propound'],
                    chunk: ['propound'],
                    container: function ViewPropoundTemplatePage(propound) {
                        return this('propound.view', `${process.env.PUBLIC_PATH || ''}/rest/propound`, propoundColumns, propound.actions, propound.selectors)
                    }
                }),
            ]
        }),
        simpleLazyLoadedRoute({
            path: `settings`,
            name: 'settings',
            pageName: 'settings',
            require: ['SettingsPage', 'settings'],
            chunk: ['cases', 'subscriptions', 'practices'],
            id: 'Settings',
            data: {
                path: '/settings',
                title: 'Settings',
                icon: 'Settings'
            },
            container: function SettingsPage(settings) {
                return this('settings', `${process.env.PUBLIC_PATH || ''}/settings`, settingsColumns, Object.assign({}, { ...settings.actions }, { updateVersion }), Object.assign({}, { ...settings.selectors }, { selectUser, selectVersion, selectDiscountDetail }))
            }
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ].filter(route => route && typeof route === 'object')
}
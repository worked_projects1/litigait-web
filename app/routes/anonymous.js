/**
 * 
 * Anonymous
 * 
 */

export default function (simpleLazyLoadedRoute) {
    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: 'Home',
                path: '/',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'login',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/login',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'reset-password',
            container: 'ResetPasswordPage'
        }),
        simpleLazyLoadedRoute({
            path: 'respond',
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'microsoft-oauth2',
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'google-oauth2',
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: 'litify',
            container: 'HomePage1'
        }),
        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}
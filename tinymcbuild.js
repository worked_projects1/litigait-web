/**
 * 
 * Copy tinyMCE modules into final build
 * 
 */
const fse = require('fs-extra');
const path = require('path');
const topDir = __dirname;
fse.copySync(path.join(topDir, 'node_modules', 'tinymce'), path.join(topDir, 'build', 'tinymce'), { overwrite: true });